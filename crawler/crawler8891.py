import os, time, socket
import pandas as pd
import re
import time
from bs4 import BeautifulSoup
from lxml import etree
import requests
import math
from datetime import datetime, timedelta, date
from random import randint
from time import sleep
import sys
import numpy as np
sys.path.insert(0, '/home/hadoop/jessielu/')

from Module.ConnFunc import read_mssql, read_test_mssql, to_mysql, read_mysql, to_test_sql_server, to_sql_server
from Module.EmailFunc import send_error_message
from dateutil.relativedelta import relativedelta

#-------------------Import end
#===========Author: Jessie Lu
#===========Date: 2021-09-27

headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36'}
URL = 'https://www.8891.com.tw/findBuz-index.html'  # 8891 Store URL
resp = requests.get(url= URL, headers=headers)
soup = BeautifulSoup(resp.text, features='html.parser')
totalNumber = int(soup.select('[style="color:#C00;"]')[0].text) #總車商數量
totalPage = math.ceil(totalNumber/30) #總頁數
now = date.today()
# now = (datetime.today() - relativedelta(days=1)).strftime('%Y-%m-%d')
store_list = '8891_store_' + str(now) + '.xlsx'
file_out='8891_Member_' + str(now) + '.xlsx'
count = 0

storeUrl = []
storeName = [] #商店名稱
contentPerson = [] #聯絡人
cellPhone = [] #手機號碼
telPhone = [] #市內電話
email = [] #信箱
address = [] #地址
onSaleCarTotal = [] #出售中車輛
OnStoreCarTotal = [] #在店車輛
isCareStore = [] #是否為嚴選
isBestStore = [] #是否為旺店

    
def crawlStoreURL(start_point):

    for i in range(start_point, totalPage+1):
        try:
            print(i)
            tmpURL = 'https://www.8891.com.tw/findBuz-index.html?firstRow=' + str(i*30)
            s = requests.session()
            s.keep_alive = False
            resp = s.get(url= tmpURL, headers=headers)
            soup = BeautifulSoup(resp.text, features='html.parser')
            nodes = soup.find_all("span", "fl mr5")
            hrefs = [node.a.get('href') for node in nodes]
            df = pd.DataFrame(hrefs, columns=['URL'])

            # 如果檔案不存在，建立檔案
            if  os.path.exists(store_list) == False:
                df.to_excel(store_list,index=False)

            # 如果檔案存在，append
            elif  os.path.exists(store_list) == True:
                df_old = pd.read_excel(store_list)
                df_combine = df_old.append(df)
                df_combine.to_excel(store_list,index=False)
            
        except Exception as e:
            print("Page {} failed, retry in 5 seconds............\n".format(i))
            print(e)
            # logPage(tmpURL)
            # 避免因短時間內重新執行而出錯
            sleep(randint(1,5))
            # 紀錄並返回錯誤的URL
            return i
        finally:
            sleep(randint(1,5))
            start_point = i
        
def crawlStore(storeList, start_point2):
        for j in range(start_point2, len(storeList['URL'])):
            try:
                print(start_point2, storeList['URL'][start_point2])
                if(storeList['URL'][start_point2].split('/')[2] == 'www.8891.com.tw'):
                    s = requests.session()
                    s.keep_alive = False
                    resp = s.get(url= storeList['URL'][start_point2], headers=headers)
                    soup = BeautifulSoup(resp.text, features='html.parser')
                    selector = etree.HTML(str(soup))
                    storeName.append(selector.xpath("//div/div[2]/h1")[0].text if len(selector.xpath("//div/div[2]/h1"))>0 else '')
                    storeUrl.append(storeList['URL'][start_point2])
                    
                    contentPerson.append(selector.xpath("//div[1]/dl[5]/dd[1]/span/span[2]")[0].text if len(selector.xpath("//div[1]/dl[5]/dd[1]/span/span[2]"))>0 else '')
                    cellPhone.append(selector.xpath("//div[1]/dl[5]/dd[1]/span/span[1]")[0].text if len(selector.xpath("//div[1]/dl[5]/dd[1]/span/span[1]"))>0 else '')
                    telPhone.append(selector.xpath("//div[1]/dl[4]/dd[1]/span")[0].text if len(selector.xpath("//div[1]/dl[4]/dd[1]/span"))>0 else '')
                    address.append(selector.xpath("//div[1]/div[1]/div[1]/div[1]/dl[2]/dd[1]")[0].text if len(selector.xpath("//div[1]/div[1]/div[1]/div[1]/dl[2]/dd[1]"))>0 else '')
                    email.append('')
                    
                    if(len(soup.find_all("ul", "buz-plus-info"))>0):
                        for p in soup.find_all("ul", "buz-plus-info")[0].get_text().split('\n'):
                            if (p.find('在店數')>=0):
                                a = p.split('：')[1].split('輛')[0].replace(' ', '')
                                if(a in (['暫無出售', '-'])):
                                    a = 0
                                OnStoreCarTotal.append(a if int(a) else 0)
                            elif(p.find('出售中')>=0):
                                a = p.split('：')[1].split('輛')[0].replace(' ', '')
                                if(a in (['暫無出售', '-'])):
                                    a = 0
                                onSaleCarTotal.append(a if int(a) else 0)
                            else:
                                pass
                            
                    #如果車商名稱前面是8891嚴選，則為嚴選車商
                    if(len(selector.xpath("//div/div[2]/h1"))>0):
                        isCareStore.append(1 if selector.xpath("//div/div[2]/h1")[0].text.find('8891嚴選') == 0 else 0)
                    else:
                        isCareStore.append(0)
                        
                    if(len(onSaleCarTotal)<len(storeName)):
                        onSaleCarTotal.append(0)
                        
                    if(len(OnStoreCarTotal)<len(storeName)):
                        OnStoreCarTotal.append(0)
                        
                    isBestStore.append(0)
                #旺店
                else:
                    s = requests.session()
                    s.keep_alive = False
                    resp = s.get(url= storeList['URL'][start_point2], headers=headers)
                    soup = BeautifulSoup(resp.text, features='html.parser')
                    selector = etree.HTML(str(soup))
                    storeName.append(selector.xpath("//div[4]/div[3]/div[1]/div[2]/div/div[2]/div/div[1]/a")[0].text if len(selector.xpath("//div[4]/div[3]/div[1]/div[2]/div/div[2]/div/div[1]/a"))>0 else '')
                    storeUrl.append(storeList['URL'][start_point2])
                    isCareStore.append(1 if storeName[-1].find('8891嚴選') == 0 else 0)
                    isBestStore.append(1)
                    if(len(soup.find_all("p", "s-bus-p"))>0):
                        for p in soup.find_all("p", "s-bus-p"):
                            tmp = p.get_text()
                            if (tmp.find('聯絡人')>=0):
                                contentPerson.append(tmp.split('：')[1])
                            elif (tmp.find('行動電話')>=0):
                                cellPhone.append(tmp.split('：')[1])
                            elif (tmp.find('市內電話')>=0):
                                telPhone.append(tmp.split('：')[1])
                            elif (tmp.find('賞車地址')>=0):
                                address.append(tmp.split('：')[1])
                            elif (tmp.find('E-mail')>=0):
                                email.append(tmp.split('：')[1])
                            elif (tmp.find('出售中')>=0):
                                a = tmp.split('：')[1].split('輛')[0]
                                if(a in (['暫無出售', '-'])):
                                    a = 0
                                onSaleCarTotal.append(a if int(a) else 0)
                            elif (tmp.find('在店數')>=0):
                                a = tmp.split('：')[1].split('輛')[0]
                                if(a in (['暫無出售', '-'])):
                                    a = 0
                                OnStoreCarTotal.append(a if int(a) else 0)
                            else:
                                pass
                        
                    if(len(contentPerson)<len(storeName)):
                        contentPerson.append('')
                    if(len(cellPhone)<len(storeName)):
                        cellPhone.append('')
                    if(len(telPhone)<len(storeName)):
                        telPhone.append('')
                    if(len(address)<len(storeName)):
                        address.append('')
                    if(len(email)<len(storeName)):
                        email.append('')
                    if(len(onSaleCarTotal)<len(storeName)):
                        onSaleCarTotal.append(0)
                    if(len(OnStoreCarTotal)<len(storeName)):
                        OnStoreCarTotal.append(0)                
                  
            
            except Exception as e:
                print("URL {} failed, retry in 5 seconds............\n".format(storeList['URL'][start_point2]))
                print(e)
                # log(storeList['URL'][start_point2])
                # 避免因短時間內重新執行而出錯
                sleep(randint(1,10))
                # 紀錄並返回錯誤的URL
                return start_point2
            
            finally:
                start_point2+=1                
                sleep(randint(1,5))
                print(str(j)+'done!')


def getAbcMemberData():
    sqlMC = "SELECT [ID] AS [MemberID], ISNULL([Cellphone],'') AS [Cellphone], CONCAT([CountryName], [DistrictName], ISNULL([Address],'')) AS [Address] FROM [ABCCarMemberCenter].[dbo].[Member] WHERE [Status] = 1"
    sqlM = "SELECT [MemberID], ISNULL([Mobile],'') AS [Mobile], ISNULL([Phone],'') AS [Phone], ISNULL([CarDealerMobile],'') AS [CarDealerMobile], ISNULL(CONCAT([CarDealerAreaCode], [CarDealerTelephone]),'') AS [TelPhone], ISNULL(CONCAT( [CarDealerCountryName], [CarDealerDistrictName], [CarDealerAddress]), ISNULL([Address],'') ) AS [Address] FROM [ABCCar].[dbo].[Member] WHERE [Status] = 1"

    ABCCarMemberCenterList = read_test_mssql(sqlMC, 'ABCCar')
    ABCCarMemberList = read_test_mssql(sqlM, 'ABCCar')

    return ABCCarMemberCenterList, ABCCarMemberList

def processData(df, ABCCarMemberCenterList, ABCCarMemberList):
    df['OnSaleCarTotal'] = df['OnSaleCarTotal'].astype(int)
    df['OnStoreCarTotal'] = df['OnStoreCarTotal'].astype(int)

    df.insert(df.shape[1], 'CreateDate', date.today())
    # 開始測量
    start = time.time()

    df['AbcMemberID'] = 0
    for i, v in df.iterrows():
        memberID = 0
        df['AbcMemberID'][i] = memberID

        #ABCCarMember
        #手機CarDealerMobile
        if (isinstance (v['CellPhone'],str)):
            v['CellPhone'] = v['CellPhone'].replace("-", "")
            for j, k in ABCCarMemberList.iterrows():
                # k['CarDealerMobile'] = k['CarDealerMobile'].str.replace("-", "")
                if v['CellPhone'] == k['CarDealerMobile']:
                    memberID = k['MemberID']
        if (memberID > 0):
            df['AbcMemberID'][i] = memberID
            continue

        #手機Mobile
        if (isinstance (v['CellPhone'],str)):
            # v['CellPhone'] = v['CellPhone'].str.replace("-", "")
            for j, k in ABCCarMemberList.iterrows():
                # k['Mobile'] = k['Mobile'].str.replace("-", "")
                if v['CellPhone'] == k['Mobile']:
                    memberID = k['MemberID']
        if (memberID > 0):
            df['AbcMemberID'][i] = memberID
            continue

        #手機Phone
        if (isinstance (v['CellPhone'],str)):
            # v['CellPhone'] = v['CellPhone'].str.replace("-", "")
            for j, k in ABCCarMemberList.iterrows():
                # k['Phone'] = k['Phone'].str.replace("-", "")
                if v['CellPhone'] == k['Phone']:
                    memberID = k['MemberID']
        if (memberID > 0):
            df['AbcMemberID'][i] = memberID
            continue

        #市話
        if (isinstance (v['TelPhone'],str)):
            # v['TelPhone'] = v['TelPhone'].str.replace("-", "")
            for j, k in ABCCarMemberList.iterrows():
                if v['TelPhone'] == k['TelPhone']:
                    memberID = k['MemberID']

        if (memberID > 0):
            df['AbcMemberID'][i] = memberID
            continue

        #地址
        if (isinstance (v['Address'],str)):
            for j, k in ABCCarMemberList.iterrows():
                if v['Address'] == k['Address']:
                    memberID = k['MemberID']        

        if (memberID > 0):
            df['AbcMemberID'][i] = memberID
            continue

        #2.比對 ABCCarMemberCenter
        #手機
        if (isinstance (v['CellPhone'],str)):
            # v['CellPhone'] = v['CellPhone'].str.replace("-", "")
            for j, k in ABCCarMemberCenterList.iterrows():
                if v['CellPhone'] == k['Cellphone']:
                    memberID = k['MemberID']        

        if (memberID > 0):
            df['AbcMemberID'][i] = memberID
            continue

        #地址
        if (isinstance (v['Address'],str)):
            for j, k in ABCCarMemberCenterList.iterrows():
                if v['Address'] == k['Address']:
                    memberID = k['MemberID']        

        if (memberID > 0):
            df['AbcMemberID'][i] = memberID
            continue

    # 結束測量
    end = time.time()

    # 輸出結果
    print("執行時間：%f 秒" % (end - start))

#將數據寫入至MSSQL SERVER
def send_to_db(data, tbl_name):
    num = 1000
    loop_no = round(len(data) / num) + 1
    for i in range(0, loop_no):
        table = data[i * num:i * num + num]
        to_sql_server('ABCCar', table, tbl_name, 'append')

def mergeProcess(df, ABCCarMemberCenterList, ABCCarMemberList):
    df['OnSaleCarTotal'] = df['OnSaleCarTotal'].astype(int)
    df['OnStoreCarTotal'] = df['OnStoreCarTotal'].astype(int)

    df.insert(df.shape[1], 'CreateDate', date.today())
    df['AbcMemberID'] = 0

    # 開始測量
    start = time.time()

    testDF = df
    testABC = ABCCarMemberList
    testABCM = ABCCarMemberCenterList
    testABC['CarDealerMobile'] = testABC['CarDealerMobile'].str.replace("-", "").drop_duplicates()
    testABC['Mobile'] = testABC['Mobile'].str.replace("-", "").drop_duplicates()
    testABC['Phone'] = testABC['Phone'].str.replace("-", "").drop_duplicates()
    testABC['TelPhone'] = testABC['TelPhone'].str.replace("-", "").drop_duplicates()
    testABCM['Cellphone'] = testABCM['Cellphone'].str.replace("-", "").drop_duplicates()
    testDF['CellPhone'] = testDF['CellPhone'].str.replace("-", "").drop_duplicates()
    testDF['TelPhone'] = testDF['TelPhone'].str.replace("-", "").drop_duplicates()

    aDF = testABC[['CarDealerMobile', 'MemberID']].rename(columns={'MemberID':'MemberID_A'}).replace('', np.nan, regex=True).dropna()
    bDF = testABC[['Mobile', 'MemberID']].rename(columns={'MemberID':'MemberID_B'}).replace('', np.nan, regex=True).dropna()
    cDF = testABC[['Phone', 'MemberID']].rename(columns={'MemberID':'MemberID_C'}).replace('', np.nan, regex=True).dropna()
    dDF = testABC[['TelPhone', 'MemberID']].rename(columns={'MemberID':'MemberID_D'}).replace('', np.nan, regex=True).dropna()
    eDF = testABC[['Address', 'MemberID']].rename(columns={'MemberID':'MemberID_E'}).replace('', np.nan, regex=True).dropna()
    fDF = testABCM[['Cellphone', 'MemberID']].rename(columns={'MemberID':'MemberID_F'}).replace('', np.nan, regex=True).dropna()
    gDF = testABCM[['Address', 'MemberID']].rename(columns={'MemberID':'MemberID_G'}).replace('', np.nan, regex=True).dropna()

    a = pd.merge(testDF, aDF, left_on=['CellPhone'], right_on=['CarDealerMobile'], how='left').drop_duplicates('Url')
    b = pd.merge(a, bDF, left_on=['CellPhone'], right_on=['Mobile'], how='left').drop_duplicates('Url')
    c = pd.merge(b, cDF, left_on=['CellPhone'], right_on=['Phone'], how='left').drop_duplicates('Url')
    d = pd.merge(c, dDF, left_on=['TelPhone'], right_on=['TelPhone'], how='left').drop_duplicates('Url')
    e = pd.merge(d, eDF, left_on=['Address'], right_on=['Address'], how='left').drop_duplicates('Url')
    f = pd.merge(e, fDF, left_on=['CellPhone'], right_on=['Cellphone'], how='left').drop_duplicates('Url')
    g = pd.merge(f, gDF, left_on=['Address'], right_on=['Address'], how='left').drop_duplicates('Url')

    for i, v in g.iterrows():
        g['AbcMemberID'][i] = 0
        
        # print(v['MemberID_A'], type(v['MemberID_A']), pd.isna(v['MemberID_A']))
        if(pd.isna(v['MemberID_A']) == False):
            g['AbcMemberID'][i] = v['MemberID_A']
            continue
            
        elif(pd.isna(v['MemberID_B']) == False):
            g['AbcMemberID'][i] = v['MemberID_B']
            continue
        elif(pd.isna(v['MemberID_C']) == False):
            g['AbcMemberID'][i] = v['MemberID_C']
            continue
        elif(pd.isna(v['MemberID_D']) == False):

            g['AbcMemberID'][i] = v['MemberID_D']
            continue
        elif(pd.isna(v['MemberID_E']) == False):

            g['AbcMemberID'][i] = v['MemberID_E']
            continue
        elif(pd.isna(v['MemberID_F']) == False):

            g['AbcMemberID'][i] = v['MemberID_F']
            continue
        elif(pd.isna(v['MemberID_G']) == False):
            g['AbcMemberID'][i] = v['MemberID_G']
            continue
        else:
            continue
    g = g[['StoreName', 'ContentPerson', 'CellPhone', 'TelPhone', 'Email', 'Address', 'OnSaleCarTotal', 'OnStoreCarTotal', 'Care', 'Best', 'Url', 'CreateDate', 'AbcMemberID']]
    # 結束測量
    end = time.time()

    # 輸出結果
    print("執行時間：%f 秒" % (end - start))

    return g

# main entrance
if __name__ == "__main__":
    print(now)
    script = 'crawler8891'
#===================Step 1: 先把所有車商頁的url存下來
    try:
        print('Part 1 start!')
        start_point = 0
        while (start_point is not None) and (start_point < totalPage):
            error_at = crawlStoreURL(start_point)
            start_point = error_at
        print('Part 1 done!')

    except Exception as e:
        msg = '{0}: PART 1: 抓取所有車上URL 錯誤! The error message : {1}'.format(script, e)
        send_error_message(script='{}'.format(script), message=msg)
        print(msg)
        sys.exit(1)
    
#===================Step 2: 再依序爬所有車商頁
    try:
        print('Part 2 start!')
        storeList = pd.read_excel(store_list)
        start_point2 = 0
        while (start_point2 is not None) and (start_point2 < totalNumber):
            error_at2 = crawlStore(storeList, start_point2)
            start_point2 = error_at2
        print('Part 2 done!')
    except Exception as e:
        msg = '{0}: PART 2: 依序爬所有車商頁 錯誤! The error message : {1}'.format(script, e)
        send_error_message(script='{}'.format(script), message=msg)
        print(msg)
        sys.exit(1)

#===================Step 3: 合併資料後再存檔
    try:
        data = []
        for i in range(len(storeName)):
            data.append([storeName[i], contentPerson[i], cellPhone[i], telPhone[i], email[i], address[i], onSaleCarTotal[i], OnStoreCarTotal[i], isCareStore[i], isBestStore[i], storeUrl[i]])

        df = pd.DataFrame(data, columns=['StoreName', 'ContentPerson', 'CellPhone', 'TelPhone', 'Email', 'Address', 'OnSaleCarTotal', 'OnStoreCarTotal', 'Care', 'Best', 'Url'])

        # 如果檔案不存在，建立檔案
        if  os.path.exists(file_out) == False:
            df.to_excel(file_out,index=False)

        # 如果檔案存在，append
        elif  os.path.exists(file_out) == True:
            df_old = pd.read_excel(file_out)
            df_combine = df_old.append(df)
            df_combine.to_excel(file_out,index=False)

        print('Part 3 done!')
    except Exception as e:
        msg = '{0}: PART 3: 合併資料後儲存 錯誤! The error message : {1}'.format(script, e)
        send_error_message(script='{}'.format(script), message=msg)
        print(msg)
        sys.exit(1)
#===================Step 4: 取得abc Member資料並合併
    try:
        print('Part 4 start!')
        # df = pd.read_excel(file_out)
        ABCCarMemberCenterList, ABCCarMemberList = getAbcMemberData()
        # FinalDF = processData(df, ABCCarMemberCenterList, ABCCarMemberList)
        FinalDF = mergeProcess(df, ABCCarMemberCenterList, ABCCarMemberList)
        FinalDF.to_excel('mergeOut.xlsx',index=False)
        print('Part 4 done!')
    except Exception as e:
        msg = '{0}: PART 4: 取得abc Member資料並合併 錯誤! The error message : {1}'.format(script, e)
        # send_error_message(script='{}'.format(script), message=msg)
        print(msg)
        sys.exit(1)

#===================Step 5: 存入資料庫
    try:
        # FinalDF = pd.read_excel('mergeOut.xlsx',index=False)
        send_to_db(FinalDF, '8891StoreList')
        print('ALL done!')
    except Exception as e:
        msg = '{0}: PART 5: 存入資料庫 錯誤! The error message : {1}'.format(script, e)
        send_error_message(script='{}'.format(script), message=msg)
        print(msg)
        sys.exit(1)
