#==============
# Path: /home/hadoop/jessielu/crawler
# Description: Crawl the Yahoo website with new car model infomation.
# Date: 2023/11/29
# Author: Sam 
#==============

import sys,re

env = "1"
# 測試機
EnvPath = "/home/jessie-ws/Dev/jupyterlab/daily_report/"
if(env=="1"):
    # 正式機
    EnvPath = "/home/hadoop/jessielu/"

sys.path.insert(0, EnvPath)

from Module.EmailFunc import send_error_message
import json
import pandas as pd
import pymssql
import time
from datetime import date
from selenium import webdriver
from selenium.webdriver.chrome.options import Options

from bs4 import BeautifulSoup

now = date.today()
baseYear = (now).strftime("%Y")

chrome_options = Options()
chrome_options.add_argument('--no-sandbox')
chrome_options.add_argument('--disable-dev-shm-usage')
chrome_options.add_argument('--headless')
ChromeDriverPath = "/home/jessie-ws/Dev/jupyterlab/daily_report/crawler/chromedriver"
if(env=="1"):
     ChromeDriverPath = "/home/hadoop/jessielu/crawler/chromedriver"
driver = webdriver.Chrome(ChromeDriverPath,chrome_options=chrome_options)


def crawlCarModel():
    brands = []
    series = []
    categorys = []
    
    
    brandUrl = f"""https://autos.yahoo.com.tw/new-cars/research/"""
    driver.get(brandUrl)
    #取得Brand
    for elm in driver.find_elements_by_xpath("//div[contains(@class,'research-main')]/div[contains(@class,'research-wrapper')]/div[contains(@class,'list')]/a[contains(@class,'gabtn')]"): 
        brandName = str(elm.get_attribute("textContent")).replace("'","").strip()
        brandUrl = str(elm.get_attribute("href")).strip()
        brandEng = re.sub('[\\u4e00-\\u9fff]+', '', brandName).strip()
        brandCht = brandName.replace(brandEng,"").strip()
        brandEng = brandEng.replace("'","")
        brands.append([brandName,brandUrl,brandEng,brandCht])

    #Brand+Year取得Series Url
    years = []
    for index in range(len(brands)):
        #if str(brands[index][2]).lower()==("Maserati").lower() :
            driver.get(brands[index][1])
            API_brandName = str(brands[index][2]).replace(" ","-").lower()
            for elm in driver.find_elements_by_xpath("//div[contains(@class,'make-main')]/div[contains(@class,'year')]"):  
                year = str(elm.get_attribute("data-year")).strip()
                #鎖定今年之後處理
                if int(year)>=int(baseYear):
                    yearUrl = f"""https://autos.yahoo.com.tw/ajax/api_car_make/{API_brandName}?year={year}"""
                    years.append([brands[index][2],brands[index][3],yearUrl])

    #取得Series
    for index in range(len(years)):
        #if index<5:
            driver.get(years[index][2])
            content = driver.find_element_by_tag_name('pre').text
            parsed_json = json.loads(content)
            for item in parsed_json:  
                soup = BeautifulSoup(item,"lxml")
                for link in soup.find_all('a', class_='gabtn'):
                    seriesUrl = str(link['href']).strip()
                    seriesName = str(link['title']).strip()                    
                    series.append([years[index][0],years[index][1],seriesName,seriesUrl])

    #取得Category
    for index in range(len(series)): 
        driver.get(series[index][3])
        model_title = ""
        model_Year = ""
        model_Name = ""     
        #取得Series Information
        for elm in driver.find_elements_by_xpath("//div[contains(@class,'model-wrapper')]/ul/li/span[1]/h1"):         
            model_title = str(elm.get_attribute("textContent")).replace("'","").strip()
            model_Year = re.findall(r'^\d+', model_title)[0]
            model_Name = model_title.replace(model_Year,"").strip()
            
        for elm in driver.find_elements_by_xpath("//div[contains(@class,'model-wrapper')]/ul/a[contains(@class,'gabtn')]"): 
            categoryUrl = ""
            categoryName = ""
            price = ""
            trim_id = ""
            categoryUrl = str(elm.get_attribute("href")).strip()
            categoryName = str(elm.get_attribute("title")).strip()
            
            for elm in driver.find_elements_by_xpath(f"""//div[contains(@class,'model-wrapper')]/ul/a[@href='{categoryUrl}']/li/span[2]"""):
                price = str(elm.get_attribute("textContent")).replace("'","").strip()
            
            for elm in driver.find_elements_by_xpath(f"""//div[contains(@class,'model-wrapper')]/ul/a[@href='{categoryUrl}']/li/div/form/input[contains(@name,'trim_id')]"""):
                trim_id = str(elm.get_attribute("value")).strip()
            
            #html格式不同重新取得price
            if price=="":
                for elm in driver.find_elements_by_xpath(f"""//div[contains(@class,'model-wrapper')]/ul/li[@class='model-sub']/a[@href='{categoryUrl}']/span[2]"""):
                    price = str(elm.get_attribute("textContent")).replace("'","").strip()

            #html格式不同重新取得trim_id
            if trim_id=="":
                for elm in driver.find_elements_by_xpath(f"""//div[contains(@class,'model-wrapper')]/ul/li[@class='model-sub']/div/a[@href='{categoryUrl}']/form/input[contains(@name,'trim_id')]"""):
                    trim_id = str(elm.get_attribute("value")).strip()
                    
            if categoryUrl!="":
                categoryUrl = categoryUrl + "/spec"
            
            categorys.append([series[index][0],series[index][1],model_title,model_Name,model_Year,series[index][3],categoryName,categoryUrl,price,trim_id])

    df = pd.DataFrame(categorys, columns=['BrandName','BrandNameCht','SeriesNameFull','SeriesName','SeriesYear','SeriesUrl','CategoryName','CategoryUrl','Price','TrimID'])

    return df

def send_to_db(df): 
    if(env=="1"):
        mssql_db = 'pdmasterdb.cpambwr2r6oj.ap-northeast-1.rds.amazonaws.com'
        mssql_user = 'administrator'
        mssql_password = '!abccar2020'
        mssql_db_name = 'ABCCar'
    else:
        mssql_db = 'pd-dev-stage.cpambwr2r6oj.ap-northeast-1.rds.amazonaws.com'
        mssql_user = 'administrator'
        mssql_password = '!abccar2020'
        mssql_db_name = 'ABCCar'
        
    # 連結 SQL                   
    connect_db = pymssql.connect(host=mssql_db, port=1433, user=mssql_user, password=mssql_password, database=mssql_db_name, charset='utf8', autocommit=True)
    start_time = time.time()
    for index in df.index: 
        brandName = df['BrandName'][index].replace("'","")
        brandNameCht = df['BrandNameCht'][index].replace("'","")
        seriesNameFull = df['SeriesNameFull'][index].replace("'","")
        seriesName = df['SeriesName'][index].replace("'","")
        seriesYear = df['SeriesYear'][index].replace("'","")
        seriesUrl = df['SeriesUrl'][index]
        categoryName = df['CategoryName'][index].replace("'","")
        categoryUrl = df['CategoryUrl'][index]
        price = df['Price'][index].replace("'","")
        trimID = df['TrimID'][index] 
        
        with connect_db.cursor() as cursor:
            # 執行 SQL 指令
            sql = f"""
    IF NOT EXISTS (SELECT 1 FROM [dbo].[WebYahooCarModel] WHERE [CategoryUrl] = '{categoryUrl}' and [SeriesYear]='{seriesYear}') 
    BEGIN 
        INSERT INTO [dbo].[WebYahooCarModel] ([BrandName],[BrandNameCht],[SeriesNameFull],[SeriesName],
        [SeriesYear],[SeriesUrl],[CategoryName],[CategoryUrl],[Price],[TrimID],[CreateDate]) 
        VALUES ('{brandName}','{brandNameCht}','{seriesNameFull}','{seriesName}',
        '{seriesYear}','{seriesUrl}','{categoryName}','{categoryUrl}','{price}','{trimID}',getdate())
    END 
    """
            cursor.execute(sql)
            #connect_db.commit()

    # 關閉 SQL 連線
    connect_db.close()
    print("--- %s seconds ---" % (time.time() - start_time))


if __name__ == "__main__":
    print(now)
    script = 'crawlerYahooCarModel'

    try:
        df = crawlCarModel()
        driver.close()
    except Exception as e:
        msg = '{0}: PART 1: 抓取新車 URL 錯誤! The error message : {1}'.format(script, e)
        send_error_message(script=script, message=msg)
        print(msg)
        driver.close()
        sys.exit(1)
        
    print('PART 1: Finished!')

    try:
        if len(df)>0:
            send_to_db(df)
    except Exception as e:
        msg = '{0}: PART 2: 寫入資料庫出現錯誤! The error message : {1}'.format(script, e)
        send_error_message(script=script, message=msg)
        print(msg)
        sys.exit(1)
        
    print('PART 2: Finished!')