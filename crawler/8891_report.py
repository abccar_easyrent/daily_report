#==============
# Path: /home/hadoop/ellen_crawl
# Description: Crawl the 8891 website with particular values.
# Date: 2022/05/24
# Author: Jessie 
#==============

import requests
from bs4 import BeautifulSoup
import re
from urllib.parse import urljoin
import json
import os
import sys
from openpyxl import load_workbook

#from selenium import webdriver
#from selenium.webdriver.chrome.options import Options
#from selenium.webdriver.support.ui import Select
#from selenium.webdriver.support.ui import WebDriverWait 
#from selenium.webdriver.support import expected_conditions as EC
#from selenium.webdriver.common.by import By
#from selenium.common.exceptions import TimeoutException
#from selenium.common.exceptions import StaleElementReferenceException
import time
import datetime
from dateutil.relativedelta import relativedelta  
import logging
logging.basicConfig(filename='/home/hadoop/jessielu/crawler/8891_report_log.txt', level=logging.INFO)

car=[]
#today = datetime.date.today().strftime('%Y'+'-'+'%m'+'-'+'%d')
today = (datetime.date.today() + relativedelta(days=1)).strftime("%Y/%m/%d")
print(today)
car.append(today)

logging.info('a1')

headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36'}
res = requests.get('https://www.8891.com.tw/', headers=headers)
soup = BeautifulSoup(res.text,"lxml")
#print(soup)

'''
for item in soup.find_all(attrs={'class':'item-count-v3'}):
    t=item.text.replace("今日新增","").replace("輛","")
    car.append(str(t))
    print(t)
'''
#2021/09/06移除
#每日新增車輛數
# if (soup.find(attrs={'class':'item-count-v3'})):
#     item=soup.find(attrs={'class':'item-count-v3'}).text
#     t=item.replace("今日新增","").replace("輛","")
#     car.append(str(t))
#     print(t)
# else:
#     t=" "
#     print(t)
#     car.append(t)
#######################################################################################   
#車商總數
logging.info('b1')
res1 = requests.get('https://www.8891.com.tw/findBuz-index.html?v=0', headers=headers)
soup1 = BeautifulSoup(res1.text,"lxml")
#print(soup)
for item1 in soup1.find_all(attrs={'class':'num'}):
    tt=item1.text
    ttt=int(tt.replace(",",""))
    car.append(ttt)
    print(str(ttt))
#######################################################################################  
#嚴選車商數
logging.info('c1')
res2 = requests.get('https://www.8891.com.tw/findBuz-index.html?v=2', headers=headers)
soup2 = BeautifulSoup(res2.text,"lxml")
#print(soup)
for item2 in soup2.find_all(attrs={'style':'color:#C00;'}):
    car.append(int(item2.text))
    print(str(item2.text))
####################################################################################### 
#旺店車商數
logging.info('d1')
res3 = requests.get('https://www.8891.com.tw/findBuz-index.html?v=1', headers=headers)
soup3 = BeautifulSoup(res3.text,"lxml")
#print(soup)
for item3 in soup3.find_all(attrs={'style':'color:#C00;'}):
    car.append(int(item3.text))
    print(str(item3.text))
logging.info('yay')
########################################################################################    
#車輛總數
#options = webdriver.ChromeOptions()
##logging.info('e1')
#options.add_argument('--headless')
#logging.info('e2')
#options.add_argument('--disable-gpu')  # Last I checked this was necessary.
#logging.info('e3')
#driver = webdriver.Chrome("/home/hadoop/jessielu/crawler/chromedriver", chrome_options = options) 
logging.info('e4')

headers = {'User-Agent': 'Mozilla/5.0 (Macintosh Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36'}

url = "https://auto.8891.com.tw/usedauto-newSearch.html"
resp = requests.get(url, headers=headers)
resp_dict = resp.json()
#print(resp_dict.get('data').get('total'))
car.append(int(resp_dict.get('data').get('total')))


#url = "https://auto.8891.com.tw/"
##driver.implicitly_wait(10)
#driver.get(url)
#time.sleep(10)
##for elm in driver.find_elements_by_class_name('_count_1fn78_402'): #new: _count_1fn78_402 / old: count--zy6sM
###    car.append(int(elm.text))
##    car.append(int(elm.find_element_by_tag_name('span').text))
###    print(str(elm.text))
##    print(int(elm.find_element_by_tag_name('span').text))

#for elm in driver.find_elements_by_xpath('//div[@id="subFilterContainer"]/div/div[3]/div[1]/p[1]/span'):
#    car.append(int(elm.text))
#    print(str(elm.text))

##########################################################################    
#嚴選車輛數
logging.info('selection')

url2 = "https://auto.8891.com.tw/usedauto-newSearch.html?page=1&yx=1"
resp = requests.get(url2, headers=headers)
resp_dict = resp.json()
#print(resp_dict.get('data').get('total'))
car.append(int(resp_dict.get('data').get('total')))

#url2 = "https://auto.8891.com.tw/?page=1&listTab=yx"
##driver.implicitly_wait(10)
#driver.get(url2)
#time.sleep(10)
##for elm4 in driver.find_elements_by_class_name('_count_1fn78_402'): #new: _count_1fn78_402 / old: count--zy6sM
###    car.append(int(elm.text))
##    car.append(int(elm4.find_element_by_tag_name('span').text))
###    print(str(elm.text))
##    print(int(elm4.find_element_by_tag_name('span').text))

#for elm4 in driver.find_elements_by_xpath('//div[@id="subFilterContainer"]/div/div[3]/div[1]/p[1]/span'):
#    car.append(int(elm4.text))
#    print(str(elm4.text))
    
########################################################################
#2021/09/06移除
#車身審核數
# url3 = "https://auto.8891.com.tw/?realprice=1&page=1"

# driver.get(url3)
# for elm2 in driver.find_elements_by_class_name('items-num-v3'):
#     car.append(str(elm2.text))
#     print(str(elm2.text))
# logging.info('selection end1')
########################################################################
#個人自售數

url4 = "https://auto.8891.com.tw/usedauto-newSearch.html?f[]=10&page=1"
resp = requests.get(url4, headers=headers)
resp_dict = resp.json()
#print(resp_dict.get('data').get('total'))
car.append(int(resp_dict.get('data').get('total')))

#url4 = "https://auto.8891.com.tw/?personal=1&page=1"
##driver.implicitly_wait(10)
#driver.get(url4)
#time.sleep(10)
##for elm3 in driver.find_elements_by_class_name('_count_1fn78_402'): #new: _count_1fn78_402 / old: count--zy6sM
###    car.append(int(elm3.find_element_by_tag_name('span').text))
##    print(int(elm3.find_element_by_tag_name('span').text))

#for elm3 in driver.find_elements_by_xpath('//div[@id="subFilterContainer"]/div/div[3]/div[1]/p[1]/span'):
#    car.append(int(elm3.text))
#    print(str(elm3.text))

logging.info('selection end2')

########################################################################
#前日成交數

url5 = "https://usedcar.8891.com.tw/api/sell/dealed"
resp = requests.get(url5, headers=headers)
resp_dict = resp.json()
#print(resp_dict.get('data').get('yesterday'))
car.append(int(resp_dict.get('data').get('yesterday')))

#url5 = "https://www.8891.com.tw/help-sell.html"
##driver.implicitly_wait(10)
#driver.get(url5)
#time.sleep(10)
##for elm3 in driver.find_elements_by_class_name('_banner-text-left_2gqkc_311'): # new: _banner-text-left_2gqkc_311 / old:banner-text-left--BVqm7
##    car.append(int((elm3.find_element_by_tag_name('span').text).split('輛')[0]))
##    print((elm3.find_element_by_tag_name('span').text).split('輛')[0])

#for elm3 in driver.find_elements_by_xpath('//div[@id="app"]/div[3]/div/p[1]/span'):
#    car.append(int((elm3.text).split('輛')[0]))
#    print((elm3.text).split('輛')[0])

logging.info('selection end2')

########################################################################
#今日新增

url6 = "https://www.8891.com.tw/api/v4/common/statistics?type[]=onSale&type[]=add&type[]=deal&type[]=online"
resp = requests.get(url6, headers=headers)
resp_dict = resp.json()
#print(resp_dict.get('data').get('add'))
car.append(int(resp_dict.get('data').get('add')))

#url6 = "https://auto.8891.com.tw/usedauto-publicity.html"
##driver.implicitly_wait(10)
#driver.get(url6)
#time.sleep(10)
##for elm in driver.find_elements_by_class_name('sale-count--2JQAD'):
##    car.append(int(elm.find_element_by_xpath('//*[@id="app"]/div[2]/div[1]/div/section/div[1]/div/span[2]').text))

#for elm in driver.find_elements_by_xpath('//div[@id="app"]/div[2]/div[1]/div/section/div[1]/div/span[2]'):
#    car.append(int(elm.text))
#    print(elm.text)
    
logging.info('selection end2')
    
print(car)
#2021/09/06 change file from csv to xlsx
path = '/home/hadoop/jessielu/crawler/'
# 將資料寫入工作表
wb = load_workbook(path + '8891_report.xlsx')

# 選擇第一個工作表
ws_main = wb.worksheets[0]
# 擷取需要append至Excel的值
print(car)
for i in car:
    if (i == 0):
        msg = 'main數據中有0值, 此程式停止執行!'
        print(msg)
        sys.exit(1)
ws_main.append(car)

# SAVE the worksheet
wb.save(path+'8891_report.xlsx')

logging.info('selection end3')

#driver.quit()
