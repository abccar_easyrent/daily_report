#==============
# Path: /home/hadoop/jessielu/crawler
# Description: Crawl the 8891 website with new car model infomation.
# Date: 2023/11/29
# Author: Sam 
#==============

import sys

env = "1"
# 測試機
EnvPath = "/home/jessie-ws/Dev/jupyterlab/daily_report/"
if(env=="1"):
    # 正式機
    EnvPath = "/home/hadoop/jessielu/"

sys.path.insert(0, EnvPath)

from Module.EmailFunc import send_error_message
import json,uuid
import pandas as pd
import pymssql
import time
from datetime import date
from selenium import webdriver
from selenium.webdriver.chrome.options import Options

now = date.today()

deviceid = str(uuid.uuid4())
version = "2.11.17"

chrome_options = Options()
chrome_options.add_argument('--no-sandbox')
chrome_options.add_argument('--disable-dev-shm-usage')
chrome_options.add_argument('--headless')
ChromeDriverPath = "/home/jessie-ws/Dev/jupyterlab/daily_report/crawler/chromedriver"
if(env=="1"):
     ChromeDriverPath = "/home/hadoop/jessielu/crawler/chromedriver"
driver = webdriver.Chrome(ChromeDriverPath,chrome_options=chrome_options)
                           

def crawlCarModel():
    # 依廠牌找出[廠牌ID]
    # 油車
    # https://c.8891.com.tw/api/v3/kinds/search/pc/brands?type=default&device-id=67a6ebd8-21ff-355c-42fc-4d9347fb9af0-a&api=2.11.17
    # EV電車
    # https://c.8891.com.tw/api/v3/kinds/search/pc/brands?type=default&device-id=67a6ebd8-21ff-355c-42fc-4d9347fb9af0-a&api=2.11.17

    # 依廠牌ID找出[車型ID]
    # https://c.8891.com.tw/api/v3/kinds/search?sort=hot&limit=20&page=1&p=[|5]&saleStop=[0|1]&b=[車型ID]&device-id=67a6ebd8-21ff-355c-42fc-4d9347fb9af0-a&api=2.11.17

    brandType = ['default','ev'] #default:油車 / ev:電車
    brands = []
    kinds = []
    for tp in brandType:
        brandUrl = f"""https://c.8891.com.tw/api/v3/kinds/search/pc/brands?type={tp}&device-id={deviceid}&api={version}"""
        #print(brandUrl)
        driver.get(brandUrl)
        time.sleep(1)
        content = driver.find_element_by_tag_name('pre').text
        parsed_json = json.loads(content)
       
        for item in parsed_json.get('data').get('list'):
            if tp=='ev':
                item['link'] = item['link'] + "/ev" 
            brands.append(item)

    saleType = ['0','1'] #0:在售 / 1:停
    for brand in brands:
        pagenum = ((int(brand['kindNum'])//10)+1)*10
        if (int(brand['kindNum'])<20):
            pagenum = 20
        for tp in saleType:
            kindUrl = f"""https://c.8891.com.tw/api/v3/kinds/search?sort=hot&limit={pagenum}&page=1&p=&saleStop={tp}&b={brand['id']}&device-id={deviceid}&api={version}"""
            #print(brand['kindNum'],kindUrl)

            driver.get(kindUrl)
            #time.sleep(1)
            #content = driver.page_source
            content = driver.find_element_by_tag_name('pre').text
            parsed_json = json.loads(content)
            for item in parsed_json.get('data').get('list'):
                kinds.append(item)

    df = pd.DataFrame(kinds)
    #print(df)
    return df

def send_to_db(df): 

    mssql_db = 'pd-dev-stage.cpambwr2r6oj.ap-northeast-1.rds.amazonaws.com'
    mssql_user = 'administrator'
    mssql_password = '!abccar2020'
    mssql_db_name = 'ABCCar'
    
    if(env=="1"):
        mssql_db = 'pdmasterdb.cpambwr2r6oj.ap-northeast-1.rds.amazonaws.com'
        mssql_user = 'administrator'
        mssql_password = '!abccar2020'
        mssql_db_name = 'ABCCar'
        
    # 連結 SQL                   
    connect_db = pymssql.connect(host=mssql_db, port=1433, user=mssql_user, password=mssql_password, database=mssql_db_name, charset='utf8', autocommit=True)
    start_time = time.time()
    for index in df.index: 
        brandId = df['brandId'][index] 
        kindId = df['kindId'][index] 
        brandName = df['brandName'][index].replace("'","")
        kindName = df['kindName'][index].replace("'","")
        price = df['price'][index].replace("'","")
        thumb = df['thumb'][index] 
        maxDiscountPrice = df['maxDiscountPrice'][index].replace("'","")
        inquiry = df['inquiry'][index]  
        modelNum = df['modelNum'][index] 
        saleStop = df['saleStop'][index] 
        usedCarLink = df['usedCarLink'][index].replace("'","")
        summaryLink = df['summaryLink'][index].replace("'","") 
        specLink = df['specLink'][index].replace("'","") 
        equipMovieId = df['equipMovieId'][index] 
        with connect_db.cursor() as cursor:
            # 執行 SQL 指令
            sql = f"""
    IF EXISTS (SELECT 1 FROM [dbo].[Web8891CarModel] WHERE [brandId] = '{brandId}' and [kindId] = '{kindId}' and [specLink] = '{specLink}') 
    BEGIN 
        UPDATE [dbo].[Web8891CarModel] 
            SET [price] = '{price}', [maxDiscountPrice] = '{maxDiscountPrice}', [inquiry] = '{inquiry}', 
                [modelNum] = '{modelNum}', [saleStop] = '{saleStop}', [usedCarLink] = '{usedCarLink}',
                [summaryLink] = '{summaryLink}', [equipMovieId] = '{equipMovieId}', [ModifyDate] = getdate() 
        WHERE [brandId] = '{brandId}' and [kindId] = '{kindId}' and [specLink] = '{specLink}'
    END 
    ELSE 
    BEGIN 
        INSERT INTO [dbo].[Web8891CarModel] ([brandId],[kindId],[brandName],[kindName],[price],[thumb],[maxDiscountPrice],
        [inquiry],[modelNum],[saleStop],[usedCarLink],[summaryLink],[specLink],[equipMovieId],[CreateDate]) 
        VALUES ('{brandId}','{kindId}','{brandName}','{kindName}','{price}','{thumb}','{maxDiscountPrice}',
        '{inquiry}','{modelNum}','{saleStop}','{usedCarLink}','{summaryLink}','{specLink}','{equipMovieId}',getdate())
    END 
    """
            cursor.execute(sql)
            #connect_db.commit()

    # 關閉 SQL 連線
    connect_db.close()
    print("--- %s seconds ---" % (time.time() - start_time))


if __name__ == "__main__":
    print(now)
    script = 'crawler8891CarModel'

    try:
        df = crawlCarModel()
        driver.close()
    except Exception as e:
        msg = '{0}: PART 1: 抓取新車 URL 錯誤! The error message : {1}'.format(script, e)
        send_error_message(script=script, message=msg)
        print(msg)
        driver.close()
        sys.exit(1)
        
    print('PART 1: Finished!')

    try:
        if len(df)>0:
            send_to_db(df)
    except Exception as e:
        msg = '{0}: PART 2: 寫入資料庫出現錯誤! The error message : {1}'.format(script, e)
        send_error_message(script=script, message=msg)
        print(msg)
        sys.exit(1)
        
    print('PART 2: Finished!')
