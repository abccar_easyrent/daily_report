#==============
# Path: /home/hadoop/jessielu/crawler
# Description: Crawl the Yahoo website with new car infomation.
# Date: 2023/12/13
# Author: Sam 
#==============

import sys,re

env = "1"
# 測試機
EnvPath = "/home/jessie-ws/Dev/jupyterlab/daily_report/"
if(env=="1"):
    # 正式機
    EnvPath = "/home/hadoop/jessielu/"

sys.path.insert(0, EnvPath)

from Module.EmailFunc import send_error_message
from Module.ConnFunc import read_mssql, read_dev_stage_mssql
import pandas as pd
import pymssql
import time
from datetime import date
from selenium import webdriver
from selenium.webdriver.chrome.options import Options

now = date.today()


chrome_options = Options()
chrome_options.add_argument('--no-sandbox')
chrome_options.add_argument('--disable-dev-shm-usage')
chrome_options.add_argument('--headless')
ChromeDriverPath = "/home/jessie-ws/Dev/jupyterlab/daily_report/crawler/chromedriver"
if(env=="1"):
     ChromeDriverPath = "/home/hadoop/jessielu/crawler/chromedriver"
driver = webdriver.Chrome(ChromeDriverPath,chrome_options=chrome_options)


def readData():
    CarModelList = f"""
    SELECT distinct CategoryUrl FROM abccar.dbo.WebYahooCarModel with (NOLOCK)
    WHERE CategoryUrl not in (SELECT CategoryUrl FROM ABCCar.dbo.WebYahooCarData with (NOLOCK) )
    """

    if(env=="1"):
        ListRtn = read_mssql(CarModelList, 'ABCCar')
    else:
        ListRtn = read_dev_stage_mssql(CarModelList, 'ABCCar')
    
    return ListRtn
    
def crawlCarData(dfUrl):

    for index in dfUrl.index:    
        CategoryUrl = dfUrl['CategoryUrl'][index]
        #print(CategoryUrl)
        driver.get(CategoryUrl)

        #麵包層取得廠牌,車型
        breadcrumb = []
        specs = []
        equips = []

        for elm in driver.find_elements_by_xpath("//div[contains(@class,'bread')]/span/a"): #//div[@id="col-0"]/dl/dd
            breadcrumb.append(str(elm.text).replace("'",""))
    
        brandName = (breadcrumb[-3]).strip()
        seriesName = (breadcrumb[-2]).strip()
        categoryName = (breadcrumb[-1]).strip()

        seriesYear = re.findall(r'^\d+', seriesName)[0]
        #print(seriesYear)
        if seriesYear.isnumeric():
            seriesName = seriesName.replace(seriesYear,"").strip()

        #print(breadcrumb[-3],(breadcrumb[-2]).replace(seriesYear,"").strip(),breadcrumb[-1])
        #print(brandName,seriesName,categoryName,seriesYear)

        displayName = ""
        for elm in driver.find_elements_by_xpath("//div[contains(@class,'trim-main')]/h1[contains(@class,'title')]"):
            displayName = str(elm.text).strip()
        #print(displayName)

        displayPrice = ""
        for elm in driver.find_elements_by_xpath("//div[contains(@class,'trim-main')]/h3[contains(@class,'price')]/span[1]"):
            displayPrice = str(elm.text).strip()
        #print(displayPrice)

        for elm in driver.find_elements_by_xpath("//div[contains(@class,'trim-spec')]/div/form/input[contains(@name,'trim_id')]"): 
            trim_id = str(elm.get_attribute("value"))
            #print(str(elm.get_attribute("value")))
        #print(trim_id)
        #print("=====left======") 
        for elm in driver.find_elements_by_xpath("//div[contains(@class,'trim-spec-detail')]/div[1]/input"): #//div[@id="col-0"]/dl/dd 

            keyType = "spec"
            keyName = str(elm.get_attribute("id"))
            Name = ""
            for elm in driver.find_elements_by_xpath(f"""//div[contains(@class,'trim-spec-detail')]/div[1]/label[contains(@for,'{keyName}')]"""):
                #print(str(elm.get_attribute("textContent")).strip())
                Name = str(elm.get_attribute("textContent")).strip()
            specs.append([keyType,keyName,Name])

        #print("=====right======")    
        for elm in driver.find_elements_by_xpath("//div[contains(@class,'trim-spec-detail')]/div[2]/input"): #//div[@id="col-0"]/dl/dd 
            keyType = "equip"
            keyName = str(elm.get_attribute("id"))
            Name = ""
            for elm in driver.find_elements_by_xpath(f"""//div[contains(@class,'trim-spec-detail')]/div[2]/label[contains(@for,'{keyName}')]"""):
                Name = str(elm.get_attribute("textContent")).strip()
            equips.append([keyType,keyName,Name])

        dfSpecs = pd.DataFrame(specs, columns=['grouptype','groupname','group'])
        dfSpecs['groupid'] = dfSpecs.index+1

        dfEquips = pd.DataFrame(equips, columns=['grouptype','groupname','group'])
        dfEquips['groupid'] = dfEquips.index+1

        df = dfSpecs.append(dfEquips,ignore_index=True)
        #print(df)

        specitem = []
        for rowidx, row in dfSpecs.iterrows():
            seq = 0
            for elm in driver.find_elements_by_xpath(f"""//div[contains(@class,'trim-spec-detail')]/div[1]/ul[{row['groupid']}]/li"""):
                seq = seq+1
                specname = ""
                specval = ""
                for elm in driver.find_elements_by_xpath(f"""//div[contains(@class,'trim-spec-detail')]/div[1]/ul[{row['groupid']}]/li[{seq}]/span[1]"""):
                    specname = str(elm.get_attribute("textContent")).strip()
                for elm in driver.find_elements_by_xpath(f"""//div[contains(@class,'trim-spec-detail')]/div[1]/ul[{row['groupid']}]/li[{seq}]/span[2]"""):
                    specval = str(elm.get_attribute("textContent")).strip()
                specitem.append([row['group'],seq,specname,specval])

        dfSpecsItem = pd.DataFrame(specitem, columns=['group','seq','specname','specval'])     
        #print(dfSpecsItem)

        equipitem = []
        for rowidx, row in dfEquips.iterrows():
            seq = 0
            for elm in driver.find_elements_by_xpath(f"""//div[contains(@class,'trim-spec-detail')]/div[2]/ul[{row['groupid']}]/li"""):
                seq = seq+1
                equipname = ""
                equipval = ""
                for elm in driver.find_elements_by_xpath(f"""//div[contains(@class,'trim-spec-detail')]/div[2]/ul[{row['groupid']}]/li[{seq}]/span[1]"""):
                    equipname = str(elm.get_attribute("textContent")).strip()
                for elm in driver.find_elements_by_xpath(f"""//div[contains(@class,'trim-spec-detail')]/div[2]/ul[{row['groupid']}]/li[{seq}]/span[2]"""):
                    equipval = str(elm.get_attribute("class")).strip()
                    if equipval=="":
                        equipval = "●"
                    else:
                        equipval = "○"
                equipitem.append([row['group'],seq,equipname,equipval])

        dfEquipsItem = pd.DataFrame(equipitem, columns=['group','seq','specname','specval'])     
        #print(dfEquipsItem)
        res = dfSpecsItem.append(dfEquipsItem,ignore_index=True)
        res = pd.merge(df,res, on=['group'],how='left')
        res['trim_id'] = trim_id
        #print(res)
        
        
        #Master 
        gasType = (res.loc[(res['specname'] == "動力型式")]["specval"]).to_string(index=False).strip()
        if gasType=="電動":
            engineType = ""
            engineDisplacement=""
            gearType=""
        else:
            engineType = (res.loc[(res['specname'] == "引擎型式")]["specval"]).to_string(index=False).strip()
            engineDisplacement = (res.loc[(res['specname'] == "排氣量")]["specval"]).to_string(index=False).strip()
            gearType = (res.loc[(res['specname'] == "變速系統")]["specval"]).to_string(index=False).strip()

        transmission = (res.loc[(res['specname'] == "驅動型式")]["specval"]).to_string(index=False).strip()
        brakeType = (res.loc[(res['specname'] == "煞車型式")]["specval"]).to_string(index=False).strip()
        bodyType = (res.loc[(res['specname'] == "車身型式")]["specval"]).to_string(index=False).strip()

        if (len(res.loc[(res['specname'] == "車門數")]))>0:
            door = (res.loc[(res['specname'] == "車門數")]["specval"]).to_string(index=False).strip()
            #door = re.findall(r'^\d+', door)[0]
            door = door.strip().replace("門","")
        else:
            door = ""

        if (len(res.loc[(res['specname'] == "座位數")]))>0:
            passenger = (res.loc[(res['specname'] == "座位數")]["specval"]).to_string(index=False).strip()
            #passenger = re.findall(r'^\d+', passenger)[0]
            passenger = passenger.strip().replace("人座","")
        else:
            passenger = ""
        
        CarData = []
        CarData.append([CategoryUrl,brandName,seriesName,categoryName,seriesYear,displayName,displayPrice,
                        gasType,engineType,engineDisplacement,gearType,transmission,brakeType,bodyType,
                        door,passenger,trim_id])
        
        master = pd.DataFrame(CarData, columns=['CategoryUrl','BrandName','SeriesName','CategoryName',
                                                'ModelYear','Title','Price','GasType','EngineType',
                                                'EngineDisplacement','GearType','Transmission','BrakeType',
                                                'BodyType','Door','Passenger','TrimID'])     

        send_to_db(master,res)


def send_to_db(df,dfSpec): 
    if(env=="1"):
        mssql_db = 'pdmasterdb.cpambwr2r6oj.ap-northeast-1.rds.amazonaws.com'
        mssql_user = 'administrator'
        mssql_password = '!abccar2020'
        mssql_db_name = 'ABCCar'
    else:
        mssql_db = 'pd-dev-stage.cpambwr2r6oj.ap-northeast-1.rds.amazonaws.com'
        mssql_user = 'administrator'
        mssql_password = '!abccar2020'
        mssql_db_name = 'ABCCar'
  

    # 連結 SQL                   
    connect_db = pymssql.connect(host=mssql_db, port=1433, user=mssql_user, password=mssql_password, database=mssql_db_name, charset='utf8', autocommit=True)
    for index in df.index: 
        categoryUrl = df['CategoryUrl'][index]
        brandName = df['BrandName'][index].replace("'","")
        seriesName = df['SeriesName'][index].replace("'","")
        categoryName = df['CategoryName'][index].replace("'","")
        modelYear = df['ModelYear'][index].replace("'","")
        title = df['Title'][index].replace("'","")
        price = df['Price'][index].replace("'","")
        gasType = df['GasType'][index].replace("'","")
        engineType = df['EngineType'][index].replace("'","")
        engineDisplacement = df['EngineDisplacement'][index].replace("'","")
        transmission = df['Transmission'][index].replace("'","")
        gearType = df['GearType'][index].replace("'","")
        brakeType = df['BrakeType'][index].replace("'","")
        bodyType = df['BodyType'][index].replace("'","")
        door = df['Door'][index].replace("'","")
        passenger = df['Passenger'][index].replace("'","")
        trimID = df['TrimID'][index] 
        
        with connect_db.cursor() as cursor:
            # 執行 SQL 指令
            sql = f"""
    IF NOT EXISTS (SELECT 1 FROM [dbo].[WebYahooCarData] WHERE [CategoryUrl] = '{categoryUrl}') 
    BEGIN 
        INSERT INTO [dbo].[WebYahooCarData] ([BrandName],[SeriesName],[CategoryName],[CategoryUrl],
        [ModelYear],[Title],[Price],[GasType],[EngineType],[EngineDisplacement],[Transmission],
        [GearType],[BrakeType],[BodyType],[Door],[Passenger],[TrimID],[CreateDate]) 
        VALUES ('{brandName}','{seriesName}','{categoryName}','{categoryUrl}',
        '{modelYear}','{title}','{price}','{gasType}','{engineType}','{engineDisplacement}',
        '{transmission}','{gearType}','{brakeType}','{bodyType}','{door}','{passenger}','{trimID}',getdate());
        
        SELECT @@IDENTITY AS Id;
    END 
    """
            cursor.execute(sql)

            try:
                row = cursor.fetchone()

                if row:
                    #record_id = cursor.fetchone()[0]
                    #print("===============")
                    #print(record_id)
                    #print("===============")
                    record_id = row[0]
                    dfSpec = dfSpec.fillna("")
                    for seq in dfSpec.index: 
                        groupClass = dfSpec['grouptype'][seq].replace("'","")
                        groupType = dfSpec['groupname'][seq].replace("'","")
                        groupName = dfSpec['group'][seq].replace("'","")
                        specName = dfSpec['specname'][seq].replace("'","")
                        specValue = dfSpec['specval'][seq].replace("'","")
                        trimID = dfSpec['trim_id'][seq] 
                
                        sql = f"""
                            INSERT INTO [dbo].[WebYahooCarSpec] ([ID],[GroupClass],[GroupType],[GroupName],
                            [SpecName],[SpecValue],[TrimID],[CreateDate]) 
                            VALUES ('{record_id}','{groupClass}','{groupType}','{groupName}','{specName}',
                            '{specValue}','{trimID}',getdate());      
                        """
                        
                        cursor.execute(sql)
            except Exception as e:
                print("=== Repeat CategoryUrl:" + categoryUrl + " exist.===")

    # 關閉 SQL 連線
    connect_db.close()
    


if __name__ == "__main__":
    print(now)
    
    script = 'crawlerYahooCarData'
    try:
        dfUrl = readData()
    except Exception as e:
        msg = '{0}: PART 1: 取得 Yahoo URL 錯誤! The error message : {1}'.format(script, e)
        send_error_message(script=script, message=msg)
        print(msg)
        sys.exit(1)
        
    print('PART 1: Finished!')


    try:
        start_time = time.time()
        crawlCarData(dfUrl)
        print("--- %s seconds ---" % (time.time() - start_time))
    except Exception as e:
        msg = '{0}: PART 2: 抓取 Yahoo 新車資料錯誤! The error message : {1}'.format(script, e)
        send_error_message(script=script, message=msg)
        print(msg)
    finally:
        driver.close()
        
    print('PART 2: Finished!')
