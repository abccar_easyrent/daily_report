import os, time, socket
import pandas as pd
import re
import time
from bs4 import BeautifulSoup
from lxml import etree
import requests
import math
from datetime import datetime, timedelta, date
from random import randint
from time import sleep
import sys
import numpy as np
import json

sys.path.insert(0, '/home/jessie/MyNotebook/crawler/')

from Module.ConnFunc import read_mssql, read_test_mssql, to_mysql, read_mysql, to_test_sql_server, to_sql_server
from Module.EmailFunc import send_error_message
from dateutil.relativedelta import relativedelta

#-------------------Import end
#===========Author: Jessie Lu
#===========Date: 2021-10-01

headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36'}
now = date.today()
# now = (datetime.today() - relativedelta(days=1)).strftime('%Y-%m-%d')
car_list = '8891_car_' + str(now) + '.xlsx'
file_out='8891_carALL_' + str(now) + '.xlsx'
totalPage = 8

name = [] #上方名字
tel = [] #上方電話
carPos = [] #上方車子所在地
nameDown = [] #下方名字
URL = []

def crawlCarURL(start_point):
    # 開始測量
    start = time.time()
    for i in range(start_point, 9):
        try:
            print(i)
            hrefs = []
            URL = 'https://auto.8891.com.tw/usedauto-newSearch.html?page=' + str(i) +'&limit=5000'
            s = requests.session()
            s.keep_alive = False
            resp = s.get(url= URL, headers=headers)        
            soup = BeautifulSoup(resp.text, features= "html.parser")
            jsonData = json.loads(soup.text)
            nodes = jsonData['data']['data']
            for j in nodes:
                hrefs.append(j['id']) #物件id

            df = pd.DataFrame(hrefs, columns=['URL'])

            # 如果檔案不存在，建立檔案
            if  os.path.exists(car_list) == False:
                df.to_excel(car_list,index=False)

            # 如果檔案存在，append
            elif  os.path.exists(car_list) == True:
                df_old = pd.read_excel(car_list)
                df_combine = df_old.append(df)
                df_combine.to_excel(car_list,index=False)

        except Exception as e:
            print("URL {} failed, retry in 5 seconds............\n".format('https://auto.8891.com.tw/usedauto-newSearch.html?page=' + str(i) +'&limit=5000'))
            print(e)
            # log(storeList['URL'][start_point2])
            # 避免因短時間內重新執行而出錯
            sleep(randint(1,10))
            # 紀錄並返回錯誤的URL
            return start_point
        
        finally:
            start_point+=1                
            sleep(randint(1,5))

    # 結束測量
    end = time.time()

    # 輸出結果
    print("執行時間：%f 秒" % (end - start))
        
def crawlALLCar(carList, start_point2):
    # 開始測量
    start = time.time()

    for idx in range(start_point2, len(carList['URL'])):
        try:
            print(carList['URL'][idx])
            s = requests.session()
            s.keep_alive = False
            resp = s.get(url= 'https://auto.8891.com.tw/usedauto-infos-' + str(carList['URL'][idx]) + '.html', headers=headers)
            soup = BeautifulSoup(resp.text, features= "lxml")

            #Check where the store page is
            if(soup.select("div.personage-car-message")):
                print('in')
                selector = etree.HTML(str(soup))
                if(selector.xpath("//div/span[@class='name']")):
                    name.append(selector.xpath("//div/span[@class='name']")[0].text if len(selector.xpath("//div/span[@class='name']")[0].text)>0 else 'No data')
                    tel.append(selector.xpath("//div/span[@class='tel']")[0].text if len(selector.xpath("//div/span[@class='tel']")[0].text)>0 else 'No data')
                    carPos.append(selector.xpath("//div/a[@class='car-pos']/span")[0].text if len(selector.xpath("//div/a[@class='car-pos']/span")[0].text)>0 else 'No data')
                    URL.append('https://auto.8891.com.tw/usedauto-infos-' + carList['URL'][idx] + '.html')

                    sleep(randint(1,2))

                    s = requests.session()
                    s.keep_alive = False
                    resp = s.get(url= 'https://www.8891.com.tw/api/v4/itemReport/index?itemId=' + str(carList['URL'][idx]), headers=headers)
                    soup = BeautifulSoup(resp.text, features= "html.parser")

                    jsonData = json.loads(soup.text)
                    nameDown.append(jsonData['data']['name'] if len(jsonData['data']['name'])>0 else 'No data')
                else:
                    pass
            else:
                pass

        except Exception as e:
            print("URL {} failed, retry in 5 seconds............\n".format(carList['URL'][idx]))
            print(e)
            # log(storeList['URL'][start_point2])
            # 避免因短時間內重新執行而出錯
            sleep(randint(1,10))
            # 紀錄並返回錯誤的URL
            return start_point2
        
        finally:
            start_point2+=1                
            sleep(randint(1,5))
            print(str(idx), str(carList['URL'][idx])+'done!')

    # 結束測量
    end = time.time()

    # 輸出結果
    print("執行時間：%f 秒" % (end - start))

# main entrance
if __name__ == "__main__":
    print(now)
    script = 'crawler8891AllCar'
#===================Step 1: 先把所有車商頁的url存下來
    try:
        print('Part 1 start!')
        start_point = 0
        while (start_point is not None) and (start_point < totalPage):
            error_at = crawlCarURL(start_point)
            start_point = error_at
        print('Part 1 done!')

    except Exception as e:
        msg = '{0}: PART 1: 抓取所有車 URL 錯誤! The error message : {1}'.format(script, e)
        send_error_message(script='{}'.format(script), message=msg)
        print(msg)
        sys.exit(1)
    
#===================Step 2: 再依序爬所有車商頁
    try:
        print('Part 2 start!')
        carList = pd.read_excel(car_list)
        start_point2 = 0
        while (start_point2 is not None) and (start_point2 < len(carList)):
            error_at2 = crawlALLCar(carList, start_point2)
            start_point2 = error_at2
        print('Part 2 done!')
    except Exception as e:
        msg = '{0}: PART 2: 依序爬所有車商頁 錯誤! The error message : {1}'.format(script, e)
        send_error_message(script='{}'.format(script), message=msg)
        print(msg)
        sys.exit(1)

#===================Step 3: 合併資料後再存檔
    try:
        data = []
        
        for i in range(len(name)):
            data.append([name[i], tel[i], carPos[i], nameDown[i], URL[i]])
        
        df = pd.DataFrame(data, columns=['Name', 'TelPhone', 'Address', 'Name_Down', 'URL'])

        # 如果檔案不存在，建立檔案
        if  os.path.exists(file_out) == False:
            df.to_excel(file_out,index=False)

        # 如果檔案存在，append
        elif  os.path.exists(file_out) == True:
            df_old = pd.read_excel(file_out)
            df_combine = df_old.append(df)
            df_combine.to_excel(file_out,index=False)

        print('Part 3 done!')
    except Exception as e:
        msg = '{0}: PART 3: 合併資料後儲存 錯誤! The error message : {1}'.format(script, e)
        send_error_message(script='{}'.format(script), message=msg)
        print(msg)
        sys.exit(1)
