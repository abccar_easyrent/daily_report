#==============
# Path: /home/hadoop/jessielu/crawler
# Description: Crawl the 8891 website with new car infomation.
# Date: 2023/12/28
# Author: Sam 
#==============

import sys,re

env = "1"
# 測試機
EnvPath = "/home/jessie-ws/Dev/jupyterlab/daily_report/"
if(env=="1"):
    # 正式機
    EnvPath = "/home/hadoop/jessielu/"

sys.path.insert(0, EnvPath)

from Module.EmailFunc import send_error_message
from Module.ConnFunc import read_mssql, read_dev_stage_mssql
import pandas as pd
import pymssql
import time
from datetime import date
from selenium import webdriver
from selenium.webdriver.chrome.options import Options

now = date.today()

chrome_options = Options()
chrome_options.add_argument('--no-sandbox')
chrome_options.add_argument('--disable-dev-shm-usage')
chrome_options.add_argument('--headless')
ChromeDriverPath = "/home/jessie-ws/Dev/jupyterlab/daily_report/crawler/chromedriver"
if(env=="1"):
     ChromeDriverPath = "/home/hadoop/jessielu/crawler/chromedriver"
driver = webdriver.Chrome(ChromeDriverPath,chrome_options=chrome_options)

def readData():
    CarModelList = f"""SELECT distinct specLink FROM abccar.dbo.Web8891CarModel with (NOLOCK)"""

    if(env=="1"):
        ListRtn = read_mssql(CarModelList, 'ABCCar')
    else:
        ListRtn = read_dev_stage_mssql(CarModelList, 'ABCCar')

    return ListRtn

def crawlCarData(dfUrl):

    for index in dfUrl.index:    
        CategoryUrl = dfUrl['specLink'][index]
        try:
            #print(CategoryUrl)
            driver.get(CategoryUrl)

            driver.implicitly_wait(10)

            #取得mids
            mids = []
            for elm in driver.find_elements_by_xpath("//a[contains(@class,'summary-model-vs')]"): #//div[@id="col-0"]/dl/dd
                #rint(str(elm.get_attribute('data-myid')).strip())
                mids.append([dfUrl['specLink'][index],str(elm.get_attribute('data-myid')).strip()])

            df = pd.DataFrame(mids, columns=['CategoryUrl','MyID'])

            if len(df)>0:
                send_to_db(df)
        except Exception as e:
            print("Unable to connection:" + CategoryUrl)

def send_to_db(df):
    if(env=="1"):
        mssql_db = 'pdmasterdb.cpambwr2r6oj.ap-northeast-1.rds.amazonaws.com'
        mssql_user = 'administrator'
        mssql_password = '!abccar2020'
        mssql_db_name = 'ABCCar'
    else:
        mssql_db = 'pd-dev-stage.cpambwr2r6oj.ap-northeast-1.rds.amazonaws.com'
        mssql_user = 'administrator'
        mssql_password = '!abccar2020'
        mssql_db_name = 'ABCCar'
  

    # 連結 SQL                   
    connect_db = pymssql.connect(host=mssql_db, port=1433, user=mssql_user, password=mssql_password, database=mssql_db_name, charset='utf8', autocommit=True)
    for index in df.index: 
        categoryUrl = df['CategoryUrl'][index]
        myID = df['MyID'][index] 
        
        with connect_db.cursor() as cursor:
            # 執行 SQL 指令
            sql = f"""
    IF NOT EXISTS (SELECT 1 FROM [dbo].[Web8891CarModelMid] WHERE [CategoryUrl] = '{categoryUrl}' AND [MyID] = '{myID}') 
    BEGIN 
        INSERT INTO [dbo].[Web8891CarModelMid] ([CategoryUrl],[MyID],[CreateDate]) 
        VALUES ('{categoryUrl}','{myID}',getdate());
    END 
    """
            cursor.execute(sql)


    # 關閉 SQL 連線
    connect_db.close()


if __name__ == "__main__":
    print(now)
    
    script = 'crawler8891CarModelMid'
    try:
        dfUrl = readData()
    except Exception as e:
        msg = '{0}: PART 1: 取得 8891 URL 錯誤! The error message : {1}'.format(script, e)
        send_error_message(script=script, message=msg)
        print(msg)
        sys.exit(1)
        
    print('PART 1: Finished!')


    try:
        start_time = time.time()       
        crawlCarData(dfUrl)
        print("--- %s seconds ---" % (time.time() - start_time))
    except Exception as e:
        msg = '{0}: PART 2: 抓取 8891 新車資料錯誤! The error message : {1}'.format(script, e)
        send_error_message(script=script, message=msg)
        print(msg)
    finally:
        driver.close()
        
    print('PART 2: Finished!')
