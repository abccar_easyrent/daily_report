import smtplib
import mimetypes
from datetime import datetime, timedelta
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from email.mime.image import MIMEImage
from email.utils import COMMASPACE

# global variables
b1day = (datetime.today() + timedelta(1)).strftime('%Y-%m-%d')
group_receiver_ls = ['JONATHANKO@hotaimotor.com.tw', 'MASA0204@hotaimotor.com.tw','WILLIAM@hotaimotor.com.tw','STEVEWU@hotaimotor.com.tw','MANDYLIN@hotaimotor.com.tw','HUNG57@hotaimotor.com.tw','31879@hotaimotor.com.tw','JOANNE373@hotaimotor.com.tw','EAGLE@hotaimotor.com.tw','maggiechen@hotaimotor.com.tw','SAM903@hotaimotor.com.tw','HENNY@hotaimotor.com.tw','LIN32093@hotaimotor.com.tw','CHERRY5588@mail.hotaimotor.com.tw','TWEETY@hotaimotor.com.tw','LSTTR12@hotaimotor.com.tw','MIA0111@hotaimotor.com.tw', 'AARON711018@hotaimotor.com.tw', 'SS0210@hotaimotor.com.tw', 'MANJAK06@hotaimotor.com.tw', 'QIAN18@hotaileasing.com.tw', 'AARONWU@hotaileasing.com.tw','ANGELLIN@hotaileasing.com.tw', 'DENNYH@hotaileasing.com.tw', 'EAGLE@hotaileasing.com.tw', '0900EMMA@hotaileasing.com.tw', 'YULI1549@hotaileasing.com.tw', 'ROMANCHEN@hotaileasing.com.tw']
# group_receiver_ls = ['WILLIAM@hotaimotor.com.tw', 'STEVEWU@hotaimotor.com.tw', 'MANDYLIN@hotaimotor.com.tw', 'MARKYAO@hotaimotor.com.tw','FARA@hotaimotor.com.tw']
private_receiver_ls = ["ROMANCHEN@hotaileasing.com.tw"]
# cc = ["YSLIU@hotaimotor.com.tw", "FRED@hotaimotor.com.tw", "CARLO@hotaimotor.com.tw", "YIHUA23@hotaimotor.com.tw"]
cc = ["MARKYAO@hotaimotor.com.tw"]
sender_ls = "abccar.easyrent@gmail.com"
fileToSend = "/home/hadoop/jessielu/crawler/8891_report.xlsx"
receiver = 'g'

def smtp_connect(sender, receivers_list, msg):
    try:
        # Gmail Sign In
        gmail_sender = sender_ls
        gmail_passwd = 'sgawhwivypmmvqrk'  # 要至官網申請應用程式密碼

        smtp = smtplib.SMTP('smtp.gmail.com:587')
        smtp.ehlo()
        smtp.starttls()
        smtp.login(gmail_sender, gmail_passwd)
        smtp.sendmail(sender, receivers_list, msg.as_string())
        smtp.quit()
        print('send successfully')
    except Exception as e:
        print('Not sent email because %s' % e)

sender = sender_ls
msg = MIMEMultipart()
msg['From'] = sender

if receiver == 'g':
    receivers_list = group_receiver_ls
#    msg['Cc'] = COMMASPACE.join(cc)

elif receiver == 'j':
    receivers_list = private_receiver_ls
    # msg['Cc'] = COMMASPACE.join(receivers_list)

msg['To'] = COMMASPACE.join(receivers_list)
msg['Subject'] = '8891 日報_{0}'.format(b1day)

# string to store the body of the mail
body = """
8891 日報。
請參閱，謝謝！
"""

# attach the body with the msg instance
msg.attach(MIMEText(body, 'plain'))

# instance of MIMEApplication and named as p
filename = "8891_report.xlsx"
path = '/home/hadoop/jessielu/crawler/'
p = MIMEApplication(open(path+"8891_report.xlsx", "rb").read())
p.add_header('Content-Disposition', "attachment", filename = filename)

# attach the instance 'p' and 'msgImage' to instance 'msg'
msg.attach(p)

# log in smtp and send out email
smtp_connect(sender, receivers_list, msg)