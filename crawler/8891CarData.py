#==============
# Path: /home/hadoop/jessielu/crawler
# Description: Crawl the 8891 website with new car infomation.
# Date: 2023/12/28
# Author: Sam 
#==============

import sys,re

env = "1"
# 測試機
EnvPath = "/home/jessie-ws/Dev/jupyterlab/daily_report/"
if(env=="1"):
    # 正式機
    EnvPath = "/home/hadoop/jessielu/"

sys.path.insert(0, EnvPath)

from Module.EmailFunc import send_error_message
from Module.ConnFunc import read_mssql, read_dev_stage_mssql
import pandas as pd
import pymssql
import time
from datetime import date
from selenium import webdriver
from selenium.webdriver.chrome.options import Options

now = date.today()

chrome_options = Options()
chrome_options.add_argument('--no-sandbox')
chrome_options.add_argument('--disable-dev-shm-usage')
chrome_options.add_argument('--headless')
ChromeDriverPath = "/home/jessie-ws/Dev/jupyterlab/daily_report/crawler/chromedriver"
if(env=="1"):
     ChromeDriverPath = "/home/hadoop/jessielu/crawler/chromedriver"
driver = webdriver.Chrome(ChromeDriverPath,chrome_options=chrome_options)

def readData():
    #CarModelList = f"""SELECT distinct specLink FROM abccar.dbo.Web8891CarModel with (NOLOCK)"""
    CarModelList = f"""SELECT distinct CategoryUrl as specLink FROM [Web8891CarModelMid] w with(nolock) 
                       WHERE w.MyID not in (SELECT k.MyID FROM Web8891CarData k with(nolock) WHERE k.CategoryUrl=w.CategoryUrl)"""

    CarList = f"""SELECT distinct CategoryUrl,MyID FROM Web8891CarData with (NOLOCK)"""
    if(env=="1"):
        ListRtn = read_mssql(CarModelList, 'ABCCar')
        CarListRtn = read_mssql(CarList, 'ABCCar')
    else:
        ListRtn = read_dev_stage_mssql(CarModelList, 'ABCCar')
        CarListRtn = read_dev_stage_mssql(CarList, 'ABCCar')

    return ListRtn, CarListRtn

def crawlCarData(dfUrl, dfCar):

    for index in dfUrl.index:    
        CategoryUrl = dfUrl['specLink'][index]
        try:
            #print(CategoryUrl)
            driver.get(CategoryUrl)

            driver.implicitly_wait(10)

            #麵包層取得廠牌,車型
            breadcrumb = []
            colIds = []
            years = []

            for elm in driver.find_elements_by_xpath("//div[@id='container']/div[contains(@class,'viewIndexSummarylocation')]/div[contains(@class,'fl')]/div[contains(@class,'h40')]/a"): #//div[@id="col-0"]/dl/dd
                breadcrumb.append(str(elm.text).replace("'",""))
            brandName = breadcrumb[-2]
            kindName = breadcrumb[-1]
            #print(breadcrumb[-2],breadcrumb[-1])


            #年度
            for elm in driver.find_elements_by_xpath("//div[@id='container']/div[contains(@class,'viewIndexSummarypageTop')]/div[contains(@class,'fl')]/ul[contains(@class,'summary-top-list')]/li[contains(@class,'summary-top-li')]/a"): 
                years.append(str(elm.get_attribute("textContent")).replace("款","").strip())
            #年度[停售車款]
            for elm in driver.find_elements_by_xpath("//li[@id='summary-top-car']/div[contains(@class,'summary-top-down-wrap')]/div[contains(@class,'summary-top-down')]/a[contains(@class,'summary-top-down-link')]"):
                years.append(str(elm.get_attribute("textContent")).replace("款","").strip())


            #col-0 [規格.配備名稱] col-1 [第一筆category資料] col-2 [第二筆category資料] 以此類推
            for elm in driver.find_elements_by_xpath("//div[@id='compare-main']/div[contains(@class,'cp-comp-col')]"): 
                #print(str(elm.get_attribute('id')))
                colIds.append(str(elm.get_attribute('id')))
            #print(colIds)

            SpecGroups = []
            #for elm in driver.find_elements_by_xpath(f"""//div[@id='compare-main']/div[contains(@class,'comp-item-menu')]/dl[@id='comp-item-th']/dt"""): #//div[@id="col-0"]/dl/dd
            for elm in driver.find_elements_by_xpath(f"""//div[@id='compare-main']/div[contains(@id,'{colIds[0]}')]/dl[@id='comp-item-th']/dt"""): #//div[@id="col-0"]/dl/dd
                #print(str(elm.get_attribute('id')),str(elm.text))
                SpecGroups.append([str(elm.get_attribute('id')),str(elm.text)])

            df = pd.DataFrame(SpecGroups, columns=['group','groupnname'])

            #df["item"] = df['group'].apply(lambda x: x.split('-')[0])
            df["groupid"] = df['group'].apply(lambda x: x.split('-')[1])
            #print(df)

            sitem = []
            for elm in driver.find_elements_by_xpath("//div[@id='compare-main']/div[contains(@class,'comp-item-menu')]/dl/dd[contains(@class,'cp-brd')]"): #//div[@id="col-0"]/dl/dd
                #print(str(elm.get_attribute('id')).replace("cell-0-",""),str(elm.text))
                sitem.append([str(elm.get_attribute('id')).replace("cell-0-",""),str(elm.text)])

            dfcell = pd.DataFrame(sitem, columns=['line','name'])

            dfcell["groupid"] = dfcell['line'].apply(lambda x: x.split('-')[0])
            #dfcell["name"] = dfcell['line'].apply(lambda x: x.split('-')[1])
            #print(dfcell)

            res = pd.merge(df,dfcell, on=['groupid'],how='left')
            #print(res)
            #print("*********************************************************")
            for index in range(len(colIds)):
                varRowID = str(colIds[index]).replace("col-","")
                if varRowID!="0":
                    CellPrefix = '{0}-'.format(str(colIds[index]).replace("col","cell"))
                    data_myid = ""
                    data_nm = ""
                    res['CellLine'] = res['line'].apply(lambda x: CellPrefix+x)
                    res['CellText'] = ""
                    res['BrandName'] = ""
                    res['SeriesID']= ""
                    res['SeriesName'] = ""
                    res['CategoryName'] = ""
                    res['ModelYear'] = ""
                    res['CategoryUrl'] = ""
                    res['MyID'] = ""
                    res['DisplayName'] = ""

                    #print("===============" + CellPrefix )

                    for elm in driver.find_elements_by_xpath(f"""//div[@id='{colIds[index]}']/div[contains(@class,'comp-col-hd')]/div[contains(@class,'comp-col-list')]/div[contains(@class,'comp-col-list-vs')]/a"""): #//div[@id="col-0"]/dl/dd
                        #print(str(elm.get_attribute('data-kid')),str(elm.get_attribute('data-myid')),str(elm.get_attribute('data-nm'))) 
                        data_kid = str(elm.get_attribute('data-kid'))
                        data_myid = str(elm.get_attribute('data-myid'))
                        data_nm = str(elm.get_attribute('data-nm')).replace("'","")

                    tmpData = dfCar.loc[(dfCar['CategoryUrl'] == str(CategoryUrl)) & (dfCar['MyID'] == str(data_myid)) , :]
                    #print(" === " + CategoryUrl + " : MyID_" + str(data_myid) + " => " +  str(len(tmpData)))

                    if len(tmpData)==0:
                            
                        for rowidx, row in res.iterrows():
                            #print(row['CellLine'], row['CellText'])
                            for elm in driver.find_elements_by_xpath(f"""//dd[@id='{row['CellLine']}']"""):
                                res['CellText'][rowidx] = str(elm.text).replace("\n一鍵詢價","")
                                res['BrandName'][rowidx] = brandName
                                res['SeriesID'][rowidx] = data_kid
                                res['SeriesName'][rowidx] = kindName
                                res['CategoryName'][rowidx] = data_nm.replace(brandName,"").replace(kindName,"").replace(str(years[0]),"").strip()
                                res['ModelYear'][rowidx] = years[0]
                                res['CategoryUrl'][rowidx] = CategoryUrl
                                res['MyID'][rowidx] = data_myid
                                res['DisplayName'][rowidx] = data_nm
                                #print(str(elm.text).replace("\n一鍵詢價","")) 
                        #print(res)
                        dfCategory = res[['BrandName','SeriesID','SeriesName','CategoryName','ModelYear','CategoryUrl','MyID','DisplayName']].copy().drop_duplicates()

                        if (len(res.loc[(res['name'] == "新車售價")]))>0:
                            dfCategory['Price'] = (res.loc[(res['name'] == "新車售價")]["CellText"]).to_string(index=False).strip()
                        else:
                            dfCategory['Price'] = ""

                        if (len(res.loc[(res['name'] == "動力型式")]))>0:            
                            dfCategory['GasType'] = (res.loc[(res['name'] == "動力型式")]["CellText"]).to_string(index=False).strip()
                        else:
                            dfCategory['GasType'] = ""        
                        #print("*******")
                        #print(len(res.loc[(res['name'] == "排氣量")]))
                        #print("*******")
                        if (len(res.loc[(res['name'] == "排氣量")]))>0:
                            dfCategory['EngineDisplacement'] = (res.loc[(res['name'] == "排氣量")]["CellText"]).to_string(index=False).strip()
                        else:
                            dfCategory['EngineDisplacement'] = ""

                        if (len(res.loc[(res['name'] == "變速系統")]))>0:  
                            dfCategory['GearType'] = (res.loc[(res['name'] == "變速系統")]["CellText"]).to_string(index=False).strip()
                        else:
                            dfCategory['GearType'] = ""       

                        if (len(res.loc[(res['name'] == "驅動型式")]))>0:      
                            dfCategory['Transmission'] = (res.loc[(res['name'] == "驅動型式")]["CellText"]).to_string(index=False).strip()
                        else:
                            dfCategory['Transmission'] = ""

                        if (len(res.loc[(res['name'] == "車身型式")]))>0:             
                            dfCategory['BodyType'] = (res.loc[(res['name'] == "車身型式")]["CellText"]).to_string(index=False).strip()
                        else:
                            dfCategory['BodyType'] = ""

                        if (len(res.loc[(res['name'] == "煞車型式")]))>0:             
                            dfCategory['BrakeType'] = (res.loc[(res['name'] == "煞車型式")]["CellText"]).to_string(index=False).strip()
                        else:
                            dfCategory['BrakeType'] = ""


                        if (len(res.loc[(res['name'] == "車門數")]))>0:       
                            #dfCategory['Door'] = re.findall(r'^\d+', (res.loc[(res['name'] == "車門數")]["CellText"]).to_string(index=False))[0]
                            door = (res.loc[(res['name'] == "車門數")]["CellText"]).to_string(index=False).strip()
                            door = door.strip().replace("門","")
                            dfCategory['Door'] = door
                        else:
                            dfCategory['Door'] = ""

                        if (len(res.loc[(res['name'] == "乘客數")]))>0:   
                            #dfCategory['Passenger'] = re.findall(r'^\d+', (res.loc[(res['name'] == "乘客數")]["CellText"]).to_string(index=False))[0]
                            passenger = (res.loc[(res['name'] == "乘客數")]["CellText"]).to_string(index=False).strip()
                            passenger = passenger.strip().replace("座","")
                            dfCategory['Passenger'] = passenger
                        else:
                            dfCategory['Passenger'] = ""

                        #print("=== master ===")    
                        #print(dfCategory)

                        #print("=== detail ===")       
                        dfCarSpec = res[['groupnname','groupid','name','CellText','MyID']].copy()
                        dfCarSpec = dfCarSpec.rename({'groupnname':'group','name':'specname','CellText':'specval'},axis='columns')
                        #print(dfCarSpec)

                        send_to_db(dfCategory,dfCarSpec)
        except Exception as e:
            print("Unable to connection:" + CategoryUrl)

def send_to_db(df,dfSpec):
    if(env=="1"):
        mssql_db = 'pdmasterdb.cpambwr2r6oj.ap-northeast-1.rds.amazonaws.com'
        mssql_user = 'administrator'
        mssql_password = '!abccar2020'
        mssql_db_name = 'ABCCar'
    else:
        mssql_db = 'pd-dev-stage.cpambwr2r6oj.ap-northeast-1.rds.amazonaws.com'
        mssql_user = 'administrator'
        mssql_password = '!abccar2020'
        mssql_db_name = 'ABCCar'
  

    # 連結 SQL                   
    connect_db = pymssql.connect(host=mssql_db, port=1433, user=mssql_user, password=mssql_password, database=mssql_db_name, charset='utf8', autocommit=True)
    for index in df.index: 
        categoryUrl = df['CategoryUrl'][index]
        myID = df['MyID'][index] 
        brandName = df['BrandName'][index].replace("'","")
        seriesName = df['SeriesName'][index].replace("'","")
        categoryName = df['CategoryName'][index].replace("'","")
        modelYear = df['ModelYear'][index].replace("'","")
        title = df['DisplayName'][index].replace("'","")
        price = df['Price'][index].replace("'","")
        gasType = df['GasType'][index].replace("'","")
        engineDisplacement = df['EngineDisplacement'][index].replace("'","")
        transmission = df['Transmission'][index].replace("'","")
        gearType = df['GearType'][index].replace("'","")
        brakeType = df['BrakeType'][index].replace("'","")
        bodyType = df['BodyType'][index].replace("'","")
        door = df['Door'][index].replace("'","")
        passenger = df['Passenger'][index].replace("'","")
        
        with connect_db.cursor() as cursor:
            # 執行 SQL 指令
            sql = f"""
    IF NOT EXISTS (SELECT 1 FROM [dbo].[Web8891CarData] WHERE [CategoryUrl] = '{categoryUrl}' AND [MyID] = '{myID}') 
    BEGIN 
        INSERT INTO [dbo].[Web8891CarData] ([CategoryUrl],[MyID],[BrandName],[SeriesName],[CategoryName],
        [ModelYear],[Title],[Price],[GasType],[EngineDisplacement],[Transmission],[GearType],[BrakeType],
        [BodyType],[Door],[Passenger],[CreateDate]) 
        VALUES ('{categoryUrl}','{myID}','{brandName}','{seriesName}','{categoryName}',
        '{modelYear}','{title}','{price}','{gasType}','{engineDisplacement}',
        '{transmission}','{gearType}','{brakeType}','{bodyType}','{door}','{passenger}',getdate());
        
        SELECT @@IDENTITY AS Id;
    END 
    """
            cursor.execute(sql)

            try:
                #record_id = cursor.fetchone()[0]
                row = cursor.fetchone()
                if row:
                    record_id = row[0]
                    dfSpec = dfSpec.fillna("")
                    for seq in dfSpec.index: 
                        groupID = dfSpec['groupid'][seq].replace("'","")
                        groupName = dfSpec['group'][seq].replace("'","")
                        specName = dfSpec['specname'][seq].replace("'","")
                        specValue = dfSpec['specval'][seq].replace("'","")
                        myID = dfSpec['MyID'][seq] 

                        sql = f"""
                            INSERT INTO [dbo].[Web8891CarSpec] ([ID],[GroupID],[GroupName],
                            [SpecName],[SpecValue],[MyID],[CreateDate]) 
                            VALUES ('{record_id}','{groupID}','{groupName}','{specName}',
                            '{specValue}','{myID}',getdate());      
                        """

                        cursor.execute(sql)
            except Exception as e:
                print("=== Repeat MyID:" + myID + " exist.===")

    # 關閉 SQL 連線
    connect_db.close()


if __name__ == "__main__":
    print(now)
    
    script = 'crawler8891CarData'
    try:
        dfUrl, dfCar = readData()
    except Exception as e:
        msg = '{0}: PART 1: 取得 8891 URL 錯誤! The error message : {1}'.format(script, e)
        send_error_message(script=script, message=msg)
        print(msg)
        sys.exit(1)
        
    print('PART 1: Finished!')


    try:
        start_time = time.time()       
        crawlCarData(dfUrl, dfCar)
        print("--- %s seconds ---" % (time.time() - start_time))
    except Exception as e:
        msg = '{0}: PART 2: 抓取 8891 新車資料錯誤! The error message : {1}'.format(script, e)
        send_error_message(script=script, message=msg)
        print(msg)
    finally:
        driver.close()
        
    print('PART 2: Finished!')
