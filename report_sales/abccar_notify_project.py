#==============
# Path: /home/hadoop/jessielu/report_sales
# Description: Transform format to pdf and image. Send notify to TeamsChannel / Line.
# Date: 2022/09/15
# Author: Sam 
#==============

# coding: utf-8

import sys, os
# 正式機
sys.path.insert(0, '/home/hadoop/jessielu/')

# 測試機
#sys.path.insert(0, '/home/jessie-ws/Dev/jupyterlab/daily_report/')

from Module.ConnFunc import read_test_mssql, read_mysql
#from Module.EmailFuncForSales import send_daily_report_onself_office, send_daily_report_onself_sales, send_daily_report_error
from openpyxl import Workbook, load_workbook

import pandas as pd
from datetime import datetime, timedelta, date
from dateutil.relativedelta import relativedelta  
#import math
#import shutil
import subprocess

from pdf2image import convert_from_path
from PIL import Image
from Module.NotifyFunc import Teams_Notify, Line_Notify, Line_Notify_TL

#系統日
now = date.today()
#系統日前一日
end_date = (now - timedelta(1)).strftime("%Y-%m-%d")

#無商品訂單帶入系統月份參數
end_date_mm = (now - timedelta(1)).strftime("%m")

#正式區路徑
path = '/home/hadoop/jessielu/report_sales/files/'
#測試機路徑
#path = '/home/jessie-ws/Dev/jupyterlab/daily_report/report_sales/files/'

os.chdir(path)

# 移除前次產出圖檔
imgs = ['abc_project.png','abc_project_crop.png']
for r in imgs:
    if os.path.isfile(r):
        os.remove(r)
        
def doc2pdf_linux(docPath, pdfPath):
    cmd = 'libreoffice --headless --convert-to pdf'.split()+[docPath] + ['--outdir'] + [pdfPath]
    p = subprocess.Popen(cmd, stderr=subprocess.PIPE, stdout=subprocess.PIPE,bufsize=1)
    p.wait(timeout=30)
    # stdout, stderr = p.communicate()
    # print(stdout, stderr)
    p.communicate()

def doc2pdf(docPath, pdfPath):
    docPathTrue = os.path.abspath(docPath)  
    return doc2pdf_linux(docPathTrue, pdfPath)

def mergeData():
    innerDB_901 = readData(901)
    #print(innerDB_901['CarID'].count())
    
    innerDB_902 = readData(902)
    #print(innerDB_902['CarID'].count())
    
    ViewDB = readData2()
    #print(int(ViewDB['ViewCount']))
    
    ProductOrder = readProduct()
    
    url = "https://docs.google.com/spreadsheets/d/e/2PACX-1vRAt6kvRycLJzSp0f4TXL3pxgJxSZwCLtb0dRIasiYMFRtprh3BlBD4tfege3HqnQoSR7Uqmak_ENSz/pubhtml?gid=0&single=true&v=1"
    # Assign the table data to a Pandas dataframe
    google_table = pd.read_html(url, encoding = 'utf8',)[0].fillna(0)

    google_table.columns = ['idx','title', 'nums']

    google_table['nums'] = pd.to_numeric(google_table['nums'], downcast="integer")
    google_table = google_table.drop(['idx'],axis=1)

    tmpA = google_table.loc[(google_table['title'] == '不二價來電數') , :]
    tmpB = google_table.loc[(google_table['title'] == '不二價看車次數') , :]
    tmpC = google_table.loc[(google_table['title'] == '不二價成交台數') , :]
    # tmpD = google_table.loc[(google_table['title'] == '廣告Banner銷售金額') , :]
    
    
    #測試機路徑
    wordpath= path + '不二價日報.xlsx'
    #=========================== Load office file      
    wb = load_workbook(wordpath)

    #Sheet 1
    ws_main = wb.worksheets[0]

    # Add 不二價架上台數
    data = ws_main.cell(row=2, column=1)
    data.value = innerDB_901['CarID'].count()
    
    ## Add 不二價物件瀏覽數
    data = ws_main.cell(row=2, column=3)
    data.value = int(ViewDB['ViewCount']) 
    
    # Add 不二價來電數
    data = ws_main.cell(row=5, column=1)
    data.value = int(tmpA['nums'].to_string(index=False))

    # Add 不二價看車次數
    data = ws_main.cell(row=5, column=3)
    data.value = int(tmpB['nums'].to_string(index=False))
    
    # Add 不二價成交台數
    data = ws_main.cell(row=5, column=5)
    data.value = int(tmpC['nums'].to_string(index=False))
    
    # Add 安心購架上台數
    data = ws_main.cell(row=8, column=1)
    data.value = innerDB_902['CarID'].count()
    
    # Add 廣宣 X月銷售金額
    # data = ws_main.cell(row=8, column=3)
    # data.value = int(tmpD['nums'].to_string(index=False))
    if (len(ProductOrder.index)==0):
        data = ws_main.cell(row=7, column=3)
        data.value = "廣宣商品\n"+ end_date_mm + "月銷售金額"
        data = ws_main.cell(row=8, column=3)
        data.value = 0
    else:
        data = ws_main.cell(row=7, column=3)
        data.value = "廣宣商品\n"+ ProductOrder['OrderYM'].to_string(index=False) + "月銷售金額"
        data = ws_main.cell(row=8, column=3)
        data.value = int(ProductOrder['OrderTotal'].to_string(index=False))
        
    wb.save(wordpath)
    
    #Excel 轉 PDF
    pdfPath = path
    doc2pdf(wordpath, pdfPath)

def readData(CID):
    # 參加活動物件數
    carList = f"""
	select 
    c.CarID
    --,c.ViewCount AS [瀏覽次數]
    --,c.MemberID
    from abccar.dbo.Car c with (NOLOCK)
    where c.CarID in (SELECT CarID FROM abccar.dbo.CampaignItem with (NOLOCK) WHERE (CampaignID = {CID}) )
	and c.Status = 1
	and GetDate() Between c.[StartDate] And c.[EndDate]
    """
    
    if CID==902:
        carList = carList + f"""
        and c.CarID in (
            select poi.CarID 
            from abccar.dbo.ProductOrderItem poi WITH (NOLOCK)
            left join [ABCCar].[dbo].ProductOrder po WITH (NOLOCK) on poi.OrderID = po.ID
            LEFT JOIN [ABCCar].[dbo].Car car WITH (NOLOCK) on poi.CarID = car.CarID
            LEFT JOIN [ABCCar].[dbo].[MemberUserAccountSalesMapping] mua WITH (NOLOCK) ON car.[MemberID] = mua.[MemberID]
            LEFT JOIN [ABCCar].[dbo].[UserAccountSales] ua WITH (NOLOCK) ON ua.[ID] = mua.[UserAccountSalesID]
            where ProductID = 7
            and po.Deleted = 0
            --排除無效作廢訂單
            and po.OrderStatusID = 30 and po.PaymentStatusID = 30 
            and car.Status = 1
            and GetDate() Between car.[StartDate] And car.[EndDate]
            --排除 異常車商 / 暫不分配 / 其他
            and ua.[ZoneName] >=1 and ua.[ZoneName] <=5 
        )
        """
        
    carListRtn = read_test_mssql(carList, 'ABCCar')
    
    return carListRtn

def readData2():
    # 瀏覽差異數
    query1 = f"""
	SELECT 
    /* a.*,ifnull(b.ViewCount,0) as bdate_ViewCount */
    sum(a.ViewCount-ifnull(b.ViewCount,0)) as ViewCount 
    FROM car_fixprice_new a left join car_fixprice_new b 
                            on a.CampaignID=b.CampaignID 
                            and b.Date = (select date_add(max(Date),interval -1 day) from car_fixprice_new) 
                            and a.CarID=b.CarID 
    WHERE a.CampaignID='901' and a.Date = (select max(Date) from car_fixprice_new)
    """
    carView = read_mysql(query1, 'report')
    
    return carView

def readProduct():
    #廣宣商品
    ProductQuery = f"""
        SELECT substring(CONVERT(varchar(7),mc.CreateDate,120),6,2) as OrderYM,CONVERT(int,sum(mc.OrderTotal)) as OrderTotal 
        FROM ProductOrder mc WITH (NOLOCK) 
        LEFT JOIN [ABCCar].[dbo].[MemberUserAccountSalesMapping] mua WITH (NOLOCK) ON mc.[MemberID] = mua.[MemberID] 
        LEFT JOIN [ABCCar].[dbo].[UserAccountSales] ua WITH (NOLOCK) ON ua.[ID] = mua.[UserAccountSalesID] 
        , ( SELECT k.OrderID,p.Name as 'ProductName',p.ID,p.ProductTypeId FROM ProductOrderItem k WITH (NOLOCK), Product p WITH (NOLOCK) 
            WHERE k.Price>0 
            and k.ID = (SELECT MAX(t.ID) FROM ProductOrderItem t where t.OrderID=k.OrderID and t.Price>0 ) 
            and k.ProductID=p.ID 
            and p.ProductTypeId in (6,7,8,9) 
        ) mb 
        WHERE mc.OrderStatusID=30 AND mc.PaymentStatusID=30 and mc.OrderTotal>0 
        and mc.ID=mb.OrderID and mua.UserAccountSalesID in 
		(
			 select ID from [ABCCar].[dbo].[UserAccountSales] 
			 where Type='2' and Status=1 and ZoneName between 1 and 5
		)
        and CONVERT(varchar(7),mc.CreateDate,120)=CONVERT(varchar(7),GETDATE()-1,120) 
		group by CONVERT(varchar(7),mc.CreateDate,120) 
    """
    ProductListRtn = read_test_mssql(ProductQuery, 'ABCCar')
    
    return ProductListRtn

if __name__ == "__main__":
    
    print(now.strftime("%Y-%m-%d"))
    # 設定日期
    script = 'send_onshelf_notify_fixprice'

    try:
        # 讀取資料寫入EXCEL轉PDF
        mergeData()
        print('Part 1 done!')
    except Exception as e:
        msg = '{0}: PART 1: 填入不二價日報值失敗! The error message : {1}'.format(script, e)
        print(msg)
        sys.exit(1)
        
    try:
        # PDF轉圖檔
        images = convert_from_path(path + '不二價日報.pdf')
        images[0].save(path + 'abc_project.png')
        # 裁圖另存
        img = Image.open(path + 'abc_project.png')
        # 測試區 im1 = img.crop((120, 140, 870, 740))
        im1 = img.crop((120, 140, 980, 740))
        im1.save(path + 'abc_project_crop.png', quality=50) 
        
        print('Part 2 done!')
    except Exception as e:
        msg = '{0}: PART 2: 不二價日報轉檔失敗! The error message : {1}'.format(script, e)
        print(msg)
        sys.exit(1)
        
    try:
         
        # 發送訊息 receiver=> j [測試] / g [正式]
        receiver = 'g'
        file = 'abc_project_crop.png'
        message = "【abc好車網_專案日報】(%s)" % end_date
        Teams_Notify(receiver = receiver, path = path,msg = message, FN = file)
        # \n Line 換行符號       
        message =  "\n" + message   
        Line_Notify(receiver = receiver, path = path,msg = message, FN = file)
        
        ## 週一發 TL
        if datetime.today().weekday()==0:
            Line_Notify_TL(receiver = receiver, path = path,msg = message, FN = file)
    
        print('Part 3 done!')
    except Exception as e:
        msg = '{0}: PART 3: 不二價日報 Notify 失敗! The error message : {1}'.format(script, e)
        print(msg)
        sys.exit(1)