 # coding: utf-8
import sys
import os
import numpy as np

sys.path.insert(0, '/home/hadoop/jessielu/')

from Module.EmailFunc8891 import send_8891_report, send_8891_report_error
from Module.ConnFunc import read_mssql, read_test_mssql, to_mysql, read_mysql

from openpyxl import load_workbook
from openpyxl.utils.dataframe import dataframe_to_rows

import pandas as pd
from datetime import datetime, timedelta, date
from dateutil.relativedelta import relativedelta  
import math

now = date.today()
# =============2021/10/01修改，每月3號凌晨跑()
# now = (datetime.today() - relativedelta(days=1)).strftime('%Y-%m-%d')

path = '/home/hadoop/jessielu/report_sales/files/'

os.chdir(path)

def ReadData():
    sql = f"""
        SELECT
        sl.ID as [流水號]
        ,ISNULL(sl.StoreName , '') as [車商名稱]
        ,ISNULL(sl.ContentPerson , '') as [聯絡人]
        ,ISNULL(sl.CellPhone , '') as [手機號碼]
        ,ISNULL(sl.TelPhone , '') as [市內電話]
        ,ISNULL(sl.Email , '') as [信箱]
        ,ISNULL(sl.Address , '') as [車商地址]
        ,ISNULL(sl.OnSaleCarTotal , '') as [在庫車輛]
        ,ISNULL(sl.OnStoreCarTotal , '') as [在店車輛]
        ,ISNULL(sl.Care , '') as [是否為嚴選]
        ,ISNULL(sl.Best , '') as [是否為旺店]
        ,ISNULL(sl.Url , '') as [車商8891網址]
        ,ISNULL(sl.CreateDate , '') as [建立日期]
        ,ISNULL(sl.AbcMemberID , '') as [ABC MemberID]
        ,ISNULL((SELECT COUNT(1) FROM [ABCCar].[dbo].[Car] WITH (NOLOCK) WHERE [MemberID] = m.MemberID AND [Status] = 1 and GetDate() Between [Car].[StartDate] And [Car].[EndDate]), 0) as [本月架上車輛]
        ,ISNULL(mc.[Name], '') as [會員名稱]
        ,ISNULL(m.[CarDealerName], '') as [車商名稱]
        ,CASE WHEN (m.[CarDealerCountryName] is null or m.[CarDealerCountryName] = '') THEN CONCAT(mc.[CountryName], mc.[DistrictName]) ELSE CONCAT(m.[CarDealerCountryName], m.[CarDealerCountryName]) END as [城市別]    ,ISNULL(m.[ContactPerson], '') AS [連絡人]
        ,ISNULL(mc.[CellPhone], '') AS [連絡電話]
        FROM [ABCCar].[dbo].[8891StoreList] sl
        LEFT JOIN [ABCCar].[dbo].Member m WITH (NOLOCK) ON sl.[AbcMemberID] = m.[MemberID]
		LEFT JOIN [ABCCarMemberCenter].[dbo].[Member] mc WITH (NOLOCK) ON m.[MemberID] = mc.[ID]
        WHERE sl.CreateDate = '{now}'
        ORDER BY m.MemberID

    """
    sql_2 = f"""
        select 
        HomeUrl,
        StoreName,
        CreateDate 
        from [WebCrawler].[dbo].[CrawlerTask] 
        order by CreateDate desc
    """

    rtn_sql = read_mssql(sql, 'ABCCar')
    rtn_sql2 = read_mssql(sql_2, 'ABCCar')

    return rtn_sql, rtn_sql2

def processData(df, df2):
    wb = load_workbook('8891_StoreList.xlsx')

    # Select the first work sheet
    ws_main = wb.worksheets[0]
    ws_main.delete_rows(2, 100000)

    n=1
    for r in dataframe_to_rows(df, index=False, header=False):
        m = 1
        for i in r:
            grid = ws_main.cell(row=1+n, column=m)
            grid.value = i
            m+=1
        n +=1

    # Select the second work sheet
    ws_side = wb.worksheets[1]
    ws_side.delete_rows(2, 100000)

    n=1
    for r in dataframe_to_rows(df2, index=False, header=False):
        m = 1
        for i in r:
            grid = ws_side.cell(row=1+n, column=m)
            grid.value = i
            m+=1
        n +=1

    wb.save('8891_StoreList.xlsx')

if __name__ == '__main__':

    script = '8891_store_info'
    print(datetime.now())
    
    try:
        rtn8891Store, rtn = ReadData()
        msg = 'PART 1: read data done!'
        print(msg)
    except Exception as e:
        msg = '{0}: PART 1: 數據連線讀取失敗! The error message : {1}'.format(script, e)
        send_8891_report_error(script='{}'.format(script), message=msg)
        print(msg)
        sys.exit(1)
    
    try:
        processData(rtn8891Store, rtn)
        msg = 'PART 2: insert data done!'
        print(msg)
    except Exception as e:
        msg = '{0}: PART 2: 數據寫入失敗! The error message : {1}'.format(script, e)
        send_8891_report_error(script='{}'.format(script), message=msg)
        print(msg)
        sys.exit(1)     

    try:
        send_8891_report(receiver='g', path=path)
        msg = 'PART 3: 寄送 8891 OK'
        print(msg)
    except Exception as e:
        msg = '{0}: PART 3: 寄送8891日報發生錯誤. The error message : {1}'.format(script, e)
        send_8891_report_error(script='{}'.format(script), message=msg)
        print(msg)
        sys.exit(1)
   