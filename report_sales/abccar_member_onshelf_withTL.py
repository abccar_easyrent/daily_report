# coding: utf-8
import sys
import os
import numpy as np

# sys.path.insert(0, '/home/jessie/MyNotebook/daily_report/')
sys.path.insert(0, '/home/hadoop/jessielu/')

from Module.EmailFuncForSales import send_daily_report_onself, send_daily_report_error, send_error_message
from Module.ConnFunc import read_mssql, read_test_mssql, to_mysql, read_mysql
from Module.HelpFunc import display_set

from openpyxl.styles import Border, Side, PatternFill, Font, GradientFill, Alignment
from openpyxl import Workbook, load_workbook
from openpyxl.utils.dataframe import dataframe_to_rows

import pandas as pd
from datetime import datetime, timedelta, date
from dateutil.relativedelta import relativedelta  

#原本
now = date.today() + relativedelta(days=1)
#補發用
# now = date.today()

if(now.day == 1): #2021/11/1; 2022/01/01
    tmpDay = now - relativedelta(days=1) #2021/10/31; 2021/12/31
    #這個月的第一天
    first_day = date(tmpDay.year, tmpDay.month, 1) #2021/10/1; 2021/12/1
    #上個月的最後一天
    lastMonth_end_day = date(first_day.year, first_day.month, 1) - relativedelta(days=1)#2021/09/30; 2021/11/30
    #下個月的第一天
    nextMonth_first_day = first_day + relativedelta(months=1)#2021/11/1; 2022/01/01
    #這個月最後一天
    thisMonth_end_day = nextMonth_first_day - relativedelta(days=1)#2021/10/31; 2021/12/31
#if now = 2022/01/02
else:
    #這個月的第一天
    first_day = date(now.year, now.month, 1) #2022/01/01
    #上個月的最後一天
    lastMonth_end_day = date(first_day.year, first_day.month, 1) - relativedelta(days=1) #2021/12/31
    #下個月的第一天
    nextMonth_first_day = first_day + relativedelta(months=1) #2022/02/01
    #這個月最後一天
    thisMonth_end_day = nextMonth_first_day - relativedelta(days=1) #2022/01/31

#昨天
end_date = (now - timedelta(1)).strftime("%Y-%m-%d") 
#前天
the_day_before_yesterday_date = (now - timedelta(2)).strftime("%Y-%m-%d") 

display_set(200, 30)
path = '/home/hadoop/jessielu/report_sales/files/'
# path = '/home/jessie/MyNotebook/easyRent/scripts/'

os.chdir(path)

# print(nextMonth_first_day)

# 讀取數據
def ReadData():

    #每月月初，要更新月底檔案
    if(now.day == 1):
        stageLastMonth = f"""
            SELECT 
                m.[MemberID],
                ISNULL((SELECT COUNT(1) FROM [ABCCar].[dbo].[Car] WITH (NOLOCK) WHERE [MemberID] = m.MemberID AND [Status] = 1  and GetDate() Between [Car].[StartDate] And [Car].[EndDate]), 0) as [上月底架上台數]
                --ISNULL((SELECT COUNT(1) FROM [Car] WITH (NOLOCK) WHERE [MemberID] = m.MemberID AND [Status] = 1 AND [EndDate] >=  '{end_date} 23:59:59'), 0) as [上月底架上台數]
            FROM [ABCCar].[dbo].Member m
            LEFT JOIN [ABCCarMemberCenter].[dbo].[Member] mc WITH (NOLOCK) ON m.[MemberID] = mc.[ID]
            LEFT JOIN [ABCCar].[dbo].[MemberAgree] ma WITH (NOLOCK) ON m.[MemberID] = ma.[MemberID]
            LEFT JOIN [ABCCar].[dbo].[BindingCardMember] bm WITH (NOLOCK) ON m.[MemberID] = bm.[MemberID] AND bm.[Status] = 0
            LEFT JOIN [ABCCar].[dbo].[MemberUserAccountSalesMapping] mua WITH (NOLOCK) ON m.[MemberID] = mua.[MemberID]
            LEFT JOIN [ABCCar].[dbo].[UserAccountSales] ua WITH (NOLOCK) ON ua.[ID] = mua.[UserAccountSalesID]
            OUTER APPLY 
                (SELECT TOP 1 *
                FROM [ABCCar].[dbo].[PoolPointsHistory] ph1 with (NOLOCK)
                WHERE m.MemberID = ph1.MemberID AND ph1.RecordTypeID = 10
                ORDER BY ph1.ID DESC
            ) AS ph
            WHERE 
            (EXISTS
                (SELECT * FROM [ABCCar].[dbo].MemberAgree WITH (NOLOCK)
                WHERE [MemberID] = m.MemberID)
            OR EXISTS
                (SELECT * FROM [ABCCar].[dbo].[Car] car 
                JOIN [ABCCar].[dbo].[CarModel] carModel WITH (NOLOCK) ON carModel.[IsEnable] = 1 
                            AND car.[BrandID] = carModel.[BrandID] 
                            AND car.[SeriesID]= carModel.[SeriesID] 
                            AND car.[CategoryID]= carModel.[CategoryID]
                JOIN [ABCCar].[dbo].[Member] member WITH(NOLOCK)  ON car.[MemberID] = CAST(member.MemberID AS bigint)
                            AND member.[Status] = 1
                            AND member.[Category] IN (2,3,5) --個人賣家、車商、經紀人
                WHERE car.[Status] IN (0, 1) --待售
                AND car.[ModifyDate] >= '2018-06-20'
                AND car.MemberID = m.MemberID)) 
            AND m.[Category] > 1
            AND (bm.BindingDate IS NOT NULL OR ph.ID IS NOT NULL)
            ORDER BY m.MemberID
        """
        # 執行讀取程式
        stageOnself = read_mssql(stageLastMonth, 'ABCCar')
        #儲存當月月底的資料
        stageOnself.to_csv('stageLastMonth_'+ str(end_date) +'.csv')
        print('output done!')

    memberAgree_sg = f"""
       SELECT 
        m.MemberID
        ,ISNULL(mc.[Name], '') as [會員名稱]
        ,ISNULL(m.[CarDealerName], '') as [車商名稱]
        --,ISNULL(CONCAT(mc.[CountryName], mc.[DistrictName]), '') AS [城市別]
        ,CASE WHEN (m.[CarDealerCountryName] is null or m.[CarDealerCountryName] = '') THEN CONCAT(mc.[CountryName], mc.[DistrictName]) ELSE CONCAT(m.[CarDealerCountryName], m.[CarDealerDistrictName]) END as [城市別]
        ,ISNULL(m.[ContactPerson], '') AS [連絡人]
        ,ISNULL(mc.[CellPhone], '') AS [連絡電話]
        ,ISNULL(m.[TaxID], '') AS [統編]
        ,ISNULL(m.[HOTMemberID], '') AS [HOT會員編號]

        -- ,IIF(m.[Source] = 'HAA' , m.[SourceKey], '') AS [HAA會員]
        
        ,(CASE(m.[Category]) 
            WHEN 1 THEN '一般買家' 
            WHEN 2 THEN '一般賣家' 
            WHEN 3 THEN '車商' 
            WHEN 4 THEN '原廠' 
            ELSE '買賣顧問' 
        END ) AS [身分別]
        ,IIF(m.[VIPLevel] = 0 , '', '是') AS [網路好店]
        ,IIF(pd.ID IS NULL, '', '是') AS [買萬送萬]
        ,ISNULL(kvd.[Name], '') AS [公會]
		,ISNULL(ua.[Name], '') AS [維護業務]
        ,ISNULL(ua.[NameSeq], '') AS [維護業務序號]
		,ISNULL(ua.[ID], '') AS [維護業務ID]
        ,IIF(m.[Source] = 'HAA' and RIGHT(m.[SourceKey], 1) <> 'A', m.[SourceKey], '') AS [HAA帳號]
 
        ,ISNULL((SELECT COUNT(1) FROM (SELECT gt.CarID FROM [ABCCar].[dbo].[GridTrans] gt WITH (NOLOCK) LEFT JOIN [ABCCar].[dbo].[Car] c WITH (NOLOCK) ON gt.CarID = c.CarID WHERE gt.[MemberID] = m.MemberID AND [GridTranTypeID] = 202 AND gt.[CreateDate] BETWEEN '{first_day} 00:00:00' AND '{nextMonth_first_day} 00:00:00' AND DATEDIFF(day, c.[StartDate], c.[EndDate])> 2 GROUP BY gt.CarID) as tmp1), 0) as [新上架實績台數]
        -- 新上架實績台數(本月)，總表用

        --[本日新上架車輛]--GridTrans.Type=202,CreateDate=今天 == 本日新上
        ,ISNULL((SELECT COUNT(1) FROM (SELECT CarID FROM [ABCCar].[dbo].[GridTrans] WITH (NOLOCK) WHERE [MemberID] = m.MemberID AND [GridTranTypeID] = 202 AND [CreateDate] BETWEEN '{end_date} 00:00:00' AND '{now} 00:00:00' GROUP BY CarID) as tmp1), 0) as [本日新上架車輛]

        --[本日下架車輛]--GridTrans.Type=201,CreateDate=今天 == 本日下架
        ,ISNULL((SELECT COUNT(1) FROM (SELECT CarID FROM [ABCCar].[dbo].[GridTrans] WITH (NOLOCK) WHERE [MemberID] = m.MemberID AND [GridTranTypeID] = 201 AND [CreateDate] BETWEEN '{end_date} 00:00:00' AND '{now} 00:00:00' GROUP BY CarID) as tmp2), 0) as [本日下架車輛]

        --[本月新上架車輛]--GridTrans.Type=202,CreateDate=本月 == 本月新上
        ,ISNULL((SELECT COUNT(1) FROM (SELECT CarID FROM [ABCCar].[dbo].[GridTrans] WITH (NOLOCK) WHERE [MemberID] = m.MemberID AND [GridTranTypeID] = 202 AND [CreateDate] BETWEEN '{first_day} 00:00:00' AND '{nextMonth_first_day} 00:00:00' GROUP BY CarID) as tmp3), 0) as [本月新上架車輛]

		--2日內下架
        ,ISNULL((SELECT COUNT(1) FROM (SELECT gt.CarID FROM [ABCCar].[dbo].[GridTrans] gt WITH (NOLOCK) LEFT JOIN [ABCCar].[dbo].[Car] c ON gt.CarID = c.CarID WHERE gt.[MemberID] = m.MemberID AND gt.[GridTranTypeID] = 201 AND gt.[CreateDate] BETWEEN '{first_day} 00:00:00' AND '{nextMonth_first_day} 00:00:00' AND DATEDIFF(day, c.[StartDate], c.[EndDate])< 3 GROUP BY gt.CarID) as tmp4), 0) as [2日內下架]

        --[本月下架車輛]--GridTrans.Type=201,CreateDate=本月 == 本月下架
        ,ISNULL((SELECT COUNT(1) FROM (SELECT CarID FROM [ABCCar].[dbo].[GridTrans] WITH (NOLOCK) WHERE [MemberID] = m.MemberID AND [GridTranTypeID] = 201 AND [CreateDate] BETWEEN '{first_day} 00:00:00' AND '{nextMonth_first_day} 00:00:00' GROUP BY CarID) as tmp4), 0) as [本月下架車輛]

		,ISNULL((SELECT COUNT(1) FROM [ABCCar].[dbo].[Car] WITH (NOLOCK) WHERE [MemberID] = m.MemberID AND [Status] NOT IN (1,0) AND [TransferCheckId] <= 0 AND [EndDate] BETWEEN '{first_day} 00:00:00' AND '{nextMonth_first_day} 00:00:00'), 0) as [當月下架未過戶]
        ,ISNULL((SELECT COUNT(1) FROM [ABCCar].[dbo].[Car] WITH (NOLOCK) WHERE [MemberID] = m.MemberID AND [Status] = 1 AND [TransferCheckId] > 0 AND [VehicleNoCheckLastTime] BETWEEN '{first_day} 00:00:00' AND '{nextMonth_first_day} 00:00:00' ), 0) as [當月過戶未下架]
        ,ISNULL((SELECT COUNT(1) FROM [ABCCar].[dbo].[Car] WITH (NOLOCK) WHERE [MemberID] = m.MemberID AND [Status] NOT IN (1,0) AND [TransferCheckId] > 0 AND [VehicleNoCheckLastTime] BETWEEN '{first_day} 00:00:00' AND '{nextMonth_first_day} 00:00:00' AND [EndDate] BETWEEN '{first_day} 00:00:00' AND '{nextMonth_first_day} 00:00:00' ), 0) as [當月過戶當月下架]
       
	    --[架上車輛]--[Status] = 1 == 本月架上
        ,ISNULL((SELECT COUNT(1) FROM [ABCCar].[dbo].[Car] WITH (NOLOCK) WHERE [MemberID] = m.MemberID AND [Status] = 1 and GetDate() Between [Car].[StartDate] And [Car].[EndDate]), 0) as [本月架上車輛]
        
        --[車輛已驗證]--[Status] = 1 == 本月架上驗證成功
        ,ISNULL((SELECT COUNT(1) FROM [ABCCar].[dbo].[Car] WITH (NOLOCK) WHERE [MemberID] = m.MemberID AND [Status] = 1　AND [VehicleNoCheckStatus] = 1 AND GetDate() Between [Car].[StartDate] And [Car].[EndDate]), 0) as [本月架上驗證成功車輛]
        --HAA上架數
        ,ISNULL((SELECT COUNT(1) FROM [ABCCar].[dbo].[Car] WITH (NOLOCK) WHERE [MemberID] = m.MemberID AND [Status] = 1 AND [ModifyDate] >= '2018-06-20' AND [Source] IS NOT NULL AND [Source] = 'HAA'), 0) as [HAA上架數]
        --HAA待售數
        ,ISNULL((SELECT COUNT(1) FROM [ABCCar].[dbo].[Car] WITH (NOLOCK) WHERE [MemberID] = m.MemberID AND [Status] = 0 AND [ModifyDate] >= '2018-06-20' AND [Source] IS NOT NULL AND [Source] = 'HAA' AND DATEDIFF(DAY, CreateDate, GETDATE()) <= 25), 0) as [HAA待售數]
        
        ,ISNULL((SELECT COUNT(1) FROM [ABCCar].[dbo].[Car] WITH (NOLOCK) WHERE [MemberID] = m.MemberID AND [Status] = 0 AND [ModifyDate] >= '2018-06-20' AND [CreateDate] BETWEEN '{first_day} 00:00:00' AND '{nextMonth_first_day} 00:00:00' AND [Source] IS NOT NULL AND ISNUMERIC([Source]) > 0), 0) as [簡易刊登本月待售數]
        ,ISNULL((SELECT COUNT(1) FROM [ABCCar].[dbo].[Car] WITH (NOLOCK) WHERE [MemberID] = m.MemberID AND [Status] = 1 AND [ModifyDate] >= '2018-06-20' AND [CreateDate] BETWEEN '{first_day} 00:00:00' AND '{nextMonth_first_day} 00:00:00' AND [Source] IS NOT NULL AND ISNUMERIC([Source]) > 0), 0) as [簡易刊登本月上架數]
        ,ISNULL((SELECT COUNT(1) FROM [ABCCar].[dbo].[Car] WITH (NOLOCK) WHERE [MemberID] = m.MemberID AND [Status] = 1 AND [ModifyDate] >= '2018-06-20' AND [Source] IS NOT NULL AND ISNUMERIC([Source]) > 0), 0) as [簡易刊登架上台數]

        ,CASE WHEN ISNULL(ph.[ID], 0) > 0 THEN ph.CreateDate ELSE NULL END AS [最新儲值時間]
        ,bm.BindingDate as [信用卡綁定時間]
        ,ma.CreateDate as [同意會員條款時間]
        ,CONVERT(varchar(100), mc.[CreateDate], 120) AS [註冊時間]
        FROM [ABCCar].[dbo].Member m WITH (NOLOCK)
        LEFT JOIN [ABCCarMemberCenter].[dbo].[Member] mc WITH (NOLOCK) ON m.[MemberID] = mc.[ID]
        LEFT JOIN [ABCCar].[dbo].[MemberAgree] ma WITH (NOLOCK) ON m.[MemberID] = ma.[MemberID]
        LEFT JOIN [ABCCar].[dbo].[BindingCardMember] bm WITH (NOLOCK) ON m.[MemberID] = bm.[MemberID] AND bm.[Status] in (0,99)
        LEFT JOIN [ABCCar].[dbo].[MemberUserAccountSalesMapping] mua WITH (NOLOCK) ON m.[MemberID] = mua.[MemberID]
        LEFT JOIN [ABCCar].[dbo].[UserAccountSales] ua WITH (NOLOCK) ON ua.[ID] = mua.[UserAccountSalesID]
        LEFT JOIN [ABCCar].[dbo].[KeyValueDetail] kvd WITH (NOLOCK) ON kvd.Value = m.Guild AND kvd.KeyName = 'Guild'
        OUTER APPLY 
        (SELECT TOP 1 po.*
			FROM [ABCCar].[dbo].ProductOrder po WITH (NOLOCK) 
			LEFT JOIN [ABCCar].[dbo].ProductOrderItem poi WITH (NOLOCK) ON po.ID = poi.OrderID
			WHERE m.MemberID = po.MemberID 
			AND poi.ProductID = 1 
			AND po.OrderStatusID = 30
			AND po.PaymentStatusID IN (30, 40, 45)
        ) AS pd

        OUTER APPLY 
            (SELECT TOP 1 *
            FROM [ABCCar].[dbo].[PoolPointsHistory] ph1 with (NOLOCK)
            WHERE m.MemberID = ph1.MemberID AND ph1.RecordTypeID = 10
            ORDER BY ph1.ID DESC
        ) AS ph

        WHERE 
        (EXISTS
            (SELECT * FROM [ABCCar].[dbo].MemberAgree WITH (NOLOCK)
            WHERE [MemberID] = m.MemberID)
        OR EXISTS
            (SELECT * FROM [ABCCar].[dbo].[Car] car WITH (NOLOCK)
            JOIN [ABCCar].[dbo].[CarModel] carModel WITH (NOLOCK) ON carModel.[IsEnable] = 1 
                        AND car.[BrandID] = carModel.[BrandID] 
                        AND car.[SeriesID]= carModel.[SeriesID] 
                        AND car.[CategoryID]= carModel.[CategoryID]
            JOIN [ABCCar].[dbo].[Member] member WITH (NOLOCK)  ON car.[MemberID] = CAST(member.MemberID AS bigint)
                        AND member.[Status] = 1
                        AND member.[Category] IN (2,3,5) --個人賣家、車商、經紀人
            WHERE car.[Status] IN (0, 1) --待售
            AND car.[ModifyDate] >= '2018-06-20'
            AND car.MemberID = m.MemberID))
			
        AND m.[Category] > 1
        AND m.[Status] = 1
        AND (bm.BindingDate IS NOT NULL OR ph.ID IS NOT NULL)
		--AND m.MemberID = 204439
        ORDER BY m.MemberID
    """

    hot_sq = f"""
        DECLARE @HotCarBatchID INT
        SELECT @HotCarBatchID = Max(ID) FROM [ABCCar].[dbo].[HotCarBatch] WITH (NOLOCK)

        SELECT ACC,CERSTS INTO #ACC FROM abccar.dbo.HotCar WITH (NOLOCK) WHERE [HotCarBatchID] = @HotCarBatchID

        SELECT
            (SELECT COUNT(1) FROM #ACC B WHERE B.[ACC] = A.[HOTMemberID]) AS [HOT官網總台數]
            ,(SELECT COUNT(1) FROM #ACC C WHERE C.[ACC] = A.[HOTMemberID] AND C.CERSTS = 4) AS [HOT官網認證台數]
            ,[MemberID]
            ,[HOTMemberID] AS [HOT主帳號]
        FROM abccar.dbo.[HotCarMember] A WITH (NOLOCK)
        WHERE A.[IsMaster] = 1
        ORDER BY [HOT官網總台數] desc

        DROP TABLE #ACC
    """

    target_sq = f"""
        select * from report.target
        where Date = '{first_day}'
    """

    # 執行讀取程式
    
    memberAgree = read_mssql(memberAgree_sg, 'ABCCar')
    hotDF = read_mssql(hot_sq, 'ABCCar')
    target = read_mysql(target_sq, 'report')

    df_hot = pd.merge(memberAgree, hotDF, on="MemberID", how='left')

    #Check data length
    print(len(df_hot))

    #確保資料只有唯一的memberID
    df_hot = df_hot.drop_duplicates(subset=['MemberID'])
    
    #Check data length
    print(len(df_hot))

    return df_hot, target


def addStageLastMonth(df):
    StageLastMonth = pd.read_csv('stageLastMonth_'+ str(lastMonth_end_day) +'.csv') 
    
    tmp = StageLastMonth[['MemberID','上月底架上台數']]
    tmp = tmp.drop_duplicates(subset=['MemberID'])

    mergeDf = pd.merge(df, tmp, on="MemberID", how='left')
    
    return mergeDf

#2022新版邏輯
def processData(df, target):

    salesID = [90, 102, 60, 101, 98, 99, 107, 104, 97, 103, 61]
    TLID = [1, 2, 3, 4, 5]
    targetSalesUpload = target.loc[target['ID'].isin(salesID)]['UploadTarget'].tolist()
    targetSalesUpload.append(sum(targetSalesUpload))

    targetSalesIncrease = target.loc[target['ID'].isin(salesID)]['IncreaseTarget'].tolist()
    targetSalesIncrease.append(sum(targetSalesIncrease))

    targetTLUpload = target.loc[target['ID'].isin(TLID)]['UploadTarget'].tolist()
    targetTLUpload.append(sum(targetTLUpload))

    targetTLIncrease = target.loc[target['ID'].isin(TLID)]['IncreaseTarget'].tolist()
    targetTLIncrease.append(sum(targetTLIncrease))

    #新上架目標台數
    # NewUploadTarget = [350, 250, 950, 350, 280, 350, 250, 200, 350, 500, 500, 4330]
    NewUploadTarget = targetSalesUpload

    # 2021-12-27 組業務排序邏輯字串
    df['維護業務'] = df.apply(lambda x: '0' + str(x['維護業務序號']) + ' ' + x['維護業務']  if len(str(x['維護業務序號'])) == 1 else str(x['維護業務序號']) + ' ' + x['維護業務'] , axis=1)
    df['維護業務'] = df['維護業務'].replace('00 ', '')
    
    # salesID = [90, 102, 60, 101, 98, 99, 107, 104, 97, 103, 61, 'all']

    #新上架實績台數
    NewUploadAct = []
    tmpA = df.loc[(df['維護業務ID'] == 90), :] #孫和莆
    tmpB = df.loc[(df['維護業務ID'] == 102), :] #邱雅凌
    tmpC = df.loc[(df['維護業務ID'] == 60), :] #陳玫君
    tmpD = df.loc[(df['維護業務ID'] == 101), :] #童嘉如
    tmpE = df.loc[(df['維護業務ID'] == 98), :] #詹朝順 
    tmpF = df.loc[(df['維護業務ID'] == 99), :] #黃煒凱 
    tmpG = df.loc[(df['維護業務ID'] == 107), :] #陳泰羽
    tmpH = df.loc[(df['維護業務ID'] == 104), :] #張可函
    tmpI = df.loc[(df['維護業務ID'] == 97), :] #林韋辰
    tmpJ = df.loc[(df['維護業務ID'] == 103), :] #黃宏智
    tmpK = df.loc[(df['維護業務ID'] == 61), :] #陳億業

    NewUploadAct.append(tmpA.sum()['新上架實績台數']) 
    NewUploadAct.append(tmpB.sum()['新上架實績台數'])
    NewUploadAct.append(tmpC.sum()['新上架實績台數']) 
    NewUploadAct.append(tmpD.sum()['新上架實績台數'])
    NewUploadAct.append(tmpE.sum()['新上架實績台數'])
    NewUploadAct.append(tmpF.sum()['新上架實績台數'])
    NewUploadAct.append(tmpG.sum()['新上架實績台數'])
    NewUploadAct.append(tmpH.sum()['新上架實績台數'])
    NewUploadAct.append(tmpI.sum()['新上架實績台數'])
    NewUploadAct.append(tmpJ.sum()['新上架實績台數'])
    NewUploadAct.append(tmpK.sum()['新上架實績台數'])
    totalA = tmpA.sum()['新上架實績台數'] + tmpB.sum()['新上架實績台數'] +tmpC.sum()['新上架實績台數']+tmpD.sum()['新上架實績台數']+tmpE.sum()['新上架實績台數']+tmpF.sum()['新上架實績台數']+tmpG.sum()['新上架實績台數']+tmpH.sum()['新上架實績台數']+tmpI.sum()['新上架實績台數']+tmpJ.sum()['新上架實績台數']+tmpK.sum()['新上架實績台數']
    NewUploadAct.append(totalA) #總和

    #新上架達成率
    NewUploadAR = []
    for i, v in enumerate(NewUploadTarget):
        NewUploadAR.append(NewUploadAct[i]/v)

    #淨增加目標台數
    # NewIncreaseTarget = [100, 80, 180, 100, 80, 100, 80, 64, 100, 120, 120, 1124]
    NewIncreaseTarget = targetSalesIncrease

    #淨增加實績台數
    NewIncreaseAct = []
    NewIncreaseAct.append(tmpA.sum()['本月淨增加(新)']) 
    NewIncreaseAct.append(tmpB.sum()['本月淨增加(新)'])
    NewIncreaseAct.append(tmpC.sum()['本月淨增加(新)']) 
    NewIncreaseAct.append(tmpD.sum()['本月淨增加(新)'])
    NewIncreaseAct.append(tmpE.sum()['本月淨增加(新)'])
    NewIncreaseAct.append(tmpF.sum()['本月淨增加(新)'])
    NewIncreaseAct.append(tmpG.sum()['本月淨增加(新)'])
    NewIncreaseAct.append(tmpH.sum()['本月淨增加(新)'])
    NewIncreaseAct.append(tmpI.sum()['本月淨增加(新)'])
    NewIncreaseAct.append(tmpJ.sum()['本月淨增加(新)'])
    NewIncreaseAct.append(tmpK.sum()['本月淨增加(新)'])        
    totalB = tmpA.sum()['本月淨增加(新)'] + tmpB.sum()['本月淨增加(新)'] +tmpC.sum()['本月淨增加(新)']+tmpD.sum()['本月淨增加(新)']+tmpE.sum()['本月淨增加(新)']+tmpF.sum()['本月淨增加(新)']+tmpG.sum()['本月淨增加(新)']+tmpH.sum()['本月淨增加(新)']+tmpI.sum()['本月淨增加(新)']+tmpJ.sum()['本月淨增加(新)']+tmpK.sum()['本月淨增加(新)']
    NewIncreaseAct.append(totalB) #總和

    #淨增加達成率
    NewIncreaseAR = []
    for i, v in enumerate(NewIncreaseTarget):
        NewIncreaseAR.append(NewIncreaseAct[i]/v)

    tmpANewMember = df.loc[(df['維護業務ID'] == 90) & (((df['最新儲值時間'] >= str(first_day)) & (df['最新儲值時間'] <= str(nextMonth_first_day))) | ((df['信用卡綁定時間'] >= str(first_day)) & (df['信用卡綁定時間'] <= str(nextMonth_first_day))) ), :] 
    tmpBNewMember = df.loc[(df['維護業務ID'] == 102) & (((df['最新儲值時間'] >= str(first_day)) & (df['最新儲值時間'] <= str(nextMonth_first_day))) | ((df['信用卡綁定時間'] >= str(first_day)) & (df['信用卡綁定時間'] <= str(nextMonth_first_day))) ), :] 
    tmpCNewMember = df.loc[(df['維護業務ID'] == 60) & (((df['最新儲值時間'] >= str(first_day)) & (df['最新儲值時間'] <= str(nextMonth_first_day))) | ((df['信用卡綁定時間'] >= str(first_day)) & (df['信用卡綁定時間'] <= str(nextMonth_first_day))) ), :] 
    tmpDNewMember = df.loc[(df['維護業務ID'] == 101) & (((df['最新儲值時間'] >= str(first_day)) & (df['最新儲值時間'] <= str(nextMonth_first_day))) | ((df['信用卡綁定時間'] >= str(first_day)) & (df['信用卡綁定時間'] <= str(nextMonth_first_day))) ), :] 
    tmpENewMember = df.loc[(df['維護業務ID'] == 98) & (((df['最新儲值時間'] >= str(first_day)) & (df['最新儲值時間'] <= str(nextMonth_first_day))) | ((df['信用卡綁定時間'] >= str(first_day)) & (df['信用卡綁定時間'] <= str(nextMonth_first_day))) ), :] 
    tmpFNewMember = df.loc[(df['維護業務ID'] == 99) & (((df['最新儲值時間'] >= str(first_day)) & (df['最新儲值時間'] <= str(nextMonth_first_day))) | ((df['信用卡綁定時間'] >= str(first_day)) & (df['信用卡綁定時間'] <= str(nextMonth_first_day))) ), :] 
    tmpGNewMember = df.loc[(df['維護業務ID'] == 107) & (((df['最新儲值時間'] >= str(first_day)) & (df['最新儲值時間'] <= str(nextMonth_first_day))) | ((df['信用卡綁定時間'] >= str(first_day)) & (df['信用卡綁定時間'] <= str(nextMonth_first_day))) ), :]    
    tmpHNewMember = df.loc[(df['維護業務ID'] == 104) & (((df['最新儲值時間'] >= str(first_day)) & (df['最新儲值時間'] <= str(nextMonth_first_day))) | ((df['信用卡綁定時間'] >= str(first_day)) & (df['信用卡綁定時間'] <= str(nextMonth_first_day))) ), :] 
    tmpINewMember = df.loc[(df['維護業務ID'] == 97) & (((df['最新儲值時間'] >= str(first_day)) & (df['最新儲值時間'] <= str(nextMonth_first_day))) | ((df['信用卡綁定時間'] >= str(first_day)) & (df['信用卡綁定時間'] <= str(nextMonth_first_day))) ), :] 
    tmpJNewMember = df.loc[(df['維護業務ID'] == 103) & (((df['最新儲值時間'] >= str(first_day)) & (df['最新儲值時間'] <= str(nextMonth_first_day))) | ((df['信用卡綁定時間'] >= str(first_day)) & (df['信用卡綁定時間'] <= str(nextMonth_first_day))) ), :] 
    tmpKNewMember = df.loc[(df['維護業務ID'] == 61) & (((df['最新儲值時間'] >= str(first_day)) & (df['最新儲值時間'] <= str(nextMonth_first_day))) | ((df['信用卡綁定時間'] >= str(first_day)) & (df['信用卡綁定時間'] <= str(nextMonth_first_day))) ), :] 

    #本月新增家數
    NewMember = []
    NewMember.append(tmpANewMember.count()['維護業務ID']) #孫和莆 
    NewMember.append(tmpBNewMember.count()['維護業務ID']) #邱雅凌 
    NewMember.append(tmpCNewMember.count()['維護業務ID']) #陳玫君 
    NewMember.append(tmpDNewMember.count()['維護業務ID']) #童嘉如
    NewMember.append(tmpENewMember.count()['維護業務ID']) #詹朝順 
    NewMember.append(tmpFNewMember.count()['維護業務ID']) #黃煒凱
    NewMember.append(tmpGNewMember.count()['維護業務ID']) #陳泰羽
    NewMember.append(tmpHNewMember.count()['維護業務ID']) #張可函
    NewMember.append(tmpINewMember.count()['維護業務ID']) #林韋辰
    NewMember.append(tmpJNewMember.count()['維護業務ID']) #黃宏智
    NewMember.append(tmpKNewMember.count()['維護業務ID']) #陳億業
    totalC = tmpANewMember.count()['維護業務ID'] + tmpBNewMember.count()['維護業務ID'] +tmpCNewMember.count()['維護業務ID']+tmpDNewMember.count()['維護業務ID']+tmpENewMember.count()['維護業務ID']+tmpFNewMember.count()['維護業務ID']+tmpGNewMember.count()['維護業務ID']+tmpHNewMember.count()['維護業務ID']+tmpINewMember.count()['維護業務ID']+tmpJNewMember.count()['維護業務ID']+tmpKNewMember.count()['維護業務ID']
    NewMember.append(totalC) #總和    

    #本月新會員新上台數
    NewMemberUpload = []
    NewMemberUpload.append(tmpANewMember.sum()['本月新上(新)']) #孫和莆 
    NewMemberUpload.append(tmpBNewMember.sum()['本月新上(新)']) #邱雅凌 
    NewMemberUpload.append(tmpCNewMember.sum()['本月新上(新)']) #陳玫君 
    NewMemberUpload.append(tmpDNewMember.sum()['本月新上(新)']) #童嘉如
    NewMemberUpload.append(tmpENewMember.sum()['本月新上(新)']) #詹朝順 
    NewMemberUpload.append(tmpFNewMember.sum()['本月新上(新)']) #黃煒凱
    NewMemberUpload.append(tmpGNewMember.sum()['本月新上(新)']) #陳泰羽
    NewMemberUpload.append(tmpHNewMember.sum()['本月新上(新)']) #張可函
    NewMemberUpload.append(tmpINewMember.sum()['本月新上(新)']) #林韋辰
    NewMemberUpload.append(tmpJNewMember.sum()['本月新上(新)']) #黃宏智
    NewMemberUpload.append(tmpKNewMember.sum()['本月新上(新)']) #陳億業
    totalD = tmpANewMember.sum()['本月新上(新)'] + tmpBNewMember.sum()['本月新上(新)'] +tmpCNewMember.sum()['本月新上(新)']+tmpDNewMember.sum()['本月新上(新)']+tmpENewMember.sum()['本月新上(新)']+tmpFNewMember.sum()['本月新上(新)']+tmpGNewMember.sum()['本月新上(新)']+tmpHNewMember.sum()['本月新上(新)']+tmpINewMember.sum()['本月新上(新)']+tmpJNewMember.sum()['本月新上(新)']+tmpKNewMember.sum()['本月新上(新)']
    NewMemberUpload.append(totalD) #總和    

    #店均獎金家數
    Bonus = []
    tmpABonus = df.loc[(df['維護業務ID'] == 90) & ((df['本月架上驗證成功車輛'] >= 20) & (df['本月新上(新)'] >=7 )), :]
    tmpBBonus = df.loc[(df['維護業務ID'] == 102) & ((df['本月架上驗證成功車輛'] >= 20) & (df['本月新上(新)'] >=7 )), :]
    tmpCBonus = df.loc[(df['維護業務ID'] == 60) & ((df['本月架上驗證成功車輛'] >= 20) & (df['本月新上(新)'] >=7 )), :]
    tmpDBonus = df.loc[(df['維護業務ID'] == 101) & ((df['本月架上驗證成功車輛'] >= 20) & (df['本月新上(新)'] >=7 )), :]
    tmpEBonus = df.loc[(df['維護業務ID'] == 98) & ((df['本月架上驗證成功車輛'] >= 20) & (df['本月新上(新)'] >=7 )), :]
    tmpFBonus = df.loc[(df['維護業務ID'] == 99) & ((df['本月架上驗證成功車輛'] >= 20) & (df['本月新上(新)'] >=7 )), :]
    tmpGBonus = df.loc[(df['維護業務ID'] == 107) & ((df['本月架上驗證成功車輛'] >= 20) & (df['本月新上(新)'] >=7 )), :]
    tmpHBonus = df.loc[(df['維護業務ID'] == 104) & ((df['本月架上驗證成功車輛'] >= 20) & (df['本月新上(新)'] >=7 )), :]
    tmpIBonus = df.loc[(df['維護業務ID'] == 97) & ((df['本月架上驗證成功車輛'] >= 20) & (df['本月新上(新)'] >=7 )), :]
    tmpJBonus = df.loc[(df['維護業務ID'] == 103) & ((df['本月架上驗證成功車輛'] >= 20) & (df['本月新上(新)'] >=7 )), :]
    tmpKBonus = df.loc[(df['維護業務ID'] == 61) & ((df['本月架上驗證成功車輛'] >= 20) & (df['本月新上(新)'] >=7 )), :]

    Bonus.append(tmpABonus.count()['維護業務ID']) #孫和莆 
    Bonus.append(tmpBBonus.count()['維護業務ID']) #邱雅凌 
    Bonus.append(tmpCBonus.count()['維護業務ID']) #陳玫君 
    Bonus.append(tmpDBonus.count()['維護業務ID']) #童嘉如
    Bonus.append(tmpEBonus.count()['維護業務ID']) #詹朝順 
    Bonus.append(tmpFBonus.count()['維護業務ID']) #黃煒凱
    Bonus.append(tmpGBonus.count()['維護業務ID']) #陳泰羽
    Bonus.append(tmpHBonus.count()['維護業務ID']) #張可函
    Bonus.append(tmpIBonus.count()['維護業務ID']) #林韋辰
    Bonus.append(tmpJBonus.count()['維護業務ID']) #黃宏智
    Bonus.append(tmpKBonus.count()['維護業務ID']) #陳億業
    totalE = tmpABonus.count()['維護業務ID'] + tmpBBonus.count()['維護業務ID'] +tmpCBonus.count()['維護業務ID']+tmpDBonus.count()['維護業務ID']+tmpEBonus.count()['維護業務ID']+tmpFBonus.count()['維護業務ID']+tmpGBonus.count()['維護業務ID']+tmpHBonus.count()['維護業務ID']+tmpIBonus.count()['維護業務ID']+tmpJBonus.count()['維護業務ID']+tmpKBonus.count()['維護業務ID']
    Bonus.append(totalE) #總和    

    # TL 新上架目標台數
    # 2022/03/01 新增TL ID & insert table (1, 2, 3, 4, 5)，張鈞偉、荊德龍、李明憲、陳叔毅、林士欽
    # TLNewUploadTarget = [600, 1300, 880, 550, 1000, 4330]
    TLNewUploadTarget = targetTLUpload

    # TL 新上架實績台數
    TLNewUploadAct = []
    TLNewUploadAct.append(tmpA.sum()['新上架實績台數'] + tmpB.sum()['新上架實績台數'])
    TLNewUploadAct.append(tmpC.sum()['新上架實績台數'] + tmpD.sum()['新上架實績台數'])
    TLNewUploadAct.append(tmpE.sum()['新上架實績台數'] + tmpF.sum()['新上架實績台數'] + tmpG.sum()['新上架實績台數'])
    TLNewUploadAct.append(tmpH.sum()['新上架實績台數'] + tmpI.sum()['新上架實績台數'])
    TLNewUploadAct.append(tmpJ.sum()['新上架實績台數'] + tmpK.sum()['新上架實績台數'])
    TLNewUploadAct.append(totalA) #總和
    

    # TL 新上架達成率
    TLNewUploadAR = []
    for i, v in enumerate(TLNewUploadTarget):
        TLNewUploadAR.append(TLNewUploadAct[i]/v)

    # TL 淨增加目標台數
    # TLNewIncreaseTarget = [180, 280, 260, 164, 240, 1124]
    TLNewIncreaseTarget = targetTLIncrease
    
    # TL 淨增加實績台數
    TLNewIncreaseAct = []
    TLNewIncreaseAct.append(tmpA.sum()['本月淨增加(新)'] + tmpB.sum()['本月淨增加(新)']) 
    TLNewIncreaseAct.append(tmpC.sum()['本月淨增加(新)'] + tmpD.sum()['本月淨增加(新)']) 
    TLNewIncreaseAct.append(tmpE.sum()['本月淨增加(新)'] + tmpF.sum()['本月淨增加(新)'] + tmpG.sum()['本月淨增加(新)']) 
    TLNewIncreaseAct.append(tmpH.sum()['本月淨增加(新)'] + tmpI.sum()['本月淨增加(新)']) 
    TLNewIncreaseAct.append(tmpJ.sum()['本月淨增加(新)'] + tmpK.sum()['本月淨增加(新)']) 
    TLNewIncreaseAct.append(totalB) #總和

    # TL 淨增加達成率
    TLNewIncreaseAR = []
    for i, v in enumerate(TLNewIncreaseTarget):
        TLNewIncreaseAR.append(TLNewIncreaseAct[i]/v)

    # TL 本月新增家數
    TLNewMember = []
    TLNewMember.append(tmpANewMember.count()['維護業務ID'] + tmpBNewMember.count()['維護業務ID'])
    TLNewMember.append(tmpCNewMember.count()['維護業務ID'] + tmpDNewMember.count()['維護業務ID'])
    TLNewMember.append(tmpENewMember.count()['維護業務ID'] + tmpFNewMember.count()['維護業務ID'] + tmpGNewMember.count()['維護業務ID'])
    TLNewMember.append(tmpHNewMember.count()['維護業務ID'] + tmpINewMember.count()['維護業務ID'])
    TLNewMember.append(tmpJNewMember.count()['維護業務ID'] + tmpKNewMember.count()['維護業務ID'])
    TLNewMember.append(totalC) #總和    

    # TL 本月新會員新上台數
    TLNewMemberUpload = []
    TLNewMemberUpload.append(tmpANewMember.sum()['本月新上(新)'] + tmpBNewMember.sum()['本月新上(新)']) 
    TLNewMemberUpload.append(tmpCNewMember.sum()['本月新上(新)'] + tmpDNewMember.sum()['本月新上(新)'])
    TLNewMemberUpload.append(tmpENewMember.sum()['本月新上(新)'] + tmpFNewMember.sum()['本月新上(新)'] + tmpGNewMember.sum()['本月新上(新)'])
    TLNewMemberUpload.append(tmpHNewMember.sum()['本月新上(新)'] + tmpINewMember.sum()['本月新上(新)'])
    TLNewMemberUpload.append(tmpJNewMember.sum()['本月新上(新)'] + tmpKNewMember.sum()['本月新上(新)'])
    TLNewMemberUpload.append(totalD) #總和
    

    df = pd.DataFrame(list(zip(NewUploadTarget, NewUploadAct, NewUploadAR, NewIncreaseTarget, NewIncreaseAct, NewIncreaseAR, NewMember, NewMemberUpload, Bonus)),
               columns =['新上架目標台數', '新上架實績台數', '新上架達成率', '淨增加目標台數', '淨增加實績台數', '淨增加達成率', '本月新增家數', '本月新會員新上台數', '店均獎金家數'])
    # df.insert(0, 'Date', end_date)

    df_down = pd.DataFrame(list(zip(TLNewUploadTarget, TLNewUploadAct, TLNewUploadAR, TLNewIncreaseTarget, TLNewIncreaseAct, TLNewIncreaseAR, TLNewMember, TLNewMemberUpload)),
               columns =['TL新上架目標台數', 'TL新上架實績台數', 'TL新上架達成率', 'TL淨增加目標台數', 'TL淨增加實績台數', 'TL淨增加達成率', 'TL本月新增家數', 'TL本月新會員新上台數'])

    return df, df_down

def addValueToExcel(ws_main, valueList, rowIndex, colIndex):
    for index, value in enumerate(valueList):
        grid = ws_main.cell(row=4+rowIndex+index, column=4+colIndex)
        grid.value = value

def splitThree(origin):
    return origin[0:13], origin[13:26], origin[26:39]

def subtract(list1, list2):
    difference = []   # initialization of result list

    for i in range(len(list1)):
        difference.append(int(list1[i]) - int(list2[i]))
        
    return difference

def send_to_db(dt, tbl_name):
    num = 1000
    loop_no = round(len(dt) / num) + 1
    for i in range(0, loop_no):
        data = dt[i * num:i * num + num]
        to_mysql('report', data, tbl_name, 'append')

def subThisMonthIncre(df):
    df['本月淨增加'] = df['本月新上架車輛'] - df['本月下架車輛']
    df['本月新上(新)'] = df['本月新上架車輛'] - df['2日內下架']
    df['本月下架(新)'] = df['當月下架未過戶'] + df['當月過戶未下架'] + df['當月過戶當月下架']
    df['本月淨增加(新)'] = df['本月新上(新)'] - df['本月下架(新)']

    return df

def changeGuildName(df):
    df['公會'] = df['公會'].replace(['台北市汽車商業同業公會','新北市汽車商業同業公會', '桃園市汽車商業同業公會', '苗栗縣汽車商業同業公會', '高雄市汽車商業同業公會', '台中市汽車商業同業公會', '基隆市汽車商業同業公會', '宜蘭縣汽車商業同業公會', '南投縣汽車商業同業公會', '台南市汽車商業同業公會', '臺南市直轄市汽車商業同業公會', '中華民國汽車商業同業公會全聯會', '臺中市直轄市汽車商業同業公會', '屏東縣汽車商業同業公會', '高雄市新汽車商業同業公會', '花蓮縣汽車商業同業公會', '嘉義市汽車商業同業公會'],['台北','新北', '桃園', '苗栗', '高雄', '台中', '基隆', '宜蘭', '南投', '台南', '台南市', '同業', '台中市', '屏東', '高雄(新)', '花蓮', '嘉義'])

    return df

if __name__ == "__main__":
    print(now)
    # 設定日期
    script = 'abccar_member_onshelf'

    try:
        memberAgree, target = ReadData()
        print('Part 1 done!')
    except Exception as e:
        msg = '{0}: PART 1: 數據連線讀取失敗! The error message : {1}'.format(script, e)
        send_error_message(script='{}'.format(script), message=msg)
        print(msg)
        sys.exit(1)
    
    try:
        memberAgree_new_tmp = addStageLastMonth(memberAgree)
        memberAgree_new = subThisMonthIncre(memberAgree_new_tmp)
        memberAgree_new = changeGuildName(memberAgree_new)
        print('Part 2 done!')
    except Exception as e:
        msg = '{0}: PART 2: 數據合併失敗! The error message : {1}'.format(script, e)
        send_error_message(script='{}'.format(script), message=msg)
        print(msg)
        sys.exit(1)

    try:
        df, df_down = processData(memberAgree_new, target)
        print('Part 3 done!')
    except Exception as e:
        msg = '{0}: PART 3: 數據計算出現錯誤! The error message : {1}'.format(script, e)
        send_error_message(script='{}'.format(script), message=msg)
        print(msg)
        sys.exit(1)
        
    try:
        #=========================== Load office file      
        wb = load_workbook('abc好車網_架上台數報表_企劃.xlsx')

        #Sheet 1
        ws_main = wb.worksheets[0]
        
        # Add today date
        date = ws_main.cell(row=2, column=3)
        date.value = 'abc好車網_架上台數報表_(' + end_date +')'

        #Up 
        #新上架目標台數
        addValueToExcel(ws_main, df['新上架目標台數'].tolist(), 0, 0)
        #新上架實績台數
        addValueToExcel(ws_main, df['新上架實績台數'].tolist(), 0, 1)       
        #新上架達成率
        addValueToExcel(ws_main, df['新上架達成率'].tolist(), 0, 2)
        #淨增加目標台數
        addValueToExcel(ws_main, df['淨增加目標台數'].tolist(), 0, 3)
        #淨增加實績台數
        addValueToExcel(ws_main, df['淨增加實績台數'].tolist(), 0, 4)
        #淨增加達成率
        addValueToExcel(ws_main, df['淨增加達成率'].tolist(), 0, 5)
        #本月新增家數
        addValueToExcel(ws_main, df['本月新增家數'].tolist(), 0, 6)
        #本月新會員新上台數
        addValueToExcel(ws_main, df['本月新會員新上台數'].tolist(), 0, 7)
        #店均獎金家數
        addValueToExcel(ws_main, df['店均獎金家數'].tolist(), 0, 8)

        #Down
        #TL新上架目標台數
        addValueToExcel(ws_main, df_down['TL新上架目標台數'].tolist(), 16, 1)
        #TL新上架實績台數
        addValueToExcel(ws_main, df_down['TL新上架實績台數'].tolist(), 16, 2)       
        #TL新上架達成率
        addValueToExcel(ws_main, df_down['TL新上架達成率'].tolist(), 16, 3)
        #TL淨增加目標台數
        addValueToExcel(ws_main, df_down['TL淨增加目標台數'].tolist(), 16, 4)
        #TL淨增加實績台數
        addValueToExcel(ws_main, df_down['TL淨增加實績台數'].tolist(), 16, 5)
        #TL淨增加達成率
        addValueToExcel(ws_main, df_down['TL淨增加達成率'].tolist(), 16, 6)
        #TL本月新增家數
        addValueToExcel(ws_main, df_down['TL本月新增家數'].tolist(), 16, 7)
        #TL本月新會員新上台數
        addValueToExcel(ws_main, df_down['TL本月新會員新上台數'].tolist(), 16, 8)

        #Sheet 2
        #取欄位
        df_Office = memberAgree_new[['MemberID', '會員名稱', '車商名稱', '城市別', '連絡人', '連絡電話', '身分別', '網路好店', '買萬送萬', '公會', '維護業務', 'HAA帳號', 'HOT主帳號', 'HOT官網認證台數', 'HOT官網總台數', '上月底架上台數', '本日新上架車輛' , '本日下架車輛', '本月新上架車輛', '2日內下架', '本月新上(新)', '本月下架車輛', '當月下架未過戶', '當月過戶未下架', '當月過戶當月下架', '本月下架(新)', '本月淨增加', '本月淨增加(新)', '本月架上車輛', '本月架上驗證成功車輛', 'HAA上架數', 'HAA待售數', '簡易刊登本月待售數', '簡易刊登本月上架數', '簡易刊登架上台數', '最新儲值時間', '信用卡綁定時間', '同意會員條款時間', '註冊時間']]

        ws = wb.worksheets[1]
        ws.delete_rows(2, 100000)

        n=1
        for r in dataframe_to_rows(df_Office, index=False, header=False):
            m = 1
            for i in r:
                grid = ws.cell(row=1+n, column=m)
                grid.value = i
                m+=1
            n +=1

        #Sheet 3
        df_Office_filter = df_Office[(df_Office['本月架上驗證成功車輛'] >= 15) | (df_Office['本月新上(新)'] >=4 )]
        ws = wb.worksheets[2]
        ws.delete_rows(2, 100000)

        n=1
        for r in dataframe_to_rows(df_Office_filter, index=False, header=False):
            m = 1
            for i in r:
                grid = ws.cell(row=1+n, column=m)
                grid.value = i
                m+=1
            n +=1

        # Save the worksheet
        wb.save('abc好車網_架上台數報表_企劃.xlsx')

        #=========================== Load sales file      
        wb = load_workbook('abc好車網_架上台數報表.xlsx')

        #Sheet 1
        ws_main = wb.worksheets[0]
        
        # Add today date
        date = ws_main.cell(row=1, column=1)
        date.value = 'abc好車網_架上台數報表_(' + end_date +')'

        #Up 
        #新上架目標台數
        addValueToExcel(ws_main, df['新上架目標台數'].tolist(), -1, -2)
        #新上架實績台數
        addValueToExcel(ws_main, df['新上架實績台數'].tolist(), -1, -1)       
        #新上架達成率
        addValueToExcel(ws_main, df['新上架達成率'].tolist(), -1, 0)
        #淨增加目標台數
        addValueToExcel(ws_main, df['淨增加目標台數'].tolist(), -1, 1)
        #淨增加實績台數
        addValueToExcel(ws_main, df['淨增加實績台數'].tolist(), -1, 2)
        #淨增加達成率
        addValueToExcel(ws_main, df['淨增加達成率'].tolist(), -1, 3)
        #本月新增家數
        addValueToExcel(ws_main, df['本月新增家數'].tolist(), -1, 4)
        #本月新會員新上台數
        addValueToExcel(ws_main, df['本月新會員新上台數'].tolist(), -1, 5)
        #店均獎金家數
        addValueToExcel(ws_main, df['店均獎金家數'].tolist(), -1, 6)

        #Down
        #TL新上架目標台數
        addValueToExcel(ws_main, df_down['TL新上架目標台數'].tolist(), 15, -1)
        #TL新上架實績台數
        addValueToExcel(ws_main, df_down['TL新上架實績台數'].tolist(), 15, 0)       
        #TL新上架達成率
        addValueToExcel(ws_main, df_down['TL新上架達成率'].tolist(), 15, 1)
        #TL淨增加目標台數
        addValueToExcel(ws_main, df_down['TL淨增加目標台數'].tolist(), 15, 2)
        #TL淨增加實績台數
        addValueToExcel(ws_main, df_down['TL淨增加實績台數'].tolist(), 15, 3)
        #TL淨增加達成率
        addValueToExcel(ws_main, df_down['TL淨增加達成率'].tolist(), 15, 4)
        #TL本月新增家數
        addValueToExcel(ws_main, df_down['TL本月新增家數'].tolist(), 15, 5)
        #TL本月新會員新上台數
        addValueToExcel(ws_main, df_down['TL本月新會員新上台數'].tolist(), 15, 6)

        # Save the worksheet
        wb.save('abc好車網_架上台數報表.xlsx')
        
        print('PART 4 all done!')
        
    except Exception as e:
        msg = '{0}: PART 4: 數據寫入出現錯誤! The error message : {1}'.format(script, e)
        send_daily_report_error(script='{}'.format(script), message=msg)
        print(msg)
        sys.exit(1)

    # try:
    #     send_daily_report_onself(receiver='g', path=path)
    #     msg = 'PART 5: 寄送架上台數OK'
    #     print(msg)
    # except Exception as e:
    #     msg = '{0}: PART 5: 寄送同意書日報發生錯誤. The error message : {1}'.format(script, e)
    #     send_daily_report_error(msg)
    #     print(msg)
    #     sys.exit(1)
