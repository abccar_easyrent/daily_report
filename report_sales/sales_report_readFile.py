
# coding: utf-8
import pandas as pd
import sys
sys.path.insert(0, '/home/hadoop/jessielu/')
#sys.path.insert(0, '/home/jessie/MyNotebook/daily_report/')

from Module.EmailFuncForSales import send_daily_report, send_daily_report_error
from openpyxl.styles import Border, Side, PatternFill, Font, GradientFill, Alignment
from openpyxl import Workbook, load_workbook
from datetime import date
import datetime
from Module.ConnFunc import read_mysql, read_testdb, to_mysql, read_test_mssql, read_mssql
from datetime import datetime, timedelta, date
from dateutil.relativedelta import relativedelta
import numpy

thick = Side(border_style="thick", color="000000")
thin = Side(border_style="thin", color="000000")

#path = '/home/jessie/MyNotebook/easyRent/scripts/'
path = '/home/hadoop/jessielu/report_sales/files/'

now = date.today()

#今年第一天
thisYear_first_day = date(now.year , 1, 1)

#每月一號的的計算邏輯要特別調整
if(now.day == 1):
    tmpDay = now - relativedelta(days=1)
    first_day = date(tmpDay.year, tmpDay.month, 1)
    tmpMonth = tmpDay - relativedelta(months=1)
    lastMonth_first_day = date(tmpMonth.year, tmpMonth.month, 1)
    dl = tmpDay - relativedelta(months=2)
    lfirst_day = date(dl.year, dl.month, 1)
    lastMonth_end_day = date(first_day.year, first_day.month, 1) - relativedelta(days=1)
else:
    #上個月的今天
    d = now - relativedelta(months=1)
    #這個月的第一天
    first_day = date(now.year, now.month, 1)
    #上個月的第一天
    lastMonth_first_day = date(d.year, d.month, 1)
    #上上個月的今天
    dl = now - relativedelta(months=2)
    #上上個月的第一天
    lfirst_day = date(dl.year, dl.month, 1)
    #上個月的最後一天
    lastMonth_end_day = date(first_day.year, first_day.month, 1) - relativedelta(days=1)
    

# 建立日期區間
# start_date = (datetime.today() - timedelta(2)).strftime("%Y-%m-%d") #前一天
start_date = lastMonth_first_day #上個月的第一天
end_date = (datetime.today() - timedelta(1)).strftime("%Y-%m-%d") #前一天

# In[30]:
def read_data():
    query1 = f"""
    SELECT 
	CONVERT (date, a.CreateDate) as CreateDate
	,b.[Quantity]
	,a.[OrderTotal]
	,a.[Salesperson]
	FROM [ABCCar].[dbo].[SOOrder] a WITH(NOLOCK)
	Join [ABCCar].[dbo].[SOOrderItem] b WITH(NOLOCK) ON a.[ID] = b.[SOOrderID]
	WHERE a.OrderStatusID = 30
	and a.PaymentStatusID = 30
	and a.AbcOrderID is NOT NULL
	and a.CreateDate between '{thisYear_first_day}' and '{now}'
    and a.[Salesperson] LIKE '-%'
    """
    
    query2 = f"""
    SELECT 
	o.OrderTotal,
	CONVERT (date, DATEADD(hour,8,o.CreatedOnUtc)) as CreateDate,
	o.CustomValueJson
    FROM [Order] o with (NOLOCK)
    JOIN [OrderItem] oi with (NOLOCK) ON oi.[OrderId] = o.Id
    JOIN [Product] p with (NOLOCK) ON p.[Id] = oi.[ProductId]
    WHERE
    (EXISTS(SELECT 1 FROM [Product_Category_Mapping] ccrm with (NOLOCK) inner join Product with (NOLOCK) on ccrm.[ProductId]=p.[Id] inner join Category cr with (NOLOCK) on cr.[Id]=ccrm.[CategoryId] WHERE cr.[Id] = 7838))
    and o.OrderStatusId = 30
	and o.PaymentStatusId = 30
    """
    grid_order = read_test_mssql(query1, 'ABCCar')
    ads_order = read_mssql(query2, 'ABCCarShop')
    
    return grid_order, ads_order

def calculateGridData(grid_order):
    
    #------------取得各區間資料
    #當日車格訂單
    gridOrderToday = grid_order[pd.to_datetime(grid_order['CreateDate'], format='%Y-%m-%d') == now - relativedelta(days=1)].groupby(['Salesperson']).sum()
    #當月車格訂單
    gridOrderMonth =  grid_order[(pd.to_datetime(grid_order['CreateDate'], format='%Y-%m-%d') < now) & (pd.to_datetime(grid_order['CreateDate'], format='%Y-%m-%d') >= first_day)].groupby(['Salesperson']).sum()
    #同期車格訂單
    gridOrderPeriod =  grid_order[(pd.to_datetime(grid_order['CreateDate'], format='%Y-%m-%d') < dl) & (pd.to_datetime(grid_order['CreateDate'], format='%Y-%m-%d') >= lfirst_day)].groupby(['Salesperson']).sum()
    #同年車格訂單
    gridOrderYear =  grid_order[pd.to_datetime(grid_order['CreateDate'], format='%Y-%m-%d') >= thisYear_first_day].groupby(['Salesperson']).sum()

    
    itemDic = {'-陳玫君(Maggie)-北區擔當':0, '-陳億業(肉粽)-南區擔當':0, '-方國淵(阿國)-雲嘉擔當':0, '-王彥彬(Roger)-南區擔當':0, '-其他':0, '-自主購買':0}

    #-------------車格數及車格業績
    QuantityListToday = [] #當日車格數 #return
    MoneyListToday = [] #當日車格業績價格
    QuantityListMonth = [] #當月累積車格數 #return
    QuantityListPeriod = [] #同期累計車格數 
    QuantityListYear = [] #同年累計車格數 #return

    GridMonthKPI = [1000, 1500, 300, 2000, '-', '-', 6000] #當月車格KPI {'陳玫君'、'陳億業'、'方國淵'、'王彥彬'、'客服'、'自主購買'、'總和'} #return
    GridMonthGrowRate = [] #車格月推進率 #return
    GridSamePeriodRate = [] #車格同期比 #return

    tmpQDicToday = itemDic.copy()
    tmpMDicToday = itemDic.copy() #return for 垂直表格
    tmpQDicMonth = itemDic.copy()
    tmpQDicPeriod = itemDic.copy()
    tmpQDicYear = itemDic.copy()

    #------------------取出每個row的數量
    for index, row in gridOrderToday.iterrows():
        if(index in tmpQDicToday):
            tmpQDicToday[index] = row['Quantity']
            tmpMDicToday[index] = row['OrderTotal']

    for index, row in gridOrderMonth.iterrows():
        if(index in tmpQDicMonth):
            tmpQDicMonth[index] = row['Quantity']

    for index, row in gridOrderPeriod.iterrows():
        if(index in tmpQDicPeriod):
            tmpQDicPeriod[index] = row['Quantity']

    for index, row in gridOrderYear.iterrows():
        if(index in tmpQDicYear):
            tmpQDicYear[index] = row['Quantity']
    
    #------------------放入excel需要的list
    summ = 0
    for value in tmpQDicToday:
        summ += int(tmpQDicToday[value])
        QuantityListToday.append(int(tmpQDicToday[value]))
    QuantityListToday.append(summ)

#     summ = 0
#     for value in tmpMDicToday:
#         summ += int(tmpMDicToday[value])
#         MoneyListToday.append(int(tmpMDicToday[value]))
#     MoneyListToday.append(summ)

    summ = 0
    for value in tmpQDicMonth:
        summ += int(tmpQDicMonth[value])
        QuantityListMonth.append(int(tmpQDicMonth[value]))
    QuantityListMonth.append(summ)

    summ = 0
    for value in tmpQDicPeriod:
        summ += int(tmpQDicPeriod[value])
        QuantityListPeriod.append(int(tmpQDicPeriod[value]))
    QuantityListPeriod.append(summ)

    summ = 0
    for value in tmpQDicYear:
        summ += int(tmpQDicYear[value])
        QuantityListYear.append(int(tmpQDicYear[value]))
    QuantityListYear.append(summ)
    
    #--------------------計算月推進率    
    for idx, value in enumerate(GridMonthKPI) :
        if(idx == 4 or idx == 5):
            GridMonthGrowRate.append(0)
        else:
            GridMonthGrowRate.append(str(int((QuantityListMonth[idx]/value)*100))+'%')
    
    #--------------------計算同期比
    for idx, value in enumerate(QuantityListPeriod):
        if(value == 0):
            GridSamePeriodRate.append('-')
        else:
            GridSamePeriodRate.append(str(int((QuantityListMonth[idx]/value)*100))+'%')
    
    return QuantityListToday, tmpMDicToday, QuantityListMonth, QuantityListYear, GridMonthKPI, GridMonthGrowRate, GridSamePeriodRate


def calculateAdsData(ads_order):
    
    #取出sales名稱
    sales = []
    for i in ads_order['CustomValueJson']:
        sales.append(i[-3])
    ads_order['Sales'] = sales
    
    #------------取得各區間資料
    #當日廣宣訂單
    adsOrderToday = ads_order[pd.to_datetime(ads_order['CreateDate'], format='%Y-%m-%d') == now - relativedelta(days=1)].groupby(['Sales']).sum().rename(index={'1': '陳億業', '3': '陳玫君', '4': '客服', '2': '陳宗德', '5': '林柏伸', '6': '李沅潔', '0': '自主購買'})
    #當月廣宣訂單
    adsOrderMonth =  ads_order[(pd.to_datetime(ads_order['CreateDate'], format='%Y-%m-%d') < now) & (pd.to_datetime(ads_order['CreateDate'], format='%Y-%m-%d') >= first_day)].groupby(['Sales']).sum().rename(index={'1': '陳億業', '3': '陳玫君', '4': '客服', '2': '陳宗德', '5': '林柏伸', '6': '李沅潔', '0': '自主購買'})
    #同期廣宣訂單
    adsOrderPeriod =  ads_order[(pd.to_datetime(ads_order['CreateDate'], format='%Y-%m-%d') < dl) & (pd.to_datetime(ads_order['CreateDate'], format='%Y-%m-%d') >= lfirst_day)].groupby(['Sales']).sum().rename(index={'1': '陳億業', '3': '陳玫君', '4': '客服', '2': '陳宗德', '5': '林柏伸', '6': '李沅潔', '0': '自主購買'})
    #同年廣宣訂單
    adsOrderYear =  ads_order[pd.to_datetime(ads_order['CreateDate'], format='%Y-%m-%d') >= thisYear_first_day].groupby(['Sales']).sum().rename(index={'1': '陳億業', '3': '陳玫君', '4': '客服', '2': '陳宗德', '5': '林柏伸', '6': '李沅潔', '0': '自主購買'})

    
    itemDicAds = {'陳玫君':0, '陳億業':0, '方國淵':0, '王彥彬':0, '客服':0, '自主購買':0}
    
    #-------------廣宣營收
    AdsListToday = [] #當日廣宣營收 #return 
    AdsListMonth = [] #當月累積廣宣營收 #return
    AdsListPeriod = [] #同期累計廣宣營收 
    AdsListYear = [] #同年累計廣宣營收 #return

    AdsMonthKPI = [20000, 20000, 20000, 20000, '-', '-', 80000] #當月廣宣營收KPI {'陳玫君'、'陳億業'、'方國淵'、'王彥彬'、'客服'、'自主購買'、'總和'} #return
    AdsMonthGrowRate = [] #廣宣營收月推進率 #return
    AdsSamePeriodRate = [] #廣宣營收同期比 #return

    tmpQDicTodayAds = itemDicAds.copy() #return for 垂直表格
    tmpQDicMonthAds = itemDicAds.copy()
    tmpQDicPeriodAds = itemDicAds.copy()
    tmpQDicYearAds = itemDicAds.copy()

    #------------------取出每個row的數量

    for index, row in adsOrderToday.iterrows():
        if(index in tmpQDicTodayAds):
            tmpQDicTodayAds[index] = row['OrderTotal']

    for index, row in adsOrderMonth.iterrows():
        if(index in tmpQDicMonthAds):
            tmpQDicMonthAds[index] = row['OrderTotal']

    for index, row in adsOrderPeriod.iterrows():
        if(index in tmpQDicPeriodAds):
            tmpQDicPeriodAds[index] = row['OrderTotal']

    for index, row in adsOrderYear.iterrows():
        if(index in tmpQDicYearAds):
            tmpQDicYearAds[index] = row['OrderTotal']
        
    #------------------放入excel需要的list
    summ = 0
    for value in tmpQDicTodayAds:
        summ += int(tmpQDicTodayAds[value])
        AdsListToday.append(int(tmpQDicTodayAds[value]))
    AdsListToday.append(summ)

    summ = 0
    for value in tmpQDicMonthAds:
        summ += int(tmpQDicMonthAds[value])
        AdsListMonth.append(int(tmpQDicMonthAds[value]))
    AdsListMonth.append(summ)

    summ = 0
    for value in tmpQDicPeriodAds:
        summ += int(tmpQDicPeriodAds[value])
        AdsListPeriod.append(int(tmpQDicPeriodAds[value]))
    AdsListPeriod.append(summ)

    summ = 0
    for value in tmpQDicYearAds:
        summ += int(tmpQDicYearAds[value])
        AdsListYear.append(int(tmpQDicYearAds[value]))
    AdsListYear.append(summ)

    #--------------------計算月推進率  
    for idx, value in enumerate(AdsMonthKPI) :
        if(idx == 4 or idx == 5):
            AdsMonthGrowRate.append('-')
        else:
            AdsMonthGrowRate.append(str(int((AdsListMonth[idx]/value)*100))+'%')
        
    #--------------------計算同期比
    for idx, value in enumerate(AdsListPeriod):
        if(value == 0):
            AdsSamePeriodRate.append('-')
        else:
            AdsSamePeriodRate.append(str(int((AdsListMonth[idx]/value)*100))+'%')
    
    return AdsListToday, AdsListMonth, AdsListYear, AdsMonthKPI, AdsMonthGrowRate, AdsSamePeriodRate, tmpQDicTodayAds

def calPerSales(tmpMDicToday, tmpQDicTodayAds):
    A = [tmpMDicToday['-陳玫君(Maggie)-北區擔當'], tmpQDicTodayAds['陳玫君']]
    B = [tmpMDicToday['-陳億業(肉粽)-南區擔當'], tmpQDicTodayAds['陳億業']]
    C = [tmpMDicToday['-方國淵(阿國)-雲嘉擔當'], tmpQDicTodayAds['方國淵']]
    D = [tmpMDicToday['-王彥彬(Roger)-南區擔當'], tmpQDicTodayAds['王彥彬']]
    
    return A, B, C, D


# 加入車格數
# valueList 一組為一個column
def carGridNumber(ws_main, valueList):
#     valueList = [[11, 12, 5, 2, 10, 20], [11, 12, 5, 2, 10, 20], [11, 12, 5, 2, 10, 20], [11, 12, 5, 2, 10, 20], [11, 12, 5, 2, 10, 20], [11, 12, 5, 2, 10, 20], [11, 12, 5, 2, 10, 20]]

    for index, col in enumerate(valueList):
        for indexRow, row in enumerate(col):
            if(type(row) == int):
                formatVal = "{:,d}".format(row)
            else:
                formatVal = row
            left_t = ws_main.cell(row=5+indexRow, column=3+index)
            left_t.value = formatVal


# In[31]:


# 加入廣宣營收
# valueList 一組為一個column
def adIncome(ws_main, valueList):
#     valueList = [[11, 12, 5, 2, 10, 20], [11, 12, 5, 2, 10, 20], [11, 12, 5, 2, 10, 20], [11, 12, 5, 2, 10, 20], [11, 12, 5, 2, 10, 20], [11, 12, 5, 2, 10, 20], [11, 12, 5, 2, 10, 20]]

    for index, col in enumerate(valueList):
        for indexRow, row in enumerate(col):
            if(type(row) == int):
                formatVal = "{:,d}".format(row)
            else:
                formatVal = row
            left_t = ws_main.cell(row=5+indexRow, column=9+index)
            left_t.value = formatVal


# 每天新增個別業務表格
# valueList 一天為一個row
def addDailyPerSalesByDay(ws_main, valueList, salesIndex):
#     valueList = [1, 2, 3]

    for index, value in enumerate(valueList):
        if((type(value) == int) or (type(value) == numpy.float64)):
            formatVal = "{:,d}".format(int(value))
        else:
            formatVal = value
            
        if(now.day == 1):
            newDay = now - relativedelta(days=1)
            left_t = ws_main.cell(row=13+newDay.day, column=salesIndex+index)
        else:
            left_t = ws_main.cell(row=12+now.day, column=salesIndex+index)
            
        left_t.border = Border(top=thin, left=thin, bottom=thin, right=thin)
        left_t.alignment = Alignment(horizontal="center", vertical="center")
        left_t.value = formatVal
    
    if(now.day == 1):
        newDay = now - relativedelta(days=1)
        date = ws_main.cell(row=13+newDay.day, column=salesIndex-1)    
    else:
        date = ws_main.cell(row=12+now.day, column=salesIndex-1)
        
    date.border = Border(top=thin, left=thin, bottom=thin, right=thin)
    date.alignment = Alignment(horizontal="center", vertical="center")
    date.value = now - relativedelta(days=1)
    
#     ws_main.column_dimensions['A'].width = 13
    
def makeReport(QuantityListToday, QuantityListMonth, QuantityListYear, GridMonthKPI, GridMonthGrowRate, GridSamePeriodRate, AdsListToday, AdsListMonth, AdsListYear, AdsMonthKPI, AdsMonthGrowRate, AdsSamePeriodRate, A, B, C, D):
    
    
    #--------------把各別list組成車格、廣宣--------------
    
    # Load excel 
    wb = load_workbook(path + 'abc業績日報表.xlsx')

    # Select the first work sheet
    ws_main = wb.worksheets[0]
    
    # Clear sheets when meet second day every month
    if(now.day == 2):
        for row in ws_main['A14:O45']:
            for cell in row:
                cell.value = None
                cell.border = None

    # Add today date
    date = ws_main.cell(row=4, column=2)
    date.value = now - relativedelta(days=1)

    gridValueList = [QuantityListToday, QuantityListMonth, GridMonthKPI, GridMonthGrowRate, GridSamePeriodRate, QuantityListYear]
    adsValueList = [AdsListToday, AdsListMonth, AdsMonthKPI, AdsMonthGrowRate, AdsSamePeriodRate, AdsListYear]
        
    # Insert date in three block
    carGridNumber(ws_main, gridValueList)
    adIncome(ws_main, adsValueList)
#     goodStore(ws_main, valueList)

    addDailyPerSalesByDay(ws_main, A, 2)
    addDailyPerSalesByDay(ws_main, B, 6)
    addDailyPerSalesByDay(ws_main, C, 10)
    addDailyPerSalesByDay(ws_main, D, 14)

    # Save the worksheet
    wb.save(path + 'abc業績日報表.xlsx')
    


# In[43]:
if __name__ == "__main__":
    
    try:
        grid_order, ads_order = read_data()
        msg = 'PART 1: 讀取數據成功!'
        print(msg)
    except Exception as e:
        msg = 'PART 1: 讀取數據發生錯誤. The error message : {}'.format(e)
        send_daily_report_error(msg)
        print(msg)
        sys.exit(1)
    
    try:
        QuantityListToday, tmpMDicToday, QuantityListMonth, QuantityListYear, GridMonthKPI, GridMonthGrowRate, GridSamePeriodRate = calculateGridData(grid_order)
        AdsListToday, AdsListMonth, AdsListYear, AdsMonthKPI, AdsMonthGrowRate, AdsSamePeriodRate, tmpQDicTodayAds = calculateAdsData(ads_order)
        msg = 'PART 2: 數據計算成功!'
        print(msg)
        
    except Exception as e:
        msg = 'PART 2: 數據計算發生錯誤. The error message : {}'.format(e)
        send_daily_report_error(msg)
        print(msg)
        sys.exit(1)

    try:        
        A, B, C, D = calPerSales(tmpMDicToday, tmpQDicTodayAds)
        msg = 'PART 3: 個別業務數據計算成功!'
        print(msg)
        
    except Exception as e:
        msg = 'PART 3: 個別業務數據計算發生錯誤. The error message : {}'.format(e)
        send_daily_report_error(msg)
        print(msg)
        sys.exit(1)
        
    try:        
        makeReport(QuantityListToday, QuantityListMonth, QuantityListYear, GridMonthKPI, GridMonthGrowRate, GridSamePeriodRate, AdsListToday, AdsListMonth, AdsListYear, AdsMonthKPI, AdsMonthGrowRate, AdsSamePeriodRate, A, B, C, D)
        msg = 'PART 4: 報表加入成功!'
        print(msg)
        
    except Exception as e:
        msg = 'PART 4: 報表加入發生錯誤. The error message : {}'.format(e)
        send_daily_report_error(msg)
        print(msg)
        sys.exit(1)
        
    try:
        send_daily_report(receiver='g', path=path)
        msg = 'PART 5: 寄送業務日報OK'
        print(msg)
    except Exception as e:
        msg = 'PART 5: 寄送業務日報發生錯誤. The error message : {}'.format(e)
        send_daily_report_error(msg)
        print(msg)
        sys.exit(1)
