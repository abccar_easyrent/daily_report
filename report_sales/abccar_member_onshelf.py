# coding: utf-8
import sys
import os
import numpy as np

# sys.path.insert(0, '/home/jessie/MyNotebook/daily_report/')
sys.path.insert(0, '/home/hadoop/jessielu/')

from Module.EmailFuncForSales import send_daily_report_onself, send_daily_report_error, send_error_message
from Module.ConnFunc import read_mssql, read_test_mssql, to_mysql, read_mysql
from Module.HelpFunc import display_set

from openpyxl.styles import Border, Side, PatternFill, Font, GradientFill, Alignment
from openpyxl import Workbook, load_workbook
from openpyxl.utils.dataframe import dataframe_to_rows

import pandas as pd
from datetime import datetime, timedelta, date
from dateutil.relativedelta import relativedelta  
import math

#原本
now = date.today() + relativedelta(days=1)
#補發用
# now = date.today()

if(now.day == 1):
    tmpDay = now - relativedelta(days=1)
    #這個月的第一天
    first_day = date(tmpDay.year, tmpDay.month, 1) 
    #上個月的最後一天
    lastMonth_end_day = date(first_day.year, first_day.month, 1) - relativedelta(days=1)
    #下個月的第一天
    nextMonth_first_day = first_day + relativedelta(months=1)
    #這個月最後一天
    thisMonth_end_day = nextMonth_first_day - relativedelta(days=1)

else:
    #這個月的第一天
    first_day = date(now.year, now.month, 1)
    #上個月的最後一天
    lastMonth_end_day = date(first_day.year, first_day.month, 1) - relativedelta(days=1)
    #下個月的第一天
    nextMonth_first_day = first_day + relativedelta(months=1)
    #這個月最後一天
    thisMonth_end_day = nextMonth_first_day - relativedelta(days=1)

#昨天
end_date = (now - timedelta(1)).strftime("%Y-%m-%d") 
#前天
the_day_before_yesterday_date = (now - timedelta(2)).strftime("%Y-%m-%d") 

display_set(200, 30)
path = '/home/hadoop/jessielu/report_sales/files/'
# path = '/home/jessie/MyNotebook/easyRent/scripts/'

os.chdir(path)

print(nextMonth_first_day)

# 讀取數據
def ReadData():

    #每月月初，要更新月底檔案
    if(now.day == 1):
        stageLastMonth = f"""
            SELECT 
                m.[MemberID],
                ISNULL((SELECT COUNT(1) FROM [Car] WITH (NOLOCK) WHERE [MemberID] = m.MemberID AND [Status] = 1  and GetDate() Between [Car].[StartDate] And [Car].[EndDate]), 0) as [上月底架上台數]
                --ISNULL((SELECT COUNT(1) FROM [Car] WITH (NOLOCK) WHERE [MemberID] = m.MemberID AND [Status] = 1 AND [EndDate] >=  '{end_date} 23:59:59'), 0) as [上月底架上台數]
            FROM [ABCCar].[dbo].Member m
            LEFT JOIN [ABCCarMemberCenter].[dbo].[Member] mc WITH (NOLOCK) ON m.[MemberID] = mc.[ID]
            LEFT JOIN [ABCCar].[dbo].[MemberAgree] ma WITH (NOLOCK) ON m.[MemberID] = ma.[MemberID]
            LEFT JOIN [ABCCar].[dbo].[BindingCardMember] bm WITH (NOLOCK) ON m.[MemberID] = bm.[MemberID] AND bm.[Status] = 0
            LEFT JOIN [ABCCar].[dbo].[MemberUserAccountSalesMapping] mua WITH (NOLOCK) ON m.[MemberID] = mua.[MemberID]
            LEFT JOIN [ABCCar].[dbo].[UserAccountSales] ua WITH (NOLOCK) ON ua.[ID] = mua.[UserAccountSalesID]
            WHERE 
            (EXISTS
                (SELECT * FROM MemberAgree WITH (NOLOCK)
                WHERE [MemberID] = m.MemberID)
            OR EXISTS
                (SELECT * FROM [ABCCar].[dbo].[Car] car 
                JOIN [dbo].[CarModel] carModel WITH (NOLOCK) ON carModel.[IsEnable] = 1 
                            AND car.[BrandID] = carModel.[BrandID] 
                            AND car.[SeriesID]= carModel.[SeriesID] 
                            AND car.[CategoryID]= carModel.[CategoryID]
                JOIN [ABCCar].[dbo].[Member] member WITH(NOLOCK)  ON car.[MemberID] = CAST(member.MemberID AS bigint)
                            AND member.[Status] = 1
                            AND member.[Category] IN (2,3,5) --個人賣家、車商、經紀人
                WHERE car.[Status] IN (0, 1) --待售
                AND car.[ModifyDate] >= '2018-06-20'
                AND car.MemberID = m.MemberID)) 
            AND m.[Category] > 1
            AND bm.BindingDate IS NOT NULL
            ORDER BY m.MemberID
        """
        # 執行讀取程式
        stageOnself = read_mssql(stageLastMonth, 'ABCCar')
        #儲存當月月底的資料
        stageOnself.to_csv('stageLastMonth_'+ str(end_date) +'.csv')
        print('output done!')

    memberAgree_sg = f"""
        SELECT 
        m.MemberID
        ,ISNULL(mc.[Name], '') as [會員名稱]
        ,ISNULL(m.[CarDealerName], '') as [車商名稱]
        --,ISNULL(CONCAT(mc.[CountryName], mc.[DistrictName]), '') AS [城市別]
        ,CASE WHEN (m.[CarDealerCountryName] is null or m.[CarDealerCountryName] = '') THEN CONCAT(mc.[CountryName], mc.[DistrictName]) ELSE CONCAT(m.[CarDealerCountryName], m.[CarDealerDistrictName]) END as [城市別]
        ,ISNULL(m.[ContactPerson], '') AS [連絡人]
        ,ISNULL(mc.[CellPhone], '') AS [連絡電話]
        ,ISNULL(m.[TaxID], '') AS [統編]
        ,ISNULL(m.[HOTMemberID], '') AS [HOT會員編號]

        -- ,IIF(m.[Source] = 'HAA' , m.[SourceKey], '') AS [HAA會員]
        
        ,(CASE(m.[Category]) 
            WHEN 1 THEN '一般買家' 
            WHEN 2 THEN '一般賣家' 
            WHEN 3 THEN '車商' 
            WHEN 4 THEN '原廠' 
            ELSE '買賣顧問' 
        END ) AS [身分別]
        ,IIF(m.[VIPLevel] = 0 , '', '是') AS [網路好店]
        ,IIF(pd.ID IS NULL, '', '是') AS [買萬送萬]
        ,ISNULL(kvd.[Name], '') AS [公會]
		,ISNULL(ua.[Name], '') AS [維護業務]
        ,ISNULL(ua.[NameSeq], '') AS [維護業務序號]
		,ISNULL(ua.[ID], '') AS [維護業務ID]
        ,IIF(m.[Source] = 'HAA' and RIGHT(m.[SourceKey], 1) <> 'A', m.[SourceKey], '') AS [HAA帳號]
        --[上月底架上台數]--每月底當日產生,其他以上月底資料為資料
        --,ISNULL((SELECT COUNT(1) FROM [Car] WITH (NOLOCK) WHERE [MemberID] = m.MemberID AND [Status] = 1 AND [StartDate] BETWEEN '2021-03-01 00:00:00' AND '2021-04-01 00:00:00'), 0) as [上月底架上台數]
        
        --[本日新上架車輛]--GridTrans.Type=202,CreateDate=今天 == 本日新上
        -- 需要另外抓上架兩天以後的值
        --,ISNULL((SELECT COUNT(1) FROM (SELECT CarID FROM [GridTrans] WITH (NOLOCK) WHERE [MemberID] = m.MemberID AND [GridTranTypeID] = 202 AND [CreateDate] BETWEEN '{end_date} 00:00:00' AND '{now} 00:00:00' AND DATEDIFF(day, [Car].[StartDate], [Car].[EndDate])> 2 GROUP BY CarID) as tmp1), 0) as [本日新上架車輛]
        ,ISNULL((SELECT COUNT(1) FROM (SELECT CarID FROM [GridTrans] WITH (NOLOCK) WHERE [MemberID] = m.MemberID AND [GridTranTypeID] = 202 AND [CreateDate] BETWEEN '{end_date} 00:00:00' AND '{now} 00:00:00' GROUP BY CarID) as tmp1), 0) as [本日新上架車輛]

        --[本日下架車輛]--GridTrans.Type=201,CreateDate=今天 == 本日下架
        ,ISNULL((SELECT COUNT(1) FROM (SELECT CarID FROM [GridTrans] WITH (NOLOCK) WHERE [MemberID] = m.MemberID AND [GridTranTypeID] = 201 AND [CreateDate] BETWEEN '{end_date} 00:00:00' AND '{now} 00:00:00' GROUP BY CarID) as tmp2), 0) as [本日下架車輛]

        --[本月新上架車輛]--GridTrans.Type=202,CreateDate=本月 == 本月新上
        ,ISNULL((SELECT COUNT(1) FROM (SELECT CarID FROM [GridTrans] WITH (NOLOCK) WHERE [MemberID] = m.MemberID AND [GridTranTypeID] = 202 AND [CreateDate] BETWEEN '{first_day} 00:00:00' AND '{nextMonth_first_day} 00:00:00' GROUP BY CarID) as tmp3), 0) as [本月新上架車輛]

        --[本月下架車輛]--GridTrans.Type=201,CreateDate=本月 == 本日下架
        ,ISNULL((SELECT COUNT(1) FROM (SELECT CarID FROM [GridTrans] WITH (NOLOCK) WHERE [MemberID] = m.MemberID AND [GridTranTypeID] = 201 AND [CreateDate] BETWEEN '{first_day} 00:00:00' AND '{nextMonth_first_day} 00:00:00' GROUP BY CarID) as tmp4), 0) as [本月下架車輛]

        --[架上車輛]--[Status] = 1 == 本月架上
        ,ISNULL((SELECT COUNT(1) FROM [ABCCar].[dbo].[Car] WITH (NOLOCK) WHERE [MemberID] = m.MemberID AND [Status] = 1 and GetDate() Between [Car].[StartDate] And [Car].[EndDate]), 0) as [本月架上車輛]
        
        --[車輛已驗證]--[Status] = 1 == 本月架上驗證成功
        ,ISNULL((SELECT COUNT(1) FROM [ABCCar].[dbo].[Car] WITH (NOLOCK) WHERE [MemberID] = m.MemberID AND [Status] = 1　AND [VehicleNoCheckStatus] = 1 AND GetDate() Between [Car].[StartDate] And [Car].[EndDate]), 0) as [本月架上驗證成功車輛]
        --HAA待售數
        ,ISNULL((SELECT COUNT(1) FROM [ABCCar].[dbo].[Car] WITH (NOLOCK) WHERE [MemberID] = m.MemberID AND [Status] = 0 AND [ModifyDate] >= '2018-06-20' AND [Source] IS NOT NULL AND [Source] = 'HAA'), 0) as [外部車源待售數]
        --HAA上架數
        ,ISNULL((SELECT COUNT(1) FROM [ABCCar].[dbo].[Car] WITH (NOLOCK) WHERE [MemberID] = m.MemberID AND [Status] = 1 AND [ModifyDate] >= '2018-06-20' AND [Source] IS NOT NULL AND [Source] = 'HAA'), 0) as [外部車源上架數]
        
        ,ISNULL((SELECT COUNT(1) FROM [ABCCar].[dbo].[Car] WITH (NOLOCK) WHERE [MemberID] = m.MemberID AND [Status] = 0 AND [ModifyDate] >= '2018-06-20' AND [CreateDate] BETWEEN '{first_day} 00:00:00' AND '{nextMonth_first_day} 00:00:00' AND [Source] IS NOT NULL AND ISNUMERIC([Source]) > 0), 0) as [簡易刊登本月待售數]
        ,ISNULL((SELECT COUNT(1) FROM [ABCCar].[dbo].[Car] WITH (NOLOCK) WHERE [MemberID] = m.MemberID AND [Status] = 1 AND [ModifyDate] >= '2018-06-20' AND [CreateDate] BETWEEN '{first_day} 00:00:00' AND '{nextMonth_first_day} 00:00:00' AND [Source] IS NOT NULL AND ISNUMERIC([Source]) > 0), 0) as [簡易刊登本月上架數]
        ,ISNULL((SELECT COUNT(1) FROM [ABCCar].[dbo].[Car] WITH (NOLOCK) WHERE [MemberID] = m.MemberID AND [Status] = 1 AND [ModifyDate] >= '2018-06-20' AND [Source] IS NOT NULL AND ISNUMERIC([Source]) > 0), 0) as [簡易刊登架上台數]

        ,ISNULL((SELECT TOP 1 IIF(a1.[UserAccountSalesID] < 60, '', a2.[ID]) FROM ABCCar.dbo.[MemberAgree] a1 WITH (NOLOCK) LEFT JOIN ABCCar.dbo.[UserAccountSales] a2 WITH (NOLOCK) on a1.[UserAccountSalesID] = a2.[ID] where a1.AgreeID = 1 AND a1.MemberId = m.[MemberID]), '') AS [會員同意條款維護業務人員ID]
        ,CASE WHEN ISNULL(ph.[ID], 0) > 0 THEN ph.CreateDate ELSE NULL END AS [最新儲值時間]
        ,bm.BindingDate as [信用卡綁定時間]
        ,ma.CreateDate as [同意會員條款時間]
        ,CONVERT(varchar(100), mc.[CreateDate], 120) AS [註冊時間]
        ,ISNULL((SELECT COUNT(1) FROM [Car] WITH (NOLOCK) WHERE [MemberID] = m.MemberID AND [Status] NOT IN (1,0) AND [TransferCheckId] <= 0 AND [EndDate] BETWEEN '{first_day} 00:00:00' AND '{nextMonth_first_day} 00:00:00'), 0) as [當月下架未過戶]
        ,ISNULL((SELECT COUNT(1) FROM [Car] WITH (NOLOCK) WHERE [MemberID] = m.MemberID AND [Status] = 1 AND [TransferCheckId] > 0 AND [VehicleNoCheckLastTime] BETWEEN '{first_day} 00:00:00' AND '{nextMonth_first_day} 00:00:00' ), 0) as [當月過戶未下架]
        ,ISNULL((SELECT COUNT(1) FROM [Car] WITH (NOLOCK) WHERE [MemberID] = m.MemberID AND [Status] NOT IN (1,0) AND [TransferCheckId] > 0 AND [VehicleNoCheckLastTime] BETWEEN '{first_day} 00:00:00' AND '{nextMonth_first_day} 00:00:00' AND [EndDate] BETWEEN '{first_day} 00:00:00' AND '{nextMonth_first_day} 00:00:00' ), 0) as [當月過戶當月下架]
        FROM [ABCCar].[dbo].Member m WITH (NOLOCK)
        LEFT JOIN [ABCCarMemberCenter].[dbo].[Member] mc WITH (NOLOCK) ON m.[MemberID] = mc.[ID]
        LEFT JOIN [ABCCar].[dbo].[MemberAgree] ma WITH (NOLOCK) ON m.[MemberID] = ma.[MemberID]
        LEFT JOIN [ABCCar].[dbo].[BindingCardMember] bm WITH (NOLOCK) ON m.[MemberID] = bm.[MemberID] AND bm.[Status] in (0,99)
        LEFT JOIN [ABCCar].[dbo].[MemberUserAccountSalesMapping] mua WITH (NOLOCK) ON m.[MemberID] = mua.[MemberID]
        LEFT JOIN [ABCCar].[dbo].[UserAccountSales] ua WITH (NOLOCK) ON ua.[ID] = mua.[UserAccountSalesID]
        LEFT JOIN [ABCCar].[dbo].[KeyValueDetail] kvd WITH (NOLOCK) ON kvd.Value = m.Guild AND kvd.KeyName = 'Guild'
        OUTER APPLY 
        (SELECT TOP 1 po.*
			FROM [ABCCar].[dbo].ProductOrder po WITH (NOLOCK) 
			LEFT JOIN [ABCCar].[dbo].ProductOrderItem poi WITH (NOLOCK) ON po.ID = poi.OrderID
			WHERE m.MemberID = po.MemberID 
			AND poi.ProductID = 1 
			AND po.OrderStatusID = 30
			AND po.PaymentStatusID IN (30, 40, 45)
        ) AS pd
        OUTER APPLY 
            (SELECT TOP 1 *
            FROM [ABCCar].[dbo].[PoolPointsHistory] ph1 with (NOLOCK)
            WHERE m.MemberID = ph1.MemberID AND ph1.RecordTypeID = 10
            ORDER BY ph1.ID DESC
        ) AS ph
        WHERE 
        (EXISTS
            (SELECT * FROM [ABCCar].[dbo].MemberAgree WITH (NOLOCK)
            WHERE [MemberID] = m.MemberID)
        OR EXISTS
            (SELECT * FROM [ABCCar].[dbo].[Car] car WITH (NOLOCK)
            JOIN [ABCCar].[dbo].[CarModel] carModel WITH (NOLOCK) ON carModel.[IsEnable] = 1 
                        AND car.[BrandID] = carModel.[BrandID] 
                        AND car.[SeriesID]= carModel.[SeriesID] 
                        AND car.[CategoryID]= carModel.[CategoryID]
            JOIN [ABCCar].[dbo].[Member] member WITH (NOLOCK)  ON car.[MemberID] = CAST(member.MemberID AS bigint)
                        AND member.[Status] = 1
                        AND member.[Category] IN (2,3,5) --個人賣家、車商、經紀人
            WHERE car.[Status] IN (0, 1) --待售
            AND car.[ModifyDate] >= '2018-06-20'
            AND car.MemberID = m.MemberID)) 
        AND m.[Category] > 1
        AND m.[Status] = 1
        AND (bm.BindingDate IS NOT NULL OR ph.ID IS NOT NULL)
        ORDER BY m.MemberID
    """

    hot_sq = f"""
        DECLARE @HotCarBatchID INT
        SELECT @HotCarBatchID = Max(ID) FROM [ABCCar].[dbo].[HotCarBatch] WITH (NOLOCK)

        SELECT ACC,CERSTS INTO #ACC FROM abccar.dbo.HotCar WITH (NOLOCK) WHERE [HotCarBatchID] = @HotCarBatchID

        SELECT
            (SELECT COUNT(1) FROM #ACC B WHERE B.[ACC] = A.[HOTMemberID]) AS [HOT官網總台數]
            ,(SELECT COUNT(1) FROM #ACC C WHERE C.[ACC] = A.[HOTMemberID] AND C.CERSTS = 4) AS [HOT官網認證台數]
            ,[MemberID]
            ,[HOTMemberID] AS [HOT主帳號]
        FROM abccar.dbo.[HotCarMember] A WITH (NOLOCK)
        WHERE A.[IsMaster] = 1
        ORDER BY [HOT官網總台數] desc

        DROP TABLE #ACC
    """

    # 執行讀取程式
    
    memberAgree = read_mssql(memberAgree_sg, 'ABCCar')
    hotDF = read_mssql(hot_sq, 'ABCCar')

    df_hot = pd.merge(memberAgree, hotDF, on="MemberID", how='left')

    #Check data length
    print(len(df_hot))

    #確保資料只有唯一的memberID
    df_hot = df_hot.drop_duplicates(subset=['MemberID'])
    
    #Check data length
    print(len(df_hot))

    return df_hot

def readReport():
    #今天上架數量報表
    today = f"""
        select * from report.memberOnself where date = '{end_date}'
    """

    #前一天上架數量(計算後)
    theDayBeforeYesterday = f"""
        select * from report.memberOnself where date = '{the_day_before_yesterday_date}'
    """

    #上月底上架數量(計算後)
    lastMonthEndDay = f"""
        select * from report.memberOnself where date = '{lastMonth_end_day}'
    """

    today_sq = read_mysql(today, 'report')
    theDayBeforeYesterday_sq = read_mysql(theDayBeforeYesterday, 'report')
    lastMonthEndDay_sq = read_mysql(lastMonthEndDay, 'report')

    return today_sq, theDayBeforeYesterday_sq, lastMonthEndDay_sq

def mergeHotMemberID(memberAgree_new):
    hot = pd.read_excel('HOT.xlsx',converters={'統一編號':str})
    hot.rename(columns={'統一編號':'統編'}, inplace=True)
    
    tmp = hot[['統編','HOT會員編號']]
    tmp = tmp.drop_duplicates(subset=['統編'])

    mergeDf = pd.merge(memberAgree, tmp, on="統編", how='left')
    mergeDf = mergeDf[['MemberID', '會員名稱', '車商名稱', '城市別', '連絡人', '連絡電話', '統編', 'HOT會員編號', 'HAA會員', '身分別', '網路好店', '公會', '註冊時間' ,'同意會員條款時間' , '信用卡綁定時間', '最新儲值時間', '本日新上架車輛' , '本日下架車輛', '本月新上架車輛', '本月下架車輛' , '本月架上車輛', '本月架上驗證成功車輛', '外部車源待售數', '外部車源上架數', '簡易刊登車源待售數', '簡易刊登車源上架數', '簡易刊登本日新上架', '簡易刊登本月新上架', '會員同意條款維護業務人員ID']]
    return mergeDf

def mergeABCSales(memberAgree_new):

    distribute = pd.read_excel('abc物件分配表(上架車輛業務人員歸戶)總表_20211221.xlsx')

    tmp = distribute[['MemberID','維護\n業務']]
    tmp.rename(columns={'維護\n業務':'維護業務'}, inplace=True)
    tmp = tmp.drop_duplicates(subset=['MemberID'])

    newMergeDf = pd.merge(memberAgree_new, tmp, on="MemberID", how='left')
    # newMergeDf = mergeDf[['MemberID', '會員名稱', '車商名稱', '城市別', '連絡人', '連絡電話', '統編', 'HOT會員編號', 'HAA會員', '身分別', '網路好店', '公會','註冊時間','維護業務', '同意會員條款時間' , '信用卡綁定時間', '最新儲值時間' , '本日新上架車輛' , '本日下架車輛', '本月新上架車輛', '本月下架車輛' , '本月架上車輛', '本月架上驗證成功車輛', '外部車源待售數', '外部車源上架數', '簡易刊登車源待售數', '簡易刊登車源上架數', '簡易刊登本日新上架', '簡易刊登本月新上架', '會員同意條款維護業務人員ID']]
    
    # tmpNameCity = distribute[['MemberID', '車商名稱', '城市別']]
    # tmpNameCity = tmpNameCity.drop_duplicates(subset=['MemberID'])
    # tmpNameCity.rename(columns={'車商名稱':'車商名稱New', '城市別':'城市別New'}, inplace=True)
    # newMergeDf = pd.merge(mergeDf, tmpNameCity, on="MemberID", how='left')
    
    # #2021/03/30 以資料庫的車商名稱、城市別為主，再疊加excel的資料到這兩個欄位
    # for index, row in newMergeDf.iterrows():
    #     if(type(row['車商名稱New']) == str):
    #         newMergeDf['車商名稱'].loc[index] = row['車商名稱New']

    # for index, row in newMergeDf.iterrows():
    #     if(type(row['城市別New']) == str):
    #         newMergeDf['城市別'].loc[index] = row['城市別New']
            
    # newMergeDf = newMergeDf[['MemberID', '會員名稱', '車商名稱', '城市別', '連絡人', '連絡電話', '統編', 'HOT會員編號', 'HAA會員', '身分別', '網路好店', '公會','註冊時間','維護業務', '同意會員條款時間' , '信用卡綁定時間' , '本日新上架車輛' , '本日下架車輛', '本月新上架車輛', '本月下架車輛' , '本月架上車輛', '本月架上驗證成功車輛', '外部車源待售數', '外部車源上架數', '簡易刊登車源待售數', '簡易刊登車源上架數', '會員同意條款維護業務人員ID']]
            
    return newMergeDf

def addHotIDSecond(df):
    
    HOTUpdate = pd.read_excel('HOT編號abcID_0603.xlsx', converters={'abc會員ID':int}) 

    container = []
    for i, v in enumerate(HOTUpdate['abc會員ID']):
        if(type(v) == int):
            container.append(i)

    tmpHOTU = HOTUpdate[['HOT會員編號','abc會員ID']]     
    tmpHOTU = tmpHOTU.drop_duplicates(subset=['HOT會員編號'])

    tmpColorRow = []
    for index, row in tmpHOTU.iterrows():
        for i, v in df.iterrows():
    #         if((isinstance(v['HOT會員編號'], float)) and (v['MemberID'] == row['abc會員ID'])):
            if(v['MemberID'] == row['abc會員ID']):
                df['HOT會員編號'].loc[i] = row['HOT會員編號']
                tmpColorRow.append(i)
    return df, tmpColorRow

def addStageLastMonth(df):
    StageLastMonth = pd.read_csv('stageLastMonth_'+ str(lastMonth_end_day) +'.csv') 
    
    tmp = StageLastMonth[['MemberID','上月底架上台數']]
    tmp = tmp.drop_duplicates(subset=['MemberID'])

    mergeDf = pd.merge(df, tmp, on="MemberID", how='left')
    # mergeDf = mergeDf[['MemberID', '會員名稱', '車商名稱', '城市別', '連絡人', '連絡電話', '統編', 'HOT會員編號', 'HAA會員', '身分別', '網路好店', '公會','註冊時間' ,'維護業務','同意會員條款時間' , '信用卡綁定時間', '最新儲值時間' , '上月底架上台數', '本日新上架車輛' , '本日下架車輛', '本月新上架車輛', '本月下架車輛' , '本月架上車輛', '本月架上驗證成功車輛', '外部車源待售數', '外部車源上架數', '簡易刊登車源待售數', '簡易刊登車源上架數', '簡易刊登本日新上架', '簡易刊登本月新上架', '會員同意條款維護業務人員ID']]
    
    return mergeDf
 
def processData(memberAgree):

    # 2021-12-27 組業務排序邏輯字串
    memberAgree['維護業務'] = memberAgree.apply(lambda x: '0' + str(x['維護業務序號']) + ' ' + x['維護業務']  if len(str(x['維護業務序號'])) == 1 else str(x['維護業務序號']) + ' ' + x['維護業務'] , axis=1)
    memberAgree['維護業務'] = memberAgree['維護業務'].replace('00 ', '')
    
    salesID = [90, 102, 60, 101, 98, 99, 104, 97, 103, 61, 'all']
    # today_sq.reindex(['90', '102', '60', '101', '98', '99', '103', '97', '61', '尚未分配', 'all'])
    #已綁卡車商家數
    totalAgree = []
    tmpA = memberAgree.loc[(memberAgree['維護業務ID'] == 90) & memberAgree['信用卡綁定時間'].notna(), :] #孫和莆
    tmpB = memberAgree.loc[(memberAgree['維護業務ID'] == 102) & memberAgree['信用卡綁定時間'].notna(), :] #邱雅凌
    tmpC = memberAgree.loc[(memberAgree['維護業務ID'] == 60) & memberAgree['信用卡綁定時間'].notna(), :] #陳玫君
    tmpD = memberAgree.loc[(memberAgree['維護業務ID'] == 101) & memberAgree['信用卡綁定時間'].notna(), :] #童嘉如
    tmpE = memberAgree.loc[(memberAgree['維護業務ID'] == 98) & memberAgree['信用卡綁定時間'].notna(), :] #詹朝順 
    tmpF = memberAgree.loc[(memberAgree['維護業務ID'] == 99) & memberAgree['信用卡綁定時間'].notna(), :] #黃煒凱 
    tmpG = memberAgree.loc[(memberAgree['維護業務ID'] == 103) & memberAgree['信用卡綁定時間'].notna(), :] #黃宏智
    tmpH = memberAgree.loc[(memberAgree['維護業務ID'] == 97) & memberAgree['信用卡綁定時間'].notna(), :] #林韋辰
    tmpI = memberAgree.loc[(memberAgree['維護業務ID'] == 61) & memberAgree['信用卡綁定時間'].notna(), :] #陳億業
    tmpJ = memberAgree.loc[(memberAgree['維護業務ID'] == 104) & memberAgree['信用卡綁定時間'].notna(), :] #張可函

    totalAgree.append(tmpA.count()['維護業務ID']) #孫和莆 
    totalAgree.append(tmpB.count()['維護業務ID']) #邱雅凌 
    totalAgree.append(tmpC.count()['維護業務ID']) #陳玫君 
    totalAgree.append(tmpD.count()['維護業務ID']) #童嘉如
    totalAgree.append(tmpE.count()['維護業務ID']) #詹朝順 
    totalAgree.append(tmpF.count()['維護業務ID']) #黃煒凱
    totalAgree.append(tmpJ.count()['維護業務ID']) #張可函
    totalAgree.append(tmpH.count()['維護業務ID']) #林韋辰
    totalAgree.append(tmpG.count()['維護業務ID']) #黃宏智 
    totalAgree.append(tmpI.count()['維護業務ID']) #陳億業
    totalA = tmpA.count()['維護業務ID'] + tmpB.count()['維護業務ID'] +tmpC.count()['維護業務ID']+tmpD.count()['維護業務ID']+tmpE.count()['維護業務ID']+tmpF.count()['維護業務ID']+tmpG.count()['維護業務ID']+tmpH.count()['維護業務ID']+tmpI.count()['維護業務ID']+tmpJ.count()['維護業務ID']
    totalAgree.append(totalA) #總和

    #已儲值車商家數
    totalDeposit = []
    tmpA = memberAgree.loc[(memberAgree['維護業務ID'] == 90) & memberAgree['最新儲值時間'].notna(), :] #孫和莆
    tmpB = memberAgree.loc[(memberAgree['維護業務ID'] == 102) & memberAgree['最新儲值時間'].notna(), :] #邱雅凌
    tmpC = memberAgree.loc[(memberAgree['維護業務ID'] == 60) & memberAgree['最新儲值時間'].notna(), :] #陳玫君
    tmpD = memberAgree.loc[(memberAgree['維護業務ID'] == 101) & memberAgree['最新儲值時間'].notna(), :] #童嘉如
    tmpE = memberAgree.loc[(memberAgree['維護業務ID'] == 98) & memberAgree['最新儲值時間'].notna(), :] #詹朝順 
    tmpF = memberAgree.loc[(memberAgree['維護業務ID'] == 99) & memberAgree['最新儲值時間'].notna(), :] #黃煒凱 
    tmpG = memberAgree.loc[(memberAgree['維護業務ID'] == 103) & memberAgree['最新儲值時間'].notna(), :] #黃宏智
    tmpH = memberAgree.loc[(memberAgree['維護業務ID'] == 97) & memberAgree['最新儲值時間'].notna(), :] #林韋辰
    tmpI = memberAgree.loc[(memberAgree['維護業務ID'] == 61) & memberAgree['最新儲值時間'].notna(), :] #陳億業
    tmpJ = memberAgree.loc[(memberAgree['維護業務ID'] == 104) & memberAgree['最新儲值時間'].notna(), :] #張可函

    totalDeposit.append(tmpA.count()['維護業務ID']) #孫和莆 
    totalDeposit.append(tmpB.count()['維護業務ID']) #邱雅凌 
    totalDeposit.append(tmpC.count()['維護業務ID']) #陳玫君 
    totalDeposit.append(tmpD.count()['維護業務ID']) #童嘉如
    totalDeposit.append(tmpE.count()['維護業務ID']) #詹朝順 
    totalDeposit.append(tmpF.count()['維護業務ID']) #黃煒凱
    totalDeposit.append(tmpJ.count()['維護業務ID']) #張可函
    totalDeposit.append(tmpH.count()['維護業務ID']) #林韋辰
    totalDeposit.append(tmpG.count()['維護業務ID']) #黃宏智 
    totalDeposit.append(tmpI.count()['維護業務ID']) #陳億業
    totalB = tmpA.count()['維護業務ID'] + tmpB.count()['維護業務ID'] +tmpC.count()['維護業務ID']+tmpD.count()['維護業務ID']+tmpE.count()['維護業務ID']+tmpF.count()['維護業務ID']+tmpG.count()['維護業務ID']+tmpH.count()['維護業務ID']+tmpI.count()['維護業務ID']+tmpJ.count()['維護業務ID']
    totalDeposit.append(totalB) #總和

    #--------------取得業務
    tmpA = memberAgree.loc[(memberAgree['維護業務ID'] == 90), :] #孫和莆
    tmpB = memberAgree.loc[(memberAgree['維護業務ID'] == 102), :] #邱雅凌
    tmpC = memberAgree.loc[(memberAgree['維護業務ID'] == 60), :] #陳玫君
    tmpD = memberAgree.loc[(memberAgree['維護業務ID'] == 101), :] #童嘉如
    tmpE = memberAgree.loc[(memberAgree['維護業務ID'] == 98), :] #詹朝順 
    tmpF = memberAgree.loc[(memberAgree['維護業務ID'] == 99), :] #黃煒凱 
    tmpG = memberAgree.loc[(memberAgree['維護業務ID'] == 103), :] #黃宏智
    tmpH = memberAgree.loc[(memberAgree['維護業務ID'] == 97), :] #林韋辰
    tmpI = memberAgree.loc[(memberAgree['維護業務ID'] == 61), :] #陳億業
    tmpJ = memberAgree.loc[(memberAgree['維護業務ID'] == 104), :] #張可函

    #--------------本日新增台數
    tmpTotalCard = tmpA.sum()['本日新上架車輛'] + tmpB.sum()['本日新上架車輛']+tmpC.sum()['本日新上架車輛']+tmpD.sum()['本日新上架車輛']+tmpE.sum()['本日新上架車輛']+tmpF.sum()['本日新上架車輛']+tmpG.sum()['本日新上架車輛']+tmpH.sum()['本日新上架車輛']+tmpI.sum()['本日新上架車輛']+tmpJ.sum()['本日新上架車輛']
    todayNewCar = []
    tmpACard = tmpA.sum()['本日新上架車輛']
    tmpBCard = tmpB.sum()['本日新上架車輛']
    tmpCCard = tmpC.sum()['本日新上架車輛']
    tmpDCard = tmpD.sum()['本日新上架車輛']
    tmpECard = tmpE.sum()['本日新上架車輛']
    tmpFCard = tmpF.sum()['本日新上架車輛']
    tmpGCard = tmpG.sum()['本日新上架車輛']
    tmpHCard = tmpH.sum()['本日新上架車輛']
    tmpICard = tmpI.sum()['本日新上架車輛']
    tmpJCard = tmpJ.sum()['本日新上架車輛']#張可函

    todayNewCar.append(tmpACard) 
    todayNewCar.append(tmpBCard) 
    todayNewCar.append(tmpCCard) 
    todayNewCar.append(tmpDCard)
    todayNewCar.append(tmpECard)
    todayNewCar.append(tmpFCard) 
    todayNewCar.append(tmpJCard) #張可函
    todayNewCar.append(tmpHCard) 
    todayNewCar.append(tmpGCard) #黃宏智
    todayNewCar.append(tmpICard) 
    todayNewCar.append(tmpTotalCard) #總和

    #--------------本日下架台數
    tmpTotalCard = tmpA.sum()['本日下架車輛'] + tmpB.sum()['本日下架車輛']+tmpC.sum()['本日下架車輛']+tmpD.sum()['本日下架車輛']+tmpE.sum()['本日下架車輛']+tmpF.sum()['本日下架車輛']+tmpG.sum()['本日下架車輛']+tmpH.sum()['本日下架車輛']+tmpI.sum()['本日下架車輛']+tmpJ.sum()['本日下架車輛']
    todayNewCarOff = []
    tmpACard = tmpA.sum()['本日下架車輛']
    tmpBCard = tmpB.sum()['本日下架車輛']
    tmpCCard = tmpC.sum()['本日下架車輛']
    tmpDCard = tmpD.sum()['本日下架車輛']
    tmpECard = tmpE.sum()['本日下架車輛']
    tmpFCard = tmpF.sum()['本日下架車輛']
    tmpGCard = tmpG.sum()['本日下架車輛'] 
    tmpHCard = tmpH.sum()['本日下架車輛']
    tmpICard = tmpI.sum()['本日下架車輛']
    tmpJCard = tmpJ.sum()['本日下架車輛']#張可函

    todayNewCarOff.append(tmpACard) 
    todayNewCarOff.append(tmpBCard) 
    todayNewCarOff.append(tmpCCard) 
    todayNewCarOff.append(tmpDCard)
    todayNewCarOff.append(tmpECard)
    todayNewCarOff.append(tmpFCard)
    todayNewCarOff.append(tmpJCard) #張可函 
    todayNewCarOff.append(tmpHCard) 
    todayNewCarOff.append(tmpGCard) #黃宏智
    todayNewCarOff.append(tmpICard) 
    todayNewCarOff.append(tmpTotalCard) #總和

    #--------------本月新增台數
    tmpTotalCard = tmpA.sum()['本月新上架車輛'] + tmpB.sum()['本月新上架車輛']+tmpC.sum()['本月新上架車輛']+tmpD.sum()['本月新上架車輛']+tmpE.sum()['本月新上架車輛']+tmpF.sum()['本月新上架車輛']+tmpG.sum()['本月新上架車輛']+tmpH.sum()['本月新上架車輛']+tmpI.sum()['本月新上架車輛']+tmpJ.sum()['本月新上架車輛']
    thidMonthNewCar = []
    tmpACard = tmpA.sum()['本月新上架車輛']
    tmpBCard = tmpB.sum()['本月新上架車輛']
    tmpCCard = tmpC.sum()['本月新上架車輛']
    tmpDCard = tmpD.sum()['本月新上架車輛']
    tmpECard = tmpE.sum()['本月新上架車輛']
    tmpFCard = tmpF.sum()['本月新上架車輛']
    tmpGCard = tmpG.sum()['本月新上架車輛']
    tmpHCard = tmpH.sum()['本月新上架車輛']
    tmpICard = tmpI.sum()['本月新上架車輛']
    tmpJCard = tmpJ.sum()['本月新上架車輛'] #張可函

    thidMonthNewCar.append(tmpACard) 
    thidMonthNewCar.append(tmpBCard) 
    thidMonthNewCar.append(tmpCCard) 
    thidMonthNewCar.append(tmpDCard)
    thidMonthNewCar.append(tmpECard)
    thidMonthNewCar.append(tmpFCard) 
    thidMonthNewCar.append(tmpJCard) #張可函
    thidMonthNewCar.append(tmpHCard) 
    thidMonthNewCar.append(tmpGCard) #黃宏智
    thidMonthNewCar.append(tmpICard) 
    thidMonthNewCar.append(tmpTotalCard) #總和

    #--------------本月下架台數
    tmpTotalCard = tmpA.sum()['本月下架車輛'] + tmpB.sum()['本月下架車輛']+tmpC.sum()['本月下架車輛']+tmpD.sum()['本月下架車輛']+tmpE.sum()['本月下架車輛']+tmpF.sum()['本月下架車輛']+tmpG.sum()['本月下架車輛']+tmpH.sum()['本月下架車輛']+tmpI.sum()['本月下架車輛']+tmpJ.sum()['本月下架車輛']
    thisMonthNewCarOff = []
    tmpACard = tmpA.sum()['本月下架車輛']
    tmpBCard = tmpB.sum()['本月下架車輛']
    tmpCCard = tmpC.sum()['本月下架車輛']
    tmpDCard = tmpD.sum()['本月下架車輛']
    tmpECard = tmpE.sum()['本月下架車輛']
    tmpFCard = tmpF.sum()['本月下架車輛']
    tmpGCard = tmpG.sum()['本月下架車輛']
    tmpHCard = tmpH.sum()['本月下架車輛']
    tmpICard = tmpI.sum()['本月下架車輛']
    tmpJCard = tmpJ.sum()['本月下架車輛']

    thisMonthNewCarOff.append(tmpACard) 
    thisMonthNewCarOff.append(tmpBCard) 
    thisMonthNewCarOff.append(tmpCCard) 
    thisMonthNewCarOff.append(tmpDCard)
    thisMonthNewCarOff.append(tmpECard)
    thisMonthNewCarOff.append(tmpFCard) 
    thisMonthNewCarOff.append(tmpJCard) #張可函
    thisMonthNewCarOff.append(tmpHCard) 
    thisMonthNewCarOff.append(tmpGCard) #黃宏智
    thisMonthNewCarOff.append(tmpICard) 
    thisMonthNewCarOff.append(tmpTotalCard) #總和

    #--------------本月架上台數
    tmpTotalCard = tmpA.sum()['本月架上車輛'] + tmpB.sum()['本月架上車輛']+tmpC.sum()['本月架上車輛']+tmpD.sum()['本月架上車輛']+tmpE.sum()['本月架上車輛']+tmpF.sum()['本月架上車輛']+tmpG.sum()['本月架上車輛']+tmpH.sum()['本月架上車輛']+tmpI.sum()['本月架上車輛']+tmpJ.sum()['本月架上車輛']
    thidMonthNewCarOnshelf = []
    tmpACard = tmpA.sum()['本月架上車輛']
    tmpBCard = tmpB.sum()['本月架上車輛']
    tmpCCard = tmpC.sum()['本月架上車輛']
    tmpDCard = tmpD.sum()['本月架上車輛']
    tmpECard = tmpE.sum()['本月架上車輛']
    tmpFCard = tmpF.sum()['本月架上車輛']
    tmpGCard = tmpG.sum()['本月架上車輛']
    tmpHCard = tmpH.sum()['本月架上車輛']
    tmpICard = tmpI.sum()['本月架上車輛']
    tmpJCard = tmpJ.sum()['本月架上車輛']

    thidMonthNewCarOnshelf.append(tmpACard) 
    thidMonthNewCarOnshelf.append(tmpBCard) 
    thidMonthNewCarOnshelf.append(tmpCCard) 
    thidMonthNewCarOnshelf.append(tmpDCard)
    thidMonthNewCarOnshelf.append(tmpECard)
    thidMonthNewCarOnshelf.append(tmpFCard)
    thidMonthNewCarOnshelf.append(tmpJCard) #張可函
    thidMonthNewCarOnshelf.append(tmpHCard) 
    thidMonthNewCarOnshelf.append(tmpGCard) #黃宏智
    thidMonthNewCarOnshelf.append(tmpICard) 
    thidMonthNewCarOnshelf.append(tmpTotalCard) #總和

    #貢獻數
    subTotal = []
    for i, v in enumerate(thidMonthNewCarOnshelf):
        if (np.isnan(v/totalAgree[i])):
            intNum = 0   
        else:
            intNum = int(v/totalAgree[i])

        subTotal.append(intNum)
    
    #本月淨增加
    thisMonthIncre = []
    for i, v in enumerate(thidMonthNewCar):
        thisMonthIncre.append(v-thisMonthNewCarOff[i])

    df = pd.DataFrame(list(zip(salesID, totalAgree, totalDeposit, todayNewCar, todayNewCarOff, thidMonthNewCar, thisMonthNewCarOff, thisMonthIncre, thidMonthNewCarOnshelf, subTotal)),
               columns =['salesID', '已綁卡車商家數', '已儲值車商家數', '本日新增台數', '本日下架台數', '本月新增台數', '本月下架台數', '本月淨增加', '當日累計架上台數', '貢獻數'])
    df.insert(0, 'Date', end_date)

    return df

def addValueToExcel(ws_main, valueList, rowIndex, colIndex):
    for index, value in enumerate(valueList):
        grid = ws_main.cell(row=4+rowIndex+index, column=4+colIndex)
        grid.value = value

def splitThree(origin):
    return origin[0:13], origin[13:26], origin[26:39]

def subtract(list1, list2):
    difference = []   # initialization of result list

    for i in range(len(list1)):
        difference.append(int(list1[i]) - int(list2[i]))
        
    return difference

def send_to_db(dt, tbl_name):
    num = 1000
    loop_no = round(len(dt) / num) + 1
    for i in range(0, loop_no):
        data = dt[i * num:i * num + num]
        to_mysql('report', data, tbl_name, 'append')

def subThisMonthIncre(df):
    df['本月淨增加'] = df['本月新上架車輛'] - df['本月下架車輛']

    return df

def changeGuildName(df):
    df['公會'] = df['公會'].replace(['台北市汽車商業同業公會','新北市汽車商業同業公會', '桃園市汽車商業同業公會', '苗栗縣汽車商業同業公會', '高雄市汽車商業同業公會', '台中市汽車商業同業公會', '基隆市汽車商業同業公會', '宜蘭縣汽車商業同業公會', '南投縣汽車商業同業公會', '台南市汽車商業同業公會', '臺南市直轄市汽車商業同業公會', '中華民國汽車商業同業公會全聯會', '臺中市直轄市汽車商業同業公會', '屏東縣汽車商業同業公會', '高雄市新汽車商業同業公會', '花蓮縣汽車商業同業公會', '嘉義市汽車商業同業公會'],['台北','新北', '桃園', '苗栗', '高雄', '台中', '基隆', '宜蘭', '南投', '台南', '台南市', '同業', '台中市', '屏東', '高雄(新)', '花蓮', '嘉義'])

    return df

if __name__ == "__main__":
    print(now)
    # 設定日期
    script = 'abccar_member_onshelf'

    try:
        memberAgree = ReadData()
        print('Part 1 done!')
    except Exception as e:
        msg = '{0}: PART 1: 數據連線讀取失敗! The error message : {1}'.format(script, e)
        send_error_message(script='{}'.format(script), message=msg)
        print(msg)
        sys.exit(1)
    
    try:
        # memberAgree_tmp = mergeHotMemberID(memberAgree)
        # df = mergeABCSales(memberAgree)
        # memberAgree_new_tmp, tmpColorRow =  addHotIDSecond(df)
        memberAgree_new_tmp = addStageLastMonth(memberAgree)
        memberAgree_new = subThisMonthIncre(memberAgree_new_tmp)
        memberAgree_new = changeGuildName(memberAgree_new)
        print('Part 2 done!')
    except Exception as e:
        msg = '{0}: PART 2: 數據合併失敗! The error message : {1}'.format(script, e)
        send_error_message(script='{}'.format(script), message=msg)
        print(msg)
        sys.exit(1)

    try:
        df = processData(memberAgree_new)
        send_to_db(df, 'memberOnself') #將當日日報寫入資料庫
        print('Part 3 done!')
    except Exception as e:
        msg = '{0}: PART 3: 數據計算出現錯誤! The error message : {1}'.format(script, e)
        send_error_message(script='{}'.format(script), message=msg)
        print(msg)
        sys.exit(1)
        
    try:
        #讀取資料庫報表
        today_sq, theDayBeforeYesterday_sq, lastMonthEndDay_sq = readReport()

        today_sq.set_index("salesID" , inplace=True)
        print(today_sq[today_sq.index.duplicated()])
        today_sq.reindex(['90', '102', '60', '101', '98', '99', '104','97', '103', '61', 'all'])
        # today_sq.reindex(['90', '60', '101', '98', '99', '97', '61', '尚未分配', 'all'])

        theDayBeforeYesterday_sq.set_index("salesID" , inplace=True)
        theDayBeforeYesterday_sq.reindex(['90', '102', '60', '101', '98', '99', '104', '97', '103', '61', 'all'])
        # theDayBeforeYesterday_sq.reindex(['90', '60', '101', '98', '99', '97', '61', '尚未分配', 'all'])

        lastMonthEndDay_sq.set_index("salesID" , inplace=True)
        lastMonthEndDay_sq.reindex(['90', '102', '60', '101', '98', '99', '104', '97', '103', '61', 'all'])
        # lastMonthEndDay_sq.reindex(['90', '60', '101', '98', '99', '97', '61', '尚未分配', 'all'])

        today_sq['前月架上台數'] = lastMonthEndDay_sq['當日累計架上台數']
        today_sq['前日架上台數'] = theDayBeforeYesterday_sq['當日累計架上台數']
        
        wb = load_workbook('abc好車網_業務架上台數報表.xlsx')

        # Select the first work sheet
        ws_main = wb.worksheets[0]
        
        # Add today date
        date = ws_main.cell(row=2, column=3)
        date.value = 'abc好車網_業務架上台數報表_(' + end_date +')'

        date2 = ws_main.cell(row=3, column=11)
        date2.value = end_date[5:] + '\n架上台數'

        date1 = ws_main.cell(row=3, column=12)
        date1.value = the_day_before_yesterday_date[5:] + '\n架上台數'

        increase = []
        todayList = today_sq.前日架上台數.tolist()
        lastDay = today_sq.當日累計架上台數.tolist()

        for i, v in enumerate(lastDay):   
            increase.append(v - todayList[i])

        #已綁卡車商家數
        addValueToExcel(ws_main, today_sq.已綁卡車商家數.tolist(), 0, 0)
        #已儲值車商家數
        addValueToExcel(ws_main, today_sq.已儲值車商家數.tolist(), 0, 1)       
        #本日新增台數
        addValueToExcel(ws_main, today_sq.本日新增台數.tolist(), 0, 2)
        #本日下架台數
        addValueToExcel(ws_main, today_sq.本日下架台數.tolist(), 0, 3)
        #本月淨增加
        addValueToExcel(ws_main, today_sq.本月淨增加.tolist(), 0, 4)
        #本月新增台數
        addValueToExcel(ws_main, today_sq.本月新增台數.tolist(), 0, 5)
        #本月下架台數
        addValueToExcel(ws_main, today_sq.本月下架台數.tolist(), 0, 6)
        #本月架上台數
        addValueToExcel(ws_main, today_sq.當日累計架上台數.tolist(), 0, 7)
        #前一天架上台數
        addValueToExcel(ws_main, today_sq.前日架上台數.tolist(), 0, 8)
        #前日淨增加
        addValueToExcel(ws_main, increase, 0, 9)

        #Sheet 2
        #取欄位，去除業務人員ID
        # memberAgree_new = memberAgree_new[['MemberID', '會員名稱', '車商名稱', '城市別', '連絡人', '連絡電話', '身分別', '網路好店', '買萬送萬', '公會', '維護業務', 'HAA帳號', 'HOT主帳號', 'HOT官網認證台數', 'HOT官網總台數', '上月底架上台數', '本日新上架車輛' , '本日下架車輛', '本月淨增加', '本月新上架車輛', '本月下架車輛' , '本月架上車輛', '本月架上驗證成功車輛', '外部車源上架數', '外部車源待售數', '簡易刊登本月待售數', '簡易刊登本月上架數', '簡易刊登架上台數', '最新儲值時間', '信用卡綁定時間', '同意會員條款時間', '註冊時間', '當月下架未過戶', '當月過戶未下架', '當月過戶當月下架']]
        memberAgree_new = memberAgree_new[['MemberID', '會員名稱', '車商名稱', '城市別', '連絡人', '連絡電話', '身分別', '網路好店', '買萬送萬', '公會', '維護業務', 'HAA帳號', 'HOT主帳號', 'HOT官網認證台數', 'HOT官網總台數', '上月底架上台數', '本日新上架車輛' , '本日下架車輛', '本月淨增加', '本月新上架車輛', '本月下架車輛' , '本月架上車輛', '本月架上驗證成功車輛', '外部車源上架數', '外部車源待售數', '簡易刊登本月待售數', '簡易刊登本月上架數', '簡易刊登架上台數', '最新儲值時間', '信用卡綁定時間', '同意會員條款時間', '註冊時間']]

        ws = wb.worksheets[1]
        ws.delete_rows(2, 100000)

        n=1
        for r in dataframe_to_rows(memberAgree_new, index=False, header=False):
            m = 1
            for i in r:
                grid = ws.cell(row=1+n, column=m)
                grid.value = i
                m+=1
            n +=1


        #Sheet 3
        memberAgree_new_filter = memberAgree_new[memberAgree_new['本月淨增加']<-10]
        ws = wb.worksheets[2]
        ws.delete_rows(2, 100000)

        n=1
        for r in dataframe_to_rows(memberAgree_new_filter, index=False, header=False):
            m = 1
            for i in r:
                grid = ws.cell(row=1+n, column=m)
                grid.value = i
                m+=1
            n +=1
        
        # for i in tmpColorRow:
        #     grid = ws.cell(row=2+i, column=8)
        #     grid.font = Font(b=True, color="FF0000")
        
#         #保存車輛明細
#         ws = wb.worksheets[2]
#         ws.delete_rows(2, 100000)

#         n=1
#         for r in dataframe_to_rows(carList, index=False, header=False):
# #             ws.append(r)
#             m = 1
#             for i in r:
#                 grid = ws.cell(row=1+n, column=m)
#                 grid.value = i
#                 m+=1
#             n +=1

        # Save the worksheet
        wb.save('abc好車網_業務架上台數報表.xlsx')
        
        print('PART 4 all done!')
        
    except Exception as e:
        msg = '{0}: PART 4: 數據寫入出現錯誤! The error message : {1}'.format(script, e)
        send_daily_report_error(script='{}'.format(script), message=msg)
        print(msg)
        sys.exit(1)

    # try:
    #     send_daily_report_onself(receiver='g', path=path)
    #     msg = 'PART 5: 寄送架上台數OK'
    #     print(msg)
    # except Exception as e:
    #     msg = '{0}: PART 5: 寄送同意書日報發生錯誤. The error message : {1}'.format(script, e)
    #     send_daily_report_error(msg)
    #     print(msg)
    #     sys.exit(1)
