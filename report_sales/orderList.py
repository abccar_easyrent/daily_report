#==============
# Path: /home/hadoop/jessielu/report_sales
# Description: 會員欠費報表
# Date: 2022/05/24
# Author: Jessie 
#==============

import sys
import os
import numpy as np

sys.path.insert(0, '/home/hadoop/jessielu/')

from Module.EmailFuncForSales import send_order_sales, send_daily_report_error, send_error_message
from Module.ConnFunc import read_mssql, read_test_mssql, to_mysql, read_mysql
from Module.NotifyFunc import Line_Notify

from openpyxl.styles import Border, Side, PatternFill, Font, GradientFill, Alignment
from openpyxl import Workbook, load_workbook
from openpyxl.utils.dataframe import dataframe_to_rows

import pandas as pd
from datetime import datetime, timedelta, date
from dateutil.relativedelta import relativedelta  
import math

from openpyxl.styles import Border, Side, PatternFill, Font, GradientFill, Alignment
from openpyxl import Workbook

#===================Style=====================
thin = Side(border_style="thin", color="000000")
double = Side(border_style="double", color="ff0000")

path = '/home/hadoop/jessielu/report_sales/files/'

#原本
now = date.today()

#補發
# now = date.today() - relativedelta(days=1)

# date格式轉換成datetime格式
now = pd.to_datetime(now)

def readData():
    orderList = f"""
    SELECT 
        cto.ID as [訂單編號]
        ,cto.MemberID as [會員編號]
        ,ISNULL(mc.[Name], '') as [會員名稱]
        ,ISNULL(m.[CarDealerName], '') as [會員車商名稱]
        ,ua.Name as [負責業務]
        ,ua.ID as [業務ID]
        ,cto.CreateDate as [訂單時間]
        ,cto.OrderDiscount as [總折扣]
        ,cto.OrderTotal as [訂單金額]
        ,cto.RefundedAmount as [退款金額]
        ,IIF(cto.[OrderStatusID] = 10 , N'未處理', '') AS [訂單狀態]
        --,IIF(cto.[PaymentStatusID] = 10 , N'未付款', '') AS [付款狀態]
		,IIF(cto.[PaymentStatusID] = 10 , N'未付款', '')  +
		(CASE WHEN cto.PayRtnCode='10100248' THEN '(拒絕交易)' 
		       WHEN cto.PayRtnCode='10100252' THEN '(額度不足)' 
			   WHEN cto.PayRtnCode='10100058' THEN '('+cto.PayRtnMsg+')'
			   WHEN cto.PayRtnCode='10100256' THEN '(被盜用卡)' 
			   WHEN cto.PayRtnCode='10800001' THEN '(因風險控管限制credit,無法繼續操作)' 
			   WHEN cto.PayRtnCode='10100245' THEN '(CardID cannot Be Found)' 
			   WHEN cto.PayRtnCode='10100277' THEN '(Card ExpiryDate Error)' 
			   WHEN cto.PayRtnCode='10100251' THEN '(卡片過期)' 
			   WHEN cto.PayRtnCode='10100255' THEN '(報失卡)' 
         ELSE IIF(cto.PayRtnMsg !='', '('+ cto.PayRtnMsg + ' ' + cto.PayRtnCode+')','') END) AS [付款狀態]
        ,IIF(bm.[Card6No] IS NULL, '', CONCAT(bm.[Card6No], '******', bm.[Card4No])) AS [卡號]
        ,cto.AbcOrderID AS [綠界訂單編號]
        ,c.CarID AS [車輛編號]
        ,cm.DisplayName as [車輛名稱]
        ,coi.UnitPrice as [單價]
        ,coi.UnitPrice - coi.Price AS [折扣]
        ,coi.RefundPrice AS [退刷]
        ,coi.Price AS [小計]
    FROM [ABCCar].[dbo].CarTransferOrder cto WITH (NOLOCK) 
    LEFT JOIN [ABCCar].[dbo].Member m WITH (NOLOCK) ON m.MemberID = cto.MemberID
    LEFT JOIN [ABCCar].[dbo].[MemberUserAccountSalesMapping] mua WITH (NOLOCK) ON m.[MemberID] = mua.[MemberID]
    LEFT JOIN [ABCCar].[dbo].[UserAccountSales] ua WITH (NOLOCK) ON ua.[ID] = mua.[UserAccountSalesID]
    LEFT JOIN [ABCCarMemberCenter].[dbo].[Member] mc WITH (NOLOCK) ON m.[MemberID] = mc.[ID]
    LEFT JOIN [ABCCar].[dbo].[BindingCardMember] bm WITH (NOLOCK) ON m.[MemberID] = bm.[MemberID] AND bm.[Status] = 0
    LEFT JOIN [ABCCar].[dbo].CarTransferOrderItem coi WITH (NOLOCK) ON coi.OrderID = cto.ID
    Left JOIN [ABCCar].[dbo].Car c WITH (NOLOCK) ON c.CarId = coi.CarId
    LEFT JOIN [ABCCar].[dbo].CarModel cm WITH (NOLOCK) ON cm.BrandID = c.BrandID AND cm.SeriesID = c.SeriesID AND cm.CategoryID = c.CategoryID
    /**每日刷卡失敗**/
    WHERE cto.OrderStatusID = 10 AND cto.PaymentStatusID = 10 AND cto.OrderTotal > 0
    --AND cto.CreateDate >= '2021-12-01 00:00:00'
    --AND cto.CreateDate < '2021-12-02 00:00:00'
    """

    #歷史欠費訂單
    order_sgl = f"""
    SELECT
        cto.MemberID as [會員編號],
        (CASE WHEN ua.Name is not null THEN ua.Name ELSE '異常車商' END) as [負責業務],
        CONVERT(varchar(4),DATEADD(DD, 0, cto.CreateDate),120) as [年度],
        CONVERT(varchar(10),DATEADD(DD, 0, cto.CreateDate),120) as [訂單日期],
        (SELECT count(cti.CarID) from [ABCCar].[dbo].CarTransferOrderItem cti where cti.OrderID=cto.ID) as [台數],
        cto.OrderTotal as [欠費],
        (CASE(m1.[Category]) 
            WHEN 1 THEN '一般買家' 
            WHEN 2 THEN '一般賣家' 
            WHEN 3 THEN '車商' 
            WHEN 4 THEN '原廠' 
            ELSE '買賣顧問' 
        END ) AS [會員身份別],
        (CASE WHEN  (CASE WHEN ua.Name is not null THEN ua.Name ELSE '異常車商' END)='異常車商' THEN 
            (CASE WHEN len(m1.CarDealerCountryName)>0 THEN 
                m1.CarDealerCountryName
            ELSE
                ISNULL(mc.CountryName,'')
            END)
        ELSE '' END) as [縣市]
    FROM [ABCCar].[dbo].CarTransferOrder cto WITH (NOLOCK) 
    LEFT JOIN [ABCCar].[dbo].[MemberUserAccountSalesMapping] mua WITH (NOLOCK) ON cto.[MemberID] = mua.[MemberID]
    LEFT JOIN [ABCCar].[dbo].[UserAccountSales] ua WITH (NOLOCK) ON ua.[ID] = mua.[UserAccountSalesID]
    LEFT JOIN [ABCCarMemberCenter].[dbo].[Member] mc WITH (NOLOCK) ON cto.[MemberID] = mc.[ID]
    LEFT JOIN [ABCCar].[dbo].[Member] m1 WITH (NOLOCK) ON cto.[MemberID] = m1.[MemberID]
    WHERE cto.OrderStatusID = 10 AND cto.PaymentStatusID = 10 AND cto.OrderTotal > 0
    ORDER BY 4
    """

    #當日過戶收費
    q1 = f"""
    SELECT 
        CONVERT(varchar(10),DATEADD(DD, 0, GETDATE()),120) as [系統日],
        ISNULL(SUM(cto.OrderTotal)-SUM(cto.RefundedAmount), 0) as [當日過戶收費],
	    COUNT(*) as [當日訂單筆數]
    FROM [ABCCar].[dbo].CarTransferOrder cto WITH (NOLOCK) 
    WHERE cto.OrderStatusID = 30 AND cto.PaymentStatusID IN (30, 40, 45) AND cto.OrderTotal > 0
	AND CONVERT(varchar(10),cto.CreateDate,120) = CONVERT(varchar(10),DATEADD(DD, 0, GETDATE()),120)
    """
    
    #當日應收催收款
    q2 = f"""
    SELECT 
        CONVERT(varchar(10),DATEADD(DD, 0, GETDATE()),120) as [系統日],
        ISNULL(SUM(cto.OrderTotal), 0) as [當日應收催收款],
	    COUNT(*) as [當日訂單筆數]
    FROM [ABCCar].[dbo].CarTransferOrder cto WITH (NOLOCK) 
    WHERE cto.OrderStatusID = 10 AND cto.PaymentStatusID = 10 AND cto.OrderTotal > 0
	AND CONVERT(varchar(10),cto.CreateDate,120) = CONVERT(varchar(10),DATEADD(DD, 0, GETDATE()),120)
    """
    
    salesList = f"""
    SELECT ID, ZoneName as ZoneID, Name, NameSeq 
    FROM UserAccountSales
    WHERE Type='2' and Status=1 and ZoneName between 1 and 5
    ORDER BY NameSeq
    """
    
    orderListRtn = read_mssql(orderList, 'ABCCar')
    orderListRtn2 = read_mssql(order_sgl, 'ABCCar')  
    q1rtn = read_mssql(q1, 'ABCCar')
    q2rtn = read_mssql(q2, 'ABCCar')
    
    salesListRtn = read_mssql(salesList, 'ABCCar')
        
    return orderListRtn, orderListRtn2, q1rtn, q2rtn, salesListRtn

def splitDF(rtn, id):
    #Filter data by sales ID
    tmp = rtn.loc[(rtn['業務ID'].isin(id)), :].reset_index().sort_values(by=['訂單時間']).drop(['index'], axis=1)
    tmpUp= tmp[['訂單編號', '會員編號', '會員名稱', '會員車商名稱', '負責業務', '訂單時間', '總折扣', '訂單金額', '退款金額', '訂單狀態', '付款狀態', '卡號', '綠界訂單編號']].drop_duplicates(subset=['訂單編號']).reset_index().drop(['index'], axis=1)
    tmpDetail = tmp[['訂單編號', '車輛編號', '車輛名稱', '單價', '折扣', '退刷', '小計']].reset_index().drop(['index'], axis=1)
    #Filter data of today
    tmpToday = tmp.loc[(tmp['訂單時間'] >= now)].reset_index().drop(['index'], axis=1)
    tmpUpToday= tmpToday[['訂單編號', '會員編號', '會員名稱', '會員車商名稱', '負責業務', '訂單時間', '總折扣', '訂單金額', '退款金額', '訂單狀態', '付款狀態', '卡號', '綠界訂單編號']].drop_duplicates(subset=['訂單編號']).reset_index().drop(['index'], axis=1)
    tmpDetailToday = tmpToday[['訂單編號', '車輛編號', '車輛名稱', '單價', '折扣', '退刷', '小計']].reset_index().drop(['index'], axis=1)

    return tmpUpToday, tmpDetailToday, tmpUp, tmpDetail

def addDFtoExcel(tmpUp, tmpDetail, sheet, path, path2):

    wb = load_workbook(path)

    # Select the work sheet with sheet number
    ws_main = wb.worksheets[sheet]
    ws_main.delete_rows(2, 100000)

    n=2
    for r in dataframe_to_rows(tmpUp, index=False, header=False):
        m = 1
        for i in r:
            grid = ws_main.cell(row=n, column=m)
            grid.value = i
            grid.border = Border(top=thin, left=thin, bottom=thin, right=thin)
            grid.fill = PatternFill("solid", fgColor="FFF8D7")
            m+=1 #換一列
            
        n +=1 #換一行
        
        # Add car detail header

        t = ws_main.cell(row=n, column=2)
        t.value = '訂單編號'
        t.border = Border(top=thin, left=thin, bottom=thin, right=thin)
        t.fill = PatternFill("solid", fgColor="E0E0E0")
        t.alignment = Alignment(horizontal="center", vertical="center")
        
        t = ws_main.cell(row=n, column=3)
        t.value = '車輛編號'
        t.border = Border(top=thin, left=thin, bottom=thin, right=thin)
        t.fill = PatternFill("solid", fgColor="E0E0E0")
        t.alignment = Alignment(horizontal="center", vertical="center")
        
        t = ws_main.cell(row=n, column=4)
        t.value = '車輛名稱'
        t.border = Border(top=thin, left=thin, bottom=thin, right=thin)
        t.fill = PatternFill("solid", fgColor="E0E0E0")
        t.alignment = Alignment(horizontal="center", vertical="center")
        
        t = ws_main.cell(row=n, column=5)
        t.value = '單價'
        t.border = Border(top=thin, left=thin, bottom=thin, right=thin)
        t.fill = PatternFill("solid", fgColor="E0E0E0")
        t.alignment = Alignment(horizontal="center", vertical="center")
        
        t = ws_main.cell(row=n, column=6)
        t.value = '折扣'
        t.border = Border(top=thin, left=thin, bottom=thin, right=thin)
        t.fill = PatternFill("solid", fgColor="E0E0E0")
        t.alignment = Alignment(horizontal="center", vertical="center")
        
        t = ws_main.cell(row=n, column=7)
        t.value = '退刷'
        t.border = Border(top=thin, left=thin, bottom=thin, right=thin)
        t.fill = PatternFill("solid", fgColor="E0E0E0")
        t.alignment = Alignment(horizontal="center", vertical="center")
        
        t = ws_main.cell(row=n, column=8)
        t.value = '小計'
        t.border = Border(top=thin, left=thin, bottom=thin, right=thin)
        t.fill = PatternFill("solid", fgColor="E0E0E0")
        t.alignment = Alignment(horizontal="center", vertical="center")

        n +=1 #換一行
        
        for j in dataframe_to_rows(tmpDetail.loc[(tmpDetail['訂單編號'] == r[0]), :], index=False, header=False):
            k = 2 #空一格
            
            for l in j:
                grid = ws_main.cell(row=n, column=k)
                grid.value = l
                grid.border = Border(top=thin, left=thin, bottom=thin, right=thin)
                k+=1
        
            n +=1 #換一行
        # n +=1 #換一行
        
    wb.save( path2 )

if __name__ == '__main__':

    try:
        rtnDF, rtnDF2, q1rtn, q2rtn, rtnDFSales = readData()
        print(now)
        msg = 'PART 1 : 讀取欠費資料成功！'
        print(msg)
        
        # 所有業務名單
        salesID = Zone1 = Zone2 = Zone3 = Zone4 = Zone5 = []
        salesID = rtnDFSales['ID'].values.tolist()

        tmp1 = rtnDFSales.loc[(rtnDFSales['ZoneID'] == "1"), :]
        tmp2 = rtnDFSales.loc[(rtnDFSales['ZoneID'] == "2"), :]
        tmp3 = rtnDFSales.loc[(rtnDFSales['ZoneID'] == "3"), :]
        tmp4 = rtnDFSales.loc[(rtnDFSales['ZoneID'] == "4"), :]
        tmp5 = rtnDFSales.loc[(rtnDFSales['ZoneID'] == "5"), :]
        
        Zone1 = tmp1['ID'].values.tolist()
        Zone2 = tmp2['ID'].values.tolist()
        Zone3 = tmp3['ID'].values.tolist()
        Zone4 = tmp4['ID'].values.tolist()
        Zone5 = tmp5['ID'].values.tolist()
   
    except Exception as e:
        msg = 'PART 1　: 讀取欠費資料發生錯誤. The error message :{}'.format(e)
        send_daily_report_error(msg)
        print(msg)
        sys.exit(1)
        
    try:
        q1rtn['當日過戶收費'] = pd.to_numeric(q1rtn['當日過戶收費'], downcast="integer")
        q1rtn['當日訂單筆數'] = pd.to_numeric(q1rtn['當日訂單筆數'], downcast="integer")        
        q2rtn['當日應收催收款'] = pd.to_numeric(q2rtn['當日應收催收款'], downcast="integer")
        q2rtn['當日訂單筆數'] = pd.to_numeric(q2rtn['當日訂單筆數'], downcast="integer")
        
        if (q1rtn['當日過戶收費'].to_string(index=False))=="0" or (q1rtn['當日訂單筆數'].to_string(index=False))=="0" or (q2rtn['當日應收催收款'].to_string(index=False))=="0" or (q2rtn['當日訂單筆數'].to_string(index=False))=="0":
            message = "\n" + q1rtn['系統日'].to_string(index=False) + " 異常，請檢查"
            message = message + "\n當日過戶收費:" + q1rtn['當日過戶收費'].to_string(index=False) 
            message = message + " 訂單筆數:" + q1rtn['當日訂單筆數'].to_string(index=False)
            message = message + "\n當日應收催收款:" + q2rtn['當日應收催收款'].to_string(index=False) 
            message = message + " 訂單筆數:" + q2rtn['當日訂單筆數'].to_string(index=False)
            Line_Notify(receiver = 'j', path = path,msg = message, FN = '')
            
        msg = 'PART 1-1 : 檢查當日訂單/過戶收費完成！'
        print(msg)
    except Exception as e:
        msg = 'PART 1-1　: 檢查當日訂單/過戶收費資料發生錯誤. The error message :{}'.format(e)
        send_daily_report_error(msg)
        print(msg)        

    try:  
        wb = load_workbook(path + '欠費清單.xlsx')    
        #Sheet 4
        ws = wb.worksheets[4]
        #清空欠費資料
        for r in range(2,200000):
            for col in range(1,9):
                ws.cell(column=col, row=r).value = ''

        #取得欠費資料
        orderListDF = rtnDF2[['會員編號', '負責業務', '年度', '訂單日期', '台數', '欠費', '會員身份別', '縣市']]
        
        #寫入欠費資料
        n=1
        for r in dataframe_to_rows(orderListDF, index=False, header=False):
            m = 1
            for i in r:
                grid = ws.cell(row=1+n, column=m)
                grid.value = i
                m+=1
            n +=1       
        
        wb.save(path + '欠費清單.xlsx')
        
        msg = 'PART 1-2 : 寫入歷史欠費記錄完成！'
        print(msg)
    except Exception as e:
        msg = 'PART 1-2　: 寫入歷史欠費記錄發生錯誤. The error message :{}'.format(e)
        send_daily_report_error(msg)
        print(msg)

    # 所有業務名單
    #salesID = [90, 102, 109, 60, 101, 99, 110, 104, 108, 61]

    # 暫存寄送業務名單
    sendSales = []
    for i in salesID:
        print(i)
        try:
            tmpUpToday, tmpDetailToday, tmpUp, tmpDetail = splitDF(rtnDF, [i])
            msg = 'PART 2-1 : 切分業務資料成功！'
            print(msg)
        except Exception as e:
            msg = 'PART 2-1　: 切分業務資料發生錯誤. The error message :{}'.format(e)
            send_daily_report_error(msg)
            print(msg)
            sys.exit(1)
        
        if(len(tmpUp['負責業務']) > 0):
            sendSales.append(tmpUp['負責業務'][0])
            try:
                addDFtoExcel(tmpUpToday, tmpDetailToday, 0, path + '欠費清單.xlsx', path + tmpUp['負責業務'][0] +'.xlsx')
                msg = 'PART 2-2 : 寫入今日資料成功！'
                print(msg)
            except Exception as e:
                msg = 'PART 2-2　: 寫入今日資料發生錯誤. The error message :{}'.format(e)
                send_daily_report_error(msg)
                print(msg)
                sys.exit(1)

            try:
                addDFtoExcel(tmpUp, tmpDetail, 1, path + tmpUp['負責業務'][0] +'.xlsx', path + tmpUp['負責業務'][0] +'.xlsx')
                msg = 'PART 2-3 : 寫入累計資料成功！'
                print(msg)
            except Exception as e:
                msg = 'PART 2-3　: 寫入累計資料發生錯誤. The error message :{}'.format(e)
                send_daily_report_error(msg)
                print(msg)
                sys.exit(1)
    print('Part 2 - All Sales File done!')

    # 暫存寄送TL名單
    sendTL = []
    #TLList = {'北區':[90, 102, 109], '桃苗':[60, 101], '中區':[99, 110],'雲嘉':[104],'高屏':[108, 61]}
    TLList = {'北區':Zone1, '桃苗':Zone2, '中區':Zone3, '雲嘉':Zone4, '高屏':Zone5}
    for i, v in enumerate(TLList):
        print(TLList[v])
        try:
            tmpUpToday, tmpDetailToday, tmpUp, tmpDetail = splitDF(rtnDF, TLList[v])
            msg = 'PART 3-1 : 切分業務資料成功！'
            print(msg)
        except Exception as e:
            msg = 'PART 3-1　: 切分業務資料發生錯誤. The error message :{}'.format(e)
            send_daily_report_error(msg)
            print(msg)
            sys.exit(1)
        
        if(len(tmpUp['負責業務']) > 0):
            sendTL.append(v)
            try:
                addDFtoExcel(tmpUpToday, tmpDetailToday, 0, path + '欠費清單.xlsx', path + v +'.xlsx')
                msg = 'PART 3-2 : 寫入今日資料成功！'
                print(msg)
            except Exception as e:
                msg = 'PART 3-2　: 寫入今日資料發生錯誤. The error message :{}'.format(e)
                send_daily_report_error(msg)
                print(msg)
                sys.exit(1)

            try:
                addDFtoExcel(tmpUp, tmpDetail, 1, path + v +'.xlsx', path + v +'.xlsx')
                msg = 'PART 3-3 : 寫入累計資料成功！'
                print(msg)
            except Exception as e:
                msg = 'PART 3-3　: 寫入累計資料發生錯誤. The error message :{}'.format(e)
                send_daily_report_error(msg)
                print(msg)
                sys.exit(1)
    print('Part 3 - All TL File done!')

    try:
        tmpUpToday, tmpDetailToday, tmpUp, tmpDetail = splitDF(rtnDF, salesID)
        msg = 'PART 4-1 : 切分業務資料成功！'
        print(msg)
    except Exception as e:
        msg = 'PART 4-1　: 切分業務資料發生錯誤. The error message :{}'.format(e)
        send_daily_report_error(msg)
        print(msg)
        sys.exit(1)
    
    try:
        addDFtoExcel(tmpUpToday, tmpDetailToday, 0, path + '欠費清單.xlsx', path + 'All_Sales.xlsx')
        msg = 'PART 4-2 : 寫入今日資料成功！'
        print(msg)
    except Exception as e:
        msg = 'PART 4-2　: 寫入今日資料發生錯誤. The error message :{}'.format(e)
        send_daily_report_error(msg)
        print(msg)
        sys.exit(1)

    try:
        addDFtoExcel(tmpUp, tmpDetail, 1, path + 'All_Sales.xlsx', path + 'All_Sales.xlsx')
        msg = 'PART 4-3 : 寫入累計資料成功！'
        print(msg)
    except Exception as e:
        msg = 'PART 4-3　: 寫入累計資料發生錯誤. The error message :{}'.format(e)
        send_daily_report_error(msg)
        print(msg)
        sys.exit(1)
    print('Part 4 - All File done!')

    try:
        #All
        All = ['PETERMAY@hotaimotor.com.tw', 'HUNG57@hotaimotor.com.tw', '31879@hotaimotor.com.tw','JOANNE373@hotaimotor.com.tw', 'JONATHANKO@hotaimotor.com.tw', 'WILLIAM@hotaimotor.com.tw', 'STEVEWU@hotaimotor.com.tw', 'MANDYLIN@hotaimotor.com.tw', 'MASA0204@hotaimotor.com.tw', 'AARONWU@hotaimotor.com.tw', 'ANGELLIN@hotaileasing.com.tw','ROMANCHEN@hotaileasing.com.tw']
        # All = ['WILLIAM@hotaimotor.com.tw', 'STEVEWU@hotaimotor.com.tw', 'MANDYLIN@hotaimotor.com.tw', 'MARKYAO@hotaimotor.com.tw','FARA@hotaimotor.com.tw' ]
        send_order_sales('g', path, All, 'All_Sales')
        
        #TL mail
        TLMail = {'北區': 'EAGLE@hotaimotor.com.tw', '桃苗': 'maggiechen@hotaimotor.com.tw', '中區': 'SAM903@hotaimotor.com.tw', '雲嘉': 'HENNY@hotaimotor.com.tw', '高屏': 'LIN32093@hotaimotor.com.tw'}
        # TLMail = {'北區': 'MANDYLIN@hotaimotor.com.tw', '桃苗': 'MANDYLIN@hotaimotor.com.tw', '中區': 'MANDYLIN@hotaimotor.com.tw', '雲嘉': 'MANDYLIN@hotaimotor.com.tw', '高屏': 'MANDYLIN@hotaimotor.com.tw'}
        for i, v in enumerate(TLMail):
            if v in sendTL:
                send_order_sales('g', path, TLMail[v], v)

        #Sales mail
        SalesMail = {'邱雅凌':'CHERRY5588@hotaimotor.com.tw', '林賢宏':'AARON711018@hotaimotor.com.tw', '童嘉如':'TWEETY@hotaimotor.com.tw', '黃煒凱':'LSTTR12@hotaimotor.com.tw', '吳美姍':'SS0210@hotaimotor.com.tw', '林心沛':'MIA0111@hotaimotor.com.tw', '陳湘月':'MANJAK06@hotaimotor.com.tw'}
        # SalesMail = {'孫和莆':'MANDYLIN@hotaimotor.com.tw', '邱雅凌':'MANDYLIN@hotaimotor.com.tw', '林賢宏':'MANDYLIN@hotaimotor.com.tw', '童嘉如':'MANDYLIN@hotaimotor.com.tw', '黃煒凱':'MANDYLIN@hotaimotor.com.tw', '吳美姍':'MANDYLIN@hotaimotor.com.tw', '陳億業':'MANDYLIN@hotaimotor.com.tw', '林心沛':'MANDYLIN@hotaimotor.com.tw', '陳湘月':'MANDYLIN@hotaimotor.com.tw'}
        for i, v in enumerate(SalesMail):
            if v in sendSales:
                send_order_sales('g', path, SalesMail[v], v)

        msg = 'PART 5 : 寄送日報OK'
        print(msg)
    except Exception as e:
        msg = 'PART 5 : 寄送日報發生錯誤. The error message : {}'.format(e)
        send_daily_report_error(msg)
        print(msg)
        sys.exit(1)