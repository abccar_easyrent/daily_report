# coding: utf-8
import sys
import os
import numpy as np

# sys.path.insert(0, '/home/jessie/MyNotebook/daily_report/')
sys.path.insert(0, '/home/hadoop/jessielu/')

from Module.EmailFuncForSales import send_daily_report_onself, send_daily_report_error, send_error_message, send_daily_report_onself_miss
from Module.ConnFunc import read_mssql, read_test_mssql, to_mysql, read_mysql
from Module.HelpFunc import display_set

from openpyxl.styles import Border, Side, PatternFill, Font, GradientFill, Alignment
from openpyxl import Workbook, load_workbook
from openpyxl.utils.dataframe import dataframe_to_rows

import pandas as pd
from datetime import datetime, timedelta, date
from dateutil.relativedelta import relativedelta  
import math
import shutil

#原本
now = date.today().strftime("%Y-%m-%d")
#補發用
now = (date.today() - relativedelta(days=1)).strftime("%Y-%m-%d")

display_set(200, 30)
path = '/home/hadoop/jessielu/report_sales/files/'
# path = '/home/jessie/MyNotebook/easyRent/scripts/'

os.chdir(path)

def mergeData():

    distribute = pd.read_excel('../../daily_report/files/sales_update.xlsx')
    file8891 = pd.read_excel('/home/hadoop/ellen_crawl/Ellen_Others/8891_report.xlsx')

    if((str(now) +'當日' ) in distribute.columns):
        print('in')
        tmp = distribute[[str(now) +'當日']].iloc[[0, 5, 13, 15]]
        wb = load_workbook('abc好車網_業務架上台數報表.xlsx')

        # Select the first work sheet
        ws_main = wb.worksheets[0]

        # Add today date
        a = ws_main.cell(row=16, column=5)
        a.value = tmp[str(now) +'當日'].loc[5]

        b = ws_main.cell(row=16, column=8)
        b.value = tmp[str(now) +'當日'].loc[13]

        c = ws_main.cell(row=16, column=11)
        c.value = tmp[str(now) +'當日'].loc[15]

        c = ws_main.cell(row=16, column=13)
        c.value =  int(file8891['所有車輛'].iloc[-1])
        
        wb.save('abc好車網_業務架上台數報表.xlsx')
    else:
        print('營運日報執行失敗!')
        
        #複製一份
        src= path + 'abc好車網_業務架上台數報表.xlsx'
        des= path + 'files/abc好車網_業務架上台數報表.xlsx'
        shutil.copy(src, des)

        wb = load_workbook(path + 'abc好車網_業務架上台數報表.xlsx')
        ws = wb.worksheets[0]
        ws.delete_rows(15, 17)
        wb.save(path + 'files/abc好車網_業務架上台數報表.xlsx')

        path_new = '/home/hadoop/jessielu/report_sales/files/files/'
        send_daily_report_onself_miss(receiver='j', path=path_new)

        raise Exception("營運日報執行失敗!")


if __name__ == "__main__":
    print(now)
    # 設定日期
    script = 'send_onshelf_email'

    try:
        mergeData()
        print('Part 1 done!')
    except Exception as e:
        msg = '{0}: PART 1: 填入營運日報值失敗! The error message : {1}'.format(script, e)
        # send_error_message(script='{}'.format(script), message=msg)
        print(msg)
        sys.exit(1)
    
    try:
        send_daily_report_onself(receiver='j', path=path)
        msg = 'PART 2: 寄送架上台數OK'
        print(msg)
    except Exception as e:
        msg = '{0}: PART 2: 寄送架上台數日報發生錯誤. The error message : {1}'.format(script, e)
        send_daily_report_error(msg)
        print(msg)
        sys.exit(1)
