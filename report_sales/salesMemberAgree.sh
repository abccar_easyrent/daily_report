#!/bin/bash
# This script is processing order for Member on ABCCar.
# Read in data : [ABCCar].[dbo].[SOOrder], [ABCCarShop].[dbo].[Order]
# Output: write out abc業績日報表.xlsx, send email

export PATH=$PATH:/home/hadoop/jessielu/daily_report/report_sales/

date -d today
echo 'start running abccar_member.agree.py'
time (/home/hadoop/anaconda3/bin/python '/home/hadoop/jessielu/report_sales/abccar_member_agree.py')
echo 'end of script'
