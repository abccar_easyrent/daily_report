#==============
# Path: /home/hadoop/jessielu/report_sales
# Description: HAA潛在客戶名單
# Date: 2022/05/24
# Author: Jessie 
#==============

import sys
sys.path.insert(0, '/home/hadoop/jessielu/')
from apiclient.discovery import build
from oauth2client.service_account import ServiceAccountCredentials
from Module.EmailFunc import send_error_message
from Module.ConnFunc import read_mssql
import re
import pandas as pd
import sqlalchemy as sa
import os
import socket
import time
from datetime import datetime, timedelta, date
from dateutil.relativedelta import relativedelta
import random
from apiclient.errors import HttpError
from openpyxl import Workbook, load_workbook
from openpyxl.utils.dataframe import dataframe_to_rows
from Module.EmailFuncHAA import send_haaAuction_report, send_error_message


#今天
now = date.today()

#前一天
lastMonthEndDay = (datetime.today() - relativedelta(days=1))

#上個月第一天
lastMonthFirstDay = date(lastMonthEndDay.year, lastMonthEndDay.month, 1)

yesterday = lastMonthEndDay.strftime("%Y-%m-%d")

#GA config and API Key
SCOPES = ['https://www.googleapis.com/auth/analytics.readonly']
os.chdir('/home/hadoop/jessielu/daily_report/scripts/')
KEY_FILE_LOCATION = 'gaApiKey.json'
VIEW_ID = '177455238'

path = '/home/hadoop/jessielu/report_sales/files/'

def initialize_analyticsreporting():
    """Initializes an Analytics Reporting API V4 service object.

    Returns:
        An authorized Analytics Reporting API V4 service object.
    """
    credentials = ServiceAccountCredentials.from_json_keyfile_name(
        KEY_FILE_LOCATION, SCOPES)

    # Build the service object.
    analytics = build('analyticsreporting', 'v4', credentials=credentials)
    return analytics

def get_report(analytics):
    """Queries the Analytics Reporting API V4.

    Args:
        analytics: An authorized Analytics Reporting API V4 service object.
    Returns:
        The Analytics Reporting API V4 response.
    """
    valuesNeed = [
        "即時拍快訊_點擊看更多"
    ]
    reports = []
    pageCnt = 1
    pageToken = None
    while True:
        data = analytics.reports().batchGet(
            body={
                'reportRequests': [{
                    'viewId': VIEW_ID,
                    'pageToken': pageToken,
                    'samplingLevel': 'LARGE',
					'dateRanges': [{'startDate': str(lastMonthFirstDay), 'endDate':str(yesterday)}],
                    'metrics': [{'expression': 'ga:totalEvents'}],
                    'dimensions': [
                        {'name': 'ga:date'},
                        {'name': 'ga:dimension7'},
                        {'name': 'ga:eventAction'}
                    ],
                    'dimensionFilterClauses': [{
                        'filters': [{
                            'dimensionName': 'ga:eventAction',
                            'operator': 'IN_LIST',
                            'expressions': valuesNeed
                        }]
                    }]
                }]
            }
        ).execute()
        reports.append(data)
        print('Get Page %i of data.' % (pageCnt))
        pageCnt += 1
        if 'nextPageToken' in data['reports'][0]:
            pageToken = data['reports'][0]['nextPageToken']
        else:
            break
  
    return reports

def prep_data(rawData):
    rows = []
    for data in rawData:
        data = data['reports'][0]
        
        cols = data['columnHeader']
        cols = cols['dimensions'] + [cols['metricHeader']['metricHeaderEntries'][0]['name']]
        cols = [re.sub('ga:','',x) for x in cols]
    
        data = data['data']['rows']
    
        for row in data:
            date = row['dimensions'][0]
            date = '-'.join([date[:4], date[4:6], date[6:]])
            row['dimensions'][0] = date
            rows.append(row['dimensions']+row['metrics'][0]['values'])

    final = pd.DataFrame(rows, columns=cols)
    return final 

def makeRequestWithExponentialBackoff(analytics):
    for n in range(0, 5):
        print(n)
        try:
            return get_report(analytics)

        except Exception as error:
            print(error)
            time.sleep((2 ** n) + random.random())

def readMemberData():
    member_sq = f"""
    SELECT 
        m.MemberID
        ,ISNULL(mc.[Name], '') as [會員名稱]
        ,ISNULL(m.[CarDealerName], '') as [車商名稱]
        ,CASE WHEN (m.[CarDealerCountryName] is null or m.[CarDealerCountryName] = '') THEN CONCAT(mc.[CountryName], mc.[DistrictName]) ELSE CONCAT(m.[CarDealerCountryName], m.[CarDealerDistrictName]) END as [城市別]
        ,ISNULL(m.[ContactPerson], '') AS [連絡人]
        ,ISNULL(mc.[CellPhone], '') AS [連絡電話]        
        ,(CASE(m.[Category])
            WHEN 1 THEN '一般買家' 
            WHEN 2 THEN '一般賣家' 
            WHEN 3 THEN '車商' 
            WHEN 4 THEN '原廠' 
            ELSE '買賣顧問' 
        END ) AS [身分別]
        ,IIF(m.[VIPLevel] = 0 , '', '是') AS [網路好店]
        ,IIF(pd.ID IS NULL, '', '是') AS [買萬送萬]
        FROM [ABCCar].[dbo].Member m WITH (NOLOCK)
        LEFT JOIN [ABCCarMemberCenter].[dbo].[Member] mc WITH (NOLOCK) ON m.[MemberID] = mc.[ID]
        OUTER APPLY 
        (SELECT TOP 1 po.*
			FROM [ABCCar].[dbo].ProductOrder po WITH (NOLOCK) 
			LEFT JOIN [ABCCar].[dbo].ProductOrderItem poi WITH (NOLOCK) ON po.ID = poi.OrderID
			WHERE m.MemberID = po.MemberID 
			AND poi.ProductID = 1 
			AND po.OrderStatusID = 30
			AND po.PaymentStatusID IN (30, 40, 45)
        ) AS pd
    """

    member = read_mssql(member_sq, 'ABCCar')

    return member   

def processData(final, memberDF):
    GA = final[['dimension7', 'totalEvents']].rename(columns={'dimension7':'MemberID'})
    memberDF['MemberID'] = memberDF['MemberID'].astype(str)
    GA['MemberID'] = GA['MemberID'].astype(str)
    GA['totalEvents'] = GA['totalEvents'].astype(int)

    mergeDF = pd.merge(memberDF, GA, on="MemberID", how='inner')
    mergeDF = mergeDF[['MemberID', 'totalEvents', '會員名稱', '車商名稱', '城市別', '連絡人', '連絡電話', '身分別', '網路好店', '買萬送萬']]

    return mergeDF

def outputFile(df):
    wb = load_workbook(path + 'HAA潛在待開發客戶名單.xlsx')

    # Select the first work sheet
    ws = wb.worksheets[0]
    ws.delete_rows(2, 100000)

    n=1
    for r in dataframe_to_rows(df, index=False, header=False):
        m = 1
        for i in r:
            grid = ws.cell(row=1+n, column=m)
            grid.value = i
            m+=1
        n +=1

    wb.save(path+ 'HAA潛在待開發客戶名單.xlsx')


if __name__ == '__main__':
    print(lastMonthFirstDay, yesterday)
    # 設定日期
    script = 'HAAInterestedUser'

    try:
        analytics = initialize_analyticsreporting()
    except Exception as e:
        msg = 'PART 1: 連線GA API 讀取失敗! The error message : {}'.format(e)
        send_error_message(script=script, message=msg)
        print(msg)
        sys.exit(1)
        
    print('PART 1: Finished!')

    try:
        response = makeRequestWithExponentialBackoff(analytics)

    except Exception as e:
        msg = 'PART 2: Fetch GA 資料出現錯誤! The error message : {}'.format(e)
        send_error_message(script=script, message=msg)
        print(msg)
        sys.exit(1)

    print('PART 2: Finished!')

    try:
        final = prep_data(response)
    except Exception as e:
        msg = 'PART 3: 清整GA資料出現錯誤! The error message : {}'.format(e)
        send_error_message(script=script, message=msg)
        print(msg)
        sys.exit(1)

    print('PART 3: Finished!')

    try:
        memberDF = readMemberData()
    except Exception as e:
        msg = 'PART 4: 取得資料庫會員資料失敗! The error message : {}'.format(e)
        send_error_message(script=script, message=msg)
        print(msg)
        sys.exit(1)
        
    print('PART 4: Finished!')

    try:
        finalDF = processData(final, memberDF)
    except Exception as e:
        msg = 'PART 5: 合併資料失敗! The error message : {}'.format(e)
        send_error_message(script=script, message=msg)
        print(msg)
        sys.exit(1)
        
    print('PART 5: Finished!')
    
    try:
        outputFile(finalDF)
    except Exception as e:
        msg = 'PART 6: 寫入資料失敗! The error message : {}'.format(e)
        send_error_message(script=script, message=msg)
        print(msg)
        sys.exit(1)
        
    print('PART 6: Finished!')

    try:
        send_haaAuction_report(receiver='g', path=path)
        msg = 'PART 7: 寄送潛在客戶名單OK'
        print(msg)
    except Exception as e:
        msg = '{0}: PART 7: 寄送潛在客戶名單. The error message : {1}'.format(script, e)
        send_error_message(msg)
        print(msg)
        sys.exit(1)

    print('PART 7: All Finished!')
