#==============
# Path: /home/hadoop/jessielu/report_sales
# Description: 每月紀錄架上台數
# Date: 2022/05/24
# Author: Jessie 
#==============

# -*- coding: utf-8 -*-
import sys
sys.path.insert(0, '/home/hadoop/jessielu/')
from apiclient.discovery import build
from oauth2client.service_account import ServiceAccountCredentials
from Module.EmailFunc import send_error_message
from Module.ConnFunc import read_mssql, read_test_mssql
import re
import pandas as pd
import sqlalchemy as sa
import os
import socket
import time
from datetime import datetime, timedelta, date
from dateutil.relativedelta import relativedelta
import random
from apiclient.errors import HttpError
from openpyxl import Workbook, load_workbook
from openpyxl.utils.dataframe import dataframe_to_rows
from Module.EmailFuncForSales import send_carList_report


#今天
now = date.today()

#補發
# now = date.today() - relativedelta(days=1)

path = '/home/hadoop/jessielu/report_sales/files/'

# 讀取資料
def readData():
    carList_sq = f"""
        SELECT 
            car.MemberID as [會員編號]
            , car.CarID as [車輛編號]
            , car.StartDate as [上架時間]
            , car.EndDate as [下架時間]
			, cm.BrandName as [廠牌]
			, cm.DisplayName as [車型]
			, convert(varchar(4),car.ManufactureDate,120) as [年份]
			, car.Mileage as [里程]
			, round(car.Price,2) as [售價]
            , car.VehicleNo as [車身編號]
            , car.VehicleNoCheckStatus as [車身號碼驗證]
            ,(CASE(m.[Category]) 
                WHEN 1 THEN '一般買家' 
                WHEN 2 THEN '一般賣家' 
                WHEN 3 THEN '車商' 
                WHEN 4 THEN '原廠' 
                ELSE '買賣顧問' 
            END ) AS [身分別]
            , bm.Card6No + '****' + bm.Card4No as [綁定信用卡號]
            , car.TransferCheckId as [過戶驗證ID]
			, ISNULL(convert(varchar(10),trod.CreateDate,120),'') as [過戶訂單日]
            , ISNULL(convert(varchar(10),f.CreateDate,120),'') [影片上傳日期]
			, CASE WHEN f.Category='12' THEN '一鍵生成' WHEN f.Category='11' THEN '手動上傳' Else '' END [影片類別]
			, ISNULL(convert(varchar(10),gpt.CreateDate,120),'') [AI生成內容建立日期]
			, ISNULL(
				CASE WHEN s.ModifyDate is null THEN convert(varchar(10),s.CreateDate,120) 
				Else convert(varchar(10),s.ModifyDate,120) 
			END,'') [實車在店驗証日]
        FROM [ABCCar].[dbo].[Car] car WITH (NOLOCK)
        LEFT JOIN [ABCCar].[dbo].[Member] m WITH (NOLOCK) ON m.MemberID = car.MemberID
        LEFT JOIN [ABCCar].[dbo].[BindingCardMember] bm WITH (NOLOCK) ON m.[MemberID] = bm.[MemberID] AND bm.[Status] = 0
		LEFT JOIN [ABCCar].[dbo].[CarModel] cm WITH (NOLOCK) ON cm.BrandID = car.BrandID and cm.SeriesID = car.SeriesID and cm.CategoryID = car.CategoryID
		LEFT JOIN (SELECT itm.CarID,od.CreateDate FROM CarTransferOrderItem itm WITH (NOLOCK),CarTransferOrder od WITH (NOLOCK) WHERE itm.OrderID=od.ID and od.OrderStatusID='30' and od.PaymentStatusID='30') trod 
		ON car.CarID=trod.CarID
        LEFT JOIN[ABCCar].[dbo].[Carfile] f WITH (NOLOCK) on car.CarID=f.CarId AND f.Status=1 and f.Category in ('11','12')
		LEFT JOIN[ABCCar].[dbo].[ChatGPTLog] gpt WITH (NOLOCK) on car.CarID=gpt.CarId AND gpt.ID = 
        (
           SELECT MAX(ID) 
           FROM [ChatGPTLog] WITH (NOLOCK)
           WHERE CarId = gpt.CarId
        )
		LEFT JOIN [ABCCar].[dbo].[CarInStoreCheckLog] s WITH (NOLOCK) ON s.InputCarId = car.CarID AND s.ID = 
		(
			SELECT max(ID) 
			FROM [dbo].[CarInStoreCheckLog] WITH (NOLOCK) 
			WHERE InputCarId=s.InputCarId and Status='1'
		)
        WHERE car.[Status] IN (1) --待售或上架
        AND car.[ModifyDate] >= '2018-06-20'
        AND GETDATE() BETWEEN car.StartDate AND car.EndDate
    """

    carList = read_test_mssql(carList_sq, 'ABCCar')

    return carList

# 輸出資料至Excel
def outputFile(df):
    wb = load_workbook(path + '架上車輛總覽.xlsx')

    # Select the first work sheet
    ws = wb.worksheets[0]
    ws.delete_rows(2, 100000)

    n=1
    for r in dataframe_to_rows(df, index=False, header=False):
        m = 1
        for i in r:
            grid = ws.cell(row=1+n, column=m)
            grid.value = i
            m+=1
        n +=1

    wb.save(path + '架上車輛總覽_'+ str(now) +'.xlsx')


if __name__ == '__main__':
    print(now)
    # 設定日期
    script = 'carList'

    try:
        carDF = readData()
    except Exception as e:
        msg = 'PART 1: 取得資料庫資料失敗! The error message : {}'.format(e)
        send_error_message(script=script, message=msg)
        print(msg)
        sys.exit(1)
        
    print('PART 1: Finished!')

    try:
        outputFile(carDF)
    except Exception as e:
        msg = 'PART 2: 寫入資料失敗! The error message : {}'.format(e)
        send_error_message(script=script, message=msg)
        print(msg)
        sys.exit(1)
        
    print('PART 2: Finished!')

    try:
        send_carList_report(receiver='g', path=path)
        msg = 'PART 3: 寄送架上車輛總覽名單OK'
        print(msg)
    except Exception as e:
        msg = '{0}: PART 3: 寄送架上車輛總覽名單. The error message : {1}'.format(script, e)
        send_error_message(msg)
        print(msg)
        sys.exit(1)

    print('PART 3: All Finished!')
