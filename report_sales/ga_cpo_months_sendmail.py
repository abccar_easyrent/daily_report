#==============
# Path: /home/hadoop/jessielu/report_sales
# Description: 每月CPO成效
# Date: 2022/07/11
# Author: Sam
#==============

# -*- coding: utf-8 -*-

import sys

#正式區
sys.path.insert(0, '/home/hadoop/jessielu/')
#測試區
#sys.path.insert(0, '/home/jessie-ws/Dev/jupyterlab/daily_report/')

from datetime import datetime, timedelta, date
from dateutil.relativedelta import relativedelta

# 重新載入更新後 Module
#import importlib, sys
#importlib.reload(sys.modules['Module.EmailFuncForSales'])

from Module.EmailFuncForSales import send_cpo_report
from Module.EmailFunc import send_error_message
from Module.ConnFunc import read_mysql
from openpyxl.utils.dataframe import dataframe_to_rows
import csv

#今天
now = date.today()

SysLastMonth = (datetime.today() - relativedelta(months=1)).strftime('%Y-%m')

#正式區
Path = '/home/hadoop/jessielu/report_sales/files/'
#測試區
#Path = '/home/jessie-ws/Dev/jupyterlab/daily_report/report_sales/files/'

fname = "CPO_LEXUS_LINE_{yyyymm}.csv".format(yyyymm = SysLastMonth)

# 讀取資料
def readData():
    #cpo_sql = f"""
    #    SELECT * FROM bidb.ga WHERE  substr(date,1,7)='{SysLastMonth}'
    #    AND eventAction REGEXP 'LINE' 
    #    AND CarID REGEXP '【dealer=(2|4|5|6|77575|77560|220874)】'
    #"""

    cpo_sql = f"""
        SELECT * FROM bidb.ga WHERE substr(date,1,7)='{SysLastMonth}' 
        AND eventAction REGEXP '^td_car' 
        AND CarID in (SELECT distinct(CarID) FROM report.status_newer where MemberID in (2,3,4,5,6,9223,77560,77575,220874))
    """

    gaList = read_mysql(cpo_sql, 'bidb')

    return gaList

# 輸出資料至CSV
def outputFile(df):
    with open(Path + fname, 'w', encoding='Big5', newline='') as csvfile:
        writer = csv.writer(csvfile)
        # 寫入二維表格
        for r in dataframe_to_rows(df, index=False, header=True):
            writer.writerow(r)

if __name__ == '__main__':
    print(now)
    # 設定日期
    script = 'ga_cpo_months_sendmail'

    try:
        gaDF = readData()
    except Exception as e:
        msg = 'PART 1: 取得BIDB.GA資料庫資料失敗! The error message : {}'.format(e)
        send_error_message(script=script, message=msg)
        print(msg)
        sys.exit(1)
        
    #print(gaDF)
    print('PART 1: Finished!')
    
    try:
        outputFile(gaDF)
    except Exception as e:
        msg = 'PART 2: 寫入CSV資料失敗! The error message : {}'.format(e)
        send_error_message(script=script, message=msg)
        print(msg)
        sys.exit(1)
        
    print('PART 2: Finished!')
  
    try:
        send_cpo_report(receiver='g', path=Path, FN=fname)
        msg = 'PART 3: 寄送 CPO 網站成效 OK'
        print(msg)
    except Exception as e:
        msg = '{0}: PART 3: 寄送 CPO 網站成效. The error message : {1}'.format(script, e)
        send_error_message(msg)
        print(msg)
        sys.exit(1)

    print('PART 3: All Finished!')   