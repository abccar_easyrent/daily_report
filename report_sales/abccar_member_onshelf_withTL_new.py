#==============
# Path: /home/hadoop/jessielu/report_sales
# Description: Sales report to calculate metrics with per sales.
# Date: 2022/05/24
# Author: Jessie 
#==============

# coding: utf-8
import sys
import os
import numpy as np

# sys.path.insert(0, '/home/jessie/MyNotebook/daily_report/')
sys.path.insert(0, '/home/hadoop/jessielu/')

from Module.EmailFuncForSales import send_daily_report_onself, send_daily_report_error, send_error_message
from Module.ConnFunc import read_mssql, read_test_mssql, to_mysql, read_mysql

from openpyxl.styles import Border, Side, PatternFill, Font, GradientFill, Alignment
from openpyxl import Workbook, load_workbook
from openpyxl.utils.dataframe import dataframe_to_rows

import pandas as pd
from datetime import datetime, timedelta, date
from dateutil.relativedelta import relativedelta  

import matplotlib as mpl
mpl.use('agg')

import matplotlib.pyplot as plt

#原本
now = date.today() + relativedelta(days=1)
# #補發用
#now = date.today()

if(now.day == 1): #2021/11/1; 2022/01/01
    tmpDay = now - relativedelta(days=1) #2021/10/31; 2021/12/31
    #這個月的第一天
    first_day = date(tmpDay.year, tmpDay.month, 1) #2021/10/1; 2021/12/1
    #上個月的最後一天
    lastMonth_end_day = date(first_day.year, first_day.month, 1) - relativedelta(days=1)#2021/09/30; 2021/11/30
    #下個月的第一天
    nextMonth_first_day = first_day + relativedelta(months=1)#2021/11/1; 2022/01/01
    #這個月最後一天
    thisMonth_end_day = nextMonth_first_day - relativedelta(days=1)#2021/10/31; 2021/12/31
#if now = 2022/01/02
else:
    #這個月的第一天
    first_day = date(now.year, now.month, 1) #2022/01/01
    #上個月的最後一天
    lastMonth_end_day = date(first_day.year, first_day.month, 1) - relativedelta(days=1) #2021/12/31
    #下個月的第一天
    nextMonth_first_day = first_day + relativedelta(months=1) #2022/02/01
    #這個月最後一天
    thisMonth_end_day = nextMonth_first_day - relativedelta(days=1) #2022/01/31

#昨天
end_date = (now - timedelta(1)).strftime("%Y-%m-%d") 
#前天
the_day_before_yesterday_date = (now - timedelta(2)).strftime("%Y-%m-%d") 

path = '/home/hadoop/jessielu/report_sales/files/'
# path = '/home/jessie/MyNotebook/easyRent/scripts/'

os.chdir(path)

# 讀取數據
def ReadData():

    #每月月初，要更新月底檔案，暫存上月底台數狀況
    if(now.day == 1):
        stageLastMonth = f"""
            SELECT 
                m.[MemberID]
                ,ISNULL(( SELECT COUNT(1) FROM ABCCar.dbo.Car c1 WITH (NOLOCK),ABCCar.dbo.CarModel carmodel WITH (NOLOCK) 
				           WHERE c1.[MemberID] = m.MemberID AND c1.[Status] = 1  AND GetDate() Between c1.[StartDate] And c1.[EndDate]
					         AND carmodel.IsEnable = 1 AND c1.BrandID = carmodel.BrandID AND c1.SeriesID = carmodel.SeriesID AND c1.CategoryID = carmodel.CategoryID
			    ), 0) as [上月底架上台數]
                --,ISNULL((SELECT COUNT(1) FROM [ABCCar].[dbo].[Car] WITH (NOLOCK) WHERE [MemberID] = m.MemberID AND [Status] = 1  and GetDate() Between [Car].[StartDate] And [Car].[EndDate]), 0) as [上月底架上台數]
                --,ISNULL((SELECT COUNT(1) FROM [Car] WITH (NOLOCK) WHERE [MemberID] = m.MemberID AND [Status] = 1 AND [EndDate] >=  '{end_date} 23:59:59'), 0) as [上月底架上台數]
            FROM [ABCCar].[dbo].Member m
            LEFT JOIN [ABCCarMemberCenter].[dbo].[Member] mc WITH (NOLOCK) ON m.[MemberID] = mc.[ID]
            LEFT JOIN [ABCCar].[dbo].[MemberAgree] ma WITH (NOLOCK) ON m.[MemberID] = ma.[MemberID]
            --LEFT JOIN [ABCCar].[dbo].[BindingCardMember] bm WITH (NOLOCK) ON m.[MemberID] = bm.[MemberID] AND bm.[Status] in (0,99)
		    LEFT JOIN (SELECT * FROM [ABCCar].[dbo].[BindingCardMember] sm WITH (NOLOCK) 
		               WHERE sm.[ID] = (SELECT Max(ID) FROM [ABCCar].[dbo].[BindingCardMember] sd WITH (NOLOCK) WHERE  sd.[MemberID]=sm.[MemberID] AND sd.[Status] in (0,99)) 
				       ) bm ON m.[MemberID] = bm.[MemberID] AND bm.[Status] in (0,99) 
            LEFT JOIN [ABCCar].[dbo].[MemberUserAccountSalesMapping] mua WITH (NOLOCK) ON m.[MemberID] = mua.[MemberID]
            LEFT JOIN [ABCCar].[dbo].[UserAccountSales] ua WITH (NOLOCK) ON ua.[ID] = mua.[UserAccountSalesID]
            OUTER APPLY 
                (SELECT TOP 1 *
                FROM [ABCCar].[dbo].[PoolPointsHistory] ph1 with (NOLOCK)
                WHERE m.MemberID = ph1.MemberID AND ph1.RecordTypeID = 10
                ORDER BY ph1.ID DESC
            ) AS ph
            WHERE 
            (EXISTS
                (SELECT * FROM [ABCCar].[dbo].MemberAgree WITH (NOLOCK)
                WHERE [MemberID] = m.MemberID)
            OR EXISTS
                (SELECT * FROM [ABCCar].[dbo].[Car] car 
                JOIN [ABCCar].[dbo].[CarModel] carModel WITH (NOLOCK) ON carModel.[IsEnable] = 1 
                            AND car.[BrandID] = carModel.[BrandID] 
                            AND car.[SeriesID]= carModel.[SeriesID] 
                            AND car.[CategoryID]= carModel.[CategoryID]
                JOIN [ABCCar].[dbo].[Member] member WITH(NOLOCK)  ON car.[MemberID] = CAST(member.MemberID AS bigint)
                            AND member.[Status] = 1
                            AND (member.[Category] IN (2,3,5) OR member.[MemberID] IN (234160,232894)) --個人賣家、車商、經紀人、特定車商
                WHERE car.[Status] IN (0, 1) --待售
                AND car.[ModifyDate] >= '2018-06-20'
                AND car.MemberID = m.MemberID)) 
            AND m.[Category] > 1
            AND m.[Status] = 1
            --AND (bm.BindingDate IS NOT NULL OR ph.ID IS NOT NULL)
            AND ((bm.BindingDate IS NOT NULL OR ph.ID IS NOT NULL) OR (m.MemberID in (234160,232894) AND bm.BindingDate IS NULL))
            --AND m.MemberID = 204439
            ORDER BY m.MemberID
        """
        # 執行讀取程式
        stageOnself = read_test_mssql(stageLastMonth, 'ABCCar')
        #儲存當月月底的資料
        stageOnself.to_csv('stageLastMonth_'+ str(end_date) +'.csv')
        print('output done!')

    #主要明細表與會員台數狀況
    memberAgree_sg = f"""
       SELECT 
        m.MemberID
        ,ISNULL(mc.[Name], '') as [會員名稱]
        ,ISNULL(m.[CarDealerName], '') as [車商名稱]
        --,ISNULL(CONCAT(mc.[CountryName], mc.[DistrictName]), '') AS [城市別]
        ,CASE WHEN (m.[CarDealerCountryName] is null or m.[CarDealerCountryName] = '') THEN CONCAT(mc.[CountryName], mc.[DistrictName]) ELSE CONCAT(m.[CarDealerCountryName], m.[CarDealerDistrictName]) END as [城市別]
        ,ISNULL(m.[ContactPerson], '') AS [連絡人]
        ,ISNULL(mc.[CellPhone], '') AS [連絡電話]
        ,ISNULL(m.[TaxID], '') AS [統編]
        ,ISNULL(m.[HOTMemberID], '') AS [HOT會員編號]

        -- ,IIF(m.[Source] = 'HAA' , m.[SourceKey], '') AS [HAA會員]
        
        ,(CASE(m.[Category]) 
            WHEN 1 THEN '一般買家' 
            WHEN 2 THEN '一般賣家' 
            WHEN 3 THEN '車商' 
            WHEN 4 THEN '原廠' 
            ELSE '買賣顧問' 
        END ) AS [身分別]
        ,IIF(m.[VIPLevel] = 0 , '', '是') AS [網路好店]
        ,IIF(pd.ID IS NULL, '', '是') AS [買萬送萬]
        ,ISNULL(kvd.[Name], '') AS [公會]
		,ISNULL(ua.[Name], '') AS [維護業務]
        ,ISNULL(ua.[NameSeq], '') AS [維護業務序號]
		,ISNULL(ua.[ID], '') AS [維護業務ID]
        ,IIF(m.[Source] = 'HAA' and RIGHT(m.[SourceKey], 1) <> 'A', m.[SourceKey], '') AS [HAA帳號]
 
        ,ISNULL((SELECT COUNT(1) FROM (SELECT gt.CarID FROM [ABCCar].[dbo].[GridTrans] gt WITH (NOLOCK) LEFT JOIN [ABCCar].[dbo].[Car] c WITH (NOLOCK) ON gt.CarID = c.CarID WHERE gt.[MemberID] = m.MemberID AND [GridTranTypeID] = 202 AND gt.[CreateDate] BETWEEN '{first_day} 00:00:00' AND '{nextMonth_first_day} 00:00:00' AND DATEDIFF(day, c.[StartDate], c.[EndDate])> 2 GROUP BY gt.CarID) as tmp1), 0) as [新上架實績台數]
        -- 新上架實績台數(本月)，總表用

        --[本日新上架車輛]--GridTrans.Type=202,CreateDate=今天 == 本日新上
        ,ISNULL((SELECT COUNT(1) FROM (SELECT CarID FROM [ABCCar].[dbo].[GridTrans] WITH (NOLOCK) WHERE [MemberID] = m.MemberID AND [GridTranTypeID] = 202 AND [CreateDate] BETWEEN '{end_date} 00:00:00' AND '{now} 00:00:00' GROUP BY CarID) as tmp1), 0) as [本日新上架車輛]

        --[本日下架車輛]--GridTrans.Type=201,CreateDate=今天 == 本日下架
        ,ISNULL((SELECT COUNT(1) FROM (SELECT CarID FROM [ABCCar].[dbo].[GridTrans] WITH (NOLOCK) WHERE [MemberID] = m.MemberID AND [GridTranTypeID] = 201 AND [CreateDate] BETWEEN '{end_date} 00:00:00' AND '{now} 00:00:00' GROUP BY CarID) as tmp2), 0) as [本日下架車輛]

        --[本月新上架車輛]--GridTrans.Type=202,CreateDate=本月 == 本月新上
        ,ISNULL((SELECT COUNT(1) FROM (SELECT CarID FROM [ABCCar].[dbo].[GridTrans] WITH (NOLOCK) WHERE [MemberID] = m.MemberID AND [GridTranTypeID] = 202 AND [CreateDate] BETWEEN '{first_day} 00:00:00' AND '{nextMonth_first_day} 00:00:00' GROUP BY CarID) as tmp3), 0) as [本月新上架車輛]

		--2日內下架
        ,ISNULL((SELECT COUNT(1) FROM (SELECT gt.CarID FROM [ABCCar].[dbo].[GridTrans] gt WITH (NOLOCK) LEFT JOIN [ABCCar].[dbo].[Car] c ON gt.CarID = c.CarID WHERE gt.[MemberID] = m.MemberID AND gt.[GridTranTypeID] = 201 AND gt.[CreateDate] BETWEEN '{first_day} 00:00:00' AND '{nextMonth_first_day} 00:00:00' AND DATEDIFF(day, c.[StartDate], c.[EndDate])< 3 GROUP BY gt.CarID) as tmp4), 0) as [2日內下架]

        --[本月下架車輛]--GridTrans.Type=201,CreateDate=本月 == 本月下架
        ,ISNULL((SELECT COUNT(1) FROM (SELECT CarID FROM [ABCCar].[dbo].[GridTrans] WITH (NOLOCK) WHERE [MemberID] = m.MemberID AND [GridTranTypeID] = 201 AND [CreateDate] BETWEEN '{first_day} 00:00:00' AND '{nextMonth_first_day} 00:00:00' GROUP BY CarID) as tmp4), 0) as [本月下架車輛]

		,ISNULL((SELECT COUNT(1) FROM [ABCCar].[dbo].[Car] WITH (NOLOCK) WHERE [MemberID] = m.MemberID AND [Status] NOT IN (1,0) AND [TransferCheckId] <= 0 AND [EndDate] BETWEEN '{first_day} 00:00:00' AND '{nextMonth_first_day} 00:00:00'), 0) as [當月下架未過戶]
        ,ISNULL((SELECT COUNT(1) FROM [ABCCar].[dbo].[Car] WITH (NOLOCK) WHERE [MemberID] = m.MemberID AND [Status] = 1 AND [TransferCheckId] > 0 AND [VehicleNoCheckLastTime] BETWEEN '{first_day} 00:00:00' AND '{nextMonth_first_day} 00:00:00' ), 0) as [當月過戶未下架]
        ,ISNULL((SELECT COUNT(1) FROM [ABCCar].[dbo].[Car] WITH (NOLOCK) WHERE [MemberID] = m.MemberID AND [Status] NOT IN (1,0) AND [TransferCheckId] > 0 AND [VehicleNoCheckLastTime] BETWEEN '{first_day} 00:00:00' AND '{nextMonth_first_day} 00:00:00' AND [EndDate] BETWEEN '{first_day} 00:00:00' AND '{nextMonth_first_day} 00:00:00' ), 0) as [當月過戶當月下架]
       
	    --[架上車輛]--[Status] = 1 == 本月架上
        --,ISNULL((SELECT COUNT(1) FROM [ABCCar].[dbo].[Car] WITH (NOLOCK) WHERE [MemberID] = m.MemberID AND [Status] = 1 and GetDate() Between [Car].[StartDate] And [Car].[EndDate]), 0) as [本月架上車輛]
        ,ISNULL((
			SELECT COUNT(1) FROM ABCCar.dbo.Car c1 WITH (NOLOCK),ABCCar.dbo.CarModel carmodel WITH (NOLOCK) 
			 WHERE c1.[MemberID] = m.MemberID AND c1.[Status] = 1  AND GetDate() Between c1.[StartDate] And c1.[EndDate]
			       AND carmodel.IsEnable = 1 AND c1.BrandID = carmodel.BrandID AND c1.SeriesID = carmodel.SeriesID AND c1.CategoryID = carmodel.CategoryID
		 ), 0) as [本月架上車輛]
        
        --[車輛已驗證]--[Status] = 1 == 本月架上驗證成功
        ,ISNULL((SELECT COUNT(1) FROM [ABCCar].[dbo].[Car] WITH (NOLOCK) WHERE [MemberID] = m.MemberID AND [Status] = 1　AND [VehicleNoCheckStatus] = 1 AND GetDate() Between [Car].[StartDate] And [Car].[EndDate]), 0) as [本月架上驗證成功車輛]
        --HAA上架數
        ,ISNULL((SELECT COUNT(1) FROM [ABCCar].[dbo].[Car] WITH (NOLOCK) WHERE [MemberID] = m.MemberID AND [Status] = 1 AND [ModifyDate] >= '2018-06-20' AND [Source] IS NOT NULL AND [Source] = 'HAA'), 0) as [HAA上架數]
        --HAA待售數
        ,ISNULL((SELECT COUNT(1) FROM [ABCCar].[dbo].[Car] WITH (NOLOCK) WHERE [MemberID] = m.MemberID AND [Status] = 0 AND [ModifyDate] >= '2018-06-20' AND [Source] IS NOT NULL AND [Source] = 'HAA' AND DATEDIFF(DAY, CreateDate, GETDATE()) <= 25), 0) as [HAA待售數]
        
        ,ISNULL((SELECT COUNT(1) FROM [ABCCar].[dbo].[Car] WITH (NOLOCK) WHERE [MemberID] = m.MemberID AND [Status] = 0 AND [ModifyDate] >= '2018-06-20' AND [CreateDate] BETWEEN '{first_day} 00:00:00' AND '{nextMonth_first_day} 00:00:00' AND [Source] IS NOT NULL AND ISNUMERIC([Source]) > 0), 0) as [簡易刊登本月待售數]
        ,ISNULL((SELECT COUNT(1) FROM [ABCCar].[dbo].[Car] WITH (NOLOCK) WHERE [MemberID] = m.MemberID AND [Status] = 1 AND [ModifyDate] >= '2018-06-20' AND [CreateDate] BETWEEN '{first_day} 00:00:00' AND '{nextMonth_first_day} 00:00:00' AND [Source] IS NOT NULL AND ISNUMERIC([Source]) > 0), 0) as [簡易刊登本月上架數]
        ,ISNULL((SELECT COUNT(1) FROM [ABCCar].[dbo].[Car] WITH (NOLOCK) WHERE [MemberID] = m.MemberID AND [Status] = 1 AND [ModifyDate] >= '2018-06-20' AND [Source] IS NOT NULL AND ISNUMERIC([Source]) > 0), 0) as [簡易刊登架上台數]

        ,CASE WHEN ISNULL(ph.[ID], 0) > 0 THEN ph.CreateDate ELSE NULL END AS [最新儲值時間]
        ,bm.BindingDate as [信用卡綁定時間]
        ,ma.CreateDate as [同意會員條款時間]
        ,CONVERT(varchar(100), mc.[CreateDate], 120) AS [註冊時間]
		,ISNULL((SELECT COUNT(1) FROM [ABCCar].[dbo].[Car] c WITH (NOLOCK) WHERE c.[MemberID] = m.MemberID AND c.[StartDate] BETWEEN '{first_day} 00:00:00' AND '{nextMonth_first_day} 00:00:00' AND ISNULL(c.HOTCERNO,'')!=''), 0) as [本月上架登錄HOT認証書台數]
		,ISNULL((SELECT COUNT(1) FROM [ABCCar].[dbo].[Car] c WITH (NOLOCK) WHERE c.[MemberID] = m.MemberID AND c.[StartDate] BETWEEN '{first_day} 00:00:00' AND '{nextMonth_first_day} 00:00:00' AND ISNULL(c.HOTCERNO,'') ='' AND c.ForFreeCampaignID>0), 0) as [本月上架參與活動台數]
        FROM [ABCCar].[dbo].Member m WITH (NOLOCK)
        LEFT JOIN [ABCCarMemberCenter].[dbo].[Member] mc WITH (NOLOCK) ON m.[MemberID] = mc.[ID]
        LEFT JOIN [ABCCar].[dbo].[MemberAgree] ma WITH (NOLOCK) ON m.[MemberID] = ma.[MemberID]
        --LEFT JOIN [ABCCar].[dbo].[BindingCardMember] bm WITH (NOLOCK) ON m.[MemberID] = bm.[MemberID] AND bm.[Status] in (0,99)
		LEFT JOIN (SELECT * FROM [ABCCar].[dbo].[BindingCardMember] sm WITH (NOLOCK) 
		           WHERE sm.[ID] = (SELECT Max(ID) FROM [ABCCar].[dbo].[BindingCardMember] sd WITH (NOLOCK) WHERE  sd.[MemberID]=sm.[MemberID] AND sd.[Status] in (0,99)) 
				  ) bm ON m.[MemberID] = bm.[MemberID] AND bm.[Status] in (0,99) 
        LEFT JOIN [ABCCar].[dbo].[MemberUserAccountSalesMapping] mua WITH (NOLOCK) ON m.[MemberID] = mua.[MemberID]
        LEFT JOIN [ABCCar].[dbo].[UserAccountSales] ua WITH (NOLOCK) ON ua.[ID] = mua.[UserAccountSalesID]
        LEFT JOIN [ABCCar].[dbo].[KeyValueDetail] kvd WITH (NOLOCK) ON kvd.Value = m.Guild AND kvd.KeyName = 'Guild'
        OUTER APPLY 
        (SELECT TOP 1 po.*
			FROM [ABCCar].[dbo].ProductOrder po WITH (NOLOCK) 
			LEFT JOIN [ABCCar].[dbo].ProductOrderItem poi WITH (NOLOCK) ON po.ID = poi.OrderID
			WHERE m.MemberID = po.MemberID 
			AND poi.ProductID = 1 
			AND po.OrderStatusID = 30
			AND po.PaymentStatusID IN (30, 40, 45)
        ) AS pd

        OUTER APPLY 
            (SELECT TOP 1 *
            FROM [ABCCar].[dbo].[PoolPointsHistory] ph1 with (NOLOCK)
            WHERE m.MemberID = ph1.MemberID AND ph1.RecordTypeID = 10
            ORDER BY ph1.ID DESC
        ) AS ph

        WHERE 
        (EXISTS
            (SELECT * FROM [ABCCar].[dbo].MemberAgree WITH (NOLOCK)
            WHERE [MemberID] = m.MemberID)
        OR EXISTS
            (SELECT * FROM [ABCCar].[dbo].[Car] car WITH (NOLOCK)
            JOIN [ABCCar].[dbo].[CarModel] carModel WITH (NOLOCK) ON carModel.[IsEnable] = 1 
                        AND car.[BrandID] = carModel.[BrandID] 
                        AND car.[SeriesID]= carModel.[SeriesID] 
                        AND car.[CategoryID]= carModel.[CategoryID]
            JOIN [ABCCar].[dbo].[Member] member WITH (NOLOCK)  ON car.[MemberID] = CAST(member.MemberID AS bigint)
                        AND member.[Status] = 1
                        AND (member.[Category] IN (2,3,5) OR member.[MemberID] IN (234160,232894)) --個人賣家、車商、經紀人、特定車商
            WHERE car.[Status] IN (0, 1) --待售
            AND car.[ModifyDate] >= '2018-06-20'
            AND car.MemberID = m.MemberID))
			
        AND m.[Category] > 1
        AND m.[Status] = 1
        --AND (bm.BindingDate IS NOT NULL OR ph.ID IS NOT NULL)
        AND ((bm.BindingDate IS NOT NULL OR ph.ID IS NOT NULL) OR (m.MemberID in (234160,232894) AND bm.BindingDate IS NULL))
		--AND m.MemberID = 204439
        ORDER BY m.MemberID
    """

    #HOT台數
    hot_sq = f"""
        DECLARE @HotCarBatchID INT
        SELECT @HotCarBatchID = Max(ID) FROM [ABCCar].[dbo].[HotCarBatch] WITH (NOLOCK)

        SELECT ACC,CERSTS INTO #ACC FROM abccar.dbo.HotCar WITH (NOLOCK) WHERE [HotCarBatchID] = @HotCarBatchID

        SELECT
            (SELECT COUNT(1) FROM #ACC B WHERE B.[ACC] = A.[HOTMemberID]) AS [HOT官網總台數]
            ,(SELECT COUNT(1) FROM #ACC C WHERE C.[ACC] = A.[HOTMemberID] AND C.CERSTS = 4) AS [HOT官網認證台數]
            ,[MemberID]
            ,[HOTMemberID] AS [HOT主帳號]
        FROM abccar.dbo.[HotCarMember] A WITH (NOLOCK)
        WHERE A.[IsMaster] = 1
        ORDER BY [HOT官網總台數] desc

        DROP TABLE #ACC
    """

    #每月業績目標
    target_sq = f"""
        select * from report.target
        where Date = '{first_day}'
        ORDER BY FIELD(ID, 90, 102, 109, 60, 101, 99, 116, 112, 61, 1, 2, 3, 4, 5)
    """

    #聯盟館本月台數
    #allianceThisMonth_sq = f"""
    #    select * from report.alliance
    #    where Date = '{end_date}'
    #"""
    allianceThisMonth_sq = f"""
        SELECT A.`Date` as `Date`,IFNULL(B.HotCarBatchID,0) as HotCarBatchID,IFNULL(B.Number,0) as Number  
        from (SELECT '{end_date}'  COLLATE utf8_general_ci  as `Date`) A 
        Left join report.alliance B on A.`Date`  COLLATE utf8_general_ci  = B.`Date`
    """

    #聯盟館上月底架上
    #allianceLastMonth_sq = f"""
    #    select * from report.alliance
    #    where Date = '{lastMonth_end_day}'
    #"""
    allianceLastMonth_sq = f"""
        SELECT A.`Date` as `Date`,IFNULL(B.HotCarBatchID,0) as HotCarBatchID,IFNULL(B.Number,0) as Number  
        from (SELECT '{lastMonth_end_day}'  COLLATE utf8_general_ci  as `Date`) A 
        Left join report.alliance B on A.`Date`  COLLATE utf8_general_ci  = B.`Date`
    """

    #CPO本月台數
    #cpoThisMonth_sq = f"""
    #    select * from report.cpo
    #    where Date = '{end_date}'
    #"""
    cpoThisMonth_sq = f"""
    select A.Date as Date,
    IFNULL(B.TCPO,0) as TCPO,
    IFNULL(B.LCPO,0) as LCPO,
    IFNULL(B.OtherCPO,0) as OtherCPO,
    IFNULL(B.Total,0) as Total
     from (SELECT '{end_date}'  COLLATE utf8_general_ci  as Date) A 
    Left join report.cpo B on A.Date  COLLATE utf8_general_ci  = B.Date
    """

    #CPO上月底架上台數
    #cpoLastMonth_sq = f"""
    #    select Total 上月底架上台數 from report.cpo
    #    where Date = '{lastMonth_end_day}'
    #"""
    cpoLastMonth_sq = f"""
    select IFNULL(B.Total,0) as 上月底架上台數 
     from (SELECT '{lastMonth_end_day}' COLLATE utf8_general_ci  as Date) A 
    Left join report.cpo B on A.Date COLLATE utf8_general_ci  = B.Date 
    """

    # 物件總瀏覽數
    query1 = f"""
        select substr(date, 1, 10) Date, sum(totalEvents) from report.ga_main
        where eventAction like '/car/' and date >= '{end_date}' and date <= '{now}' group by date
    """

    # 網站整體成效(總瀏覽數, 到站人次, 到站新客)
    query2 = f"""
        select a.QueryDate Date, a.visitor, a.New_visitor, a.all from CollectGA.ga_visitor_report_new a
        where QueryDate >= '{end_date}' and 
        QueryDate <= '{now}'
    """

    #昨日總上架數
    #query3 = f"""
    #    select date Date,  count(*) Count from report.status_new
    #    where date = '{end_date}' group by date
    #"""
    query3 = f"""
        SELECT A.`Date` as `Date`,
        (SELECT count(`Date`) FROM report.status_new B WHERE A.`Date` = B.date ) as Count
        FROM (SELECT '{end_date}' COLLATE utf8_general_ci as `Date`) A 
    """
    
    #上月總上架數
    #query4 = f"""
    #    select date Date,  count(*) Count from report.status_new
    #    where date = '{lastMonth_end_day}' group by date
    #"""
    query4 = f"""
        SELECT A.`Date` as `Date`,
        (SELECT count(`Date`) FROM report.status_new B WHERE A.`Date` = B.date ) as Count
        FROM (SELECT '{lastMonth_end_day}' COLLATE utf8_general_ci as `Date`) A 
    """
    
    #昨日上架數,依區域&架上天數分類(上架天數)
    query5 = f"""
        SELECT Date,ZoneID,DayType,Count(CarID) as CarNum FROM ( 
            SELECT Date,ZoneID,CarID, (CASE WHEN OnlineDays<=30 THEN 1 
                                       WHEN OnlineDays<=60 THEN 2 
                                       WHEN OnlineDays<=90 THEN 3 
                                       WHEN OnlineDays<=180 THEN 4
                                       WHEN OnlineDays<=360 THEN 5 
                                       ELSE 6 END) AS DayType 
            FROM `status_new_detail` WHERE Date=(SELECT max(Date) FROM status_new_detail) and Category<>'4' 
        ) m group by Date,ZoneID,DayType
    """
    
    #當日過戶收費
    q5 = f"""
    SELECT 
        ISNULL(SUM(cto.OrderTotal)-SUM(cto.RefundedAmount), 0) as [當日過戶收費],
	    COUNT(*) as [當日訂單筆數]
    FROM [ABCCar].[dbo].CarTransferOrder cto WITH (NOLOCK) 
    WHERE cto.OrderStatusID = 30 AND cto.PaymentStatusID IN (30, 40, 45) AND cto.OrderTotal > 0
    AND cto.CreateDate >= '{end_date} 00:00:00'
    AND cto.CreateDate < '{now} 00:00:00'
    """

    #當月過戶收費
    q6 = f"""
    SELECT 
        ISNULL(SUM(cto.OrderTotal)-SUM(cto.RefundedAmount), 0) as [當月過戶收費],
	    COUNT(*) as [當月訂單筆數]
    FROM [ABCCar].[dbo].CarTransferOrder cto WITH (NOLOCK) 
    WHERE cto.OrderStatusID = 30 AND cto.PaymentStatusID IN (30, 40, 45) AND cto.OrderTotal > 0
    AND cto.CreateDate >= '{first_day} 00:00:00'
    AND cto.CreateDate < '{nextMonth_first_day} 00:00:00'
    """

    #當日應收催收款
    q7 = f"""
    SELECT 
        ISNULL(SUM(cto.OrderTotal), 0) as [當日應收催收款],
	    COUNT(*) as [當日訂單筆數]
    FROM [ABCCar].[dbo].CarTransferOrder cto WITH (NOLOCK) 
    WHERE cto.OrderStatusID = 10 AND cto.PaymentStatusID = 10 AND cto.OrderTotal > 0
    AND cto.CreateDate >= '{end_date} 00:00:00'
    AND cto.CreateDate < '{now} 00:00:00'
    """

    #當月應收催收款
    q8 = f"""
    SELECT 
        ISNULL(SUM(cto.OrderTotal), 0) as [當月應收催收款],
	    COUNT(*) as [當月訂單筆數]
    FROM [ABCCar].[dbo].CarTransferOrder cto WITH (NOLOCK) 
    WHERE cto.OrderStatusID = 10 AND cto.PaymentStatusID = 10 AND cto.OrderTotal > 0
    AND cto.CreateDate >= '{first_day} 00:00:00'
    AND cto.CreateDate < '{nextMonth_first_day} 00:00:00'
    """

    #執行讀取程式
    # MSSQL
    memberAgree = read_test_mssql(memberAgree_sg, 'ABCCar')
    hotDF = read_test_mssql(hot_sq, 'ABCCar')
    q5rtn = read_test_mssql(q5, 'ABCCar')
    q6rtn = read_test_mssql(q6, 'ABCCar')
    q7rtn = read_test_mssql(q7, 'ABCCar')
    q8rtn = read_test_mssql(q8, 'ABCCar')
    
    # MySQL
    target = read_mysql(target_sq, 'report')
    aTM = read_mysql(allianceThisMonth_sq, 'report')
    aLM = read_mysql(allianceLastMonth_sq, 'report')
    cTM = read_mysql(cpoThisMonth_sq, 'report')
    cLM = read_mysql(cpoLastMonth_sq, 'report')
    #GACar = read_mysql(query1, 'report')
    #GAWeb = read_mysql(query2, 'report')
    allCar = read_mysql(query3, 'report')
    allCarLM = read_mysql(query4, 'report')
    allCarDetail = read_mysql(query5, 'report')
    
    df_hot = pd.merge(memberAgree, hotDF, on="MemberID", how='left')

    #Check data length
    #print(len(df_hot))

    #確保資料只有唯一的memberID，刪除重複memberID，只保留一筆
    df_hot = df_hot.drop_duplicates(subset=['MemberID'])
    
    #Check data length
    #print(len(df_hot))

    #return df_hot, target, aTM, aLM, cTM, cLM, GACar, GAWeb, allCar, allCarLM, allCarDetail, q5rtn, q6rtn, q7rtn, q8rtn
    return df_hot, target, aTM, aLM, cTM, cLM, allCar, allCarLM, allCarDetail, q5rtn, q6rtn, q7rtn, q8rtn

# 讀取暫存的上月底台數檔案
def addStageLastMonth(df):
    StageLastMonth = pd.read_csv('stageLastMonth_'+ str(lastMonth_end_day) +'.csv') 
    
    tmp = StageLastMonth[['MemberID','上月底架上台數']]
    tmp = tmp.drop_duplicates(subset=['MemberID'])

    mergeDf = pd.merge(df, tmp, on="MemberID", how='left')
    mergeDf['上月底架上台數'] = mergeDf['上月底架上台數'].fillna(0)
    
    return mergeDf

# 總表運算
def processData(df, target, aTM, aLM, cTM, cLM, allCar, allCarLM, allCarDetail):
    
    # ** 重要須注意 (日報上業務序號有異動增或減都需要調整序號)
    # ** salesNumber 須與 [ABCCar].[dbo].[UserAccountSales] 的欄位 [NameSeq] (維護業務序號) 相同
    # ** ID in (90, 102, 109, 60, 101, 99, 116, 112, 61)
    
    # 用序號篩選業務
    salesNumber = [1, 2, 3, 4, 5, 6, 7, 8, 9]

    df = df.loc[df['維護業務序號'].isin(salesNumber)]
    
    #TOP table---------start

    #本月台數 20220304新增
    #本月未分配 = mysql總和 - 車商 - 原廠
    a = int(allCar['Count']) - int(sum(df['本月架上車輛'])) - int(cTM['Total'])
    #上月未分配 = mysql總和 - 車商 - 原廠
    b = int(allCarLM['Count']) - int(sum(df['上月底架上台數'])) - int(cLM['上月底架上台數'])

    carThisMonth = []
    carThisMonth.append(int(sum(df['本月架上車輛']))) #車商
    carThisMonth.append(a) #未分配 
    carThisMonth.append(int(aTM['Number'])) #聯盟館
    carThisMonth.append(int(cTM['Total'])) #原廠館
    carThisMonth.append(int(allCar['Count']) + int(aTM['Number'])) #總和 = mysql總和 + 聯盟館

    #上月底架上台數 20220304新增
    carLastMonth = []
    carLastMonth.append(int(sum(df['上月底架上台數']))) #車商
    carLastMonth.append(b) #未分配 = mysql總和 - 車商 - 原廠
    carLastMonth.append(int(aLM['Number'])) #聯盟館
    carLastMonth.append(int(cLM['上月底架上台數'])) #原廠館
    carLastMonth.append(int(allCarLM['Count']) + int(aLM['Number'])) #總和

    #淨增加 20220304新增
    carIncrease = []
    carIncrease.append(int(sum(df['本月架上車輛'])) - int(sum(df['上月底架上台數']))) #車商
    carIncrease.append(a - b)
    carIncrease.append(int(aTM['Number']) - int(aLM['Number'])) #聯盟館
    carIncrease.append(int(cTM['Total']) - int(cLM['上月底架上台數'])) #原廠館
    carIncrease.append(sum(carIncrease)) #總和

    #車商本月新上架
    carThisMonthNew = [] 
    carThisMonthNew.append(sum(df['本月新上(新)'])) 
    #車商本月下架
    carThisMonthOff = []
    carThisMonthOff.append(sum(df['本月下架車輛']))

    #TOP table---------end

    # 業務 ID
    salesID = [90, 102, 109, 60, 101, 99, 116, 112, 61]
    # TL ID (報表定義，未寫入abc的DB)
    TLID = [1, 2, 3, 4, 5]

    #業務新上架目標台數
    targetSalesUpload = target.loc[target['ID'].isin(salesID)]['UploadTarget'].tolist()
    targetSalesUpload.append(sum(targetSalesUpload))
    #業務淨增加目標台數
    targetSalesIncrease = target.loc[target['ID'].isin(salesID)]['IncreaseTarget'].tolist()
    targetSalesIncrease.append(sum(targetSalesIncrease))
    #業務廣宣銷售目標
    #targetSalesPromote = target.loc[target['ID'].isin(salesID)]['PromoteTarget'].tolist()
    #targetSalesPromote.append(sum(targetSalesPromote))

    #TL新上架目標台數
    targetTLUpload = target.loc[target['ID'].isin(TLID)]['UploadTarget'].tolist()
    targetTLUpload.append(sum(targetTLUpload))
    #TL淨增加目標台數
    targetTLIncrease = target.loc[target['ID'].isin(TLID)]['IncreaseTarget'].tolist()
    targetTLIncrease.append(sum(targetTLIncrease))

    #新上架目標台數
    # NewUploadTarget = [350, 250, 950, 350, 280, 350, 250, 200, 350, 500, 500, 4330]
    NewUploadTarget = targetSalesUpload
    
    #新上架實績台數
    NewUploadAct = []
    tmpA = df.loc[(df['維護業務ID'] == 90), :]  #孫和莆
    tmpB = df.loc[(df['維護業務ID'] == 102), :] #邱雅凌
    tmpC = df.loc[(df['維護業務ID'] == 109), :] #林賢宏
    tmpD = df.loc[(df['維護業務ID'] == 60), :]  #陳玫君
    tmpE = df.loc[(df['維護業務ID'] == 101), :] #童嘉如
    tmpF = df.loc[(df['維護業務ID'] == 99), :]  #黃煒凱
    tmpG = df.loc[(df['維護業務ID'] == 116), :] #吳美珊
    tmpH = df.loc[(df['維護業務ID'] == 112), :] #林心沛
    tmpI = df.loc[(df['維護業務ID'] == 61), :]  #陳億業
    #tmpJ = df.loc[(df['維護業務ID'] == 61), :]  #陳億業
    #tmpJ = df.loc[(df['維護業務ID'] == 108), :] #黃枚琴
    #tmpK = df.loc[(df['維護業務ID'] == 61), :]  #陳億業
    #tmpH = df.loc[(df['維護業務ID'] == 111), :] #蔡依翎
    #tmpI = df.loc[(df['維護業務ID'] == 108), :] #黃枚琴
    #tmpJ = df.loc[(df['維護業務ID'] == 61), :] #陳億業

    
    NewUploadAct.append(tmpA.sum()['新上架實績台數']) 
    NewUploadAct.append(tmpB.sum()['新上架實績台數'])
    NewUploadAct.append(tmpC.sum()['新上架實績台數']) 
    NewUploadAct.append(tmpD.sum()['新上架實績台數'])
    NewUploadAct.append(tmpE.sum()['新上架實績台數'])
    NewUploadAct.append(tmpF.sum()['新上架實績台數'])
    NewUploadAct.append(tmpG.sum()['新上架實績台數'])
    NewUploadAct.append(tmpH.sum()['新上架實績台數'])
    NewUploadAct.append(tmpI.sum()['新上架實績台數'])
    #NewUploadAct.append(tmpJ.sum()['新上架實績台數'])
    #NewUploadAct.append(tmpK.sum()['新上架實績台數'])
    #totalA = tmpA.sum()['新上架實績台數'] + tmpB.sum()['新上架實績台數'] +tmpC.sum()['新上架實績台數']+tmpD.sum()['新上架實績台數']+tmpE.sum()['新上架實績台數']+tmpF.sum()['新上架實績台數']+tmpG.sum()['新上架實績台數']+tmpH.sum()['新上架實績台數']+tmpI.sum()['新上架實績台數']+tmpJ.sum()['新上架實績台數']+tmpK.sum()['新上架實績台數']
    #totalA = tmpA.sum()['新上架實績台數'] + tmpB.sum()['新上架實績台數'] +tmpC.sum()['新上架實績台數']+tmpD.sum()['新上架實績台數']+tmpE.sum()['新上架實績台數']+tmpF.sum()['新上架實績台數']+tmpG.sum()['新上架實績台數']+tmpH.sum()['新上架實績台數']+tmpI.sum()['新上架實績台數']+tmpJ.sum()['新上架實績台數']
    totalA = tmpA.sum()['新上架實績台數'] + tmpB.sum()['新上架實績台數'] +tmpC.sum()['新上架實績台數']+tmpD.sum()['新上架實績台數']+tmpE.sum()['新上架實績台數']+tmpF.sum()['新上架實績台數']+tmpG.sum()['新上架實績台數']+tmpH.sum()['新上架實績台數']+tmpI.sum()['新上架實績台數']
    #totalA = tmpA.sum()['新上架實績台數'] + tmpB.sum()['新上架實績台數'] +tmpC.sum()['新上架實績台數']+tmpD.sum()['新上架實績台數']+tmpE.sum()['新上架實績台數']+tmpF.sum()['新上架實績台數']+tmpG.sum()['新上架實績台數']+tmpH.sum()['新上架實績台數']
    NewUploadAct.append(totalA) #總和

    #新上架達成率
    NewUploadAR = []
    for i, v in enumerate(NewUploadTarget):
        NewUploadAR.append(NewUploadAct[i]/v)
    """
    #淨增加目標台數
    # NewIncreaseTarget = [100, 80, 180, 100, 80, 100, 80, 64, 100, 120, 120, 1124]
    NewIncreaseTarget = targetSalesIncrease

    #淨增加實績台數
    NewIncreaseAct = []
    NewIncreaseAct.append(tmpA.sum()['本月淨增加(新)']) 
    NewIncreaseAct.append(tmpB.sum()['本月淨增加(新)'])
    NewIncreaseAct.append(tmpC.sum()['本月淨增加(新)']) 
    NewIncreaseAct.append(tmpD.sum()['本月淨增加(新)'])
    NewIncreaseAct.append(tmpE.sum()['本月淨增加(新)'])
    NewIncreaseAct.append(tmpF.sum()['本月淨增加(新)'])
    NewIncreaseAct.append(tmpG.sum()['本月淨增加(新)'])
    NewIncreaseAct.append(tmpH.sum()['本月淨增加(新)'])
    NewIncreaseAct.append(tmpI.sum()['本月淨增加(新)'])
    NewIncreaseAct.append(tmpJ.sum()['本月淨增加(新)'])
    NewIncreaseAct.append(tmpK.sum()['本月淨增加(新)'])        
    totalB = tmpA.sum()['本月淨增加(新)'] + tmpB.sum()['本月淨增加(新)'] +tmpC.sum()['本月淨增加(新)']+tmpD.sum()['本月淨增加(新)']+tmpE.sum()['本月淨增加(新)']+tmpF.sum()['本月淨增加(新)']+tmpG.sum()['本月淨增加(新)']+tmpH.sum()['本月淨增加(新)']+tmpI.sum()['本月淨增加(新)']+tmpJ.sum()['本月淨增加(新)']+tmpK.sum()['本月淨增加(新)']
    #totalB = tmpA.sum()['本月淨增加(新)'] + tmpB.sum()['本月淨增加(新)'] +tmpC.sum()['本月淨增加(新)']+tmpD.sum()['本月淨增加(新)']+tmpE.sum()['本月淨增加(新)']+tmpF.sum()['本月淨增加(新)']+tmpG.sum()['本月淨增加(新)']+tmpH.sum()['本月淨增加(新)']+tmpI.sum()['本月淨增加(新)']+tmpJ.sum()['本月淨增加(新)']
    NewIncreaseAct.append(totalB) #總和

    #淨增加達成率
    NewIncreaseAR = []
    for i, v in enumerate(NewIncreaseTarget):
        NewIncreaseAR.append(NewIncreaseAct[i]/v)
    """
    tmpANewMember = df.loc[(df['維護業務ID'] == 90) & (((df['最新儲值時間'] >= str(first_day)) & (df['最新儲值時間'] <= str(nextMonth_first_day))) | ((df['信用卡綁定時間'] >= str(first_day)) & (df['信用卡綁定時間'] <= str(nextMonth_first_day))) ), :] 
    tmpBNewMember = df.loc[(df['維護業務ID'] == 102) & (((df['最新儲值時間'] >= str(first_day)) & (df['最新儲值時間'] <= str(nextMonth_first_day))) | ((df['信用卡綁定時間'] >= str(first_day)) & (df['信用卡綁定時間'] <= str(nextMonth_first_day))) ), :] 
    tmpCNewMember = df.loc[(df['維護業務ID'] == 109) & (((df['最新儲值時間'] >= str(first_day)) & (df['最新儲值時間'] <= str(nextMonth_first_day))) | ((df['信用卡綁定時間'] >= str(first_day)) & (df['信用卡綁定時間'] <= str(nextMonth_first_day))) ), :] 
    tmpDNewMember = df.loc[(df['維護業務ID'] == 60) & (((df['最新儲值時間'] >= str(first_day)) & (df['最新儲值時間'] <= str(nextMonth_first_day))) | ((df['信用卡綁定時間'] >= str(first_day)) & (df['信用卡綁定時間'] <= str(nextMonth_first_day))) ), :] 
    tmpENewMember = df.loc[(df['維護業務ID'] == 101) & (((df['最新儲值時間'] >= str(first_day)) & (df['最新儲值時間'] <= str(nextMonth_first_day))) | ((df['信用卡綁定時間'] >= str(first_day)) & (df['信用卡綁定時間'] <= str(nextMonth_first_day))) ), :] 
    tmpFNewMember = df.loc[(df['維護業務ID'] == 99) & (((df['最新儲值時間'] >= str(first_day)) & (df['最新儲值時間'] <= str(nextMonth_first_day))) | ((df['信用卡綁定時間'] >= str(first_day)) & (df['信用卡綁定時間'] <= str(nextMonth_first_day))) ), :] 
    tmpGNewMember = df.loc[(df['維護業務ID'] == 116) & (((df['最新儲值時間'] >= str(first_day)) & (df['最新儲值時間'] <= str(nextMonth_first_day))) | ((df['信用卡綁定時間'] >= str(first_day)) & (df['信用卡綁定時間'] <= str(nextMonth_first_day))) ), :]    
    tmpHNewMember = df.loc[(df['維護業務ID'] == 112) & (((df['最新儲值時間'] >= str(first_day)) & (df['最新儲值時間'] <= str(nextMonth_first_day))) | ((df['信用卡綁定時間'] >= str(first_day)) & (df['信用卡綁定時間'] <= str(nextMonth_first_day))) ), :] 
    tmpINewMember = df.loc[(df['維護業務ID'] == 61) & (((df['最新儲值時間'] >= str(first_day)) & (df['最新儲值時間'] <= str(nextMonth_first_day))) | ((df['信用卡綁定時間'] >= str(first_day)) & (df['信用卡綁定時間'] <= str(nextMonth_first_day))) ), :] 
    #tmpJNewMember = df.loc[(df['維護業務ID'] == 61) & (((df['最新儲值時間'] >= str(first_day)) & (df['最新儲值時間'] <= str(nextMonth_first_day))) | ((df['信用卡綁定時間'] >= str(first_day)) & (df['信用卡綁定時間'] <= str(nextMonth_first_day))) ), :] 
    #tmpJNewMember = df.loc[(df['維護業務ID'] == 108) & (((df['最新儲值時間'] >= str(first_day)) & (df['最新儲值時間'] <= str(nextMonth_first_day))) | ((df['信用卡綁定時間'] >= str(first_day)) & (df['信用卡綁定時間'] <= str(nextMonth_first_day))) ), :] 
    #tmpKNewMember = df.loc[(df['維護業務ID'] == 61) & (((df['最新儲值時間'] >= str(first_day)) & (df['最新儲值時間'] <= str(nextMonth_first_day))) | ((df['信用卡綁定時間'] >= str(first_day)) & (df['信用卡綁定時間'] <= str(nextMonth_first_day))) ), :] 
    #tmpHNewMember = df.loc[(df['維護業務ID'] == 111) & (((df['最新儲值時間'] >= str(first_day)) & (df['最新儲值時間'] <= str(nextMonth_first_day))) | ((df['信用卡綁定時間'] >= str(first_day)) & (df['信用卡綁定時間'] <= str(nextMonth_first_day))) ), :] 
    #tmpINewMember = df.loc[(df['維護業務ID'] == 108) & (((df['最新儲值時間'] >= str(first_day)) & (df['最新儲值時間'] <= str(nextMonth_first_day))) | ((df['信用卡綁定時間'] >= str(first_day)) & (df['信用卡綁定時間'] <= str(nextMonth_first_day))) ), :] 
    #tmpJNewMember = df.loc[(df['維護業務ID'] == 61) & (((df['最新儲值時間'] >= str(first_day)) & (df['最新儲值時間'] <= str(nextMonth_first_day))) | ((df['信用卡綁定時間'] >= str(first_day)) & (df['信用卡綁定時間'] <= str(nextMonth_first_day))) ), :] 

    #本月新增家數
    NewMember = []
    NewMember.append(tmpANewMember.count()['維護業務ID']) #孫和莆 
    NewMember.append(tmpBNewMember.count()['維護業務ID']) #邱雅凌 
    NewMember.append(tmpCNewMember.count()['維護業務ID']) #林賢宏
    NewMember.append(tmpDNewMember.count()['維護業務ID']) #陳玫君
    NewMember.append(tmpENewMember.count()['維護業務ID']) #童嘉如
    NewMember.append(tmpFNewMember.count()['維護業務ID']) #黃煒凱
    NewMember.append(tmpGNewMember.count()['維護業務ID']) #吳美珊
    NewMember.append(tmpHNewMember.count()['維護業務ID']) #林心沛
    NewMember.append(tmpINewMember.count()['維護業務ID']) #陳億業
    #NewMember.append(tmpJNewMember.count()['維護業務ID']) #陳億業
    #NewMember.append(tmpJNewMember.count()['維護業務ID']) #黃枚琴
    #NewMember.append(tmpKNewMember.count()['維護業務ID']) #陳億業
    #NewMember.append(tmpHNewMember.count()['維護業務ID']) #蔡依翎
    #NewMember.append(tmpINewMember.count()['維護業務ID']) #黃枚琴
    #NewMember.append(tmpJNewMember.count()['維護業務ID']) #陳億業
    #totalC = tmpANewMember.count()['維護業務ID'] + tmpBNewMember.count()['維護業務ID'] +tmpCNewMember.count()['維護業務ID']+tmpDNewMember.count()['維護業務ID']+tmpENewMember.count()['維護業務ID']+tmpFNewMember.count()['維護業務ID']+tmpGNewMember.count()['維護業務ID']+tmpHNewMember.count()['維護業務ID']+tmpINewMember.count()['維護業務ID']+tmpJNewMember.count()['維護業務ID']+tmpKNewMember.count()['維護業務ID']
    #totalC = tmpANewMember.count()['維護業務ID'] + tmpBNewMember.count()['維護業務ID'] +tmpCNewMember.count()['維護業務ID']+tmpDNewMember.count()['維護業務ID']+tmpENewMember.count()['維護業務ID']+tmpFNewMember.count()['維護業務ID']+tmpGNewMember.count()['維護業務ID']+tmpHNewMember.count()['維護業務ID']+tmpINewMember.count()['維護業務ID']+tmpJNewMember.count()['維護業務ID']
    totalC = tmpANewMember.count()['維護業務ID'] + tmpBNewMember.count()['維護業務ID'] +tmpCNewMember.count()['維護業務ID']+tmpDNewMember.count()['維護業務ID']+tmpENewMember.count()['維護業務ID']+tmpFNewMember.count()['維護業務ID']+tmpGNewMember.count()['維護業務ID']+tmpHNewMember.count()['維護業務ID']+tmpINewMember.count()['維護業務ID']
    #totalC = tmpANewMember.count()['維護業務ID'] + tmpBNewMember.count()['維護業務ID'] +tmpCNewMember.count()['維護業務ID']+tmpDNewMember.count()['維護業務ID']+tmpENewMember.count()['維護業務ID']+tmpFNewMember.count()['維護業務ID']+tmpGNewMember.count()['維護業務ID']+tmpHNewMember.count()['維護業務ID']
    NewMember.append(totalC) #總和    

    #本月新會員新上台數
    NewMemberUpload = []
    NewMemberUpload.append(tmpANewMember.sum()['本月新上(新)']) #孫和莆 
    NewMemberUpload.append(tmpBNewMember.sum()['本月新上(新)']) #邱雅凌 
    NewMemberUpload.append(tmpCNewMember.sum()['本月新上(新)']) #林賢宏 
    NewMemberUpload.append(tmpDNewMember.sum()['本月新上(新)']) #陳玫君
    NewMemberUpload.append(tmpENewMember.sum()['本月新上(新)']) #童嘉如 
    NewMemberUpload.append(tmpFNewMember.sum()['本月新上(新)']) #黃煒凱
    NewMemberUpload.append(tmpGNewMember.sum()['本月新上(新)']) #吳美珊
    NewMemberUpload.append(tmpHNewMember.sum()['本月新上(新)']) #林心沛
    NewMemberUpload.append(tmpINewMember.sum()['本月新上(新)']) #陳億業
    #NewMemberUpload.append(tmpJNewMember.sum()['本月新上(新)']) #陳億業
    #NewMemberUpload.append(tmpJNewMember.sum()['本月新上(新)']) #黃枚琴
    #NewMemberUpload.append(tmpKNewMember.sum()['本月新上(新)']) #陳億業
    #NewMemberUpload.append(tmpHNewMember.sum()['本月新上(新)']) #蔡依翎
    #NewMemberUpload.append(tmpINewMember.sum()['本月新上(新)']) #黃枚琴
    #NewMemberUpload.append(tmpJNewMember.sum()['本月新上(新)']) #陳億業
    #totalD = tmpANewMember.sum()['本月新上(新)'] + tmpBNewMember.sum()['本月新上(新)'] +tmpCNewMember.sum()['本月新上(新)']+tmpDNewMember.sum()['本月新上(新)']+tmpENewMember.sum()['本月新上(新)']+tmpFNewMember.sum()['本月新上(新)']+tmpGNewMember.sum()['本月新上(新)']+tmpHNewMember.sum()['本月新上(新)']+tmpINewMember.sum()['本月新上(新)']+tmpJNewMember.sum()['本月新上(新)']+tmpKNewMember.sum()['本月新上(新)']
    #totalD = tmpANewMember.sum()['本月新上(新)'] + tmpBNewMember.sum()['本月新上(新)'] +tmpCNewMember.sum()['本月新上(新)']+tmpDNewMember.sum()['本月新上(新)']+tmpENewMember.sum()['本月新上(新)']+tmpFNewMember.sum()['本月新上(新)']+tmpGNewMember.sum()['本月新上(新)']+tmpHNewMember.sum()['本月新上(新)']+tmpINewMember.sum()['本月新上(新)']+tmpJNewMember.sum()['本月新上(新)']
    totalD = tmpANewMember.sum()['本月新上(新)'] + tmpBNewMember.sum()['本月新上(新)'] +tmpCNewMember.sum()['本月新上(新)']+tmpDNewMember.sum()['本月新上(新)']+tmpENewMember.sum()['本月新上(新)']+tmpFNewMember.sum()['本月新上(新)']+tmpGNewMember.sum()['本月新上(新)']+tmpHNewMember.sum()['本月新上(新)']+tmpINewMember.sum()['本月新上(新)']
    #totalD = tmpANewMember.sum()['本月新上(新)'] + tmpBNewMember.sum()['本月新上(新)'] +tmpCNewMember.sum()['本月新上(新)']+tmpDNewMember.sum()['本月新上(新)']+tmpENewMember.sum()['本月新上(新)']+tmpFNewMember.sum()['本月新上(新)']+tmpGNewMember.sum()['本月新上(新)']+tmpHNewMember.sum()['本月新上(新)']
    NewMemberUpload.append(totalD) #總和    

    #店均獎金家數
    Bonus = []
    tmpABonus = df.loc[(df['維護業務ID'] == 90) & ((df['本月架上驗證成功車輛'] >= 20) & (df['本月新上(新)'] >=7 )), :]
    tmpBBonus = df.loc[(df['維護業務ID'] == 102) & ((df['本月架上驗證成功車輛'] >= 20) & (df['本月新上(新)'] >=7 )), :]
    tmpCBonus = df.loc[(df['維護業務ID'] == 109) & ((df['本月架上驗證成功車輛'] >= 20) & (df['本月新上(新)'] >=7 )), :]
    tmpDBonus = df.loc[(df['維護業務ID'] == 60) & ((df['本月架上驗證成功車輛'] >= 20) & (df['本月新上(新)'] >=7 )), :]
    tmpEBonus = df.loc[(df['維護業務ID'] == 101) & ((df['本月架上驗證成功車輛'] >= 20) & (df['本月新上(新)'] >=7 )), :]
    tmpFBonus = df.loc[(df['維護業務ID'] == 99) & ((df['本月架上驗證成功車輛'] >= 20) & (df['本月新上(新)'] >=7 )), :]
    tmpGBonus = df.loc[(df['維護業務ID'] == 116) & ((df['本月架上驗證成功車輛'] >= 20) & (df['本月新上(新)'] >=7 )), :]
    tmpHBonus = df.loc[(df['維護業務ID'] == 112) & ((df['本月架上驗證成功車輛'] >= 20) & (df['本月新上(新)'] >=7 )), :]
    tmpIBonus = df.loc[(df['維護業務ID'] == 61) & ((df['本月架上驗證成功車輛'] >= 20) & (df['本月新上(新)'] >=7 )), :]
    #tmpJBonus = df.loc[(df['維護業務ID'] == 61) & ((df['本月架上驗證成功車輛'] >= 20) & (df['本月新上(新)'] >=7 )), :]
    #tmpJBonus = df.loc[(df['維護業務ID'] == 108) & ((df['本月架上驗證成功車輛'] >= 20) & (df['本月新上(新)'] >=7 )), :]
    #tmpKBonus = df.loc[(df['維護業務ID'] == 61) & ((df['本月架上驗證成功車輛'] >= 20) & (df['本月新上(新)'] >=7 )), :]
    #tmpHBonus = df.loc[(df['維護業務ID'] == 111) & ((df['本月架上驗證成功車輛'] >= 20) & (df['本月新上(新)'] >=7 )), :]
    #tmpIBonus = df.loc[(df['維護業務ID'] == 108) & ((df['本月架上驗證成功車輛'] >= 20) & (df['本月新上(新)'] >=7 )), :]
    #tmpJBonus = df.loc[(df['維護業務ID'] == 61) & ((df['本月架上驗證成功車輛'] >= 20) & (df['本月新上(新)'] >=7 )), :]

    Bonus.append(tmpABonus.count()['維護業務ID']) #孫和莆 
    Bonus.append(tmpBBonus.count()['維護業務ID']) #邱雅凌 
    Bonus.append(tmpCBonus.count()['維護業務ID']) #林賢宏
    Bonus.append(tmpDBonus.count()['維護業務ID']) #陳玫君
    Bonus.append(tmpEBonus.count()['維護業務ID']) #童嘉如 
    Bonus.append(tmpFBonus.count()['維護業務ID']) #黃煒凱
    Bonus.append(tmpGBonus.count()['維護業務ID']) #吳美珊
    Bonus.append(tmpHBonus.count()['維護業務ID']) #林心沛
    Bonus.append(tmpIBonus.count()['維護業務ID']) #陳億業
    #Bonus.append(tmpJBonus.count()['維護業務ID']) #陳億業
    #Bonus.append(tmpJBonus.count()['維護業務ID']) #黃枚琴
    #Bonus.append(tmpKBonus.count()['維護業務ID']) #陳億業
    #Bonus.append(tmpHBonus.count()['維護業務ID']) #蔡依翎
    #Bonus.append(tmpIBonus.count()['維護業務ID']) #黃枚琴
    #Bonus.append(tmpJBonus.count()['維護業務ID']) #陳億業
    #totalE = tmpABonus.count()['維護業務ID'] + tmpBBonus.count()['維護業務ID'] +tmpCBonus.count()['維護業務ID']+tmpDBonus.count()['維護業務ID']+tmpEBonus.count()['維護業務ID']+tmpFBonus.count()['維護業務ID']+tmpGBonus.count()['維護業務ID']+tmpHBonus.count()['維護業務ID']+tmpIBonus.count()['維護業務ID']+tmpJBonus.count()['維護業務ID']+tmpKBonus.count()['維護業務ID']
    #totalE = tmpABonus.count()['維護業務ID'] + tmpBBonus.count()['維護業務ID'] +tmpCBonus.count()['維護業務ID']+tmpDBonus.count()['維護業務ID']+tmpEBonus.count()['維護業務ID']+tmpFBonus.count()['維護業務ID']+tmpGBonus.count()['維護業務ID']+tmpHBonus.count()['維護業務ID']+tmpIBonus.count()['維護業務ID']+tmpJBonus.count()['維護業務ID']
    totalE = tmpABonus.count()['維護業務ID'] + tmpBBonus.count()['維護業務ID'] +tmpCBonus.count()['維護業務ID']+tmpDBonus.count()['維護業務ID']+tmpEBonus.count()['維護業務ID']+tmpFBonus.count()['維護業務ID']+tmpGBonus.count()['維護業務ID']+tmpHBonus.count()['維護業務ID']+tmpIBonus.count()['維護業務ID']
    #totalE = tmpABonus.count()['維護業務ID'] + tmpBBonus.count()['維護業務ID'] +tmpCBonus.count()['維護業務ID']+tmpDBonus.count()['維護業務ID']+tmpEBonus.count()['維護業務ID']+tmpFBonus.count()['維護業務ID']+tmpGBonus.count()['維護業務ID']+tmpHBonus.count()['維護業務ID']
    Bonus.append(totalE) #總和    

    # TL 新上架目標台數
    # 2022/03/01 新增TL ID & insert table (1, 2, 3, 4, 5)，張鈞偉、荊德龍、李明憲、陳叔毅、林士欽
    # TLNewUploadTarget = [600, 1300, 880, 550, 1000, 4330]
    TLNewUploadTarget = targetTLUpload

    # TL 新上架實績台數
    TLNewUploadAct = []
    TLNewUploadAct.append(tmpA.sum()['新上架實績台數'] + tmpB.sum()['新上架實績台數'] + tmpC.sum()['新上架實績台數'])
    TLNewUploadAct.append(tmpD.sum()['新上架實績台數'] + tmpE.sum()['新上架實績台數'])
    TLNewUploadAct.append(tmpF.sum()['新上架實績台數'] + tmpG.sum()['新上架實績台數'])
    TLNewUploadAct.append(tmpH.sum()['新上架實績台數'])
    TLNewUploadAct.append(tmpI.sum()['新上架實績台數'])
    #TLNewUploadAct.append(tmpJ.sum()['新上架實績台數'] + tmpK.sum()['新上架實績台數'])
    #TLNewUploadAct.append(tmpH.sum()['新上架實績台數'])
    #TLNewUploadAct.append(tmpI.sum()['新上架實績台數'] + tmpJ.sum()['新上架實績台數'])
    TLNewUploadAct.append(totalA) #總和

    # TL 新上架達成率
    TLNewUploadAR = []
    for i, v in enumerate(TLNewUploadTarget):
        TLNewUploadAR.append(TLNewUploadAct[i]/v)
    """
    # TL 淨增加目標台數
    # TLNewIncreaseTarget = [180, 280, 260, 164, 240, 1124]
    TLNewIncreaseTarget = targetTLIncrease
    
    # TL 淨增加實績台數
    TLNewIncreaseAct = []
    TLNewIncreaseAct.append(tmpA.sum()['本月淨增加(新)'] + tmpB.sum()['本月淨增加(新)'] + tmpC.sum()['本月淨增加(新)']) 
    TLNewIncreaseAct.append(tmpD.sum()['本月淨增加(新)'] + tmpE.sum()['本月淨增加(新)']) 
    TLNewIncreaseAct.append(tmpF.sum()['本月淨增加(新)'] + tmpG.sum()['本月淨增加(新)']) 
    TLNewIncreaseAct.append(tmpH.sum()['本月淨增加(新)'] + tmpI.sum()['本月淨增加(新)']) 
    TLNewIncreaseAct.append(tmpJ.sum()['本月淨增加(新)'] + tmpK.sum()['本月淨增加(新)']) 
    #TLNewIncreaseAct.append(tmpH.sum()['本月淨增加(新)']) 
    #TLNewIncreaseAct.append(tmpI.sum()['本月淨增加(新)'] + tmpJ.sum()['本月淨增加(新)'])
    TLNewIncreaseAct.append(totalB) #總和

    # TL 淨增加達成率
    TLNewIncreaseAR = []
    for i, v in enumerate(TLNewIncreaseTarget):
        TLNewIncreaseAR.append(TLNewIncreaseAct[i]/v)
    """
    # TL 本月新增家數
    TLNewMember = []
    TLNewMember.append(tmpANewMember.count()['維護業務ID'] + tmpBNewMember.count()['維護業務ID'] + tmpCNewMember.count()['維護業務ID'])
    TLNewMember.append(tmpDNewMember.count()['維護業務ID'] + tmpENewMember.count()['維護業務ID'])
    TLNewMember.append(tmpFNewMember.count()['維護業務ID'] + tmpGNewMember.count()['維護業務ID'])
    TLNewMember.append(tmpHNewMember.count()['維護業務ID'])
    TLNewMember.append(tmpINewMember.count()['維護業務ID'])
    #TLNewMember.append(tmpJNewMember.count()['維護業務ID'] + tmpKNewMember.count()['維護業務ID'])
    #TLNewMember.append(tmpHNewMember.count()['維護業務ID'])
    #TLNewMember.append(tmpINewMember.count()['維護業務ID'] + tmpJNewMember.count()['維護業務ID'])
    
    TLNewMember.append(totalC) #總和    

    # TL 本月新會員新上台數
    TLNewMemberUpload = []
    TLNewMemberUpload.append(tmpANewMember.sum()['本月新上(新)'] + tmpBNewMember.sum()['本月新上(新)'] + tmpCNewMember.sum()['本月新上(新)']) 
    TLNewMemberUpload.append(tmpDNewMember.sum()['本月新上(新)'] + tmpENewMember.sum()['本月新上(新)'])
    TLNewMemberUpload.append(tmpFNewMember.sum()['本月新上(新)'] + tmpGNewMember.sum()['本月新上(新)'])
    TLNewMemberUpload.append(tmpHNewMember.sum()['本月新上(新)'])
    TLNewMemberUpload.append(tmpINewMember.sum()['本月新上(新)'])
    #TLNewMemberUpload.append(tmpJNewMember.sum()['本月新上(新)'] + tmpKNewMember.sum()['本月新上(新)'])
    #TLNewMemberUpload.append(tmpHNewMember.sum()['本月新上(新)'])
    #TLNewMemberUpload.append(tmpINewMember.sum()['本月新上(新)'] + tmpJNewMember.sum()['本月新上(新)'])
    TLNewMemberUpload.append(totalD) #總和
    
    """
    df = pd.DataFrame(list(zip(NewUploadTarget, NewUploadAct, NewUploadAR, NewIncreaseTarget, NewIncreaseAct, NewIncreaseAR, NewMember, NewMemberUpload, Bonus)),
               columns =['新上架目標台數', '新上架實績台數', '新上架達成率', '淨增加目標台數', '淨增加實績台數', '淨增加達成率', '本月新增家數', '本月新會員新上台數', '店均獎金家數'])

    df_down = pd.DataFrame(list(zip(TLNewUploadTarget, TLNewUploadAct, TLNewUploadAR, TLNewIncreaseTarget, TLNewIncreaseAct, TLNewIncreaseAR, TLNewMember, TLNewMemberUpload)),
               columns =['TL新上架目標台數', 'TL新上架實績台數', 'TL新上架達成率', 'TL淨增加目標台數', 'TL淨增加實績台數', 'TL淨增加達成率', 'TL本月新增家數', 'TL本月新會員新上台數'])
    """
    df = pd.DataFrame(list(zip(NewUploadTarget, NewUploadAct, NewUploadAR, NewMember, NewMemberUpload, Bonus)),
               columns =['新上架目標台數', '新上架實績台數', '新上架達成率', '本月新增家數', '本月新會員新上台數', '店均獎金家數'])

    df_down = pd.DataFrame(list(zip(TLNewUploadTarget, TLNewUploadAct, TLNewUploadAR, TLNewMember, TLNewMemberUpload)),
               columns =['TL新上架目標台數', 'TL新上架實績台數', 'TL新上架達成率', 'TL本月新增家數', 'TL本月新會員新上台數'])
    
    # 上架台數,依區域/上架天數種類
    OnLineCar = []
    ZoneIDs = [1,2,3,4,5,0,9] #區域分類 1-北區/2-桃苗/3-中區/4-雲嘉/5-高屏/0-異常車商/9-總程
    DayTypeIDs = [1,2,3,4,5,6] #上架天數種類 1[0~30天]/2[31~60天]/3[61~90天]/4[91~180天]/5[181~360天]/6[361天以上]  
    for x in ZoneIDs: 
        ZoneRow = []
        for y in DayTypeIDs:
            if x == 9: #總和
                Zone = allCarDetail.loc[(allCarDetail['ZoneID'] >= 0) & (allCarDetail['ZoneID'] <=5),:] 
            else: #各區   
                Zone = allCarDetail.loc[(allCarDetail['ZoneID'] == x), :] #區域
            ZoneRow.append(Zone.loc[(Zone['DayType'] == y )].sum()['CarNum'].astype(int)) 
        #OnLineCar.append({x:ZoneRow})
        OnLineCar.append(ZoneRow)
    
    df_zone = pd.DataFrame(list(OnLineCar),
              columns =['30天以內', '31-60天', '61-90天', '91-180天', '181-360天', '361天以上'])

    return df, df_down, df_zone, carThisMonth, carThisMonthNew, carThisMonthOff, carIncrease, carLastMonth

# 將資料輸出至Excel
def addValueToExcel(ws_main, valueList, rowIndex, colIndex):
    for index, value in enumerate(valueList):
        grid = ws_main.cell(row=4+rowIndex+index, column=4+colIndex)
        grid.value = value

# 加工企劃定義的指標
def subThisMonthIncre(df):
    df['本月淨增加'] = df['本月新上架車輛'] - df['本月下架車輛']
    df['本月新上(新)'] = df['本月新上架車輛'] - df['2日內下架']
    df['本月下架(新)'] = df['當月下架未過戶'] + df['當月過戶未下架'] + df['當月過戶當月下架']
    df['本月淨增加(新)'] = df['本月新上(新)'] - df['本月下架(新)']

    return df

# 更改公會名稱
def changeGuildName(df):
    df['公會'] = df['公會'].replace(['台北市汽車商業同業公會','新北市汽車商業同業公會', '桃園市汽車商業同業公會', '苗栗縣汽車商業同業公會', '高雄市汽車商業同業公會', '台中市汽車商業同業公會', '基隆市汽車商業同業公會', '宜蘭縣汽車商業同業公會', '南投縣汽車商業同業公會', '台南市汽車商業同業公會', '臺南市直轄市汽車商業同業公會', '中華民國汽車商業同業公會全聯會', '臺中市直轄市汽車商業同業公會', '屏東縣汽車商業同業公會', '高雄市新汽車商業同業公會', '花蓮縣汽車商業同業公會', '嘉義市汽車商業同業公會'],['台北','新北', '桃園', '苗栗', '高雄', '台中', '基隆', '宜蘭', '南投', '台南', '台南市', '同業', '台中市', '屏東', '高雄(新)', '花蓮', '嘉義'])

    return df

# 數據寫入image中
def append_to_graph(carThisMonth, sheet5):
    # 擷取製圖需要的數據
    b1day_dt =  sheet5[0]
    print(b1day_dt)

    x1 = carThisMonth[0]#車商上架數
    x2 = carThisMonth[2]#聯盟館上架數
    x3 = carThisMonth[3]#原廠認證上架數
    x4 = carThisMonth[4]#總上架數
    x5 = sheet5[1] #到站人次
    x6 = sheet5[2] #到站新客
    x7 = sheet5[3] #總瀏覽數
    x8 = sheet5[4] #物件總瀏覽數

    labels = f'車商上架數{x1}', f'聯盟館上架數{x2}', f'原廠認證上架數{x3}', f'總上架數{x4}', \
             f'到站人次{x5}', f'到站新客{x6}', f'總瀏覽數{x7}', f'物件總瀏覽數{x8}'
    size = [10, 10, 10, 10, 10, 10, 10, 10]
    explode = (0, 0, 0, 0, 0, 0, 0, 0)

    # 設置製圖的變數
    mpl.rcParams[u'font.sans-serif'] = ['simhei']
    mpl.rcParams['axes.unicode_minus'] = False
    mpl.rcParams['font.size'] = 20.0
    mpl.rcParams['figure.figsize'] = 14, 9
    colors = ['red', 'orange', 'yellow', 'green', 'blue', 'lightblue', 'purple', 'pink']

    # 選取圖形為圓餅圖
    plt.pie(size, labels=labels, colors=colors, shadow=True, explode=explode)

    # 顯示圖中間的字體
    plt.text(-0.55, 0.03, '{0}日營運指標'.format(end_date), size=25)

    centre_circle = plt.Circle((0, 0), 0.6, color='grey', fc='white', linewidth=1)
    fig = plt.gcf()
    fig.gca().add_artist(centre_circle)
    plt.axis('equal')
    fig.savefig(path+'sales_graph.pdf')

if __name__ == "__main__":
    print(now)

    script = 'abccar_member_onshelf_withTL_new'

    try:
        #memberAgree, target, aTM, aLM, cTM, cLM, GACar, GAWeb, allCar, allCarLM, allCarDetail, q5rtn, q6rtn, q7rtn, q8rtn = ReadData() 
        memberAgree, target, aTM, aLM, cTM, cLM, allCar, allCarLM, allCarDetail, q5rtn, q6rtn, q7rtn, q8rtn = ReadData() 
        print('Part 1 done!')
    except Exception as e:
        msg = '{0}: PART 1: 數據連線讀取失敗! The error message : {1}'.format(script, e)
        send_error_message(script='{}'.format(script), message=msg)
        print(msg)
        sys.exit(1)
    
    try:
        memberAgree_new_tmp = addStageLastMonth(memberAgree)
        memberAgree_new = subThisMonthIncre(memberAgree_new_tmp)
        memberAgree_new = changeGuildName(memberAgree_new)
        print('Part 2 done!')
    except Exception as e:
        msg = '{0}: PART 2: 數據合併失敗! The error message : {1}'.format(script, e)
        send_error_message(script='{}'.format(script), message=msg)
        print(msg)
        sys.exit(1)

    try:
        df, df_down, df_zone, carThisMonth, carThisMonthNew, carThisMonthOff, carIncrease, carLastMonth = processData(memberAgree_new, target, aTM, aLM, cTM, cLM, allCar, allCarLM, allCarDetail)
        print('Part 3 done!')
    except Exception as e:
        msg = '{0}: PART 3: 數據計算出現錯誤! The error message : {1}'.format(script, e)
        send_error_message(script='{}'.format(script), message=msg)
        print(msg)
        sys.exit(1)
        
    try:
        #=========================== Load office file      
        wb = load_workbook('abc好車網_架上台數報表_企劃.xlsx')

        #Sheet 1
        ws_main = wb.worksheets[0]

        # Add today date
        date = ws_main.cell(row=2, column=3)
        date.value = 'abc好車網_營運報表_(' + end_date +')'

        #Top table
        #本月架上台數
        addValueToExcel(ws_main, carThisMonth, 0, 0)
        #本月淨增加
        addValueToExcel(ws_main, carIncrease, 0, 1)
        #上月架上台數
        addValueToExcel(ws_main, carLastMonth, 0, 2)

        # Add today date
        date = ws_main.cell(row=12, column=3)
        date.value = 'abc好車網_架上台數報表_(' + end_date +')'

        # 2022/08/10 TL 與 Sales 交換位置 
        #Middle table 
        #TL新上架目標台數
        addValueToExcel(ws_main, df_down['TL新上架目標台數'].tolist(), 10, 1)
        #TL新上架實績台數
        addValueToExcel(ws_main, df_down['TL新上架實績台數'].tolist(), 10, 2)       
        #TL新上架達成率
        addValueToExcel(ws_main, df_down['TL新上架達成率'].tolist(), 10, 3)
        """
        #TL淨增加目標台數
        addValueToExcel(ws_main, df_down['TL淨增加目標台數'].tolist(), 10, 4)
        #TL淨增加實績台數
        addValueToExcel(ws_main, df_down['TL淨增加實績台數'].tolist(), 10, 5)
        #TL淨增加達成率
        addValueToExcel(ws_main, df_down['TL淨增加達成率'].tolist(), 10, 6)
        #TL本月新增家數
        addValueToExcel(ws_main, df_down['TL本月新增家數'].tolist(), 10, 7)
        #TL本月新會員新上台數
        addValueToExcel(ws_main, df_down['TL本月新會員新上台數'].tolist(), 10, 8)
        """
        #TL本月新增家數
        addValueToExcel(ws_main, df_down['TL本月新增家數'].tolist(), 10, 4)
        #TL本月新會員新上台數
        addValueToExcel(ws_main, df_down['TL本月新會員新上台數'].tolist(), 10, 5)
        
        #Down table
        #新上架目標台數
        addValueToExcel(ws_main, df['新上架目標台數'].tolist(), 18, 0)
        #新上架實績台數
        addValueToExcel(ws_main, df['新上架實績台數'].tolist(), 18, 1)    
        #新上架達成率
        addValueToExcel(ws_main, df['新上架達成率'].tolist(), 18, 2)
        """
        #淨增加目標台數
        addValueToExcel(ws_main, df['淨增加目標台數'].tolist(), 18, 3)
        #淨增加實績台數
        addValueToExcel(ws_main, df['淨增加實績台數'].tolist(), 18, 4)
        #淨增加達成率
        addValueToExcel(ws_main, df['淨增加達成率'].tolist(), 18, 5)
        #本月新增家數
        addValueToExcel(ws_main, df['本月新增家數'].tolist(), 18, 6)
        #本月新會員新上台數
        addValueToExcel(ws_main, df['本月新會員新上台數'].tolist(), 18, 7)
        #店均獎金家數
        addValueToExcel(ws_main, df['店均獎金家數'].tolist(), 18, 8)
        """
        #本月新增家數
        addValueToExcel(ws_main, df['本月新增家數'].tolist(), 18, 3)
        #本月新會員新上台數
        addValueToExcel(ws_main, df['本月新會員新上台數'].tolist(), 18, 4)
        #店均獎金家數
        addValueToExcel(ws_main, df['店均獎金家數'].tolist(), 18, 5)

        # 2021-12-27 組業務排序邏輯字串
        memberAgree_new['維護業務'] = memberAgree_new.apply(lambda x: '0' + str(x['維護業務序號']) + ' ' + x['維護業務']  if len(str(x['維護業務序號'])) == 1 else str(x['維護業務序號']) + ' ' + x['維護業務'] , axis=1)
        memberAgree_new['維護業務'] = memberAgree_new['維護業務'].replace('00 ', '')

        #Sheet 2
        #取欄位
        df_Office = memberAgree_new[['MemberID', '會員名稱', '車商名稱', '城市別', '連絡人', '連絡電話', '身分別', '網路好店', '買萬送萬', '公會', '維護業務', 'HAA帳號', 'HOT主帳號', 'HOT官網認證台數', 'HOT官網總台數', '上月底架上台數', '本日新上架車輛' , '本日下架車輛', '本月新上架車輛', '2日內下架', '本月新上(新)', '本月下架車輛', '當月下架未過戶', '當月過戶未下架', '當月過戶當月下架', '本月下架(新)', '本月淨增加', '本月淨增加(新)', '本月架上車輛', '本月架上驗證成功車輛', 'HAA上架數', 'HAA待售數', '簡易刊登本月待售數', '簡易刊登本月上架數', '簡易刊登架上台數', '最新儲值時間', '信用卡綁定時間', '同意會員條款時間', '註冊時間', '本月上架登錄HOT認証書台數', '本月上架參與活動台數']]
        #清空表格
        ws = wb.worksheets[1]
        ws.delete_rows(2, 100000)

        n=1
        for r in dataframe_to_rows(df_Office, index=False, header=False):
            m = 1
            for i in r:
                grid = ws.cell(row=1+n, column=m)
                grid.value = i
                m+=1
            n +=1

        #Sheet 3
        df_Office_filter = df_Office[(df_Office['本月架上驗證成功車輛'] >= 15) | (df_Office['本月新上(新)'] >=4 )]
        #清空表格
        ws = wb.worksheets[2]
        ws.delete_rows(2, 100000)

        n=1
        for r in dataframe_to_rows(df_Office_filter, index=False, header=False):
            m = 1
            for i in r:
                grid = ws.cell(row=1+n, column=m)
                grid.value = i
                m+=1
            n +=1

        #Sheet 4
        ws = wb.worksheets[3]
        sheet4 = []
        sheet4.append(end_date) #日期
        sheet4.append(int(sum(memberAgree_new['本月架上車輛']))) #車商會員上架數
        sheet4.append(int(cTM['Total'])) #原廠認證上架數
        sheet4.append(int(cTM['TCPO'])) #TCPO
        sheet4.append(int(cTM['LCPO'])) #LCPO
        sheet4.append(int(cTM['OtherCPO'])) #OtherCPO
        sheet4.append(int(aTM['Number'])) #聯盟館上架數
        sheet4.append(int(sum(memberAgree_new['本月架上車輛'])) + int(cTM['Total']) + int(aTM['Number'])) #聯盟館上架數

        ws.append(sheet4)

        ##Sheet 5 ==========================GA資料==========================
        #sheet5 = []
        ##如果GA有資料
        #if(len(GAWeb)>0 and len(GACar)>0):

        #    GAWeb['visitor'] = GAWeb['visitor'].astype(int)
        #    GAWeb['New_visitor'] = GAWeb['New_visitor'].astype(int)
        #    GAWeb['all'] = GAWeb['all'].astype(int)
        #    GACar['sum(totalEvents)'] = GACar['sum(totalEvents)'].astype(int)

        #    ws = wb.worksheets[4]

        #    sheet5.append(end_date) #日期
        #    sheet5.append(int(GAWeb['visitor'][0])) #到站人次
        #    sheet5.append(int(GAWeb['New_visitor'][0])) #到站新客
        #    sheet5.append(int(GAWeb['all'][0])) #總瀏覽數
        #    sheet5.append(int(GACar['sum(totalEvents)'][0])) #物件總瀏覽數
        #    ws.append(sheet5)
        #else:
        #    for i in range(5):
        #        sheet5.append('')

        # Save the worksheet
        wb.save('abc好車網_架上台數報表_企劃.xlsx')

        #=========================== Load sales file for 高階主管      
        wb = load_workbook('abc好車網_架上台數報表.xlsx')

        #Sheet 1
        ws_main = wb.worksheets[0]

        #Top table

        ## 1.營運報表
        #Add today date
        date = ws_main.cell(row=1, column=1)
        date.value = 'abc好車網_營運報表_(' + end_date +')'

        #本月架上台數
        addValueToExcel(ws_main, carThisMonth, -1, -2)
        #本月淨增加
        addValueToExcel(ws_main, carIncrease, -1, -1)
        #上月架上台數
        addValueToExcel(ws_main, carLastMonth, -1, 0)

        ## 2.營收報表
        # Add today date
        date = ws_main.cell(row=11, column=1)
        date.value = 'abc好車網_營收報表_(' + end_date +')'

        date = ws_main.cell(row=12, column=2)
        date.value = end_date[5:]

        date = ws_main.cell(row=12, column=4)
        date.value = end_date[5:] + '\n訂單筆數'

        #當日過戶收費
        q5 = ws_main.cell(row=13, column=2)
        #q5.value = str(int(q5rtn['當日過戶收費'][0])) + '元' 
        q5.value = str(format(int(q5rtn['當日過戶收費'][0]), ',d')) + '元' 
        #當月過戶收費
        q6 = ws_main.cell(row=13, column=3)
        #q6.value = str(int(q6rtn['當月過戶收費'][0])) + '元' 
        q6.value = str(format(int(q6rtn['當月過戶收費'][0]), ',d')) + '元'
        #當日欠費
        q7 = ws_main.cell(row=14, column=2)
        #q7.value = str(int(q7rtn['當日應收催收款'][0])) + '元' 
        q7.value = str(format(int(q7rtn['當日應收催收款'][0]), ',d')) + '元'
        #當月欠費
        q8 = ws_main.cell(row=14, column=3)
        #q8.value = str(int(q8rtn['當月應收催收款'][0])) + '元' 
        q8.value = str(format(int(q8rtn['當月應收催收款'][0]), ',d')) + '元'

        #當日過戶訂單數
        q9 = ws_main.cell(row=13, column=4)
        #q9.value = str(int(q5rtn['當日訂單筆數'][0])) + '筆' 
        q9.value = str(format(int(q5rtn['當日訂單筆數'][0]), ',d')) + '筆' 
        #當月過戶訂單數
        q10 = ws_main.cell(row=13, column=5)
        #q10.value = str(int(q6rtn['當月訂單筆數'][0])) + '筆' 
        q10.value = str(format(int(q6rtn['當月訂單筆數'][0]), ',d')) + '筆'
        #當日欠費訂單數
        q11 = ws_main.cell(row=14, column=4)
        #q11.value = str(int(q7rtn['當日訂單筆數'][0])) + '筆' 
        q11.value = str(format(int(q7rtn['當日訂單筆數'][0]), ',d')) + '筆' 
        #當月欠費訂單數
        q12 = ws_main.cell(row=14, column=5)
        #q12.value = str(int(q8rtn['當月訂單筆數'][0])) + '筆' 
        q12.value = str(format(int(q8rtn['當月訂單筆數'][0]), ',d')) + '筆' 

        ## 3.各區架上台數天數
        #各區架上台數天數======2022/08/29
        addValueToExcel(ws_main, df_zone['30天以內'].tolist(), 16, -2)
        addValueToExcel(ws_main, df_zone['31-60天'].tolist(), 16, -1)
        addValueToExcel(ws_main, df_zone['61-90天'].tolist(), 16, 0)
        addValueToExcel(ws_main, df_zone['91-180天'].tolist(), 16, 1)
        addValueToExcel(ws_main, df_zone['181-360天'].tolist(), 16, 2)
        addValueToExcel(ws_main, df_zone['361天以上'].tolist(), 16, 3)

        ## 4.TL
        # Add today date
        date = ws_main.cell(row=30, column=1)
        date.value = 'abc好車網_架上台數報表_(' + end_date +')'

        #TL新上架目標台數
        addValueToExcel(ws_main, df_down['TL新上架目標台數'].tolist(), 28, -1)
        #TL新上架實績台數
        addValueToExcel(ws_main, df_down['TL新上架實績台數'].tolist(), 28, 0)       
        #TL新上架達成率
        addValueToExcel(ws_main, df_down['TL新上架達成率'].tolist(), 28, 1)
        """
        #TL淨增加目標台數
        addValueToExcel(ws_main, df_down['TL淨增加目標台數'].tolist(), 28, 2)
        #TL淨增加實績台數
        addValueToExcel(ws_main, df_down['TL淨增加實績台數'].tolist(), 28, 3)
        #TL淨增加達成率
        addValueToExcel(ws_main, df_down['TL淨增加達成率'].tolist(), 28, 4)
        #TL本月新增家數
        addValueToExcel(ws_main, df_down['TL本月新增家數'].tolist(), 28, 5)
        #TL本月新會員新上台數
        addValueToExcel(ws_main, df_down['TL本月新會員新上台數'].tolist(), 28, 6)
        """
        #TL本月新增家數
        addValueToExcel(ws_main, df_down['TL本月新增家數'].tolist(), 28, 2)
        #TL本月新會員新上台數
        addValueToExcel(ws_main, df_down['TL本月新會員新上台數'].tolist(), 28, 3)
        
        ## 5.Sales 2022-10-12 Disable
        #新上架目標台數
        addValueToExcel(ws_main, df['新上架目標台數'].tolist(), 36, -2)
        #新上架實績台數
        addValueToExcel(ws_main, df['新上架實績台數'].tolist(), 36, -1)       
        #新上架達成率
        addValueToExcel(ws_main, df['新上架達成率'].tolist(), 36, 0)
        """
        #淨增加目標台數
        addValueToExcel(ws_main, df['淨增加目標台數'].tolist(), 36, 1)
        #淨增加實績台數
        addValueToExcel(ws_main, df['淨增加實績台數'].tolist(), 36, 2)
        #淨增加達成率
        addValueToExcel(ws_main, df['淨增加達成率'].tolist(), 36, 3)
        #本月新增家數
        addValueToExcel(ws_main, df['本月新增家數'].tolist(), 36, 4)
        #本月新會員新上台數
        addValueToExcel(ws_main, df['本月新會員新上台數'].tolist(), 36, 5)
        #店均獎金家數
        addValueToExcel(ws_main, df['店均獎金家數'].tolist(), 36, 6)
        """
         #本月新增家數
        addValueToExcel(ws_main, df['本月新增家數'].tolist(), 36, 1)
        #本月新會員新上台數
        addValueToExcel(ws_main, df['本月新會員新上台數'].tolist(), 36, 2)
        #店均獎金家數
        addValueToExcel(ws_main, df['店均獎金家數'].tolist(), 36, 3)
               
        # Save the worksheet
        wb.save('abc好車網_架上台數報表.xlsx')

        print('PART 4 all done!')

        #輸出圖檔
        #append_to_graph(carThisMonth, sheet5)

    except Exception as e:
        msg = '{0}: PART 4: 數據寫入出現錯誤! The error message : {1}'.format(script, e)
        send_daily_report_error(script='{}'.format(script), message=msg)
        print(msg)
        sys.exit(1)
