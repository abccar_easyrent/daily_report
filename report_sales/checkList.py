#==============
# Path: /home/hadoop/jessielu/report_sales
# Description: 過戶驗證報表
# Date: 2022/05/24
# Author: Jessie 
#==============
# Description: 過戶驗證報表週報改日報
# Modify: 2022/10/03
# Modify User: Sam
#==============

# coding: utf-8
import sys
import os
import numpy as np

#測試
sys.path.insert(0, '/home/jessie-ws/Dev/jupyterlab/daily_report/')
#sys.path.insert(0, '/home/jessie/MyNotebook/daily_report/')
#正式
#sys.path.insert(0, '/home/hadoop/jessielu/')

from Module.EmailFuncForSales import send_check_sales, send_daily_report_error, send_error_message
from Module.ConnFunc import read_mssql, read_test_mssql, to_mysql, read_mysql

from openpyxl.styles import Border, Side, PatternFill, Font, GradientFill, Alignment
from openpyxl import Workbook, load_workbook
from openpyxl.utils.dataframe import dataframe_to_rows

import pandas as pd
from datetime import datetime, timedelta, date
from dateutil.relativedelta import relativedelta  
import math

from openpyxl.styles import Border, Side, PatternFill, Font, GradientFill, Alignment
from openpyxl import Workbook

#===================Style=====================
thin = Side(border_style="thin", color="000000")
double = Side(border_style="double", color="ff0000")

#正式
#path = '/home/hadoop/jessielu/report_sales/files/'
#測試
path = '/home/jessie-ws/Dev/jupyterlab/daily_report/report_sales/files/'

#原本
now = date.today()
# now = date.today() - relativedelta(days=1)

# #補發
# start_date = '2022-02-01'
# end_date = '2022-02-11'

start_date = date.today() 
end_date = date.today() - relativedelta(days=7) # 取過去一周

def readData():
    orderList = f"""
    select 
    b.MemberID as [會員編號]
    ,ISNULL(m.[CarDealerName], '') as [會員車商名稱]
    ,coi.CarID as [車輛編號]
    ,cm.DisplayName as [車輛名稱]
    ,c.StartDate as [上架時間]
    ,b.CreateDate as [訂單時間]
    ,DATEADD(hour, 8, b.PaidDateUtc) as [付款時間]  --有時區問題所以+8小時
        ,ua.Name as [負責業務]
        ,ua.ID as [業務ID]
        ,coi.UnitPrice as [單價]
        ,coi.UnitPrice - coi.Price AS [折扣]
        ,coi.RefundPrice AS [退刷]
        ,coi.Price AS [小計]
        ,coi.OrderId AS [訂單編號]
    from abccar.dbo.CarTransferOrderItem coi WITH (NOLOCK) 
    LEFT JOIN abccar.dbo.CarTransferOrder b WITH (NOLOCK) ON b.Id = coi.OrderId
    LEFT JOIN [ABCCar].[dbo].Member m WITH (NOLOCK) ON m.MemberID = b.MemberID
    LEFT JOIN [ABCCar].[dbo].[MemberUserAccountSalesMapping] mua WITH (NOLOCK) ON m.[MemberID] = mua.[MemberID]
    LEFT JOIN [ABCCar].[dbo].[UserAccountSales] ua WITH (NOLOCK) ON ua.[ID] = mua.[UserAccountSalesID]
    LEFT JOIN [ABCCar].[dbo].Car c WITH (NOLOCK) ON c.CarId = coi.CarId
    LEFT JOIN [ABCCar].[dbo].CarModel cm WITH (NOLOCK) ON cm.BrandID = c.BrandID AND cm.SeriesID = c.SeriesID AND cm.CategoryID = c.CategoryID
    --LEFT JOIN [ABCCar].[dbo].CarTransferCheckSuccess b1 WITH (NOLOCK) ON coi.OrderID=b1.OrderId and b1.CarId = coi.CarID and b1.CreateDate between '{end_date} 00:00:00' AND '{start_date} 00:00:00'
    --where b1.CreateDate between '{end_date} 00:00:00' AND '{start_date} 00:00:00'
    LEFT JOIN [ABCCar].[dbo].CarTransferCheckSuccess b1 WITH (NOLOCK) ON coi.OrderID=b1.OrderId and b1.CarId = coi.CarID and b1.CreateDate between '{start_date} 00:00:00' AND '{start_date} 23:59:59'
    --where b1.CreateDate between '{start_date} 00:00:00' AND '{start_date} 23:59:59'
    where b.PaidDateUtc between '{start_date} 00:00:00' AND '{start_date} 23:59:59'  --20240828 企劃提出顯示當月付款時間
    and coi.Price not in ('0')
    and b.PaidDateUtc is not null  --20240828 不顯示沒付款的筆數
    order by b.MemberID, b.CreateDate ASC

    """
    
    
    orderList_Today = f"""
    select 
    b.MemberID as [會員編號]
    ,ISNULL(m.[CarDealerName], '') as [會員車商名稱]
    ,coi.CarID as [車輛編號]
    ,cm.DisplayName as [車輛名稱]
    ,c.StartDate as [上架時間]
    ,b.CreateDate as [訂單時間]
    ,DATEADD(hour, 8, b.PaidDateUtc) as [付款時間]  --有時區問題所以+8小時
        ,ua.Name as [負責業務]
        ,ua.ID as [業務ID]
        ,coi.UnitPrice as [單價]
        ,coi.UnitPrice - coi.Price AS [折扣]
        ,coi.RefundPrice AS [退刷]
        ,coi.Price AS [小計]
        ,coi.OrderId AS [訂單編號]
    from abccar.dbo.CarTransferOrderItem coi WITH (NOLOCK) 
    LEFT JOIN abccar.dbo.CarTransferOrder b WITH (NOLOCK) ON b.Id = coi.OrderId
    LEFT JOIN [ABCCar].[dbo].Member m WITH (NOLOCK) ON m.MemberID = b.MemberID
    LEFT JOIN [ABCCar].[dbo].[MemberUserAccountSalesMapping] mua WITH (NOLOCK) ON m.[MemberID] = mua.[MemberID]
    LEFT JOIN [ABCCar].[dbo].[UserAccountSales] ua WITH (NOLOCK) ON ua.[ID] = mua.[UserAccountSalesID]
    LEFT JOIN [ABCCar].[dbo].Car c WITH (NOLOCK) ON c.CarId = coi.CarId
    LEFT JOIN [ABCCar].[dbo].CarModel cm WITH (NOLOCK) ON cm.BrandID = c.BrandID AND cm.SeriesID = c.SeriesID AND cm.CategoryID = c.CategoryID
    --LEFT JOIN [ABCCar].[dbo].CarTransferCheckSuccess b1 WITH (NOLOCK) ON coi.OrderID=b1.OrderId and b1.CarId = coi.CarID and b1.CreateDate between CAST(CAST(GETDATE() AS date) AS datetime) AND CAST(CAST(GETDATE()+1 AS date) AS datetime)
    --where b1.CreateDate between CAST(CAST(GETDATE() AS date) AS datetime) AND CAST(CAST(GETDATE()+1 AS date) AS datetime)
    LEFT JOIN [ABCCar].[dbo].CarTransferCheckSuccess b1 WITH (NOLOCK) ON coi.OrderID=b1.OrderId and b1.CarId = coi.CarID and b1.CreateDate between CAST(CAST(GETDATE() AS date) AS datetime) AND CAST(CAST(GETDATE()+1 AS date) AS datetime)
    --where b1.CreateDate between CAST(CAST(GETDATE() AS date) AS datetime) AND CAST(CAST(GETDATE()+1 AS date) AS datetime)
    where b.PaidDateUtc between CAST(CAST(GETDATE() AS date) AS datetime) AND CAST(CAST(GETDATE()+1 AS date) AS datetime)  --20240828 企劃提出顯示當日付款時間
    and coi.Price not in ('0')
    and b.PaidDateUtc is not null  --20240828 不顯示沒付款的筆數
    order by b.MemberID, b.CreateDate ASC

    """
    
    orderList_Month = f"""
    select 
    b.MemberID as [會員編號]
    ,ISNULL(m.[CarDealerName], '') as [會員車商名稱]
    ,coi.CarID as [車輛編號]
    ,cm.DisplayName as [車輛名稱]
    ,c.StartDate as [上架時間]
    ,b.CreateDate as [訂單時間]
    ,DATEADD(hour, 8, b.PaidDateUtc) as [付款時間]  --有時區問題所以+8小時
        ,ua.Name as [負責業務]
        ,ua.ID as [業務ID]
        ,coi.UnitPrice as [單價]
        ,coi.UnitPrice - coi.Price AS [折扣]
        ,coi.RefundPrice AS [退刷]
        ,coi.Price AS [小計]
        ,coi.OrderId AS [訂單編號]
    from abccar.dbo.CarTransferOrderItem coi WITH (NOLOCK) 
    LEFT JOIN abccar.dbo.CarTransferOrder b WITH (NOLOCK) ON b.Id = coi.OrderId
    LEFT JOIN [ABCCar].[dbo].Member m WITH (NOLOCK) ON m.MemberID = b.MemberID
    LEFT JOIN [ABCCar].[dbo].[MemberUserAccountSalesMapping] mua WITH (NOLOCK) ON m.[MemberID] = mua.[MemberID]
    LEFT JOIN [ABCCar].[dbo].[UserAccountSales] ua WITH (NOLOCK) ON ua.[ID] = mua.[UserAccountSalesID]
    LEFT JOIN [ABCCar].[dbo].Car c WITH (NOLOCK) ON c.CarId = coi.CarId
    LEFT JOIN [ABCCar].[dbo].CarModel cm WITH (NOLOCK) ON cm.BrandID = c.BrandID AND cm.SeriesID = c.SeriesID AND cm.CategoryID = c.CategoryID
    --LEFT JOIN [ABCCar].[dbo].CarTransferCheckSuccess b1 WITH (NOLOCK) ON coi.OrderID=b1.OrderId and b1.CarId = coi.CarID and b1.CreateDate between CAST(CAST( dateadd(m, datediff(m,0,getdate())  ,0)  AS date) AS datetime) AND CAST(CAST( dateadd(m, datediff(m,0,getdate())+1,0)    AS date) AS datetime)
    --where b1.CreateDate between CAST(CAST( dateadd(m, datediff(m,0,getdate())  ,0)  AS date) AS datetime) AND CAST(CAST( dateadd(m, datediff(m,0,getdate())+1,0)    AS date) AS datetime)
    LEFT JOIN [ABCCar].[dbo].CarTransferCheckSuccess b1 WITH (NOLOCK) ON coi.OrderID=b1.OrderId and b1.CarId = coi.CarID and b1.CreateDate between CAST(CAST( dateadd(m, datediff(m,0,getdate())  ,0)  AS date) AS datetime) AND CAST(CAST( dateadd(m, datediff(m,0,getdate())+1,0)    AS date) AS datetime)
    --where b1.CreateDate between CAST(CAST( dateadd(m, datediff(m,0,getdate())  ,0)  AS date) AS datetime) AND CAST(CAST( dateadd(m, datediff(m,0,getdate())+1,0)    AS date) AS datetime)
    where b.PaidDateUtc between CAST(CAST( dateadd(m, datediff(m,0,getdate())  ,0)  AS date) AS datetime) AND CAST(CAST( dateadd(m, datediff(m,0,getdate())+1,0)    AS date) AS datetime)  --20240828 企劃提出顯示當月付款時間
    and coi.Price not in ('0')
    and b.PaidDateUtc is not null  --20240828 不顯示沒付款的筆數
    order by b.MemberID, b.CreateDate ASC

    """
    
    salesList = f"""
    SELECT ID, ZoneName as ZoneID, Name, NameSeq 
    FROM UserAccountSales
    WHERE Type='2' and Status=1 and ZoneName between 1 and 5
    ORDER BY NameSeq
    """
    
    #orderListRtn = read_mssql(orderList, 'ABCCar')
    orderListRtn = read_mssql(orderList_Today, 'ABCCar')
    orderListMonthRtn = read_mssql(orderList_Month, 'ABCCar')
    salesListRtn = read_mssql(salesList, 'ABCCar')   
    
    return orderListRtn, orderListMonthRtn, salesListRtn

def splitDF(rtn, id):
    #Filter data by sales ID
    tmp = rtn.loc[(rtn['業務ID'].isin(id)), :].reset_index().drop(['index'], axis=1)
    tmpSplit= tmp[['會員編號', '會員車商名稱', '車輛編號', '車輛名稱', '上架時間', '訂單時間', '付款時間', '負責業務', '單價', '折扣', '退刷', '小計', '訂單編號']].reset_index().drop(['index'], axis=1)

    return tmpSplit

def addDFtoExcel(tmpUp, tmpUpMonth, sheet, path, path2):

    wb = load_workbook(path)

    # Select the work sheet with sheet number
    ws_main = wb.worksheets[sheet]
    ws_main.delete_rows(1, 100000)

    n=1
    tmpMemberID = 0
    for r in dataframe_to_rows(tmpUp, index=False, header=False):
        if(tmpMemberID != r[0]):
            # Add header
            t = ws_main.cell(row=n, column=1)
            t.value = '會員編號'
            t.border = Border(top=thin, left=thin, bottom=thin, right=thin)
            t.fill = PatternFill("solid", fgColor="E0E0E0")
            t.alignment = Alignment(horizontal="center", vertical="center")
            
            t = ws_main.cell(row=n, column=2)
            t.value = '會員車商名稱'
            t.border = Border(top=thin, left=thin, bottom=thin, right=thin)
            t.fill = PatternFill("solid", fgColor="E0E0E0")
            t.alignment = Alignment(horizontal="center", vertical="center")
            
            t = ws_main.cell(row=n, column=3)
            t.value = '車輛編號'
            t.border = Border(top=thin, left=thin, bottom=thin, right=thin)
            t.fill = PatternFill("solid", fgColor="E0E0E0")
            t.alignment = Alignment(horizontal="center", vertical="center")

            t = ws_main.cell(row=n, column=4)
            t.value = '車輛名稱'
            t.border = Border(top=thin, left=thin, bottom=thin, right=thin)
            t.fill = PatternFill("solid", fgColor="E0E0E0")
            t.alignment = Alignment(horizontal="center", vertical="center")

            t = ws_main.cell(row=n, column=5)
            t.value = '上架時間'
            t.border = Border(top=thin, left=thin, bottom=thin, right=thin)
            t.fill = PatternFill("solid", fgColor="E0E0E0")
            t.alignment = Alignment(horizontal="center", vertical="center")
            
            t = ws_main.cell(row=n, column=6)
            t.value = '訂單時間'
            t.border = Border(top=thin, left=thin, bottom=thin, right=thin)
            t.fill = PatternFill("solid", fgColor="E0E0E0")
            t.alignment = Alignment(horizontal="center", vertical="center")
            
            t = ws_main.cell(row=n, column=7)
            t.value = '付款時間'
            t.border = Border(top=thin, left=thin, bottom=thin, right=thin)
            t.fill = PatternFill("solid", fgColor="E0E0E0")
            t.alignment = Alignment(horizontal="center", vertical="center")
            
            t = ws_main.cell(row=n, column=8)
            t.value = '負責業務'
            t.border = Border(top=thin, left=thin, bottom=thin, right=thin)
            t.fill = PatternFill("solid", fgColor="E0E0E0")
            t.alignment = Alignment(horizontal="center", vertical="center")
                    
            t = ws_main.cell(row=n, column=9)
            t.value = '單價'
            t.border = Border(top=thin, left=thin, bottom=thin, right=thin)
            t.fill = PatternFill("solid", fgColor="E0E0E0")
            t.alignment = Alignment(horizontal="center", vertical="center")
            
            t = ws_main.cell(row=n, column=10)
            t.value = '折扣'
            t.border = Border(top=thin, left=thin, bottom=thin, right=thin)
            t.fill = PatternFill("solid", fgColor="E0E0E0")
            t.alignment = Alignment(horizontal="center", vertical="center")
            
            t = ws_main.cell(row=n, column=11)
            t.value = '退刷'
            t.border = Border(top=thin, left=thin, bottom=thin, right=thin)
            t.fill = PatternFill("solid", fgColor="E0E0E0")
            t.alignment = Alignment(horizontal="center", vertical="center")
            
            t = ws_main.cell(row=n, column=12)
            t.value = '小計'
            t.border = Border(top=thin, left=thin, bottom=thin, right=thin)
            t.fill = PatternFill("solid", fgColor="E0E0E0")
            t.alignment = Alignment(horizontal="center", vertical="center")

            t = ws_main.cell(row=n, column=13)
            t.value = '訂單編號'
            t.border = Border(top=thin, left=thin, bottom=thin, right=thin)
            t.fill = PatternFill("solid", fgColor="E0E0E0")
            t.alignment = Alignment(horizontal="center", vertical="center")

            n +=1 #換一行

        m = 1
        for i in r:
            grid = ws_main.cell(row=n, column=m)
            grid.value = i
            grid.border = Border(top=thin, left=thin, bottom=thin, right=thin)
            m +=1 #換一列
        
        n +=1 #換一行
        tmpMemberID = r[0]
    print('過戶驗證名單OK')

    ws_main = wb.worksheets[1]
    ws_main.delete_rows(1, 100000)

    n=1
    tmpMemberID = 0
    for r in dataframe_to_rows(tmpUpMonth, index=False, header=False):
        if(tmpMemberID != r[0]):
            # Add header
            t = ws_main.cell(row=n, column=1)
            t.value = '會員編號'
            t.border = Border(top=thin, left=thin, bottom=thin, right=thin)
            t.fill = PatternFill("solid", fgColor="E0E0E0")
            t.alignment = Alignment(horizontal="center", vertical="center")
            
            t = ws_main.cell(row=n, column=2)
            t.value = '會員車商名稱'
            t.border = Border(top=thin, left=thin, bottom=thin, right=thin)
            t.fill = PatternFill("solid", fgColor="E0E0E0")
            t.alignment = Alignment(horizontal="center", vertical="center")
            
            t = ws_main.cell(row=n, column=3)
            t.value = '車輛編號'
            t.border = Border(top=thin, left=thin, bottom=thin, right=thin)
            t.fill = PatternFill("solid", fgColor="E0E0E0")
            t.alignment = Alignment(horizontal="center", vertical="center")

            t = ws_main.cell(row=n, column=4)
            t.value = '車輛名稱'
            t.border = Border(top=thin, left=thin, bottom=thin, right=thin)
            t.fill = PatternFill("solid", fgColor="E0E0E0")
            t.alignment = Alignment(horizontal="center", vertical="center")

            t = ws_main.cell(row=n, column=5)
            t.value = '上架時間'
            t.border = Border(top=thin, left=thin, bottom=thin, right=thin)
            t.fill = PatternFill("solid", fgColor="E0E0E0")
            t.alignment = Alignment(horizontal="center", vertical="center")
            
            t = ws_main.cell(row=n, column=6)
            t.value = '訂單時間'
            t.border = Border(top=thin, left=thin, bottom=thin, right=thin)
            t.fill = PatternFill("solid", fgColor="E0E0E0")
            t.alignment = Alignment(horizontal="center", vertical="center")
            
            t = ws_main.cell(row=n, column=7)
            t.value = '付款時間'
            t.border = Border(top=thin, left=thin, bottom=thin, right=thin)
            t.fill = PatternFill("solid", fgColor="E0E0E0")
            t.alignment = Alignment(horizontal="center", vertical="center")
            
            t = ws_main.cell(row=n, column=8)
            t.value = '負責業務'
            t.border = Border(top=thin, left=thin, bottom=thin, right=thin)
            t.fill = PatternFill("solid", fgColor="E0E0E0")
            t.alignment = Alignment(horizontal="center", vertical="center")
                    
            t = ws_main.cell(row=n, column=9)
            t.value = '單價'
            t.border = Border(top=thin, left=thin, bottom=thin, right=thin)
            t.fill = PatternFill("solid", fgColor="E0E0E0")
            t.alignment = Alignment(horizontal="center", vertical="center")
            
            t = ws_main.cell(row=n, column=10)
            t.value = '折扣'
            t.border = Border(top=thin, left=thin, bottom=thin, right=thin)
            t.fill = PatternFill("solid", fgColor="E0E0E0")
            t.alignment = Alignment(horizontal="center", vertical="center")
            
            t = ws_main.cell(row=n, column=11)
            t.value = '退刷'
            t.border = Border(top=thin, left=thin, bottom=thin, right=thin)
            t.fill = PatternFill("solid", fgColor="E0E0E0")
            t.alignment = Alignment(horizontal="center", vertical="center")
            
            t = ws_main.cell(row=n, column=12)
            t.value = '小計'
            t.border = Border(top=thin, left=thin, bottom=thin, right=thin)
            t.fill = PatternFill("solid", fgColor="E0E0E0")
            t.alignment = Alignment(horizontal="center", vertical="center")

            t = ws_main.cell(row=n, column=13)
            t.value = '訂單編號'
            t.border = Border(top=thin, left=thin, bottom=thin, right=thin)
            t.fill = PatternFill("solid", fgColor="E0E0E0")
            t.alignment = Alignment(horizontal="center", vertical="center")


            n +=1 #換一行

        m = 1
        for i in r:
            grid = ws_main.cell(row=n, column=m)
            grid.value = i
            grid.border = Border(top=thin, left=thin, bottom=thin, right=thin)
            m +=1 #換一列
        
        n +=1 #換一行
        tmpMemberID = r[0]

    print('本月過戶驗證名單OK')
    wb.save( path2 )

if __name__ == '__main__':

    try:
        rtnDF, rtnDFMonth, rtnDFSales = readData()
        msg = 'PART 1 : 讀取欠費資料成功！'
        print(msg)
        
        # 所有業務名單
        salesID = Zone1 = Zone2 = Zone3 = Zone4 = Zone5 = []
        salesID = rtnDFSales['ID'].values.tolist()

        tmp1 = rtnDFSales.loc[(rtnDFSales['ZoneID'] == "1"), :]
        tmp2 = rtnDFSales.loc[(rtnDFSales['ZoneID'] == "2"), :]
        tmp3 = rtnDFSales.loc[(rtnDFSales['ZoneID'] == "3"), :]
        tmp4 = rtnDFSales.loc[(rtnDFSales['ZoneID'] == "4"), :]
        tmp5 = rtnDFSales.loc[(rtnDFSales['ZoneID'] == "5"), :]
        
        Zone1 = tmp1['ID'].values.tolist()
        Zone2 = tmp2['ID'].values.tolist()
        Zone3 = tmp3['ID'].values.tolist()
        Zone4 = tmp4['ID'].values.tolist()
        Zone5 = tmp5['ID'].values.tolist()
   
    except Exception as e:
        msg = 'PART 1　: 讀取欠費資料發生錯誤. The error message :{}'.format(e)
        send_daily_report_error(msg)
        print(msg)
        sys.exit(1)
    
    # 所有業務名單
    #salesID = [90, 102, 109, 60, 101, 99, 110, 104, 108, 61]

    # 儲存業務檔案
    sendSales = []
    for i in salesID:
        print(i)
        try:
            tmpUp = splitDF(rtnDF, [i])
            tmpUpMonth = splitDF(rtnDFMonth, [i])
            msg = 'PART 2-1 : 切分業務資料成功！'
            print(msg)
        except Exception as e:
            msg = 'PART 2-1　: 切分業務資料發生錯誤. The error message :{}'.format(e)
            send_daily_report_error(msg)
            print(msg)
            sys.exit(1)
        
        if(len(tmpUp['負責業務']) > 0):
            sendSales.append(tmpUp['負責業務'][0])
            try:
                addDFtoExcel(tmpUp, tmpUpMonth, 0, path + '過戶驗證清單.xlsx', path + tmpUp['負責業務'][0] +'_過戶驗證清單.xlsx')
                msg = 'PART 2-2 : 寫入今日資料成功！'
                print(msg)
            except Exception as e:
                msg = 'PART 2-2　: 寫入今日資料發生錯誤. The error message :{}'.format(e)
                send_daily_report_error(msg)
                print(msg)
                sys.exit(1)
    print('Part 2 - All Sales File done!')

    # 儲存TL檔案
    sendTL = []
    #TLList = {'北區':[90, 102, 109], '桃苗':[60, 101], '中區':[99, 110],'雲嘉':[104],'高屏':[108, 61]}
    TLList = {'北區':Zone1, '桃苗':Zone2, '中區':Zone3, '雲嘉':Zone4, '高屏':Zone5}
    for i, v in enumerate(TLList):
        print(TLList[v])
        try:
            tmpUp = splitDF(rtnDF, TLList[v])
            tmpUpMonth = splitDF(rtnDFMonth, TLList[v])
            msg = 'PART 3-1 : 切分業務資料成功！'
            print(msg)
        except Exception as e:
            msg = 'PART 3-1　: 切分業務資料發生錯誤. The error message :{}'.format(e)
            send_daily_report_error(msg)
            print(msg)
            sys.exit(1)
        
        if(len(tmpUp['負責業務']) > 0):
            sendTL.append(v)
            try:
                addDFtoExcel(tmpUp, tmpUpMonth, 0, path + '過戶驗證清單.xlsx', path + v +'_過戶驗證清單.xlsx')
                msg = 'PART 3-2 : 寫入今日資料成功！'
                print(msg)
            except Exception as e:
                msg = 'PART 3-2　: 寫入今日資料發生錯誤. The error message :{}'.format(e)
                send_daily_report_error(msg)
                print(msg)
                sys.exit(1)

    print('Part 3 - All TL File done!')

    try:
        tmpUp = splitDF(rtnDF, salesID)
        tmpUpMonth = splitDF(rtnDFMonth, salesID)
        msg = 'PART 4-1 : 切分業務資料成功！'
        print(msg)
    except Exception as e:
        msg = 'PART 4-1　: 切分業務資料發生錯誤. The error message :{}'.format(e)
        send_daily_report_error(msg)
        print(msg)
        sys.exit(1)
    
    try:
        addDFtoExcel(tmpUp, tmpUpMonth, 0, path + '過戶驗證清單.xlsx', path + 'All_Sales_過戶驗證清單.xlsx')
        msg = 'PART 4-2 : 寫入今日資料成功！'
        print(msg)
    except Exception as e:
        msg = 'PART 4-2　: 寫入今日資料發生錯誤. The error message :{}'.format(e)
        send_daily_report_error(msg)
        print(msg)
        sys.exit(1)

    print('Part 4 - All File done!')

    try:
        #寄送全部資料
        #All = ['PETERMAY@hotaimotor.com.tw', 'HUNG57@hotaimotor.com.tw', '31879@hotaimotor.com.tw','JOANNE373@hotaimotor.com.tw', 'JONATHANKO@hotaimotor.com.tw', 'WILLIAM@hotaimotor.com.tw', 'STEVEWU@hotaimotor.com.tw', 'MANDYLIN@hotaimotor.com.tw', 'MASA0204@hotaimotor.com.tw', 'AARONWU@hotaimotor.com.tw', 'ANGELLIN@hotaileasing.com.tw', 'ROMANCHEN@hotaileasing.com.tw', 'FARA@hotaimotor.com.tw']
        # All = ['WILLIAM@hotaimotor.com.tw', 'STEVEWU@hotaimotor.com.tw', 'MANDYLIN@hotaimotor.com.tw', 'MARKYAO@hotaimotor.com.tw','FARA@hotaimotor.com.tw' ]
        #send_check_sales('g', path, All, 'All_Sales_過戶驗證清單')  #20240903 企劃提出不需寄送
        
        #寄送TL對應資料
        TLMail = {'北區': 'EAGLE@hotaimotor.com.tw', '桃苗': 'maggiechen@hotaimotor.com.tw', '中區': 'SAM903@hotaimotor.com.tw', '雲嘉': 'HENNY@hotaimotor.com.tw', '高屏': 'LIN32093@hotaimotor.com.tw'}
        # TLMail = {'北區': 'MANDYLIN@hotaimotor.com.tw', '桃苗': 'MANDYLIN@hotaimotor.com.tw', '中區': 'MANDYLIN@hotaimotor.com.tw', '雲嘉': 'MANDYLIN@hotaimotor.com.tw', '高屏': 'MANDYLIN@hotaimotor.com.tw'}
        for i, v in enumerate(TLMail):
            if v in sendTL:
                send_check_sales('g', path, TLMail[v], v+'_過戶驗證清單')

        #寄送業務對應資料
        SalesMail = {'邱雅凌':'CHERRY5588@hotaimotor.com.tw', '林賢宏':'AARON711018@hotaimotor.com.tw', '童嘉如':'TWEETY@hotaimotor.com.tw', '黃煒凱':'LSTTR12@hotaimotor.com.tw', '吳美姍':'SS0210@hotaimotor.com.tw', '林心沛':'MIA0111@hotaimotor.com.tw', '陳湘月':'MANJAK06@hotaimotor.com.tw'}
        # SalesMail = {'孫和莆':'MANDYLIN@hotaimotor.com.tw', '邱雅凌':'MANDYLIN@hotaimotor.com.tw', '林賢宏':'MANDYLIN@hotaimotor.com.tw', '童嘉如':'MANDYLIN@hotaimotor.com.tw', '黃煒凱':'MANDYLIN@hotaimotor.com.tw', '吳美姍':'MANDYLIN@hotaimotor.com.tw', '陳億業':'MANDYLIN@hotaimotor.com.tw', '林心沛':'MANDYLIN@hotaimotor.com.tw', '陳湘月':'MANDYLIN@hotaimotor.com.tw'}
        for i, v in enumerate(SalesMail):
            if v in sendSales:
                send_check_sales('g', path, SalesMail[v], v+'_過戶驗證清單')

        msg = 'PART 5 : 寄送日報OK'
        print(msg)
    except Exception as e:
        msg = 'PART 5 : 寄送日報發生錯誤. The error message : {}'.format(e)
        send_daily_report_error(msg)
        print(msg)
        sys.exit(1)