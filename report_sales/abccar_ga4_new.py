#==============
# Path: /home/hadoop/jessielu/report_sales
# Description: GA4
# Date: 2023/11/06
# Author: Sam
#==============

# coding: utf-8
import sys

# sys.path.insert(0, '/home/jessie/MyNotebook/daily_report/')
sys.path.insert(0, '/home/hadoop/jessielu/')

from Module.EmailFuncForSales import send_daily_report_error
from openpyxl import Workbook, load_workbook
import pandas as pd
from datetime import datetime
from datetime import date, timedelta
from dateutil.relativedelta import relativedelta
import random
import time
from google.oauth2 import service_account
from google.analytics.data_v1beta import BetaAnalyticsDataClient
from google.analytics.data_v1beta.types import (
    DateRange,
    Dimension,
    Filter,
    FilterExpression,
    FilterExpressionList,
    Metric,
    RunReportRequest,
)


#path = '/home/jessie-ws/Dev/jupyterlab/daily_report/report_sales/files/'
path = '/home/hadoop/jessielu/report_sales/files/'
fname = "abc好車網_架上台數報表_企劃.xlsx"
wordpath= path + fname

#KEY_FILE_PATH = '/home/jessie-ws/Dev/jupyterlab/daily_report/report_sales/'
KEY_FILE_PATH = '/home/hadoop/'
KEY_FILE_LOCATION = 'ga4ApiKey.json'
property_id = "312585480"

def initialize_analyticsreporting():
    credentials = service_account.Credentials.from_service_account_file(KEY_FILE_PATH + KEY_FILE_LOCATION)
    client = BetaAnalyticsDataClient(credentials=credentials)
    return client

def get_report_web(client,sysdate):
    dimension_list =  [Dimension(name="date"),Dimension(name="eventName")]

    """
    eventCount = 事件計數
    TotalUsers = 總人數
    """
    metrics_list = [Metric(name='eventCount'),Metric(name='TotalUsers')]
    request = RunReportRequest(
            property=f"properties/{property_id}",
            dimensions=dimension_list,
            metrics=metrics_list,
            date_ranges=[DateRange(start_date=sysdate, end_date=sysdate)],
            dimension_filter=FilterExpression(
                filter=Filter(
                    field_name="eventName",
                    in_list_filter=Filter.InListFilter(
                        values=[
                            "page_view",
                            "first_visit",
                        ]
                    ),
                )
            ),
    )

    # Execute GA4 request
    reports = client.run_report(request)

    return reports

def prep_data_web(reports):
    data = []
    for row in response.rows:
        dimension_values = [value.value for value in row.dimension_values]
        metric_values = [value.value for value in row.metric_values]
        data.append(dimension_values + metric_values)

    columns = [dimension.name for dimension in reports.dimension_headers] + [metric.name for metric in reports.metric_headers]
    df = pd.DataFrame(data, columns=columns)
    
    r=[]
    columns=['QueryDate','visitor','New_visitor','all']
      
    GA = pd.DataFrame(data=r,columns=columns)
    for i in range(len(df.index)):
        GA['QueryDate'] = pd.to_datetime(df['date'], format="%Y-%m-%d").apply(lambda x: x.strftime('%Y-%m-%d'))
        if df['eventName'][i]=="page_view":
            GA['all'] = pd.to_numeric(df['eventCount'][i])
            GA['visitor'] = pd.to_numeric(df['TotalUsers'][i])
        elif df['eventName'][i]=="first_visit":  
            GA['New_visitor'] = pd.to_numeric(df['TotalUsers'][i])
            
    GA = GA.drop_duplicates(subset=['QueryDate']).fillna(0)

    return GA

def get_report_pageview(client,sysdate):

    data = []
    pageOffset = 0
    while True:
        request = RunReportRequest(
                property=f"properties/{property_id}",
                dimensions=[Dimension(name = "date"),
                            Dimension(name="sessionMedium"),
                            Dimension(name="sessionSource"),
                            Dimension(name = "eventName"),
                            Dimension(name="pagepath")],
                metrics=[Metric(name="eventCount")],
                date_ranges=[DateRange(start_date=sysdate, end_date=sysdate)],
                limit = 10000,
                offset = pageOffset,
                dimension_filter=FilterExpression(
                    and_group=FilterExpressionList(
                        expressions=[
                            FilterExpression(
                                filter=Filter(
                                    field_name="eventName",
                                    string_filter=Filter.StringFilter(value="page_view"),
                                )
                            ),
                            FilterExpression(
                                filter=Filter(
                                    field_name="pagepath",
                                    string_filter=Filter.StringFilter(
                                        match_type=Filter.StringFilter.MatchType.CONTAINS,
                                        value="/car/"
                                    )

                                )
                           ),
                        ]
                    )
                ),
        )
        response = client.run_report(request)

        for row in response.rows:
            dimension_values = [value.value for value in row.dimension_values]
            metric_values = [value.value for value in row.metric_values]
            data.append(dimension_values + metric_values)

        if pageOffset > response.row_count:
            break
        else:
            pageOffset = pageOffset + 10000;

    columns = [dimension.name for dimension in response.dimension_headers] + [metric.name for metric in response.metric_headers]
    reports = pd.DataFrame(data, columns=columns)

    reports['date'] = pd.to_datetime(reports['date'], format="%Y-%m-%d").apply(lambda x: x.strftime('%Y-%m-%d'))
    reports['eventCount'] = reports['eventCount'].astype(int)
    
    reports.rename(columns = {'date':'QueryDate', 'eventCount':'totalviews'}, inplace = True)
    pageviewDF = reports.groupby(['QueryDate','eventName'], as_index=False)['totalviews'].sum()

    #print(pageviewDF)
    return pageviewDF


def makeRequestWithExponentialBackoff(analytics,sysdate):
    for n in range(0, 5):
        #print(n)
        try:
            return get_report_web(analytics,sysdate)

        except Exception as error:
            print(error)
            time.sleep((2 ** n) + random.random())
            
def makeRequestWithExponentialBackoff_pageview(analytics,sysdate):
    for n in range(0, 5):
        #print(n)
        try:
            return get_report_pageview(analytics,sysdate)

        except Exception as error:
            print(error)
            time.sleep((2 ** n) + random.random())


if __name__ == "__main__":

    script = 'abccar_ga4_new'

    try:
        analytics = initialize_analyticsreporting()
    except Exception as e:
        msg = '{0}: PART 1: 連線GA API 讀取失敗! The error message : {1}'.format(script, e)
        send_daily_report_error(script='{}'.format(script), message=msg)
        print(msg)
        sys.exit(1)
        
    print('PART 1: Finished!')
    
    
    try:
        df = pd.read_excel(wordpath, sheet_name="網站數據推移").tail(1).reset_index()
        #print(df)
        now = (date.today() - relativedelta(days=2))
        start_date = datetime.strptime(df["日期"][0], '%Y-%m-%d').date()
        delta = timedelta(days=1)
        seq = 0
        while start_date < now:
            start_date += delta
            #print(start_date.strftime("%Y-%m-%d"))
            response = makeRequestWithExponentialBackoff(analytics,start_date.strftime("%Y-%m-%d"))
            pageviews = makeRequestWithExponentialBackoff_pageview(analytics,start_date.strftime("%Y-%m-%d"))
            final = prep_data_web(response)
            final = pd.merge(final,pageviews[['QueryDate','totalviews']], on='QueryDate',how='left')
            #print(final)
            if seq==0: 
                GA4List =  final
            else:
                GA4List = pd.concat([GA4List, final])
            seq = seq + 1

    except Exception as e:
        msg = '{0}: PART 2: Fetch GA 資料出現錯誤! The error message : {1}'.format(script, e)
        send_daily_report_error(script='{}'.format(script), message=msg)
        print(msg)
        sys.exit(1)

    print('PART 2: Finished!')
    
    try:
        if (len(GA4List)>0):
            #print(GA4List.reset_index())
            wb = load_workbook(wordpath)
            ws = wb.worksheets[4]
            for index, row in GA4List.iterrows():
                print(row['QueryDate'], row['visitor'], row['New_visitor'], row['all'], row['totalviews'])   
                sheet5 = []
                sheet5.append(row['QueryDate']) #日期
                sheet5.append(int(row['visitor'])) #到站人次
                sheet5.append(int(row['New_visitor'])) #到站新客
                sheet5.append(int(row['all'])) #總瀏覽數
                sheet5.append(int(row['totalviews'])) #物件總瀏覽數
                ws.append(sheet5)  
            wb.save(wordpath)
                
    except Exception as e:
        msg = '{0}: PART 3: 數據寫入EXCEL出現錯誤! The error message : {1}'.format(script, e)
        send_daily_report_error(script='{}'.format(script), message=msg)
        print(msg)
        sys.exit(1)
    
    print('PART 3: Finished!')