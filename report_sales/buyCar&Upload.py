#==============
# Path: /home/hadoop/jessielu/report_sales
# Description: 安心購與不二價報表
# Date: 2022/05/24
# Author: Jessie 
#==============

# coding: utf-8
import sys
import os
import numpy as np

import smtplib
from datetime import datetime, timedelta
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from email.mime.image import MIMEImage
from email.utils import COMMASPACE
from dateutil.relativedelta import relativedelta

# sys.path.insert(0, '/home/jessie/MyNotebook/daily_report/')
sys.path.insert(0, '/home/hadoop/jessielu/')

from Module.EmailFuncForSales import send_carBuy_report, send_error_message
from Module.ConnFunc import read_mssql, read_test_mssql, to_mysql, read_mysql

from openpyxl.styles import Border, Side, PatternFill, Font, GradientFill, Alignment
from openpyxl import Workbook, load_workbook
from openpyxl.utils.dataframe import dataframe_to_rows

import pandas as pd
from datetime import datetime, timedelta, date
from dateutil.relativedelta import relativedelta  
import math

from openpyxl.styles import Border, Side, PatternFill, Font, GradientFill, Alignment
from openpyxl import Workbook

#==================GA import===========
import sys
from googleapiclient.discovery import build
from oauth2client.service_account import ServiceAccountCredentials
from Module.EmailFunc import send_error_message
from Module.ConnFunc import read_mssql, read_test_mssql

import re
import pandas as pd
import sqlalchemy as sa
import os
import socket
import time
from datetime import datetime, timedelta, date
from dateutil.relativedelta import relativedelta
import random
import requests
from apiclient.errors import HttpError

SCOPES = ['https://www.googleapis.com/auth/analytics.readonly']
KEY_FILE_LOCATION = '/home/hadoop/jessielu/report_sales/gaApiKey.json'

VIEW_ID = '177455238'
#==================GA import===========

b1day = (datetime.today()).strftime('%Y-%m-%d')
sender_ls = "abccar.easyrent@gmail.com"

#原本
now = date.today() + relativedelta(days=1)
# #補發用
#now = date.today()

if(now.day == 1): #2021/11/1; 2022/01/01
    tmpDay = now - relativedelta(days=1) #2021/10/31; 2021/12/31
    #這個月的第一天
    first_day = date(tmpDay.year, tmpDay.month, 1) #2021/10/1; 2021/12/1
    #上個月的最後一天
    lastMonth_end_day = date(first_day.year, first_day.month, 1) - relativedelta(days=1)#2021/09/30; 2021/11/30
    #下個月的第一天
    nextMonth_first_day = first_day + relativedelta(months=1)#2021/11/1; 2022/01/01
    #這個月最後一天
    thisMonth_end_day = nextMonth_first_day - relativedelta(days=1)#2021/10/31; 2021/12/31
#if now = 2022/01/02
else:
    #這個月的第一天
    first_day = date(now.year, now.month, 1) #2022/01/01
    #上個月的最後一天
    lastMonth_end_day = date(first_day.year, first_day.month, 1) - relativedelta(days=1) #2021/12/31
    #下個月的第一天
    nextMonth_first_day = first_day + relativedelta(months=1) #2022/02/01
    #這個月最後一天
    thisMonth_end_day = nextMonth_first_day - relativedelta(days=1) #2022/01/31

#昨天
end_date = (now - timedelta(1)).strftime("%Y-%m-%d")
#前天
the_day_before_yesterday_date = (now - timedelta(2)).strftime("%Y-%m-%d") 

path = '/home/hadoop/jessielu/report_sales/files/'

os.chdir(path)

print(end_date)

def readDataOrder():
    orderList = f"""
    select 
        poi.CarID AS [車輛編號],
        c.MemberID,
        poi.CreateDate AS [簽約時間],
        ISNULL(ua.[Name], '') AS [維護業務],
        ISNULL(ua.[NameSeq], '') AS [維護業務序號],
        ISNULL(ua.[ID], '') AS [維護業務ID],
		po.OrderStatusID AS [訂單狀態],
		po.PaymentStatusID AS [付款狀態]

    from abccar.dbo.ProductOrderItem poi WITH (NOLOCK)
    left join [ABCCar].[dbo].ProductOrder po WITH (NOLOCK) on poi.OrderID = po.ID
    LEFT JOIN [ABCCar].[dbo].Car c WITH (NOLOCK) on poi.CarID = c.CarID
    LEFT JOIN [ABCCar].[dbo].[MemberUserAccountSalesMapping] mua WITH (NOLOCK) ON c.[MemberID] = mua.[MemberID]
    LEFT JOIN [ABCCar].[dbo].[UserAccountSales] ua WITH (NOLOCK) ON ua.[ID] = mua.[UserAccountSalesID]
    where ProductID = 7
    and po.Deleted = 0
	--and po.OrderStatusID = 30 and po.PaymentStatusID = 30
    and c.Status = 1
    and GetDate() Between c.[StartDate] And c.[EndDate]
    """
    
    orderListRtn = read_test_mssql(orderList, 'ABCCar')
    
    return orderListRtn

def contractTimeBySalesID(rtn, id):


    contractTime = rtn.groupby(['MemberID', '維護業務', '維護業務ID', '維護業務序號'])['簽約時間'].min().to_frame().reset_index()
    contractTimeToday = contractTime.loc[(contractTime['維護業務ID'].isin(id)) & ((contractTime['簽約時間'] > pd.to_datetime(end_date)) & (contractTime['簽約時間'] <= pd.to_datetime(now)))].count()['維護業務ID'] #當日簽約家數
    contractTimeThisMonth = contractTime.loc[(contractTime['維護業務ID'].isin(id)) & ((contractTime['簽約時間'] > pd.to_datetime(first_day)) & (contractTime['簽約時間'] <= pd.to_datetime(now)))].count()['維護業務ID'] #當月簽約家數
    contractTimeALL = contractTime.loc[(contractTime['維護業務ID'].isin(id))].count()['維護業務ID'] #累計簽約家數
    rtnCT = [contractTimeToday, contractTimeThisMonth, contractTimeALL]
    return rtnCT

def uploadTimeBySalesID(rtn, id):
    rtnUT = []
    #-----------上架台數
    uploadTime = rtn.copy()
    uploadTimeToday = uploadTime.loc[(uploadTime['維護業務ID'].isin(id)) & ((uploadTime['簽約時間'] > pd.to_datetime(end_date)) & (uploadTime['簽約時間'] <= pd.to_datetime(now)))].count()['車輛編號'] #當日上架台數
    uploadTimeTodayMID = len(uploadTime.loc[(uploadTime['維護業務ID'].isin(id)) & ((uploadTime['簽約時間'] > pd.to_datetime(end_date)) & (uploadTime['簽約時間'] <= pd.to_datetime(now)))]['MemberID'].unique()) #當日上架家數  
    uploadTimeThisMonth = uploadTime.loc[(uploadTime['維護業務ID'].isin(id)) & ((uploadTime['簽約時間'] > pd.to_datetime(first_day)) & (uploadTime['簽約時間'] <= pd.to_datetime(now)))].count()['車輛編號'] #當月上架
    uploadTimeThisMonthMID = len(uploadTime.loc[(uploadTime['維護業務ID'].isin(id)) & ((uploadTime['簽約時間'] > pd.to_datetime(first_day)) & (uploadTime['簽約時間'] <= pd.to_datetime(now)))]['MemberID'].unique()) #當月上架家數
    uploadTimeAll = uploadTime.loc[(uploadTime['維護業務ID'].isin(id))].count()['車輛編號'] #累計上架
    uploadTimeAllMID = len(uploadTime.loc[(uploadTime['維護業務ID'].isin(id))]['MemberID'].unique()) #累計上架家數
    uploadTimeAllReal = uploadTime.loc[(uploadTime['維護業務ID'].isin(id)) & (uploadTime['訂單狀態'] == 30) & (uploadTime['付款狀態'] == 30) ].count()['車輛編號'] #累計上架
    uploadTimeAllMIDReal = len(uploadTime.loc[(uploadTime['維護業務ID'].isin(id)) & (uploadTime['訂單狀態'] == 30) & (uploadTime['付款狀態'] == 30)]['MemberID'].unique()) #累計上架家數
    
    rtnUT = [uploadTimeToday, uploadTimeTodayMID, uploadTimeThisMonth, uploadTimeThisMonthMID, uploadTimeAll, uploadTimeAllMID, uploadTimeAllReal, uploadTimeAllMIDReal]
    return rtnUT

def computeData(rtn):
    outputList = []
    #TLList = {'北區':[90, 102, 109], '桃苗':[60, 101], '中區':[99, 110], '雲嘉':[111], '高屏':[108, 61], 'ALL': [90, 102, 109, 60, 101, 99, 110, 111, 108, 61]}
    #TLList = {'北區':[90, 102, 109], '桃苗':[60, 101], '中區':[99, 110], '雲嘉':[111, 112], '高屏':[108, 61], 'ALL': [90, 102, 109, 60, 101, 99, 110, 111, 112, 108, 61]}
    #TLList = {'北區':[90, 102, 109], '桃苗':[101], '中區':[99, 116], '雲嘉':[112], '高屏':[61, 117], 'ALL': [90, 102, 109, 101, 99, 116, 112, 61, 117]}
    TLList = {'北區':[102, 109], '桃苗':[101], '中區':[99,116], '雲嘉':[112, 119], '高屏':[117], 'ALL': [102, 109, 101, 99, 116, 112, 119, 117]}
    #TLList = {'北區':[102, 109], '桃苗':[120], '中區':[99,116], '雲嘉':[112, 119, 121], '高屏':[117], 'ALL': [102, 109, 120, 99, 116, 121, 119, 112, 117]}

    for index, row in enumerate(TLList):
        outputList.append(contractTimeBySalesID(rtn, TLList[row]) + uploadTimeBySalesID(rtn, TLList[row]))

    outputListDown = []
    #salesID = [90, 102, 109, 60, 101, 99, 110, 111, 108, 61]
    #salesID = [90, 102, 109, 60, 101, 99, 110, 111, 112, 108, 61]
    #salesID = [90, 102, 109, 101, 99, 116, 112, 61, 117]
    salesID = [102, 109, 101, 99, 116, 112, 119, 117]
    #salesID = [102, 109, 120, 99, 116, 121, 119, 112, 117]
    
    for i in salesID:
        outputListDown.append(contractTimeBySalesID(rtn, [i]) + uploadTimeBySalesID(rtn, [i]))
    outputListDown.append(contractTimeBySalesID(rtn, salesID) + uploadTimeBySalesID(rtn, salesID))

    return outputList, outputListDown

def addValueToExcel(ws_main, valueList, rowIndex, colIndex):
    a = [9, 11, 13, 15]
    for index, value in enumerate(valueList):
        if(colIndex+index in a):
            grid = ws_main.cell(row=rowIndex+1, column=colIndex+index)
        else:
            grid = ws_main.cell(row=rowIndex, column=colIndex+index)
        grid.value = value

def outputToExcel(outputList, outputListDown, a, b, c, d):
    wb = load_workbook('安心購日報.xlsx')

    # Select the first work sheet
    ws_main = wb.worksheets[0]

    # Add today date
    date = ws_main.cell(row=4, column=5)
    date.value = end_date[5:]

    date = ws_main.cell(row=3, column=8)
    date.value = end_date[5:]

    date = ws_main.cell(row=21, column=5)
    date.value = end_date[5:]

    date = ws_main.cell(row=20, column=8)
    date.value = end_date[5:]

    #Add month
    date = ws_main.cell(row=4, column=6)
    date.value = str(first_day.month) + '月'

    date = ws_main.cell(row=3, column=10)
    date.value = str(first_day.month) + '月'

    date = ws_main.cell(row=21, column=6)
    date.value = str(first_day.month) + '月'

    date = ws_main.cell(row=20, column=10)
    date.value = str(first_day.month) + '月'

    for i, v in enumerate(outputList):
        addValueToExcel(ws_main, v, 6+2*i, 5)
        
    for j, k in enumerate(outputListDown):
        addValueToExcel(ws_main, k, 23+2*j, 5)

    # Select the second work sheet
    ws_side = wb.worksheets[1]
    ws_side.delete_rows(2, 10000)

    n=1
    for r in dataframe_to_rows(a, index=False, header=False):
        m = 1
        for i in r:
            grid = ws_side.cell(row=1+n, column=m)
            grid.value = i
            m+=1
        n +=1

    # Select the third work sheet
    ws_side = wb.worksheets[2]
    ws_side.delete_rows(2, 10000)

    n=1
    for r in dataframe_to_rows(b, index=False, header=False):
        m = 1
        for i in r:
            grid = ws_side.cell(row=1+n, column=m)
            grid.value = i
            m+=1
        n +=1

    # Select the foruth work sheet
    ws_side = wb.worksheets[3]
    ws_side.delete_rows(2, 10000)

    n=1
    for r in dataframe_to_rows(c, index=False, header=False):
        m = 1
        for i in r:
            grid = ws_side.cell(row=1+n, column=m)
            grid.value = i
            m+=1
        n +=1
        

    # Select the five work sheet
    ws_side = wb.worksheets[4]
    ws_side.delete_rows(2, 100)
    ws_side.font=Font(name='新細明體',size=12,bold=True)
    n=1
    for r in dataframe_to_rows(d, index=False, header=True):
        m = 1
        for i in r:
            grid = ws_side.cell(row=n, column=m)
            grid.alignment = Alignment(vertical='center', horizontal='center')
            if (m>1):
                grid.number_format = '#,##0'
            grid.value = i
            m+=1
        n +=1
        
    wb.save('安心購日報.xlsx')


def initialize_analyticsreporting():
    """Initializes an Analytics Reporting API V4 service object.

    Returns:
        An authorized Analytics Reporting API V4 service object.
    """
    credentials = ServiceAccountCredentials.from_json_keyfile_name(
        KEY_FILE_LOCATION, SCOPES)

    # Build the service object.
    analytics = build('analyticsreporting', 'v4', credentials=credentials)
    return analytics

def prep_data(rawData):
    rows = []
    for data in rawData:
        data = data['reports'][0]
        
        cols = data['columnHeader']
        cols = cols['dimensions'] + [cols['metricHeader']['metricHeaderEntries'][0]['name']]
        cols = [re.sub('ga:','',x) for x in cols]
    
        data = data['data']['rows']
    
        for row in data:
            date = row['dimensions'][0]
            date = '-'.join([date[:4], date[4:6], date[6:]])
            row['dimensions'][0] = date
            # 2022-07-15 補 不二價活動_點擊 / 帶看服務聯繫電話
            #if (row['dimensions'][2]=="不二價活動_點擊" and row['dimensions'][3]=="帶看服務聯繫電話"):
            #    row['dimensions'][2]=row['dimensions'][3]
            rows.append(row['dimensions']+row['metrics'][0]['values'])

    final = pd.DataFrame(rows, columns=cols)
    final['CarID'] = final.pagePathLevel2.str[1:8].astype(int)
    
    test = final
    test = test.groupby(['date', 'CarID', 'eventAction']).sum()['totalEvents'].to_frame().reset_index()
    test_dt = pd.pivot_table(test, index='CarID', columns=['eventAction'], values='totalEvents', aggfunc=np.sum).reset_index().fillna(0)
        
    return test_dt

def makeRequestWithExponentialBackoff(analytics):
    for n in range(0, 10):
        print(n)
        try:
            return get_report(analytics)

        except Exception as error:
            print(error)
            time.sleep((2 ** n) + random.random())
            
def makeRequestWithExponentialBackoffA(analytics):
    for n in range(0, 10):
        print(n)
        try:
            return get_reportA(analytics)

        except Exception as error:
            print(error)
            time.sleep((2 ** n) + random.random())

def makeRequestWithExponentialBackoffB(analytics):
    for n in range(0, 10):
        print(n)
        try:
            return get_reportB(analytics)

        except Exception as error:
            print(error)
            time.sleep((2 ** n) + random.random())

def readData(CID):
    if(CID==901):
        orderList = f"""
        SELECT CAST(a.CarID AS UNSIGNED) CarID,a.ViewCount as '瀏覽次數',v2.MemberID 
        FROM car_fixprice_new a LEFT JOIN status_new_v2 v2 on a.CarID=v2.CarID and a.Date=v2.Date 
        WHERE a.CampaignID='901' and a.Date = (select max(Date) from car_fixprice_new)
        """    
        orderListRtn = read_mysql(orderList, 'report')
        print('CID 901 ok')
        return orderListRtn
         
    else:
        orderList = f"""
	    select 
        c.CarID,
        c.ViewCount AS [瀏覽次數],
        c.MemberID
        from abccar.dbo.Car c with (NOLOCK)
        where c.CarID in (
        SELECT CarID
        FROM abccar.dbo.CampaignItem with (NOLOCK)
        WHERE (CampaignID = {CID}) )
	    and c.Status = 1
	    and GetDate() Between c.[StartDate] And c.[EndDate]
        """
        orderListRtn = read_test_mssql(orderList, 'ABCCar')
        print('CID N901 ok')
        
        for index in orderListRtn.index:    
            url = "http://13.231.123.131/Api/Car/GetCarInfo?CarID={0}&ViewCount=0".format(orderListRtn['CarID'][index])
            resp = requests.get(url)
            resp_dict = resp.json()
            orderListRtn['瀏覽次數'][index] = resp_dict.get('CarInfo').get('ViewCount')
        return orderListRtn


def readDataAll(CID):
    orderList = f"""
	SELECT 
    ci.CarID
    --,CASE WHEN c.Status=1 THEN 1 ELSE 0 END as Status
    ,c.ViewCount AS [瀏覽次數]
    , c.MemberID
	FROM abccar.dbo.CampaignItem ci with (NOLOCK),abccar.dbo.Car c with (NOLOCK)
	WHERE ci.CampaignID = {CID} and ci.CarID=c.CarID 
    """
    
    orderListRtn = read_test_mssql(orderList, 'ABCCar')
                
    for index in orderListRtn.index:    
        url = "http://13.231.123.131/Api/Car/GetCarInfo?CarID={0}&ViewCount=0".format(orderListRtn['CarID'][index])
        resp = requests.get(url)
        resp_dict = resp.json()
        orderListRtn['瀏覽次數'][index] = resp_dict.get('CarInfo').get('ViewCount')
            
    return orderListRtn

# 不二價 Start
def get_report(analytics):
    """Queries the Analytics Reporting API V4.

    Args:
        analytics: An authorized Analytics Reporting API V4 service object.
    Returns:
        The Analytics Reporting API V4 response.
    """
    #valuesNeed = [
    #    "不二價活動_點擊",  # {'dimensions': ['20220713', '/1762338', '不二價活動_點擊', '帶看服務聯繫電話'], 'metrics': [{'values': ['1']}]}
    #    "【大網】查看聯繫LINE_下",
    #    "【大網】LINE聯繫賣家_上",
    #    "【大網】查看電話",
    #    "【小網】立即通話內層",
    #    "【大網】查看車商電話",
    #    "【小網】LINE 聯繫賣家",
    #    "【小網】立即通話",
    #    "LINE聯繫_主要"
    #]    
    # 20230426 換新版
    valuesNeed = [
        "帶看服務聯繫電話",
        "通話",
        "查看聯繫手機1",
        "查看聯繫手機2",
        "查看車商電話",
        "LINE聯繫",
        "LINE聯繫1",
        "LINE聯繫2"  
    ]
    reports = []
    pageCnt = 1
    pageToken = None
    while True:
        data = analytics.reports().batchGet(
            body={
                'reportRequests': [{
                    'viewId': VIEW_ID,
                    'pageToken': pageToken,
                    'samplingLevel': 'LARGE',
					'dateRanges': [{'startDate': str(end_date), 'endDate':str(end_date)}],
                    'metrics': [{'expression': 'ga:totalEvents'}],
                    'dimensions': [
                        {'name': 'ga:date'},
                        {'name': 'ga:pagePathLevel2'},
                        {'name': 'ga:eventAction'}
                        #{'name': 'ga:eventLabel'}
                    ],
                    'dimensionFilterClauses': [{
                        'filters': [{
                            'dimensionName': 'ga:eventAction',
                            'operator': 'IN_LIST',
                            'expressions': valuesNeed
                            }]
                    }]                   
                    #'dimensionFilterClauses': [{
                    #    'filters': [{
                    #        'dimensionName': 'ga:eventAction',
                    #        'operator': 'IN_LIST',
                    #        'expressions': valuesNeed
                    #        },{
                    #        'dimensionName': 'ga:eventLabel',
                    #        'operator': 'EXACT',
                    #        'expressions': ['帶看服務聯繫電話']
                    #        }]
                    #}]
                    
                }]
            }
        ).execute()
        reports.append(data)
        print('Get Page %i of data.' % (pageCnt))
        pageCnt += 1
        if 'nextPageToken' in data['reports'][0]:
            pageToken = data['reports'][0]['nextPageToken']
        else:
            break
  
    return reports

def get_reportA(analytics):
    """Queries the Analytics Reporting API V4.

    Args:
        analytics: An authorized Analytics Reporting API V4 service object.
    Returns:
        The Analytics Reporting API V4 response.
    """
    #valuesNeed = [
    #    "不二價活動_點擊",  # {'dimensions': ['20220713', '/1762338', '不二價活動_點擊', '帶看服務聯繫電話'], 'metrics': [{'values': ['1']}]}
    #    "【大網】查看聯繫LINE_下",
    #    "【大網】LINE聯繫賣家_上",
    #    "【大網】查看電話",
    #    "【小網】立即通話內層",
    #    "【大網】查看車商電話",
    #    "【小網】LINE 聯繫賣家",
    #    "【小網】立即通話",
    #    "LINE聯繫_主要"
    #]
    # 20230426 換新版
    valuesNeed = [
        "帶看服務聯繫電話",
        "通話",
        "查看聯繫手機1",
        "查看聯繫手機2",
        "查看車商電話",
        "LINE聯繫",
        "LINE聯繫1",
        "LINE聯繫2"  
    ]
    reports = []
    pageCnt = 1
    pageToken = None
    while True:
        data = analytics.reports().batchGet(
            body={
                'reportRequests': [{
                    'viewId': VIEW_ID,
                    'pageToken': pageToken,
                    'samplingLevel': 'LARGE',
					#'dateRanges': [{'startDate': str(start_date), 'endDate':str(end_date)}],
                    'metrics': [{'expression': 'ga:totalEvents'}],
                    'dimensions': [
                        {'name': 'ga:date'},
                        {'name': 'ga:pagePathLevel2'},
                        {'name': 'ga:eventAction'}
                        #{'name': 'ga:eventLabel'}
                    ],
                    'dimensionFilterClauses': [{
                        'filters': [{
                            'dimensionName': 'ga:eventAction',
                            'operator': 'IN_LIST',
                            'expressions': valuesNeed
                            }]
                    }]                   
                    #'dimensionFilterClauses': [{
                    #    'filters': [{
                    #        'dimensionName': 'ga:eventAction',
                    #        'operator': 'IN_LIST',
                    #        'expressions': valuesNeed
                    #        },{
                    #        'dimensionName': 'ga:eventLabel',
                    #        'operator': 'EXACT',
                    #        'expressions': ['帶看服務聯繫電話']
                    #        }]
                    #}]
                    
                }]
            }
        ).execute()
        reports.append(data)
        print('Get Page %i of data.' % (pageCnt))
        pageCnt += 1
        if 'nextPageToken' in data['reports'][0]:
            pageToken = data['reports'][0]['nextPageToken']
        else:
            break
  
    return reports

# 不二價 End

# 安心購 Start
def get_reportB(analytics):
    """Queries the Analytics Reporting API V4.

    Args:
        analytics: An authorized Analytics Reporting API V4 service object.
    Returns:
        The Analytics Reporting API V4 response.
    """

    #valuesNeed = [
    #    "【大網】查看聯繫LINE_下",
    #    "【大網】LINE聯繫賣家_上",
    #    "【大網】查看電話",
    #    "【小網】立即通話內層",
    #    "【大網】查看車商電話",
    #    "【小網】LINE 聯繫賣家",
    #    "【小網】立即通話",
    #    "LINE聯繫_主要"
    #]
    # 20230426 換新版
    valuesNeed = [
        "帶看服務聯繫電話",
        "通話",
        "查看聯繫手機1",
        "查看聯繫手機2",
        "查看車商電話",
        "LINE聯繫",
        "LINE聯繫1",
        "LINE聯繫2"  
    ]
    reports = []
    pageCnt = 1
    pageToken = None
    while True:
        data = analytics.reports().batchGet(
            body={
                'reportRequests': [{
                    'viewId': VIEW_ID,
                    'pageToken': pageToken,
                    'samplingLevel': 'LARGE',
					'dateRanges': [{'startDate': str(end_date), 'endDate':str(end_date)}],
                    'metrics': [{'expression': 'ga:totalEvents'}],
                    'dimensions': [
                        {'name': 'ga:date'},
                        {'name': 'ga:pagePathLevel2'},
                        {'name': 'ga:eventAction'}
                        #{'name': 'ga:eventLabel'}
                    ],
                    'dimensionFilterClauses': [{
                        'filters': [{
                            'dimensionName': 'ga:eventAction',
                            'operator': 'IN_LIST',
                            'expressions': valuesNeed
                            }]
                    }]
                }]
            }
        ).execute()
        reports.append(data)
        print('Get Page %i of data.' % (pageCnt))
        pageCnt += 1
        if 'nextPageToken' in data['reports'][0]:
            pageToken = data['reports'][0]['nextPageToken']
        else:
            break
  
    return reports

def readProductOrderData():
    ProductOrdreList = f"""
        SELECT us.NameSeq,us.ID
        --,od.OrderYM
        ,CONVERT(varchar(7),GETDATE()-1,120) as OrderYM
        ,(RIGHT(REPLICATE('0', 2) + CAST(us.NameSeq as NVARCHAR),2) + ' ' + Name) AS [維護業務]
        ,ISNULL(od.[HAA],0) AS [購買認証]
        ,ISNULL(od.[置頂],0) AS [置頂]
        ,ISNULL(od.[更新],0) AS [更新]
        ,ISNULL(od.[全面曝光],0) AS  [全面曝光]
        ,ISNULL(od.[BANNER曝光],0) AS  [BANNER曝光]
        FROM UserAccountSales AS us WITH (NOLOCK)
        LEFT JOIN (

            SELECT * FROM
            (
            SELECT ISNULL(ua.[ID], '') as SaleID,--CONVERT(varchar,mb.ProductTypeId) ProductTypeId,
            CONVERT(varchar(7),mc.CreateDate,120) as OrderYM,
            (CASE WHEN ProductTypeId='2' THEN 'HAA' 
            WHEN ProductTypeId='6' THEN '置頂' 
            WHEN ProductTypeId='7' THEN '更新' 
            WHEN ProductTypeId='8' THEN '全面曝光' 
            WHEN ProductTypeId='9' THEN 'BANNER曝光'
            ELSE '' END) as ProductType
            ,mc.OrderTotal as OrderTotal
            --,sum(mc.OrderTotal) as OrderTotal
            FROM ProductOrder mc WITH (NOLOCK)
            LEFT JOIN [ABCCar].[dbo].[MemberUserAccountSalesMapping] mua WITH (NOLOCK) ON mc.[MemberID] = mua.[MemberID]
            LEFT JOIN [ABCCar].[dbo].[UserAccountSales] ua WITH (NOLOCK) ON ua.[ID] = mua.[UserAccountSalesID]
            , (
            SELECT k.OrderID,p.Name as 'ProductName',p.ID,p.ProductTypeId FROM ProductOrderItem k WITH (NOLOCK), Product p WITH (NOLOCK)
            WHERE k.Price>0 
            and k.ID = (SELECT MAX(t.ID) FROM ProductOrderItem t where t.OrderID=k.OrderID and t.Price>0 )
            and k.ProductID=p.ID 
            --2:HAA認證書,6:置頂,7:更新,8:全面曝光,9:BANNER曝光
            and p.ProductTypeId in (2,6,7,8,9)
            ) mb 
            WHERE mc.OrderStatusID=30 AND mc.PaymentStatusID=30 and mc.OrderTotal>0
            and mc.ID=mb.OrderID 
            and CONVERT(varchar(7),mc.CreateDate,120)=CONVERT(varchar(7),GETDATE()-1,120)
            --Group by ISNULL(ua.[ID], '') ,mb.ProductTypeId
            --Order by 1
            ) mas
             PIVOT
            (SUM(OrderTotal) FOR ProductType in (HAA,置頂,更新,全面曝光,BANNER曝光)) AS PivotTable

        ) as od on us.ID=od.SaleID
        WHERE us.Type='2' and us.Status=1 and us.ZoneName between 1 and 5
        ORDER BY 1"""
    
    ProductOrdreListRtn = read_test_mssql(ProductOrdreList, 'ABCCar')
    return ProductOrdreListRtn

if __name__ == "__main__":
    script = 'buyCar&Upload.py'

    try:
        rtnDF = readDataOrder()
        msg = 'PART 1 : 讀取資料成功！'
        print(msg)
    except Exception as e:
        msg = 'PART 1　: 讀取資料發生錯誤. The error message :{}'.format(e)
        send_error_message(script=script, message=msg)
        print(msg)
        sys.exit(1)

    try:
        outputList, outputListDown = computeData(rtnDF)
        msg = 'PART 2 : 計算資料成功！'
        print(msg)
    except Exception as e:
        msg = 'PART 2　: 計算資料發生錯誤. The error message :{}'.format(e)
        send_error_message(script=script, message=msg)
        print(msg)
        sys.exit(1)
    """
    try:
        analytics = initialize_analyticsreporting()
    except Exception as e:
        msg = 'PART 3: 連線GA API 讀取失敗! The error message : {}'.format(e)
        send_error_message(script=script, message=msg)
        print(msg)
        sys.exit(1)

    print('PART 3: Finished!')

    try:
        response = makeRequestWithExponentialBackoff(analytics)
        responseB = makeRequestWithExponentialBackoffB(analytics)
        responseA = makeRequestWithExponentialBackoffA(analytics)

    except Exception as e:
        msg = 'PART 4: Fetch GA 資料出現錯誤! The error message : {}'.format(e)
        send_error_message(script=script, message=msg)
        print(msg)
        sys.exit(1)

    print('PART 4: Finished!')

    try:
        final = prep_data(response)
        finalB = prep_data(responseB)
        finalAll = prep_data(responseA)
        
    except Exception as e:
        msg = 'PART 5: 清整GA資料出現錯誤! The error message : {}'.format(e)
        send_error_message(script=script, message=msg)
        print(msg)
        sys.exit(1)

    print('PART 5: Finished!')
    """
    try:
        #不二價
        innerDB = readData(901)
        #a = pd.merge(innerDB, final, on=["CarID"], how='left').fillna(0)
        a = innerDB
        a['date'] = end_date
        #cols = ['date','CarID', 'MemberID', '瀏覽次數', '帶看服務聯繫電話', '【大網】查看聯繫LINE_下', '【大網】LINE聯繫賣家_上', '【大網】查看電話', '【小網】立即通話內層', '【小網】LINE 聯繫賣家', '【小網】立即通話', 'LINE聯繫_主要']
        #cols = ['date','CarID', 'MemberID', '瀏覽次數', '帶看服務聯繫電話', '通話', '查看聯繫手機1', '查看聯繫手機2', '查看車商電話', 'LINE聯繫', 'LINE聯繫1', 'LINE聯繫2']
        cols = ['date','CarID', 'MemberID', '瀏覽次數']
        output = pd.DataFrame(a, columns=cols).fillna(0)
        print('不二價 ok')

        #安心購
        innerDBB = readData(902)
        #b = pd.merge(innerDBB, finalB, on=["CarID"], how='left').fillna(0)
        b = innerDBB
        b['date'] = end_date
        #colsB = ['date','CarID', 'MemberID', '瀏覽次數', '【大網】查看聯繫LINE_下', '【大網】LINE聯繫賣家_上', '【大網】查看電話', '【小網】立即通話內層', '【小網】LINE 聯繫賣家', '【小網】立即通話', 'LINE聯繫_主要']
        #colsB = ['date','CarID', 'MemberID', '瀏覽次數', '帶看服務聯繫電話', '通話', '查看聯繫手機1', '查看聯繫手機2', '查看車商電話', 'LINE聯繫', 'LINE聯繫1', 'LINE聯繫2']
        colsB = ['date','CarID', 'MemberID', '瀏覽次數']
        outputB = pd.DataFrame(b, columns=colsB).fillna(0)
        print('安心購 ok')

        #不二價總表
        innerDBAll = readDataAll(901)
        #c = pd.merge(innerDBAll, finalAll, on=["CarID"], how='left').fillna(0)
        #colsC = ['CarID', 'MemberID', '瀏覽次數', '帶看服務聯繫電話', '【大網】查看聯繫LINE_下', '【大網】LINE聯繫賣家_上', '【大網】查看電話', '【小網】立即通話內層', '【小網】LINE 聯繫賣家', '【小網】立即通話', 'LINE聯繫_主要']
        #colsC = ['CarID', 'MemberID', '瀏覽次數', '帶看服務聯繫電話', '通話', '查看聯繫手機1', '查看聯繫手機2', '查看車商電話', 'LINE聯繫', 'LINE聯繫1', 'LINE聯繫2']
        c = innerDBAll
        colsC = ['CarID', 'MemberID', '瀏覽次數']
        outputAll = pd.DataFrame(c, columns=colsC).fillna(0)
        
         #商品訂單
        ProductOrderDF = readProductOrderData()
        ProductOrderDF = ProductOrderDF.astype({'購買認証': 'int', '置頂': 'int', '更新': 'int', '全面曝光': 'int', 'BANNER曝光': 'int'})

        OrderYm = ProductOrderDF["OrderYM"].unique()[0]
        #print(OrderYm)
        outProductOrderDF = ProductOrderDF[["維護業務","購買認証","置頂","更新","全面曝光","BANNER曝光"]].copy()        
        outProductOrderDF.rename(columns = {'購買認証': OrderYm + '\n購買認証',
                                             '置頂': OrderYm + '\n置頂',
                                             '更新': OrderYm + '\n更新',
                                             '全面曝光': OrderYm + '\n全面曝光',
                                             'BANNER曝光': OrderYm + '\nBanner曝光'}, inplace = True)        
        
        outProductOrderDF.loc["Total"] = outProductOrderDF.sum(numeric_only=True)
        outProductOrderDF['維護業務'] = outProductOrderDF['維護業務'].fillna('小計')
        print('不二價總表 ok')
        
        del ProductOrderDF
    except Exception as e:
        msg = 'PART 6: 讀取內部資料出現錯誤! The error message : {}'.format(e)
        send_error_message(script=script, message=msg)
        print(msg)
        sys.exit(1)
    
    print('PART 6: Finished!')

    try:
        outputToExcel(outputList, outputListDown, output, outputB, outputAll, outProductOrderDF)
        msg = 'PART 7 : 輸出資料成功！'
        print(msg)
    except Exception as e:
        msg = 'PART 7　: 輸出資料發生錯誤. The error message :{}'.format(e)
        send_error_message(script=script, message=msg)
        print(msg)
        sys.exit(1)

    try:
        send_carBuy_report(receiver='j', path=path)
        msg = 'PART 8 : 寄送安心購報表OK'
        print(msg)
    except Exception as e:
        msg = 'PART 8　: 寄送安心購報表. The error message :{}'.format(e)
        send_error_message(script=script, message=msg)
        print(msg)
        sys.exit(1)

    print('PART 8: All Finished!')
    