
#==============
# Path: /home/hadoop/jessielu/report_sales
# Description: HAA Upload PDF convert to image and crop image. Send notify to Line.
# Date: 2022/12/29
# Author: Sam 
#==============

# coding: utf-8

import sys, os, importlib, re, glob
from pathlib import Path
# 正式機
sys.path.insert(0, '/home/hadoop/jessielu/')

# 測試機
#sys.path.insert(0, '/home/jessie-ws/Dev/jupyterlab/daily_report/')

# 重新載入更新後 Module
#importlib.reload(sys.modules['Module.NotifyFunc'])
from Module.NotifyFunc import HAA_Line_Notify
from datetime import datetime, timedelta, date
from dateutil.relativedelta import relativedelta  
from pdf2image import convert_from_path
from PIL import Image
import subprocess

now = datetime.now().strftime("%Y-%m-%d %H:%M")
YYYY = (date.today() - timedelta(1)).strftime("%Y")

# 正式機
path = '/home/hadoop/jessielu/report_sales/files/'
BackupPath = "/home/hadoop/jessielu/report_sales/files/HAA_{0}/".format(YYYY)

# 測試機
#path = '/home/jessie-ws/Dev/jupyterlab/daily_report/report_sales/files/files'
#BackupPath = "/home/jessie-ws/Dev/jupyterlab/daily_report/report_sales/files/files/HAA_{0}/".format(YYYY)

os.chdir(path)

# 移除前次產出圖檔
imgs = ['haa_project.png','haa_project_crop.png']
for r in imgs:
    if os.path.isfile(r):
        os.remove(r)
        
if __name__ == "__main__":
    print(now)
    script = 'send_haa_notify_project'
    
    try:
        # 正式機
        folder_path = "/home/ubuntu/HAA/"
        # 測試機
        #folder_path = "/home/jessie-ws/Dev/jupyterlab/daily_report/report_sales/files/files/"
        files_path = os.path.join(folder_path, '速報*.pdf')
        files = sorted(glob.iglob(files_path), key=os.path.getctime, reverse=True) 
        
        if (len(files)>0):
            #print (files[0]) #latest file 
            #print (files[0],files[1]) #latest two files

            filename = files[0].rsplit('/', 1)[-1]
            filename_ymd = filename.lower().replace(".pdf", "").replace("速報", "").replace("_1_2", "").replace("_1_3", "").replace("_1_4", "").replace("_1_5", "")
            filename_date = datetime.strptime(filename_ymd, '%Y%m%d').strftime('%Y/%m/%d')
            
            images = convert_from_path(files[0])
            images[0].save('haa_project.png')

            img = Image.open("haa_project.png")
            # img.crop(左，上，右，下)
            # 裁圖處理 

            im1 = img.crop((300, 60, 1160, 295))  
            im1.save('haa_project_crop.png', quality=100) 

            # 發送訊息 receiver=> j [測試發送] / g [正式發送]
            receiver = 'g'
            NotifyList = {'HAA 速報':'haa_project_crop.png'}
            for i, v in enumerate(NotifyList):
                file = NotifyList[v]
                message = "【{0}】{1}".format(str(v),filename_date)
                # \n Line 換行符號       
                message =  "\n" + message   
                HAA_Line_Notify(receiver = receiver, path = path,msg = message, FN = file)
                print(message)

            msg = 'HAA 速報 Notify 日報 OK'
            print(msg)
            
            if not os.path.exists(BackupPath):
                os.makedirs(BackupPath)
                
            for file in files:
                #if re.search('^(?=.*\d{8}).*.pdf$', file):
                #if re.search('^(.*)\d{8}.pdf$', file):
                #    print(file)
                process = "mv -f {0} {1}".format(file,BackupPath)
                subprocess.run(process, shell=True)
                print("File ({0}) move to {1}".format(file,BackupPath))
            
    except Exception as e:
        msg = '{0}: HAA 速報 Notify 日報發生錯誤. The error message : {1}'.format(script, e)
        #send_daily_report_error(msg)
        print(msg)
        sys.exit(1)
        
