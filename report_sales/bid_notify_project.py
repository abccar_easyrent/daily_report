
# coding: utf-8
import sys
import os
import numpy as np

#正式區
sys.path.insert(0, '/home/hadoop/jessielu/')

#測試區
#sys.path.insert(0, '/home/jessie-ws/Dev/jupyterlab/daily_report/')


#from Module.EmailFuncForSales import send_check_sales, send_daily_report_error, send_error_message
from Module.NotifyFunc import HAA_Line_Notify
from Module.ConnFunc import read_test_mssql

from openpyxl.styles import Border, Side, PatternFill, Font, GradientFill, Alignment
from openpyxl import Workbook, load_workbook
from openpyxl.utils.dataframe import dataframe_to_rows

import pandas as pd
from datetime import datetime, timedelta, date
from dateutil.relativedelta import relativedelta  
import subprocess

from pdf2image import convert_from_path
from PIL import Image

#原本
now = date.today()
end_date = (now - timedelta(1)).strftime("%Y-%m-%d") 
end_YM = (now - timedelta(1)).strftime("%Y-%m") 
end_Y = (now - timedelta(1)).strftime("%Y") 


# #補發
# end_date = '2023-06-17'
# end_YM = '2023-06'
# end_Y = '2023'

print(end_date)

#正式區路徑
path = '/home/hadoop/jessielu/report_sales/files/'

#測試機路徑
#path = '/home/jessie-ws/Dev/jupyterlab/daily_report/report_sales/files/'

os.chdir(path)


# 移除前次產出圖檔
imgs = ['bid_project.png','bid_project_crop.png']
for r in imgs:
    if os.path.isfile(r):
        os.remove(r)
        
def doc2pdf_linux(docPath, pdfPath):
    cmd = 'libreoffice --headless --convert-to pdf'.split()+[docPath] + ['--outdir'] + [pdfPath]
    p = subprocess.Popen(cmd, stderr=subprocess.PIPE, stdout=subprocess.PIPE,bufsize=1)
    p.wait(timeout=30)
    # stdout, stderr = p.communicate()
    # print(stdout, stderr)
    p.communicate()

def doc2pdf(docPath, pdfPath):
    docPathTrue = os.path.abspath(docPath)  
    return doc2pdf_linux(docPathTrue, pdfPath)

def readData():
    # 上架數 for DATE
    DayList1 = f"""
SELECT a.SOUID,a.SOUNM,count(ID) as [筆數] from  (
	select mas.SOUID,mas.SOU as SOUNM from (
	 select distinct 
	 isnull(SOU,'') SOU,
	(Case WHEN SOU='租賃' then 1 
		  WHEN SOU='法拍' then 2
		  WHEN SOU='一般委拍' then 3
		  WHEN SOU='個人委拍' then 4
		  WHEN SOU='經銷商委拍' then 5 
		  WHEN SOU='其他租賃' then 6 
		  ELSE 7  
		  END
	) as SOUID
	from HAAWebBidProduct group by SOU
	) mas 
) a left join  (
    SELECT * from (
		SELECT --*
		p.ID,p.Name,p.Sku,p.CreatedOnUtc,haaProduct.MEMID,haaProduct.CARID,haaProduct.CARNUM,haaProduct.SOU,haaProduct.SOUID ,haaProduct.PLATENO,haaProduct.USERID,haaProduct.USERNAME,
		--(case when DATEPART(hour,p.CreatedOnUtc) > 15 then convert(varchar(10),dateadd(DD,+1,p.CreatedOnUtc),120) else convert(varchar(10),p.CreatedOnUtc,120) end) CloseDate, 
		--(case when DATEPART(hour,p.CreatedOnUtc) > 15 then convert(varchar(7),dateadd(DD,+1,p.CreatedOnUtc),120) else convert(varchar(7),p.CreatedOnUtc,120) end) CloseYm, 
		--(case when DATEPART(hour,p.CreatedOnUtc) > 15 then convert(varchar(4),dateadd(DD,+1,p.CreatedOnUtc),120) else convert(varchar(4),p.CreatedOnUtc,120) end) CloseY 
		convert(varchar(10),DATEADD(day, 0, DATEADD(hour,(8),p.CreatedOnUtc)),120) CloseDate,  
		convert(varchar(7),DATEADD(day, 0, DATEADD(hour,(8),p.CreatedOnUtc)),120) CloseYm,  
		convert(varchar(4),DATEADD(day, 0, DATEADD(hour,(8),p.CreatedOnUtc)),120) CloseY 
		from Product p with (NOLOCK)
		OUTER APPLY 
		 (SELECT TOP 1 * ,
		(Case WHEN SOU='租賃' then 1 
			  WHEN SOU='法拍' then 2
			  WHEN SOU='一般委拍' then 3
			  WHEN SOU='個人委拍' then 4
			  WHEN SOU='經銷商委拍' then 5 
			  WHEN SOU='其他租賃' then 6 
			  ELSE 7  
			  END
		) as SOUID
		  FROM HAAWebBidProduct hwp with (NOLOCK)
		  WHERE hwp.SEQNO = p.Sku
		  ORDER BY hwp.Id DESC
		 ) AS haaProduct
		--where p.CreatedOnUtc between '2023-06-12 16:00:00' and '2023-06-13 16:00:00'
		--where p.CreatedOnUtc between convert(varchar(10), DATEADD(month, -13,getdate()),120) + ' 16:00:00' and convert(varchar(10),dateadd(DD,-1,getdate()),120) + ' 16:00:00'
  		where DATEADD(day, 0, DATEADD(hour,(8),p.CreatedOnUtc)) between convert(varchar(10), DATEADD(month, -13,getdate()),120) + ' 00:00:00' and convert(varchar(10),dateadd(DD,-1,getdate()),120) + ' 23:59:59'
	) k 
	where k.CloseDate = '{end_date}'
) b on a.SOUID=b.SOUID 
group by a.SOUID,a.SOUNM
order by 1 
    """
    
    # 結標數 for DATE (納入提前結標有產生訂單)
    DayList2 = f"""
SELECT a.SOUID,a.SOUNM,count(ID) as [筆數] from  (
	select mas.SOUID,mas.SOU as SOUNM from (
	 select distinct 
	 isnull(SOU,'') SOU,
	(Case WHEN SOU='租賃' then 1 
		  WHEN SOU='法拍' then 2
		  WHEN SOU='一般委拍' then 3
		  WHEN SOU='個人委拍' then 4
		  WHEN SOU='經銷商委拍' then 5 
		  WHEN SOU='其他租賃' then 6 
		  ELSE 7  
		  END
	) as SOUID
	from HAAWebBidProduct group by SOU
	) mas 
) a left join  (
    SELECT * from (
		SELECT --*
		p.ID,p.Name,p.Sku,p.CreatedOnUtc,haaProduct.MEMID,haaProduct.CARID,haaProduct.CARNUM,haaProduct.SOU,haaProduct.SOUID ,haaProduct.PLATENO,haaProduct.USERID,haaProduct.USERNAME,
		convert(varchar(10),DATEADD(day, 0, DATEADD(hour,(8), ISNULL(ord.OrderUtc,ISNULL(ext.EndDateTimeOnUtc, p.AuctionEndDateTimeUtc)) )),120) CloseDate,  
		convert(varchar(7),DATEADD(day, 0, DATEADD(hour,(8), ISNULL(ord.OrderUtc,ISNULL(ext.EndDateTimeOnUtc, p.AuctionEndDateTimeUtc)) )),120) CloseYm,  
		convert(varchar(4),DATEADD(day, 0, DATEADD(hour,(8), ISNULL(ord.OrderUtc,ISNULL(ext.EndDateTimeOnUtc, p.AuctionEndDateTimeUtc)) )),120) CloseY 
		from Product p with (NOLOCK)
		OUTER APPLY 
		 (SELECT TOP 1 *, (SELECT COUNT(1) FROM ProductBidHistory with (NOLOCK) WHERE ProductBidHistory.ProductId = p.Id) AS BidCount
		  FROM ProductBidHistory pb with (NOLOCK)
		  WHERE pb.ProductId = p.Id
		  ORDER BY pb.BidPrice DESC
		) AS bid
		OUTER APPLY 
		 (SELECT TOP 1 *
		  FROM ProductAuctionExtendHistory pa with (NOLOCK)
		  WHERE pa.ProductId = p.Id
		  ORDER BY pa.Id DESC
		 ) AS ext          
		OUTER APPLY 
		 (SELECT TOP 1 * ,
		(Case WHEN SOU='租賃' then 1 
			  WHEN SOU='法拍' then 2
			  WHEN SOU='一般委拍' then 3
			  WHEN SOU='個人委拍' then 4
			  WHEN SOU='經銷商委拍' then 5 
			  WHEN SOU='其他租賃' then 6 
			  ELSE 7  
			  END
		) as SOUID
		  FROM HAAWebBidProduct hwp with (NOLOCK)
		  WHERE hwp.SEQNO = p.Sku
		  ORDER BY hwp.Id DESC
		 ) AS haaProduct 
		LEFT JOIN (SELECT oi.OrderId,o.OrderStatusId,o.CreatedOnUtc as OrderUtc,oi.ProductId from [Order] o with (NOLOCK),OrderItem oi with (NOLOCK) where oi.OrderId = o.Id and o.OrderStatusId in (10,30)) ord on ord.ProductId=p.Id 
		WHERE  p.Deleted = 0 AND p.AuctionStatusId > 0 
		AND p.AuctionCheckoutTypeId > 0 --結標
		--AND p.AuctionCheckoutTypeId in (10, 20, 30) --結標成交
		--AND ISNULL(ext.EndDateTimeOnUtc, p.AuctionEndDateTimeUtc) between '2023-05-31 16:00:00' and '2023-06-01 16:00:00'
		AND DATEADD(day, 0, DATEADD(hour,(8), ISNULL(ord.OrderUtc,ISNULL(ext.EndDateTimeOnUtc, p.AuctionEndDateTimeUtc)) )) between convert(varchar(10), DATEADD(month, -13,getdate()),120) + ' 00:00:00' and convert(varchar(10),dateadd(DD,-1,getdate()),120) + ' 23:59:59'
		AND p.ID not in (SELECT e.ProductId FROM LogExt e with (NOLOCK) WHERE e.TypeId='10') --剔除手動提前結束
	) k 
	where k.CloseDate = '{end_date}'
) b on a.SOUID=b.SOUID 
group by a.SOUID,a.SOUNM
order by 1   
    """
    
    # 成交數 for DATE (已付款)
    DayList3_1 = f"""
SELECT a.SOUID,a.SOUNM,count(ID) as [筆數] from  (
	select mas.SOUID,mas.SOU as SOUNM from (
	 select distinct 
	 isnull(SOU,'') SOU,
	(Case WHEN SOU='租賃' then 1 
		  WHEN SOU='法拍' then 2
		  WHEN SOU='一般委拍' then 3
		  WHEN SOU='個人委拍' then 4
		  WHEN SOU='經銷商委拍' then 5 
		  WHEN SOU='其他租賃' then 6 
		  ELSE 7  
		  END
	) as SOUID
	from HAAWebBidProduct group by SOU
	) mas 
) a left join  (
    SELECT * from (
		SELECT --*
		o.ID as OrderID,p.ID,p.Name,p.Sku,p.CreatedOnUtc,haaProduct.MEMID,haaProduct.CARID,haaProduct.CARNUM,haaProduct.SOU,haaProduct.SOUID ,haaProduct.PLATENO,haaProduct.USERID,haaProduct.USERNAME,
		convert(varchar(10),DATEADD(day, 0, DATEADD(hour,(8),o.CreatedOnUtc)),120) CloseDate,  
		convert(varchar(7),DATEADD(day, 0, DATEADD(hour,(8),o.CreatedOnUtc)),120) CloseYm,  
		convert(varchar(4),DATEADD(day, 0, DATEADD(hour,(8),o.CreatedOnUtc)),120) CloseY 
		from [Order] o with (NOLOCK)
		LEFT JOIN OrderItem oi with (NOLOCK) on oi.OrderId = o.Id
		LEFT JOIN Product p with (NOLOCK) on p.Id = oi.ProductId
		OUTER APPLY 
		 (SELECT TOP 1 * ,
		(Case WHEN SOU='租賃' then 1 
			  WHEN SOU='法拍' then 2
			  WHEN SOU='一般委拍' then 3
			  WHEN SOU='個人委拍' then 4
			  WHEN SOU='經銷商委拍' then 5 
			  WHEN SOU='其他租賃' then 6 
			  ELSE 7  
			  END
		) as SOUID
		  FROM HAAWebBidProduct hwp with (NOLOCK)
		  WHERE hwp.SEQNO = p.Sku
		  ORDER BY hwp.Id DESC
		 ) AS haaProduct
		--where o.CreatedOnUtc between '2023-06-12 16:00:00' and '2023-06-13 16:00:00'
		where DATEADD(day, 0, DATEADD(hour,(8),o.CreatedOnUtc)) between convert(varchar(10), DATEADD(month, -13,getdate()),120) + ' 00:00:00' and convert(varchar(10),dateadd(DD,-1,getdate()),120) + ' 23:59:59'
		and o.OrderStatusId = 30
	) k 
	where k.CloseDate= '{end_date}'
) b on a.SOUID=b.SOUID 
group by a.SOUID,a.SOUNM
order by 1 
 
    """   
    
    # 成交數 for DATE (待付款)
    DayList3_2 = f"""
SELECT a.SOUID,a.SOUNM,count(ID) as [筆數] from  (
	select mas.SOUID,mas.SOU as SOUNM from (
	 select distinct 
	 isnull(SOU,'') SOU,
	(Case WHEN SOU='租賃' then 1 
		  WHEN SOU='法拍' then 2
		  WHEN SOU='一般委拍' then 3
		  WHEN SOU='個人委拍' then 4
		  WHEN SOU='經銷商委拍' then 5 
		  WHEN SOU='其他租賃' then 6 
		  ELSE 7  
		  END
	) as SOUID
	from HAAWebBidProduct group by SOU
	) mas 
) a left join  (
    SELECT * from (
		SELECT --*
		o.ID as OrderID,p.ID,p.Name,p.Sku,p.CreatedOnUtc,haaProduct.MEMID,haaProduct.CARID,haaProduct.CARNUM,haaProduct.SOU,haaProduct.SOUID ,haaProduct.PLATENO,haaProduct.USERID,haaProduct.USERNAME,
		convert(varchar(10),DATEADD(day, 0, DATEADD(hour,(8),o.CreatedOnUtc)),120) CloseDate,  
		convert(varchar(7),DATEADD(day, 0, DATEADD(hour,(8),o.CreatedOnUtc)),120) CloseYm,  
		convert(varchar(4),DATEADD(day, 0, DATEADD(hour,(8),o.CreatedOnUtc)),120) CloseY 
		from [Order] o with (NOLOCK)
		LEFT JOIN OrderItem oi with (NOLOCK) on oi.OrderId = o.Id
		LEFT JOIN Product p with (NOLOCK) on p.Id = oi.ProductId
		OUTER APPLY 
		 (SELECT TOP 1 * ,
		(Case WHEN SOU='租賃' then 1 
			  WHEN SOU='法拍' then 2
			  WHEN SOU='一般委拍' then 3
			  WHEN SOU='個人委拍' then 4
			  WHEN SOU='經銷商委拍' then 5 
			  WHEN SOU='其他租賃' then 6 
			  ELSE 7  
			  END
		) as SOUID
		  FROM HAAWebBidProduct hwp with (NOLOCK)
		  WHERE hwp.SEQNO = p.Sku
		  ORDER BY hwp.Id DESC
		 ) AS haaProduct
		--where o.CreatedOnUtc between '2023-06-12 16:00:00' and '2023-06-13 16:00:00'
		where DATEADD(day, 0, DATEADD(hour,(8),o.CreatedOnUtc)) between convert(varchar(10), DATEADD(month, -13,getdate()),120) + ' 00:00:00' and convert(varchar(10),dateadd(DD,-1,getdate()),120) + ' 23:59:59'
		and o.OrderStatusId = 10
	) k 
	where k.CloseDate= '{end_date}'
) b on a.SOUID=b.SOUID 
group by a.SOUID,a.SOUNM
order by 1 
 
    """   
    
    
    # 上架數 for Month
    MonthList1 = f""" 
SELECT a.SOUID,a.SOUNM,count(ID) as [筆數] from  (
	select mas.SOUID,mas.SOU as SOUNM from (
	 select distinct 
	 isnull(SOU,'') SOU,
	(Case WHEN SOU='租賃' then 1 
		  WHEN SOU='法拍' then 2
		  WHEN SOU='一般委拍' then 3
		  WHEN SOU='個人委拍' then 4
		  WHEN SOU='經銷商委拍' then 5 
		  WHEN SOU='其他租賃' then 6 
		  ELSE 7  
		  END
	) as SOUID
	from HAAWebBidProduct group by SOU
	) mas 
) a left join  (
    SELECT * from (
		SELECT --*
		p.ID,p.Name,p.Sku,p.CreatedOnUtc,haaProduct.MEMID,haaProduct.CARID,haaProduct.CARNUM,haaProduct.SOU,haaProduct.SOUID ,haaProduct.PLATENO,haaProduct.USERID,haaProduct.USERNAME,
		--(case when DATEPART(hour,p.CreatedOnUtc) > 15 then convert(varchar(10),dateadd(DD,+1,p.CreatedOnUtc),120) else convert(varchar(10),p.CreatedOnUtc,120) end) CloseDate, 
		--(case when DATEPART(hour,p.CreatedOnUtc) > 15 then convert(varchar(7),dateadd(DD,+1,p.CreatedOnUtc),120) else convert(varchar(7),p.CreatedOnUtc,120) end) CloseYm, 
		--(case when DATEPART(hour,p.CreatedOnUtc) > 15 then convert(varchar(4),dateadd(DD,+1,p.CreatedOnUtc),120) else convert(varchar(4),p.CreatedOnUtc,120) end) CloseY 
		convert(varchar(10),DATEADD(day, 0, DATEADD(hour,(8),p.CreatedOnUtc)),120) CloseDate,  
		convert(varchar(7),DATEADD(day, 0, DATEADD(hour,(8),p.CreatedOnUtc)),120) CloseYm,  
		convert(varchar(4),DATEADD(day, 0, DATEADD(hour,(8),p.CreatedOnUtc)),120) CloseY 
		from Product p with (NOLOCK)
		OUTER APPLY 
		 (SELECT TOP 1 * ,
		(Case WHEN SOU='租賃' then 1 
			  WHEN SOU='法拍' then 2
			  WHEN SOU='一般委拍' then 3
			  WHEN SOU='個人委拍' then 4
			  WHEN SOU='經銷商委拍' then 5 
			  WHEN SOU='其他租賃' then 6 
			  ELSE 7  
			  END
		) as SOUID
		  FROM HAAWebBidProduct hwp with (NOLOCK)
		  WHERE hwp.SEQNO = p.Sku
		  ORDER BY hwp.Id DESC
		 ) AS haaProduct
		--where p.CreatedOnUtc between '2023-06-12 16:00:00' and '2023-06-13 16:00:00'
		--where p.CreatedOnUtc between convert(varchar(10), DATEADD(month, -13,getdate()),120) + ' 16:00:00' and convert(varchar(10),dateadd(DD,-1,getdate()),120) + ' 16:00:00'
		where DATEADD(day, 0, DATEADD(hour,(8),p.CreatedOnUtc)) between convert(varchar(10), DATEADD(month, -13,getdate()),120) + ' 00:00:00' and convert(varchar(10),dateadd(DD,-1,getdate()),120) + ' 23:59:59'
	) k 
	where k.CloseYm = '{end_YM}'
) b on a.SOUID=b.SOUID 
group by a.SOUID,a.SOUNM
order by 1 
    """

    # 結標數 for Month (納入提前結標有產生訂單)
    MonthList2 = f"""
SELECT a.SOUID,a.SOUNM,count(ID) as [筆數] from  (
	select mas.SOUID,mas.SOU as SOUNM from (
	 select distinct 
	 isnull(SOU,'') SOU,
	(Case WHEN SOU='租賃' then 1 
		  WHEN SOU='法拍' then 2
		  WHEN SOU='一般委拍' then 3
		  WHEN SOU='個人委拍' then 4
		  WHEN SOU='經銷商委拍' then 5 
		  WHEN SOU='其他租賃' then 6 
		  ELSE 7  
		  END
	) as SOUID
	from HAAWebBidProduct group by SOU
	) mas 
) a left join  (
    SELECT * from (
		SELECT --*
		p.ID,p.Name,p.Sku,p.CreatedOnUtc,haaProduct.MEMID,haaProduct.CARID,haaProduct.CARNUM,haaProduct.SOU,haaProduct.SOUID ,haaProduct.PLATENO,haaProduct.USERID,haaProduct.USERNAME,
		convert(varchar(10),DATEADD(day, 0, DATEADD(hour,(8), ISNULL(ord.OrderUtc,ISNULL(ext.EndDateTimeOnUtc, p.AuctionEndDateTimeUtc)) )),120) CloseDate,  
		convert(varchar(7),DATEADD(day, 0, DATEADD(hour,(8), ISNULL(ord.OrderUtc,ISNULL(ext.EndDateTimeOnUtc, p.AuctionEndDateTimeUtc)) )),120) CloseYm,  
		convert(varchar(4),DATEADD(day, 0, DATEADD(hour,(8), ISNULL(ord.OrderUtc,ISNULL(ext.EndDateTimeOnUtc, p.AuctionEndDateTimeUtc)) )),120) CloseY 
		from Product p with (NOLOCK)
		OUTER APPLY 
		 (SELECT TOP 1 *, (SELECT COUNT(1) FROM ProductBidHistory with (NOLOCK) WHERE ProductBidHistory.ProductId = p.Id) AS BidCount
		  FROM ProductBidHistory pb with (NOLOCK)
		  WHERE pb.ProductId = p.Id
		  ORDER BY pb.BidPrice DESC
		) AS bid
		OUTER APPLY 
		 (SELECT TOP 1 *
		  FROM ProductAuctionExtendHistory pa with (NOLOCK)
		  WHERE pa.ProductId = p.Id
		  ORDER BY pa.Id DESC
		 ) AS ext          
		OUTER APPLY 
		 (SELECT TOP 1 * ,
		(Case WHEN SOU='租賃' then 1 
			  WHEN SOU='法拍' then 2
			  WHEN SOU='一般委拍' then 3
			  WHEN SOU='個人委拍' then 4
			  WHEN SOU='經銷商委拍' then 5 
			  WHEN SOU='其他租賃' then 6 
			  ELSE 7  
			  END
		) as SOUID
		  FROM HAAWebBidProduct hwp with (NOLOCK)
		  WHERE hwp.SEQNO = p.Sku
		  ORDER BY hwp.Id DESC
		 ) AS haaProduct 
		LEFT JOIN (SELECT oi.OrderId,o.OrderStatusId,o.CreatedOnUtc as OrderUtc,oi.ProductId from [Order] o with (NOLOCK),OrderItem oi with (NOLOCK) where oi.OrderId = o.Id and o.OrderStatusId in (10,30)) ord on ord.ProductId=p.Id 
		WHERE  p.Deleted = 0 AND p.AuctionStatusId > 0 
		AND p.AuctionCheckoutTypeId > 0 --結標
		--AND p.AuctionCheckoutTypeId in (10, 20, 30) --結標成交
		--AND ISNULL(ext.EndDateTimeOnUtc, p.AuctionEndDateTimeUtc) between '2023-05-31 16:00:00' and '2023-06-01 16:00:00'
		AND DATEADD(day, 0, DATEADD(hour,(8), ISNULL(ord.OrderUtc,ISNULL(ext.EndDateTimeOnUtc, p.AuctionEndDateTimeUtc)) )) between convert(varchar(10), DATEADD(month, -13,getdate()),120) + ' 00:00:00' and convert(varchar(10),dateadd(DD,-1,getdate()),120) + ' 23:59:59'
		AND p.ID not in (SELECT e.ProductId FROM LogExt e with (NOLOCK) WHERE e.TypeId='10') --剔除手動提前結束
	) k 
	where k.CloseYm = '{end_YM}'
) b on a.SOUID=b.SOUID 
group by a.SOUID,a.SOUNM
order by 1    
    """
    
    # 成交數 for Month (已付款)
    MonthList3_1 = f"""
SELECT a.SOUID,a.SOUNM,count(ID) as [筆數] from  (
	select mas.SOUID,mas.SOU as SOUNM from (
	 select distinct 
	 isnull(SOU,'') SOU,
	(Case WHEN SOU='租賃' then 1 
		  WHEN SOU='法拍' then 2
		  WHEN SOU='一般委拍' then 3
		  WHEN SOU='個人委拍' then 4
		  WHEN SOU='經銷商委拍' then 5 
		  WHEN SOU='其他租賃' then 6 
		  ELSE 7  
		  END
	) as SOUID
	from HAAWebBidProduct group by SOU
	) mas 
) a left join  (
    SELECT * from (
		SELECT --*
		o.ID as OrderID,p.ID,p.Name,p.Sku,p.CreatedOnUtc,haaProduct.MEMID,haaProduct.CARID,haaProduct.CARNUM,haaProduct.SOU,haaProduct.SOUID ,haaProduct.PLATENO,haaProduct.USERID,haaProduct.USERNAME,
		convert(varchar(10),DATEADD(day, 0, DATEADD(hour,(8),o.CreatedOnUtc)),120) CloseDate,  
		convert(varchar(7),DATEADD(day, 0, DATEADD(hour,(8),o.CreatedOnUtc)),120) CloseYm,  
		convert(varchar(4),DATEADD(day, 0, DATEADD(hour,(8),o.CreatedOnUtc)),120) CloseY 
		from [Order] o with (NOLOCK)
		LEFT JOIN OrderItem oi with (NOLOCK) on oi.OrderId = o.Id
		LEFT JOIN Product p with (NOLOCK) on p.Id = oi.ProductId
		OUTER APPLY 
		 (SELECT TOP 1 * ,
		(Case WHEN SOU='租賃' then 1 
			  WHEN SOU='法拍' then 2
			  WHEN SOU='一般委拍' then 3
			  WHEN SOU='個人委拍' then 4
			  WHEN SOU='經銷商委拍' then 5 
			  WHEN SOU='其他租賃' then 6 
			  ELSE 7  
			  END
		) as SOUID
		  FROM HAAWebBidProduct hwp with (NOLOCK)
		  WHERE hwp.SEQNO = p.Sku
		  ORDER BY hwp.Id DESC
		 ) AS haaProduct
		--where o.CreatedOnUtc between '2023-06-12 16:00:00' and '2023-06-13 16:00:00'
		where DATEADD(day, 0, DATEADD(hour,(8),o.CreatedOnUtc)) between convert(varchar(10), DATEADD(month, -13,getdate()),120) + ' 00:00:00' and convert(varchar(10),dateadd(DD,-1,getdate()),120) + ' 23:59:59'
		and o.OrderStatusId = 30
	) k 
	where k.CloseYm = '{end_YM}'
) b on a.SOUID=b.SOUID 
group by a.SOUID,a.SOUNM
order by 1 

    """   
    
    # 成交數 for Month (待付款)
    MonthList3_2 = f"""
SELECT a.SOUID,a.SOUNM,count(ID) as [筆數] from  (
	select mas.SOUID,mas.SOU as SOUNM from (
	 select distinct 
	 isnull(SOU,'') SOU,
	(Case WHEN SOU='租賃' then 1 
		  WHEN SOU='法拍' then 2
		  WHEN SOU='一般委拍' then 3
		  WHEN SOU='個人委拍' then 4
		  WHEN SOU='經銷商委拍' then 5 
		  WHEN SOU='其他租賃' then 6 
		  ELSE 7  
		  END
	) as SOUID
	from HAAWebBidProduct group by SOU
	) mas 
) a left join  (
    SELECT * from (
		SELECT --*
		o.ID as OrderID,p.ID,p.Name,p.Sku,p.CreatedOnUtc,haaProduct.MEMID,haaProduct.CARID,haaProduct.CARNUM,haaProduct.SOU,haaProduct.SOUID ,haaProduct.PLATENO,haaProduct.USERID,haaProduct.USERNAME,
		convert(varchar(10),DATEADD(day, 0, DATEADD(hour,(8),o.CreatedOnUtc)),120) CloseDate,  
		convert(varchar(7),DATEADD(day, 0, DATEADD(hour,(8),o.CreatedOnUtc)),120) CloseYm,  
		convert(varchar(4),DATEADD(day, 0, DATEADD(hour,(8),o.CreatedOnUtc)),120) CloseY 
		from [Order] o with (NOLOCK)
		LEFT JOIN OrderItem oi with (NOLOCK) on oi.OrderId = o.Id
		LEFT JOIN Product p with (NOLOCK) on p.Id = oi.ProductId
		OUTER APPLY 
		 (SELECT TOP 1 * ,
		(Case WHEN SOU='租賃' then 1 
			  WHEN SOU='法拍' then 2
			  WHEN SOU='一般委拍' then 3
			  WHEN SOU='個人委拍' then 4
			  WHEN SOU='經銷商委拍' then 5 
			  WHEN SOU='其他租賃' then 6 
			  ELSE 7  
			  END
		) as SOUID
		  FROM HAAWebBidProduct hwp with (NOLOCK)
		  WHERE hwp.SEQNO = p.Sku
		  ORDER BY hwp.Id DESC
		 ) AS haaProduct
		--where o.CreatedOnUtc between '2023-06-12 16:00:00' and '2023-06-13 16:00:00'
		where DATEADD(day, 0, DATEADD(hour,(8),o.CreatedOnUtc)) between convert(varchar(10), DATEADD(month, -13,getdate()),120) + ' 00:00:00' and convert(varchar(10),dateadd(DD,-1,getdate()),120) + ' 23:59:59'
		and o.OrderStatusId = 10
	) k 
	where k.CloseYm = '{end_YM}'
) b on a.SOUID=b.SOUID 
group by a.SOUID,a.SOUNM
order by 1 

    """   
    
    # 上架數 for YEAR
    YearList1 = f"""
SELECT a.SOUID,a.SOUNM,count(ID) as [筆數] from  (
	select mas.SOUID,mas.SOU as SOUNM from (
	 select distinct 
	 isnull(SOU,'') SOU,
	(Case WHEN SOU='租賃' then 1 
		  WHEN SOU='法拍' then 2
		  WHEN SOU='一般委拍' then 3
		  WHEN SOU='個人委拍' then 4
		  WHEN SOU='經銷商委拍' then 5 
		  WHEN SOU='其他租賃' then 6 
		  ELSE 7  
		  END
	) as SOUID
	from HAAWebBidProduct group by SOU
	) mas 
) a left join  (
    SELECT * from (
		SELECT --*
		p.ID,p.Name,p.Sku,p.CreatedOnUtc,haaProduct.MEMID,haaProduct.CARID,haaProduct.CARNUM,haaProduct.SOU,haaProduct.SOUID ,haaProduct.PLATENO,haaProduct.USERID,haaProduct.USERNAME,
		--(case when DATEPART(hour,p.CreatedOnUtc) > 15 then convert(varchar(10),dateadd(DD,+1,p.CreatedOnUtc),120) else convert(varchar(10),p.CreatedOnUtc,120) end) CloseDate, 
		--(case when DATEPART(hour,p.CreatedOnUtc) > 15 then convert(varchar(7),dateadd(DD,+1,p.CreatedOnUtc),120) else convert(varchar(7),p.CreatedOnUtc,120) end) CloseYm, 
		--(case when DATEPART(hour,p.CreatedOnUtc) > 15 then convert(varchar(4),dateadd(DD,+1,p.CreatedOnUtc),120) else convert(varchar(4),p.CreatedOnUtc,120) end) CloseY 
		convert(varchar(10),DATEADD(day, 0, DATEADD(hour,(8),p.CreatedOnUtc)),120) CloseDate,  
		convert(varchar(7),DATEADD(day, 0, DATEADD(hour,(8),p.CreatedOnUtc)),120) CloseYm,  
		convert(varchar(4),DATEADD(day, 0, DATEADD(hour,(8),p.CreatedOnUtc)),120) CloseY 
		from Product p with (NOLOCK)
		OUTER APPLY 
		 (SELECT TOP 1 * ,
		(Case WHEN SOU='租賃' then 1 
			  WHEN SOU='法拍' then 2
			  WHEN SOU='一般委拍' then 3
			  WHEN SOU='個人委拍' then 4
			  WHEN SOU='經銷商委拍' then 5 
			  WHEN SOU='其他租賃' then 6 
			  ELSE 7  
			  END
		) as SOUID
		  FROM HAAWebBidProduct hwp with (NOLOCK)
		  WHERE hwp.SEQNO = p.Sku
		  ORDER BY hwp.Id DESC
		 ) AS haaProduct
		--where p.CreatedOnUtc between '2023-06-12 16:00:00' and '2023-06-13 16:00:00'
		--where p.CreatedOnUtc between convert(varchar(10), DATEADD(month, -13,getdate()),120) + ' 16:00:00' and convert(varchar(10),dateadd(DD,-1,getdate()),120) + ' 16:00:00'
		where DATEADD(day, 0, DATEADD(hour,(8),p.CreatedOnUtc)) between convert(varchar(10), DATEADD(month, -13,getdate()),120) + ' 00:00:00' and convert(varchar(10),dateadd(DD,-1,getdate()),120) + ' 23:59:59'
	) k 
	where k.CloseY = '{end_Y}'
) b on a.SOUID=b.SOUID 
group by a.SOUID,a.SOUNM
order by 1 
    """
    
    # 結標數 for YEAR (納入提前結標有產生訂單)
    YearList2 = f"""
SELECT a.SOUID,a.SOUNM,count(ID) as [筆數] from  (
	select mas.SOUID,mas.SOU as SOUNM from (
	 select distinct 
	 isnull(SOU,'') SOU,
	(Case WHEN SOU='租賃' then 1 
		  WHEN SOU='法拍' then 2
		  WHEN SOU='一般委拍' then 3
		  WHEN SOU='個人委拍' then 4
		  WHEN SOU='經銷商委拍' then 5 
		  WHEN SOU='其他租賃' then 6 
		  ELSE 7  
		  END
	) as SOUID
	from HAAWebBidProduct group by SOU
	) mas 
) a left join  (
    SELECT * from (
		SELECT --*
		p.ID,p.Name,p.Sku,p.CreatedOnUtc,haaProduct.MEMID,haaProduct.CARID,haaProduct.CARNUM,haaProduct.SOU,haaProduct.SOUID ,haaProduct.PLATENO,haaProduct.USERID,haaProduct.USERNAME,
		convert(varchar(10),DATEADD(day, 0, DATEADD(hour,(8), ISNULL(ord.OrderUtc,ISNULL(ext.EndDateTimeOnUtc, p.AuctionEndDateTimeUtc)) )),120) CloseDate,  
		convert(varchar(7),DATEADD(day, 0, DATEADD(hour,(8), ISNULL(ord.OrderUtc,ISNULL(ext.EndDateTimeOnUtc, p.AuctionEndDateTimeUtc)) )),120) CloseYm,  
		convert(varchar(4),DATEADD(day, 0, DATEADD(hour,(8), ISNULL(ord.OrderUtc,ISNULL(ext.EndDateTimeOnUtc, p.AuctionEndDateTimeUtc)) )),120) CloseY 
		from Product p with (NOLOCK)
		OUTER APPLY 
		 (SELECT TOP 1 *, (SELECT COUNT(1) FROM ProductBidHistory with (NOLOCK) WHERE ProductBidHistory.ProductId = p.Id) AS BidCount
		  FROM ProductBidHistory pb with (NOLOCK)
		  WHERE pb.ProductId = p.Id
		  ORDER BY pb.BidPrice DESC
		) AS bid
		OUTER APPLY 
		 (SELECT TOP 1 *
		  FROM ProductAuctionExtendHistory pa with (NOLOCK)
		  WHERE pa.ProductId = p.Id
		  ORDER BY pa.Id DESC
		 ) AS ext          
		OUTER APPLY 
		 (SELECT TOP 1 * ,
		(Case WHEN SOU='租賃' then 1 
			  WHEN SOU='法拍' then 2
			  WHEN SOU='一般委拍' then 3
			  WHEN SOU='個人委拍' then 4
			  WHEN SOU='經銷商委拍' then 5 
			  WHEN SOU='其他租賃' then 6 
			  ELSE 7  
			  END
		) as SOUID
		  FROM HAAWebBidProduct hwp with (NOLOCK)
		  WHERE hwp.SEQNO = p.Sku
		  ORDER BY hwp.Id DESC
		 ) AS haaProduct 
		LEFT JOIN (SELECT oi.OrderId,o.OrderStatusId,o.CreatedOnUtc as OrderUtc,oi.ProductId from [Order] o with (NOLOCK),OrderItem oi with (NOLOCK) where oi.OrderId = o.Id and o.OrderStatusId in (10,30)) ord on ord.ProductId=p.Id 
		WHERE  p.Deleted = 0 AND p.AuctionStatusId > 0 
		AND p.AuctionCheckoutTypeId > 0 --結標
		--AND p.AuctionCheckoutTypeId in (10, 20, 30) --結標成交
		--AND ISNULL(ext.EndDateTimeOnUtc, p.AuctionEndDateTimeUtc) between '2023-05-31 16:00:00' and '2023-06-01 16:00:00'
		AND DATEADD(day, 0, DATEADD(hour,(8), ISNULL(ord.OrderUtc,ISNULL(ext.EndDateTimeOnUtc, p.AuctionEndDateTimeUtc)) )) between convert(varchar(10), DATEADD(month, -13,getdate()),120) + ' 00:00:00' and convert(varchar(10),dateadd(DD,-1,getdate()),120) + ' 23:59:59'
		AND p.ID not in (SELECT e.ProductId FROM LogExt e with (NOLOCK) WHERE e.TypeId='10') --剔除手動提前結束
	) k 
	where k.CloseY = '{end_Y}'
) b on a.SOUID=b.SOUID 
group by a.SOUID,a.SOUNM
order by 1   
    """
    
    # 成交數 for YEAR
    YearList3 = f"""
SELECT a.SOUID,a.SOUNM,count(ID) as [筆數] from  (
	select mas.SOUID,mas.SOU as SOUNM from (
	 select distinct 
	 isnull(SOU,'') SOU,
	(Case WHEN SOU='租賃' then 1 
		  WHEN SOU='法拍' then 2
		  WHEN SOU='一般委拍' then 3
		  WHEN SOU='個人委拍' then 4
		  WHEN SOU='經銷商委拍' then 5 
		  WHEN SOU='其他租賃' then 6 
		  ELSE 7  
		  END
	) as SOUID
	from HAAWebBidProduct group by SOU
	) mas 
) a left join  (
    SELECT * from (
		SELECT --*
		o.ID as OrderID,p.ID,p.Name,p.Sku,p.CreatedOnUtc,haaProduct.MEMID,haaProduct.CARID,haaProduct.CARNUM,haaProduct.SOU,haaProduct.SOUID ,haaProduct.PLATENO,haaProduct.USERID,haaProduct.USERNAME,
		convert(varchar(10),DATEADD(day, 0, DATEADD(hour,(8),o.CreatedOnUtc)),120) CloseDate,  
		convert(varchar(7),DATEADD(day, 0, DATEADD(hour,(8),o.CreatedOnUtc)),120) CloseYm,  
		convert(varchar(4),DATEADD(day, 0, DATEADD(hour,(8),o.CreatedOnUtc)),120) CloseY 
		from [Order] o with (NOLOCK)
		LEFT JOIN OrderItem oi with (NOLOCK) on oi.OrderId = o.Id
		LEFT JOIN Product p with (NOLOCK) on p.Id = oi.ProductId
		OUTER APPLY 
		 (SELECT TOP 1 * ,
		(Case WHEN SOU='租賃' then 1 
			  WHEN SOU='法拍' then 2
			  WHEN SOU='一般委拍' then 3
			  WHEN SOU='個人委拍' then 4
			  WHEN SOU='經銷商委拍' then 5 
			  WHEN SOU='其他租賃' then 6 
			  ELSE 7  
			  END
		) as SOUID
		  FROM HAAWebBidProduct hwp with (NOLOCK)
		  WHERE hwp.SEQNO = p.Sku
		  ORDER BY hwp.Id DESC
		 ) AS haaProduct
		--where o.CreatedOnUtc between '2023-06-12 16:00:00' and '2023-06-13 16:00:00'
		where DATEADD(day, 0, DATEADD(hour,(8),o.CreatedOnUtc)) between convert(varchar(10), DATEADD(month, -13,getdate()),120) + ' 00:00:00' and convert(varchar(10),dateadd(DD,-1,getdate()),120) + ' 23:59:59'
		and o.OrderStatusId = 30
	) k 
	where k.CloseY = '{end_Y}'
) b on a.SOUID=b.SOUID 
group by a.SOUID,a.SOUNM
order by 1 

    """   
    
    Date1Rtn = read_test_mssql(DayList1, 'NopHotDB')
    Date2Rtn = read_test_mssql(DayList2, 'NopHotDB')
    Date3Rtn1 = read_test_mssql(DayList3_1, 'NopHotDB')  
    Date3Rtn2 = read_test_mssql(DayList3_2, 'NopHotDB') 
    Month1Rtn = read_test_mssql(MonthList1, 'NopHotDB')
    Month2Rtn = read_test_mssql(MonthList2, 'NopHotDB')
    Month3Rtn1 = read_test_mssql(MonthList3_1, 'NopHotDB')
    Month3Rtn2 = read_test_mssql(MonthList3_2, 'NopHotDB')    
    Year1Rtn = read_test_mssql(YearList1, 'NopHotDB')
    Year2Rtn = read_test_mssql(YearList2, 'NopHotDB')
    Year3Rtn = read_test_mssql(YearList3, 'NopHotDB')
    
    return Date1Rtn,Date2Rtn,Date3Rtn1,Date3Rtn2,Month1Rtn,Month2Rtn,Month3Rtn1,Month3Rtn2,Year1Rtn,Year2Rtn,Year3Rtn

# 將資料輸出至Excel
def addValueToExcel(ws_main, valueList, rowIndex, colIndex):
    for index, value in enumerate(valueList):
        grid = ws_main.cell(row=rowIndex+index, column=colIndex)
        grid.value = value

if __name__ == '__main__':
    
    script = 'bid_notify_project'

    try:
        rtnDF1,rtnDF2,rtnDF31,rtnDF32,rtnMF1,rtnMF2,rtnMF31,rtnMF32,rtnYF1,rtnYF2,rtnYF3 = readData()
        msg = 'PART 1 done!'
        print(msg) 
        
    except Exception as e:
        msg = '{0}: PART 1: 讀取資料發生錯誤! The error message : {1}'.format(script, e)
        #send_daily_report_error(msg)
        print(msg)
        sys.exit(1)
        
    try:
        #=========================== Load office file 
        wordpath= path + '即時拍日報.xlsx'
        
        wb = load_workbook(wordpath)

        #Sheet 1
        ws_main = wb.worksheets[0]

        # Add today date
        date = ws_main.cell(row=3, column=1)
        date.value = end_date
        # 本月合計
        month = ws_main.cell(row=3, column=7)
        month.value = end_YM + '合計'       
        # 年度合計
        year = ws_main.cell(row=3, column=13)
        year.value = end_Y + '年度合計'
        
        #當日上架數
        addValueToExcel(ws_main, rtnDF1['筆數'].tolist(), 6, 3)
        #當日結標數
        addValueToExcel(ws_main, rtnDF2['筆數'].tolist(), 6, 4) 
        #當日成交數 (已付款)
        addValueToExcel(ws_main, rtnDF31['筆數'].tolist(), 6, 5)   
        #當日成交數 (待付款)
        addValueToExcel(ws_main, rtnDF32['筆數'].tolist(), 6, 6)      
        #當月上架數
        addValueToExcel(ws_main, rtnMF1['筆數'].tolist(), 6, 9)
        #當月結標數
        addValueToExcel(ws_main, rtnMF2['筆數'].tolist(), 6, 10)
        #當月成交數 (已付款)
        addValueToExcel(ws_main, rtnMF31['筆數'].tolist(), 6, 11)
        #當月成交數 (未付款)
        addValueToExcel(ws_main, rtnMF32['筆數'].tolist(), 6, 12)
        #當年上架數
        addValueToExcel(ws_main, rtnYF1['筆數'].tolist(), 6, 15)
        #當年結標數
        addValueToExcel(ws_main, rtnYF2['筆數'].tolist(), 6, 16)
        #當年成交數
        addValueToExcel(ws_main, rtnYF3['筆數'].tolist(), 6, 17)
 
        # Save the worksheet
        wb.save(wordpath)
        
        msg = 'PART 2 done!'
        print('數據寫入!')

        #Excel 轉 PDF
        pdfPath = path
        doc2pdf(wordpath, pdfPath)   

    except Exception as e:
        msg = '{0}: PART 2: 數據寫入出現錯誤! The error message : {1}'.format(script, e)
        #send_daily_report_error(script='{}'.format(script), message=msg)
        print(msg)
        sys.exit(1)

    try:
        # PDF轉圖檔
        images = convert_from_path(path + '即時拍日報.pdf')
        images[0].save(path + 'bid_project.png')
        # 裁圖另存
        img = Image.open(path + 'bid_project.png')
        
        # img.crop(左，上，右，下)
        # 正式區
        #im1 = img.crop((120, 140, 2200, 770))
        im1 = img.crop((95, 140, 2225, 810))
        
        # 測試區
        #im1 = img.crop((120, 140, 1945, 770))
        #im1 = img.crop((120, 140, 2145, 690))
        im1.save(path + 'bid_project_crop.png', quality=50) 
        
        print('Part 3 done!')
    except Exception as e:
        msg = '{0}: PART 3: 即時拍日報轉檔失敗! The error message : {1}'.format(script, e)
        print(msg)
        sys.exit(1)

    try:
        # 發送訊息 receiver=> j [測試] / g [正式]
        receiver = 'g'
        file = 'bid_project_crop.png'
        message = "【HAA 即時拍速報】(%s)" % end_date
        # \n Line 換行符號       
        message =  "\n" + message   
        HAA_Line_Notify(receiver = receiver, path = path,msg = message, FN = file)
        
        print('Part 4 done!')
    except Exception as e:
        msg = '{0}: PART 4: 即時拍速報 Notify 失敗! The error message : {1}'.format(script, e)
        print(msg)
        sys.exit(1)
