
# coding: utf-8
import sys, os
import numpy as np

sys.path.insert(0, '/home/hadoop/jessielu/')

from Module.EmailFuncForSales import send_estimateNew_report
from Module.EmailFunc import send_error_message
from Module.ConnFunc import read_mssql, read_test_mssql
from datetime import datetime, timedelta, date
from dateutil.relativedelta import relativedelta
from openpyxl import Workbook, load_workbook
from openpyxl.utils.dataframe import dataframe_to_rows
import pandas as pd

#原本路徑
path = '/home/hadoop/jessielu/report_sales/files/'

#現在 2024/08/27
now = date.today()
#這個月的第一天 V
thisMonth_first_day = date(now.year, now.month, 1) #2024/08/01
#下個月的第一天 
nextMonth_first_day = thisMonth_first_day + relativedelta(months=1) #2024/09/01
#這個月最後一天 V
thisMonth_end_day = nextMonth_first_day - relativedelta(days=1) #2024/08/31
#今年第一天 V
thisYear_first_day = date(now.year, 1, 1) #2024/01/01
#明年第一天 
nextYear_first_day = thisYear_first_day + relativedelta(years=1) #2025/01/01
#今年最後一天 V
thisYear_end_day = nextYear_first_day - relativedelta(days=1) #2024/12/31

os.chdir(path)

sysdate = date.today().strftime("%Y-%m-%d")
fname = "abc估車日報.xlsx"

def readData():
    sql = f"""
    --新估車報表
    SELECT 
        ROW_NUMBER() OVER(ORDER BY c.MemberID ASC) AS '序號',
        c.MemberID '車商ID',
        isnull(m.CarDealerCountryName,isnull(mmc.CountryName,''))'車商區域',
        m.Name '車商名稱',
        uas.Name '負責業務',
        (case when ZoneName = 1 then '北區' 
            when ZoneName = 2 then '桃苗' 
            when ZoneName = 3 then '中區' 
            when ZoneName = 4 then '雲嘉' 
            when ZoneName = 5 then '高屏' end) '業務區域',
        Convert(varchar(11),c.ContractSDate,120) '起約日',
        Convert(varchar(11),c.ContractEDate,120) '到約日',
        --mmc.Cellphone,
        --m.Email COLLATE Chinese_Taiwan_Stroke_CI_AS Email,
        ISNULL(di.dCount,0) '當日進件',
        ISNULL(dq.dCount,0) '當日報價',
        Format(ISNULL((case when ISNULL(di.dCount,0) > 0 and ISNULL(dq.dCount,0) > 0
            then Convert(float,dq.dCount)/Convert(float,di.dCount) end),0),'P') '當日報價比率',
        ISNULL(dd.dCount,0) '當日成交',

        ISNULL(mi.mCount,0) '當月進件',
        ISNULL(mq.mCount,0) '當月報價',
        Format(ISNULL((case when ISNULL(mi.mCount,0) > 0 and ISNULL(mq.mCount,0) > 0
            then Convert(float,mq.mCount)/Convert(float,mi.mCount) end),0),'P') '當月報價比率',
        ISNULL(md.mCount,0) '當月成交',
        Format(ISNULL((case when ISNULL(md.mCount,0) > 0 and ISNULL(mi.mCount,0) > 0
            then Convert(float,md.mCount)/Convert(float,mi.mCount) end),0),'P') '當月收購成功比率',

        ISNULL(yi.yCount,0) '年度累積進件',
        ISNULL(yq.yCount,0) '年度累積報價',
        Format(ISNULL((case when ISNULL(yi.yCount,0) > 0 and ISNULL(yq.yCount,0) > 0
            then Convert(float,yq.yCount)/Convert(float,yi.yCount) end),0),'P') '年度累積報價比率',
        ISNULL(yd.yCount,0) '年度累積成交',
        Format(ISNULL((case when ISNULL(yd.yCount,0) > 0 and ISNULL(yi.yCount,0) > 0
            then Convert(float,yd.yCount)/Convert(float,yi.yCount) end),0),'P') '年度累積收購成功比率'
    FROM MemberEstimateContract c WITH(NOLOCK) --估車合約
    LEFT JOIN [Member] m WITH(NOLOCK) on m.MemberID = c.Memberid --會員資訊
    LEFT JOIN [ABCCarMemberCenter].[dbo].[Member] mmc WITH(NOLOCK) on mmc.ID = m.MemberID --會員資訊 
    LEFT JOIN [MemberUserAccountSalesEstimateMapping] mep WITH(NOLOCK) on mep.memberid = m.MemberID --會員業務對應
    LEFT JOIN UserAccountSales uas WITH(NOLOCK) on uas.ID = mep.UserAccountSalesID --業務資訊
    --當日進件
    LEFT JOIN (
        SELECT MemberID,Count(Year(r.StartDate)) dCount
        FROM MemberEstimateContract c WITH(NOLOCK)
            LEFT JOIN (
                SELECT StartDate FROM [RDSpdmaster].[ABCCarAPP].[dbo].[CarReservation] WITH(NOLOCK)
                WHERE StartDate between '{now} 00:00:00' and '{now} 23:59:59'
            ) r ON r.StartDate between c.ContractSDate and c.ContractEDate
        GROUP BY MemberID, Year(r.StartDate)
    ) di ON di.MemberID = c.MemberID
    --當日報價
    LEFT JOIN (
    SELECT c.MemberID,Count(c.MemberID) dCount
        FROM MemberEstimateContract c WITH(NOLOCK)	
        LEFT JOIN [ABCCar].[dbo].[Member] m WITH(NOLOCK) ON m.MemberID = c.MemberID	
            LEFT JOIN ( -- b.Status =1 then N'收購成功' when b.Status =-1 then N'未成功收購' else N'已報價' end) 
                SELECT b.Email,b.status,StartDate FROM [RDSpdmaster].[ABCCarAPP].[dbo].[CarReservation] a WITH(NOLOCK)
                LEFT JOIN [RDSpdmaster].[ABCCarAPP].[dbo].[CarReservationBid] b with (NOLOCK) ON a.ReserveID = b.ReserveID 
                WHERE StartDate between '{now} 00:00:00' and '{now} 23:59:59'
            ) r ON r.StartDate between c.ContractSDate and c.ContractEDate
        WHERE r.Email COLLATE Chinese_Taiwan_Stroke_CI_AS = m.Email COLLATE Chinese_Taiwan_Stroke_CI_AS
        GROUP BY c.MemberID
    ) dq ON dq.MemberID = c.MemberID
    --當日成交
    LEFT JOIN (
    SELECT c.MemberID,Count(c.MemberID) dCount
        FROM MemberEstimateContract c WITH(NOLOCK)	
        LEFT JOIN [ABCCar].[dbo].[Member] m WITH(NOLOCK) ON m.MemberID = c.MemberID	
            LEFT JOIN (
                SELECT b.Email,b.status,StartDate FROM [RDSpdmaster].[ABCCarAPP].[dbo].[CarReservation] a WITH(NOLOCK)
                LEFT JOIN [RDSpdmaster].[ABCCarAPP].[dbo].[CarReservationBid] b with (NOLOCK) ON a.ReserveID = b.ReserveID 
                WHERE StartDate between '{now} 00:00:00' and '{now} 23:59:59' and b.Status = 1
            ) r ON r.StartDate between c.ContractSDate and c.ContractEDate		
        WHERE r.Email COLLATE Chinese_Taiwan_Stroke_CI_AS = m.Email COLLATE Chinese_Taiwan_Stroke_CI_AS
        GROUP BY c.MemberID
    ) dd ON dd.MemberID = c.MemberID
    --當月進件
    LEFT JOIN (
        SELECT MemberID,Count(Year(r.StartDate)) mCount
        FROM MemberEstimateContract c WITH(NOLOCK)
            LEFT JOIN (
                SELECT StartDate FROM [RDSpdmaster].[ABCCarAPP].[dbo].[CarReservation] WITH(NOLOCK)
                WHERE StartDate between '{thisMonth_first_day} 00:00:00' and '{thisMonth_end_day} 23:59:59' 
            ) r ON r.StartDate between c.ContractSDate and c.ContractEDate
        GROUP BY MemberID, Year(r.StartDate)
    ) mi ON mi.MemberID = c.MemberID
    --當月報價
    LEFT JOIN (
    SELECT c.MemberID,Count(c.MemberID) mCount
        FROM MemberEstimateContract c WITH(NOLOCK)	
        LEFT JOIN [ABCCar].[dbo].[Member] m WITH(NOLOCK) ON m.MemberID = c.MemberID	
            LEFT JOIN ( 
                SELECT b.Email,b.status,StartDate FROM [RDSpdmaster].[ABCCarAPP].[dbo].[CarReservation] a WITH(NOLOCK)
                LEFT JOIN [RDSpdmaster].[ABCCarAPP].[dbo].[CarReservationBid] b with (NOLOCK) ON a.ReserveID = b.ReserveID 
                WHERE StartDate between '{thisMonth_first_day} 00:00:00' and '{thisMonth_end_day} 23:59:59'
            ) r ON r.StartDate between c.ContractSDate and c.ContractEDate
        WHERE r.Email COLLATE Chinese_Taiwan_Stroke_CI_AS = m.Email COLLATE Chinese_Taiwan_Stroke_CI_AS
        GROUP BY c.MemberID
    ) mq ON mq.MemberID = c.MemberID
    --當月成交
    LEFT JOIN (
    SELECT c.MemberID,Count(c.MemberID) mCount
        FROM MemberEstimateContract c WITH(NOLOCK)	
        LEFT JOIN [ABCCar].[dbo].[Member] m WITH(NOLOCK) ON m.MemberID = c.MemberID	
            LEFT JOIN (
                SELECT b.Email,b.status,StartDate FROM [RDSpdmaster].[ABCCarAPP].[dbo].[CarReservation] a WITH(NOLOCK)
                LEFT JOIN [RDSpdmaster].[ABCCarAPP].[dbo].[CarReservationBid] b with (NOLOCK) ON a.ReserveID = b.ReserveID 
                WHERE StartDate between '{thisMonth_first_day} 00:00:00' and '{thisMonth_end_day} 23:59:59' and b.Status = 1
            ) r ON r.StartDate between c.ContractSDate and c.ContractEDate		
        WHERE r.Email COLLATE Chinese_Taiwan_Stroke_CI_AS = m.Email COLLATE Chinese_Taiwan_Stroke_CI_AS
        GROUP BY c.MemberID
    ) md ON md.MemberID = c.MemberID
    --年度進件
    LEFT JOIN (
        SELECT MemberID,Count(Year(r.StartDate)) yCount
        FROM MemberEstimateContract c WITH(NOLOCK)
            LEFT JOIN (
                SELECT StartDate FROM [RDSpdmaster].[ABCCarAPP].[dbo].[CarReservation] WITH(NOLOCK)
                WHERE Year(StartDate) = '{now.year}'
            ) r ON r.StartDate between c.ContractSDate and c.ContractEDate
        GROUP BY MemberID, Year(r.StartDate)
    ) yi ON yi.MemberID = c.MemberID
    --年度報價
    LEFT JOIN (
    SELECT c.MemberID,Count(c.MemberID) yCount
        FROM MemberEstimateContract c WITH(NOLOCK)	
        LEFT JOIN [ABCCar].[dbo].[Member] m WITH(NOLOCK) ON m.MemberID = c.MemberID	
            LEFT JOIN ( 
                SELECT b.Email,b.status,StartDate FROM [RDSpdmaster].[ABCCarAPP].[dbo].[CarReservation] a WITH(NOLOCK)
                LEFT JOIN [RDSpdmaster].[ABCCarAPP].[dbo].[CarReservationBid] b with (NOLOCK) ON a.ReserveID = b.ReserveID 
                WHERE Year(StartDate) = '{now.year}'
            ) r ON r.StartDate between c.ContractSDate and c.ContractEDate		
        WHERE r.Email COLLATE Chinese_Taiwan_Stroke_CI_AS = m.Email COLLATE Chinese_Taiwan_Stroke_CI_AS
        GROUP BY c.MemberID
    ) yq ON yq.MemberID = c.MemberID
    --年度成交
    LEFT JOIN (
    SELECT c.MemberID,Count(c.MemberID) yCount
        FROM MemberEstimateContract c WITH(NOLOCK)	
        LEFT JOIN [ABCCar].[dbo].[Member] m WITH(NOLOCK) ON m.MemberID = c.MemberID	
            LEFT JOIN (
                SELECT b.Email,b.status,StartDate FROM [RDSpdmaster].[ABCCarAPP].[dbo].[CarReservation] a WITH(NOLOCK)
                LEFT JOIN [RDSpdmaster].[ABCCarAPP].[dbo].[CarReservationBid] b with (NOLOCK) ON a.ReserveID = b.ReserveID 
                WHERE Year(StartDate) = '{now.year}' and b.Status = 1
            ) r ON r.StartDate between c.ContractSDate and c.ContractEDate		
        WHERE r.Email COLLATE Chinese_Taiwan_Stroke_CI_AS = m.Email COLLATE Chinese_Taiwan_Stroke_CI_AS
        GROUP BY c.MemberID
    ) yd ON yd.MemberID = c.MemberID
    ORDER BY 1
    """
    estimateRtn = read_test_mssql(sql, 'Abccar')   
    return estimateRtn

if __name__ == '__main__':
    
    script = 'Estimation_Daily'

    #=========================== Load Data 
    try:  
        print(now)       
        msg = 'PART 1: 取得估車資料 Start'
        estimateData = readData()        
        msg = 'PART 1: 取得估車資料 OK'
        print(msg)
        
    except Exception as e:
        msg = '{0}: PART 1: 讀取資料發生錯誤! The error message : {1}'.format(script, e)
        #send_daily_report_error(msg)
        print(msg)
        sys.exit(1)
         

    try:
        #=========================== Load office file 
        msg = 'PART 2: 寫入EXCEL Start'
        wordpath= path + fname        
        wb = load_workbook(wordpath)        
        ws = wb.worksheets[0]
        ws.delete_rows(2, 100000)

        n=1
        for r in dataframe_to_rows(estimateData, index=False, header=False):
            ws.append(r)          
            n +=1

        syi = sum(estimateData['年度累積進件'])
        syq = sum(estimateData['年度累積報價'])
        syd = sum(estimateData['年度累積成交'])
        date = ws.cell(row=n+4, column=4)
        date.value = '平均每台報價次數'
        date = ws.cell(row=n+4, column=5)             
        date.value = '{:.2}'.format(syi/syq) if (syq>0) else 0
        date = ws.cell(row=n+5, column=4)
        date.value = '成交比(%)'
        date = ws.cell(row=n+5, column=5)             
        date.value = '{:.2%}'.format(syd/syi) if (syi > 0) else '{:.2%}'.format(0)

        # Save the worksheet
        wb.save(wordpath)        
        msg = 'PART 2: 寫入EXCEL OK'
        print(msg)

    except Exception as e:
        msg = '{0}: PART 2: 數據寫入出現錯誤! The error message : {1}'.format(script, e)
        #send_daily_report_error(script='{}'.format(script), message=msg)
        print(msg)
        sys.exit(1)

    try:
        send_estimateNew_report(receiver='g', path=path, FN=fname)
        msg = 'PART 3: 寄送abc估車日報 OK'
        print(msg)
        del(estimateData)

    except Exception as e:
        msg = '{0}: PART 3: 寄送abc估車日報. The error message : {1}'.format(script, e)
        send_error_message(msg)
        print(msg)
        sys.exit(1)
        
    print('All Finished!')        
