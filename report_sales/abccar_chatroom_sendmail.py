import sys, os

#正式區
sys.path.insert(0, '/home/hadoop/jessielu/')
#測試區
#sys.path.insert(0, '/home/jessie-ws/Dev/jupyterlab/daily_report/')

from Module.EmailFuncForSales import send_chatroom_report
from Module.EmailFunc import send_error_message
from Module.ConnFunc import read_mssql, read_test_mssql
from datetime import datetime, timedelta, date
from dateutil.relativedelta import relativedelta
from openpyxl import Workbook, load_workbook
from openpyxl.utils.dataframe import dataframe_to_rows

#正式區路徑
path = '/home/hadoop/jessielu/report_sales/files/'
#測試機路徑
#path = '/home/jessie-ws/Dev/jupyterlab/daily_report/report_sales/files/'

os.chdir(path)

sysdate = date.today().strftime("%Y-%m-%d")
fname = "聊聊日報.xlsx"

# 讀取資料
def readData():
    # 聊聊記錄
    sql1 = f"""
SELECT crc.RoomID
    ,cr.CarID
    ,c.memberid AS DealerID    
    ,CASE 
        WHEN mm.Category < 3 THEN ISNULL(mm.Name,'')
        ELSE ISNULL(mm.CarDealerName,mm.Name)
        END AS CarDealerName
    ,ISNULL(ms.[UserAccountSalesID],'') AS SalesID    
    ,ISNULL(u.name,'') AS SalesName
    ,RTRIM(crc.Content) as Content    
    ,ISNULL(cru.MemberID,0) MemberID
    ,CASE 
        WHEN m.Name <> '' and m.Name is not null THEN m.Name
        WHEN cru.MemberID is null THEN '　【　自動回覆　】　'
        WHEN mc.Name <> '' and  mc.Name is not null THEN  mc.Name
        ELSE ''
        END AS Name
    ,CASE 
        WHEN (cru.MemberID <> cr.[MemberID] or ISNULL(cru.MemberID,0)=0)
            THEN '賣'
        ELSE '買'
        END AS [type]
    ,crc.CreateDate
FROM [ABCCar].[dbo].[ChatRoom] cr WITH (NOLOCK) -- 
FULL OUTER JOIN [ABCCar].[dbo].[ChatRoomContent] crc WITH (NOLOCK) ON crc.RoomID = cr.id  --聊天室內容
LEFT JOIN ChatRoomUser cru WITH (NOLOCK) ON cru.RoomID = crc.RoomID AND cru.ID = crc.RoomUserID --聊天室成員    
LEFT JOIN [ABCCarMemberCenter].[dbo].[Member] mc WITH (NOLOCK) ON cru.MemberID = mc.ID --聊天室成員名稱顯示1
LEFT JOIN [ABCCar].[dbo].[Member] m WITH (NOLOCK) ON cru.MemberID = m.MemberID --聊天室成員名稱顯示2 (若1無2;若2無則預設)
LEFT JOIN [ABCCar].[dbo].car c WITH (NOLOCK) ON c.CarID = cr.CarID  --物件所屬人
LEFT JOIN [ABCCar].[dbo].[Member] mm WITH (NOLOCK) ON c.MemberID = mm.MemberID --物件所屬人名稱
LEFT JOIN [ABCCar].[dbo].[MemberUserAccountSalesMapping] ms WITH (NOLOCK) ON ms.MemberID = c.MemberID --物件所屬業務
LEFT JOIN [ABCCar].[dbo].[UserAccountSales] u WITH (NOLOCK) ON u.ID = ms.UserAccountSalesID and u.Type='2' and u.Status='1' --物件所屬業務名稱
--WHERE RoomUserID <> 0 
WHERE crc.CreateDate between DATEADD(dd, -1,DATEDIFF(dd, 0, GETDATE())) and DATEADD(dd, 0,DATEDIFF(dd, 0, GETDATE())) 
ORDER BY crc.RoomID    ,crc.CreateDate

    """
    


    sql2 = f"""
SELECT [聊天室ID],[物件ID],[車商ID],[車商名稱],[業務ID],[業務],[留言內容],[會員ID],[會員名稱],[哪方留言],[留言順序],[聊天室建立月份],[留言日期],[待車商回覆]
 FROM (
SELECT crc.RoomID as [聊天室ID]
    ,ROW_NUMBER() OVER ( PARTITION BY crc.RoomID ORDER BY crc.CreateDate DESC ) AS [LastRow]
	,ROW_NUMBER() OVER ( PARTITION BY crc.RoomID ORDER BY crc.CreateDate asc ) AS [留言順序]
    ,cr.CarID as [物件ID]
    ,c.memberid as [車商ID] 
    ,CASE 
        WHEN mm.Category < 3 THEN ISNULL(mm.Name,'')
        ELSE ISNULL(mm.CarDealerName,mm.Name)
        END AS [車商名稱] 
    ,ISNULL(ms.[UserAccountSalesID],'') AS [業務ID]     
    ,ISNULL(u.name,'') AS  [業務]     
    ,RTRIM(crc.Content) as  [留言內容]     
    ,ISNULL(cru.MemberID,0) as [會員ID] 
    ,CASE 
        WHEN m.Name <> '' and m.Name is not null THEN m.Name
        WHEN cru.MemberID is null THEN '　【　自動回覆　】　'
        WHEN mc.Name <> '' and  mc.Name is not null THEN  mc.Name
        ELSE ''
        END AS [會員名稱] 

    ,CASE 
        WHEN (cru.MemberID <> cr.[MemberID] or ISNULL(cru.MemberID,0)=0)
            THEN '賣'
        ELSE '買'
        END AS [哪方留言] 
    ,convert(varchar(7),cr.CreateDate,120) as [聊天室建立月份]	
    ,crc.CreateDate AS [留言日期] 
	,CASE WHEN MAX(crc.CreateDate) OVER ( PARTITION BY crc.RoomID ) = crc.CreateDate AND (cru.MemberID = cr.[MemberID] or (ISNULL(cru.MemberID,0) = 0 and CHARINDEX('通知賣方', RTRIM(crc.Content)) > 0)) THEN '＊' ELSE '' END as [待車商回覆] 
FROM [ABCCar].[dbo].[ChatRoom] cr WITH (NOLOCK) -- 
FULL OUTER JOIN [ABCCar].[dbo].[ChatRoomContent] crc WITH (NOLOCK) ON crc.RoomID = cr.id  --聊天室內容
LEFT JOIN ChatRoomUser cru WITH (NOLOCK) ON cru.RoomID = crc.RoomID AND cru.ID = crc.RoomUserID --聊天室成員    
LEFT JOIN [ABCCarMemberCenter].[dbo].[Member] mc WITH (NOLOCK) ON cru.MemberID = mc.ID --聊天室成員名稱顯示1
LEFT JOIN [ABCCar].[dbo].[Member] m WITH (NOLOCK) ON cru.MemberID = m.MemberID --聊天室成員名稱顯示2 (若1無2;若2無則預設)
LEFT JOIN [ABCCar].[dbo].car c WITH (NOLOCK) ON c.CarID = cr.CarID  --物件所屬人
LEFT JOIN [ABCCar].[dbo].[Member] mm WITH (NOLOCK) ON c.MemberID = mm.MemberID --物件所屬人名稱
LEFT JOIN [ABCCar].[dbo].[MemberUserAccountSalesMapping] ms WITH (NOLOCK) ON ms.MemberID = c.MemberID --物件所屬業務
LEFT JOIN [ABCCar].[dbo].[UserAccountSales] u WITH (NOLOCK) ON u.ID = ms.UserAccountSalesID and u.Type='2' and u.Status='1' --物件所屬業務名稱
WHERE c.Status=1 and getdate() between StartDate and EndDate 
and cr.CreateDate >'2023-09-01 00:00:00' 
) MA 
--WHERE [LastRow]=1
ORDER BY [聊天室ID] , [留言日期] 
    """

    sql3 = f"""
SELECT [車商ID],[車商名稱],[業務ID],[業務],[待車商回覆],count([聊天室ID]) CountRow into #temp 
 FROM (
SELECT crc.RoomID as [聊天室ID]
    ,ROW_NUMBER() OVER ( PARTITION BY crc.RoomID ORDER BY crc.CreateDate DESC ) AS [LastRow]
	,ROW_NUMBER() OVER ( PARTITION BY crc.RoomID ORDER BY crc.CreateDate asc ) AS [留言順序]
    ,cr.CarID as [物件ID]
    ,c.memberid as [車商ID] 
    ,CASE 
        WHEN mm.Category < 3 THEN ISNULL(mm.Name,'')
        ELSE ISNULL(mm.CarDealerName,mm.Name)
        END AS [車商名稱] 
    ,ISNULL(ms.[UserAccountSalesID],'') AS [業務ID]     
    ,ISNULL(u.name,'') AS  [業務]     
    ,RTRIM(crc.Content) as  [留言內容]     
    ,ISNULL(cru.MemberID,0) as [會員ID] 
    ,CASE 
        WHEN m.Name <> '' and m.Name is not null THEN m.Name
        WHEN cru.MemberID is null THEN '　【　自動回覆　】　'
        WHEN mc.Name <> '' and  mc.Name is not null THEN  mc.Name
        ELSE ''
        END AS [會員名稱] 

    ,CASE 
        WHEN (cru.MemberID <> cr.[MemberID] or ISNULL(cru.MemberID,0)=0)
            THEN '賣'
        ELSE '買'
        END AS [哪方留言] 
    ,convert(varchar(7),cr.CreateDate,120) as [聊天室建立月份]	
    ,crc.CreateDate AS [留言日期] 
	,CASE WHEN MAX(crc.CreateDate) OVER ( PARTITION BY crc.RoomID ) = crc.CreateDate AND (cru.MemberID = cr.[MemberID] or (ISNULL(cru.MemberID,0) = 0 and CHARINDEX('通知賣方', RTRIM(crc.Content)) > 0)) THEN '＊' ELSE '' END as [待車商回覆] 
FROM [ABCCar].[dbo].[ChatRoom] cr WITH (NOLOCK) -- 
FULL OUTER JOIN [ABCCar].[dbo].[ChatRoomContent] crc WITH (NOLOCK) ON crc.RoomID = cr.id  --聊天室內容
LEFT JOIN ChatRoomUser cru WITH (NOLOCK) ON cru.RoomID = crc.RoomID AND cru.ID = crc.RoomUserID --聊天室成員    
LEFT JOIN [ABCCarMemberCenter].[dbo].[Member] mc WITH (NOLOCK) ON cru.MemberID = mc.ID --聊天室成員名稱顯示1
LEFT JOIN [ABCCar].[dbo].[Member] m WITH (NOLOCK) ON cru.MemberID = m.MemberID --聊天室成員名稱顯示2 (若1無2;若2無則預設)
LEFT JOIN [ABCCar].[dbo].car c WITH (NOLOCK) ON c.CarID = cr.CarID  --物件所屬人
LEFT JOIN [ABCCar].[dbo].[Member] mm WITH (NOLOCK) ON c.MemberID = mm.MemberID --物件所屬人名稱
LEFT JOIN [ABCCar].[dbo].[MemberUserAccountSalesMapping] ms WITH (NOLOCK) ON ms.MemberID = c.MemberID --物件所屬業務
LEFT JOIN [ABCCar].[dbo].[UserAccountSales] u WITH (NOLOCK) ON u.ID = ms.UserAccountSalesID and u.Type='2' and u.Status='1' --物件所屬業務名稱
WHERE c.Status=1 and getdate() between StartDate and EndDate
and cr.CreateDate >'2023-09-01 00:00:00' 
) MA 
WHERE [LastRow]=1
GROUP BY [車商ID],[車商名稱],[業務ID],[業務],[待車商回覆]
ORDER BY [車商ID],[待車商回覆];

SELECT a.車商ID,a.車商名稱,a.業務,a.[聊天室數量],--ISNULL((SELECT SUM(b.CountRow) FROM #temp b WHERE a.[車商ID]=b.[車商ID] and b.[待車商回覆]='＊'),0) as [待車商回覆],
--ISNULL((SELECT SUM(b.CountRow) FROM #temp b WHERE a.[車商ID]=b.[車商ID] and b.[待車商回覆]='＊'),0)/(a.[聊天室數量]*1.0) as  [未回覆率],
Convert(varchar(10),Convert(float,Round(ISNULL((SELECT SUM(b.CountRow) FROM #temp b WHERE a.[車商ID]=b.[車商ID] and b.[待車商回覆]='＊'),0)/(a.[聊天室數量]*1.0),3))*100) +'%' as  [未回覆率],
(CASE WHEN ISNULL(p.LineID,'')!='' THEN 'V' ELSE '' END) as [LINE],
(CASE WHEN ISNULL(p.LineNotifyToken,'')!='' THEN 'V' ELSE '' END) as [TOKEN] 
FROM (SELECT [車商ID],[車商名稱],[業務ID],[業務],SUM(CountRow) [聊天室數量] FROM #temp GROUP BY [車商ID],[車商名稱],[業務ID],[業務]) a 
LEFT JOIN (
SELECT [MemberID],[LineID],[LineNotifyToken] --,[displayName],[status],[UpdateDate],[CreateDate]
  FROM ChatRoomUserMapping c with(nolock)
  WHERE c.MemberID>0 AND c.status='1'
  AND c.ID = (SELECT MAX(k.ID) FROM ChatRoomUserMapping k with(nolock) where c.MemberID=k.MemberID)
) p on a.[車商ID]= p.MemberID

    """

    RoomRtnList1 = read_test_mssql(sql1, 'Abccar')
    RoomRtnList2 = read_test_mssql(sql2, 'Abccar')
    RoomRtnList3 = read_test_mssql(sql3, 'Abccar')

    return RoomRtnList1, RoomRtnList2, RoomRtnList3


if __name__ == '__main__':
    print(sysdate)
    # 設定日期
    script = 'abccar_chatroom_sendmail'

    try:
        rtnRoomDF1, rtnRoomDF2, rtnRoomDF3 = readData()
        msg = 'PART 1: 取得聊聊資料 OK'
        print(msg)
    except Exception as e:
        msg = 'PART 1: 取得聊聊資料失敗! The error message : {}'.format(e)
        send_error_message(script=script, message=msg)
        print(msg)
        sys.exit(1)
        
    try:
        wordpath= path + fname
        wb = load_workbook(wordpath)
        #清空表格
        ws = wb.worksheets[0]
        ws.delete_rows(2, 100000)

        n=1
        for r in dataframe_to_rows(rtnRoomDF1, index=False, header=False):
            m = 1
            for i in r:
                grid = ws.cell(row=1+n, column=m)
                grid.value = i
                m+=1
            n +=1


        #清空表格
        ws = wb.worksheets[1]
        ws.delete_rows(2, 100000)

        n=1
        for r in dataframe_to_rows(rtnRoomDF2, index=False, header=False):
            m = 1
            for i in r:
                grid = ws.cell(row=1+n, column=m)
                grid.value = i
                m+=1
            n +=1


        #清空表格
        ws = wb.worksheets[2]
        ws.delete_rows(2, 100000)

        n=1
        for r in dataframe_to_rows(rtnRoomDF3, index=False, header=False):
            m = 1
            for i in r:
                grid = ws.cell(row=1+n, column=m)
                grid.value = i
                m+=1
            n +=1


        # Save the worksheet
        wb.save(wordpath)
        
        msg = 'PART 2: 寫入EXCEL OK'
        print(msg)
    except Exception as e:
        msg = 'PART 2: 寫入EXCEL失敗! The error message : {}'.format(e)
        send_error_message(script=script, message=msg)
        print(msg)
        sys.exit(1)
        
    try:
        send_chatroom_report(receiver='g', path=path, FN=fname)
        msg = 'PART 3: 寄送聊聊日報 OK'
        print(msg)
    except Exception as e:
        msg = '{0}: PART 3: 寄送聊聊日報. The error message : {1}'.format(script, e)
        send_error_message(msg)
        print(msg)
        sys.exit(1)
        
    print('PART 3: All Finished!') 
        