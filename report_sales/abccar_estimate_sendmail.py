
# coding: utf-8
import sys, os
import numpy as np

#正式區
sys.path.insert(0, '/home/hadoop/jessielu/')
#測試區
#sys.path.insert(0, '/home/jessie-ws/Dev/jupyterlab/daily_report/')


from Module.EmailFuncForSales import send_estimate_report
from Module.EmailFunc import send_error_message
from Module.ConnFunc import read_mssql, read_test_mssql
from datetime import datetime, timedelta, date
from dateutil.relativedelta import relativedelta
from openpyxl import Workbook, load_workbook
from openpyxl.utils.dataframe import dataframe_to_rows


#原本路徑
path = '/home/hadoop/jessielu/report_sales/files/'

#測試機路徑
#path = '/home/jessie-ws/Dev/jupyterlab/daily_report/report_sales/files/'

os.chdir(path)

sysdate = date.today().strftime("%Y-%m-%d")
fname = "估車日報.xlsx"

def readData():
    sql = f"""
SELECT b.BidID '報價編號', a.ReserveID '估車編號',a.Name '車主姓名',(Case when a.Gender='M' then N'男' else N'女' end) as '車主性別',
k.Name '車輛品牌',SeriesName '車系',CategoryName '車款',l.Name '車輛所在地',Area '地區',convert(varchar(7),ManufactureDate,120) '出廠年月',
PlateNo '車牌號碼',Mileage '里程數', m.Name '報價車商',ISNULL(t.Name,'') '業務',convert(varchar,b.BidDate,120) '報價時間',
(Case when b.Status =1 then N'收購成功' when b.Status =-1 then N'未成功收購' else N'已報價' end) '交易狀態' 
  FROM [RDSpdmaster].[ABCCarAPP].[dbo].[CarReservationBid]  b with (NOLOCK) 
  JOIN [RDSpdmaster].[ABCCarAPP].[dbo].[CarReservation] a  with (NOLOCK) ON a.ReserveID=b.ReserveID 
  LEFT JOIN [ABCCar].[dbo].[Brand] k with (NOLOCK)  ON  a.BrandID=k.BrandID
  LEFT JOIN [ABCCar].[dbo].[KeyValueDetail] l with (NOLOCK) ON l.KeyName='Location' AND a.Location=l.DetailID
  LEFT JOIN [ABCCar].[dbo].[Member] m with (NOLOCK) ON b.Email COLLATE Chinese_Taiwan_Stroke_CI_AS = m.Email COLLATE Chinese_Taiwan_Stroke_CI_AS 
  LEFT JOIN [dbo].[MemberUserAccountSalesMapping] ma with(nolock) on m.MemberID=ma.MemberID
  LEFT JOIN [dbo].[UserAccountSales] t with(nolock) on ma.UserAccountSalesID=t.ID
--where a.PlateNo='1689-GN'
WHERE a.StartDate>'2023-06-01 00:00:00' and b.BidDate < convert(varchar(10), getdate(),120) + ' 00:00:00'
ORDER BY a.ReserveID DESC,b.BidID
    """
    
    estimateRtn = read_test_mssql(sql, 'Abccar')

    return estimateRtn

if __name__ == '__main__':
    
    script = 'Estimation_Daily'

    try:
        estimateDF = readData()
        msg = 'PART 1: 取得估車資料 OK'
        print(msg)
        
    except Exception as e:
        msg = '{0}: PART 1: 讀取資料發生錯誤! The error message : {1}'.format(script, e)
        #send_daily_report_error(msg)
        print(msg)
        sys.exit(1)
         

    try:
        #=========================== Load office file 
        wordpath= path + fname
        
        wb = load_workbook(wordpath)
        
        ws = wb.worksheets[0]
        ws.delete_rows(2, 100000)

        n=1
        for r in dataframe_to_rows(estimateDF, index=False, header=False):
            m = 1
            for i in r:
                grid = ws.cell(row=1+n, column=m)
                grid.value = i
                m+=1
            n +=1

        # Save the worksheet
        wb.save(wordpath)
        
        msg = 'PART 2: 寫入EXCEL OK'
        print(msg)
    except Exception as e:
        msg = '{0}: PART 2: 數據寫入出現錯誤! The error message : {1}'.format(script, e)
        #send_daily_report_error(script='{}'.format(script), message=msg)
        print(msg)
        sys.exit(1)

    try:
        send_estimate_report(receiver='g', path=path, FN=fname)
        msg = 'PART 3: 寄送估車日報 OK'
        print(msg)
        del(estimateDF)
    except Exception as e:
        msg = '{0}: PART 3: 寄送估車日報. The error message : {1}'.format(script, e)
        send_error_message(msg)
        print(msg)
        sys.exit(1)
        
    print('PART 3: All Finished!')        
