
#==============
# Path: /home/hadoop/jessielu/report_sales
# Description: Get google sheet transform format to pdf and cut image. Send notify to Line.
# Date: 2022/12/29
# Author: Sam 
#==============

# coding: utf-8

import sys, os, importlib
# 正式機
sys.path.insert(0, '/home/hadoop/jessielu/')

# 測試機
#sys.path.insert(0, '/home/jessie-ws/Dev/jupyterlab/daily_report/')

from openpyxl import Workbook, load_workbook

import pandas as pd
from datetime import datetime, timedelta, date
from dateutil.relativedelta import relativedelta  
#import math
#import shutil
import subprocess

from pdf2image import convert_from_path
from PIL import Image

# 重新載入更新後 Module
#importlib.reload(sys.modules['Module.NotifyFunc'])

from Module.NotifyFunc import HOT_Line_Notify

#系統日
now = date.today()
#系統日前一日
end_date = (now - timedelta(1)).strftime("%Y-%m-%d")
#正式區路徑
path = '/home/hadoop/jessielu/report_sales/files/'
#測試機路徑
#path = '/home/jessie-ws/Dev/jupyterlab/daily_report/report_sales/files/'

os.chdir(path)

# 移除前次產出圖檔
imgs = ['hot_project.png','hot_project_crop.png']
for r in imgs:
    if os.path.isfile(r):
        os.remove(r)
        
def doc2pdf_linux(docPath, pdfPath):
    cmd = 'libreoffice --headless --convert-to pdf'.split()+[docPath] + ['--outdir'] + [pdfPath]
    p = subprocess.Popen(cmd, stderr=subprocess.PIPE, stdout=subprocess.PIPE,bufsize=1)
    p.wait(timeout=30)
    # stdout, stderr = p.communicate()
    # print(stdout, stderr)
    p.communicate()

def doc2pdf(docPath, pdfPath):
    docPathTrue = os.path.abspath(docPath)  
    return doc2pdf_linux(docPathTrue, pdfPath)

def mergeData():

    url = "https://docs.google.com/spreadsheets/d/e/2PACX-1vRqJViM2wQA0y5MDHuPFLzj3FyzHq5vmku6ScvB-w4O9PgMuDUAxN_ufwnBhWsD_HnxE4XhWZiCw8xH/pubhtml?gid=0&single=true&v=1"
    # Assign the table data to a Pandas dataframe
    google_table = pd.read_html(url, encoding = 'utf8',)[0].fillna(0)
    #print(google_table)
    google_table.columns = ['idx', 'title', 'targets', 'nums']
    #print(google_table)
    google_table['targets'] = pd.to_numeric(google_table['targets'], errors='coerce', downcast="integer")
    google_table['nums'] = pd.to_numeric(google_table['nums'], errors='coerce', downcast="integer")
    
    google_table = google_table.drop(['idx'],axis=1)
    #print(google_table)
    
    tmpTitle = google_table['title']
    #print(tmpTitle)
    tmpA1 = google_table.loc[(google_table['title'] == 'HOT好車加盟數') , :]
    tmpA2 = google_table.loc[(google_table['title'] == '認證數') , :]
    tmpA3 = google_table.loc[(google_table['title'] == '保固通過數') , :]
    
    tmpB1 = google_table.loc[(google_table['title'] == '直營廠家數') , :]
    tmpB2 = google_table.loc[(google_table['title'] == '直營廠總營收') , :]
    tmpB3 = google_table.loc[(google_table['title'] == '直營廠外部營收') , :]
    tmpB4 = google_table.loc[(google_table['title'] == 'HOT保修加盟數') , :]
    tmpB5 = google_table.loc[(google_table['title'] == '保修加盟訂購業績') , :]
    tmpB6 = google_table.loc[(google_table['title'] == '原廠零件外販業績') , :]
    tmpB7 = google_table.loc[(google_table['title'] == '非原廠零件外販業績') , :]
    
    ReportSendDate = google_table.loc[(google_table['title'] == '發送日') , :]
    
    #測試機路徑
    wordpath= path + 'HOT週報.xlsx'
    #=========================== Load office file      
    wb = load_workbook(wordpath)

    #Sheet 1
    ws_main = wb.worksheets[0]

    # Add 日期
    data = ws_main.cell(row=1, column=2)
    data.value = tmpTitle[0]
    
    # Add HOT好車加盟數_目標
    data = ws_main.cell(row=3, column=2)
    data.value = int(float(tmpA1['targets'].to_string(index=False)))
    # Add HOT好車加盟數_實績
    data = ws_main.cell(row=3, column=3)
    data.value = int(float(tmpA1['nums'].to_string(index=False)))
    
    # Add 認證數_目標
    data = ws_main.cell(row=4, column=2)
    data.value = int(float(tmpA2['targets'].to_string(index=False)))
    # Add 認證數_實績
    data = ws_main.cell(row=4, column=3)
    data.value = int(float(tmpA2['nums'].to_string(index=False)))
    
    # Add 保固通過數_目標
    data = ws_main.cell(row=5, column=2)
    data.value = int(float(tmpA3['targets'].to_string(index=False)))
    # Add 保固通過數_實績
    data = ws_main.cell(row=5, column=3)
    data.value = int(float(tmpA3['nums'].to_string(index=False)))
    

    # Add 日期
    data = ws_main.cell(row=7, column=2)
    data.value = tmpTitle[0]
    
    # Add 直營廠家數_目標
    data = ws_main.cell(row=9, column=2)
    data.value = int(float(tmpB1['targets'].to_string(index=False)))
    # Add 直營廠家數_實績
    data = ws_main.cell(row=9, column=3)
    data.value = int(float(tmpB1['nums'].to_string(index=False)))
    
    # Add 直營廠總營收_目標
    data = ws_main.cell(row=10, column=2)
    data.value = int(float(tmpB2['targets'].to_string(index=False)))
    # Add 直營廠總營收_實績
    data = ws_main.cell(row=10, column=3)
    data.value = int(float(tmpB2['nums'].to_string(index=False)))
    
    # Add 直營廠外部營收_目標
    data = ws_main.cell(row=11, column=2)
    data.value = int(float(tmpB3['targets'].to_string(index=False)))
    # Add 直營廠外部營收_實績
    data = ws_main.cell(row=11, column=3)
    data.value = int(float(tmpB3['nums'].to_string(index=False)))
    
    # Add HOT保修加盟數_目標
    data = ws_main.cell(row=12, column=2)
    data.value = int(float(tmpB4['targets'].to_string(index=False)))
    # Add HOT保修加盟數_實績
    data = ws_main.cell(row=12, column=3)
    data.value = int(float(tmpB4['nums'].to_string(index=False)))
    
    # Add 保修加盟訂購業績_目標
    data = ws_main.cell(row=13, column=2)
    data.value = int(float(tmpB5['targets'].to_string(index=False)))
    # Add 保修加盟訂購業績_實績
    data = ws_main.cell(row=13, column=3)
    data.value = int(float(tmpB5['nums'].to_string(index=False)))
    
    # Add 原廠零件外販業績_目標
    data = ws_main.cell(row=14, column=2)
    data.value = int(float(tmpB6['targets'].to_string(index=False)))
    # Add 原廠零件外販業績_實績
    data = ws_main.cell(row=14, column=3)
    data.value = int(float(tmpB6['nums'].to_string(index=False)))
    
    # Add 非原廠零件外販業績_目標
    data = ws_main.cell(row=15, column=2)
    data.value = int(float(tmpB7['targets'].to_string(index=False)))
    # Add 非原廠零件外販業績_實績
    data = ws_main.cell(row=15, column=3)
    data.value = int(float(tmpB7['nums'].to_string(index=False)))
    
    wb.save(wordpath)
    
    #Excel 轉 PDF
    pdfPath = path
    doc2pdf(wordpath, pdfPath)   

    return ReportSendDate

if __name__ == "__main__":
    
    
    print(now.strftime("%Y-%m-%d"))
    # 設定日期
    script = 'send_hot_notify_project'

    SendDate1 = now.strftime("%Y%m%d")
    SendDate2 = now.strftime("%Y%-m%-d")
    
    try:
        # 讀取資料寫入EXCEL轉PDF
        DF_Send = mergeData()
        # 取得設定發送日
        google_send_date=str(int(float(DF_Send['targets'].to_string(index=False))))
        
        if (google_send_date==SendDate1 or google_send_date==SendDate2):
            print('Part 1 done!')
        else:
            sys.exit(1)
            
    except Exception as e:
        msg = '{0}: PART 1: 填入HOT週報值失敗! The error message : {1}'.format(script, e)
        print(msg)
        sys.exit(1)

    try:
        # PDF轉圖檔
        images = convert_from_path(path + 'HOT週報.pdf')
        images[0].save(path + 'hot_project.png')
        # 裁圖另存
        img = Image.open(path + 'hot_project.png')
        
        # img.crop(左，上，右，下)
        # 正式區
        im1 = img.crop((100, 140, 1390, 860))
        
        # 測試區
        #im1 = img.crop((100, 140, 1220, 860))

        im1.save(path + 'hot_project_crop.png', quality=50) 
        
        print('Part 2 done!')
    except Exception as e:
        msg = '{0}: PART 2: HOT週報轉檔失敗! The error message : {1}'.format(script, e)
        print(msg)
        sys.exit(1)

    try:
 
        # 發送訊息 receiver=> j [測試] / g [正式]
        receiver = 'g'
        file = 'hot_project_crop.png'
        message = "【HOT週報】(%s)" % end_date
        # \n Line 換行符號       
        message =  "\n" + message   
        HOT_Line_Notify(receiver = receiver, path = path,msg = message, FN = file)
        
        print('Part 3 done!')
    except Exception as e:
        msg = '{0}: PART 3: HOT週報 Notify 失敗! The error message : {1}'.format(script, e)
        print(msg)
        sys.exit(1)