import sys, os

#正式區
sys.path.insert(0, '/home/hadoop/jessielu/')
#測試區
#sys.path.insert(0, '/home/jessie-ws/Dev/jupyterlab/daily_report/')

from Module.EmailFuncForSales import send_safty_report
from Module.EmailFunc import send_error_message
from Module.ConnFunc import read_mssql, read_test_mssql
from datetime import datetime, timedelta, date
from dateutil.relativedelta import relativedelta
from openpyxl import Workbook, load_workbook
from openpyxl.utils.dataframe import dataframe_to_rows

#正式區路徑
path = '/home/hadoop/jessielu/report_sales/files/'
#測試機路徑
#path = '/home/jessie-ws/Dev/jupyterlab/daily_report/report_sales/files/'

os.chdir(path)

sysdate = date.today().strftime("%Y-%m-%d")
fname = "安心購.xlsx"

# 讀取資料
def readData(CID):
    sql = f"""
        SELECT MemberID as '會員ID',count(*) as '台數'
        FROM CampaignItem
        WHERE (CampaignID = {CID}) AND (Status = 1)
        GROUP BY MemberID """
        
    sql2 = f"""
        SELECT a.MemberID, b.CarID, b.CreateDate, b.StartDate, c.ChangeIssueDate as '行照過戶日', c.CreatedOn as '系統過戶驗證日', b.TransferCheckId 
        FROM CampaignItem a, Car b left join CarTransferCheckLog c on b.TransferCheckId=c.Id
        WHERE a.CampaignID = {CID} and a.Status = 1 and a.CarID=b.CarID
        ORDER BY 1,2
        """
        
    carList = read_mssql(sql, 'ABCCar')
    carDetail = read_mssql(sql2, 'ABCCar')
    
    return carList, carDetail

def mergeData():
    #移除
    if os.path.isfile(fname):
        os.remove(fname)
          
    wb = Workbook()    
    ws1 = wb.worksheets[0]
    ws1.title='會員申請安心購'
    for r in dataframe_to_rows(carDF, index=False, header=True):
        ws1.append(r)

    ws2 = wb.create_sheet("安心購物件", 1)
    for r in dataframe_to_rows(carDFList, index=False, header=True):
        ws2.append(r)
 
    wb.save(fname)
    
if __name__ == '__main__':
    print(sysdate)
    # 設定日期
    script = 'abccar_project_safty_sendmail'

    try:
        carDF, carDFList = readData(902)
        msg = 'PART 1: 取得安心購資料 OK'
        print(msg)
    except Exception as e:
        msg = 'PART 1: 取得安心購資料失敗! The error message : {}'.format(e)
        send_error_message(script=script, message=msg)
        print(msg)
        sys.exit(1)
        
    try:
        mergeData()
        msg = 'PART 2: 寫入EXCEL OK'
        print(msg)
    except Exception as e:
        msg = 'PART 2: 寫入EXCEL失敗! The error message : {}'.format(e)
        send_error_message(script=script, message=msg)
        print(msg)
        sys.exit(1)
        
    try:
        send_safty_report(receiver='g', path=path, FN=fname)
        msg = 'PART 3: 寄送安心購 OK'
        print(msg)
    except Exception as e:
        msg = '{0}: PART 3: 寄送安心購. The error message : {1}'.format(script, e)
        send_error_message(msg)
        print(msg)
        sys.exit(1)
        
    print('PART 3: All Finished!') 
        