# coding: utf-8
import sys
import os

# sys.path.insert(0, '/home/jessie/MyNotebook/daily_report/')
sys.path.insert(0, '/home/hadoop/jessielu/')

from Module.EmailFuncForSales import send_daily_report_memberAgree, send_daily_report_error, send_error_message
from Module.ConnFunc import read_mssql, read_test_mssql
from Module.HelpFunc import display_set

from openpyxl.styles import Border, Side, PatternFill, Font, GradientFill, Alignment
from openpyxl import Workbook, load_workbook
from openpyxl.utils.dataframe import dataframe_to_rows

import pandas as pd
from datetime import datetime, timedelta, date
from dateutil.relativedelta import relativedelta  
import math

now = date.today() + relativedelta(days=1)
# now = date.today()

#今年第一天
thisYear_first_day = date(now.year , 1, 1)

#每月一號的的計算邏輯要特別調整
if(now.day == 1):
    tmpDay = now - relativedelta(days=1)
    first_day = date(tmpDay.year, tmpDay.month, 1)
    tmpMonth = tmpDay - relativedelta(months=1)
    lastMonth_first_day = date(tmpMonth.year, tmpMonth.month, 1)
    dl = tmpDay - relativedelta(months=2)
    lfirst_day = date(dl.year, dl.month, 1)
    lastMonth_end_day = date(first_day.year, first_day.month, 1) - relativedelta(days=1)
else:
    #上個月的今天
    d = now - relativedelta(months=1)
    #這個月的第一天
    first_day = date(now.year, now.month, 1)
    #上個月的第一天
    lastMonth_first_day = date(d.year, d.month, 1)
    #上上個月的今天
    dl = now - relativedelta(months=2)
    #上上個月的第一天
    lfirst_day = date(dl.year, dl.month, 1)
    #上個月的最後一天
    lastMonth_end_day = date(first_day.year, first_day.month, 1) - relativedelta(days=1)
    
# 建立日期區間
# start_date = (datetime.today() - timedelta(2)).strftime("%Y-%m-%d") #前一天
start_date = lastMonth_first_day #上個月的第一天
end_date = (now - timedelta(1)).strftime("%Y-%m-%d") #前一天

display_set(200, 30)
path = '/home/hadoop/jessielu/report_sales/files/'
# path = '/home/jessie/MyNotebook/easyRent/scripts/'

os.chdir(path)

# 讀取數據
def ReadData():

    memberAgree_sg = f"""
        SELECT 
        m.MemberID
        ,ISNULL(mc.[Name], '') as [會員名稱]
        ,ISNULL(m.[CarDealerName], '') as [車商名稱]
        --,ISNULL(CONCAT(mc.[CountryName], mc.[DistrictName]), '') AS [城市別]
        ,CASE WHEN (m.[CarDealerCountryName] is null or m.[CarDealerCountryName] = '') THEN CONCAT(mc.[CountryName], mc.[DistrictName]) ELSE CONCAT(m.[CarDealerCountryName], m.[CarDealerDistrictName]) END as [城市別]        
        ,ISNULL(m.[ContactPerson], '') AS [連絡人]
        ,ISNULL(mc.[CellPhone], '') AS [連絡電話]
        ,ISNULL(m.[TaxID], '') AS [統編]
        ,ISNULL(m.[HOTMemberID], '') AS [HOT會員編號]
        ,(CASE(m.[Category]) 
            WHEN 1 THEN '一般買家' 
            WHEN 2 THEN '一般賣家' 
            WHEN 3 THEN '車商' 
            WHEN 4 THEN '原廠' 
            ELSE '買賣顧問' 
        END ) AS [身分別]
        ,IIF(m.[VIPLevel] = 0 , '', '是') AS [網路好店]
        ,CONVERT(varchar(100), mc.[CreateDate], 120) AS [註冊時間]
        ,ISNULL((SELECT TOP 1 [Salesperson] FROM SOOrder WITH (NOLOCK) WHERE [MemberID] = m.[MemberID] order by Id), '') AS 車格維護業務人員
        ,ISNULL((SELECT TOP 1 IIF(a1.[UserAccountSalesID] < 60, '', a2.[Name]) FROM [MemberAgree] a1 WITH (NOLOCK) LEFT JOIN [UserAccountSales] a2 WITH (NOLOCK) on a1.[UserAccountSalesID] = a2.[ID] where a1.AgreeID = 1 AND a1.MemberId = m.[MemberID]), '') AS [會員同意條款維護業務人員]
        ,ISNULL((SELECT TOP 1 IIF(a1.[UserAccountSalesID] < 60, '', a2.[ID]) FROM ABCCar.dbo.[MemberAgree] a1 WITH (NOLOCK) LEFT JOIN ABCCar.dbo.[UserAccountSales] a2 WITH (NOLOCK) on a1.[UserAccountSalesID] = a2.[ID] where a1.AgreeID = 1 AND a1.MemberId = m.[MemberID]), '') AS [會員同意條款維護業務人員ID]
        ,ma.CreateDate as [同意會員條款時間]
        ,bm.BindingDate as [信用卡綁定時間]
        ,CASE WHEN ISNULL(ph.[ID], 0) > 0 THEN ph.CreateDate ELSE NULL END AS [最新儲值時間]
        ,(SELECT COUNT(1) FROM [ABCCar].[dbo].[GridTrans] WITH (NOLOCK) WHERE m.[MemberID] = [MemberID] AND [GridTranTypeID] = 202 AND [CreateDate] >= '2020-11-09 11:00:00' AND [GridCount] = 0) as [同意條款及綁卡後上架數]
        ,ISNULL((SELECT COUNT(1) FROM [Car] WITH (NOLOCK) WHERE [MemberID] = m.MemberID AND [Status] = 1 and GetDate() Between [Car].[StartDate] And [Car].[EndDate]), 0) as [上架車輛數]
        ,ISNULL((SELECT COUNT(1) FROM [Car] WITH (NOLOCK) WHERE [EndDate] >= '{end_date} 23:59:59' AND [Status] = 1 and [MemberID] = m.MemberID AND [VehicleNo] is not null AND LEN([VehicleNo]) > 0), 0) AS [有車身號碼車輛數]
        ,ISNULL((SELECT COUNT(1) FROM [Car] WITH (NOLOCK) WHERE [EndDate] >= '{end_date} 23:59:59' AND [Status] = 1 and [MemberID] = m.MemberID AND ([VehicleNo] is null OR LEN([VehicleNo]) <= 0)), 0) AS [無車身號碼車輛數]
        ,ISNULL((SELECT TOP 1 GridCount FROM GridStock WITH (NOLOCK) WHERE MemberID = m.MemberID AND GridIfEnabled = 1), 0) as [有效車格數]
        ,(SELECT TOP 1 [StockEndDate] FROM GridStock WITH (NOLOCK) WHERE MemberID = m.MemberID AND GridIfEnabled = 1) as [有效車格最後期限]
        ,ISNULL((SELECT COUNT(1) FROM [ABCCar].[dbo].[Car] WITH (NOLOCK) WHERE [MemberID] = m.MemberID AND [Status] = 0 AND [ModifyDate] >= '2018-06-20'), 0) as [總待售車輛數]
        ,ISNULL((SELECT COUNT(1) FROM [ABCCar].[dbo].[Car] WITH (NOLOCK) WHERE [MemberID] = m.MemberID AND [Status] = 0 AND [ModifyDate] >= '2018-06-20' AND ([Source] IS NULL OR LEN([Source]) <= 0)), 0) as [abc好車網車源待售數]
        ,ISNULL((SELECT COUNT(1) FROM [ABCCar].[dbo].[Car] WITH (NOLOCK) WHERE [MemberID] = m.MemberID AND [Status] = 0 AND [ModifyDate] >= '2018-06-20' AND [Source] IS NOT NULL AND [Source] = 'HAA'), 0) as [外部車源待售數]
        ,ISNULL((SELECT COUNT(1) FROM [ABCCar].[dbo].[Car] WITH (NOLOCK) WHERE [MemberID] = m.MemberID AND [Status] = 0 AND [ModifyDate] >= '2018-06-20' AND [Source] IS NOT NULL AND ISNUMERIC([Source]) > 0), 0) as [簡易刊登車源待售數]
        ,ISNULL((SELECT COUNT(1) FROM [ABCCar].[dbo].[Car] WITH (NOLOCK) WHERE [MemberID] = m.MemberID AND [Status] = 1 AND [ModifyDate] >= '2018-06-20' AND [Source] IS NOT NULL AND ISNUMERIC([Source]) > 0), 0) as [簡易刊登車源上架數]
        ,IIF(m.[Source] = 'HAA' , m.[Email], '') AS [HAA會員]
        FROM Member m
        LEFT JOIN [ABCCarMemberCenter].[dbo].[Member] mc WITH (NOLOCK) ON m.[MemberID] = mc.[ID]
        LEFT JOIN [ABCCar].[dbo].[MemberAgree] ma WITH (NOLOCK) ON m.[MemberID] = ma.[MemberID]
        LEFT JOIN [ABCCar].[dbo].[BindingCardMember] bm WITH (NOLOCK) ON m.[MemberID] = bm.[MemberID] AND bm.[Status] = 0
        OUTER APPLY 
		(SELECT TOP 1 *
		FROM [ABCCar].[dbo].[PoolPointsHistory] ph1 with (NOLOCK)
		WHERE m.MemberID = ph1.MemberID AND ph1.RecordTypeID = 10
		ORDER BY ph1.ID DESC
	    ) AS ph
        WHERE EXISTS
         (SELECT * FROM MemberAgree WITH (NOLOCK)
          WHERE [MemberID] = m.MemberID)
        OR EXISTS
         (SELECT * FROM [dbo].[Car] car 
           JOIN [dbo].[CarModel] carModel WITH (NOLOCK) ON carModel.[IsEnable] = 1 
                      AND car.[BrandID] = carModel.[BrandID] 
                      AND car.[SeriesID]= carModel.[SeriesID] 
                      AND car.[CategoryID]= carModel.[CategoryID]
           JOIN [ABCCar].[dbo].[Member] member WITH(NOLOCK)  ON car.[MemberID] = CAST(member.MemberID AS bigint)
                      AND member.[Status] = 1
                      AND member.[Category] IN (2,3,5) --個人賣家、車商、經紀人
          WHERE car.[Status] IN (0, 1) --待售
           AND car.[ModifyDate] >= '2018-06-20'
           AND car.MemberID = m.MemberID)
        ORDER BY m.MemberID
    """
    # 執行讀取程式

    memberAgree = read_mssql(memberAgree_sg, 'ABCCar')

    return memberAgree

def mergeHotMemberID(memberAgree_new):
    hot = pd.read_excel('HOT.xlsx',converters={'統一編號':str}) 
    hot.rename(columns={'統一編號':'統編'}, inplace=True)
    
    tmp = hot[['統編','HOT會員編號']]
#     tmp['統編'] = tmp['統編'].astype(str).str.strip()
    tmp = tmp.drop_duplicates(subset=['統編'])

    mergeDf = pd.merge(memberAgree, tmp, on="統編", how='left')
    mergeDf = mergeDf[['MemberID', '會員名稱', '車商名稱', '城市別', '連絡人', '連絡電話', '統編', 'HOT會員編號', 'HAA會員', '身分別', '網路好店','註冊時間' , '車格維護業務人員' , '會員同意條款維護業務人員' , '會員同意條款維護業務人員ID', '同意會員條款時間' , '信用卡綁定時間', '最新儲值時間' , '同意條款及綁卡後上架數' , '上架車輛數' , '有車身號碼車輛數' , '無車身號碼車輛數' , '有效車格數' , '有效車格最後期限' , '總待售車輛數', 'abc好車網車源待售數', '外部車源待售數', '簡易刊登車源待售數', '簡易刊登車源上架數']]
    
#     HotMemberID = []
#     for i, m in enumerate(memberAgree_new['統編']):
#         for j, v in enumerate(hot['統編']):
#             if (m == str(v)):
#                 HotMemberID.append(j)
                
#     tmp = hot.iloc[HotMemberID,[4, 19]]
#     tmp['統編'] = tmp['統編'].astype(str)
#     tmp = tmp.drop_duplicates(subset=['統編'])
    
#     memberAgree_new['TaxID'] = memberAgree_new['TaxID'].fillna('')
#     mergeDf = pd.merge(memberAgree_new, tmp, on="統編", how='left')
#     mergeDf = mergeDf[['MemberID', '會員名稱', '車商名稱', '城市別', '連絡人', '連絡電話', '統編', 'HOT會員編號', '身分別', '網路好店','註冊時間' , '車格維護業務人員' , '會員同意條款維護業務人員' , '會員同意條款維護業務人員ID', '同意會員條款時間' , '信用卡綁定時間' , '同意條款及綁卡後上架數' , '上架車輛數' , '有車身號碼車輛數' , '無車身號碼車輛數' , '有效車格數' , '有效車格最後期限' , '總待售車輛數', 'abc好車網車源待售數', '外部車源待售數', '簡易刊登車源待售數', '簡易刊登車源上架數']]

#     mergeDf = mergeDf.drop_duplicates()
    
    return mergeDf


def mergeABCSales(memberAgree_new):
#     distribute = pd.read_excel('abc物件分配表(上架車輛業務人員歸戶)總表.xlsx', index_col=0) 

#     distributeMemberID = []
#     for i, m in enumerate(memberAgree_new['MemberID']):
#         for j, v in enumerate(distribute['MemberID']):
#             if (m == v):
#                 distributeMemberID.append(j)

#     tmp = distribute.iloc[distributeMemberID,[0, 13]]
# #     tmp['車輛上架維護業務人員'] = tmp['車輛上架維護業務人員'].astype(str)
#     tmp = tmp.drop_duplicates(subset=['車輛上架維護業務人員'])

#     # #     memberAgree_new['TaxID'] = memberAgree_new['TaxID'].fillna('')
#     mergeDf = pd.merge(memberAgree_new, tmp, on="MemberID", how='left')
#     mergeDf = mergeDf[['MemberID', '會員名稱', '車商名稱', '城市別', '連絡人', '連絡電話', '統編', 'HOT會員編號', '身分別', '網路好店','註冊時間' , '車格維護業務人員' , '會員同意條款維護業務人員' , '車輛上架維護業務人員', '會員同意條款維護業務人員ID', '同意會員條款時間' , '信用卡綁定時間' , '同意條款及綁卡後上架數' , '上架車輛數' , '有車身號碼車輛數' , '無車身號碼車輛數' , '有效車格數' , '有效車格最後期限' , '總待售車輛數', 'abc好車網車源待售數', '外部車源待售數', '簡易刊登車源待售數', '簡易刊登車源上架數']]

    distribute = pd.read_excel('abc物件分配表(上架車輛業務人員歸戶)總表_20210812.xlsx')

    tmp = distribute[['MemberID','車輛上架維護業務人員']]
    tmp = tmp.drop_duplicates(subset=['MemberID'])

    mergeDf = pd.merge(memberAgree_new, tmp, on="MemberID", how='left')
    newMergeDf = mergeDf[['MemberID', '會員名稱', '車商名稱', '城市別', '連絡人', '連絡電話', '統編', 'HOT會員編號', 'HAA會員', '身分別', '網路好店','註冊時間' , '車格維護業務人員' , '會員同意條款維護業務人員' , '車輛上架維護業務人員', '會員同意條款維護業務人員ID', '同意會員條款時間' , '信用卡綁定時間', '最新儲值時間' , '同意條款及綁卡後上架數' , '上架車輛數' , '有車身號碼車輛數' , '無車身號碼車輛數' , '有效車格數' , '有效車格最後期限' , '總待售車輛數', 'abc好車網車源待售數', '外部車源待售數', '簡易刊登車源待售數', '簡易刊登車源上架數']]
    
    # tmpNameCity = distribute[['MemberID','車商名稱', '城市別']]
    # tmpNameCity = tmpNameCity.drop_duplicates(subset=['MemberID'])
    # tmpNameCity.rename(columns={'車商名稱':'車商名稱New', '城市別':'城市別New'}, inplace=True)
    # newMergeDf = pd.merge(mergeDf, tmpNameCity, on="MemberID", how='left')
    
    # #2021/03/30 以資料庫的車商名稱、城市別為主，再疊加excel的資料到這兩個欄位
    # for index, row in newMergeDf.iterrows():
    #     if(type(row['車商名稱New']) == str):
    #         newMergeDf['車商名稱'].loc[index] = row['車商名稱New']

    # for index, row in newMergeDf.iterrows():
    #     if(type(row['城市別New']) == str):
    #         newMergeDf['城市別'].loc[index] = row['城市別New']
            
    # newMergeDf = newMergeDf[['MemberID', '會員名稱', '車商名稱', '城市別', '連絡人', '連絡電話', '統編', 'HOT會員編號', 'HAA會員', '身分別', '網路好店','註冊時間' , '車格維護業務人員' , '會員同意條款維護業務人員' , '車輛上架維護業務人員', '會員同意條款維護業務人員ID', '同意會員條款時間' , '信用卡綁定時間' , '同意條款及綁卡後上架數' , '上架車輛數' , '有車身號碼車輛數' , '無車身號碼車輛數' , '有效車格數' , '有效車格最後期限' , '總待售車輛數', 'abc好車網車源待售數', '外部車源待售數', '簡易刊登車源待售數', '簡易刊登車源上架數']]

#     abcSales = [89, 63, 61, 60, 62, 90, 97, 98, 99]

#     for index, row in mergeDf.iterrows():
#         if ((type(row['車輛上架維護業務人員']) == float) and (math.isnan(row['車輛上架維護業務人員'])) and (row['會員同意條款維護業務人員ID'] in abcSales)):
#             mergeDf['車輛上架維護業務人員'].loc[index] = row['會員同意條款維護業務人員']
            
    return newMergeDf

def splitDataByDay(memberAgree_new):
    #當天
    memberAgreeToday = memberAgree_new.loc[(pd.to_datetime(memberAgree_new['同意會員條款時間'], format='%Y-%m-%d')< now)  & (pd.to_datetime(memberAgree_new['同意會員條款時間'], format='%Y-%m-%d') >= end_date)]
    #當月
    memberAgreeMonth = memberAgree_new[(pd.to_datetime(memberAgree_new['同意會員條款時間'], format='%Y-%m-%d') < now) & (pd.to_datetime(memberAgree_new['同意會員條款時間'], format='%Y-%m-%d') >= first_day)]

    #當年
    memberAgreeYear = memberAgree_new[(pd.to_datetime(memberAgree_new['同意會員條款時間'], format='%Y-%m-%d') < now) & (pd.to_datetime(memberAgree_new['同意會員條款時間'], format='%Y-%m-%d') >= '2020-11-09')]  #上線為起始時間
    
    return memberAgreeToday, memberAgreeMonth, memberAgreeYear
    
def processData(memberAgree):
    #------------總和 start
    #已同意新條款
    totalAgree = []

    abcSales = [89, 63, 61, 60, 62, 90, 97, 98, 99]
    abcSalesWithNone = [89, 63, 61, 60, 62, 90, 97, 88, 99, 98]
    tmpOther = memberAgree.loc[~memberAgree['會員同意條款維護業務人員ID'].isin(abcSalesWithNone)]
    tmpAbcSales = memberAgree.loc[memberAgree['會員同意條款維護業務人員ID'].isin(abcSales)]
    tmpG = memberAgree.loc[memberAgree['會員同意條款維護業務人員ID'] == 88, :] #無

    totalAgree.append(memberAgree.count()['會員同意條款維護業務人員ID']) #總和
    totalAgree.append(tmpAbcSales.count()['會員同意條款維護業務人員ID']) #abc小計
    totalAgree.append(tmpOther.count()['會員同意條款維護業務人員ID']) #HOT
    totalAgree.append(tmpG.count()['會員同意條款維護業務人員ID']) #無
    
    tmpTotalCard = memberAgree.loc[(~memberAgree['信用卡綁定時間'].isnull()) & (memberAgree.同意條款及綁卡後上架數 > 0)].count()['會員同意條款維護業務人員ID']
    #已信用卡綁定
    totalAgreeCard = []
    tmpOtherCard = tmpOther.loc[(~tmpOther['信用卡綁定時間'].isnull()) & (tmpOther.同意條款及綁卡後上架數 > 0)].count()['會員同意條款維護業務人員ID']
    tmpAbcSaleCard = tmpAbcSales.loc[(~tmpAbcSales['信用卡綁定時間'].isnull()) & (tmpAbcSales.同意條款及綁卡後上架數 > 0)].count()['會員同意條款維護業務人員ID']
    tmpGCard = tmpG.loc[(~tmpG['信用卡綁定時間'].isnull()) & (tmpG.同意條款及綁卡後上架數 > 0)].count()['會員同意條款維護業務人員ID']


    totalAgreeCard.append(tmpTotalCard) #總和
    totalAgreeCard.append(tmpAbcSaleCard) #abc小計
    totalAgreeCard.append(tmpOtherCard) #HOT
    totalAgreeCard.append(tmpGCard) #無
    
    #缺信用卡
    subTotal = []
    for i, v in enumerate(totalAgree):
        subTotal.append(v - totalAgreeCard[i])
    
    #------------總和 end
 
    return totalAgree, totalAgreeCard, subTotal

def addValueToExcel(ws_main, valueList, rowIndex, colIndex):
    for index, value in enumerate(valueList):
        grid = ws_main.cell(row=4+rowIndex+index, column=3+colIndex)
        grid.value = value

def mergeLastMonth():
    lastMonth = pd.read_excel('abc好車網_會員同意書報表_3月.xlsx', index_col=0) 
    deleteList = [0]
    agree = []
    for i, v in enumerate(lastMonth['Unnamed: 12']):
        if(i not in deleteList):
            agree.append(v)
    card = []
    for i,v in enumerate(lastMonth['Unnamed: 13']):
        if(i not in deleteList):
            card.append(v)
#     sub = []
#     for i,v in enumerate(lastMonth['Unnamed: 10']):
#         if(i not in deleteList):
#             sub.append(v)    
    
#     return agree, card, sub
    print(agree, card)
    return agree, card

def splitThree(origin):
    return origin[0:13], origin[13:26], origin[26:39]

def subtract(list1, list2):
    difference = []   # initialization of result list
#     print(list1, list2)

#     for i, v in enumerate(list1):
#         if(math.isnan(v)):
#             list1[i] = np.nan_to_num(v)

#     for i, v in enumerate(list2):
#         if(math.isnan(v)):
#             list2[i] = np.nan_to_num(v)

#     zip_object = zip(list1, list2)

#     for list1_i, list2_i in zip_object:
#         difference.append(list1_i-list2_i) # append each difference to list

    for i in range(len(list1)):
        difference.append(int(list1[i]) - int(list2[i]))
        
    return difference

def addHotIDSecond(df):
    
    HOTUpdate = pd.read_excel('HOT未簽約ABC名單0204.xlsx', converters={'車商統編':str}) 

    container = []
    for i, v in enumerate(HOTUpdate['abc會員ID']):
        if(type(v) == int):
            container.append(i)

#     tmpHOTU = HOTUpdate.iloc[container, lambda HOTUpdate: [2, 4]]
    tmpHOTU = HOTUpdate[['HOT會員編號','abc會員ID']]     
    tmpHOTU = tmpHOTU.drop_duplicates(subset=['HOT會員編號'])

    tmpColorRow = []
    for index, row in tmpHOTU.iterrows():
        for i, v in df.iterrows():
            if((isinstance(v['HOT會員編號'], float)) and (v['MemberID'] == row['abc會員ID'])):
                df['HOT會員編號'].loc[i] = row['HOT會員編號']
                tmpColorRow.append(i)
    
    return df, tmpColorRow
            

if __name__ == "__main__":

    # 設定日期
    script = 'abccar_member_agree'
    #Load上個月資料
#     agree, card, sub = mergeLastMonth()
    agree, card = mergeLastMonth()
    one1, two1, three1 = splitThree(agree)
    one2, two2, three2 = splitThree(card)

    try:
        memberAgree = ReadData()
        print('Part 1 done!')
    except Exception as e:
        msg = '{0}: PART 1: 數據連線讀取失敗! The error message : {1}'.format(script, e)
        send_error_message(script='{}'.format(script), message=msg)
        print(msg)
        sys.exit(1)
    
    try:
        # memberAgree_tmp = mergeHotMemberID(memberAgree)
        df = mergeABCSales(memberAgree)
        # memberAgree_new, tmpColorRow =  addHotIDSecond(df)
        print('Part 2 done!')
    except Exception as e:
        msg = '{0}: PART 2: 數據合併失敗! The error message : {1}'.format(script, e)
        send_error_message(script='{}'.format(script), message=msg)
        print(msg)
        sys.exit(1)
        
        

    try:
        memberAgreeToday, memberAgreeMonth, memberAgreeYear = splitDataByDay(df)
        print('Part 3 done!')
    except Exception as e:
        msg = '{0}: PART 3: 數據日期切割過程出現錯誤! The error message : {1}'.format(script, e)
        send_error_message(script='{}'.format(script), message=msg)
        print(msg)
        sys.exit(1)

    try:
        #當日
        totalAgreeToday, totalAgreeCardToday, subTotalToday = processData(memberAgreeToday)
        #當月
        totalAgreeMonth, totalAgreeCardMonth, subTotalMonth = processData(memberAgreeMonth)
        #當年
        totalAgreeYear, totalAgreeCardYear, subTotalYear = processData(memberAgreeYear)    
        print('Part 4 done!')
    except Exception as e:
        msg = '{0}: PART 4: 數據計算出現錯誤! The error message : {1}'.format(script, e)
        send_error_message(script='{}'.format(script), message=msg)
        print(msg)
        sys.exit(1)
        
    try:
#         one3, two3, three3 = splitThree(sub)
        month1 = subtract(totalAgreeYear, one1) #左上
        month4 = subtract(totalAgreeCardYear, one2) #中上

        wb = load_workbook('abc好車網_會員同意書報表.xlsx')
        # Select the first work sheet
        ws_main = wb.worksheets[0]
        
        # Add today date
        date = ws_main.cell(row=2, column=2)
        date.value = '會員簽約狀況(總和-' + end_date +')'
        
        #-------當日---------
        #總和已同意
        addValueToExcel(ws_main, totalAgreeToday, 0, 0)
        #總和已同意信用卡
        addValueToExcel(ws_main, totalAgreeCardToday, 0, 1)

        #-------當月---------
        #總和已同意
        addValueToExcel(ws_main, month1, 0, 5)
        #總和已同意信用卡
        addValueToExcel(ws_main, month4, 0, 6)
        
        #-------當年---------
        #總和已同意
        addValueToExcel(ws_main, totalAgreeYear, 0, 10)
        #總和已同意信用卡
        addValueToExcel(ws_main, totalAgreeCardYear, 0, 11)
        #總和缺漏
        addValueToExcel(ws_main, subTotalYear, 0, 12)
        
        ws = wb.worksheets[1]
        ws.delete_rows(2, 100000)

        n=1
        for r in dataframe_to_rows(df, index=False, header=False):
#             ws.append(r)
            m = 1
            for i in r:
                grid = ws.cell(row=1+n, column=m)
                grid.value = i
                m+=1
            n +=1
        
        # for i in tmpColorRow:
        #     grid = ws.cell(row=2+i, column=8)
        #     grid.font = Font(b=True, color="FF0000")
            
        # Save the worksheet
        wb.save('abc好車網_會員同意書報表.xlsx')
        
        print('PART 5 all done!')
        
    except Exception as e:
        msg = '{0}: PART 5: 數據寫入出現錯誤! The error message : {1}'.format(script, e)
        send_error_message(script='{}'.format(script), message=msg)
        print(msg)
        sys.exit(1)

    try:
        send_daily_report_memberAgree(receiver='j', path=path)
        msg = 'PART 6: 寄送同意書日報OK'
        print(msg)
    except Exception as e:
        msg = '{0}: PART 6: 寄送同意書日報發生錯誤. The error message : {1}'.format(script, e)
        send_daily_report_error(msg)
        print(msg)
        sys.exit(1)
