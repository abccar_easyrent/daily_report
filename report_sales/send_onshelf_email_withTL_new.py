#==============
# Path: /home/hadoop/jessielu/report_sales
# Description: Send email and transform format to pdf.
# Date: 2022/05/24
# Author: Jessie 
#==============
# Description: Add Teams / Line notify message.
# Modify Date: 2022/09/20
# Modify User: Sam
# Crontab Send Time : 08:10
#==============

import sys
import os
import numpy as np

# sys.path.insert(0, '/home/jessie/MyNotebook/daily_report/')
sys.path.insert(0, '/home/hadoop/jessielu/')

from Module.EmailFuncForSales import send_daily_report_onself_office, send_daily_report_onself_sales, send_daily_report_error
from Module.NotifyFunc import Teams_Notify, Line_Notify, Line_Notify_TL
from openpyxl import Workbook, load_workbook
from openpyxl.drawing.spreadsheet_drawing import AbsoluteAnchor
from openpyxl.drawing.xdr import XDRPoint2D, XDRPositiveSize2D
from openpyxl.utils.units import pixels_to_EMU, cm_to_EMU
import openpyxl
import pandas as pd
from datetime import datetime, timedelta, date
from dateutil.relativedelta import relativedelta  
import math
import shutil
import subprocess

# import matplotlib相關套件
import matplotlib.pyplot as plt
plt.switch_backend('agg')
import matplotlib.dates as mdates
# import字型管理套件
from matplotlib.font_manager import FontProperties
#plt.rcParams['font.weight']= 'bold'
from PIL import Image
from pdf2image import convert_from_path

#原本
now = date.today().strftime("%Y-%m-%d")

#系統日 2022-08-11 (執行日期)
sysdate =  date.today()

#系統日昨天 2022-08-10
end_date = (sysdate - timedelta(1)).strftime("%Y-%m-%d")
print(end_date)

#系統日回推一個月
one_month_firstday = sysdate + relativedelta(months=-1)

path = '/home/hadoop/jessielu/report_sales/files/'

os.chdir(path)

# 移除前次產出圖檔
imgs = ['abc_line_chart.png','abc_line_chart_crop.png','abc_report.png','abc_report_crop1.png','abc_report_crop2.png','abc_report_daily.png']
for r in imgs:
    if os.path.isfile(r):
        os.remove(r)
        
        
def doc2pdf_linux(docPath, pdfPath):
    cmd = 'libreoffice --headless --convert-to pdf'.split()+[docPath] + ['--outdir'] + [pdfPath]
    p = subprocess.Popen(cmd, stderr=subprocess.PIPE, stdout=subprocess.PIPE,bufsize=1)
    p.wait(timeout=30)
    # stdout, stderr = p.communicate()
    # print(stdout, stderr)
    p.communicate()

def doc2pdf(docPath, pdfPath):
    docPathTrue = os.path.abspath(docPath)  
    return doc2pdf_linux(docPathTrue, pdfPath)

def mergeData():
    #file8891 = pd.read_excel('/home/hadoop/ellen_crawl/Ellen_Others/8891_report.xlsx')
    file8891 = pd.read_excel('/home/hadoop/jessielu/crawler/8891_report.xlsx')
    
   
    #企劃檔案
    wb = load_workbook('abc好車網_架上台數報表_企劃.xlsx')

    # Select the first work sheet
    ws_main = wb.worksheets[0]

    c = ws_main.cell(row=10, column=5)
    #c.value = int(file8891['所有車輛'].iloc[-1]) #8891總數 當日
    #c.value = int(file8891['所有車輛'].iloc[-2]) #8891總數 前一日
    #8891總數
    if (np.isnan(file8891['所有車輛'].iloc[-2])):
        #print("-")
        c.value = "-" 
    else:
        c.value = int(file8891['所有車輛'].iloc[-2])
        #c.value = int(file8891['所有車輛'].iloc[-1])
        #print(int(file8891['所有車輛'].iloc[-1])) 
        


    wb.save('abc好車網_架上台數報表_企劃.xlsx')

    #PDF檔案
    wb = load_workbook('abc好車網_架上台數報表.xlsx')

    # Select the first work sheet
    ws_main = wb.worksheets[0]

    c = ws_main.cell(row=9, column=2)
    #c.value =  int(file8891['所有車輛'].iloc[-1]) #8891總數 當日
    #c.value =  int(file8891['所有車輛'].iloc[-2]) #8891總數 前一日
    #8891總數
    if (np.isnan(file8891['所有車輛'].iloc[-2])):
        #print("-")
        c.value = "-" 
    else:
        c.value = int(file8891['所有車輛'].iloc[-2])
        #c.value = int(file8891['所有車輛'].iloc[-1])
        #print(int(file8891['所有車輛'].iloc[-1])) 

    # 移除折線圖     
    print(ws_main._images)
    for idx, x in enumerate(ws_main._images):
        del ws_main._images [idx]
        print(idx, x)   

    # 插入折線圖
    lineChart = 'abc_line_chart_crop.png';
    if os.path.isfile(lineChart):
        img = openpyxl.drawing.image.Image(lineChart)
        #img.anchor = 'A34'
        #img.width = 560
        #img.height = 180
        #ws_main.add_image(img)
        
        #p2e = pixels_to_EMU
        #h, w = img.height, img.width
        #position = XDRPoint2D(p2e(0), p2e(790))
        #size = XDRPositiveSize2D(p2e(565), p2e(180))

        #img.anchor = AbsoluteAnchor(pos=position, ext=size)
        #ws_main.add_image(img)
        p2e = pixels_to_EMU
        #h, w = img.height, img.width
        position = XDRPoint2D(p2e(315), p2e(0))
        size = XDRPositiveSize2D(p2e(318), p2e(180))

        img.anchor = AbsoluteAnchor(pos=position, ext=size)
        ws_main.add_image(img)
        
    wb.save('abc好車網_架上台數報表.xlsx')

    wordpath= '/home/hadoop/jessielu/report_sales/files/abc好車網_架上台數報表.xlsx'
    pdfpath= '/home/hadoop/jessielu/report_sales/files'

    #Excel 轉 PDF
    doc2pdf(wordpath, pdfpath)

def dftoLineChart():
    # 讀取 Excel 檔案轉為 Pandas 資料表
    df8891 = pd.read_excel('/home/hadoop/jessielu/crawler/8891_report.xlsx', index_col=None, engine='openpyxl')
    #df8891 = pd.read_excel('/home/jessie-ws/Dev/jupyterlab/daily_report/report_sales/files/files/8891_report.xlsx', index_col=None, engine='openpyxl')
    # 轉換日期型態
    df8891['日期']= pd.to_datetime(df8891['日期'])
    # 取出近一個月資料
    df8891_filtered = df8891[df8891['日期'].dt.strftime('%Y-%m-%d') >= one_month_firstday.strftime('%Y-%m-%d')].drop(columns=['全部車商數量','嚴選車商','旺店車商','嚴選車輛','自售車輛','前日成交數','今日新增'])
    #df8891.info()

    # 讀取 Excel 檔案轉為 Pandas 資料表
    dfabc = pd.read_excel("abc好車網_架上台數報表_企劃.xlsx", '上架數推移',index_col=None, engine='openpyxl').drop_duplicates(subset=['日期'], keep='last')
    # 轉換型態
    dfabc['日期']= pd.to_datetime(dfabc['日期'])
    dfabc['總上架數']= pd.to_numeric(dfabc['總上架數'], downcast="integer")
    # 取出近一個月資料
    dfabc_filtered = dfabc[dfabc['日期'].dt.strftime('%Y-%m-%d') >= one_month_firstday.strftime('%Y-%m-%d')].drop(columns=['車商會員上架數','原廠認證上架數','TCPO','LCPO','他牌CPO','聯盟館上架數'])
    
    # 使用日期條件合併二個 Pandas 資料表
    mergeDf = pd.merge(dfabc_filtered, df8891_filtered, on="日期", how='left')
    mergeDf['所有車輛'] = mergeDf['所有車輛'].fillna(0)
    mergeDf['總上架數']= pd.to_numeric(mergeDf['總上架數'], downcast="integer")
    mergeDf['所有車輛']= pd.to_numeric(mergeDf['所有車輛'], downcast="integer")
    #dfabc.info()

    # Line Chart Start
    # 使用月份當做X軸資料
    month = mergeDf['日期']
    # 取ABCCar架上台數
    dafabc = mergeDf['總上架數']
    # 取8891架上台數
    daf8891 = mergeDf['所有車輛']

    values = []
    values.append(dafabc.max())
    values.append(daf8891.max())
    values.append(dafabc[dafabc > 0].min())
    values.append(daf8891[daf8891 > 0].min())

    # 指定使用字型和大小
    myfont = FontProperties(fname='/home/hadoop/jessielu/report_sales/files/simhei.ttf')
    #myfont = FontProperties(fname='/home/jessie-ws/Dev/jupyterlab/daily_report/report_sales/files/simhei.ttf', size=14)

    # 設定圖片大小為長5、寬3,邊框
    plt.figure(figsize=(5,3),dpi=110,linewidth = 4)
    plt.gca().yaxis.set_major_formatter(plt.matplotlib.ticker.StrMethodFormatter('{x:,.0f}'))


    # 把資料放進來並指定對應的X軸、Y軸的資料 用圓形做標記(o-)，並指定線條顏色為綠色、使用label標記線條含意
    #plt.plot(month, dafabc,'o-',color = 'r', label="abc好車網")
    plt.plot(month, dafabc,'.-',color = 'r', label="abc好車網架上台數")

    # 把資料放進來並指定對應的X軸、Y軸的資料，用方形做標記(s-)，並指定線條顏色為紅色，使用label標記線條含意
    #plt.plot(month,daf8891,'s-',color = 'b', label="8891")
    plt.plot(month,daf8891,'.-',color = 'b', label="8891架上台數")

    # 設定圖片標題，以及指定字型設定，x代表與圖案最左側的距離，y代表與圖片的距離
    plt.title("abc好車網與競品架上台數比較("+ end_date + ")", backgroundcolor="#FFF2CC", fontproperties=myfont , fontsize=13, x=0.45, y=1.01)
    ##plt.title("abc好車網與競品架上台數比較("+ end_date + ")", fontproperties=myfont , fontsize=13, x=0.45, y=1.01)

    # 設定x軸/y軸起始值.終止值
    y_min = (np.ceil(values[values.index(min(values))]/1000)*1000-2000)
    y_max = (np.ceil(values[values.index(max(values))]/1000)*1000+2000)
    plt.xlim((one_month_firstday,date.today()))
    plt.ylim((y_min,y_max))

    # 設定x軸文字翻轉度.字型
    #plt.xticks(rotation=25, fontweight='light',  fontsize='x-small',)
    plt.xticks(rotation=0, fontweight='bold',  fontsize='small',)

    # 設定x軸/y軸刻度字体大小
    plt.xticks(fontproperties=myfont)
    plt.yticks(fontproperties=myfont)
    plt.xticks(fontsize=8)
    plt.yticks(fontsize=12)
    # 設定x軸/y軸刻度間隔
    my_x_ticks = np.arange(one_month_firstday,date.today(),5)
    plt.xticks(my_x_ticks)
    my_y_ticks = np.arange(y_min,y_max,1000)
    plt.yticks(my_y_ticks)

    # 標示x軸(labelpad代表與圖片的距離)
    #plt.xlabel("日期", fontsize=30, labelpad = 15)

    # 標示y軸(labelpad代表與圖片的距離)
    #plt.ylabel("台數", fontsize=30, labelpad = 20)

    # 顯示出線條標記位置
    plt.legend(loc = "best", prop=myfont, fontsize=6)
    #plt.legend(bbox_to_anchor =(1.0, 1.13), ncol = 2, fontsize=9) 
    #plt.legend(bbox_to_anchor =(0.6, 1.15), ncol = 2, fontsize=16) 
    #plt.legend(bbox_to_anchor =(0.75, 1.15), ncol = 2) 
  
    # 顯示格線
    plt.grid(ls='--')

    # x 軸日期格式化
    dtFmt = mdates.DateFormatter('%m月%d日') # define the formatting
    plt.gca().xaxis.set_major_formatter(dtFmt) # apply the format to the desired axis

    # 另存圖檔
    plt.savefig('abc_line_chart.png')
    # 畫出圖片
    # plt.show()
    
    # 裁圖
    img = Image.open("abc_line_chart.png")
    (w, h) = img.size
    #im1 = img.crop((0, 0, (w-40), h)) 
    im1 = img.crop((0, 0, (w-44), h)) 
    im1.save('abc_line_chart_crop.png', quality=100) 
    
if __name__ == "__main__":
    print(now)
    # 設定日期
    script = 'send_onshelf_email_withTL_new'
    
    try:
        #企劃檔案
        wb = load_workbook('abc好車網_架上台數報表_企劃.xlsx',read_only=True, data_only=True, keep_links=False, keep_vba=False)
        sheet1 = wb.worksheets[0]
        # Read Date
        ReportDate = sheet1.cell(row=2, column=3)
        Subtitle = ReportDate.value 
        wb.close()
        #標題清除字串,取出日期
        for r in (("abc好車網_營運報表_", ""), ("(", ""), (")", "")):
            Subtitle = Subtitle.replace(*r)
        #檢查報表產生日期,與昨天日期是否相同,不同就停止發送     
        if Subtitle!=end_date:
            msg = '日報檢查:報表未產生(報表日{0}/執行日{1}),停止執行發送.'.format(Subtitle, end_date)
            raise Exception(msg)
        print('Part Check Date OK.')
    except Exception as e: 
        msg = '{0} {1}'.format(script, e)       
        print(msg)
        #Line 通知管理員
        Line_Notify(receiver = 'j', path = path,msg = msg, FN = '')
        sys.exit(1)
        
    try:
        # ABCCar & 8891 架上台數比較
        dftoLineChart()
        print('Part 0 done!')
    except Exception as e:
        msg = '{0}: PART 0: 繪製折線圖失敗! The error message : {1}'.format(script, e)
        print(msg)
        sys.exit(1)
        
    try:
        mergeData()
        print('Part 1 done!')
    except Exception as e:
        msg = '{0}: PART 1: 填入營運日報值失敗! The error message : {1}'.format(script, e)
        print(msg)
        sys.exit(1)
    
    try:
        #正式區寄送
        send_daily_report_onself_office(receiver='g', path=path) #五個Sheets的明細版本，並移除營收資訊
        send_daily_report_onself_sales(receiver='g', path=path) #僅寄出PDF版本(含營收)
        #測試區寄送
        #send_daily_report_onself_office(receiver='j', path=path) #五個Sheets的明細版本，並移除營收資訊
        #send_daily_report_onself_sales(receiver='j', path=path) #僅寄出PDF版本(含營收)
        msg = 'PART 2: 寄送架上台數OK'
        print(msg)
    except Exception as e:
        msg = '{0}: PART 2: 寄送架上台數日報發生錯誤. The error message : {1}'.format(script, e)
        send_daily_report_error(msg)
        print(msg)
        sys.exit(1)
  
    try:
        #PDF日報表轉換IMAGE
        images = convert_from_path('abc好車網_架上台數報表.pdf')
        images[0].save('abc_report.png')
        
        img = Image.open("abc_report.png")
        # 每日營運表 裁圖處理 正式區 img.crop((120, 85, 725, 590)) / 測試區 img.crop((110, 85, 635, 590)) 
        im1 = img.crop((120, 85, 725, 590))  
        im1.save('abc_report_crop1.png', quality=100) 
        # 每日營收表 裁圖處理 正式區 img.crop((120, 600, 855, 845)) / 測試區 img.crop((110, 600, 750, 860)) 
        im2 = img.crop((120, 600, 855, 845))
        im2.save('abc_report_crop2.png', quality=100)         
        
        
        # 依序合併3張圖表
        # abc好車網營運報表 => abc_report_crop1.png
        # abc好車網與競品架上台數比較 => abc_line_chart_crop.png 
        # abc好車網營收報表 => abc_report_crop2.png
        list_im = ['abc_report_crop1.png','abc_line_chart_crop.png','abc_report_crop2.png']

        # creates a new empty image, RGB mode, and size 444 by 95
        file = 'abc_report_daily.png'
        # 正式區 Image.new('RGB', (735,1135), color="white")
        # 測試區 Image.new('RGB', (640,1115), color="white")
        new_im = Image.new('RGB', (735,1135), color="white")
        vertical = 10
        for elem in list_im:
            im=Image.open(elem)
            (w, h) = im.size
            if elem=='abc_line_chart_crop.png':
                new_im.paste(im,(10,vertical))
            else:
                new_im.paste(im,(0,vertical))
            vertical = vertical + h + 20
            
        new_im.save(file)

        # Notify 是否停止發送 pause=> Y [停用] / N [啟用]
        pause = 'N'
        if pause=='N':
            # 發送訊息 receiver=> j [測試發送] / g [正式發送]
            receiver = 'g'
            if receiver=='g':
                message = "【abc好車網每日戰報】(%s)" % end_date
            else:
                message = "TEST【abc好車網每日戰報】(%s)" % end_date
            Teams_Notify(receiver = receiver, path = path,msg = message, FN = file)
            ## \n Line 換行符號       
            message =  "\n" + message   
            Line_Notify(receiver = receiver, path = path,msg = message, FN = file)
            ## 週一發 TL
            if datetime.today().weekday()==0:
                Line_Notify_TL(receiver = receiver, path = path,msg = message, FN = file)
        
        #NotifyList = {'abc好車網_營運報表':'abc_report_crop1.png', 'abc好車網_營收報表':'abc_report_crop2.png', 'abc好車網與競品架上台數比較':'abc_line_chart_crop.png'}
        #for i, v in enumerate(NotifyList):
        #    file = NotifyList[v]
        #    if receiver=='g':
        #        message = str(v) + " (%s)" % end_date
        #    else:
        #        message = "TEST_" + str(v) + " (%s)" % end_date
        #    Teams_Notify(receiver = receiver, path = path,msg = message, FN = file)
        #    # \n Line 換行符號       
        #    message =  "\n" + message   
        #    Line_Notify(receiver = receiver, path = path,msg = message, FN = file)
    
        msg = 'PART 3: abc好車網每日戰報 Notify OK'
        print(msg)
    except Exception as e:
        msg = '{0}: PART 3: abc好車網每日戰報 Notify 發生錯誤. The error message : {1}'.format(script, e)
        send_daily_report_error(msg)
        print(msg)
        sys.exit(1)
        
        
        
        
