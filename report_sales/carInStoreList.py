#==============
# Path: /home/hadoop/jessielu/report_sales
# Description: 當月實車在店物件
# Date: 2024/01/16
# Author: Sam 
#==============

# -*- coding: utf-8 -*-
import sys
sys.path.insert(0, '/home/hadoop/jessielu/')

from Module.EmailFuncForSales import send_carinstore_sales, send_daily_report_error, send_error_message
from Module.ConnFunc import read_test_mssql
from datetime import datetime, timedelta, date
from dateutil.relativedelta import relativedelta

from openpyxl import Workbook, load_workbook
from openpyxl.utils.dataframe import dataframe_to_rows

#今天
now = date.today()

#補發
# now = date.today() - relativedelta(days=1)

path = '/home/hadoop/jessielu/report_sales/files/'

# 讀取資料
def readData():
    carList_sql = f"""
        SELECT mc.MemberID as [會員編號]
            ,ISNULL(m.[Name], '') as [會員名稱]
            --,ISNULL(m.[CarDealerName], '') as [車商名稱]
            ,(CASE(m.[Category]) 
                    WHEN 1 THEN '一般買家' 
                    WHEN 2 THEN '一般賣家' 
                    WHEN 3 THEN '車商' 
                    WHEN 4 THEN '原廠' 
                    ELSE '買賣顧問' 
            END ) AS [身分別]
            , t.ID as [業務ID]
            , ISNULL(t.[Name],'')  as [負責業務]
            ,mc.CarID as [車輛編號]
            ,mc.StartDate as [上架時間]
            ,mc.EndDate as [下架時間]
            ,cm.BrandName as[廠牌]
            ,cm.DisplayName as [車輛名稱]
            ,Convert(varchar(7),mc.ManufactureDate,120) as [出廠年月]
            ,mc.Mileage as [里程]
            ,round(mc.Price,2) as [售價]
            ,(CASE WHEN ISNULL(mc.HOTCERNO,'')!='' THEN 'Y' ELSE '' END) as [認證車輛]
            ,(CASE WHEN mc.ForFreeCampaignID>0 THEN 'Y' ELSE '' END) as [公會專案]

            --,mp.CarInStoreDate as [實車在店建立日]
            ,(CASE WHEN mp.CarInStoreDate<(dateadd(m, datediff(m,0,mc.StartDate)+1,5)) THEN 'Y' ELSE '' END) as [實車在店] --CarInStoreValid
            ,(CASE WHEN (SELECT COUNT(1) FROM CarFile f1 with(nolock) WHERE f1.Category = '11' and f1.Status=1 and f1.CarID=mc.CarID)>0 THEN 'Y' ELSE '' END) as [上傳影片]
            ,(CASE WHEN (SELECT COUNT(1) FROM CarFile f2 with(nolock) WHERE f2.Category = '12' and f2.Status=1 and f2.CarID=mc.CarID)>0 THEN 'Y' ELSE '' END)  AS [一鍵生成]
        FROM Car mc with(nolock) 
        LEFT JOIN (SELECT a.ID,a.InputCarId,a.Status as CarInStoreStatus, a.CreateDate as CarInStoreDate --實車在店
                    FROM [dbo].[CarInStoreCheckLog] a with(nolock)
                WHERE a.ID = (select max(ID) from [dbo].[CarInStoreCheckLog] p with(nolock) where p.InputCarId=a.InputCarId and p.Status='1')
                    and a.CreateDate>'2024-01-01 00:00:00') mp on mc.CarID=mp.InputCarID
        LEFT JOIN Member m with(nolock) on mc.MemberID=m.MemberID
        LEFT JOIN [dbo].[MemberUserAccountSalesMapping] k with(nolock) on m.MemberID=k.MemberID
        LEFT JOIN [dbo].[UserAccountSales] t with(nolock) on k.UserAccountSalesID=t.ID
        LEFT JOIN [ABCCar].[dbo].CarModel cm WITH (NOLOCK) ON cm.BrandID = mc.BrandID AND cm.SeriesID = mc.SeriesID AND cm.CategoryID = mc.CategoryID
        WHERE mc.StartDate between 
                CAST(CAST( dateadd(m, datediff(m,0,DATEADD(DAY,-1,getdate()))  ,0)  AS date) AS datetime) 
                AND  
                CAST(CAST( dateadd(m, datediff(m,0,DATEADD(DAY,-1,getdate()))+1,0)    AS date) AS datetime) 
        ORDER BY 4
    """

    salesList_sql = f"""
    SELECT ID, ZoneName as ZoneID, Name, NameSeq 
    FROM UserAccountSales
    WHERE Type='2' and Status=1 and ZoneName between 1 and 5
    ORDER BY NameSeq
    """

    carListRtn = read_test_mssql(carList_sql, 'ABCCar')
    salesListRtn = read_test_mssql(salesList_sql, 'ABCCar')
    return carListRtn, salesListRtn


def splitDF(rtn, id):
    #Filter data by sales ID
    tmp = rtn.loc[(rtn['業務ID'].isin(id)), :].reset_index().sort_values(by=['上架時間']).drop(['index'], axis=1)

    tmpDetail = tmp[['會員編號', '會員名稱', '身分別', '負責業務', '車輛編號', '上架時間', '下架時間', '廠牌', '車輛名稱', '出廠年月', '里程', '售價', '認證車輛', '公會專案', '實車在店', '上傳影片', '一鍵生成']].reset_index().drop(['index'], axis=1)

    return tmpDetail

# 輸出資料至Excel
def addDFtoExcel(df, path, path2):
    wb = load_workbook(path)

    # Select the first work sheet
    ws = wb.worksheets[0]
    ws.delete_rows(2, 100000)

    n=1
    for r in dataframe_to_rows(df, index=False, header=False):
        m = 1
        for i in r:
            grid = ws.cell(row=1+n, column=m)
            grid.value = i
            m+=1
        n +=1

    wb.save(path2)


if __name__ == '__main__':
    print(now)
    # 設定日期
    script = 'carInStoreList'

    try:
        rtnDF, rtnDFSales = readData()

        # 所有業務名單
        salesID = Zone1 = Zone2 = Zone3 = Zone4 = Zone5 = []
        salesID = rtnDFSales['ID'].values.tolist()

        tmp1 = rtnDFSales.loc[(rtnDFSales['ZoneID'] == "1"), :]
        tmp2 = rtnDFSales.loc[(rtnDFSales['ZoneID'] == "2"), :]
        tmp3 = rtnDFSales.loc[(rtnDFSales['ZoneID'] == "3"), :]
        tmp4 = rtnDFSales.loc[(rtnDFSales['ZoneID'] == "4"), :]
        tmp5 = rtnDFSales.loc[(rtnDFSales['ZoneID'] == "5"), :]
        
        Zone1 = tmp1['ID'].values.tolist()
        Zone2 = tmp2['ID'].values.tolist()
        Zone3 = tmp3['ID'].values.tolist()
        Zone4 = tmp4['ID'].values.tolist()
        Zone5 = tmp5['ID'].values.tolist()

    except Exception as e:
        msg = 'PART 1: 取得資料庫資料失敗! The error message : {}'.format(e)
        send_error_message(script=script, message=msg)
        print(msg)
        sys.exit(1)
        
    print('PART 1: Finished!')

    # 暫存寄送業務名單
    sendSales = []
    for i in salesID:
        print(i)
        try:
            tmp = splitDF(rtnDF, [i])
            msg = 'PART 2-1 : 切分業務資料成功！'
            print(msg)
        except Exception as e:
            msg = 'PART 2-1　: 切分業務資料發生錯誤. The error message :{}'.format(e)
            send_daily_report_error(msg)
            print(msg)
            sys.exit(1)
        
        if(len(tmp['負責業務']) > 0):
            sendSales.append(tmp['負責業務'][0])
            try:
                addDFtoExcel(tmp, path + '實車在店.xlsx', path + tmp['負責業務'][0] +'_實車在店.xlsx')
                msg = 'PART 2-2 : 寫入本月資料成功！'
                print(msg)
            except Exception as e:
                msg = 'PART 2-2　: 寫入本月資料發生錯誤. The error message :{}'.format(e)
                send_daily_report_error(msg)
                print(msg)
                sys.exit(1)

    print('PART 2: Finished!')

    # 暫存寄送TL名單
    sendTL = []
    #TLList = {'北區':[90, 102, 109], '桃苗':[60, 101], '中區':[99, 110],'雲嘉':[104],'高屏':[108, 61]}
    TLList = {'北區':Zone1, '桃苗':Zone2, '中區':Zone3, '雲嘉':Zone4, '高屏':Zone5}
    for i, v in enumerate(TLList):
        print(TLList[v])
        try:
            tmp = splitDF(rtnDF, TLList[v])
            msg = 'PART 3-1 : 切分業務資料成功！'
            print(msg)
        except Exception as e:
            msg = 'PART 3-1　: 切分業務資料發生錯誤. The error message :{}'.format(e)
            send_daily_report_error(msg)
            print(msg)
            sys.exit(1)

        if(len(tmp['負責業務']) > 0):
            sendTL.append(v)
            try:
                addDFtoExcel(tmp, path + '實車在店.xlsx', path + v +'_實車在店.xlsx')
                msg = 'PART 3-2 : 寫入本月資料成功！'
                print(msg)
            except Exception as e:
                msg = 'PART 3-2　: 寫入本月資料發生錯誤. The error message :{}'.format(e)
                send_daily_report_error(msg)
                print(msg)
                sys.exit(1)

    print('PART 3: Finished!')

    try:
        tmpDetail = rtnDF[['會員編號', '會員名稱', '身分別', '負責業務', '車輛編號', '上架時間', '下架時間', '廠牌', '車輛名稱', '出廠年月', '里程', '售價', '認證車輛', '公會專案', '實車在店', '上傳影片', '一鍵生成']].reset_index().drop(['index'], axis=1)
        addDFtoExcel(tmpDetail, path + '實車在店.xlsx', path + 'All_Sales_實車在店.xlsx')
        msg = 'PART 4-3 : 寫入累計資料成功！'
        print(msg)
    except Exception as e:
        msg = 'PART 4-3　: 寫入累計資料發生錯誤. The error message :{}'.format(e)
        send_daily_report_error(msg)
        print(msg)
        sys.exit(1)
        
    print('PART 4: Finished!')

    try:
        #All
        All = ['HUNG57@hotaimotor.com.tw', '31879@hotaimotor.com.tw','JOANNE373@hotaimotor.com.tw', 'JONATHANKO@hotaimotor.com.tw', 'WILLIAM@hotaimotor.com.tw', 'STEVEWU@hotaimotor.com.tw', 'MANDYLIN@hotaimotor.com.tw', 'MASA0204@hotaimotor.com.tw', 'AARONWU@hotaimotor.com.tw', 'ANGELLIN@hotaileasing.com.tw','SAM903@hotaileasing.com.tw','ROMANCHEN@hotaileasing.com.tw']
        # All = ['WILLIAM@hotaimotor.com.tw', 'STEVEWU@hotaimotor.com.tw', 'MANDYLIN@hotaimotor.com.tw', 'MARKYAO@hotaimotor.com.tw','FARA@hotaimotor.com.tw' ]
        send_carinstore_sales('g', path, All, 'All_Sales_實車在店')
        
        #TL mail
        TLMail = {'北區': 'EAGLE@hotaimotor.com.tw', '桃苗': 'maggiechen@hotaimotor.com.tw', '雲嘉': 'HENNY@hotaimotor.com.tw', '高屏': 'LIN32093@hotaimotor.com.tw'}
        #TLMail = {'北區': 'EAGLE@hotaimotor.com.tw', '桃苗': 'maggiechen@hotaimotor.com.tw', '中區': 'MANDYLIN@hotaimotor.com.tw', '雲嘉': 'HENNY@hotaimotor.com.tw', '高屏': 'LIN32093@hotaimotor.com.tw'}
        #TLMail = {'北區': 'EAGLE@hotaimotor.com.tw', '桃苗': 'maggiechen@hotaimotor.com.tw', '中區': 'SAM903@hotaimotor.com.tw', '雲嘉': 'HENNY@hotaimotor.com.tw', '高屏': 'LIN32093@hotaimotor.com.tw'}
        # TLMail = {'北區': 'MANDYLIN@hotaimotor.com.tw', '桃苗': 'MANDYLIN@hotaimotor.com.tw', '中區': 'MANDYLIN@hotaimotor.com.tw', '雲嘉': 'MANDYLIN@hotaimotor.com.tw', '高屏': 'MANDYLIN@hotaimotor.com.tw'}
        for i, v in enumerate(TLMail):
            if v in sendTL:
                send_carinstore_sales('g', path, TLMail[v], v +'_實車在店')

        #Sales mail
        SalesMail = {'邱雅凌':'CHERRY5588@hotaimotor.com.tw', '林賢宏':'AARON711018@hotaimotor.com.tw', '童嘉如':'TWEETY@hotaimotor.com.tw', '黃煒凱':'LSTTR12@hotaimotor.com.tw', '吳美姍':'SS0210@hotaimotor.com.tw', '林心沛':'MIA0111@hotaimotor.com.tw', '陳湘月':'MANJAK06@hotaimotor.com.tw'}
        # SalesMail = {'孫和莆':'MANDYLIN@hotaimotor.com.tw', '邱雅凌':'MANDYLIN@hotaimotor.com.tw', '林賢宏':'MANDYLIN@hotaimotor.com.tw', '童嘉如':'MANDYLIN@hotaimotor.com.tw', '黃煒凱':'MANDYLIN@hotaimotor.com.tw', '吳美姍':'MANDYLIN@hotaimotor.com.tw', '陳億業':'MANDYLIN@hotaimotor.com.tw', '林心沛':'MANDYLIN@hotaimotor.com.tw', '陳湘月':'MANDYLIN@hotaimotor.com.tw'}
        for i, v in enumerate(SalesMail):
            if v in sendSales:
                send_carinstore_sales('g', path, SalesMail[v], v +'_實車在店')

    except Exception as e:
        msg = 'PART 5 : 寄送日報發生錯誤. The error message : {}'.format(e)
        send_daily_report_error(msg)
        print(msg)
        sys.exit(1)

    print('PART 5 : 寄送日報OK!')
