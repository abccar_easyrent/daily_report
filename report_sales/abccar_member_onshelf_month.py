#==============
# Path: /home/hadoop/jessielu/report_sales
# Description: Sales report to calculate metrics with per sales.
# Date: 2024/01/24
# Author: Sam
#==============

# coding: utf-8
import sys
import os
import numpy as np

#sys.path.insert(0, '/home/jessie-ws/Dev/jupyterlab/daily_report/')
sys.path.insert(0, '/home/hadoop/jessielu/')
import pymssql
from Module.EmailFuncForSales import send_monthly_report_onself_office, send_daily_report_error, send_error_message
from Module.ConnFunc import read_test_mssql
from Module.ConnectInfo import mssql_info

#from openpyxl.styles import Border, Side, PatternFill, Font, GradientFill, Alignment, numbers
from openpyxl import Workbook, load_workbook
from openpyxl.utils.dataframe import dataframe_to_rows

import pandas as pd
from datetime import datetime, timedelta, date
from dateutil.relativedelta import relativedelta  

#正式機原本
now = date.today() - relativedelta(days=1)

StartYear = str(now.year)
EndYear = str(now.year+1)

path = '/home/hadoop/jessielu/report_sales/files/'
#path = '/home/jessie-ws/Dev/jupyterlab/daily_report/report_sales/files/'

os.chdir(path)

# 建立暫存View
def CreateTempView():
    
    mssql_db = mssql_info['address']
    mssql_user = mssql_info['user']
    mssql_password = mssql_info['password']
    mssql_db_name = 'ABCCar'

    # 連結 SQL                   
    connect_db = pymssql.connect(host=mssql_db, port=1433, user=mssql_user, password=mssql_password, database=mssql_db_name, charset='utf8', autocommit=True)
   
    cursor = connect_db.cursor()
    # 執行 SQL 指令
    sql = f"""
        IF EXISTS(SELECT * FROM sys.views WHERE name = 'V_TempView')
        BEGIN
            DROP VIEW V_TempView
        END
     """
    cursor.execute(sql)
    
    sql = f"""
        CREATE VIEW V_TempView
        AS SELECT p.MemberID,convert(varchar(7), p.CreateDate,120) as [CreateYm],CONVERT(varchar(10),count(p.CarID)) as [Nums] 
        FROM Car p with(nolock) 
        WHERE p.StartDate>'{StartYear}-01-01 00:00:00' and p.StartDate<'{EndYear}-01-01 00:00:00'
        Group By p.MemberID,convert(varchar(7), p.CreateDate,120)
    """
    cursor.execute(sql)
    
    # 關閉 SQL 連線
    connect_db.close()
    
# 讀取數據
def ReadData():
    CreateTempView()
    memberAgree_sg2 = f"""
    SELECT 
        m.MemberID
        ,ISNULL(mc.[Name], '') as [會員名稱]
        ,ISNULL(m.[CarDealerName], '') as [車商名稱]
        --,ISNULL(CONCAT(mc.[CountryName], mc.[DistrictName]), '') AS [城市別]
        ,CASE WHEN (m.[CarDealerCountryName] is null or m.[CarDealerCountryName] = '') THEN CONCAT(mc.[CountryName], mc.[DistrictName]) ELSE CONCAT(m.[CarDealerCountryName], m.[CarDealerDistrictName]) END as [城市別]
        ,ISNULL(m.[ContactPerson], '') AS [連絡人]
        ,ISNULL(mc.[CellPhone], '') AS [連絡電話]
        ,ISNULL(m.[TaxID], '') AS [統編]
        ,ISNULL(m.[HOTMemberID], '') AS [HOT會員編號]

        -- ,IIF(m.[Source] = 'HAA' , m.[SourceKey], '') AS [HAA會員]
        
        ,(CASE(m.[Category]) 
            WHEN 1 THEN '一般買家' 
            WHEN 2 THEN '一般賣家' 
            WHEN 3 THEN '車商' 
            WHEN 4 THEN '原廠' 
            ELSE '買賣顧問' 
        END ) AS [身分別]
        ,IIF(m.[VIPLevel] = 0 , '', '是') AS [網路好店]
        ,IIF(pd.ID IS NULL, '', '是') AS [買萬送萬]
        ,ISNULL(kvd.[Name], '') AS [公會]
        ,ISNULL(ua.[Name], '') AS [維護業務]
        ,ISNULL(ua.[NameSeq], '') AS [維護業務序號]
        ,ISNULL(ua.[ID], '') AS [維護業務ID]
        
        ,(CASE WHEN convert(varchar(7), getdate(),120) > '{StartYear}-01' THEN ISNULL(m1.Nums,'0') ELSE '' END) as [1月上架台數]
        ,(CASE WHEN convert(varchar(7), getdate(),120) > '{StartYear}-02' THEN ISNULL(m2.Nums,'0') ELSE '' END) as [2月上架台數]
        ,(CASE WHEN convert(varchar(7), getdate(),120) > '{StartYear}-03' THEN ISNULL(m3.Nums,'0') ELSE '' END) as [3月上架台數]
        ,(CASE WHEN convert(varchar(7), getdate(),120) > '{StartYear}-04' THEN ISNULL(m4.Nums,'0') ELSE '' END) as [4月上架台數]
        ,(CASE WHEN convert(varchar(7), getdate(),120) > '{StartYear}-05' THEN ISNULL(m5.Nums,'0') ELSE '' END) as [5月上架台數]
        ,(CASE WHEN convert(varchar(7), getdate(),120) > '{StartYear}-06' THEN ISNULL(m6.Nums,'0') ELSE '' END) as [6月上架台數]
        ,(CASE WHEN convert(varchar(7), getdate(),120) > '{StartYear}-07' THEN ISNULL(m7.Nums,'0') ELSE '' END) as [7月上架台數]
        ,(CASE WHEN convert(varchar(7), getdate(),120) > '{StartYear}-08' THEN ISNULL(m8.Nums,'0') ELSE '' END) as [8月上架台數]
        ,(CASE WHEN convert(varchar(7), getdate(),120) > '{StartYear}-09' THEN ISNULL(m9.Nums,'0') ELSE '' END) as [9月上架台數]
        ,(CASE WHEN convert(varchar(7), getdate(),120) > '{StartYear}-10' THEN ISNULL(m10.Nums,'0') ELSE '' END) as [10月上架台數]
        ,(CASE WHEN convert(varchar(7), getdate(),120) > '{StartYear}-11' THEN ISNULL(m11.Nums,'0') ELSE '' END) as [11月上架台數]
        ,(CASE WHEN convert(varchar(7), getdate(),120) > '{StartYear}-12' THEN ISNULL(m12.Nums,'0') ELSE '' END) as [12月上架台數]

    FROM [ABCCar].[dbo].Member m WITH (NOLOCK)
        LEFT JOIN [ABCCarMemberCenter].[dbo].[Member] mc WITH (NOLOCK) ON m.[MemberID] = mc.[ID]
        LEFT JOIN (SELECT * FROM [ABCCar].[dbo].[BindingCardMember] sm WITH (NOLOCK) 
                WHERE sm.[ID] = (SELECT Max(ID) FROM [ABCCar].[dbo].[BindingCardMember] sd WITH (NOLOCK) WHERE  sd.[MemberID]=sm.[MemberID] AND sd.[Status] in (0,99)) 
                ) bm ON m.[MemberID] = bm.[MemberID] AND bm.[Status] in (0,99) 

        LEFT JOIN [ABCCar].[dbo].[MemberUserAccountSalesMapping] mua WITH (NOLOCK) ON m.[MemberID] = mua.[MemberID]
        LEFT JOIN [ABCCar].[dbo].[UserAccountSales] ua WITH (NOLOCK) ON ua.[ID] = mua.[UserAccountSalesID]
        LEFT JOIN [ABCCar].[dbo].[KeyValueDetail] kvd WITH (NOLOCK) ON kvd.Value = m.Guild AND kvd.KeyName = 'Guild'
		LEFT JOIN (select p1.MemberID,p1.Nums from [ABCCar].[dbo].[V_TempView] p1 WHERE p1.CreateYm = '{StartYear}-01') m1 on m1.MemberID=m.MemberID  
		LEFT JOIN (select p2.MemberID,p2.Nums from [ABCCar].[dbo].[V_TempView] p2 WHERE p2.CreateYm = '{StartYear}-02') m2 on m2.MemberID=m.MemberID  
		LEFT JOIN (select p3.MemberID,p3.Nums from [ABCCar].[dbo].[V_TempView] p3 WHERE p3.CreateYm = '{StartYear}-03') m3 on m3.MemberID=m.MemberID  
		LEFT JOIN (select p4.MemberID,p4.Nums from [ABCCar].[dbo].[V_TempView] p4 WHERE p4.CreateYm = '{StartYear}-04') m4 on m4.MemberID=m.MemberID  
		LEFT JOIN (select p5.MemberID,p5.Nums from [ABCCar].[dbo].[V_TempView] p5 WHERE p5.CreateYm = '{StartYear}-05') m5 on m5.MemberID=m.MemberID  
		LEFT JOIN (select p6.MemberID,p6.Nums from [ABCCar].[dbo].[V_TempView] p6 WHERE p6.CreateYm = '{StartYear}-06') m6 on m6.MemberID=m.MemberID  
		LEFT JOIN (select p7.MemberID,p7.Nums from [ABCCar].[dbo].[V_TempView] p7 WHERE p7.CreateYm = '{StartYear}-07') m7 on m7.MemberID=m.MemberID  
		LEFT JOIN (select p8.MemberID,p8.Nums from [ABCCar].[dbo].[V_TempView] p8 WHERE p8.CreateYm = '{StartYear}-08') m8 on m8.MemberID=m.MemberID  
		LEFT JOIN (select p9.MemberID,p9.Nums from [ABCCar].[dbo].[V_TempView] p9 WHERE p9.CreateYm = '{StartYear}-09') m9 on m9.MemberID=m.MemberID  
		LEFT JOIN (select p10.MemberID,p10.Nums from [ABCCar].[dbo].[V_TempView] p10 WHERE p10.CreateYm = '{StartYear}-10') m10 on m10.MemberID=m.MemberID  
		LEFT JOIN (select p11.MemberID,p11.Nums from [ABCCar].[dbo].[V_TempView] p11 WHERE p11.CreateYm = '{StartYear}-11') m11 on m11.MemberID=m.MemberID  
		LEFT JOIN (select p12.MemberID,p12.Nums from [ABCCar].[dbo].[V_TempView] p12 WHERE p12.CreateYm = '{StartYear}-12') m12 on m12.MemberID=m.MemberID  
        OUTER APPLY 
        (SELECT TOP 1 po.*
            FROM [ABCCar].[dbo].ProductOrder po WITH (NOLOCK) 
            LEFT JOIN [ABCCar].[dbo].ProductOrderItem poi WITH (NOLOCK) ON po.ID = poi.OrderID
            WHERE m.MemberID = po.MemberID 
            AND poi.ProductID = 1 
            AND po.OrderStatusID = 30
            AND po.PaymentStatusID IN (30, 40, 45)
        ) AS pd

        OUTER APPLY 
            (SELECT TOP 1 *
            FROM [ABCCar].[dbo].[PoolPointsHistory] ph1 with (NOLOCK)
            WHERE m.MemberID = ph1.MemberID AND ph1.RecordTypeID = 10
            ORDER BY ph1.ID DESC
        ) AS ph

        WHERE 
        (EXISTS
            (SELECT * FROM [ABCCar].[dbo].MemberAgree WITH (NOLOCK)
            WHERE [MemberID] = m.MemberID)
        OR EXISTS
            (SELECT * FROM [ABCCar].[dbo].[Car] car WITH (NOLOCK)
            JOIN [ABCCar].[dbo].[CarModel] carModel WITH (NOLOCK) ON carModel.[IsEnable] = 1 
                        AND car.[BrandID] = carModel.[BrandID] 
                        AND car.[SeriesID]= carModel.[SeriesID] 
                        AND car.[CategoryID]= carModel.[CategoryID]
            JOIN [ABCCar].[dbo].[Member] member WITH (NOLOCK)  ON car.[MemberID] = CAST(member.MemberID AS bigint)
                        AND member.[Status] = 1
                        AND (member.[Category] IN (2,3,5) OR member.[MemberID] IN (234160,232894)) --個人賣家、車商、經紀人、特定車商
            WHERE car.[Status] IN (0, 1) --待售
            AND car.[ModifyDate] >= '2018-06-20'
            AND car.MemberID = m.MemberID))
            
        AND m.[Category] > 1
        AND m.[Status] = 1
        --AND (bm.BindingDate IS NOT NULL OR ph.ID IS NOT NULL)
        AND ((bm.BindingDate IS NOT NULL OR ph.ID IS NOT NULL) OR (m.MemberID in (234160,232894) AND bm.BindingDate IS NULL))
        --AND m.MemberID = 204439
        ORDER BY m.MemberID    
    """
    
    #主要明細表與會員台數狀況
    memberAgree_sg1 = f"""
       SELECT 
        m.MemberID
        ,ISNULL(mc.[Name], '') as [會員名稱]
        ,ISNULL(m.[CarDealerName], '') as [車商名稱]
        ,CASE WHEN (m.[CarDealerCountryName] is null or m.[CarDealerCountryName] = '') THEN CONCAT(mc.[CountryName], mc.[DistrictName]) ELSE CONCAT(m.[CarDealerCountryName], m.[CarDealerDistrictName]) END as [城市別]
        ,ISNULL(m.[ContactPerson], '') AS [連絡人]
        ,ISNULL(mc.[CellPhone], '') AS [連絡電話]
        ,ISNULL(m.[TaxID], '') AS [統編]
        ,ISNULL(m.[HOTMemberID], '') AS [HOT會員編號]

        ,(CASE(m.[Category]) 
            WHEN 1 THEN '一般買家' 
            WHEN 2 THEN '一般賣家' 
            WHEN 3 THEN '車商' 
            WHEN 4 THEN '原廠' 
            ELSE '買賣顧問' 
        END ) AS [身分別]
        ,IIF(m.[VIPLevel] = 0 , '', '是') AS [網路好店]
        ,IIF(pd.ID IS NULL, '', '是') AS [買萬送萬]
        ,ISNULL(kvd.[Name], '') AS [公會]
        ,ISNULL(ua.[Name], '') AS [維護業務]
        ,ISNULL(ua.[NameSeq], '') AS [維護業務序號]
        ,ISNULL(ua.[ID], '') AS [維護業務ID]
        FROM [ABCCar].[dbo].Member m WITH (NOLOCK)
        LEFT JOIN [ABCCarMemberCenter].[dbo].[Member] mc WITH (NOLOCK) ON m.[MemberID] = mc.[ID]

        LEFT JOIN (SELECT * FROM [ABCCar].[dbo].[BindingCardMember] sm WITH (NOLOCK) 
                WHERE sm.[ID] = (SELECT Max(ID) FROM [ABCCar].[dbo].[BindingCardMember] sd WITH (NOLOCK) WHERE  sd.[MemberID]=sm.[MemberID] AND sd.[Status] in (0,99)) 
                ) bm ON m.[MemberID] = bm.[MemberID] AND bm.[Status] in (0,99) 

        LEFT JOIN [ABCCar].[dbo].[MemberUserAccountSalesMapping] mua WITH (NOLOCK) ON m.[MemberID] = mua.[MemberID]
        LEFT JOIN [ABCCar].[dbo].[UserAccountSales] ua WITH (NOLOCK) ON ua.[ID] = mua.[UserAccountSalesID]
        LEFT JOIN [ABCCar].[dbo].[KeyValueDetail] kvd WITH (NOLOCK) ON kvd.Value = m.Guild AND kvd.KeyName = 'Guild'
        OUTER APPLY 
        (SELECT TOP 1 po.*
            FROM [ABCCar].[dbo].ProductOrder po WITH (NOLOCK) 
            LEFT JOIN [ABCCar].[dbo].ProductOrderItem poi WITH (NOLOCK) ON po.ID = poi.OrderID
            WHERE m.MemberID = po.MemberID 
            AND poi.ProductID = 1 
            AND po.OrderStatusID = 30
            AND po.PaymentStatusID IN (30, 40, 45)
        ) AS pd

        OUTER APPLY 
            (SELECT TOP 1 *
            FROM [ABCCar].[dbo].[PoolPointsHistory] ph1 with (NOLOCK)
            WHERE m.MemberID = ph1.MemberID AND ph1.RecordTypeID = 10
            ORDER BY ph1.ID DESC
        ) AS ph

        WHERE 
        (EXISTS
            (SELECT * FROM [ABCCar].[dbo].MemberAgree WITH (NOLOCK)
            WHERE [MemberID] = m.MemberID)
        OR EXISTS
            (SELECT * FROM [ABCCar].[dbo].[Car] car WITH (NOLOCK)
            JOIN [ABCCar].[dbo].[CarModel] carModel WITH (NOLOCK) ON carModel.[IsEnable] = 1 
                        AND car.[BrandID] = carModel.[BrandID] 
                        AND car.[SeriesID]= carModel.[SeriesID] 
                        AND car.[CategoryID]= carModel.[CategoryID]
            JOIN [ABCCar].[dbo].[Member] member WITH (NOLOCK)  ON car.[MemberID] = CAST(member.MemberID AS bigint)
                        AND member.[Status] = 1
                        AND (member.[Category] IN (2,3,5) OR member.[MemberID] IN (234160,232894)) --個人賣家、車商、經紀人、特定車商
            WHERE car.[Status] IN (0, 1) --待售
            AND car.[ModifyDate] >= '2018-06-20'
            AND car.MemberID = m.MemberID))
            
        AND m.[Category] > 1
        AND m.[Status] = 1
        --AND (bm.BindingDate IS NOT NULL OR ph.ID IS NOT NULL)
        AND ((bm.BindingDate IS NOT NULL OR ph.ID IS NOT NULL) OR (m.MemberID in (234160,232894) AND bm.BindingDate IS NULL))
        --AND m.MemberID = 204439
        ORDER BY m.MemberID
    
    """
    
    memberAgree1 = read_test_mssql(memberAgree_sg1, 'ABCCar')   
    memberAgree2 = read_test_mssql(memberAgree_sg2, 'ABCCar')

    return memberAgree1,memberAgree2


# 讀取暫存的每月月底台數檔案
def addStageLastMonth(df):
    
    df2 = df
    for month in range(1,13):
        if month<12:
            month_end_day = datetime(now.year, month + 1, 1) - timedelta(days=1)
        else:
            month_end_day = datetime(now.year+1, 1, 1) - timedelta(days=1)
        print(month)
        
        #monthStr = month_end_day.strftime("%m")
        #print(month_end_day.strftime("%Y-%m-%d"))
        
        csvFile = 'stageLastMonth_'+ str(month_end_day.strftime("%Y-%m-%d")) +'.csv'
        if os.path.exists(csvFile):
            StageLastMonth = pd.read_csv(csvFile) 
            print( str(month) + '月底架上台數')
            tmp = StageLastMonth[['MemberID','上月底架上台數']]
            tmp = tmp.drop_duplicates(subset=['MemberID'])
            tmp = tmp.rename(columns={'MemberID': 'MemberID', '上月底架上台數': str(month)+'月底架上台數'})

            mergeDf = pd.merge(df2, tmp, on="MemberID", how='left')
            mergeDf[str(month)+'月底架上台數'] = mergeDf[str(month)+'月底架上台數'].fillna(0)

            df2 = mergeDf
        else:
            break
    
    for month in range(1,13):
        if str(str(month)+'月底架上台數') not in df2:
            df2[str(month)+'月底架上台數'] = ''
     
    return df2

# 更改公會名稱
def changeGuildName(df):
    df['公會'] = df['公會'].replace(['台北市汽車商業同業公會','新北市汽車商業同業公會', '桃園市汽車商業同業公會', '苗栗縣汽車商業同業公會', '高雄市汽車商業同業公會', '台中市汽車商業同業公會', '基隆市汽車商業同業公會', '宜蘭縣汽車商業同業公會', '南投縣汽車商業同業公會', '台南市汽車商業同業公會', '臺南市直轄市汽車商業同業公會', '中華民國汽車商業同業公會全聯會', '臺中市直轄市汽車商業同業公會', '屏東縣汽車商業同業公會', '高雄市新汽車商業同業公會', '花蓮縣汽車商業同業公會', '嘉義市汽車商業同業公會'],['台北','新北', '桃園', '苗栗', '高雄', '台中', '基隆', '宜蘭', '南投', '台南', '台南市', '同業', '台中市', '屏東', '高雄(新)', '花蓮', '嘉義'])

    return df

# 將資料輸出至Excel
def addValueToExcel(ws_main, valueList, rowIndex, colIndex):
    for index, value in enumerate(valueList):
        grid = ws_main.cell(row=4+rowIndex+index, column=4+colIndex)
        grid.value = value
        
if __name__ == "__main__":
    print(now)
    print(StartYear + " - " + EndYear)
    script = 'abccar_member_onshelf_month'

    try:
        memberAgree1,memberAgree2 = ReadData() 
        print('Part 1 done!')
    except Exception as e:
        msg = '{0}: PART 1: 數據連線讀取失敗! The error message : {1}'.format(script, e)
        send_error_message(script='{}'.format(script), message=msg)
        print(msg)
        sys.exit(1)
    
    try:
        memberAgree_new_tmp = addStageLastMonth(memberAgree1)
        memberAgree1_new = changeGuildName(memberAgree_new_tmp)
        
        memberAgree2_new = changeGuildName(memberAgree2)
        print('Part 2 done!')
    except Exception as e:
        msg = '{0}: PART 2: 數據合併失敗! The error message : {1}'.format(script, e)
        send_error_message(script='{}'.format(script), message=msg)
        print(msg)
        sys.exit(1)
    
    try:
        #=========================== Load office file      
        wb = load_workbook('abc好車網_架上台數月報表.xlsx')
        

        #Sheet 1
        ws = wb.worksheets[0]
        ws.delete_rows(2, 200000)
        
        # 組業務排序邏輯字串
        if len(memberAgree1_new)>0:
            memberAgree1_new['維護業務'] = memberAgree1_new.apply(lambda x: '0' + str(x['維護業務序號']) + ' ' + x['維護業務']  if len(str(x['維護業務序號'])) == 1 else str(x['維護業務序號']) + ' ' + x['維護業務'] , axis=1)
            memberAgree1_new['維護業務'] = memberAgree1_new['維護業務'].replace('00 ', '')
        
        df_Office1 = memberAgree1_new[[
            'MemberID', '會員名稱', '車商名稱', '城市別', '連絡人', '連絡電話', 
            '身分別', '網路好店', '買萬送萬', '公會', '維護業務', 
            '1月底架上台數', '2月底架上台數', '3月底架上台數', '4月底架上台數', 
            '5月底架上台數', '6月底架上台數', '7月底架上台數', '8月底架上台數', 
            '9月底架上台數', '10月底架上台數', '11月底架上台數', '12月底架上台數']]
        
        n=1
        for r in dataframe_to_rows(df_Office1, index=False, header=False):
            m = 1
            for i in r:
                grid = ws.cell(row=1+n, column=m)
                grid.value = i
                m+=1
            n +=1

        
        #Sheet 2
        ws = wb.worksheets[1]
        ws.delete_rows(2, 200000)
        # 組業務排序邏輯字串
        if len(memberAgree2_new)>0:
            memberAgree2_new['維護業務'] = memberAgree2_new.apply(lambda x: '0' + str(x['維護業務序號']) + ' ' + x['維護業務']  if len(str(x['維護業務序號'])) == 1 else str(x['維護業務序號']) + ' ' + x['維護業務'] , axis=1)
            memberAgree2_new['維護業務'] = memberAgree2_new['維護業務'].replace('00 ', '')
        
        df_Office2 = memberAgree2_new[[
            'MemberID', '會員名稱', '車商名稱', '城市別', '連絡人', '連絡電話', 
            '身分別', '網路好店', '買萬送萬', '公會', '維護業務', 
            '1月上架台數', '2月上架台數', '3月上架台數', '4月上架台數', 
            '5月上架台數', '6月上架台數', '7月上架台數', '8月上架台數', 
            '9月上架台數', '10月上架台數', '11月上架台數', '12月上架台數']]
        
        n=1
        for r in dataframe_to_rows(df_Office2, index=False, header=False):
            m = 1
            for i in r:
                grid = ws.cell(row=1+n, column=m)
                grid.value = i
                m+=1
            n +=1
            
        wb.save('abc好車網_架上台數月報表.xlsx')
        print('PART 3 all done!')

    except Exception as e:
        msg = '{0}: PART 3: 數據寫入出現錯誤! The error message : {1}'.format(script, e)
        send_daily_report_error(script='{}'.format(script), message=msg)
        print(msg)
        sys.exit(1)


    try:
        
        #正式區寄送
        send_monthly_report_onself_office(receiver='g', path=path)

        msg = 'PART 4 : 寄送月報OK'
        print(msg)
    except Exception as e:
        msg = 'PART 4 : 寄送月報發生錯誤. The error message : {}'.format(e)
        send_daily_report_error(msg)
        print(msg)
        sys.exit(1)
