# coding: utf-8
import sys
import os
import numpy as np

# sys.path.insert(0, '/home/jessie/MyNotebook/daily_report/')
sys.path.insert(0, '/home/hadoop/jessielu/')

from Module.EmailFuncForSales import send_daily_report_onself_office, send_daily_report_onself_sales, send_error_message, send_daily_report_error
from Module.ConnFunc import read_mssql, read_test_mssql, to_mysql, read_mysql
from Module.HelpFunc import display_set

from openpyxl.styles import Border, Side, PatternFill, Font, GradientFill, Alignment
from openpyxl import Workbook, load_workbook
from openpyxl.utils.dataframe import dataframe_to_rows

import pandas as pd
from datetime import datetime, timedelta, date
from dateutil.relativedelta import relativedelta  
import math
import shutil
import subprocess

#原本
now = date.today().strftime("%Y-%m-%d")
# # 補發用
# now = (date.today() - relativedelta(days=1)).strftime("%Y-%m-%d")

display_set(200, 30)
path = '/home/hadoop/jessielu/report_sales/files/'
# path = '/home/jessie/MyNotebook/easyRent/scripts/'

os.chdir(path)

def doc2pdf_linux(docPath, pdfPath):
    cmd = 'libreoffice --headless --convert-to pdf'.split()+[docPath] + ['--outdir'] + [pdfPath]
    p = subprocess.Popen(cmd, stderr=subprocess.PIPE, stdout=subprocess.PIPE,bufsize=1)
    p.wait(timeout=30)
    # stdout, stderr = p.communicate()
    # print(stdout, stderr)
    p.communicate()

def doc2pdf(docPath, pdfPath):
    docPathTrue = os.path.abspath(docPath)  
    return doc2pdf_linux(docPathTrue, pdfPath)

def mergeData():

    # distribute = pd.read_excel('../../daily_report/files/sales_update.xlsx')
    file8891 = pd.read_excel('/home/hadoop/ellen_crawl/Ellen_Others/8891_report.xlsx')
    
    #企劃檔案
    wb = load_workbook('abc好車網_架上台數報表_企劃.xlsx')

    # Select the first work sheet
    ws_main = wb.worksheets[0]

    c = ws_main.cell(row=9, column=4)
    c.value =  int(file8891['所有車輛'].iloc[-1]) #8891總數


    wb.save('abc好車網_架上台數報表_企劃.xlsx')

    #PDF檔案
    wb = load_workbook('abc好車網_架上台數報表.xlsx')

    # Select the first work sheet
    ws_main = wb.worksheets[0]

    c = ws_main.cell(row=8, column=2)
    c.value =  int(file8891['所有車輛'].iloc[-1]) #8891總數

    wb.save('abc好車網_架上台數報表.xlsx')

    wordpath= '/home/hadoop/jessielu/report_sales/files/abc好車網_架上台數報表.xlsx'
    pdfpath= '/home/hadoop/jessielu/report_sales/files'

    doc2pdf(wordpath, pdfpath)

if __name__ == "__main__":
    print(now)
    # 設定日期
    script = 'send_onshelf_email_withTL'

    try:
        mergeData()
        print('Part 1 done!')
    except Exception as e:
        msg = '{0}: PART 1: 填入營運日報值失敗! The error message : {1}'.format(script, e)
        # send_error_message(script='{}'.format(script), message=msg)
        print(msg)
        sys.exit(1)
    
    try:
        send_daily_report_onself_office(receiver='g', path=path)
        send_daily_report_onself_sales(receiver='g', path=path)
        msg = 'PART 2: 寄送架上台數OK'
        print(msg)
    except Exception as e:
        msg = '{0}: PART 2: 寄送架上台數日報發生錯誤. The error message : {1}'.format(script, e)
        send_daily_report_error(msg)
        print(msg)
        sys.exit(1)
