#==============
# Path: /home/hadoop/jessielu/report_sales
# Description: Sales report to calculate metrics with per sales.
# Date: 2022/05/24
# Author: Jessie 
#==============

# coding: utf-8
import copy
import sys
import os
import numpy as np

sys.path.insert(0, '/home/jessie-ws/Dev/jupyterlab/daily_report/')
#sys.path.insert(0, '/home/hadoop/jessielu/')

from Module.EmailFuncForSales import send_daily_report_onself, send_daily_report_error, send_error_message
from Module.ConnFunc import read_mssql, read_test_mssql, to_mysql, read_mysql

from openpyxl.styles import Border, Side, PatternFill, Font, GradientFill, Alignment, numbers
from openpyxl import Workbook, load_workbook
from openpyxl.utils.dataframe import dataframe_to_rows

import pandas as pd
from datetime import datetime, timedelta, date
from dateutil.relativedelta import relativedelta  
from copy import copy

import matplotlib as mpl
mpl.use('agg')

import matplotlib.pyplot as plt

#原本
#now = date.today() + relativedelta(days=1)
# #補發用
now = date.today()

if(now.day == 1): #2021/11/1; 2022/01/01
    tmpDay = now - relativedelta(days=1) #2021/10/31; 2021/12/31
    #這個月的第一天
    first_day = date(tmpDay.year, tmpDay.month, 1) #2021/10/1; 2021/12/1
    #上個月的最後一天
    lastMonth_end_day = date(first_day.year, first_day.month, 1) - relativedelta(days=1)#2021/09/30; 2021/11/30
    #下個月的第一天
    nextMonth_first_day = first_day + relativedelta(months=1)#2021/11/1; 2022/01/01
    #這個月最後一天
    thisMonth_end_day = nextMonth_first_day - relativedelta(days=1)#2021/10/31; 2021/12/31
#if now = 2022/01/02
else:
    #這個月的第一天
    first_day = date(now.year, now.month, 1) #2022/01/01
    #上個月的最後一天
    lastMonth_end_day = date(first_day.year, first_day.month, 1) - relativedelta(days=1) #2021/12/31
    #下個月的第一天
    nextMonth_first_day = first_day + relativedelta(months=1) #2022/02/01
    #這個月最後一天
    thisMonth_end_day = nextMonth_first_day - relativedelta(days=1) #2022/01/31

#昨天
end_date = (now - timedelta(1)).strftime("%Y-%m-%d") 
#前天
the_day_before_yesterday_date = (now - timedelta(2)).strftime("%Y-%m-%d") 

path = '/home/jessie-ws/Dev/jupyterlab/daily_report/report_sales/files/'
#path = '/home/hadoop/jessielu/report_sales/files/'

os.chdir(path)

# 讀取數據
def ReadData():

    #每月月初，要更新月底檔案，暫存上月底台數狀況
    if(now.day == 1):
        stageLastMonth = f"""
            SELECT 
                m.[MemberID]
                ,ISNULL(( SELECT COUNT(1) FROM ABCCar.dbo.Car c1 WITH (NOLOCK),ABCCar.dbo.CarModel carmodel WITH (NOLOCK) 
				           WHERE c1.[MemberID] = m.MemberID AND c1.[Status] = 1  AND GetDate() Between c1.[StartDate] And c1.[EndDate]
					         AND carmodel.IsEnable = 1 AND c1.BrandID = carmodel.BrandID AND c1.SeriesID = carmodel.SeriesID AND c1.CategoryID = carmodel.CategoryID
			    ), 0) as [上月底架上台數]
                --,ISNULL((SELECT COUNT(1) FROM [ABCCar].[dbo].[Car] WITH (NOLOCK) WHERE [MemberID] = m.MemberID AND [Status] = 1  and GetDate() Between [Car].[StartDate] And [Car].[EndDate]), 0) as [上月底架上台數]
                --,ISNULL((SELECT COUNT(1) FROM [Car] WITH (NOLOCK) WHERE [MemberID] = m.MemberID AND [Status] = 1 AND [EndDate] >=  '{end_date} 23:59:59'), 0) as [上月底架上台數]
            FROM [ABCCar].[dbo].Member m
            LEFT JOIN [ABCCarMemberCenter].[dbo].[Member] mc WITH (NOLOCK) ON m.[MemberID] = mc.[ID]
            LEFT JOIN [ABCCar].[dbo].[MemberAgree] ma WITH (NOLOCK) ON m.[MemberID] = ma.[MemberID]
            --LEFT JOIN [ABCCar].[dbo].[BindingCardMember] bm WITH (NOLOCK) ON m.[MemberID] = bm.[MemberID] AND bm.[Status] in (0,99)
		    LEFT JOIN (SELECT * FROM [ABCCar].[dbo].[BindingCardMember] sm WITH (NOLOCK) 
		               WHERE sm.[ID] = (SELECT Max(ID) FROM [ABCCar].[dbo].[BindingCardMember] sd WITH (NOLOCK) WHERE  sd.[MemberID]=sm.[MemberID] AND sd.[Status] in (0,99)) 
				       ) bm ON m.[MemberID] = bm.[MemberID] AND bm.[Status] in (0,99) 
            LEFT JOIN [ABCCar].[dbo].[MemberUserAccountSalesMapping] mua WITH (NOLOCK) ON m.[MemberID] = mua.[MemberID]
            LEFT JOIN [ABCCar].[dbo].[UserAccountSales] ua WITH (NOLOCK) ON ua.[ID] = mua.[UserAccountSalesID]
            OUTER APPLY 
                (SELECT TOP 1 *
                FROM [ABCCar].[dbo].[PoolPointsHistory] ph1 with (NOLOCK)
                WHERE m.MemberID = ph1.MemberID AND ph1.RecordTypeID = 10
                ORDER BY ph1.ID DESC
            ) AS ph
            WHERE 
            (EXISTS
                (SELECT * FROM [ABCCar].[dbo].MemberAgree WITH (NOLOCK)
                WHERE [MemberID] = m.MemberID)
            OR EXISTS
                (SELECT * FROM [ABCCar].[dbo].[Car] car 
                JOIN [ABCCar].[dbo].[CarModel] carModel WITH (NOLOCK) ON carModel.[IsEnable] = 1 
                            AND car.[BrandID] = carModel.[BrandID] 
                            AND car.[SeriesID]= carModel.[SeriesID] 
                            AND car.[CategoryID]= carModel.[CategoryID]
                JOIN [ABCCar].[dbo].[Member] member WITH(NOLOCK)  ON car.[MemberID] = CAST(member.MemberID AS bigint)
                            AND member.[Status] = 1
                            AND (member.[Category] IN (2,3,5) OR member.[MemberID] IN (234160,232894)) --個人賣家、車商、經紀人、特定車商
                WHERE car.[Status] IN (0, 1) --待售
                AND car.[ModifyDate] >= '2018-06-20'
                AND car.MemberID = m.MemberID)) 
            AND m.[Category] > 1
            AND m.[Status] = 1
            --AND (bm.BindingDate IS NOT NULL OR ph.ID IS NOT NULL)
            AND ((bm.BindingDate IS NOT NULL OR ph.ID IS NOT NULL) OR (m.MemberID in (234160,232894) AND bm.BindingDate IS NULL))
            --AND m.MemberID = 204439
            ORDER BY m.MemberID
        """
        # 執行讀取程式
        stageOnself = read_test_mssql(stageLastMonth, 'ABCCar')
        #儲存當月月底的資料
        stageOnself.to_csv('stageLastMonth_'+ str(end_date) +'.csv')
        print('output done!')

    #主要明細表與會員台數狀況
    memberAgree_sg = f"""
    SELECT 
        m.MemberID
        ,ISNULL(mc.[Name], '') as [會員名稱]
        ,ISNULL(m.[CarDealerName], '') as [車商名稱]
        --,ISNULL(CONCAT(mc.[CountryName], mc.[DistrictName]), '') AS [城市別]
        ,CASE WHEN (m.[CarDealerCountryName] is null or m.[CarDealerCountryName] = '') 
		THEN CONCAT(mc.[CountryName], mc.[DistrictName]) 
		ELSE CONCAT(m.[CarDealerCountryName], m.[CarDealerDistrictName]) END as [城市別]

        ,ISNULL(m.[ContactPerson], '') AS [連絡人]
        ,ISNULL(mc.[CellPhone], '') AS [連絡電話]
        ,ISNULL(m.[TaxID], '') AS [統編]
        ,ISNULL(m.[HOTMemberID], '') AS [HOT會員編號]

        -- ,IIF(m.[Source] = 'HAA' , m.[SourceKey], '') AS [HAA會員]
        
        ,(CASE(m.[Category]) 
            WHEN 1 THEN '一般買家' 
            WHEN 2 THEN '一般賣家' 
            WHEN 3 THEN '車商' 
            WHEN 4 THEN '原廠' 
            ELSE '買賣顧問' 
        END ) AS [身分別]
        ,IIF(m.[VIPLevel] = 0 , '', '是') AS [網路好店]
        ,IIF(pd.ID IS NULL, '', '是') AS [買萬送萬]
        ,ISNULL(kvd.[Name], '') AS [公會]
		,ISNULL(ua.[Name], '') AS [維護業務]
        ,ISNULL(ua.[NameSeq], '') AS [維護業務序號]
		,ISNULL(ua.[ID], '') AS [維護業務ID]
        ,IIF(m.[Source] = 'HAA' and RIGHT(m.[SourceKey], 1) <> 'A', m.[SourceKey], '') AS [HAA帳號]
 
        --,ISNULL((SELECT COUNT(1) FROM (SELECT gt.CarID FROM [ABCCar].[dbo].[GridTrans] gt WITH (NOLOCK) LEFT JOIN [ABCCar].[dbo].[Car] c WITH (NOLOCK) ON gt.CarID = c.CarID WHERE gt.[MemberID] = m.MemberID AND [GridTranTypeID] = 202 AND gt.[CreateDate] BETWEEN '{first_day} 00:00:00' AND '{nextMonth_first_day} 00:00:00' AND DATEDIFF(day, c.[StartDate], c.[EndDate])> 2 GROUP BY gt.CarID) as tmp1), 0) as [新上架實績台數]
        -- 新上架實績台數(本月)，總表用

        --[本日新上架車輛]--GridTrans.Type=202,CreateDate=今天 == 本日新上
        ,ISNULL((SELECT COUNT(1) FROM (SELECT CarID FROM [ABCCar].[dbo].[GridTrans] WITH (NOLOCK) WHERE [MemberID] = m.MemberID AND [GridTranTypeID] = 202 
		AND [CreateDate] BETWEEN '{end_date} 00:00:00' AND '{now} 00:00:00' AND CARID IN (SELECT carid from Car WITH (NOLOCK) where (ISNULL(Source, N' ') <> 'HOTV2')) 
		GROUP BY CarID) as tmp1), 0) as [本日新上架車輛]

        --[本日下架車輛]--GridTrans.Type=201,CreateDate=今天 == 本日下架
        ,ISNULL((SELECT COUNT(1) FROM (SELECT CarID FROM [ABCCar].[dbo].[GridTrans] WITH (NOLOCK) WHERE [MemberID] = m.MemberID AND [GridTranTypeID] = 201  
		AND [CreateDate] BETWEEN '{end_date} 00:00:00' AND '{now} 00:00:00' AND CARID IN (SELECT carid from Car WITH (NOLOCK) where (ISNULL(Source, N' ') <> 'HOTV2')) 
		GROUP BY CarID) as tmp2), 0) as [本日下架車輛]

        --[本月新上架車輛]--GridTrans.Type=202,CreateDate=本月 == 本月新上
		--20240829 企劃提出 : 多出些奇怪的數字
		--修改邏輯原本只取得GridTranTypeID = 202資料 改為 先抓上架日+3天 小於下架日 的CarID 從CarID抓201及202互相抵消並且保留沒有被抵銷的202
        ,ISNULL((SELECT COUNT(1) FROM (SELECT CarID FROM [ABCCar].[dbo].[GridTrans] WITH (NOLOCK) WHERE [MemberID] = m.MemberID AND [GridTranTypeID] = 202
               AND ( CarID IN (SELECT CarID FROM [ABCCar].[dbo].[Car] WITH (NOLOCK) WHERE DATEADD(day, 3, StartDate) < EndDate 
			   AND MONTH(StartDate) = MONTH(GETDATE()) AND YEAR(StartDate) = YEAR(GETDATE()) AND (ISNULL(Source, N' ') <> 'HOTV2'))
               OR NOT EXISTS (SELECT 1 FROM [ABCCar].[dbo].[GridTrans] t2 WITH (NOLOCK) WHERE t2.MemberID = MemberID AND t2.CarID = CarID AND t2.GridTranTypeID = 201))
               AND CreateDate BETWEEN '{first_day} 00:00:00' AND '{nextMonth_first_day} 00:00:00'
             GROUP BY CarID
         ) as tmp3), 0) as [本月新上架車輛]

		--2日內下架
        ,ISNULL((SELECT COUNT(1) FROM (SELECT gt.CarID FROM [ABCCar].[dbo].[GridTrans] gt WITH (NOLOCK) LEFT JOIN [ABCCar].[dbo].[Car] c WITH (NOLOCK) ON gt.CarID = c.CarID WHERE gt.[MemberID] = m.MemberID AND gt.[GridTranTypeID] = 201 
		AND gt.[CreateDate] BETWEEN '{first_day} 00:00:00' AND '{nextMonth_first_day} 00:00:00' AND (ISNULL(c.Source, N' ') <> 'HOTV2') AND DATEDIFF(day, c.[StartDate], c.[EndDate])< 3 
		GROUP BY gt.CarID) as tmp4), 0) as [2日內下架]

        --[本月下架車輛]--GridTrans.Type=201,CreateDate=本月 == 本月下架
        ,ISNULL((SELECT COUNT(1) FROM (SELECT CarID FROM [ABCCar].[dbo].[GridTrans] WITH (NOLOCK) WHERE [MemberID] = m.MemberID AND [GridTranTypeID] = 201 
		AND [CreateDate] BETWEEN '{first_day} 00:00:00' AND '{nextMonth_first_day} 00:00:00' AND CARID IN (SELECT carid from Car WITH (NOLOCK) where (ISNULL(Source, N' ') <> 'HOTV2')) 
		GROUP BY CarID) as tmp4), 0) as [本月下架車輛]

		--,ISNULL((SELECT COUNT(1) FROM [ABCCar].[dbo].[Car] WITH (NOLOCK) WHERE [MemberID] = m.MemberID AND [Status] NOT IN (1,0) AND [TransferCheckId] <= 0 AND [EndDate] BETWEEN '{first_day} 00:00:00' AND '{nextMonth_first_day} 00:00:00'), 0) as [當月下架未過戶]
        --,ISNULL((SELECT COUNT(1) FROM [ABCCar].[dbo].[Car] WITH (NOLOCK) WHERE [MemberID] = m.MemberID AND [Status] = 1 AND [TransferCheckId] > 0 AND [VehicleNoCheckLastTime] BETWEEN '{first_day} 00:00:00' AND '{nextMonth_first_day} 00:00:00' ), 0) as [當月過戶未下架]
        --,ISNULL((SELECT COUNT(1) FROM [ABCCar].[dbo].[Car] WITH (NOLOCK) WHERE [MemberID] = m.MemberID AND [Status] NOT IN (1,0) AND [TransferCheckId] > 0 AND [VehicleNoCheckLastTime] BETWEEN '{first_day} 00:00:00' AND '{nextMonth_first_day} 00:00:00' AND [EndDate] BETWEEN '{first_day} 00:00:00' AND '{nextMonth_first_day} 00:00:00' ), 0) as [當月過戶當月下架]
       
	    --[架上車輛]--[Status] = 1 == 本月架上
        --,ISNULL((SELECT COUNT(1) FROM [ABCCar].[dbo].[Car] WITH (NOLOCK) WHERE [MemberID] = m.MemberID AND [Status] = 1 and GetDate() Between [Car].[StartDate] And [Car].[EndDate]), 0) as [本月架上車輛]
        ,ISNULL((
			SELECT COUNT(1) FROM ABCCar.dbo.Car c1 WITH (NOLOCK),ABCCar.dbo.CarModel carmodel WITH (NOLOCK) 
			 WHERE c1.[MemberID] = m.MemberID AND c1.[Status] = 1  AND GetDate() Between c1.[StartDate] And c1.[EndDate]
			       AND carmodel.IsEnable = 1 AND c1.BrandID = carmodel.BrandID AND c1.SeriesID = carmodel.SeriesID AND c1.CategoryID = carmodel.CategoryID AND (ISNULL(Source, N' ') <> 'HOTV2')
		 ), 0) as [本月架上車輛]
        
        --[車輛已驗證]--[Status] = 1 == 本月架上驗證成功
        --,ISNULL((SELECT COUNT(1) FROM [ABCCar].[dbo].[Car] WITH (NOLOCK) WHERE [MemberID] = m.MemberID AND [Status] = 1　AND [VehicleNoCheckStatus] = 1 AND GetDate() Between [Car].[StartDate] And [Car].[EndDate]), 0) as [本月架上驗證成功車輛]

        --[可收費新上架台數]*(車商1.2其它1.0)
        ,ISNULL((SELECT COUNT(1) FROM [ABCCar].[dbo].[vw_會員上架_Report] WHERE [MemberID] = m.MemberID 
		AND [StartDate]>='{first_day} 00:00:00' AND [StartDate]<'{nextMonth_first_day} 00:00:00' AND [ForFreeCampaignID]=0 AND [HOTCERNO]='' 
		AND [CarInStoreValid]=0), 0) as [可收費新上架台數]    --*(CASE m.[Category] WHEN 3 THEN 1.2 WHEN 4 THEN 1.2 ELSE 1.0 END)

        --[可收費實車在店台數]*1.5
        ,ISNULL((SELECT COUNT(1) FROM [ABCCar].[dbo].[vw_會員上架_Report] WHERE [MemberID] = m.MemberID 
		AND [StartDate]>='{first_day} 00:00:00' AND [StartDate]<'{nextMonth_first_day} 00:00:00' AND [ForFreeCampaignID]=0 AND [HOTCERNO]='' 
		AND [CarInStoreValid]=1), 0) as [可收費實車在店台數]   --*1.5

        --[HOT認証車無實車在店新上架台數]*0.5 
        ,ISNULL((SELECT COUNT(1) FROM [ABCCar].[dbo].[vw_會員上架_Report] WHERE [MemberID] = m.MemberID 
		AND [StartDate]>='{first_day} 00:00:00' AND [StartDate]<'{nextMonth_first_day} 00:00:00' AND [ForFreeCampaignID]=0 AND [HOTCERNO]!='' 
		AND [CarInStoreValid]=0), 0) as [HOT認証車新上架台數]  --*0.5

        --[活動專案新上架台數]*0.5
        ,ISNULL((SELECT COUNT(1) FROM [ABCCar].[dbo].[vw_會員上架_Report] WHERE [MemberID] = m.MemberID 
		AND [StartDate]>='{first_day} 00:00:00' AND [StartDate]<'{nextMonth_first_day} 00:00:00' 
		AND [ForFreeCampaignID]>0 AND [CarInStoreValid]=0), 0) as [活動專案新上架台數]  --*0.5

        --[專案上架(含HOT認証車)實車在店台數]*0.7
        ,(
            (ISNULL((SELECT COUNT(1) FROM [ABCCar].[dbo].[vw_會員上架_Report] WHERE [MemberID] = m.MemberID 
			AND [StartDate]>='{first_day} 00:00:00' AND [StartDate]<'{nextMonth_first_day} 00:00:00' 
			AND [ForFreeCampaignID]>0 AND [CarInStoreValid]=1), 0))  --*0.7
             +
            (ISNULL((SELECT COUNT(1) FROM [ABCCar].[dbo].[vw_會員上架_Report] WHERE [MemberID] = m.MemberID 
			AND [StartDate]>='{first_day} 00:00:00' AND [StartDate]<'{nextMonth_first_day} 00:00:00' AND [ForFreeCampaignID]=0 
			AND [HOTCERNO]!='' AND [CarInStoreValid]=1), 0))  --*0.7
        ) as [專案上架實車在店台數]               
       
		--[HOT認証車淨台數] 不乘業績
		,ISNULL((SELECT COUNT(1) FROM [ABCCar].[dbo].[vw_會員上架_Report] WHERE [MemberID] = m.MemberID 
		AND [StartDate]>='{first_day} 00:00:00' AND [StartDate]<'{nextMonth_first_day} 00:00:00' 
		AND [ForFreeCampaignID]=0 AND [HOTCERNO]!='' ),0) as [HOT認証車淨台數]

		--[公會專案淨台數] 不乘業績
		,ISNULL((SELECT COUNT(1) FROM [ABCCar].[dbo].[vw_會員上架_Report] WHERE [MemberID] = m.MemberID 
		AND [StartDate]>='{first_day} 00:00:00' AND [StartDate]<'{nextMonth_first_day} 00:00:00' 
		AND [ForFreeCampaignID]>0 ),0) as [公會專案淨台數]

         --[可收費車輛淨台數] 不乘業績
		,ISNULL((SELECT COUNT(1) FROM [ABCCar].[dbo].[vw_會員上架_Report] WHERE [MemberID] = m.MemberID 
		AND [StartDate]>='{first_day} 00:00:00' AND [StartDate]<'{nextMonth_first_day} 00:00:00' 
		AND [ForFreeCampaignID]=0 AND [HOTCERNO]='' ),0) as [可收費車輛淨台數]

        ,CASE WHEN ISNULL(ph.[ID], 0) > 0 THEN ph.CreateDate ELSE NULL END AS [最新儲值時間]
        ,bm.BindingDate as [信用卡綁定時間]
        ,ma.CreateDate as [同意會員條款時間]
        ,CONVERT(varchar(100), mc.[CreateDate], 120) AS [註冊時間]

        FROM [ABCCar].[dbo].Member m WITH (NOLOCK)
        LEFT JOIN [ABCCarMemberCenter].[dbo].[Member] mc WITH (NOLOCK) ON m.[MemberID] = mc.[ID]
        LEFT JOIN [ABCCar].[dbo].[MemberAgree] ma WITH (NOLOCK) ON m.[MemberID] = ma.[MemberID]
        --LEFT JOIN [ABCCar].[dbo].[BindingCardMember] bm WITH (NOLOCK) ON m.[MemberID] = bm.[MemberID] AND bm.[Status] in (0,99)
		LEFT JOIN (SELECT * FROM [ABCCar].[dbo].[BindingCardMember] sm WITH (NOLOCK) 
		           WHERE sm.[ID] = (SELECT Max(ID) FROM [ABCCar].[dbo].[BindingCardMember] sd WITH (NOLOCK) WHERE  sd.[MemberID]=sm.[MemberID] AND sd.[Status] in (0,99)) 
				  ) bm ON m.[MemberID] = bm.[MemberID] AND bm.[Status] in (0,99) 
        LEFT JOIN [ABCCar].[dbo].[MemberUserAccountSalesMapping] mua WITH (NOLOCK) ON m.[MemberID] = mua.[MemberID]
        LEFT JOIN [ABCCar].[dbo].[UserAccountSales] ua WITH (NOLOCK) ON ua.[ID] = mua.[UserAccountSalesID]
        LEFT JOIN [ABCCar].[dbo].[KeyValueDetail] kvd WITH (NOLOCK) ON kvd.Value = m.Guild AND kvd.KeyName = 'Guild'
        OUTER APPLY 
        (SELECT TOP 1 po.*
			FROM [ABCCar].[dbo].ProductOrder po WITH (NOLOCK) 
			LEFT JOIN [ABCCar].[dbo].ProductOrderItem poi WITH (NOLOCK) ON po.ID = poi.OrderID
			WHERE m.MemberID = po.MemberID 
			AND poi.ProductID = 1 
			AND po.OrderStatusID = 30
			AND po.PaymentStatusID IN (30, 40, 45)
        ) AS pd

        OUTER APPLY 
            (SELECT TOP 1 *
            FROM [ABCCar].[dbo].[PoolPointsHistory] ph1 with (NOLOCK)
            WHERE m.MemberID = ph1.MemberID AND ph1.RecordTypeID = 10
            ORDER BY ph1.ID DESC
        ) AS ph

        WHERE 
        (EXISTS
            (SELECT * FROM [ABCCar].[dbo].MemberAgree WITH (NOLOCK)
            WHERE [MemberID] = m.MemberID)
        OR EXISTS
            (SELECT * FROM [ABCCar].[dbo].[Car] car WITH (NOLOCK)
            JOIN [ABCCar].[dbo].[CarModel] carModel WITH (NOLOCK) ON carModel.[IsEnable] = 1 
                        AND car.[BrandID] = carModel.[BrandID] 
                        AND car.[SeriesID]= carModel.[SeriesID] 
                        AND car.[CategoryID]= carModel.[CategoryID]
            JOIN [ABCCar].[dbo].[Member] member WITH (NOLOCK)  ON car.[MemberID] = CAST(member.MemberID AS bigint)
                        AND member.[Status] = 1
                        AND (member.[Category] IN (2,3,5) OR member.[MemberID] IN (234160,232894)) --個人賣家、車商、經紀人、特定車商
            WHERE car.[Status] IN (0, 1) --待售
            AND car.[ModifyDate] >= '2018-06-20'
            AND car.MemberID = m.MemberID))

        AND m.[Category] > 1
        AND m.[Status] = 1
		--AND m.MemberID in (15593,3386) 
        --AND (bm.BindingDate IS NOT NULL OR ph.ID IS NOT NULL)
        AND ((bm.BindingDate IS NOT NULL OR ph.ID IS NOT NULL) OR (m.MemberID in (234160,232894) AND bm.BindingDate IS NULL))
		--AND m.MemberID = 204439
        ORDER BY m.MemberID
    """

    #HOT台數
    hot_sq = f"""
        DECLARE @HotCarBatchID INT
        SELECT @HotCarBatchID = Max(ID) FROM [ABCCar].[dbo].[HotCarBatch] WITH (NOLOCK)

        SELECT ACC,CERSTS INTO #ACC FROM abccar.dbo.HotCar WITH (NOLOCK) WHERE [HotCarBatchID] = @HotCarBatchID

        SELECT
            (SELECT COUNT(1) FROM #ACC B WHERE B.[ACC] = A.[HOTMemberID]) AS [HOT官網總台數]
            ,(SELECT COUNT(1) FROM #ACC C WHERE C.[ACC] = A.[HOTMemberID] AND C.CERSTS = 4) AS [HOT官網認證台數]
            ,[MemberID]
            ,[HOTMemberID] AS [HOT主帳號]
        FROM abccar.dbo.[HotCarMember] A WITH (NOLOCK)
        WHERE A.[IsMaster] = 1
        ORDER BY [HOT官網總台數] desc

        DROP TABLE #ACC
    """

    #每月業績目標
    target_sq = f"""
        select * from report.target
        where Date = '{first_day}'
        ORDER BY FIELD(ID, 102, 109, 101, 99, 116, 112, 119, 117, 1, 2, 3, 4, 5)
        --ORDER BY FIELD(ID, 102, 109, 120, 99, 116, 121, 119, 112, 117, 1, 2, 3, 4, 5)
    """

    #聯盟館本月台數
    #allianceThisMonth_sq = f"""
    #    select * from report.alliance
    #    where Date = '{end_date}'
    #"""
    allianceThisMonth_sq = f"""
        SELECT A.`Date` as `Date`,IFNULL(B.HotCarBatchID,0) as HotCarBatchID,IFNULL(B.Number,0) as Number  
        from (SELECT '{end_date}'  COLLATE utf8_general_ci  as `Date`) A 
        Left join report.alliance B on A.`Date`  COLLATE utf8_general_ci  = B.`Date`
    """

    #聯盟館上月底架上
    #allianceLastMonth_sq = f"""
    #    select * from report.alliance
    #    where Date = '{lastMonth_end_day}'
    #"""
    allianceLastMonth_sq = f"""
        SELECT A.`Date` as `Date`,IFNULL(B.HotCarBatchID,0) as HotCarBatchID,IFNULL(B.Number,0) as Number  
        from (SELECT '{lastMonth_end_day}'  COLLATE utf8_general_ci  as `Date`) A 
        Left join report.alliance B on A.`Date`  COLLATE utf8_general_ci  = B.`Date`
    """

    #CPO本月台數
    #cpoThisMonth_sq = f"""
    #    select * from report.cpo
    #    where Date = '{end_date}'
    #"""
    cpoThisMonth_sq = f"""
    select A.Date as Date,
    IFNULL(B.TCPO,0) as TCPO,
    IFNULL(B.LCPO,0) as LCPO,
    IFNULL(B.OtherCPO,0) as OtherCPO,
    IFNULL(B.Total,0) as Total
     from (SELECT '{end_date}'  COLLATE utf8_general_ci  as Date) A 
    Left join report.cpo B on A.Date  COLLATE utf8_general_ci  = B.Date
    """

    #CPO上月底架上台數
    #cpoLastMonth_sq = f"""
    #    select Total 上月底架上台數 from report.cpo
    #    where Date = '{lastMonth_end_day}'
    #"""
    cpoLastMonth_sq = f"""
    select IFNULL(B.Total,0) as 上月底架上台數 
     from (SELECT '{lastMonth_end_day}' COLLATE utf8_general_ci  as Date) A 
    Left join report.cpo B on A.Date COLLATE utf8_general_ci  = B.Date 
    """

    # 物件總瀏覽數
    query1 = f"""
        select substr(date, 1, 10) Date, sum(totalEvents) from report.ga_main
        where eventAction like '/car/' and date >= '{end_date}' and date <= '{now}' group by date
    """

    # 網站整體成效(總瀏覽數, 到站人次, 到站新客)
    query2 = f"""
        select a.QueryDate Date, a.visitor, a.New_visitor, a.all from CollectGA.ga_visitor_report_new a
        where QueryDate >= '{end_date}' and 
        QueryDate <= '{now}'
    """

    #昨日總上架數
    #query3 = f"""
    #    select date Date,  count(*) Count from report.status_new
    #    where date = '{end_date}' group by date
    #"""
    # query3 = f"""
    #     SELECT A.`Date` as `Date`,
    #     (SELECT count(`Date`) FROM report.status_new B WHERE A.`Date` = B.date ) as Count
    #     FROM (SELECT '{end_date}' COLLATE utf8_general_ci as `Date`) A 
    # """
    query3 = f"""
        SELECT `CarID` as CarID 
        FROM report.`status_new` WHERE `Date` = (SELECT '{end_date}' COLLATE utf8_general_ci as `Date`) 
    """
    
    #上月總上架數
    #query4 = f"""
    #    select date Date,  count(*) Count from report.status_new
    #    where date = '{lastMonth_end_day}' group by date
    #"""
    query4 = f"""
        SELECT A.`Date` as `Date`,
        (SELECT count(`Date`) FROM report.status_new B WHERE A.`Date` = B.date ) as Count
        FROM (SELECT '{lastMonth_end_day}' COLLATE utf8_general_ci as `Date`) A 
    """
    
    #昨日上架數,依區域&架上天數分類(上架天數)
    query5 = f"""
        SELECT Date,ZoneID,DayType,Count(CarID) as CarNum FROM ( 
            SELECT Date,ZoneID,CarID, (CASE WHEN OnlineDays<=30 THEN 1 
                                       WHEN OnlineDays<=60 THEN 2 
                                       WHEN OnlineDays<=90 THEN 3 
                                       WHEN OnlineDays<=180 THEN 4
                                       WHEN OnlineDays<=360 THEN 5 
                                       ELSE 6 END) AS DayType 
            FROM `status_new_detail` WHERE Date=(SELECT max(Date) FROM status_new_detail) and Category<>'4' 
        ) m group by Date,ZoneID,DayType
    """
    
    #當日過戶收費
    #當日過戶收費金額
    #q5 = f"""
    #SELECT 
    #    --ISNULL(SUM(cto.OrderTotal)-SUM(cto.RefundedAmount), 0) as [當日過戶收費],
	#    --COUNT(*) as [當日訂單筆數]
    #    ISNULL(SUM(cto.OrderTotal)-SUM(cto.RefundedAmount), 0) as [當日過戶收費金額],
    #    COUNT(*) as [當日過戶台數]
    #FROM [ABCCar].[dbo].CarTransferOrder cto WITH (NOLOCK) 
    #WHERE cto.OrderStatusID = 30 AND cto.PaymentStatusID IN (30, 40, 45) AND cto.OrderTotal > 0
    #AND cto.CreateDate >= '{end_date} 00:00:00'
    #AND cto.CreateDate < '{now} 00:00:00'
    #"""
    
    q5 = f"""
    SELECT 
        --ISNULL(SUM(cto.OrderTotal)-SUM(cto.RefundedAmount), 0) as [當日過戶收費],
	    --COUNT(*) as [當日訂單筆數]
        ISNULL(SUM(ctoi.Price), 0) as [當日過戶收費金額],
        COUNT(*) as [當日過戶台數]
    FROM [ABCCar].[dbo].CarTransferOrder cto WITH (NOLOCK) 
	LEFT JOIN abccar.dbo.CarTransferOrderItem ctoi WITH (NOLOCK) on cto.Id = ctoi.OrderId
	LEFT JOIN [ABCCar].[dbo].Member m WITH (NOLOCK) ON m.MemberID = cto.MemberID
    LEFT JOIN [ABCCar].[dbo].[MemberUserAccountSalesMapping] mua WITH (NOLOCK) ON m.[MemberID] = mua.[MemberID]
    LEFT JOIN [ABCCar].[dbo].[UserAccountSales] ua WITH (NOLOCK) ON ua.[ID] = mua.[UserAccountSalesID]
	LEFT JOIN [ABCCar].[dbo].CarTransferCheckSuccess b1 WITH (NOLOCK) ON ctoi.OrderID = b1.OrderId AND b1.CarId = ctoi.CarID AND b1.CreateDate BETWEEN '{end_date} 00:00:00' AND '{now} 00:00:00'
    WHERE cto.OrderStatusID = 30 AND cto.PaymentStatusID IN (30, 40, 45) AND cto.OrderTotal > 0
	AND cto.PaidDateUtc BETWEEN '{end_date} 00:00:00' AND '{now} 00:00:00'
    AND cto.CreateDate BETWEEN '{end_date} 00:00:00' AND '{now} 00:00:00'
    AND ua.ID is not null AND ua.ID not in ('88')   -- 過濾暫不分配
    AND ctoi.Price not in ('0')
	AND cto.PaidDateUtc is not null
    """

    #當月過戶收費
    #當月累計過戶金額
    #q6 = f"""
    #SELECT 
    #    --ISNULL(SUM(cto.OrderTotal)-SUM(cto.RefundedAmount), 0) as [當月過戶收費],
	#    --COUNT(*) as [當月訂單筆數]
    #    ISNULL(SUM(cto.OrderTotal)-SUM(cto.RefundedAmount), 0) as [當月累計過戶金額],
    #    COUNT(*) as [當月累計過戶台數]
    #FROM [ABCCar].[dbo].CarTransferOrder cto WITH (NOLOCK) 
    #WHERE cto.OrderStatusID = 30 AND cto.PaymentStatusID IN (30, 40, 45) AND cto.OrderTotal > 0
    #AND cto.CreateDate >= '{first_day} 00:00:00'
    #AND cto.CreateDate < '{nextMonth_first_day} 00:00:00'
    #"""
    
    q6 = f"""
    SELECT 
        --ISNULL(SUM(cto.OrderTotal)-SUM(cto.RefundedAmount), 0) as [當月過戶收費],
	    --COUNT(*) as [當月訂單筆數]
        ISNULL(SUM(ctoi.Price), 0) as [當月累計過戶金額],
        COUNT(*) as [當月累計過戶台數]
    FROM [ABCCar].[dbo].CarTransferOrder cto WITH (NOLOCK) 
	LEFT JOIN abccar.dbo.CarTransferOrderItem ctoi WITH (NOLOCK) on cto.Id = ctoi.OrderId
	LEFT JOIN [ABCCar].[dbo].Member m WITH (NOLOCK) ON m.MemberID = cto.MemberID
    LEFT JOIN [ABCCar].[dbo].[MemberUserAccountSalesMapping] mua WITH (NOLOCK) ON m.[MemberID] = mua.[MemberID]
    LEFT JOIN [ABCCar].[dbo].[UserAccountSales] ua WITH (NOLOCK) ON ua.[ID] = mua.[UserAccountSalesID]
	LEFT JOIN [ABCCar].[dbo].CarTransferCheckSuccess b1 WITH (NOLOCK) ON ctoi.OrderID = b1.OrderId AND b1.CarId = ctoi.CarID AND b1.CreateDate BETWEEN '{first_day} 00:00:00' AND '{nextMonth_first_day} 00:00:00'
    WHERE cto.OrderStatusID = 30 AND cto.PaymentStatusID IN (30, 40, 45) AND cto.OrderTotal > 0
	AND cto.PaidDateUtc BETWEEN '{first_day} 00:00:00' AND '{nextMonth_first_day} 00:00:00'
    AND cto.CreateDate BETWEEN '{first_day} 00:00:00' AND '{nextMonth_first_day} 00:00:00'
	AND ua.ID is not null AND ua.ID not in ('88')   -- 過濾暫不分配
	AND ctoi.Price not in ('0')
	AND cto.PaidDateUtc is not null
    """

    #當日應收催收款
    #q7 = f"""
    #SELECT 
    #    ISNULL(SUM(cto.OrderTotal), 0) as [當日應收催收款],
	#    COUNT(*) as [當日訂單筆數]
    #FROM [ABCCar].[dbo].CarTransferOrder cto WITH (NOLOCK) 
    #WHERE cto.OrderStatusID = 10 AND cto.PaymentStatusID = 10 AND cto.OrderTotal > 0
    #AND cto.CreateDate >= '{end_date} 00:00:00'
    #AND cto.CreateDate < '{now} 00:00:00'
    #"""

    #20240903改為已過戶但未繳費催收款
    q7 = f"""
    SELECT 
    SUM([當日應收催收款]) AS [當日應收催收款],
    COUNT(*) AS [當日訂單筆數]
FROM (
    SELECT DISTINCT
        cto.[ID],
        m.[MemberID],
        ctoi.Price AS [當日應收催收款]
    FROM [ABCCar].[dbo].[CarTransferOrder] cto
    LEFT JOIN abccar.dbo.CarTransferOrderItem ctoi WITH (NOLOCK) ON cto.Id = ctoi.OrderId
    LEFT JOIN [ABCCar].[dbo].Member m WITH (NOLOCK) ON m.MemberID = cto.MemberID
    LEFT JOIN [ABCCar].[dbo].[MemberUserAccountSalesMapping] mua WITH (NOLOCK) ON cto.MemberID = mua.[MemberID]
    LEFT JOIN [ABCCar].[dbo].[UserAccountSales] ua WITH (NOLOCK) ON ua.[ID] = mua.[UserAccountSalesID]
    LEFT JOIN [ABCCar].[dbo].CarTransferCheckSuccess b1 WITH (NOLOCK) ON ctoi.OrderID = b1.OrderId AND b1.CarId = ctoi.CarID AND b1.CreateDate BETWEEN '{first_day} 00:00:00' AND '{nextMonth_first_day} 00:00:00'
    WHERE cto.OrderStatusID = 10 
      AND cto.PaymentStatusID = 10 
      AND cto.OrderTotal > 0
      AND cto.CreateDate BETWEEN '{end_date} 00:00:00' AND '{now} 00:00:00'
      AND ua.ID IS NOT NULL 
      AND ua.ID NOT IN ('88')
      AND ctoi.Price NOT IN ('0')
) AS DistinctResults
    """

    #當月應收催收款
    #q8 = f"""
    #SELECT 
    #    ISNULL(SUM(cto.OrderTotal), 0) as [當月應收催收款],
	#    COUNT(*) as [當月訂單筆數]
    #FROM [ABCCar].[dbo].CarTransferOrder cto WITH (NOLOCK) 
    #WHERE cto.OrderStatusID = 10 AND cto.PaymentStatusID = 10 AND cto.OrderTotal > 0
    #AND cto.CreateDate >= '{first_day} 00:00:00'
    #AND cto.CreateDate < '{nextMonth_first_day} 00:00:00'
    #"""
    
    #20240903改為已過戶但未繳費催收款
    q8 = f"""
SELECT 
    SUM([當月應收催收款]) AS [當月應收催收款],
    COUNT(*) AS [當月訂單筆數]
FROM (
    SELECT DISTINCT
        cto.[ID],
        m.[MemberID],
        ctoi.Price AS [當月應收催收款]
    FROM [ABCCar].[dbo].[CarTransferOrder] cto
    LEFT JOIN abccar.dbo.CarTransferOrderItem ctoi WITH (NOLOCK) ON cto.Id = ctoi.OrderId
    LEFT JOIN [ABCCar].[dbo].Member m WITH (NOLOCK) ON m.MemberID = cto.MemberID
    LEFT JOIN [ABCCar].[dbo].[MemberUserAccountSalesMapping] mua WITH (NOLOCK) ON cto.MemberID = mua.[MemberID]
    LEFT JOIN [ABCCar].[dbo].[UserAccountSales] ua WITH (NOLOCK) ON ua.[ID] = mua.[UserAccountSalesID]
    LEFT JOIN [ABCCar].[dbo].CarTransferCheckSuccess b1 WITH (NOLOCK) ON ctoi.OrderID = b1.OrderId AND b1.CarId = ctoi.CarID AND b1.CreateDate BETWEEN '{first_day} 00:00:00' AND '{nextMonth_first_day} 00:00:00'
    WHERE cto.OrderStatusID = 10 
      AND cto.PaymentStatusID = 10 
      AND cto.OrderTotal > 0
      AND cto.CreateDate BETWEEN '{first_day} 00:00:00' AND '{nextMonth_first_day} 00:00:00'
      AND ua.ID IS NOT NULL 
      AND ua.ID NOT IN ('88')
      AND ctoi.Price NOT IN ('0')
) AS DistinctResults
    """

    #當月購買認証廣宣
    q9 = f"""
        SELECT us.NameSeq,us.ID
        --,od.OrderYM
        ,CONVERT(varchar(7),GETDATE()-1,120) as OrderYM
        ,(RIGHT(REPLICATE('0', 2) + CAST(us.NameSeq as NVARCHAR),2) + ' ' + Name) AS [維護業務]
        ,ISNULL(od.[HAA],0) AS [購買認証]
        ,ISNULL(od.[置頂],0) AS [置頂]
        ,ISNULL(od.[更新],0) AS [更新]
        ,ISNULL(od.[全面曝光],0) AS  [全面曝光]
        ,ISNULL(od.[BANNER曝光],0) AS  [BANNER曝光]
        FROM UserAccountSales AS us WITH (NOLOCK)
        LEFT JOIN (

            SELECT * FROM
            (
            SELECT ISNULL(ua.[ID], '') as SaleID,--CONVERT(varchar,mb.ProductTypeId) ProductTypeId,
            CONVERT(varchar(7),mc.CreateDate,120) as OrderYM,
            (CASE WHEN ProductTypeId='2' THEN 'HAA' 
            WHEN ProductTypeId='6' THEN '置頂' 
            WHEN ProductTypeId='7' THEN '更新' 
            WHEN ProductTypeId='8' THEN '全面曝光' 
            WHEN ProductTypeId='9' THEN 'BANNER曝光' 
            ELSE '' END) as ProductType
            ,mc.OrderTotal as OrderTotal
            --,sum(mc.OrderTotal) as OrderTotal
            FROM ProductOrder mc WITH (NOLOCK)
            LEFT JOIN [ABCCar].[dbo].[MemberUserAccountSalesMapping] mua WITH (NOLOCK) ON mc.[MemberID] = mua.[MemberID]
            LEFT JOIN [ABCCar].[dbo].[UserAccountSales] ua WITH (NOLOCK) ON ua.[ID] = mua.[UserAccountSalesID]
            , (
            SELECT k.OrderID,p.Name as 'ProductName',p.ID,p.ProductTypeId FROM ProductOrderItem k WITH (NOLOCK), Product p WITH (NOLOCK)
            WHERE k.Price>0 
            and k.ID = (SELECT MAX(t.ID) FROM ProductOrderItem t where t.OrderID=k.OrderID and t.Price>0 )
            and k.ProductID=p.ID 
            --2:HAA認證書,6:置頂,7:更新,8:全面曝光,9:BANNER曝光
            and p.ProductTypeId in (2,6,7,8,9)
            ) mb 
            WHERE mc.OrderStatusID=30 AND mc.PaymentStatusID=30 and mc.OrderTotal>0
            and mc.ID=mb.OrderID 
            and mc.CreateDate >= '{first_day} 00:00:00' 
            and mc.CreateDate < '{nextMonth_first_day} 00:00:00'
            --and CONVERT(varchar(7),mc.CreateDate,120)=CONVERT(varchar(7),GETDATE()-1,120)
            --Group by ISNULL(ua.[ID], '') ,mb.ProductTypeId
            --Order by 1
            ) mas
             PIVOT
            (SUM(OrderTotal) FOR ProductType in (HAA,置頂,更新,全面曝光,BANNER曝光)) AS PivotTable

        ) as od on us.ID=od.SaleID
        WHERE us.Type='2' and us.Status=1 and us.ZoneName between 1 and 5
        ORDER BY 1"""
    
    #各業務當月過戶實際台數及實際收到金額
    q10 = f"""
        SELECT 
		ua.NameSeq as [維護業務序號]
        ,ua.Name as [維護業務]
        ,ua.ID as [維護業務ID]
        ,coi.UnitPrice as [單價]
        ,coi.UnitPrice - coi.Price AS [折扣]
        ,coi.RefundPrice AS [退刷]
        ,coi.Price AS [當日過戶收費金額]
    from abccar.dbo.CarTransferOrderItem coi WITH (NOLOCK) 
    LEFT JOIN abccar.dbo.CarTransferOrder b WITH (NOLOCK) ON b.Id = coi.OrderId
    LEFT JOIN [ABCCar].[dbo].Member m WITH (NOLOCK) ON m.MemberID = b.MemberID
    LEFT JOIN [ABCCar].[dbo].[MemberUserAccountSalesMapping] mua WITH (NOLOCK) ON m.[MemberID] = mua.[MemberID]
    LEFT JOIN [ABCCar].[dbo].[UserAccountSales] ua WITH (NOLOCK) ON ua.[ID] = mua.[UserAccountSalesID]
    LEFT JOIN [ABCCar].[dbo].Car c WITH (NOLOCK) ON c.CarId = coi.CarId
    LEFT JOIN [ABCCar].[dbo].CarModel cm WITH (NOLOCK) ON cm.BrandID = c.BrandID AND cm.SeriesID = c.SeriesID AND cm.CategoryID = c.CategoryID
    LEFT JOIN [ABCCar].[dbo].CarTransferCheckSuccess b1 WITH (NOLOCK) ON coi.OrderID=b1.OrderId and b1.CarId = coi.CarID and b1.CreateDate between '{first_day} 00:00:00' AND '{nextMonth_first_day} 00:00:00'
    --where b1.CreateDate between '{first_day} 00:00:00' AND '{nextMonth_first_day} 00:00:00'
    where b.CreateDate between '{first_day} 00:00:00' AND '{nextMonth_first_day} 00:00:00'  --20240828 企劃提出只顯示當月過戶且已付款
    and coi.Price not in ('0')
    and b.PaidDateUtc is not null --付款日期
    order by ua.NameSeq ASC
    """

    #執行讀取程式
    # MSSQL
    memberAgree = read_test_mssql(memberAgree_sg, 'ABCCar')
    hotDF = read_test_mssql(hot_sq, 'ABCCar')
    q5rtn = read_test_mssql(q5, 'ABCCar')
    q6rtn = read_test_mssql(q6, 'ABCCar')
    q7rtn = read_test_mssql(q7, 'ABCCar')
    q8rtn = read_test_mssql(q8, 'ABCCar')
    q9rtn = read_test_mssql(q9, 'ABCCar')
    q10rtn = read_test_mssql(q10, 'ABCCar')

    # MySQL
    target = read_mysql(target_sq, 'report')
    aTM = read_mysql(allianceThisMonth_sq, 'report')
    aLM = read_mysql(allianceLastMonth_sq, 'report')
    cTM = read_mysql(cpoThisMonth_sq, 'report')
    cLM = read_mysql(cpoLastMonth_sq, 'report')
    #GACar = read_mysql(query1, 'report')
    #GAWeb = read_mysql(query2, 'report')
    allCar = read_mysql(query3, 'report')
    allCarLM = read_mysql(query4, 'report')
    allCarDetail = read_mysql(query5, 'report')
    
    df_hot = pd.merge(memberAgree, hotDF, on="MemberID", how='left')

    #Check data length
    #print(len(df_hot))
    #確保資料只有唯一的memberID，刪除重複memberID，只保留一筆
    df_hot = df_hot.drop_duplicates(subset=['MemberID'])
    #Check data length
    #print(len(df_hot))

    #(暫時加0815) start
    #當月HOT拋轉車
    tmpSql = f"""    
        SELECT CARID as CarID
        FROM [ABCCar].[dbo].CAR WITH (NOLOCK)
        WHERE (ISNULL(Source, N' ') = 'HOTV2') AND [Status] = 1
        --AND [StartDate]>='{first_day} 00:00:00' AND [StartDate]<'{nextMonth_first_day} 00:00:00'
    """
    TmpHOTV2 = read_test_mssql(tmpSql, 'ABCCar') 
    # allCar排除TmpHOTV2  
    res = list(set(allCar['CarID']).difference(set(TmpHOTV2['CarID'])))
    allCar = {"Count": len(res)}
    #(暫時加0815) end

    #return df_hot, target, aTM, aLM, cTM, cLM, GACar, GAWeb, allCar, allCarLM, allCarDetail, q5rtn, q6rtn, q7rtn, q8rtn
    return df_hot, target, aTM, aLM, cTM, cLM, allCar, allCarLM, allCarDetail, q5rtn, q6rtn, q7rtn, q8rtn, q9rtn, q10rtn

# 讀取暫存的上月底台數檔案
def addStageLastMonth(df):
    StageLastMonth = pd.read_csv('stageLastMonth_'+ str(lastMonth_end_day) +'.csv') 
    
    tmp = StageLastMonth[['MemberID','上月底架上台數']]
    tmp = tmp.drop_duplicates(subset=['MemberID'])

    mergeDf = pd.merge(df, tmp, on="MemberID", how='left')
    mergeDf['上月底架上台數'] = mergeDf['上月底架上台數'].fillna(0)
    
    return mergeDf

# 總表運算
def processData(df, target, aTM, aLM, cTM, cLM, allCar, allCarLM, allCarDetail, df9, df10):
    
    # ** 重要須注意 (日報上業務序號有異動增或減都需要調整序號)
    # ** salesNumber 須與 [ABCCar].[dbo].[UserAccountSales] 的欄位 [NameSeq] (維護業務序號) 相同
    # ** ID in (102, 109, 101, 99, 116, 112, 117)
    
    # 用序號篩選業務
    salesNumber = [1, 2, 3, 4, 5, 6, 7, 8]
    #salesNumber = [1, 2, 3, 4, 5, 6, 7, 8, 9]

    df = df.loc[df['維護業務序號'].isin(salesNumber)]
    df10 = df10.loc[df10['維護業務序號'].isin(salesNumber)]
    
    #TOP table---------start

    #本月台數 20220304新增
    #本月未分配 = mysql總和 - 車商 - 原廠
    a = int(allCar['Count']) - int(sum(df['本月架上車輛'])) - int(cTM['Total'])
    #上月未分配 = mysql總和 - 車商 - 原廠
    b = int(allCarLM['Count']) - int(sum(df['上月底架上台數'])) - int(cLM['上月底架上台數'])

    carThisMonth = []
    carThisMonth.append(int(sum(df['本月架上車輛']))) #車商
    carThisMonth.append(a) #未分配 
    carThisMonth.append(int(aTM['Number'])) #聯盟館
    carThisMonth.append(int(cTM['Total'])) #原廠館
    carThisMonth.append(int(allCar['Count']) + int(aTM['Number'])) #總和 = mysql總和 + 聯盟館

    #上月底架上台數 20220304新增
    carLastMonth = []
    #carLastMonth.append(int(sum(df['上月底架上台數']))) #車商
    #carLastMonth.append(b) #未分配 = mysql總和 - 車商 - 原廠
    #carLastMonth.append(int(aLM['Number'])) #聯盟館
    #carLastMonth.append(int(cLM['上月底架上台數'])) #原廠館
    #carLastMonth.append(int(allCarLM['Count']) + int(aLM['Number'])) #總和
    carLastMonth.append(38052) #車商   20240901暫時新增
    carLastMonth.append(419) #未分配 = mysql總和 - 車商 - 原廠
    carLastMonth.append(7317) #聯盟館
    carLastMonth.append(1258) #原廠館
    carLastMonth.append(47046) #總和

    #淨增加 20220304新增
    carIncrease = []
    #carIncrease.append(int(sum(df['本月架上車輛'])) - int(sum(df['上月底架上台數']))) #車商
    #carIncrease.append(a - b)
    #carIncrease.append(int(aTM['Number']) - int(aLM['Number'])) #聯盟館
    #carIncrease.append(int(cTM['Total']) - int(cLM['上月底架上台數'])) #原廠館
    #carIncrease.append(sum(carIncrease)) #總和
    carIncrease.append(int(sum(df['本月架上車輛'])) - 38052) #車商   20240901暫時新增
    carIncrease.append(a - 419)
    carIncrease.append(int(aTM['Number']) - 7317) #聯盟館
    carIncrease.append(int(cTM['Total']) - 1258) #原廠館
    carIncrease.append(sum(carIncrease)) #總和

    #車商本月新上架
    carThisMonthNew = [] 
    carThisMonthNew.append(sum(df['本月新上(新)'])) 
    #車商本月下架
    carThisMonthOff = []
    carThisMonthOff.append(sum(df['本月下架車輛']))

    #TOP table---------end

    # 業務 ID
    salesID = [102, 109, 101, 99, 116, 112, 119, 117]
    #salesID = [102, 109, 120, 99, 116, 121, 119, 112, 117]
    # TL ID (報表定義，未寫入abc的DB)
    TLID = [1, 2, 3, 4, 5]

    #業務新上架目標台數
    #targetSalesUpload = target.loc[target['ID'].isin(salesID)]['UploadTarget'].tolist()
    targetSalesUpload = target.loc[target['ID'].isin(salesID)]['TransferTarget'].tolist()
    targetSalesUpload.append(sum(targetSalesUpload))
    #業務淨增加目標台數
    targetSalesIncrease = target.loc[target['ID'].isin(salesID)]['IncreaseTarget'].tolist()
    targetSalesIncrease.append(sum(targetSalesIncrease))
    #業務廣宣銷售目標
    targetSalesPromote = target.loc[target['ID'].isin(salesID)]['PromoteTarget'].tolist()
    targetSalesPromote.append(sum(targetSalesPromote))

    #TL新上架目標台數
    #targetTLUpload = target.loc[target['ID'].isin(TLID)]['UploadTarget'].tolist()
    targetTLUpload = target.loc[target['ID'].isin(TLID)]['TransferTarget'].tolist()
    targetTLUpload.append(sum(targetTLUpload))
    #TL淨增加目標台數
    targetTLIncrease = target.loc[target['ID'].isin(TLID)]['IncreaseTarget'].tolist()
    targetTLIncrease.append(sum(targetTLIncrease))

    #新上架目標台數
    # NewUploadTarget = [350, 250, 950, 350, 280, 350, 250, 200, 350, 500, 500, 4330]
    NewUploadTarget = targetSalesUpload

    PromoteTarget = targetSalesPromote
    
    tmpA = df.loc[(df['維護業務ID'] == 102), :] #邱雅凌
    tmpB = df.loc[(df['維護業務ID'] == 109), :] #林賢宏
    tmpC = df.loc[(df['維護業務ID'] == 101), :] #童嘉如
    #tmpC = df.loc[(df['維護業務ID'] == 120), :] #朱紜庭
    tmpD = df.loc[(df['維護業務ID'] == 99), :]  #黃煒凱
    tmpE = df.loc[(df['維護業務ID'] == 116), :] #吳美姍
    tmpF = df.loc[(df['維護業務ID'] == 112), :] #林心沛
    tmpG = df.loc[(df['維護業務ID'] == 119), :] #李蒨盈
    tmpH = df.loc[(df['維護業務ID'] == 117), :] #陳湘月
    #tmpF = df.loc[(df['維護業務ID'] == 121), :] #莊子琳
    #tmpG = df.loc[(df['維護業務ID'] == 119), :] #李蒨盈
    #tmpH = df.loc[(df['維護業務ID'] == 112), :] #林心沛
    #tmpI = df.loc[(df['維護業務ID'] == 117), :] #陳湘月
    
    tmpdf10A = df10.loc[(df10['維護業務ID'] == 102), :] #邱雅凌
    tmpdf10B = df10.loc[(df10['維護業務ID'] == 109), :] #林賢宏
    tmpdf10C = df10.loc[(df10['維護業務ID'] == 101), :] #童嘉如
    #tmpdf10C = df10.loc[(df10['維護業務ID'] == 120), :] #朱紜庭
    tmpdf10D = df10.loc[(df10['維護業務ID'] == 99), :]  #黃煒凱
    tmpdf10E = df10.loc[(df10['維護業務ID'] == 116), :] #吳美姍
    tmpdf10F = df10.loc[(df10['維護業務ID'] == 112), :] #林心沛
    tmpdf10G = df10.loc[(df10['維護業務ID'] == 119), :] #李蒨盈
    tmpdf10H = df10.loc[(df10['維護業務ID'] == 117), :] #陳湘月
    #tmpdf10F = df10.loc[(df10['維護業務ID'] == 121), :] #莊子琳
    #tmpdf10G = df10.loc[(df10['維護業務ID'] == 119), :] #李蒨盈
    #tmpdf10H = df10.loc[(df10['維護業務ID'] == 112), :] #林心沛
    #tmpdf10I = df10.loc[(df10['維護業務ID'] == 117), :] #陳湘月
    
    tmpAPromote = df9.loc[(df9['ID'] == 102), :] #邱雅凌
    tmpBPromote = df9.loc[(df9['ID'] == 109), :] #林賢宏
    tmpCPromote = df9.loc[(df9['ID'] == 101), :] #童嘉如
    #tmpCPromote = df9.loc[(df9['ID'] == 120), :] #朱紜庭
    tmpDPromote = df9.loc[(df9['ID'] == 99), :]  #黃煒凱
    tmpEPromote = df9.loc[(df9['ID'] == 116), :] #吳美姍
    tmpFPromote = df9.loc[(df9['ID'] == 112), :] #林心沛
    tmpGPromote = df9.loc[(df9['ID'] == 119), :] #李蒨盈
    tmpHPromote = df9.loc[(df9['ID'] == 117), :] #陳湘月
    #tmpFPromote = df9.loc[(df9['ID'] == 121), :] #莊子琳
    #tmpGPromote = df9.loc[(df9['ID'] == 119), :] #李蒨盈
    #tmpHPromote = df9.loc[(df9['ID'] == 112), :] #林心沛
    #tmpIPromote = df9.loc[(df9['ID'] == 117), :] #陳湘月
    
    #過戶收費實績台數
    NewUploadAct = []

    #NewUploadAct.append((tmpA.sum()['可收費新上架台數'] + tmpA.sum()['可收費實車在店台數'] + tmpA.sum()['HOT認証車新上架台數'] + tmpA.sum()['活動專案新上架台數'] + tmpA.sum()['專案上架實車在店台數']).round(0))
    #NewUploadAct.append((tmpB.sum()['可收費新上架台數'] + tmpB.sum()['可收費實車在店台數'] + tmpB.sum()['HOT認証車新上架台數'] + tmpB.sum()['活動專案新上架台數'] + tmpB.sum()['專案上架實車在店台數']).round(0))
    #NewUploadAct.append((tmpC.sum()['可收費新上架台數'] + tmpC.sum()['可收費實車在店台數'] + tmpC.sum()['HOT認証車新上架台數'] + tmpC.sum()['活動專案新上架台數'] + tmpC.sum()['專案上架實車在店台數']).round(0))
    #NewUploadAct.append((tmpD.sum()['可收費新上架台數'] + tmpD.sum()['可收費實車在店台數'] + tmpD.sum()['HOT認証車新上架台數'] + tmpD.sum()['活動專案新上架台數'] + tmpD.sum()['專案上架實車在店台數']).round(0))
    #NewUploadAct.append((tmpE.sum()['可收費新上架台數'] + tmpE.sum()['可收費實車在店台數'] + tmpE.sum()['HOT認証車新上架台數'] + tmpE.sum()['活動專案新上架台數'] + tmpE.sum()['專案上架實車在店台數']).round(0))
    #NewUploadAct.append((tmpF.sum()['可收費新上架台數'] + tmpF.sum()['可收費實車在店台數'] + tmpF.sum()['HOT認証車新上架台數'] + tmpF.sum()['活動專案新上架台數'] + tmpF.sum()['專案上架實車在店台數']).round(0))
    #NewUploadAct.append((tmpG.sum()['可收費新上架台數'] + tmpG.sum()['可收費實車在店台數'] + tmpG.sum()['HOT認証車新上架台數'] + tmpG.sum()['活動專案新上架台數'] + tmpG.sum()['專案上架實車在店台數']).round(0))

    NewUploadAct.append(tmpdf10A.count()['維護業務ID'])
    NewUploadAct.append(tmpdf10B.count()['維護業務ID'])
    NewUploadAct.append(tmpdf10C.count()['維護業務ID'])
    NewUploadAct.append(tmpdf10D.count()['維護業務ID'])
    NewUploadAct.append(tmpdf10E.count()['維護業務ID'])
    NewUploadAct.append(tmpdf10F.count()['維護業務ID'])
    NewUploadAct.append(tmpdf10G.count()['維護業務ID'])
    NewUploadAct.append(tmpdf10H.count()['維護業務ID'])
    #NewUploadAct.append(tmpdf10I.count()['維護業務ID'])
    
    totalA = 0
    for x in NewUploadAct:
        #print(x)
        totalA = totalA + x
        
    NewUploadAct.append(totalA) #總和

    #新上架達成率
    NewUploadAR = []
    for i, v in enumerate(NewUploadTarget):
        NewUploadAR.append(NewUploadAct[i]/v)

    #過戶收費金額
    NewUploadMoney = []
    
    NewUploadMoney.append(int((tmpdf10A["當日過戶收費金額"]).sum().round(0)))
    NewUploadMoney.append(int((tmpdf10B["當日過戶收費金額"]).sum().round(0)))
    NewUploadMoney.append(int((tmpdf10C["當日過戶收費金額"]).sum().round(0)))
    NewUploadMoney.append(int((tmpdf10D["當日過戶收費金額"]).sum().round(0)))
    NewUploadMoney.append(int((tmpdf10E["當日過戶收費金額"]).sum().round(0)))
    NewUploadMoney.append(int((tmpdf10F["當日過戶收費金額"]).sum().round(0)))
    NewUploadMoney.append(int((tmpdf10G["當日過戶收費金額"]).sum().round(0)))
    NewUploadMoney.append(int((tmpdf10H["當日過戶收費金額"]).sum().round(0)))
    #NewUploadMoney.append(int((tmpdf10I["當日過戶收費金額"]).sum().round(0)))
    
    totalMoney = 0
    for x in NewUploadMoney:
        #print(x)
        totalMoney = totalMoney + x

    NewUploadMoney.append(totalMoney) #總和
    
    #過戶台數
    NewUploadCount = []
    NewUploadCount.append(tmpdf10A.count()['維護業務ID']) #邱雅凌
    NewUploadCount.append(tmpdf10B.count()['維護業務ID']) #林賢宏
    NewUploadCount.append(tmpdf10C.count()['維護業務ID']) #童嘉如
    #NewUploadCount.append(tmpdf10C.count()['維護業務ID']) #朱紜庭
    NewUploadCount.append(tmpdf10D.count()['維護業務ID']) #黃煒凱
    NewUploadCount.append(tmpdf10E.count()['維護業務ID']) #吳美姍
    NewUploadCount.append(tmpdf10F.count()['維護業務ID']) #林心沛
    NewUploadCount.append(tmpdf10G.count()['維護業務ID']) #李蒨盈
    NewUploadCount.append(tmpdf10H.count()['維護業務ID']) #陳湘月
    #NewUploadCount.append(tmpdf10F.count()['維護業務ID']) #莊子琳
    #NewUploadCount.append(tmpdf10G.count()['維護業務ID']) #李蒨盈
    #NewUploadCount.append(tmpdf10H.count()['維護業務ID']) #林心沛
    #NewUploadCount.append(tmpdf10I.count()['維護業務ID']) #陳湘月
    
    totalCount = 0
    for x in NewUploadCount:
        #print(x)
        totalCount = totalCount + x

    NewUploadCount.append(totalCount) #總和

    #本月新增家數
    NewMember = []
    tmpANewMember = df.loc[(df['維護業務ID'] == 102) & (((df['同意會員條款時間'] >= str(first_day)) & (df['同意會員條款時間'] <= str(nextMonth_first_day))) ), :] 
    tmpBNewMember = df.loc[(df['維護業務ID'] == 109) & (((df['同意會員條款時間'] >= str(first_day)) & (df['同意會員條款時間'] <= str(nextMonth_first_day))) ), :] 
    tmpCNewMember = df.loc[(df['維護業務ID'] == 101) & (((df['同意會員條款時間'] >= str(first_day)) & (df['同意會員條款時間'] <= str(nextMonth_first_day))) ), :] 
    #tmpCNewMember = df.loc[(df['維護業務ID'] == 120) & (((df['同意會員條款時間'] >= str(first_day)) & (df['同意會員條款時間'] <= str(nextMonth_first_day))) ), :]
    tmpDNewMember = df.loc[(df['維護業務ID'] == 99) & (((df['同意會員條款時間'] >= str(first_day)) & (df['同意會員條款時間'] <= str(nextMonth_first_day))) ), :] 
    tmpENewMember = df.loc[(df['維護業務ID'] == 116) & (((df['同意會員條款時間'] >= str(first_day)) & (df['同意會員條款時間'] <= str(nextMonth_first_day))) ), :] 
    tmpFNewMember = df.loc[(df['維護業務ID'] == 112) & (((df['同意會員條款時間'] >= str(first_day)) & (df['同意會員條款時間'] <= str(nextMonth_first_day))) ), :] 
    tmpGNewMember = df.loc[(df['維護業務ID'] == 119) & (((df['同意會員條款時間'] >= str(first_day)) & (df['同意會員條款時間'] <= str(nextMonth_first_day))) ), :]    
    tmpHNewMember = df.loc[(df['維護業務ID'] == 117) & (((df['同意會員條款時間'] >= str(first_day)) & (df['同意會員條款時間'] <= str(nextMonth_first_day))) ), :]
    #tmpFNewMember = df.loc[(df['維護業務ID'] == 121) & (((df['同意會員條款時間'] >= str(first_day)) & (df['同意會員條款時間'] <= str(nextMonth_first_day))) ), :]
    #tmpGNewMember = df.loc[(df['維護業務ID'] == 119) & (((df['同意會員條款時間'] >= str(first_day)) & (df['同意會員條款時間'] <= str(nextMonth_first_day))) ), :]
    #tmpHNewMember = df.loc[(df['維護業務ID'] == 112) & (((df['同意會員條款時間'] >= str(first_day)) & (df['同意會員條款時間'] <= str(nextMonth_first_day))) ), :]
    #tmpINewMember = df.loc[(df['維護業務ID'] == 117) & (((df['同意會員條款時間'] >= str(first_day)) & (df['同意會員條款時間'] <= str(nextMonth_first_day))) ), :]
    
    NewMember.append(tmpANewMember.count()['維護業務ID']) #邱雅凌
    NewMember.append(tmpBNewMember.count()['維護業務ID']) #林賢宏
    NewMember.append(tmpCNewMember.count()['維護業務ID']) #童嘉如
    #NewMember.append(tmpCNewMember.count()['維護業務ID']) #朱紜庭
    NewMember.append(tmpDNewMember.count()['維護業務ID']) #黃煒凱
    NewMember.append(tmpENewMember.count()['維護業務ID']) #吳美珊
    NewMember.append(tmpFNewMember.count()['維護業務ID']) #林心沛
    NewMember.append(tmpGNewMember.count()['維護業務ID']) #李蒨盈
    NewMember.append(tmpHNewMember.count()['維護業務ID']) #陳湘月
    #NewMember.append(tmpFNewMember.count()['維護業務ID']) #莊子琳
    #NewMember.append(tmpGNewMember.count()['維護業務ID']) #李蒨盈
    #NewMember.append(tmpHNewMember.count()['維護業務ID']) #林心沛
    #NewMember.append(tmpINewMember.count()['維護業務ID']) #陳湘月

    totalB = 0
    for x in NewMember:
        #print(x)
        totalB = totalB + x

    NewMember.append(totalB) #總和


    #本月會員新上台數
    NewMemberUpload = []
    #NewMemberUpload.append(tmpANewMember.sum()['本月新上(新)']) #邱雅凌 
    #NewMemberUpload.append(tmpBNewMember.sum()['本月新上(新)']) #林賢宏 
    #NewMemberUpload.append(tmpCNewMember.sum()['本月新上(新)']) #童嘉如
    #NewMemberUpload.append(tmpDNewMember.sum()['本月新上(新)']) #黃煒凱
    #NewMemberUpload.append(tmpENewMember.sum()['本月新上(新)']) #吳美珊
    #NewMemberUpload.append(tmpFNewMember.sum()['本月新上(新)']) #林心沛
    #NewMemberUpload.append(tmpGNewMember.sum()['本月新上(新)']) #李蒨盈
    #NewMemberUpload.append(tmpHNewMember.sum()['本月新上(新)']) #陳湘月
    NewMemberUpload.append(tmpANewMember.select_dtypes(include='number').sum()['本月新上(新)']) #邱雅凌
    NewMemberUpload.append(tmpBNewMember.select_dtypes(include='number').sum()['本月新上(新)']) #林賢宏
    NewMemberUpload.append(tmpCNewMember.select_dtypes(include='number').sum()['本月新上(新)']) #童嘉如
    #NewMemberUpload.append(tmpCNewMember.select_dtypes(include='number').sum()['本月新上(新)']) #朱紜庭
    NewMemberUpload.append(tmpDNewMember.select_dtypes(include='number').sum()['本月新上(新)']) #黃煒凱
    NewMemberUpload.append(tmpENewMember.select_dtypes(include='number').sum()['本月新上(新)']) #吳美珊
    NewMemberUpload.append(tmpFNewMember.select_dtypes(include='number').sum()['本月新上(新)']) #林心沛
    NewMemberUpload.append(tmpGNewMember.select_dtypes(include='number').sum()['本月新上(新)']) #李蒨盈
    NewMemberUpload.append(tmpHNewMember.select_dtypes(include='number').sum()['本月新上(新)']) #陳湘月
    #NewMemberUpload.append(tmpFNewMember.select_dtypes(include='number').sum()['本月新上(新)']) #莊子琳
    #NewMemberUpload.append(tmpGNewMember.select_dtypes(include='number').sum()['本月新上(新)']) #李蒨盈
    #NewMemberUpload.append(tmpHNewMember.select_dtypes(include='number').sum()['本月新上(新)']) #林心沛
    #NewMemberUpload.append(tmpINewMember.select_dtypes(include='number').sum()['本月新上(新)']) #陳湘月
    
    totalC = 0
    for x in NewMemberUpload:
        #print(x)
        totalC = totalC + x

    NewMemberUpload.append(totalC) #總和


    #HOT認證車淨台數 D
    NewHotCarNet = []  
    #NewHotCarNet.append(tmpA.sum()['HOT認証車淨台數'])
    #NewHotCarNet.append(tmpB.sum()['HOT認証車淨台數'])
    #NewHotCarNet.append(tmpC.sum()['HOT認証車淨台數'])
    #NewHotCarNet.append(tmpD.sum()['HOT認証車淨台數'])
    #NewHotCarNet.append(tmpE.sum()['HOT認証車淨台數'])
    #NewHotCarNet.append(tmpF.sum()['HOT認証車淨台數'])
    #NewHotCarNet.append(tmpG.sum()['HOT認証車淨台數']) 
    NewHotCarNet.append(tmpA.select_dtypes(include='number').sum()['HOT認証車淨台數'])
    NewHotCarNet.append(tmpB.select_dtypes(include='number').sum()['HOT認証車淨台數'])
    NewHotCarNet.append(tmpC.select_dtypes(include='number').sum()['HOT認証車淨台數'])
    NewHotCarNet.append(tmpD.select_dtypes(include='number').sum()['HOT認証車淨台數'])
    NewHotCarNet.append(tmpE.select_dtypes(include='number').sum()['HOT認証車淨台數'])
    NewHotCarNet.append(tmpF.select_dtypes(include='number').sum()['HOT認証車淨台數'])
    NewHotCarNet.append(tmpG.select_dtypes(include='number').sum()['HOT認証車淨台數'])
    NewHotCarNet.append(tmpH.select_dtypes(include='number').sum()['HOT認証車淨台數'])
    #NewHotCarNet.append(tmpI.select_dtypes(include='number').sum()['HOT認証車淨台數'])
    
    totalD = 0
    for x in NewHotCarNet:
        #print(x)
        totalD = totalD + x

    NewHotCarNet.append(totalD) #總和

    #公會專案淨台數 E
    NewGuildNet = []  
    #NewGuildNet.append(tmpA.sum()['公會專案淨台數'])
    #NewGuildNet.append(tmpB.sum()['公會專案淨台數'])
    #NewGuildNet.append(tmpC.sum()['公會專案淨台數'])
    #NewGuildNet.append(tmpD.sum()['公會專案淨台數'])
    #NewGuildNet.append(tmpE.sum()['公會專案淨台數'])
    #NewGuildNet.append(tmpF.sum()['公會專案淨台數'])
    #NewGuildNet.append(tmpG.sum()['公會專案淨台數']) 
    NewGuildNet.append(tmpA.select_dtypes(include='number').sum()['公會專案淨台數'])
    NewGuildNet.append(tmpB.select_dtypes(include='number').sum()['公會專案淨台數'])
    NewGuildNet.append(tmpC.select_dtypes(include='number').sum()['公會專案淨台數'])
    NewGuildNet.append(tmpD.select_dtypes(include='number').sum()['公會專案淨台數'])
    NewGuildNet.append(tmpE.select_dtypes(include='number').sum()['公會專案淨台數'])
    NewGuildNet.append(tmpF.select_dtypes(include='number').sum()['公會專案淨台數'])
    NewGuildNet.append(tmpG.select_dtypes(include='number').sum()['公會專案淨台數'])
    NewGuildNet.append(tmpH.select_dtypes(include='number').sum()['公會專案淨台數'])
    #NewGuildNet.append(tmpI.select_dtypes(include='number').sum()['公會專案淨台數'])
    
    totalE = 0
    for x in NewGuildNet:
        #print(x)
        totalE = totalE + x

    NewGuildNet.append(totalE) #總和

    #可收費車輛淨台數 F
    NewFeeNet = []  
    #NewFeeNet.append(tmpA.sum()['可收費車輛淨台數'])
    #NewFeeNet.append(tmpB.sum()['可收費車輛淨台數'])
    #NewFeeNet.append(tmpC.sum()['可收費車輛淨台數'])
    #NewFeeNet.append(tmpD.sum()['可收費車輛淨台數'])
    #NewFeeNet.append(tmpE.sum()['可收費車輛淨台數'])
    #NewFeeNet.append(tmpF.sum()['可收費車輛淨台數'])
    #NewFeeNet.append(tmpG.sum()['可收費車輛淨台數']) 
    NewFeeNet.append(tmpA.select_dtypes(include='number').sum()['可收費車輛淨台數'])
    NewFeeNet.append(tmpB.select_dtypes(include='number').sum()['可收費車輛淨台數'])
    NewFeeNet.append(tmpC.select_dtypes(include='number').sum()['可收費車輛淨台數'])
    NewFeeNet.append(tmpD.select_dtypes(include='number').sum()['可收費車輛淨台數'])
    NewFeeNet.append(tmpE.select_dtypes(include='number').sum()['可收費車輛淨台數'])
    NewFeeNet.append(tmpF.select_dtypes(include='number').sum()['可收費車輛淨台數'])
    NewFeeNet.append(tmpG.select_dtypes(include='number').sum()['可收費車輛淨台數'])
    NewFeeNet.append(tmpH.select_dtypes(include='number').sum()['可收費車輛淨台數'])
    #NewFeeNet.append(tmpI.select_dtypes(include='number').sum()['可收費車輛淨台數'])
    
    totalF = 0
    for x in NewFeeNet:
        #print(x)
        totalF = totalF + x

    NewFeeNet.append(totalF) #總和

    #本月新上架淨台數 G
    NewUploadNet = []  
    #NewUploadNet.append(tmpA.sum()['本月新上(新)'])
    #NewUploadNet.append(tmpB.sum()['本月新上(新)'])
    #NewUploadNet.append(tmpC.sum()['本月新上(新)'])
    #NewUploadNet.append(tmpD.sum()['本月新上(新)'])
    #NewUploadNet.append(tmpE.sum()['本月新上(新)'])
    #NewUploadNet.append(tmpF.sum()['本月新上(新)'])
    #NewUploadNet.append(tmpG.sum()['本月新上(新)'])
    NewUploadNet.append(tmpA.select_dtypes(include='number').sum()['本月新上(新)']) #邱雅凌
    NewUploadNet.append(tmpB.select_dtypes(include='number').sum()['本月新上(新)']) #林賢宏
    NewUploadNet.append(tmpC.select_dtypes(include='number').sum()['本月新上(新)']) #童嘉如
    #NewUploadNet.append(tmpC.select_dtypes(include='number').sum()['本月新上(新)']) #朱紜庭
    NewUploadNet.append(tmpD.select_dtypes(include='number').sum()['本月新上(新)']) #黃煒凱
    NewUploadNet.append(tmpE.select_dtypes(include='number').sum()['本月新上(新)']) #吳美珊
    NewUploadNet.append(tmpF.select_dtypes(include='number').sum()['本月新上(新)']) #林心沛
    NewUploadNet.append(tmpG.select_dtypes(include='number').sum()['本月新上(新)']) #李蒨盈
    NewUploadNet.append(tmpH.select_dtypes(include='number').sum()['本月新上(新)']) #陳湘月
    #NewUploadNet.append(tmpF.select_dtypes(include='number').sum()['本月新上(新)']) #莊子琳
    #NewUploadNet.append(tmpG.select_dtypes(include='number').sum()['本月新上(新)']) #李蒨盈
    #NewUploadNet.append(tmpH.select_dtypes(include='number').sum()['本月新上(新)']) #林心沛
    #NewUploadNet.append(tmpI.select_dtypes(include='number').sum()['本月新上(新)']) #陳湘月
    
    
    totalG = 0
    for x in NewUploadNet:
        #print(x)
        totalG = totalG + x

    NewUploadNet.append(totalG) #總和

    #廣宣銷售實績 H
    PromoteAct = []
   
    PromoteAct.append(tmpAPromote.sum()['置頂'] + tmpAPromote.sum()['更新'] + tmpAPromote.sum()['全面曝光'] + tmpAPromote.sum()['BANNER曝光'])
    PromoteAct.append(tmpBPromote.sum()['置頂'] + tmpBPromote.sum()['更新'] + tmpBPromote.sum()['全面曝光'] + tmpBPromote.sum()['BANNER曝光'])
    PromoteAct.append(tmpCPromote.sum()['置頂'] + tmpCPromote.sum()['更新'] + tmpCPromote.sum()['全面曝光'] + tmpCPromote.sum()['BANNER曝光'])
    PromoteAct.append(tmpDPromote.sum()['置頂'] + tmpDPromote.sum()['更新'] + tmpDPromote.sum()['全面曝光'] + tmpDPromote.sum()['BANNER曝光'])
    PromoteAct.append(tmpEPromote.sum()['置頂'] + tmpEPromote.sum()['更新'] + tmpEPromote.sum()['全面曝光'] + tmpEPromote.sum()['BANNER曝光'])
    PromoteAct.append(tmpFPromote.sum()['置頂'] + tmpFPromote.sum()['更新'] + tmpFPromote.sum()['全面曝光'] + tmpFPromote.sum()['BANNER曝光'])
    PromoteAct.append(tmpGPromote.sum()['置頂'] + tmpGPromote.sum()['更新'] + tmpGPromote.sum()['全面曝光'] + tmpGPromote.sum()['BANNER曝光'])
    PromoteAct.append(tmpHPromote.sum()['置頂'] + tmpHPromote.sum()['更新'] + tmpHPromote.sum()['全面曝光'] + tmpHPromote.sum()['BANNER曝光'])
    #PromoteAct.append(tmpIPromote.sum()['置頂'] + tmpIPromote.sum()['更新'] + tmpIPromote.sum()['全面曝光'] + tmpIPromote.sum()['BANNER曝光'])
   
    totalH = 0
    for x in PromoteAct:
        #print(x)
        totalH = totalH + x

    PromoteAct.append(totalH) #總和

    #Banner銷售實績 I   合併至廣宣銷售實績
    #PromoteBanner = [] 
    #PromoteBanner.append(tmpAPromote.sum()['BANNER曝光'])
    #PromoteBanner.append(tmpBPromote.sum()['BANNER曝光'])
    #PromoteBanner.append(tmpCPromote.sum()['BANNER曝光'])
    #PromoteBanner.append(tmpDPromote.sum()['BANNER曝光'])
    #PromoteBanner.append(tmpEPromote.sum()['BANNER曝光'])
    #PromoteBanner.append(tmpFPromote.sum()['BANNER曝光'])
    #PromoteBanner.append(tmpGPromote.sum()['BANNER曝光'])   

    #totalI = 0
    #for x in PromoteBanner:
        #print(x)
    #    totalI = totalI + x

    #PromoteBanner.append(totalI) #總和

    #廣宣銷售達成率
    PromoteAR = []
    for i, v in enumerate(PromoteTarget):
    #    PromoteAR.append((PromoteAct[i]+PromoteBanner[i])/v)
        if v == 0:
            PromoteAR.append(0)
        else:
            PromoteAR.append((PromoteAct[i])/v)
    

    # TL 新上架目標台數
    # 2022/03/01 新增TL ID & insert table (1, 2, 3, 4, 5)，張鈞偉、荊德龍、李明憲、陳叔毅、林士欽
    # TLNewUploadTarget = [600, 1300, 880, 550, 1000, 4330]
    TLNewUploadTarget = targetTLUpload

    # TL過戶收費實績台數
    TLNewUploadAct = []

    #北區
    #TLNewUploadAct.append((tmpA.sum()['可收費新上架台數'] + tmpA.sum()['可收費實車在店台數'] + tmpA.sum()['HOT認証車新上架台數'] + tmpA.sum()['活動專案新上架台數'] + tmpA.sum()['專案上架實車在店台數']).round(0) +
                        #(tmpB.sum()['可收費新上架台數'] + tmpB.sum()['可收費實車在店台數'] + tmpB.sum()['HOT認証車新上架台數'] + tmpB.sum()['活動專案新上架台數'] + tmpB.sum()['專案上架實車在店台數']).round(0) )
    #桃苗
    #TLNewUploadAct.append((tmpC.sum()['可收費新上架台數'] + tmpC.sum()['可收費實車在店台數'] + tmpC.sum()['HOT認証車新上架台數'] + tmpC.sum()['活動專案新上架台數'] + tmpC.sum()['專案上架實車在店台數']).round(0) )
    #中區
    #TLNewUploadAct.append((tmpD.sum()['可收費新上架台數'] + tmpD.sum()['可收費實車在店台數'] + tmpD.sum()['HOT認証車新上架台數'] + tmpD.sum()['活動專案新上架台數'] + tmpD.sum()['專案上架實車在店台數']).round(0) +
                          #(tmpE.sum()['可收費新上架台數'] + tmpE.sum()['可收費實車在店台數'] + tmpE.sum()['HOT認証車新上架台數'] + tmpE.sum()['活動專案新上架台數'] + tmpE.sum()['專案上架實車在店台數']).round(0) )
    #雲嘉   
    #TLNewUploadAct.append((tmpF.sum()['可收費新上架台數'] + tmpF.sum()['可收費實車在店台數'] + tmpF.sum()['HOT認証車新上架台數'] + tmpF.sum()['活動專案新上架台數'] + tmpF.sum()['專案上架實車在店台數']).round(0) )     
    #高屏
    #TLNewUploadAct.append((tmpG.sum()['可收費新上架台數'] + tmpG.sum()['可收費實車在店台數'] + tmpG.sum()['HOT認証車新上架台數'] + tmpG.sum()['活動專案新上架台數'] + tmpG.sum()['專案上架實車在店台數']).round(0) )

    # 北區
    TLNewUploadAct.append(tmpdf10A.count()['維護業務ID'] + tmpdf10B.count()['維護業務ID'])
    # 桃苗
    TLNewUploadAct.append(tmpdf10C.count()['維護業務ID'])
    # 中區
    TLNewUploadAct.append(tmpdf10D.count()['維護業務ID'] + tmpdf10E.count()['維護業務ID'])
    # 雲嘉
    TLNewUploadAct.append(tmpdf10F.count()['維護業務ID'] + tmpdf10G.count()['維護業務ID'])
    #TLNewUploadAct.append(tmpdf10F.count()['維護業務ID'] + tmpdf10G.count()['維護業務ID']) + tmpdf10H.count()['維護業務ID'])
    # 高屏
    TLNewUploadAct.append(tmpdf10H.count()['維護業務ID'])
    #TLNewUploadAct.append(tmpdf10I.count()['維護業務ID'])
        
    totalAll = 0
    for x in TLNewUploadAct:
        #print(x)
        totalAll = totalAll + x
        
    TLNewUploadAct.append(totalAll) #總和

    # TL 新上架達成率
    TLNewUploadAR = []
    for i, v in enumerate(TLNewUploadTarget):
        TLNewUploadAR.append(TLNewUploadAct[i]/v)
        
    # TL 過戶收費金額
    TLNewUploadMoney = []
    #北區
    TLNewUploadMoney.append(int((tmpdf10A["當日過戶收費金額"]).sum().round(0)) + (int((tmpdf10B["當日過戶收費金額"]).sum().round(0))))
    #桃苗
    TLNewUploadMoney.append(int((tmpdf10C["當日過戶收費金額"]).sum().round(0)))
    #中區
    TLNewUploadMoney.append(int((tmpdf10D["當日過戶收費金額"]).sum().round(0)) + (int((tmpdf10E["當日過戶收費金額"]).sum().round(0))))
    #雲嘉 
    TLNewUploadMoney.append(int((tmpdf10F["當日過戶收費金額"]).sum().round(0)) + (int((tmpdf10G["當日過戶收費金額"]).sum().round(0))))
    #TLNewUploadMoney.append(int((tmpdf10F["當日過戶收費金額"]).sum().round(0)) + (int((tmpdf10G["當日過戶收費金額"]).sum().round(0))) + (int((tmpdf10H["當日過戶收費金額"]).sum().round(0))))
    #高屏
    TLNewUploadMoney.append(int((tmpdf10H["當日過戶收費金額"]).sum().round(0)))
    #TLNewUploadMoney.append(int((tmpdf10I["當日過戶收費金額"]).sum().round(0)))

    TLtotalMoney = 0
    for x in TLNewUploadMoney:
        #print(x)
        TLtotalMoney = TLtotalMoney + x

    TLNewUploadMoney.append(TLtotalMoney) #總和
    
    # TL 過戶台數
    TLNewUploadCount = []
    #北區
    TLNewUploadCount.append(tmpdf10A.count()['維護業務ID'] + tmpdf10B.count()['維護業務ID'])
    #桃苗
    TLNewUploadCount.append(tmpdf10C.count()['維護業務ID'])
    #中區
    TLNewUploadCount.append(tmpdf10D.count()['維護業務ID'] + tmpdf10E.count()['維護業務ID'])
    #雲嘉
    TLNewUploadCount.append(tmpdf10F.count()['維護業務ID'] + tmpdf10G.count()['維護業務ID'])
    #TLNewUploadCount.append(tmpdf10F.count()['維護業務ID'] + tmpdf10G.count()['維護業務ID'] + tmpdf10H.count()['維護業務ID'])
    #高屏
    TLNewUploadCount.append(tmpdf10H.count()['維護業務ID'])
    #TLNewUploadCount.append(tmpdf10I.count()['維護業務ID'])
    
    TLtotalCount = 0
    for x in TLNewUploadCount:
        #print(x)
        TLtotalCount = TLtotalCount + x
        
    TLNewUploadAct.append(TLtotalCount) #總和
        
    # TL 本月新增家數
    TLNewMember = []
    TLNewMember.append(tmpANewMember.count()['維護業務ID'] + tmpBNewMember.count()['維護業務ID'])   
    TLNewMember.append(tmpCNewMember.count()['維護業務ID'])
    TLNewMember.append(tmpDNewMember.count()['維護業務ID'] + tmpENewMember.count()['維護業務ID'])
    TLNewMember.append(tmpFNewMember.count()['維護業務ID'] + tmpGNewMember.count()['維護業務ID'])
    #TLNewMember.append(tmpFNewMember.count()['維護業務ID'] + tmpGNewMember.count()['維護業務ID'] + tmpHNewMember.count()['維護業務ID'])
    TLNewMember.append(tmpHNewMember.count()['維護業務ID'])
    #TLNewMember.append(tmpINewMember.count()['維護業務ID'])
    
    TLNewMember.append(totalB) #總和  
    
    # TL 本月新會員新上台數
    TLNewMemberUpload = []
    #TLNewMemberUpload.append(tmpANewMember.sum()['本月新上(新)'] + tmpBNewMember.sum()['本月新上(新)']) 
    #TLNewMemberUpload.append(tmpCNewMember.sum()['本月新上(新)']) 
    #TLNewMemberUpload.append(tmpDNewMember.sum()['本月新上(新)'] + tmpENewMember.sum()['本月新上(新)'])
    #TLNewMemberUpload.append(tmpFNewMember.sum()['本月新上(新)'])
    #TLNewMemberUpload.append(tmpGNewMember.sum()['本月新上(新)'])
    TLNewMemberUpload.append(tmpANewMember.select_dtypes(include='number').sum()['本月新上(新)'] + tmpBNewMember.select_dtypes(include='number').sum()['本月新上(新)'])
    TLNewMemberUpload.append(tmpCNewMember.select_dtypes(include='number').sum()['本月新上(新)'])
    TLNewMemberUpload.append(tmpDNewMember.select_dtypes(include='number').sum()['本月新上(新)'] + tmpENewMember.select_dtypes(include='number').sum()['本月新上(新)'])
    TLNewMemberUpload.append(tmpFNewMember.select_dtypes(include='number').sum()['本月新上(新)'] + tmpGNewMember.select_dtypes(include='number').sum()['本月新上(新)'])
    #TLNewMemberUpload.append(tmpFNewMember.select_dtypes(include='number').sum()['本月新上(新)'] + tmpGNewMember.select_dtypes(include='number').sum()['本月新上(新)'] + tmpHNewMember.select_dtypes(include='number').sum()['本月新上(新)'])
    TLNewMemberUpload.append(tmpHNewMember.select_dtypes(include='number').sum()['本月新上(新)'])
    #TLNewMemberUpload.append(tmpINewMember.select_dtypes(include='number').sum()['本月新上(新)'])
    
    
    TLNewMemberUpload.append(totalC) #總和  

    # TL 本月新上架淨台數
    TLNewUploadNet = []
    #TLNewUploadNet.append(tmpA.sum()['本月新上(新)'] + tmpB.sum()['本月新上(新)']) 
    #TLNewUploadNet.append(tmpC.sum()['本月新上(新)']) 
    #TLNewUploadNet.append(tmpD.sum()['本月新上(新)'] + tmpE.sum()['本月新上(新)'])
    #TLNewUploadNet.append(tmpF.sum()['本月新上(新)'])
    #TLNewUploadNet.append(tmpG.sum()['本月新上(新)'])
    TLNewUploadNet.append(tmpA.select_dtypes(include='number').sum()['本月新上(新)'] + tmpB.select_dtypes(include='number').sum()['本月新上(新)'])
    TLNewUploadNet.append(tmpC.select_dtypes(include='number').sum()['本月新上(新)'])
    TLNewUploadNet.append(tmpD.select_dtypes(include='number').sum()['本月新上(新)'] + tmpE.select_dtypes(include='number').sum()['本月新上(新)'])
    TLNewUploadNet.append(tmpF.select_dtypes(include='number').sum()['本月新上(新)'] + tmpG.select_dtypes(include='number').sum()['本月新上(新)'])
    #TLNewUploadNet.append(tmpF.select_dtypes(include='number').sum()['本月新上(新)'] + tmpG.select_dtypes(include='number').sum()['本月新上(新)'] + tmpH.select_dtypes(include='number').sum()['本月新上(新)'])
    TLNewUploadNet.append(tmpH.select_dtypes(include='number').sum()['本月新上(新)'])
    #TLNewUploadNet.append(tmpI.select_dtypes(include='number').sum()['本月新上(新)'])


    TLNewUploadNet.append(totalG) #總和

    df = pd.DataFrame(list(zip(NewUploadTarget, NewUploadAct, NewUploadMoney,NewUploadAR, PromoteTarget, PromoteAct, PromoteAR, NewHotCarNet, NewGuildNet, NewFeeNet, NewMember, NewMemberUpload,NewUploadNet)),
               #columns =['新上架目標台數', '新上架實績台數', '新上架達成率', '廣宣銷售目標', '廣宣銷售實績', 'Banner銷售實績','廣宣達成率', 'HOT認證車淨台數', '公會專案淨台數', '可收費車輛淨台數', '本月新增家數', '本月會員新上台數', '本月新上架淨台數'])
                columns =['過戶收費目標台數', '過戶收費實績台數', '過戶收費金額', '過戶收費台數達成率', '廣宣銷售目標', '廣宣銷售實績','廣宣達成率', 'HOT認證車淨台數', '公會專案淨台數', '可收費車輛淨台數', '本月新增家數', '本月會員新上台數', '本月新上架淨台數'])
    
    df_down = pd.DataFrame(list(zip(TLNewUploadTarget, TLNewUploadAct, TLNewUploadMoney, TLNewUploadAR, TLNewMember, TLNewMemberUpload, TLNewUploadNet)),
                columns =['TL過戶收費目標台數', 'TL過戶收費實績台數', 'TL過戶收費金額', 'TL過戶收費台數達成率', 'TL本月新增家數', 'TL本月會員新上台數', 'TL本月新上架淨台數'])


    # 上架台數,依區域/上架天數種類
    OnLineCar = []
    ZoneIDs = [1,2,3,4,5,0,9] #區域分類 1-北區/2-桃苗/3-中區/4-雲嘉/5-高屏/0-異常車商/9-總程
    DayTypeIDs = [1,2,3,4,5,6] #上架天數種類 1[0~30天]/2[31~60天]/3[61~90天]/4[91~180天]/5[181~360天]/6[361天以上]  
    for x in ZoneIDs: 
        ZoneRow = []
        for y in DayTypeIDs:
            if x == 9: #總和
                Zone = allCarDetail.loc[(allCarDetail['ZoneID'] >= 0) & (allCarDetail['ZoneID'] <=5),:] 
            else: #各區   
                Zone = allCarDetail.loc[(allCarDetail['ZoneID'] == x), :] #區域
            #ZoneRow.append(Zone.loc[(Zone['DayType'] == y )].sum()['CarNum'].astype(int)) 
            ZoneRow.append(Zone.loc[Zone['DayType'] == y, 'CarNum'].sum().astype(int))
        #OnLineCar.append({x:ZoneRow})
        OnLineCar.append(ZoneRow)
    
    df_zone = pd.DataFrame(list(OnLineCar),
              columns =['30天以內', '31-60天', '61-90天', '91-180天', '181-360天', '361天以上'])

    return df, df_down, df_zone, carThisMonth, carThisMonthNew, carThisMonthOff, carIncrease, carLastMonth

# 將資料輸出至Excel
def addValueToExcel(ws_main, valueList, rowIndex, colIndex):
    for index, value in enumerate(valueList):
        grid = ws_main.cell(row=4+rowIndex+index, column=4+colIndex)
        grid.value = value

# 加工企劃定義的指標
def subThisMonthIncre(df):
    #df['本月淨增加'] = df['本月新上架車輛'] - df['本月下架車輛']
    #20240830 已將2日內下架數量扣除於本月新上架車輛的sql語法
    #df['本月新上(新)'] = df['本月新上架車輛'] - df['2日內下架']
    df['本月新上(新)'] = df['本月新上架車輛']
    #df['本月下架(新)'] = df['當月下架未過戶'] + df['當月過戶未下架'] + df['當月過戶當月下架']
    #df['本月淨增加(新)'] = df['本月新上(新)'] - df['本月下架(新)']

    df['新上架業績台數'] = df['可收費新上架台數'] + df['可收費實車在店台數'] + df['HOT認証車新上架台數'] + df['活動專案新上架台數'] + df['專案上架實車在店台數']

    return df

# 更改公會名稱
def changeGuildName(df):
    df['公會'] = df['公會'].replace(['台北市汽車商業同業公會','新北市汽車商業同業公會', '桃園市汽車商業同業公會', '苗栗縣汽車商業同業公會', '高雄市汽車商業同業公會', '台中市汽車商業同業公會', '基隆市汽車商業同業公會', '宜蘭縣汽車商業同業公會', '南投縣汽車商業同業公會', '台南市汽車商業同業公會', '臺南市直轄市汽車商業同業公會', '中華民國汽車商業同業公會全聯會', '臺中市直轄市汽車商業同業公會', '屏東縣汽車商業同業公會', '高雄市新汽車商業同業公會', '花蓮縣汽車商業同業公會', '嘉義市汽車商業同業公會'],['台北','新北', '桃園', '苗栗', '高雄', '台中', '基隆', '宜蘭', '南投', '台南', '台南市', '同業', '台中市', '屏東', '高雄(新)', '花蓮', '嘉義'])

    return df

# 數據寫入image中
def append_to_graph(carThisMonth, sheet5):
    # 擷取製圖需要的數據
    b1day_dt =  sheet5[0]
    print(b1day_dt)

    x1 = carThisMonth[0]#車商上架數
    x2 = carThisMonth[2]#聯盟館上架數
    x3 = carThisMonth[3]#原廠認證上架數
    x4 = carThisMonth[4]#總上架數
    x5 = sheet5[1] #到站人次
    x6 = sheet5[2] #到站新客
    x7 = sheet5[3] #總瀏覽數
    x8 = sheet5[4] #物件總瀏覽數

    labels = f'車商上架數{x1}', f'聯盟館上架數{x2}', f'原廠認證上架數{x3}', f'總上架數{x4}', \
             f'到站人次{x5}', f'到站新客{x6}', f'總瀏覽數{x7}', f'物件總瀏覽數{x8}'
    size = [10, 10, 10, 10, 10, 10, 10, 10]
    explode = (0, 0, 0, 0, 0, 0, 0, 0)

    # 設置製圖的變數
    mpl.rcParams[u'font.sans-serif'] = ['simhei']
    mpl.rcParams['axes.unicode_minus'] = False
    mpl.rcParams['font.size'] = 20.0
    mpl.rcParams['figure.figsize'] = 14, 9
    colors = ['red', 'orange', 'yellow', 'green', 'blue', 'lightblue', 'purple', 'pink']

    # 選取圖形為圓餅圖
    plt.pie(size, labels=labels, colors=colors, shadow=True, explode=explode)

    # 顯示圖中間的字體
    plt.text(-0.55, 0.03, '{0}日營運指標'.format(end_date), size=25)

    centre_circle = plt.Circle((0, 0), 0.6, color='grey', fc='white', linewidth=1)
    fig = plt.gcf()
    fig.gca().add_artist(centre_circle)
    plt.axis('equal')
    fig.savefig(path+'sales_graph.pdf')

if __name__ == "__main__":
    print(now)

    script = 'abccar_member_onshelf_withTL_newV2'

    try:
        #memberAgree, target, aTM, aLM, cTM, cLM, GACar, GAWeb, allCar, allCarLM, allCarDetail, q5rtn, q6rtn, q7rtn, q8rtn = ReadData() 
        memberAgree, target, aTM, aLM, cTM, cLM, allCar, allCarLM, allCarDetail, q5rtn, q6rtn, q7rtn, q8rtn, q9rtn, q10rtn = ReadData() 

        #商品訂單
        q9rtn = q9rtn.astype({'購買認証': 'int', '置頂': 'int', '更新': 'int', '全面曝光': 'int', 'BANNER曝光': 'int'})
        print('Part 1 done!')
    except Exception as e:
        msg = '{0}: PART 1: 數據連線讀取失敗! The error message : {1}'.format(script, e)
        send_error_message(script='{}'.format(script), message=msg)
        print(msg)
        sys.exit(1)
    
    try:
        memberAgree_new_tmp = addStageLastMonth(memberAgree)
        memberAgree_new = subThisMonthIncre(memberAgree_new_tmp)
        memberAgree_new = changeGuildName(memberAgree_new)
        print('Part 2 done!')
    except Exception as e:
        msg = '{0}: PART 2: 數據合併失敗! The error message : {1}'.format(script, e)
        send_error_message(script='{}'.format(script), message=msg)
        print(msg)
        sys.exit(1)

    try:
        df, df_down, df_zone, carThisMonth, carThisMonthNew, carThisMonthOff, carIncrease, carLastMonth = processData(memberAgree_new, target, aTM, aLM, cTM, cLM, allCar, allCarLM, allCarDetail, q9rtn, q10rtn)
        print('Part 3 done!')
    except Exception as e:
        msg = '{0}: PART 3: 數據計算出現錯誤! The error message : {1}'.format(script, e)
        send_error_message(script='{}'.format(script), message=msg)
        print(msg)
        sys.exit(1)
        
    try:
        #=========================== Load office file      
        wb = load_workbook('abc好車網_架上台數報表_企劃_20240822RomanTest.xlsx')

        #Sheet 1
        ws_main = wb.worksheets[0]

        # Add today date
        date = ws_main.cell(row=2, column=3)
        date.value = 'abc好車網_營運報表_(' + end_date +')'
        #Top table
        #本月架上台數
        addValueToExcel(ws_main, carThisMonth, 0, 1)
        #本月淨增加
        addValueToExcel(ws_main, carIncrease, 0, 2)
        #上月架上台數
        addValueToExcel(ws_main, carLastMonth, 0, 3)

        # Add today date
        date = ws_main.cell(row=12, column=3)
        date.value = 'abc好車網_架上台數報表_各區(' + end_date +')'
        # 2022/08/10 TL 與 Sales 交換位置 
        #Middle table 
        #TL新上架目標台數
        #addValueToExcel(ws_main, df_down['TL新上架目標台數'].tolist(), 10, 1)
        #TL新上架實績台數
        #addValueToExcel(ws_main, df_down['TL新上架實績台數'].tolist(), 10, 2)       
        #TL新上架達成率
        #addValueToExcel(ws_main, df_down['TL新上架達成率'].tolist(), 10, 3)

        #TL過戶收費目標台數
        addValueToExcel(ws_main, df_down['TL過戶收費目標台數'].tolist(), 10, 1)
        #TL過戶收費實績台數
        addValueToExcel(ws_main, df_down['TL過戶收費實績台數'].tolist(), 10, 2)    
        #TL過戶收費台數達成率
        addValueToExcel(ws_main, df_down['TL過戶收費台數達成率'].tolist(), 10, 3)
        #TL過戶收費金額
        addValueToExcel(ws_main, df_down['TL過戶收費金額'].tolist(), 10, 4)   
        #TL本月新增家數
        addValueToExcel(ws_main, df_down['TL本月新增家數'].tolist(), 10, 5)
        #TL本月新上台數
        addValueToExcel(ws_main, df_down['TL本月會員新上台數'].tolist(), 10, 6)
        #TL本月新上架淨台數
        addValueToExcel(ws_main, df_down['TL本月新上架淨台數'].tolist(), 10, 7)
        
        # Add today date
        date = ws_main.cell(row=21, column=3)
        date.value = 'abc好車網_架上台數報表_業務(' + end_date +')'
        #Down table
        #新上架目標台數
        #addValueToExcel(ws_main, df['新上架目標台數'].tolist(), 19, 0)
        #新上架實績台數
        #addValueToExcel(ws_main, df['新上架實績台數'].tolist(), 19, 1)
        #新上架達成率
        #addValueToExcel(ws_main, df['新上架達成率'].tolist(), 19, 2)

        #過戶收費目標台數
        addValueToExcel(ws_main, df['過戶收費目標台數'].tolist(), 19, 1)
        #過戶收費實績台數
        addValueToExcel(ws_main, df['過戶收費實績台數'].tolist(), 19, 2) 
        #過戶收費台數達成率
        addValueToExcel(ws_main, df['過戶收費台數達成率'].tolist(), 19, 3)
        #過戶收費金額
        addValueToExcel(ws_main, df['過戶收費金額'].tolist(), 19, 4)   
        #本月新增家數
        addValueToExcel(ws_main, df['本月新增家數'].tolist(), 19, 5)
        #本月新上台數
        addValueToExcel(ws_main, df['本月會員新上台數'].tolist(), 19, 6)
        #HOT認證車淨台數
        addValueToExcel(ws_main, df['HOT認證車淨台數'].tolist(), 19, 7)
        #公會專案淨台數
        addValueToExcel(ws_main, df['公會專案淨台數'].tolist(), 19, 8)
        #可收費車輛淨台數
        addValueToExcel(ws_main, df['可收費車輛淨台數'].tolist(), 19, 9)        
        #本月新上架淨台數
        addValueToExcel(ws_main, df['本月新上架淨台數'].tolist(), 19, 10)
       
        # Add today date
        date = ws_main.cell(row=33, column=3)
        date.value = 'abc好車網_廣宣銷售報表_業務(' + end_date +')'
        #廣宣銷售目標
        addValueToExcel(ws_main, df['廣宣銷售目標'].tolist(), 31, 1)
        #廣宣銷售實績
        addValueToExcel(ws_main, df['廣宣銷售實績'].tolist(), 31, 2)
        #Banner銷售實績 併入廣宣銷售實績
        #addValueToExcel(ws_main, df['Banner銷售實績'].tolist(), 30, 3)
        #廣宣達成率
        addValueToExcel(ws_main, df['廣宣達成率'].tolist(), 31, 3)

        # 2021-12-27 組業務排序邏輯字串
        memberAgree_new['維護業務'] = memberAgree_new.apply(lambda x: '0' + str(x['維護業務序號']) + ' ' + x['維護業務']  if len(str(x['維護業務序號'])) == 1 else str(x['維護業務序號']) + ' ' + x['維護業務'] , axis=1)
        memberAgree_new['維護業務'] = memberAgree_new['維護業務'].replace('00 ', '')

        #Sheet 2
        #取欄位

        #df_Office = memberAgree_new[[
        #    'MemberID', '會員名稱', '車商名稱', '城市別', '連絡人', '連絡電話', '身分別', 
        #    '網路好店', '買萬送萬', '公會', '維護業務', 'HAA帳號', 'HOT主帳號', 'HOT官網認證台數', 
        #    'HOT官網總台數', '上月底架上台數', '本日新上架車輛' , '本日下架車輛', 
        #    '本月新上架車輛', '2日內下架', '本月新上(新)', '本月下架車輛', '當月下架未過戶', '當月過戶未下架', 
        #    '當月過戶當月下架', '本月下架(新)', '本月淨增加', '本月淨增加(新)', '本月架上車輛', 
        #    '本月架上驗證成功車輛', 'HAA上架數', 'HAA待售數', 
        #    '簡易刊登本月待售數', '簡易刊登本月上架數', '簡易刊登架上台數', 
        #    '最新儲值時間', '信用卡綁定時間', '同意會員條款時間', '註冊時間', 
        #    '本月上架登錄HOT認証書台數', '本月上架參與活動台數']]
        
        df_Office = memberAgree_new[[
            'MemberID', '會員名稱', '車商名稱', '城市別', '連絡人', '連絡電話', '身分別', 
            '網路好店', '買萬送萬', '公會', '維護業務', 'HAA帳號', 'HOT主帳號', 'HOT官網認證台數', 
            'HOT官網總台數', '上月底架上台數', '本日新上架車輛' , '本日下架車輛', 
            '本月新上(新)', '本月下架車輛', '可收費新上架台數', '可收費實車在店台數',
            'HOT認証車新上架台數', '活動專案新上架台數', '專案上架實車在店台數', '新上架業績台數', 
            '本月架上車輛', '最新儲值時間', '信用卡綁定時間', '同意會員條款時間', '註冊時間']]
        
        #清空表格
        ws = wb.worksheets[1]
        ws.delete_rows(2, 100000)

        n=1
        for r in dataframe_to_rows(df_Office, index=False, header=False):
            m = 1
            for i in r:
                grid = ws.cell(row=1+n, column=m)
                grid.value = i
                m+=1
            n +=1

        #Sheet 3 
        outProductOrderDF = q9rtn[["維護業務","購買認証","置頂","更新","全面曝光","BANNER曝光"]].copy() 
        outProductOrderDF.loc["Total"] = outProductOrderDF.sum(numeric_only=True)
        outProductOrderDF['維護業務'] = outProductOrderDF['維護業務'].fillna('小計')
        #df_Office_filter = df_Office[(df_Office['本月架上驗證成功車輛'] >= 15) | (df_Office['本月新上(新)'] >=4 )]
        #清空表格
        ws = wb.worksheets[2]
        ws.delete_rows(2, 1000)

        n=1
        for r in dataframe_to_rows(outProductOrderDF, index=False, header=False):
            m = 1
            for i in r:
                grid = ws.cell(row=1+n, column=m)
                grid.value = i
                grid.alignment = Alignment(horizontal="center",vertical="center")
                if (m>1):
                    grid.number_format = '#,##0'
                m+=1
            n +=1

        #Sheet 4
        ws = wb.worksheets[3]
        sheet4 = []
        sheet4.append(end_date) #日期
        sheet4.append(int(sum(memberAgree_new['本月架上車輛']))) #車商會員上架數
        sheet4.append(int(cTM['Total'].item())) #原廠認證上架數
        sheet4.append(int(cTM['TCPO'].item())) #TCPO
        sheet4.append(int(cTM['LCPO'].item())) #LCPO
        sheet4.append(int(cTM['OtherCPO'].item())) #OtherCPO
        sheet4.append(int(aTM['Number'].item())) #聯盟館上架數
        sheet4.append(int(sum(memberAgree_new['本月架上車輛'])) + int(cTM['Total'].item()) + int(aTM['Number'].item())) #聯盟館上架數

        ws.append(sheet4)

        ##Sheet 5 ==========================GA資料==========================
        #sheet5 = []
        ##如果GA有資料
        #if(len(GAWeb)>0 and len(GACar)>0):

        #    GAWeb['visitor'] = GAWeb['visitor'].astype(int)
        #    GAWeb['New_visitor'] = GAWeb['New_visitor'].astype(int)
        #    GAWeb['all'] = GAWeb['all'].astype(int)
        #    GACar['sum(totalEvents)'] = GACar['sum(totalEvents)'].astype(int)

        #    ws = wb.worksheets[4]

        #    sheet5.append(end_date) #日期
        #    sheet5.append(int(GAWeb['visitor'][0])) #到站人次
        #    sheet5.append(int(GAWeb['New_visitor'][0])) #到站新客
        #    sheet5.append(int(GAWeb['all'][0])) #總瀏覽數
        #    sheet5.append(int(GACar['sum(totalEvents)'][0])) #物件總瀏覽數
        #    ws.append(sheet5)
        #else:
        #    for i in range(5):
        #        sheet5.append('')

        # Save the worksheet
        
        #嘗試合併Excel
        wb_a = load_workbook('All_Sales_過戶驗證清單.xlsx')
        sheet_a1 = wb_a.worksheets[0]  # 驗證過戶名單
        sheet_a2 = wb_a.worksheets[1]  # 本月累計驗證過戶名單
        
        while len(wb.worksheets) < 6:
            wb.create_sheet()

        # 取得目標工作簿中的第五和第六個工作表
        sheet_b5 = wb.worksheets[4]
        sheet_b6 = wb.worksheets[5]
        
        #清空目標工作表的內容
        sheet_b5.delete_rows(2, sheet_b5.max_row)
        sheet_b6.delete_rows(2, sheet_b6.max_row)
        
        def copy_worksheet(source_sheet, target_sheet):
            for row in source_sheet.iter_rows():
                for cell in row:
                    new_cell = target_sheet.cell(row=cell.row, column=cell.col_idx, value=cell.value)
                    if cell.has_style:
                        new_cell.font = copy(cell.font)
                        new_cell.border = copy(cell.border)
                        new_cell.fill = copy(cell.fill)
                        new_cell.number_format = copy(cell.number_format)
                        new_cell.protection = copy(cell.protection)
                        new_cell.alignment = copy(cell.alignment)
                    if cell.hyperlink:
                        new_cell.hyperlink = copy(cell.hyperlink)
                    if cell.comment:
                        new_cell.comment = copy(cell.comment)
                    
        copy_worksheet(sheet_a1, sheet_b5)
        copy_worksheet(sheet_a2, sheet_b6)
        sheet_b5.title = "本日驗證過戶名單"
        sheet_b6.title = "本月累計驗證過戶名單"

        wb.save('abc好車網_架上台數報表_企劃_20240822RomanTest.xlsx')
        print('本日驗證過戶名單 及 本月累計驗證過戶名單 合併完成')
        #=========================== Load sales file for 高階主管      
        wb = load_workbook('abc好車網_架上台數報表_20240822RomanTest.xlsx')

        #Sheet 1
        ws_main = wb.worksheets[0]

        #Top table

        ## 1.營運報表
        #Add today date
        date = ws_main.cell(row=1, column=1)
        date.value = 'abc好車網_營運報表_(' + end_date +')'

        #本月架上台數
        addValueToExcel(ws_main, carThisMonth, -1, -2)
        #本月淨增加
        addValueToExcel(ws_main, carIncrease, -1, -1)
        #上月架上台數
        addValueToExcel(ws_main, carLastMonth, -1, 0)

        ## 2.營收報表
        # Add today date
        date = ws_main.cell(row=11, column=1)
        date.value = 'abc好車網_營收報表_(' + end_date +')'

        date = ws_main.cell(row=12, column=2)
        #date.value = end_date[5:]
        date.value = end_date[5:] + '\n過戶台數'

        date = ws_main.cell(row=12, column=4)
        #date.value = end_date[5:] + '\n訂單筆數'
        date.value = end_date[5:] + '\n過戶收費金額'

        #當日過戶收費
        #當日過戶收費金額
        #q5 = ws_main.cell(row=13, column=2)
        #q5.value = str(int(q5rtn['當日過戶收費'][0])) + '元' 
        #q5.value = str(format(int(q5rtn['當日過戶收費'][0]), ',d')) + '元' 
        q5 = ws_main.cell(row=13, column=4)
        #q5.value = str(format(int(q5rtn['當日過戶收費金額'][0]), ',d')) + '元'
        q5.value = f"{int(q5rtn['當日過戶收費金額'][0].item()):,d}元"
        print( '當日過戶收費金額: ' + f"{int(q5rtn['當日過戶收費金額'][0].item()):,d}元")
        # 打印結果以確認
        
        #當月過戶收費
        #當月累計過戶金額
        #q6 = ws_main.cell(row=13, column=3)
        #q6.value = str(int(q6rtn['當月過戶收費'][0])) + '元' 
        #q6.value = str(format(int(q6rtn['當月過戶收費'][0]), ',d')) + '元'
        q6 = ws_main.cell(row=13, column=5)
        #q6.value = str(format(int(q6rtn['當月累計過戶金額'][0]), ',d')) + '元'
        q6.value = f"{int(q6rtn['當月累計過戶金額'][0].item()):,d}元"
        print('當月累計過戶金額: ' + f"{int(q6rtn['當月累計過戶金額'][0].item()):,d}元")
        #當日欠費
        #q7 = ws_main.cell(row=14, column=2)
        #q7.value = str(int(q7rtn['當日應收催收款'][0])) + '元' 
        #q7.value = str(format(int(q7rtn['當日應收催收款'][0]), ',d')) + '元'
        q7 = ws_main.cell(row=14, column=4)
        #q7.value = str(format(int(q7rtn['當日應收催收款'][0]), ',d')) + '元'
        q7.value = f"{int(q7rtn['當日應收催收款'][0].item()):,d}元"
        print('當日應收催收款: ' + f"{int(q7rtn['當日應收催收款'][0].item()):,d}元")
        #當月欠費
        #q8 = ws_main.cell(row=14, column=3)
        #q8.value = str(int(q8rtn['當月應收催收款'][0])) + '元' 
        #q8.value = str(format(int(q8rtn['當月應收催收款'][0]), ',d')) + '元'
        q8 = ws_main.cell(row=14, column=5)
        #q8.value = str(format(int(q8rtn['當月應收催收款'][0]), ',d')) + '元'
        q8.value = f"{int(q8rtn['當月應收催收款'][0].item()):,d}元"
        print('當月應收催收款: ' + f"{int(q8rtn['當月應收催收款'][0].item()):,d}元")

        #當日過戶訂單數
        #當日過戶台數
        #q9 = ws_main.cell(row=13, column=4)
        #q9.value = str(int(q5rtn['當日訂單筆數'][0])) + '筆' 
        #q9.value = str(format(int(q5rtn['當日訂單筆數'][0]), ',d')) + '筆' 
        q9 = ws_main.cell(row=13, column=2)
        #q9.value = str(format(int(q5rtn['當日過戶台數'][0]), ',d')) + '筆'
        q9.value = f"{int(q5rtn['當日過戶台數'][0].item()):,d}台"
        print('當日過戶台數: ' + f"{int(q5rtn['當日過戶台數'][0].item()):,d}台")
        #當月過戶訂單數
        #當月累計過戶台數
        #q10 = ws_main.cell(row=13, column=5)
        #q10.value = str(int(q6rtn['當月訂單筆數'][0])) + '筆' 
        #q10.value = str(format(int(q6rtn['當月訂單筆數'][0]), ',d')) + '筆'
        q10 = ws_main.cell(row=13, column=3)
        #q10.value = str(format(int(q6rtn['當月累計過戶台數'][0]), ',d')) + '筆'
        q10.value = f"{int(q6rtn['當月累計過戶台數'][0].item()):,d}台"
        print('當月累計過戶台數: ' + f"{int(q6rtn['當月累計過戶台數'][0].item()):,d}台")
        #當日欠費訂單數
        #q11 = ws_main.cell(row=14, column=4)
        #q11.value = str(int(q7rtn['當日訂單筆數'][0])) + '筆' 
        #q11.value = str(format(int(q7rtn['當日訂單筆數'][0]), ',d')) + '筆' 
        q11 = ws_main.cell(row=14, column=2)
        #q11.value = str(format(int(q7rtn['當日訂單筆數'][0]), ',d')) + '筆'
        q11.value = f"{int(q7rtn['當日訂單筆數'][0].item()):,d}台"
        print('當日訂單筆數: ' + f"{int(q7rtn['當日訂單筆數'][0].item()):,d}台")
        #當月欠費訂單數
        #q12 = ws_main.cell(row=14, column=5)
        #q12.value = str(int(q8rtn['當月訂單筆數'][0])) + '筆' 
        #q12.value = str(format(int(q8rtn['當月訂單筆數'][0]), ',d')) + '筆' 
        q12 = ws_main.cell(row=14, column=3)
        #q12.value = str(format(int(q8rtn['當月訂單筆數'][0]), ',d')) + '筆'
        q12.value = f"{int(q8rtn['當月訂單筆數'][0].item()):,d}台"
        print('當月訂單筆數: ' + f"{int(q8rtn['當月訂單筆數'][0].item()):,d}台")


        ## 3.各區架上台數天數
        #各區架上台數天數======2022/08/29
        addValueToExcel(ws_main, df_zone['30天以內'].tolist(), 16, -2)
        addValueToExcel(ws_main, df_zone['31-60天'].tolist(), 16, -1)
        addValueToExcel(ws_main, df_zone['61-90天'].tolist(), 16, 0)
        addValueToExcel(ws_main, df_zone['91-180天'].tolist(), 16, 1)
        addValueToExcel(ws_main, df_zone['181-360天'].tolist(), 16, 2)
        addValueToExcel(ws_main, df_zone['361天以上'].tolist(), 16, 3)

        ## 4.TL
        # Add today date
        date = ws_main.cell(row=30, column=1)
        date.value = 'abc好車網_架上台數報表_(' + end_date +')'

        #TL新上架目標台數
        #addValueToExcel(ws_main, df_down['TL新上架目標台數'].tolist(), 28, -1)
        #TL新上架實績台數
        #addValueToExcel(ws_main, df_down['TL新上架實績台數'].tolist(), 28, 0)       
        #TL新上架達成率
        #addValueToExcel(ws_main, df_down['TL新上架達成率'].tolist(), 28, 1)

        #TL過戶收費目標台數
        addValueToExcel(ws_main, df_down['TL過戶收費目標台數'].tolist(), 28, -1)
        #TL過戶收費實績台數
        addValueToExcel(ws_main, df_down['TL過戶收費實績台數'].tolist(), 28, 0)
        #TL過戶收費金額
        addValueToExcel(ws_main, df_down['TL過戶收費金額'].tolist(), 28, 1)    
        #TL過戶收費台數達成率
        addValueToExcel(ws_main, df_down['TL過戶收費台數達成率'].tolist(), 28, 2)
        #TL本月新增家數
        addValueToExcel(ws_main, df_down['TL本月新增家數'].tolist(), 28, 3)
        #TL本月新上台數
        addValueToExcel(ws_main, df_down['TL本月會員新上台數'].tolist(), 28, 4)
        
        ## 5.Sales 2022-10-12 Disable
        #新上架目標台數
        #addValueToExcel(ws_main, df['新上架目標台數'].tolist(), 36, -2)
        #新上架實績台數
        #addValueToExcel(ws_main, df['新上架實績台數'].tolist(), 36, -1)       
        #新上架達成率
        #addValueToExcel(ws_main, df['新上架達成率'].tolist(), 36, 0)

        #過戶收費目標台數
        addValueToExcel(ws_main, df['過戶收費目標台數'].tolist(), 36, -2)
        #過戶收費實績台數
        addValueToExcel(ws_main, df['過戶收費實績台數'].tolist(), 36, -1)
        #過戶收費金額
        addValueToExcel(ws_main, df['過戶收費金額'].tolist(), 36, 0)    
        #過戶收費台數達成率
        addValueToExcel(ws_main, df['過戶收費台數達成率'].tolist(), 36, 1)

        #廣宣銷售目標
        addValueToExcel(ws_main, df['廣宣銷售目標'].tolist(), 36, 2)
        #廣宣銷售實績
        addValueToExcel(ws_main, df['廣宣銷售實績'].tolist(), 36, 3)
        #廣宣達成率
        addValueToExcel(ws_main, df['廣宣達成率'].tolist(), 36, 4)

        #本月新增家數
        addValueToExcel(ws_main, df['本月新增家數'].tolist(), 36, 5)
        #本月新上台數
        addValueToExcel(ws_main, df['本月會員新上台數'].tolist(), 36, 6)
      
        # Save the worksheet
        wb.save('abc好車網_架上台數報表_20240822RomanTest.xlsx')

        print('PART 4 all done!')

        #輸出圖檔
        #append_to_graph(carThisMonth, sheet5)

    except Exception as e:
        msg = '{0}: PART 4: 數據寫入出現錯誤! The error message : {1}'.format(script, e)
        send_daily_report_error(script='{}'.format(script), message=msg)
        print(msg)
        sys.exit(1)
