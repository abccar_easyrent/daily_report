# coding: utf-8
import pandas as pd
import numpy as np
import os
import sys

sys.path.insert(0, '/home/hadoop/jessielu/')

import matplotlib as mpl
from openpyxl import load_workbook
from openpyxl.utils.dataframe import dataframe_to_rows

from Module.ConnFunc import read_mssql
from Module.EmailFuncHAA import send_HAA_report_error, send_haa_report
from Module.drawExcel import header, target, addValueVertical


path = '/home/hadoop/jessielu/report_sales/files/'

os.chdir(path)

from datetime import datetime, timedelta, date
from dateutil.relativedelta import relativedelta     

now = date.today() + relativedelta(days=1)
# now = date.today()

# # 建立日期區間
# start_date = (datetime.today() - timedelta(2)).strftime("%Y-%m-%d")
# end_date = (datetime.today() - timedelta(1)).strftime("%Y-%m-%d")

# Part 1 : 讀取數據
def read_data():
    query = f"""
    select 
         car.createdate as '匯入日期',
         DATEDIFF(DAY,car.CreateDate,GETDATE()) as '匯入天數',
         member.Email as 'abc帳號(email)',
         member.MemberID as 'abc會員編號',
         car.SourceKey as 'HAA物件編號',
         car.CarID as 'abc物件編號',
         CASE WHEN (
             car.Status = 1 and GETDATE() between car.StartDate and car.EndDate
         ) THEN '是' 
         ELSE '否'
         END 
         as '是否上架', 
         member.CarDealerName as '車商名稱',
         member.CarDealerContactPerson as '車商聯絡人'
    from ABCCar.dbo.car, ABCCar.dbo.Member member
    where 
         member.MemberID = car.MemberID and
         car.Source = 'HAA' and
         DATEDIFF(DAY,car.CreateDate,GETDATE()) <= 25
    order by car.CreateDate
    """

    haa = read_mssql(query, 'ABCCar')
    
    return haa

# Part 2: 寫入excel
def append_to_excel(haa):
    
    # 將資料寫入工作表
    wb = load_workbook(path + 'haa.xlsx')
    
    ws = wb.worksheets[0]
    ws.delete_rows(2, 100000)

    n=1
    for r in dataframe_to_rows(haa, index=False, header=False):
        m = 1
        for i in r:
            grid = ws.cell(row=1+n, column=m)
            grid.value = i
            m+=1
        n +=1
        
    # Save the worksheet
    wb.save('haa.xlsx')
    
    
if __name__ == "__main__":

    now = date.today()
    print(now)

    try:
        haa = read_data()
        msg = 'Part 1: 讀取資料正確'
        print(msg)
    except Exception as e:
        msg = 'PART 1: 讀取數據發生錯誤. The error message : {}'.format(e)
        send_HAA_report_error(msg)
        print(msg)
        sys.exit(1)
        
    try:
        append_to_excel(haa)
        msg = 'Part 2: 數據寫入Excel OK'
        print(msg)
    except Exception as e:
        msg = 'PART 2: 數據寫入Excel出現錯誤. The error message : {}'.format(e)
        send_HAA_report_error(msg)
        print(msg)
        sys.exit(1)
        
    try:
        send_haa_report(receiver='g', path=path)
        msg = 'Part 3: 寄送HAA日報OK'
        print(msg)
    except Exception as e:
        msg = 'Part 3: 寄送HAA日報發生錯誤. The error message : {}'.format(e)
        send_HAA_report_error(msg)
        print(msg)
        sys.exit(1)