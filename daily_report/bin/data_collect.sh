#!/bin/bash

#Step 1: ga_toMySQL.py: Connect to Google API and fetch events data -> bidb.ga
#Step 2: gapv_toMySQL.py: Connect to Google API and fetch pageviews data -> bidb.gapv
#Step 3: countGA_Session.py: Connect to Google API and fetch users, newUsers, total pageviews
#Step 4: virtual_call.py: webscrape and download the csv file of virtual call

set -e  #terminate if error occur.Note: there will be no notice.
export PATH=$PATH:/home/hadoop/jessielu/daily_report/bin

date -d today

echo The bash script is to fetch ga and virtual call data
echo Step 1:ga_toMySQL.py is running!
#time (/home/hadoop/anaconda3/bin/python '/home/hadoop/jessielu/daily_report/scripts/ga_toMySQL.py')
time (/home/hadoop/anaconda3/bin/python '/home/hadoop/jessielu/daily_report/scripts/ga4_toMySQL_new.py')
echo Step 1 finished!

echo Step 2:gapv_toMySQL.py is running!
#time (/home/hadoop/anaconda3/bin/python '/home/hadoop/jessielu/daily_report/scripts/gapv_toMySQL.py')
time (/home/hadoop/anaconda3/bin/python '/home/hadoop/jessielu/daily_report/scripts/ga4pv_toMySQL_new.py')
echo Step 2 finished!

echo Step 3:countGA_Session.py is running!
#time (/home/hadoop/anaconda3/bin/python '/home/hadoop/jessielu/daily_report/scripts/countGA_Session.py')
time (/home/hadoop/anaconda3/bin/python '/home/hadoop/jessielu/daily_report/scripts/countGA4_Session_new.py')
echo Step 3 finished!

# echo Step 4:virtual_call.py is running!
# time (/home/hadoop/anaconda3/bin/python '/home/hadoop/jessielu/daily_report/scripts/virtual_call.py')
# echo Step 4 finished!
