#!/bin/bash
# This script is processing and calculating figures for MY DATA services on ABCCar.
# Read in data : Car_New, status_new, mstatus_vip, ga_main, Brand, index_color
# Output: write out GAboard, All_sales, Group_performances and send data to ABCCar master DB

export PATH=$PATH:/home/hadoop/jessielu/daily_report/scripts/

date -d today
echo 'start running client_board.py'
time (/home/hadoop/anaconda3/bin/python '/home/hadoop/jessielu/daily_report/scripts/client_board.py')
echo 'end of script'
