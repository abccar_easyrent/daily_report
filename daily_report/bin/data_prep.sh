#!/bin/bash

#This script is to process the data for daily report and PowerBI
#Step 1: mem_location.py - get a more complete list of members' location, mainly for virtual call daily report
#Step 2: mkt_original.py - process the data from Car_New/Member_New to car_all/mem_all for PowerBI/report
#Step 3: GA_p.py - fetch bidb.ga/gapv, add Category, VIPLeve, CampaignID columns / fetch status_new, add CampaignID column

set -e  #terminate if error occur.Note: there will be no notice. 
export PATH=$PATH:/home/hadoop/jessielu/daily_report/bin

date -d today

echo Step 1:mem_location.py is running!
time (/home/hadoop/anaconda3/bin/python '/home/hadoop/jessielu/daily_report/scripts/mem_location.py')
echo Step 1 finish!

echo Step 2:mkt_original.py is running!
time (/home/hadoop/anaconda3/bin/python '/home/hadoop/jessielu/daily_report/scripts/mkt_original.py') 
echo Step 2 finish!

echo Step 3:GA.py is running!
time (/home/hadoop/anaconda3/bin/python '/home/hadoop/jessielu/daily_report/scripts/GA_p.py')
echo Step 3 finish!

