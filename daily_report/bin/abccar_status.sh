#!/bin/bash
#abccar_status_update.py: This script is to record the status of carid/member of ABCCAR and save data to AWS MySQL

export PATH=$PATH:/home/hadoop/jessielu/daily_report/bin
set -e #terminate if error occur. Note: there will be no notice
date -d today

echo abccar_status_update.py starts running!
time (/home/hadoop/anaconda3/bin/python '/home/hadoop/jessielu/daily_report/scripts/abccar_status_update.py')
echo abccar_status_update.py finished!
