#!/bin/bash
# This script is to produce sales daily report and virtual call report
# Step 1: daily_vc.py - process virtual call data, write to excel file, send out through Email
# Step 2: sales_daily_update.py - collect data and prepare the report, send out through Email

export PATH=$PATH:/home/hadoop/jessielu/daily_report/bin

set -e # terminate if error occurred
date -d today

#echo daily_vc.py starts to run!
#time (/home/hadoop/anaconda3/bin/python '/home/hadoop/jessielu/daily_report/scripts/daily_vc.py')
#echo daily_vc.py finished!

echo sales_daily_update.py starts to run!
time (cd /home/hadoop/jasminewang && /home/hadoop/anaconda3/bin/pipenv run python '/home/hadoop/jessielu/daily_report/scripts/sales_daily_update.py')
echo sales_daily_update.py finished!
