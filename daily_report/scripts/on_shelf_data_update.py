
# coding: utf-8

# In[20]:


from definition import read_mssql, to_mysql
from datetime import datetime, timedelta
import pandas as pd


# In[16]:


query="""
SELECT member.[MemberID], member.[Category], member.[VIPLevel], count(*) as ONshelf
FROM [ABCCar].[dbo].[Car] car WITH(NOLOCK)
INNER JOIN [ABCCar].[dbo].[Member] member WITH(NOLOCK)
ON
car.[MemberID] = CAST(member.MemberID AS bigint)
AND
member.Status = 1
INNER JOIN [ABCCar].[dbo].[CarModel] carModel WITH(NOLOCK)
ON  
carModel.[IsEnable] = 1 
AND car.[BrandID] = carModel.[BrandID] 
AND car.[SeriesID] = carModel.[SeriesID] 
AND car.[CategoryID] = carModel.[CategoryID] 
LEFT JOIN [ABCCar].[dbo].[GridOrderItem]  gridOrderItem WITH(NOLOCK)
ON
car.[CarID] = gridOrderItem.[CarId]
AND gridOrderItem.[Status] ='Y'
AND gridOrderItem.[CarId] IS NOT NULL
AND GETDATE() >=gridOrderItem.[StartDate]
AND GETDATE() <= gridOrderItem.[EndDate]
LEFT JOIN [ABCCar].[dbo].[CampaignItem] campaignItem WITH(NOLOCK)
ON
car.[CarID] = campaignItem.[CarID]
AND car.[MemberID] = campaignItem.[MemberID]
AND campaignItem.[CampaignID] = 0
AND campaignItem.[Status] = 1
WHERE
member.[Category] = 3
AND
car.[Status] IN (0)
AND
car.[ModifyDate] >= DATEFROMPARTS(2018,6,20)
AND 
DATEDIFF(YEAR, car.[ModifyDate],GETDATE()) <= 1
GROUP BY
member.[MemberID], member.[Category], member.[VIPLevel]
"""
all_on_shelf=read_mssql(query, 'ABCCar')


# In[22]:


today = datetime.today() - timedelta(0)
t_day = today.strftime("%Y-%m-%d")

all_on_shelf['Date']=t_day
all_on_shelf=all_on_shelf.groupby(['Date', 'Category','VIPLevel'])['ONshelf'].sum().reset_index()

to_mysql('report', all_on_shelf, 'on_shelf')
#to_mysql('report', all_on_shelf, 'on_shelf_test')
