# coding: utf-8
import sys
sys.path.insert(0, '/home/hadoop/jessielu/')
import re
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
from Module.EmailFunc import send_error_message
# from Module.HelpFunc import display_set
from Module.ConnFunc import read_mysql, to_mysql, read_test_mssql
from datetime import datetime, timedelta

# mpl.pyplot.switch_backend('agg')
# display_set(200, 30)

# 建立日期區間
#start_date = (datetime.today() - timedelta(0)).strftime("%Y-%m-%d")
#end_date = (datetime.today() + timedelta(1)).strftime("%Y-%m-%d")

start_date = (datetime.today() - timedelta(2)).strftime("%Y-%m-%d")
end_date = (datetime.today() - timedelta(1)).strftime("%Y-%m-%d")

# # 補發用
# start_date = '2022-05-12'
# end_date = '2022-05-13'

# 讀取數據
def ReadData():

    gaev_sg = f"""
    select date, medium, sourceMedium, eventAction, CarID carid, totalEvents from bidb.ga 
    where date >= '{start_date}' and date < '{end_date}' and eventAction not like '%%分享%%'
    """
    gapv_sg = f"""
    select date, medium, sourceMedium, pagePathLevel1 eventAction, pagePathLevel2 carid, pageviews totalEvents 
    from bidb.gapv where date >= '{start_date}' and date < '{end_date}'
    """
    car_all_sg = f"""
    SELECT CarID carid, MemberID, Email from report.car_all where Email != '0'
    """
    mstatus_vip_sg = f"""
    select Date date, Email, Category, VIPLevel from report.mstatus_vip where date >= '{start_date}' 
    and date < '{end_date}'
    """
    mstatus_other_sg = f"""
    select Email, Category, VIPLevel from report.mem_all where Category in (2) and Status = 1
    """
    car_status_sg = f"""
    select * from report.status_new where date >= '{start_date}' and date < '{end_date}'
    """
    campaign_sg = f"""
    select convert(varchar, CreateDate, 23) AS createdate, CarID carid, CampaignID from ABCCar.dbo.CampaignItem
    where CampaignID = 6 and Status = 1
    """
    # 執行讀取程式
    gaev = read_mysql(gaev_sg, 'bidb')
    gapv = read_mysql(gapv_sg, 'bidb')
    car_all = read_mysql(car_all_sg, 'report')
    mstatus_vip = read_mysql(mstatus_vip_sg, 'report')
    mstatus_oth = read_mysql(mstatus_other_sg, 'report')
    cstatus = read_mysql(car_status_sg, 'report')
    campaign = read_test_mssql(campaign_sg, 'ABCCar')

    # 檢查筆數是否為0
    tbl_in_list = pd.DataFrame({'gaev_rows': [len(gaev)],
                                'gapv_rows': [len(gapv)],
                                'car_all_rows': [len(car_all)],
                                'mstatus_vip_rows': [len(mstatus_vip)],
                                'mstatus_oth_rows': [len(mstatus_oth)],
                                'cstatus_rows': [len(cstatus)]})
                                # 'campaign_rows': [len(campaign)]})
    print(tbl_in_list)

    return gaev, gapv, car_all, mstatus_vip, mstatus_oth, cstatus, campaign, tbl_in_list


# GA 數據資料清整
# Step 1: DELETE SPECIAL CHARACTER IN eventAction/CarID columns
def clear_ga(gapv_dt, gaev_dt):

    gapv = gapv_dt.copy()
    gaev = gaev_dt.copy()

    
    ## 把carid欄位中有'】'篩選出
    #gaev = gaev[gaev.carid.str.contains('】')]
    ## 汲取出carid
    #gaev.carid = gaev.carid.apply(lambda x: x.split('】', 1)[1])
    
    #把carid欄位中有符合7位數值篩選出
    gaev = gaev[gaev.carid.str.contains(r'^\d{7}$')]

    ga = gapv.append(gaev)
    # 刪除特殊/文字--->確保carid為7位數
    ga.carid = ga.carid.apply(lambda x: re.sub('[^A-Za-z0-9]+', '', x)) #刪除special character
    ga.carid = ga.carid.apply(lambda x: re.sub('([^\x00-\x7F])+','', x)) #刪除中文字
    ga.carid = ga.carid.apply(lambda x: re.sub(r'[a-z]+','', x)) #刪除英文字
    ga.carid = ga.carid.apply(lambda x: re.sub(r'[A-Z]+','', x)) #刪除英文字
    ga.carid = ga.carid.apply(lambda x: x[:7])  # 刪除超過7位數
    print('Delete carid colums is empty after prep:' + str(len(ga.loc[ga.carid =='', :])))
    ga = ga.loc[ga.carid!='', :]

    return ga


# Step 2. ADD Category/VIPLevel的值
def add_category_viplevel(ga_dt, car_all_dt):

    ga = ga_dt.copy()
    car_all = car_all_dt.copy()
    car_all.carid = car_all.carid.astype(int).astype(str)
    mstatus_o = mstatus_oth.copy()
    mstatus_o['date'] = start_date
    mstatus = mstatus_vip.append(mstatus_o)

    ga = pd.merge(ga, car_all[['carid', 'Email']], on='carid', how='left')
    ga = pd.merge(ga, mstatus, on=['date', 'Email'], how='left')

    return ga

# Step 3. ADD CampaignID to GA
def add_campaignid_ga(ga_dt):

    ga = ga_dt.copy()
    camp = campaign.copy()
    camp.carid = camp.carid.astype(str)

    ga = pd.merge(ga, camp, on='carid', how='left')
    ga.loc[((ga.CampaignID == 6) & (ga.date >= ga.createdate)), 'CampaignID_a'] = 6
    ga.CampaignID_a.fillna(0, inplace=True)
    ga.drop(columns=['createdate', 'CampaignID'], inplace=True)
    ga.rename(columns={'CampaignID_a': 'CampaignID'}, inplace=True)

    return ga

# Step 4. ADD CampaignID to car status
def add_campaignid_to_status(status_dt):

    status = status_dt.copy()
    camp = campaign.copy()

    status = pd.merge(status, camp, left_on='CarID', right_on='carid', how='left')
    status.loc[((status.CampaignID == 6) & (status.Date >= status.createdate)), 'CampaignID_a'] = 6
    status.CampaignID_a.fillna(0, inplace=True)
    status.drop(columns=['createdate', 'CampaignID', 'carid'], inplace=True)
    status.rename(columns={'CampaignID_a': 'CampaignID'}, inplace=True)

    return status


def send_to_db(data, tbl_name):
    num = 10000
    loop_no = round(len(data) / num) + 1
    for i in range(0, loop_no):
        table = data[i * num:i * num + num]
        to_mysql('report', table, tbl_name, 'append')


if __name__ == "__main__":

    script_subject = 'GA_p.py'

    try:
        gaev, gapv, car_all, mstatus_vip, mstatus_oth, cstatus, campaign, tbl_in_list = ReadData()
        if (0 in tbl_in_list.values) == True:
            msg = 'PART 1: 數據中有0值, 此程式停止執行!'
            print(msg)
            # send_daily_report_error(msg)
            sys.exit(1)
        else:
            print('PART 1: 數據正確且無0值')

    except Exception as e:
        msg = '{0}: PART 1: 數據連線讀取失敗! The error message : {1}'.format(script_subject, e)
        send_error_message(script='{}'.format(script_subject), message=msg)
        print(msg)
        sys.exit(1)

    try:
        ga_1 = clear_ga(gapv, gaev)
        ga_2 = add_category_viplevel(ga_1, car_all)
        ga_3 = add_campaignid_ga(ga_2)
        print('PART 2: GA數據處理正確')
    except Exception as e:
        msg = '{0}: PART 2: GA數據處理中發生錯誤! The error message : {1}'.format(script_subject, e)
        send_error_message(script='{}'.format(script_subject), message=msg)
        print(msg)
        sys.exit(1)

    try:
        send_to_db(ga_3, 'ga_main')
        print('PART 3: GA數據處理正確')
    except Exception as e:
        msg = '{0}: PART 3: 寫入GA處理後的數據至資料庫時發生錯誤! The error message : {1}'.format(script_subject, e)
        send_error_message(script='{}'.format(script_subject), message=msg)
        print(msg)
        sys.exit(1)

    try:
        car_status = add_campaignid_to_status(cstatus)
        print('PART 4: Car Status 數據處理正確')
    except Exception as e:
        msg = '{0}: PART 4: Car Status 數據處理中發生錯誤! The error message : {1}'.format(script_subject, e)
        send_error_message(script='{}'.format(script_subject), message=msg)
        print(msg)
        sys.exit(1)
        
    try:
        send_to_db(car_status, 'status_newer')
        print('PART 4: 寫入Car Status處理後的數據至資料庫正確')

    except Exception as e:
        msg = '{0}: PART 5: 寫入Car Status處理後的數據至資料庫時發生錯誤! The error message : {1}'.format(script_subject, e)
        send_error_message(script='{}'.format(script_subject), message=msg)
        print(msg)
        sys.exit(1)
