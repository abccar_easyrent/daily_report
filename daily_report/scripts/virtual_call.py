
# coding: utf-8

# In[1]:


import selenium
import time
import pandas as pd
import numpy as np
import sqlalchemy as sa
from selenium import webdriver
from selenium.webdriver.support.ui import Select
from datetime import datetime, timedelta
from time import strptime
from selenium import webdriver


# In[8]:


#開始日期=昨天的日期 ＃建立格式:August 30
start_date = datetime.today() - timedelta(1)
s_d = start_date.strftime("%B")+" "+start_date.strftime('%d')

#建立開始日期的月份 #格式:9
m_d = start_date.strftime("%m")
m_d = int(m_d)

#結束日期＝昨天的日期 ＃建立格式:August 30
end_date = start_date
e_d = end_date.strftime("%B")+" "+end_date.strftime('%d')

#建立結束日期的月份 #格式:9
l_d = end_date.strftime("%m")
l_d = int(l_d)

#---滑鼠爬蟲------
#滑鼠控制頁面設置及建立連結
def enable_downoad_headless(browser,download_dir):
    browser.command_executor._commands["send_command"] = ("POST", '/session/$sessionId/chromium/send_command')
    params = {'cmd':'Page.setDownloadBehavior',
    'params': {'behavior': 'allow', 'downloadPath': download_dir}}
    browser.execute("send_command", params)



options = webdriver.ChromeOptions()
options.add_argument('--headless')
#prefs = {'download.default_directory' : '/media/sf_R/VC/'}
#options.add_experimental_option('prefs', prefs)
driver = webdriver.Chrome("/home/hadoop/jessielu/daily_report/scripts/chromedriver", chrome_options = options)   #ubuntu須先安裝
url = "http://api.sunlinenp.com.tw/Report/index.aspx"
enable_downoad_headless(driver, "/home/hadoop/jessielu/daily_report/vc_raw")
driver.get(url)

#滑鼠頁面click位置建立
s_date="""//table[@id='cal_beginDate']//a[@title='{0}']""".format(s_d)
e_date="""//table[@id='cal_endDate']//a[@title='{0}']""".format(e_d)

#起始日期的月份（頁面元素） #格式：September 2018
ms1 = driver.find_element_by_xpath("//table[@id='cal_beginDate']//table[@class='query-item-input']/tbody/tr/td[2]").text

#起始日期-只取前三個字以便做時間轉換 #格式:Sep
ms2 = ms1[:3]

#起始日期-將月份轉成數字 #格式:Sep -> 9
ms = strptime(ms2,'%b').tm_mon

#結束日期的月份（頁面元素） #格式:September 2018
me1 = driver.find_element_by_xpath("//table[@id='cal_endDate']//table[@class='query-item-input']/tbody/tr/td[2]").text

#結束日期-只取前三個字以便做時間轉換 #格式:Sep
me2 = me1[:3]

#結束日期-將月份轉成數字 #格式:Sep -> 9
me = strptime(me2,'%b').tm_mon

#####################################################################
if m_d < ms:
    check=driver.find_element_by_xpath("//table[@id='cal_beginDate']//table[@class='query-item-input']/tbody/tr/td[1]/a").click()

if l_d < ms:
    check=driver.find_element_by_xpath("//table[@id='cal_endDate']//table[@class='query-item-input']/tbody/tr/td[1]/a").click()

start = driver.find_element_by_xpath(s_date).click()
end = driver.find_element_by_xpath(e_date).click()

l = d = driver.find_element_by_xpath("//div[@class='query-button']/input[@id='bt_submit']").click()
d = driver.find_element_by_xpath("//div[@class='query-button']/input[@id='bt_exportExcel']").click()


# In[9]:


#建立file讀取的文件名----
date = start_date
date_f = date.strftime("%Y%m%d")
time.sleep(5)
filepath = (''.join(["/home/hadoop/jessielu/daily_report/vc_raw/Export_", date_f, "_", date_f, ".csv"]))
virtual_dt = pd.read_csv(filepath)


# In[10]:


virtual_dt = virtual_dt[['代號','車商編號', '客戶來電', 'DID', '轉車家電話', '車輛編號(分機)', '起始時間', '結束時間',
                         '通話秒數', '通話金額', '狀態', '通話來源']]


# In[11]:


#刪除總計以及車商編號A0003欄位 #刪除內部測試電話:
virtual_dt = virtual_dt[:-1]

if virtual_dt['車商編號'].dtype != np.float64:
    virtual_dt = virtual_dt[virtual_dt.車商編號 != 'A0003']


# In[12]:


#轉欄位格式  ＃object -> int64
virtual_dt['車商編號'] = virtual_dt['車商編號'].map(int)

#資料整理----
date_c = date.strftime("%Y-%m-%d")

virtual_dt = virtual_dt.reset_index(drop=True)  #因為前面有刪除column, index need to reset, drop=T(represent not to show)
virtual_dt["車商編號"] = virtual_dt["車商編號"].map(int)
virtual_dt["日期"] = date_c
virtual_dt = virtual_dt[['日期']+list(virtual_dt.columns[:-1])]


driver.quit()

#轉入MySQL資料庫---
engine = sa.create_engine('mysql+pymysql://abccar:abccar@172.31.42.43:3306/report?charset=utf8', encoding='utf8')
#virtual_dt.to_sql('virtual', con=engine, if_exists = 'append', index = False)
virtual_dt.to_sql('virtual', con=engine, if_exists = 'append', index = False)

