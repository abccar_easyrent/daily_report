#設定連線----bidb
library(DBI)
library(RMySQL)
library(dplyr)
mysqlconn  <- dbConnect(MySQL(),user = 'abccar',password = 'abccar',
                        dbname='bidb',host='172.31.42.43')
dbSendQuery(mysqlconn, 'SET NAMES utf8')          #ubuntu的環境為utf8，避免亂碼

#---Car_New & Mem_New & Ga & Gapv檔----
yesterday  <- as.character(Sys.Date()-1)          #設定時間範圍
mysqlquery <- dbSendQuery(mysqlconn,paste0("select * from bidb.ga where date =", "\'", yesterday, "\'",
                                           "and eventAction not like '%分享%'"))
gaev       <- fetch(mysqlquery,n=-1)

mysqlquery <- dbSendQuery(mysqlconn,paste0("select * from bidb.gapv where date =", "\'", yesterday, "\'"))
gapv       <- fetch(mysqlquery,n=-1)

#結束連線 bidb---
dbDisconnect(mysqlconn)

#Pageviews數據處理 ----
gapv$pagePathLevel2 <- gsub("/","",gapv$pagePathLevel2) #去除/car/
#t <- ga.pvdata_2018[grep("[^0-9]", ga.pvdata_2018$pagePath),] #找出有8字以上

colnames(gapv) <- c("date", "medium", "sourceMedium", "eventAction", 
                              "carid", "totalEvents") #更改欄位名

#GA Events數據處理---- 
library(tidyr)
     

gaev                <- gaev[gaev$CarID!='(not set)',]   #delete (not set)
gaev                <- separate(gaev,col="CarID",into=c("OwnerID","CarID"),sep="】")
gaev                <- gaev[,-5]

#gaev                <- gaev[-grep("(not set)", gaev$CarID),]         #delete (not set)
#ga.evdata_2018$OwnerID <- gsub("【dealer=",ga.evdata_2018$OwnerID)
gaev$eventAction <- iconv(gaev$eventAction,"UTF-8","BIG-5")          #將中文decode from UTF-8 to BIG-5

#---寫入report DB----
mysqlconn  <- dbConnect(MySQL(),user = 'abccar',password = 'abccar',
                        dbname='report',host='172.31.42.43')
dbSendQuery(mysqlconn, 'SET NAMES big5')
dbWriteTable(mysqlconn, value = gapv, "ga2018", append = T, overwrite = F, row.names = FALSE) #BI refresh GA檔
dbWriteTable(mysqlconn, value = gaev, "ga2018", append = T, overwrite = F, row.names = FALSE) #BI refresh GA檔

#---結束連線 report---
dbDisconnect(mysqlconn)

#[備註程式碼]
#mem_original$Company[mem_original$Company == ""] <- NA
#mem_original <- mem_original[!is.na(mem_original$Company),]
#empty <- mem_good$Company[mem_good$Company == ""]                   #確認Empty Field