# -*- coding: utf-8 -*-
import sys
sys.path.insert(0, '/home/hadoop/jessielu/')
from apiclient.discovery import build
from oauth2client.service_account import ServiceAccountCredentials
from Module.EmailFunc import send_error_message
import re
import pandas as pd
import sqlalchemy as sa
import os
import time
from datetime import datetime, timedelta, date
from dateutil.relativedelta import relativedelta
import random
from apiclient.errors import HttpError

#原本
now = date.today()
#補發用
# now = (datetime.today() - relativedelta(days=1)).strftime('%Y-%m-%d')
# now = '2022-02-27'
print(now)

SCOPES = ['https://www.googleapis.com/auth/analytics.readonly']
os.chdir('/home/hadoop/jessielu/daily_report/scripts')
KEY_FILE_LOCATION = 'gaApiKey.json'
VIEW_ID = '177455238'

def initialize_analyticsreporting():
    """Initializes an Analytics Reporting API V4 service object.

    Returns:
        An authorized Analytics Reporting API V4 service object.
    """
    credentials = ServiceAccountCredentials.from_json_keyfile_name(
        KEY_FILE_LOCATION, SCOPES)

    # Build the service object.
    analytics = build('analyticsreporting', 'v4', credentials=credentials)
    return analytics

def get_report(analytics):
    """Queries the Analytics Reporting API V4.

    Args:
        analytics: An authorized Analytics Reporting API V4 service object.
    Returns:
        The Analytics Reporting API V4 response.
    """

    reports = []
    pageCnt = 1
    pageToken = None
    while True:
        data = analytics.reports().batchGet(
            body={
                'reportRequests': [{
                    'viewId': VIEW_ID,
                    'pageToken': pageToken,
                    'samplingLevel': 'LARGE',
                    'pageSize': 10000,
					'dateRanges': [{'startDate': str(now), 'endDate':str(now)}],
                    'metrics': [{'expression': 'ga:pageviews'}],
                    'dimensions': [
                        {'name': 'ga:date'},
                        {'name': 'ga:medium'},
                        {'name': 'ga:sourceMedium'},
                        {'name': 'ga:pagePathLevel1'},
                        {'name': 'ga:pagePathLevel2'}
                        #   {'name': 'ga:dimension2'}
                    ],
                    'dimensionFilterClauses': [{
                        'filters': [{
                            'dimensionName': 'ga:pagePathLevel1',
                            'operator': 'EXACT',
                            'expressions': ['/car/']
                        }]
                    }]
                }]
            }
        ).execute()
        reports.append(data)
        print('Get Page %i of data.' % (pageCnt))
        pageCnt += 1
        if 'nextPageToken' in data['reports'][0]:
            pageToken = data['reports'][0]['nextPageToken']
        else:
            break
  
    return reports

def makeRequestWithExponentialBackoff(analytics):
    for n in range(0, 5):
        print(n)
        try:
            return get_report(analytics)

        except Exception as error:
            print(error)
            time.sleep((2 ** n) + random.random())
  
def prep_data(rawData):
    rows = []
    zhPattern = re.compile(u'[\u4e00-\u9fa5]+')
    for data in rawData:
        data = data['reports'][0]
        
        cols = data['columnHeader']
        cols = cols['dimensions'] + [cols['metricHeader']['metricHeaderEntries'][0]['name']]
        cols = [re.sub('ga:','',x) for x in cols]
    
        data = data['data']['rows']
    
        for row in data:
            date = row['dimensions'][0]
            date = '-'.join([date[:4], date[4:6], date[6:]])
            row['dimensions'][0] = date
            rows.append(row['dimensions']+row['metrics'][0]['values'])

    final = pd.DataFrame(rows, columns=cols)
    final['sourceMedium'] = final['sourceMedium'].map(lambda x: 'other' if zhPattern.search(x) else x)
    final.pagePathLevel2 = final.pagePathLevel2.str[:8]
    return final    

def insert_mysql(data):
    engine = sa.create_engine('mysql+pymysql://abccar:abccar@172.31.42.43:3306/bidb?charset=utf8', encoding='utf8')
    data.to_sql('gapv',con=engine,if_exists='append', index=False)

def main():
    script = 'gapv_toMySQL.py'

    try:
        analytics = initialize_analyticsreporting()
    except Exception as e:
        msg = 'PART 1: 連線GA API 讀取失敗! The error message : {}'.format(e)
        send_error_message(script=script, message=msg)
        print(msg)
        sys.exit(1)

    print('PART 1: Finished!')

    try:
        response = makeRequestWithExponentialBackoff(analytics)

    except Exception as e:
        msg = 'PART 2: Fetch GA 資料出現錯誤! The error message : {}'.format(e)
        send_error_message(script=script, message=msg)
        print(msg)
        sys.exit(1)

    print('PART 2: Finished!')

    try:
        final = prep_data(response)
    except Exception as e:
        msg = 'PART 3: 清整GA資料出現錯誤! The error message : {}'.format(e)
        send_error_message(script=script, message=msg)
        print(msg)
        sys.exit(1)

    print('PART 3: Finished!')

    try:
        insert_mysql(final)
    except Exception as e:
        msg = 'PART 4: 寫入資料庫出現錯誤! The error message : {}'.format(e)
        send_error_message(script=script, message=msg)
        print(msg)
        sys.exit(1)


if __name__ == '__main__':
    main()







