# -*- coding: utf-8 -*-

import os
import sys
sys.path.insert(0, '/home/hadoop/jessielu/')
from Module.EmailFunc import send_error_message
import pandas as pd
import numpy as np
import sqlalchemy as sa
from datetime import date
from dateutil.relativedelta import relativedelta
import random
import time
from google.oauth2 import service_account
from google.analytics.data_v1beta import BetaAnalyticsDataClient
from google.analytics.data_v1beta.types import (
    DateRange,
    Dimension,
    Filter,
    FilterExpression,
    FilterExpressionList,
    Metric,
    RunReportRequest,
)

#原本
now = (date.today() - relativedelta(days=2)).strftime("%Y-%m-%d")
#補發用
# now = '2022-02-27'
print(now)

KEY_FILE_PATH = '/home/hadoop/jessielu/daily_report/scripts/'
#KEY_FILE_PATH = '/home/hadoop/'
KEY_FILE_LOCATION = 'ga4ApiKey.json'
property_id = "312585480"

def initialize_analyticsreporting():
    """Initializes an Analytics Reporting API V4 service object.

    Returns:
        An authorized Analytics Reporting API V4 service object.
    """
    credentials = service_account.Credentials.from_service_account_file(KEY_FILE_PATH + KEY_FILE_LOCATION)
	#Build the service object.
    client = BetaAnalyticsDataClient(credentials=credentials)

    return client

def get_report(client):
    """Queries the Analytics Reporting API V4.

    Args:
        analytics: An authorized Analytics Reporting API V4 service object.
    Returns:
        The Analytics Reporting API V4 response.
    """
    data = []
    pageOffset = 0
    while True:
        request = RunReportRequest(
                property=f"properties/{property_id}",
                dimensions=[Dimension(name = "date"),
                            Dimension(name="sessionMedium"),
                            Dimension(name="sessionSource"),
                            Dimension(name = "eventName"),
                            Dimension(name="pagepath")],
                metrics=[Metric(name="eventCount")],
                date_ranges=[DateRange(start_date=now, end_date=now)],
                limit = 10000,
                offset = pageOffset,
                dimension_filter=FilterExpression(
                    and_group=FilterExpressionList(
                        expressions=[
                            FilterExpression(
                                filter=Filter(
                                    field_name="eventName",
                                    string_filter=Filter.StringFilter(value="page_view"),
                                )
                            ),
                            FilterExpression(
                                filter=Filter(
                                    field_name="pagepath",
                                    string_filter=Filter.StringFilter(
                                        match_type=Filter.StringFilter.MatchType.CONTAINS,
                                        value="/car/"
                                    )

                                )
                           ),
                        ]
                    )
                ),
        )
        response = client.run_report(request)

        for row in response.rows:
            dimension_values = [value.value for value in row.dimension_values]
            metric_values = [value.value for value in row.metric_values]
            data.append(dimension_values + metric_values)

        if pageOffset > response.row_count:
            break
        else:
            pageOffset = pageOffset + 10000;

    columns = [dimension.name for dimension in response.dimension_headers] + [metric.name for metric in response.metric_headers]
    reports = pd.DataFrame(data, columns=columns)

    return reports

def prep_data(reports):
    reports['date'] = pd.to_datetime(reports['date'], format="%Y-%m-%d").apply(lambda x: x.strftime('%Y-%m-%d'))
    reports['CarID'] = np.where(reports['pagepath'].apply(str.lower).str.contains(r'/car/\d{7,}'), reports['pagepath'].astype(str).replace(['/','Car','car','dkgc'], '', regex=True), '0')
    reports.rename(columns = {'sessionMedium':'medium', 'sessionSource':'sourceMedium','pagepath':'pagePathLevel1','CarID':'pagePathLevel2','eventCount':'pageviews'}, inplace = True)
    reports.drop(['eventName'], axis=1, inplace=True)

    return reports

def makeRequestWithExponentialBackoff(analytics):
    for n in range(0, 5):
        print(n)
        try:
            return get_report(analytics)

        except Exception as error:
            print(error)
            time.sleep((2 ** n) + random.random())

def insert_mysql(data):
    engine = sa.create_engine('mysql+pymysql://abccar:abccar@172.31.42.43:3306/bidb?charset=utf8', encoding='utf8')
    data.to_sql('gapv',con=engine,if_exists='append', index=False)

def main():
    script = 'ga4pv_toMySQL_new.py'

    try:
        analytics = initialize_analyticsreporting()
    except Exception as e:
        msg = 'PART 1: 連線GA API 讀取失敗! The error message : {}'.format(e)
        send_error_message(script=script, message=msg)
        print(msg)
        sys.exit(1)

    print('PART 1: Finished!')

    try:
        response = makeRequestWithExponentialBackoff(analytics)

    except Exception as e:
        msg = 'PART 2: Fetch GA 資料出現錯誤! The error message : {}'.format(e)
        send_error_message(script=script, message=msg)
        print(msg)
        sys.exit(1)

    print('PART 2: Finished!')

    try:
        final = prep_data(response)
    except Exception as e:
        msg = 'PART 3: 清整GA資料出現錯誤! The error message : {}'.format(e)
        send_error_message(script=script, message=msg)
        print(msg)
        sys.exit(1)

    print('PART 3: Finished!')

    try:
        insert_mysql(final)
    except Exception as e:
        msg = 'PART 4: 寫入資料庫出現錯誤! The error message : {}'.format(e)
        send_error_message(script=script, message=msg)
        print(msg)
        sys.exit(1)


if __name__ == '__main__':
    main()
