# coding: utf-8
import sys
sys.path.insert(0, '/home/hadoop/jessielu/')
import numpy as np

from Module.EmailFunc import send_error_message
from Module.ConnFunc import read_mssql, to_mysql


# PART 1: Read in data
def read_in_data():
    mem_query="""
        select a.MemberID, a.Name, a.CompanyName, a.Category, a.VIPLevel, a.CarDealerCountryName, a.CarDealerDistrictName,
        a.CarDealerAddress, a.Address,
        b.CountryName, b.DistrictName, b.Address,
        c.MemberId, c.Mobile
        from ABCCar.dbo.Member a
        join ABCCarMemberCenter.dbo.Member b
        on a.MemberID = b.ID
        left outer join ABCCar.dbo.VirtualCodeMas c
        on a.MemberId = c.MemberId
        """
    mem = read_mssql(mem_query, 'ABCCarMemberCenter')
    return mem

# PART 2
def data_process(mem_dt):

    member = mem_dt.copy()
    # 補齊CarDealerCountryName (Location欄位)
    member.CarDealerCountryName = np.where(member.CarDealerCountryName.isnull(), member.CountryName,
                                           member.CarDealerCountryName)
    member.CarDealerCountryName = np.where(member.CarDealerCountryName.isnull(), member.CarDealerDistrictName,
                                           member.CarDealerCountryName)
    # 只鎖定車商身分
    member = member.loc[(member.Category == 3) | (member.Category == 4), :]

    # 整理地址資訊
    member.CarDealerCountryName = member.CarDealerCountryName.astype(str)
    replacements_ls = [("中和區", "新北市"), ("中華路", "新竹市"), ("北市南", "台北市"), ("臺北市", "台北市"),
                       ("中壢區", "桃園市"), ("神岡區", "台中市"), ("烏日區", "台中市"), ("左營區", "高雄市"),
                       ("士林區", "台北市"), ("羅斯福", "台北市"), ("汐止市", "新北市"), ("None", "未提供"),
                       ("在家", "未提供")]
    for old_wd, new_wd in replacements_ls:
        member.CarDealerCountryName = member.CarDealerCountryName.apply(lambda x: x.replace(old_wd, new_wd))
    member.loc[member.CarDealerCountryName=='', 'CarDealerCountryName'] = "未提供"

    # 選取所需欄位
    col_ls = ["MemberID", "Mobile", "CarDealerCountryName"]
    member = member.loc[:, col_ls]
    member.rename(columns={'MemberID':'MemberId'}, inplace=True)
    return member

# PART 3
def write_to_mysql(tbl):
    to_mysql('report', tbl, 'member_location', 'replace')


if __name__ == "__main__":

    script = 'mem_location.py'

    try:
        mem = read_in_data()
    except Exception as e:
        msg = '{0} PART 1: 數據連線讀取失敗! The error message : {1}'.format(script, e)
        send_error_message(script=script, message=msg)
        print(msg)
        sys.exit(1)

    try:
        member_loc = data_process(mem)
    except Exception as e:
        msg = '{0} PART 2: 數據清整失敗! The error message : {1}'.format(script, e)
        send_error_message(script=script, message=msg)
        print(msg)
        sys.exit(1)

    try:
        write_to_mysql(member_loc)
    except Exception as e:
        msg = '{0} PART 3:數據寫入失敗! The error message : {1}'.format(script, e)
        send_error_message(script=script, message=msg)
        print(msg)
        sys.exit(1)


