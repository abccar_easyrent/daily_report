import pandas as pd
from sklearn import preprocessing
import seaborn as sns
import sqlalchemy as sa
import matplotlib.pyplot as plt


# read data from mysql
def read_mysql(query, db_name):

    engineX = 'mysql+pymysql://abccar:abccar@172.31.42.43:3306/{0}?charset=utf8'.format(db_name)
    engine = sa.create_engine(engineX, encoding='utf8', connect_args={'charset':'utf8'})
    get_data = pd.read_sql(query, con=engine)

    return get_data

# read data from mysql
def read_testdb(query, db_name):

    engineX = 'mysql+pymysql://abccar:abccar@54.168.246.58:3306/{0}?charset=utf8'.format(db_name)
    engine = sa.create_engine(engineX, encoding='utf8', connect_args={'charset':'utf8'})
    get_data = pd.read_sql(query, con=engine)

    return get_data


def read_mydb(query, db_name):

    engineX = 'mysql+pymysql://root:abccar@172.16.221.143:3306/{0}?charset=utf8'.format(db_name)
    engine = sa.create_engine(engineX, encoding='utf8', connect_args={'charset': 'utf8'})
    get_data = pd.read_sql(query, con=engine)

    return get_data


# write to mysql
def to_mysql(db_name, data, db_tableName, method):

    engine = sa.create_engine('mysql+pymysql://abccar:abccar@172.31.42.43:3306/{0}?charset=utf8'.format(db_name))
    data.to_sql('{0}'.format(db_tableName), con=engine, if_exists = method, index = False) #method = 'replace' or 'append'


def to_mydb(db_name, data, db_tableName, method):

    engine = sa.create_engine('mysql+pymysql://root:abccar@172.16.221.143:3306/{0}?charset=utf8'.format(db_name))
    data.to_sql('{0}'.format(db_tableName), con=engine, if_exists = method, index = False) #method = 'replace' or 'append'

# read data from mssql
def read_mssql(query, db_name):

    engineX = 'mssql+pymssql://administrator:!abccar2020@pdmasterdb.cpambwr2r6oj.ap-northeast-1.rds.amazonaws.com/{0}?charset=utf8'.format(db_name)
    engine = sa.create_engine(engineX, encoding='utf8', connect_args={'charset':'utf8'})
    get_data = pd.read_sql(query, con=engine)

    return get_data

# write to sql server
def to_sql_server(db_name, data, db_tableName, method):

    engine = sa.create_engine('mssql+pymssql://administrator:!abccar2020@pdmasterdb.cpambwr2r6oj.ap-northeast-1.rds.amazonaws.com/{0}?charset=utf8'.format(db_name))
    data.to_sql('{0}'.format(db_tableName), con=engine, if_exists= method, index=False) #method = 'replace' or 'append'

# display format setting
def display_set(width, no_columns):

    pd.set_option('display.width', width)
    pd.set_option("display.max_columns", no_columns)

# 找重複值
def find_duplicate(df, indexColumn):

    duplicates = pd.concat(g for _, g in df.groupby(indexColumn) if len(g) > 1)
    return duplicates

# 標準化
def standardize_x(X_variables, dataframe):

    Standardize_columns = dataframe[X_variables]
    features = Standardize_columns.columns
    scaler = preprocessing.StandardScaler()
    dataframe[features] = scaler.fit_transform(dataframe[features])

    return dataframe

# pearson correlation & heatmap
def plot_corr(testData):

    correlation = testData.corr()
    plt.figure(figsize=(50, 30))
    sns.heatmap(correlation, vmax=5, square=True, annot=True, cmap='Blues')
    plt.title('Correlation between fearures')





