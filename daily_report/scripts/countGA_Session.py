# -*- coding: utf-8 -*-
"""
Created on Fri Nov 23 10:28:04 2018

@author: Abner
"""

import sys
sys.path.insert(0, '/home/hadoop/jessielu/')
from apiclient.discovery import build
from oauth2client.service_account import ServiceAccountCredentials
from Module.EmailFunc import send_error_message
import pandas as pd
import sqlalchemy as sa
from datetime import datetime, timedelta, date
from dateutil.relativedelta import relativedelta
import random
from apiclient.errors import HttpError
import time
import os

#原本
now = date.today()
#補發用
# now = (datetime.today() - relativedelta(days=1)).strftime('%Y-%m-%d')
# now = '2022-02-27'

print(now)

SCOPES=['https://www.googleapis.com/auth/analytics.readonly']
os.chdir('/home/hadoop/jessielu/daily_report/scripts')
KEY_FILE_LOCATION = 'gaApiKey.json'
VIEW_ID='177455238'

def initialize_analyticsreporting():
	"""Initializes an Analytics Reporting API V4 service object.

    Returns:
        An authorized Analytics Reporting API V4 service object.
    """
	credentials = ServiceAccountCredentials.from_json_keyfile_name(KEY_FILE_LOCATION, SCOPES)

	#Build the service object.
	analytics = build('analyticsreporting', 'v4', credentials=credentials)
	
	return analytics

def get_report(analytics):
	"""Queries the Analytics Reporting API V4.

    Args:
        analytics: An authorized Analytics Reporting API V4 service object.
    Returns:
        The Analytics Reporting API V4 response.
    """
	reports = []
	pageCnt = 1
	pageToken = None

	while True:
		data = analytics.reports().batchGet(
			body={
				'reportRequests': [{
					'viewId': VIEW_ID,
					'pageToken': pageToken,
					'samplingLevel': 'LARGE',
					'pageSize': 10000,
					'dateRanges': [{'startDate': str(now), 'endDate':str(now)}],					
					'metrics': [{'expression':'ga:users'},{'expression':'ga:newUsers'},{'expression':'ga:pageviews'}],
					'dimensions': [
						{'name': 'ga:date'},
					]
				}]
			}
		).execute()
		reports.append(data)
		print('Get Page %i of data.' % (pageCnt))
		pageCnt += 1
		
		if 'nextPageToken' in data['reports'][0]:
			print(data['reports'][0]['nextPageToken'])
			pageToken = data['reports'][0]['nextPageToken']
		else:
			break
	return reports

def makeRequestWithExponentialBackoff(analytics):
    for n in range(0, 5):
        print(n)
        try:
            return get_report(analytics)

        except Exception as error:
            print(error)
            time.sleep((2 ** n) + random.random())
    
def prep_data(reports):
	temp={}
	for i in range(len(reports)):
		for j in reports[i]['reports'][0]['data']['rows']:
			
			dateindex=0
			for x in j['dimensions']:
				if dateindex==1:
					if x=='New Visitor':
						temp[j['dimensions'][0][0:4]+'-'+j['dimensions'][0][4:6]+'-'+j['dimensions'][0][6:]]=[]
						for l in j['metrics'][0]['values']:
							temp[j['dimensions'][0][0:4]+'-'+j['dimensions'][0][4:6]+'-'+j['dimensions'][0][6:]].append(reports[i]['reports'][0]['data']['totals'][0]['values'][0])
							temp[j['dimensions'][0][0:4]+'-'+j['dimensions'][0][4:6]+'-'+j['dimensions'][0][6:]].append(l)
				dateindex=dateindex+1

	r=[]
	for i in range(len(reports)):
		for j in reports[i]['reports'][0]['data']['rows']:
			temp=[]
			dateindex=0
			for x in j['dimensions']:
				if dateindex==0:
					x=x[0:4]+'-'+x[4:6]+'-'+x[6:]
				dateindex=dateindex+1
				temp.append(x)
			for l in j['metrics'][0]['values']:
				temp.append(l)
			r.append(temp)
		
	columns=['QueryDate','visitor','New_visitor','all']

	df=pd.DataFrame(data=r,columns=columns)

	df['visitor'] = pd.to_numeric(df['visitor'])
	df['New_visitor'] = pd.to_numeric(df['New_visitor'])
	df['all'] = pd.to_numeric(df['all'])
	df['QueryDate'] = pd.to_datetime(df['QueryDate'])

	return df

def insert_mysql(data):
    engine = sa.create_engine('mysql+pymysql://abccar:abccar@172.31.42.43:3306/CollectGA?charset=utf8', encoding='utf8')
    data.to_sql('ga_visitor_report_new',con=engine,if_exists='append', index=False)

def main():

    script = 'countGA_Session.py'

    try:
        analytics = initialize_analyticsreporting()
    except Exception as e:
        msg = 'PART 1: 連線GA API 讀取失敗! The error message : {}'.format(e)
        send_error_message(script=script, message=msg)
        print(msg)
        sys.exit(1)
        
    print('PART 1: Finished!')

    try:
        response = makeRequestWithExponentialBackoff(analytics)

    except Exception as e:
        msg = 'PART 2: Fetch GA 資料出現錯誤! The error message : {}'.format(e)
        send_error_message(script=script, message=msg)
        print(msg)
        sys.exit(1)

    print('PART 2: Finished!')

    try:
        final = prep_data(response)
    except Exception as e:
        msg = 'PART 3: 清整GA資料出現錯誤! The error message : {}'.format(e)
        send_error_message(script=script, message=msg)
        print(msg)
        sys.exit(1)

    print('PART 3: Finished!')

    try:
        insert_mysql(final)
    except Exception as e:
        msg = 'PART 4: 寫入資料庫出現錯誤! The error message : {}'.format(e)
        send_error_message(script=script, message=msg)
        print(msg)
        sys.exit(1)


if __name__ == '__main__':
    main()