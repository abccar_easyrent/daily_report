# coding: utf-8
import os
import sys
sys.path.insert(0, '/home/hadoop/jessielu/')

import functools
import pandas as pd

from datetime import datetime, timedelta
from Module.EmailFunc import send_vc_report, send_vc_error
from Module.ConnFunc import read_mysql
from Module.HelpFunc import display_set

display_set(150, 30)

path = '/home/hadoop/jessielu/daily_report/files/'
#path = '/home/jessielu/PycharmProjects/daily_report/'
os.chdir(path)

# 建立日期區間 ＃區間：當月第1天/過去1天/過去7天/過去14天/當月
b1day = datetime.strftime(datetime.today()-timedelta(1), '%Y-%m-%d')
b7day = datetime.strftime(datetime.today()-timedelta(7), '%Y-%m-%d')
b8day = datetime.strftime(datetime.today()-timedelta(8), '%Y-%m-%d')
b14day = datetime.strftime(datetime.today()-timedelta(14), '%Y-%m-%d')

cur_mth = datetime.strftime(datetime.today().replace(day=1), '%Y-%m-%d')
last_mth = datetime.strftime((datetime.today()-timedelta(14)).replace(day=1), '%Y-%m-%d')


# PART 1: 讀取所需數據
def read_data():

    # 建立query條件
    vc_query = (''.join(["select * from report.virtual where 日期 >= '", last_mth, "'"]))
    vip_query = (''.join(["select Date, MemberID, Category, VIPLevel from report.mstatus_vip where Date >= '", last_mth, "'"]))
    mem_query = "select MemberID, CompanyName from report.mem_all"
    mem_area_query = "select MemberId MemberID, Mobile, CarDealerCountryName from report.member_location"
    idx_mem_query = "select a.Unique, a.Group_2, a.Group_3 from report.index_memcategory a"

    # 讀取虛擬號碼原始檔
    virtual_raw = read_mysql(vc_query, 'report')
    # 讀取會員身份紀錄檔
    mem_vip = read_mysql(vip_query, 'report')
    # 會員檔
    mem_all = read_mysql(mem_query, 'report')
    # 會員地區
    mem_area = read_mysql(mem_area_query, 'report')
    # 會員身份mapping表
    index_mcategory = read_mysql(idx_mem_query, 'report')

    # 確認數據讀取筆數
    tbl_in_list = pd.DataFrame({'virtual_raw_rows': [len(virtual_raw)],
                                'mem_vip_rows': [len(mem_vip)]})

    return virtual_raw, mem_vip, mem_all, mem_area, index_mcategory, tbl_in_list


# PART 2: 刪除值/格式轉換/建立mapping欄位
def convert_data():

    # 刪除內部測試資料
    delete_ls = [83194, 18384]
    virtual = virtual_raw[~virtual_raw.車商編號.isin(delete_ls)]

    # virtual_raw:'轉車家電話'轉數值格式
    virtual['轉車家電話'].fillna(0, inplace=True)
    virtual['轉車家電話'] = virtual['轉車家電話'].astype(int)

    # mem_vip:新增Distinct欄位: Category + VIPLevel
    mem_vip["Unique"] = mem_vip.Category.map(str) + mem_vip.VIPLevel.map(str)
    mem_vip.drop(columns=['Category', 'VIPLevel'], inplace=True)

    # index_mcategory:轉換成字串格式
    index_mcategory['Unique'] = index_mcategory.Unique.map(str)

    return virtual, mem_vip, index_mcategory


# PART 3 : 根據時間點切割表格
def subset_data():

    # 整合各Member相關資訊
    dfs = [mem_vip, mem_all, mem_area]
    mem_info = functools.reduce(lambda left, right: pd.merge(left, right, on='MemberID', how='left'), dfs)
    mem_info = pd.merge(mem_info, index_mcategory, on='Unique', how='left')

    # 整合成一張大表
    virtual_fn = pd.merge(virtual, mem_info, left_on=['日期', '車商編號'], right_on=['Date', 'MemberID'], how='left')
    virtual_fn.drop(columns=['Date', 'MemberID'], inplace=True)
    virtual_fn['日期'] = pd.to_datetime(virtual_fn['日期'])

    # 切割資料 #過去1天 #前一週 #前二週 #本月
    vc_1day = virtual_fn[(virtual_fn['日期'] == b1day)]
    vc_7day = virtual_fn[(virtual_fn['日期'] >= b7day) & (virtual_fn['日期'] <= b1day)]
    vc_14day = virtual_fn[(virtual_fn['日期'] >= b14day) & (virtual_fn['日期'] < b7day)]
    vc_cmth = virtual_fn[(virtual_fn['日期'] >= cur_mth)]

    return vc_1day, vc_7day, vc_14day, vc_cmth


# PART 4: 建立表格二-各撥打撥打狀態總覽表格整理成所需格式
def create_tableTwo():

    # 昨日撥打總覽
    vc_b1 = vc_1day.groupby(['狀態']).size().reset_index(name='當日撥打數')
    # 過去7天撥打總覽
    vc_b7 = vc_7day.groupby(['狀態']).size().reset_index(name='過去7天撥打數')
    # 上上週撥打總覽
    vc_b14 = vc_14day.groupby(['狀態']).size().reset_index(name='上上週撥打數')
    # 當月撥打總覽
    vc_mth = vc_cmth.groupby(['狀態']).size().reset_index(name='本月累積撥打數')

    vc_dfs = [vc_b1, vc_b7, vc_b14, vc_mth]
    vc_dt = functools.reduce(lambda left, right: pd.merge(left, right, on='狀態', how='outer'), vc_dfs)
    vc_dt = vc_dt.fillna(0)

    return vc_dt


# PART 5 : 表格一-撥打數總覽(成功/未成功)
def create_tableOne():

    # 表格1-成功通話數/未成功通話數 #欄位:狀態/當日/過去7天/上上週/增幅(%)/本月累積/(上月同期)/增幅（％）
    fail_ls = ['消費者等待時掛斷', '車商聽語音時掛斷', '車商未接', '消費者輸入的分機錯誤', '轉換車商號碼失敗',
               '消費者撥打後立刻掛斷', '消費者未輸入分機']
    success_ls = ['通話10秒內', '通話5分鐘內', '通話5-10分鐘內', '通話超過10分鐘']

    vc_fail = vc_dt[vc_dt['狀態'].isin(fail_ls)].sum().reset_index(name='未成功通話數')
    vc_suc = vc_dt[vc_dt['狀態'].isin(success_ls)].sum().reset_index(name='成功通話數')

    vc_ov = pd.merge(vc_suc, vc_fail, left_on='index', right_on='index', how='outer')

    # 產生表格所需格式及計算指標
    vc_ov = (vc_ov.set_index('index').T)
    vc_ov = vc_ov.drop(columns=['狀態'])
    vc_ov = vc_ov.reindex(['成功通話數', '未成功通話數'])
    vc_ov['每週增幅(%)'] = ((vc_ov['過去7天撥打數'] / vc_ov['上上週撥打數']) - 1) * 100
    vc_ov_dt = vc_ov.loc[:, ['當日撥打數', '過去7天撥打數', '上上週撥打數', '每週增幅(%)', '本月累積撥打數']]

    return vc_ov_dt


# PART 6: 表格三-各身份撥打通數總覽(當日/過去7天/上上週/本月）
def create_tableThree():

    # 撥打數 #當日 ＃過去7天 #上上週 ＃本月累積
    g_col = ['Group_2']

    vcg_1d = vc_1day.groupby(g_col).size().reset_index(name='當日撥打數')
    vcg_7d = vc_7day.groupby(g_col).size().reset_index(name='過去7天撥打數')
    vcg_14d = vc_14day.groupby(g_col).size().reset_index(name='上上週撥打數')
    vcg_cmth = vc_cmth.groupby(g_col).size().reset_index(name='本月累積撥打數')

    # 所有會員身份表格
    mem_category = index_mcategory[['Group_2']].drop_duplicates()

    # 合併4張表
    dfs = [vcg_1d, vcg_7d, vcg_14d, vcg_cmth]
    vc_group = functools.reduce(lambda left, right: pd.merge(left, right, on='Group_2', how='outer'), dfs)

    # 針對某些身份無值時補0
    vc_group = pd.merge(mem_category, vc_group, on='Group_2', how='left')
    vc_group = vc_group.fillna(0)

    # 加入總計
    vc_g = vc_group.append(vc_group.sum(numeric_only=True), ignore_index=True)

    # 計算過去7天/上上週及比較
    vc_g.insert(4, '增幅(%)', (((vc_g['過去7天撥打數'] / vc_g['上上週撥打數']) - 1) * 100).fillna(0))
    vc_g['Group_2'].fillna('總計', inplace=True)
    vc_g.rename(columns={'Group_2':'車商身份'}, inplace=True)

    return vc_g


# PART 7: 表格四-各廣宣身份撥打通數總覽
def create_tabelFour():

    # 表格四-各廣宣身份撥打數總覽(當天/過去7天/上上週/本月）#欄位: 網路好店/平輸好店(廣宣方案）
    # 撥打數 #當天 ＃過去7天 #上上週 ＃本月累積
    g_col = ['Group_2', 'Group_3']
    vcm_1d = vc_1day.groupby(g_col).size().reset_index(name='當天撥打數')
    vcm_7d = vc_7day.groupby(g_col).size().reset_index(name='過去7天撥打數')
    vcm_14d = vc_14day.groupby(g_col).size().reset_index(name='上上週撥打數')
    vcm_cmth = vc_cmth.groupby(g_col).size().reset_index(name='本月累積撥打數')

    # 所有會員身份表格
    mem_category = index_mcategory[['Group_2', 'Group_3']].drop_duplicates()

    # 合併4張表
    dfs = [vcm_1d, vcm_7d, vcm_14d, vcm_cmth]
    vcm_group = functools.reduce(lambda left, right: pd.merge(left, right, on=['Group_2', 'Group_3'], how='outer'), dfs)
    vcm_group = pd.merge(mem_category, vcm_group, on=['Group_2', 'Group_3'], how='left')

    # 只篩選會員身份為網路好店/平輸好店
    cat_list = ['網路好店', '平輸好店']
    vcm_group = vcm_group[vcm_group['Group_2'].isin(cat_list)]
    vcm_group = vcm_group.fillna(0)

    # 加入總計
    vcm_g = vcm_group.append(vcm_group.sum(numeric_only=True), ignore_index=True)

    # 計算過去7天/上上週及比較
    vcm_g.insert(5, '增幅(%)', (((vcm_g['過去7天撥打數'] / vcm_g['上上週撥打數']) - 1) * 100).fillna(0))
    vcm_g['Group_2'] = vcm_g['Group_2'].fillna('總計')

    vcm_g.rename(columns={'Group_2':'車商身份', 'Group_3':'廣宣身份'}, inplace=True)

    return vcm_g


# PART 8: 表格五-當日車商撥打數/接聽率％(成功接聽數/撥打數）
def create_tableFive():

    # 當日各車商撥打數
    vc_cus_1d = vc_1day.groupby(['車商編號', 'CompanyName']).size().reset_index(name='當日撥打數')
    # 當月各車商撥打數
    vc_cus_mth = vc_cmth.groupby(['車商編號']).size().reset_index(name='本月累積撥打數')
    # 當月車商成功通話數
    success_ls = ['通話10秒內', '通話5分鐘內', '通話5-10分鐘內', '通話超過10分鐘']
    vc_cus_suc = vc_cmth[vc_cmth.狀態.isin(success_ls)]
    vc_cus_suc = vc_cus_suc.groupby(['車商編號']).size().reset_index(name='本月成功接聽數')
    # 整合3張表格
    dfs = [vc_cus_1d, vc_cus_mth, vc_cus_suc]
    vc_cus_g = functools.reduce(lambda left, right: pd.merge(left, right, on=['車商編號'], how='left'), dfs)
    vc_cus_g.fillna(0, inplace=True)
    # 計算本月接聽率
    vc_cus_g['本月接聽率(%)'] = vc_cus_g['本月成功接聽數'] / vc_cus_g['本月累積撥打數'] * 100

    vc_cus_g.rename(columns={'CompanyName':'車商名'}, inplace=True)
    vc_cus_g = vc_cus_g.sort_values(by=['當日撥打數'], ascending=False)

    return vc_cus_g


# PART 9: 車商撥打明細表 (表格六: 當日被撥打車商明細 + 第二個工作表: 車商撥打明細-過去7天)
def create_call_list():

    need_col = ['日期', '車商編號', 'CompanyName', 'Group_2', 'Group_3', 'CarDealerCountryName', '客戶來電', '轉車家電話',
                '起始時間', '結束時間', '通話秒數', '狀態']
    name_col = ['日期', '車商編號', '車商名', '車商身份', '廣宣身份', '縣市', '客戶來電', '車商電話', '起始時間', '結束時間',
                '通話秒數', '狀態']

    # 表格六: 當日被撥打車商明細
    call_list_1d = vc_1day.loc[:, need_col]
    call_list_1d.columns = name_col

    # 第二個工作表: 車商撥打明細-過去7天
    call_list_7d = vc_7day.loc[:, need_col]
    call_list_7d.columns = name_col

    return call_list_1d, call_list_7d


# PART 10 : 數據寫入指定虛擬號碼日報表中
def save_to_csv():

    writer = pd.ExcelWriter('虛擬號碼來電數日報.xlsx', engine='openpyxl')
    # 表格一
    vc_ov_dt.to_excel(writer, startrow=4, sheet_name='總覽')
    # 表格二
    vc_dt.to_excel(writer, startrow=9, sheet_name='總覽', index=False)
    # 表格三
    vc_g.to_excel(writer, startrow=21, sheet_name='總覽', index=False)
    # 表格四
    vcm_g.to_excel(writer, startrow=32, sheet_name='總覽', index=False)
    # 表格五
    vc_cus_g.to_excel(writer, startrow=4, startcol=7, sheet_name='總覽', index=False)
    # 表格六
    call_list_1d.to_excel(writer, startrow=4, startcol=14, sheet_name='總覽', index=False)
    # 表格七
    call_list_7d.to_excel(writer, startrow=4, startcol=1, sheet_name='車商撥打明細-過去7天', index=False)

    #workbook = writer.book
    ws1 = writer.sheets['總覽']
    ws2 = writer.sheets['車商撥打明細-過去7天']

    # 設定標題
    ws1.cell(row=2, column=2).value = "日期:" + b1day
    ws1.cell(row=2, column=2).value = "過去7天:" + b7day + "~" + b1day
    ws1.cell(row=1, column=2).value = "當日:" + b1day
    ws1.cell(row=2, column=2).value = "過去7天:" + b7day + "~" + b1day
    ws1.cell(row=3, column=2).value = "上上週:" + b14day + "~" + b8day
    ws2.cell(row=3, column=2).value = "日期:" + b7day + "~" + b1day

    # 插入表格標題
    ws1.cell(row=4, column=1).value = "表格一-撥打數總覽(成功/未成功)"
    ws1.cell(row=9, column=1).value = "表格二-各撥打撥打狀態總覽"
    ws1.cell(row=21, column=1).value = "表格三-各車商身份撥打數總覽"
    ws1.cell(row=32, column=1).value = "表格四-各廣宣身份撥打數總覽"
    ws1.cell(row=4, column=8).value = "表格五-當日被撥打車商-撥打數/接聽率"
    ws1.cell(row=4, column=15).value = "表格六-當日被撥打車商明細"
    ws2.cell(row=2, column=2).value = "表格七-撥打車商明細-過去7天累積"

    # 儲存
    writer.save()


if __name__ == "__main__":

    # PART 1: 讀取數據及確認數據中是否有0值
    try:
        virtual_raw, mem_vip, mem_all, mem_area, index_mcategory, tbl_in_list = read_data()
        if (0 in tbl_in_list.values) == True:
            msg = 'PART 1: 數據中有0值, 此程式停止執行!'
            send_vc_error(msg)
            print(msg)
            sys.exit(1)
        else:
            print('PART 1: 數據正確且無0值!')
    except Exception as e:
        msg = 'PART 1:讀取數據發生錯誤. The error message : {}'.format(e)
        print(msg)
        send_vc_error(msg)
        sys.exit(1)

    # PART 2~3: 轉換格式及切割資料
    try:
        virtual, mem_vip, index_mcategory = convert_data()
        vc_1day, vc_7day, vc_14day, vc_cmth = subset_data()
        msg = 'PART 2~3: 轉換格式及切割數據OK!'
        print(msg)
    except Exception as e:
        msg = 'PART 2~3: 轉換格式及切割數據發生錯誤!. The error message : {}'.format(e)
        print(msg)
        send_vc_error(msg)
        sys.exit(1)

    # PART 4: 產出表格二
    try:
        vc_dt = create_tableTwo()
        msg = 'PART 4: 產出表格二OK!'
        print(msg)
    except Exception as e:
        msg = 'PART 4: 產出表格二發生錯誤!. The error message : {}'.format(e)
        print(msg)
        send_vc_error(msg)
        sys.exit(1)

    # PART 5: 產出表格一
    try:
        vc_ov_dt = create_tableOne()
        msg = 'PART 5: 產出表格一OK!'
        print(msg)
    except Exception as e:
        msg = 'PART 5: 產出表格一發生錯誤!. The error message : {}'.format(e)
        print(msg)
        send_vc_error(msg)
        sys.exit(1)

    # PART 6: 產出表格三
    try:
        vc_g = create_tableThree()
        msg = 'PART 6: 產出表格三OK!'
        print(msg)
    except Exception as e:
        msg = 'PART 6: 產出表格三發生錯誤!. The error message : {}'.format(e)
        print(msg)
        send_vc_error(msg)
        sys.exit(1)

    # PART 7: 產出表格四
    try:
        vcm_g = create_tabelFour()
        msg = 'PART 7: 產出表格四OK!'
        print(msg)
    except Exception as e:
        msg = 'PART 7: 產出表格四發生錯誤!. The error message : {}'.format(e)
        print(msg)
        send_vc_error(msg)
        sys.exit(1)

    # PART 8: 產出表格五
    try:
        vc_cus_g = create_tableFive()
        msg = 'PART 8: 產出表格五OK!'
        print(msg)
    except Exception as e:
        msg = 'PART 8: 產出表格五發生錯誤!. The error message : {}'.format(e)
        print(msg)
        send_vc_error(msg)
        sys.exit(1)

    # PART 9: 產出表格六/七
    try:
        call_list_1d, call_list_7d = create_call_list()
        msg = 'PART 9: 產出表格六/七OK!'
        print(msg)
    except Exception as e:
        msg = 'PART 9: 產出表格六/七發生錯誤!. The error message : {}'.format(e)
        print(msg)
        send_vc_error(msg)
        sys.exit(1)

    # PART 10: 儲存至excel
    try:
        save_to_csv()
        msg = 'PART 10: 產出表格六/七OK!'
        print(msg)
    except Exception as e:
        msg = 'PART 10: 產出表格六/七發生錯誤!. The error message : {}'.format(e)
        print(msg)
        send_vc_error(msg)
        sys.exit(1)

    # 寄送虛擬號碼日報
    try:
        send_vc_report(receiver='g', path=path)
        msg = 'PART 11: 寄送虛擬號碼日報OK'
        print(msg)
    except Exception as e:
        msg = 'PART 11: 寄送虛擬號碼日報發生錯誤. The error message : {}'.format(e)
        send_vc_error(msg)
        print(msg)
        sys.exit(1)
