# -*- coding: utf-8 -*-
#==============
# Path: /home/hadoop/jessielu/daily_report/scripts
# Description: GA4 [visitor:到站人次,New_visitor:到站新客,all:總瀏覽數]
# Date: 2023/10/30
# Author: Sam
#==============

import os
import sys
sys.path.insert(0, '/home/hadoop/jessielu/')
from Module.EmailFunc import send_error_message
import pandas as pd
import sqlalchemy as sa
from datetime import date
from dateutil.relativedelta import relativedelta
import random
import time
from google.oauth2 import service_account
from google.analytics.data_v1beta import BetaAnalyticsDataClient
from google.analytics.data_v1beta.types import (
    DateRange,
    Dimension,
    Filter,
    FilterExpression,
    FilterExpressionList,
    Metric,
    RunReportRequest,
)


#原本
now = (date.today() - relativedelta(days=2)).strftime("%Y-%m-%d")
#補發用
# now = '2022-02-27'

print(now)
KEY_FILE_PATH = '/home/hadoop/jessielu/daily_report/scripts/'
KEY_FILE_LOCATION = 'ga4ApiKey.json'
property_id = "312585480"

def initialize_analyticsreporting():
	"""Initializes an Analytics Reporting API V4 service object.

    Returns:
        An authorized Analytics Reporting API V4 service object.
    """
     
	credentials = service_account.Credentials.from_service_account_file(KEY_FILE_PATH + KEY_FILE_LOCATION)
	#Build the service object.
	client = BetaAnalyticsDataClient(credentials=credentials)
	
	return client

def get_report(client):
	"""Queries the Analytics Reporting API V4.

    Args:
        analytics: An authorized Analytics Reporting API V4 service object.
    Returns:
        The Analytics Reporting API V4 response.
    """
	dimension_list =  [Dimension(name="date"),Dimension(name="eventName")]

	"""
	eventCount = 事件計數
	TotalUsers = 總人數
	"""
	metrics_list = [Metric(name='eventCount'),Metric(name='TotalUsers')]
	request = RunReportRequest(
			property=f"properties/{property_id}",
			dimensions=dimension_list,
			metrics=metrics_list,
			limit=25000,
			date_ranges=[DateRange(start_date=now, end_date=now)],
			dimension_filter=FilterExpression(
				filter=Filter(
					field_name="eventName",
					in_list_filter=Filter.InListFilter(
						values=[
							"page_view",
							"first_visit",
						]
					),
				)
			),
	)

	# Execute GA4 request
	reports = client.run_report(request)
	return reports

def makeRequestWithExponentialBackoff(analytics):
    for n in range(0, 5):
        print(n)
        try:
            return get_report(analytics)

        except Exception as error:
            print(error)
            time.sleep((2 ** n) + random.random())
    
def prep_data(reports):
	data = []
	for row in reports.rows:
		dimension_values = [value.value for value in row.dimension_values]
		metric_values = [value.value for value in row.metric_values]
		data.append(dimension_values + metric_values)

	columns = [dimension.name for dimension in reports.dimension_headers] + [metric.name for metric in reports.metric_headers]
	df = pd.DataFrame(data, columns=columns)

	df["date"] = pd.to_datetime(df["date"], format="%Y-%m-%d")

	r=[]
	columns=['QueryDate','visitor','New_visitor','all']
      
	GA=pd.DataFrame(data=r,columns=columns)
	for i in range(len(df.index)):
		GA['QueryDate'] = pd.to_datetime(df['date'])
		if df['eventName'][i]=="page_view":
			GA['all'] = pd.to_numeric(df['eventCount'][i])
			GA['visitor'] = pd.to_numeric(df['TotalUsers'][i])
		elif df['eventName'][i]=="first_visit":  
			GA['New_visitor'] = pd.to_numeric(df['TotalUsers'][i])

			
	GA = GA.drop_duplicates(subset=['QueryDate'])

	return GA

def insert_mysql(data):
    engine = sa.create_engine('mysql+pymysql://abccar:abccar@172.31.42.43:3306/CollectGA?charset=utf8', encoding='utf8')
    data.to_sql('ga_visitor_report_new',con=engine,if_exists='append', index=False)

def main():

    script = 'countGA4_Session_new.py'

    try:
        analytics = initialize_analyticsreporting()
    except Exception as e:
        msg = 'PART 1: 連線GA API 讀取失敗! The error message : {}'.format(e)
        send_error_message(script=script, message=msg)
        print(msg)
        sys.exit(1)
        
    print('PART 1: Finished!')

    try:
        response = makeRequestWithExponentialBackoff(analytics)

    except Exception as e:
        msg = 'PART 2: Fetch GA 資料出現錯誤! The error message : {}'.format(e)
        send_error_message(script=script, message=msg)
        print(msg)
        sys.exit(1)

    print('PART 2: Finished!')

    try:
        final = prep_data(response)
    except Exception as e:
        msg = 'PART 3: 清整GA資料出現錯誤! The error message : {}'.format(e)
        send_error_message(script=script, message=msg)
        print(msg)
        sys.exit(1)

    print('PART 3: Finished!')

    try:
        insert_mysql(final)
    except Exception as e:
        msg = 'PART 4: 寫入資料庫出現錯誤! The error message : {}'.format(e)
        send_error_message(script=script, message=msg)
        print(msg)
        sys.exit(1)


if __name__ == '__main__':
    main()