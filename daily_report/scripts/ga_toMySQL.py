# -*- coding: utf-8 -*-
import sys
sys.path.insert(0, '/home/hadoop/jessielu/')
from apiclient.discovery import build
from oauth2client.service_account import ServiceAccountCredentials
from Module.EmailFunc import send_error_message
import re
import pandas as pd
import sqlalchemy as sa
import os
import socket
import time
from datetime import datetime, timedelta, date
from dateutil.relativedelta import relativedelta
import random
from apiclient.errors import HttpError

#原本
now = date.today()
#補發用
# now = (datetime.today() - relativedelta(days=1)).strftime('%Y-%m-%d')
# now = '2022-01-30'
print(now)

SCOPES = ['https://www.googleapis.com/auth/analytics.readonly']
os.chdir('/home/hadoop/jessielu/daily_report/scripts/')
KEY_FILE_LOCATION = 'gaApiKey.json'
VIEW_ID = '177455238'

def initialize_analyticsreporting():
    """Initializes an Analytics Reporting API V4 service object.

    Returns:
        An authorized Analytics Reporting API V4 service object.
    """
    credentials = ServiceAccountCredentials.from_json_keyfile_name(
        KEY_FILE_LOCATION, SCOPES)

    # Build the service object.
    analytics = build('analyticsreporting', 'v4', credentials=credentials)
    return analytics

def get_report(analytics):
    """Queries the Analytics Reporting API V4.

    Args:
        analytics: An authorized Analytics Reporting API V4 service object.
    Returns:
        The Analytics Reporting API V4 response.
    """
    valuesNeed = [
        "【小網】立即通話",
        "【大網】查看聯繫手機",
        "【大網】查看聯繫電話",
        "【大網】收藏物件",
        "【小網】收藏物件",
        "【小網】收藏其他物件",
        "【分享】Facebook",
        "【分享】Line",
        "【分享】Messenger",
        "【分享】email",
        "【小網】立即通話內層",
        "【大網】查看電話",
        "【大網】查看車商電話",
        "【小網】LINE 聯繫賣家",
        "【大網】LINE聯繫賣家_上",
        "【大網】查看聯繫LINE_下"
    ]
    reports = []
    pageCnt = 1
    pageToken = None
    while True:
        data = analytics.reports().batchGet(
            body={
                'reportRequests': [{
                    'viewId': VIEW_ID,
                    'pageToken': pageToken,
                    'samplingLevel': 'LARGE',
					'dateRanges': [{'startDate': str(now), 'endDate':str(now)}],
                    'metrics': [{'expression': 'ga:totalEvents'}],
                    'dimensions': [
                        {'name': 'ga:date'},
                        {'name': 'ga:medium'},
                        {'name': 'ga:sourceMedium'},
                        {'name': 'ga:eventAction'},
                        {'name': 'ga:eventLabel'}
                    ],
                    'dimensionFilterClauses': [{
                        'filters': [{
                            'dimensionName': 'ga:eventAction',
                            'operator': 'IN_LIST',
                            'expressions': valuesNeed
                        }]
                    }]
                }]
            }
        ).execute()
        reports.append(data)
        print('Get Page %i of data.' % (pageCnt))
        pageCnt += 1
        if 'nextPageToken' in data['reports'][0]:
            pageToken = data['reports'][0]['nextPageToken']
        else:
            break
  
    return reports

def prep_data(rawData):
    rows = []
    for data in rawData:
        data = data['reports'][0]
        
        cols = data['columnHeader']
        cols = cols['dimensions'] + [cols['metricHeader']['metricHeaderEntries'][0]['name']]
        cols = [re.sub('ga:','',x) for x in cols]
    
        data = data['data']['rows']
    
        for row in data:
            date = row['dimensions'][0]
            date = '-'.join([date[:4], date[4:6], date[6:]])
            row['dimensions'][0] = date
            rows.append(row['dimensions']+row['metrics'][0]['values'])

    cols[4] = 'CarID'
    final = pd.DataFrame(rows, columns=cols)
    return final 

def makeRequestWithExponentialBackoff(analytics):
    for n in range(0, 5):
        print(n)
        try:
            return get_report(analytics)

        except Exception as error:
            print(error)
            time.sleep((2 ** n) + random.random())

    
def insert_mysql(data):
    engine = sa.create_engine('mysql+pymysql://abccar:abccar@172.31.42.43:3306/bidb?charset=utf8', encoding='utf8')
    data.to_sql('ga',con=engine,if_exists='append', index=False)


def main():

    script = 'ga_toMySQL.py'

    try:
        analytics = initialize_analyticsreporting()
    except Exception as e:
        msg = 'PART 1: 連線GA API 讀取失敗! The error message : {}'.format(e)
        send_error_message(script=script, message=msg)
        print(msg)
        sys.exit(1)
        
    print('PART 1: Finished!')

    try:
        response = makeRequestWithExponentialBackoff(analytics)

    except Exception as e:
        msg = 'PART 2: Fetch GA 資料出現錯誤! The error message : {}'.format(e)
        send_error_message(script=script, message=msg)
        print(msg)
        sys.exit(1)

    print('PART 2: Finished!')

    try:
        final = prep_data(response)
    except Exception as e:
        msg = 'PART 3: 清整GA資料出現錯誤! The error message : {}'.format(e)
        send_error_message(script=script, message=msg)
        print(msg)
        sys.exit(1)

    print('PART 3: Finished!')

    try:
        insert_mysql(final)
    except Exception as e:
        msg = 'PART 4: 寫入資料庫出現錯誤! The error message : {}'.format(e)
        send_error_message(script=script, message=msg)
        print(msg)
        sys.exit(1)


if __name__ == '__main__':
    main()






