#==============
# Path: /home/hadoop/jessielu/daily_report/scripts/
# Description: abc會員中心-我的數據
# Date: 2022/05/24
# Author: Jessie 
#==============

# coding: utf-8
import sys
sys.path.insert(0, '/home/hadoop/jessielu/')

from Module.ConnFunc import read_mysql, read_test_mssql, to_sql_server
# from Module.HelpFunc import display_set
from Module.EmailFunc import send_error_message

import pandas as pd
from pandas.api.types import is_string_dtype
from datetime import datetime, timedelta

# display_set(200, 30)

b1day = datetime.strftime(datetime.today(), "%Y-%m-%d")
today = datetime.strftime(datetime.today()+timedelta(1), "%Y-%m-%d")

# #補發用
# b1day = '2022-05-12'
# today = '2022-05-13'

# PART 1: 讀取數據
def ReadData():

    # Car_New : CarID車輛資訊檔
    car_new_sg = "select CarID, Email, BrandID, SeriesName, CategoryName, Mileage, ManufactureDate, color, EngineDisplacement from bidb.[dbo].Car_New"
    # status_new : 每日上架CarID #只選category = 3, 4, 5
    status_new_sg = f"""
    select * from report.status_new where Date >= '{b1day}' and Date < '{today}' and Category in (3, 4, 5)
    """
    # mstatus_vip : 每日會員id #因需區分各車商身分加入時間，所以讀入此table
    mstatus_vip_sg = f"""
    select * from report.mstatus_vip where Date >= '{b1day}' and Date < '{today}'
    """
    # GA檔 + 其他副檔(index_brand & index_color)
    # ga_main
    ga_main_sg = f"""
    select Date, eventAction, CarID, totalEvents from report.ga_main where date >= '{b1day}' and Date < '{today}'
    """
    # index_brand
    index_brand_sg = f"""
    select BrandID, Name from ABCCar.[dbo].Brand    
    """
    # index_color
    index_color_sg = f"""
    select Code color, Color from report.index_color
    """

    # 執行讀取程式
    car_new = read_test_mssql(car_new_sg, 'bidb')
    status_new = read_mysql(status_new_sg, 'report')
    mstatus_vip = read_mysql(mstatus_vip_sg, 'report')
    ga_main = read_mysql(ga_main_sg, 'report')
    idx_brand = read_test_mssql(index_brand_sg, 'ABCCar')
    idx_color = read_mysql(index_color_sg, 'report')

    # 確認數據讀取筆數
    tbl_in_list = pd.DataFrame({'car_new_rows': [len(car_new)],
                                'status_new_rows': [len(status_new)],
                                'mstatus_vip_rows': [len(mstatus_vip)],
                                'ga_main_rows': [len(ga_main)],
                                'idx_brand': [len(idx_brand)],
                                'idx_color': [len(idx_color)]})

    return car_new, status_new, mstatus_vip, ga_main, idx_brand, idx_color, tbl_in_list
    # return car_new, status_new, mstatus_vip, ga_main, idx_brand, idx_color


# PART 2: 確認欄位資料格式, 並且針對必要欄位統一轉成相同格式
def convert_data_type(dt):
    data = dt.copy()
    check_column = ['CarID', 'VIPLevel', 'Category']
    for column in check_column:
        if column in data.columns:
            if is_string_dtype(data[column]) == False:
                data[column] = data[column].astype(int).astype(str)
            else:
                pass
        else:
            pass

    if 'MemberID' in data.columns:
        data.MemberID = data.MemberID.astype(int)
    else:
        pass

    if 'Date' in data.columns:
        try:
            data.Date = data.Date.apply(lambda x: datetime.strftime(x, '%Y-%m-%d'))
        except:
            pass
    else:
        pass

    return data

# 轉欄位格式
def converting_data():

    car_new_dt = convert_data_type(car_new)
    status_new_dt = convert_data_type(status_new)
    ga_main_dt = convert_data_type(ga_main)
    mstatus_vip_dt = convert_data_type(mstatus_vip)

    return car_new_dt, status_new_dt, ga_main_dt, mstatus_vip_dt


# PART 3: 車輛檔/會員檔整理
def car_prep():

    # Car_New的BrandID=0去除, 正常情況下, 無BrandID = 0
    c_new = car_new_dt.copy()
    c_new.ManufactureDate = c_new.ManufactureDate.apply(lambda x: datetime.strftime(x, '%Y-%m-%d'))
    c_new = c_new.loc[c_new.BrandID!=0, :]
    # 取得廠牌欄位
    c_new = pd.merge(c_new, idx_brand.iloc[:, 0:2], on='BrandID', how='left')
    # 取得顏色欄位
    c_new = pd.merge(c_new, idx_color, on='color', how='left')
    # 新增連結欄位
    c_new['Link'] = "https://www.abccar.com.tw/car/" + c_new.CarID.astype(str)
    c_new.rename(columns={'Mileage':'Mileage_range'}, inplace=True)
    # 拋棄不需要的欄位
    c_new.drop(columns=['BrandID', 'color'], inplace=True)

    # mstatus_vip : 取得Member_New中的CompanyName
    m_vip = mstatus_vip_dt.copy()
    # 取得車商所有的CarID
    m_vip = pd.merge(m_vip, c_new[['Email', "CarID"]], on='Email', how='left')

    return c_new, m_vip


# PART 4: GA檔整理
def ga_prep():

    ga = ga_main_dt.copy()
    # 重新命名瀏覽數/來電數/收藏數
    replace_ls = [("【大網】查看聯繫手機", "Calls"), ("【大網】查看聯繫電話", "Calls"), ("【小網】立即通話", "Calls"), ("【大網】收藏物件", "Collects"),("【小網】收藏物件", "Collects"),("【小網】收藏其他物件", "Collects"),("【大網】查看車商電話", "Calls"),("【大網】查看 Line ID", "Calls"),("【小網】LINE 聯繫賣家", "Calls"),("【大網】LINE聯繫賣家_上", "Calls"),("【大網】查看聯繫LINE_下", "Calls"),("【大網】查看電話", "Calls"), ("【小網】立即通話內層", "Calls"), ("/car/", "Pageviews")]
    for old_wd, new_wd in replace_ls:
        ga.eventAction = ga.eventAction.apply(lambda x: x.replace(old_wd, new_wd))

    # group by每個CarID的指標 # 將指標列轉成欄位 (pageviews/calls/collects)
    ga = ga.groupby(['Date', 'CarID', 'eventAction'])['totalEvents'].sum().reset_index()
    ga_r = pd.pivot_table(data=ga, index=['Date', 'CarID'], columns='eventAction',
                          values='totalEvents').reset_index().fillna(0)
    return ga_r

# PART 5 : 整併資料併產出3個DataFrame
def merge_dataframe():
    # DataFrame 1 : GAboard
    # 取交集 : 有產生流量的carid才會包含在此表格中+產生流量的carid中的會員數皆為status=1 and category in (3,4,5)
    gaboard = pd.merge(m_vip, ga, on=['Date', 'CarID'])
    # 將車輛檔詳細資訊整進去
    gaboard = pd.merge(gaboard, c_new, on='CarID', how='left')
    # 排除不需要的欄位
    gaboard.drop(columns=['Email_y', 'Status', 'Email_x'], inplace=True)
    gaboard.rename(columns={'Name':'Brand'}, inplace=True)

    # 收藏數如果無值的話補0
    if 'Collects' not in gaboard.columns:
        gaboard['Collects'] = 0
    else:
        gaboard

    # DataFrame 2 : all_sales
    need_col = ['Date', 'MemberID', 'VIPLevel', 'Category', 'CarID']
    status_s = status_new_dt[need_col]
    # all_sales取上架CarID和GA CarID的聯集
    ga_s = gaboard[need_col]
    all_carid = status_s.append(ga_s)
    all_carid = all_carid.drop_duplicates(subset='CarID', keep='last')
    all_sales = all_carid.groupby(['Date', 'MemberID', 'VIPLevel',
                                   'Category'])['CarID'].count().reset_index(name='Sales')

    # DataFrame 3 : Group_performance
    g_col = ['Date', 'Category', 'VIPLevel']
    all_sales_g = all_carid.groupby(g_col)['CarID'].count().reset_index(name='Sales')
    gaboard_g = gaboard.groupby(g_col)['Calls', 'Collects', 'Pageviews'].sum().reset_index()
    group_performance = pd.merge(all_sales_g, gaboard_g, on=['Date', 'Category', 'VIPLevel'])

    return gaboard, all_sales, group_performance

# PART 6 : 將數據寫入至MSSQL SERVER
def send_to_db(data, tbl_name):
    num = 1000
    loop_no = round(len(data) / num) + 1
    for i in range(0, loop_no):
        table = data[i * num:i * num + num]
        to_sql_server('ABCCar', table, tbl_name, 'append')


if __name__ == "__main__":

    script = 'client_board.py'

    try:
        car_new, status_new, mstatus_vip, ga_main, idx_brand, idx_color, tbl_in_list = ReadData()

        if (0 in tbl_in_list.values) == True:
            msg = '{}: PART 1: 數據中有0值, 此程式停止執行!'.format(script)
            send_error_message(script='{}'.format(script), message=msg)
            print(msg)
            sys.exit(1)
        else:
            print('{} PART 1: 數據正確且無0值!'.format(script))

    except Exception as e:
        msg = '{0}: PART 1 - 數據連線讀取失敗! The error message : {1}'.format(script, e)
        send_error_message(script='{}'.format(script), message=msg)
        print(msg)
        sys.exit(1)

    try:
        car_new_dt, status_new_dt, ga_main_dt, mstatus_vip_dt = converting_data()
        c_new, m_vip = car_prep()
        ga = ga_prep()
        print('{}: PART 2~4 - 數據整理OK!'.format(script))
    except Exception as e:
        msg = '{0}: PART 2~4 - 數據整理出現錯誤! The error message : {1}'.format(script, e)
        send_error_message(script='{}'.format(script), message=msg)
        print(msg)
        sys.exit(1)

    try:
        gaboard, all_sales, group_performance = merge_dataframe()
        print('{}: PART 5 - 數據合併OK!'.format(script))
    except Exception as e:
        msg = '{0}: PART 5 - 數據合併出現錯誤! The error message : {1}'.format(script, e)
        send_error_message(script='{}'.format(script), message=msg)
        print(msg)
        sys.exit(1)

    try:
        send_to_db(gaboard, 'GAboard')
        send_to_db(all_sales, 'All_sales')
        send_to_db(group_performance, 'Group_performance')
        print('{}: PART 6 - 數據寫入DB OK!'.format(script))
    except Exception as e:
        msg = '{0}: PART 6 - 數據寫入出現錯誤! The error message : {1}'.format(script, e)
        send_error_message(script='{}'.format(script), message=msg)
        print(msg)
        sys.exit(1)
