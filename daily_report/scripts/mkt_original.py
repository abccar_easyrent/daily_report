# coding: utf-8
import sys
sys.path.insert(0, '/home/hadoop/jessielu/')
import numpy as np
import pandas as pd

from Module.ConnFunc import read_mysql, to_mysql, read_test_mssql
# from Module.HelpFunc import display_set
from Module.EmailFunc import send_error_message
from Module.ConnectInfo import mysql_info
import pymysql

# display_set(150, 30)


# PART 1: Read in data
def read_in_data():

    car_new_query="""
    select CarID, Email, BrandID, SeriesName, CategoryName, Mileage, ManufactureDate, color, EngineDisplacement, 
    Location, Price, StartDate from bidb.dbo.Car_New
    """
    mem_new_query= """
    select Email, MemberID, CompanyName, Name, Category, VIPLevel, Status from bidb.dbo.Member_New where Status = '1'
    """
    mem_original_query="""
    select *
    from report.mem_original
    """
    color_query="""
    select * from report.index_color
    """

    car_new = read_test_mssql(car_new_query, 'bidb')
    mem_new = read_test_mssql(mem_new_query, 'bidb')
    mem_original = read_mysql(mem_original_query, 'report')
    color_idx = read_mysql(color_query, 'report')

    return car_new, mem_new, mem_original, color_idx


# PART 2 : 處理Member_New, 以便符合PowerBI所需數據
def mem_new_processing(mem_new_dt):

    mem_new = mem_new_dt.copy()
    # 補CompanyName資訊
    mem_new.CompanyName= np.where(mem_new.CompanyName.isnull(), mem_new.Name, mem_new.CompanyName)
    mem_new = mem_new.loc[mem_new.Email != '', :]
    # 合併member原廠的會員資訊
    mem_all = pd.merge(mem_new, mem_original[['MemberID', 'Company', 'Area']], on='MemberID', how='left')
    # 建立Category / VIPLevel的Unique
    mem_all['Unique'] = mem_all.Category.astype(str) + mem_all.VIPLevel.astype(str)
    # 選擇想要的欄位順序
    mem_all = mem_all[["MemberID","Email","Category","VIPLevel","Unique","Status", "Company","Area",
                       "CompanyName","Name"]]

    return mem_all


# PART 3 : 處理Car_New, 以便符合PowerBI所需數據
def car_new_processing(car_new_dt):
    car_new = car_new_dt
    # 確保Email欄位'0', 此為異常
    car_new = car_new.loc[car_new.Email != '0', :]
    # Car_New + Mem_New 交集的車輛
    car_all = pd.merge(car_new, mem_all.iloc[:, 0:2], on='Email', how='left')
    # 合併color_index
    car_all = pd.merge(car_all, color_idx, left_on='color', right_on='Code', how='left')
    # ManufactureDate欄位只選取年份資料
    car_all.ManufactureDate = car_all.ManufactureDate.astype(str).apply(lambda x: x[0:4])
    # 建立里程數區間欄位
    car_all['Mileage_range'] = pd.cut(x=car_all.Mileage,
                                  bins=[0, 10000, 30000, 50000, 80000, 120000, 150000, 1000000, 10000000],
                                  labels=["Below 1", "1~3", "3~5", "5~8", "8~12", "12~15", "Above 15", "Above 100"])
    # 建立Link欄位
    car_all['Link'] = "https://www.abccar.com.tw/car/" + car_all.CarID.astype(str)
    # 選擇需要欄位/位置
    car_all = car_all[["CarID","MemberID","Email","BrandID","SeriesName","CategoryName","ManufactureDate",
                       "EngineDisplacement","Color","Location", "Price","StartDate","Mileage_range","Link"]]
    return car_all


# PART 4 : write tbl to MySQL
def execute_mysql(sql_command):
    # Read in database information
    my_server = mysql_info['address']
    my_user = mysql_info['user']
    my_password = mysql_info['password']
    # Connect to mysql
    connect = pymysql.connect(host=my_server,
                                 user=my_user,
                                 password=my_password,
                                 db='report')
    # create cursor to execute command
    cursor = connect.cursor()
    sql_query = sql_command
    # execution commit
    cursor.execute(sql_query)
    connect.commit()
    # close the connection
    connect.close()


def write_to_db(data, tbl_name):
    num = 1000
    loop_no = round(len(data) / num) + 1
    for i in range(0, loop_no):
        table = data[i * num:i * num + num]
        to_mysql('report', table, tbl_name, 'append')
        #print(i * num, i * num + num)


if __name__ == "__main__":

    script_subject = 'mkt_original.py'

    try:
        car_new, mem_new, mem_original, color_idx = read_in_data()
    except Exception as e:
        msg = '{0}: PART 1 - 數據連線讀取失敗! The error message : {1}'.format(script_subject, e)
        send_error_message(script='{}'.format(script_subject), message=msg)
        print(msg)
        sys.exit(1)

    try:
        mem_all = mem_new_processing(mem_new)
    except Exception as e:
        msg = '{0}: PART 2: 數據清整Member_New失敗! The error message : {1}'.format(script_subject, e)
        send_error_message(script='{}'.format(script_subject), message=msg)
        print(msg)
        sys.exit(1)

    try:
        car_all = car_new_processing(car_new)
    except Exception as e:
        msg = '{0}: PART 3: 數據清整Car_New失敗! The error message : {1}'.format(script_subject, e)
        send_error_message(script='{}'.format(script_subject), message=msg)
        print(msg)
        sys.exit(1)

    try:
        # Delete rows before writing updated table to DB
        execute_mysql('DELETE FROM report.mem_all')
        execute_mysql('DELETE FROM report.car_all')
        # Append data to DB (DB 中的table unicode 記得需要設成utf8)
        write_to_db(mem_all, 'mem_all')
        write_to_db(car_all, 'car_all')
    except Exception as e:
        msg = '{0}: PART 4: 數據寫入MySQL失敗! The error message : {1}'.format(script_subject, e)
        send_error_message(script='{}'.format(script_subject), message=msg)
        print(msg)
        sys.exit(1)
