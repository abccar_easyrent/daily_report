# -*- coding: utf-8 -*-
import sys

sys.path.insert(0, '/home/hadoop/jessielu/')

from apiclient.discovery import build
from oauth2client.service_account import ServiceAccountCredentials
from Module.EmailFunc import send_error_message
from Module.ConnFunc import read_mssql,read_test_mssql,read_dev_stage_mssql,to_sql_server,to_test_sql_server,to_dev_stage_sql_server
import re
import pandas as pd
import numpy as np
import sqlalchemy as sa
import os
import time
from datetime import datetime, timedelta, date
from dateutil.relativedelta import relativedelta
import random
from apiclient.errors import HttpError

#原本使用系統日取前一日
#now = date.today() - timedelta(1)
#AWS主機使用 UTC 時間與台灣(UTC+8:00)差8小時 

#原本
now = date.today()
# #補發用
#now = date.today() + relativedelta(days=-1)
print(now)


#正式區:official / 營運區:develop /  測試區:dev_stage
#ENV = 'official'
ENV = 'dev_stage'

SCOPES = ['https://www.googleapis.com/auth/analytics.readonly']
os.chdir('/home/hadoop/jessielu/daily_report/scripts')
KEY_FILE_LOCATION = 'gaApiKey.json'
VIEW_ID = '177455238'

def initialize_analyticsreporting():
    """Initializes an Analytics Reporting API V4 service object.

    Returns:
        An authorized Analytics Reporting API V4 service object.
    """
    credentials = ServiceAccountCredentials.from_json_keyfile_name(
        KEY_FILE_LOCATION, SCOPES)

    # Build the service object.
    analytics = build('analyticsreporting', 'v4', credentials=credentials)
    return analytics

def get_report(analytics):
    """Queries the Analytics Reporting API V4.

    Args:
        analytics: An authorized Analytics Reporting API V4 service object.
    Returns:
        The Analytics Reporting API V4 response.
    """

    reports = []
    pageCnt = 1
    pageToken = None
    while True:
        data = analytics.reports().batchGet(
            body={
                'reportRequests': [{
                    'viewId': VIEW_ID,
                    'pageToken': pageToken,
                    'samplingLevel': 'LARGE',
                    'pageSize': 10000,
					'dateRanges': [{'startDate': str(now), 'endDate':str(now)}],
                    'metrics': [{'expression': 'ga:pageviews'}],
                    'dimensions': [
                        {'name': 'ga:date'},
                        #{'name': 'ga:medium'},
                        #{'name': 'ga:sourceMedium'},
                        {'name': 'ga:pagePathLevel1'},
                        {'name': 'ga:pagePathLevel2'},
                        #{'name': 'ga:dimension2'},
                        #{"name": "ga:pagePath"},
                        #{"name": "ga:hostName"},
                        #{'name': 'ga:dimension2'},
                        {'name': 'ga:deviceCategory'} #檢查device desktop/mobile/tablet
                    ],
                    'dimensionFilterClauses': [{
                        'filters': [{
                            'dimensionName': 'ga:pagePathLevel1',
                            'operator': 'EXACT',
                            'expressions': ['/car/']
                        }]
                    }]
                }]
            }
        ).execute()
        reports.append(data)
        print('Get Page %i of data.' % (pageCnt))
        pageCnt += 1
        if 'nextPageToken' in data['reports'][0]:
            pageToken = data['reports'][0]['nextPageToken']
        else:
            break
  
    return reports

def makeRequestWithExponentialBackoff(analytics):
    for n in range(0, 5):
        print(n)
        try:
            return get_report(analytics)

        except Exception as error:
            print(error)
            time.sleep((2 ** n) + random.random())
            

def get_report_event(analytics):
    """Queries the Analytics Reporting API V4.

    Args:
        analytics: An authorized Analytics Reporting API V4 service object.
    Returns:
        The Analytics Reporting API V4 response.
    """

    valuesNeed = [
        "【小網】立即通話",
        "【大網】查看聯繫手機",
        "【大網】查看聯繫電話",
        "【大網】收藏物件",
        "【小網】收藏物件",
        "【小網】收藏其他物件",
        "【分享】Facebook",
        "【分享】Line",
        "【分享】Messenger",
        "【分享】email",
        "【小網】立即通話內層",
        "【大網】查看電話",
        "【大網】查看車商電話",
        "【小網】LINE 聯繫賣家",
        "【大網】LINE聯繫賣家_上",
        "【大網】查看聯繫LINE_下"
    ]
    reports = []
    pageCnt = 1
    pageToken = None
    while True:
        data = analytics.reports().batchGet(
            body={
                'reportRequests': [{
                    'viewId': VIEW_ID,
                    'pageToken': pageToken,
                    'samplingLevel': 'LARGE',
					'dateRanges': [{'startDate': str(now), 'endDate':str(now)}],
                    'metrics': [{'expression': 'ga:totalEvents'}],
                    'dimensions': [
                        {'name': 'ga:date'},
                        {'name': 'ga:medium'},
                        {'name': 'ga:sourceMedium'},
                        {'name': 'ga:eventAction'},
                        {'name': 'ga:eventLabel'}
                    ],
                    'dimensionFilterClauses': [{
                        'filters': [{
                            'dimensionName': 'ga:eventAction',
                            'operator': 'IN_LIST',
                            'expressions': valuesNeed
                        }]
                    }]
                }]
            }
        ).execute()
        reports.append(data)
        print('Get Page %i of data.' % (pageCnt))
        pageCnt += 1
        if 'nextPageToken' in data['reports'][0]:
            pageToken = data['reports'][0]['nextPageToken']
        else:
            break
  
    return reports

def makeRequestWithExponentialBackoffEvent(analytics):
    for n in range(0, 5):
        print(n)
        try:
            return get_report_event(analytics)

        except Exception as error:
            print(error)
            time.sleep((2 ** n) + random.random())

def prep_data(rawData):
    rows = []
    zhPattern = re.compile(u'[\u4e00-\u9fa5]+')
    for data in rawData:
        data = data['reports'][0]
        
        cols = data['columnHeader']
        cols = cols['dimensions'] + [cols['metricHeader']['metricHeaderEntries'][0]['name']]
        cols = [re.sub('ga:','',x) for x in cols]
    
        data = data['data']['rows']
    
        for row in data:
            date = row['dimensions'][0]
            date = '-'.join([date[:4], date[4:6], date[6:]])
            row['dimensions'][0] = date
            rows.append(row['dimensions']+row['metrics'][0]['values'])

    final = pd.DataFrame(rows, columns=cols)
    
    #if final.columns.isin(['pagePathLevel2', 'hostName']).any():
    #    final.pagePathLevel2 = final.pagePathLevel2.str[1:8]
    #    final.hostName = final.hostName.str.replace(".abccar.com.tw", "", regex=True)
    #    final = final.loc[(final['pagePathLevel2'].str.match(r'\d').astype(bool)), :]
    if final.columns.isin(['pagePathLevel2']).any():
        final.pagePathLevel2 = final.pagePathLevel2.str[1:8]
        final = final.loc[(final['pagePathLevel2'].str.match(r'\d').astype(bool)), :]
    
    if final.columns.isin(['eventLabel']).any():
        final["CarID"] = final["eventLabel"].str.split('】').str[1]
        final["CarID"] = final["CarID"].str.split('/').str[0]
        final = final.loc[(final['CarID'].str.match(r'\d').astype(bool)), :]

    return final

def clear_db():
    Sql = f"""DELETE FROM GAPageView WHERE QueryDate = '{now}'; 
              DELETE FROM GAEventAction WHERE QueryDate = '{now}'; 
              --SELECT Count(CarID) FROM GAEventAction (nolock) WHERE QueryDate = '{now}'; 
              SELECT convert(varchar(10),getdate(),120) as SysDate ;
            """
        
    if ENV=="official":
        read_mssql(Sql,'ABCCar')
    elif ENV=="develop":
        read_test_mssql(Sql,'ABCCar')
    else:
        read_dev_stage_mssql(Sql,'ABCCar')
        

def send_to_db(dt, tbl_name):
    num = 1000
    loop_no = round(len(dt) / num) + 1
    for i in range(0, loop_no):
        data = dt[i * num:i * num + num]
        if ENV=="official":
            to_sql_server('ABCCar', data, tbl_name, 'append')
        elif ENV=="develop":
            to_test_sql_server('ABCCar', data, tbl_name, 'append')
        else:
            to_dev_stage_sql_server('ABCCar', data, tbl_name, 'append')
        
        
if __name__ == '__main__':
    script = 'ga_toABCCar.py'

    try:
        analytics = initialize_analyticsreporting()
    except Exception as e:
        msg = 'PART 1: 連線GA API 讀取失敗! The error message : {}'.format(e)
        send_error_message(script=script, message=msg)
        print(msg)
        sys.exit(1)

    print('PART 1: Finished!')

    try:
        #瀏覽數
        response = makeRequestWithExponentialBackoff(analytics)
        #點擊數
        responseEvent = makeRequestWithExponentialBackoffEvent(analytics)
    except Exception as e:
        msg = 'PART 2: Fetch GA 資料出現錯誤! The error message : {}'.format(e)
        send_error_message(script=script, message=msg)
        print(msg)
        sys.exit(1)

    print('PART 2: Finished!')

    try:
        # 清整GA瀏覽數
        final = prep_data(response)
        final = final.drop(columns=['pagePathLevel1'], axis=1)        
        final.rename(columns = {'date':'QueryDate', 
                                'pagePathLevel2':'CarID', 
                                #'hostName':'HostName',
                                'deviceCategory': 'Device', 
                                'pageviews':'PageViews'}, inplace = True)
        
        final['PageViews'] = final['PageViews'].astype(int)
        dfCar = final.groupby(['QueryDate', 'CarID'],as_index = False)['PageViews'].agg('sum')
        dfDevice = final.groupby(['QueryDate', 'CarID', 'Device'],as_index = False)['PageViews'].agg('sum')
        
        #desktop / tablet / mobile
        dfDesktop = dfDevice.loc[(dfDevice['Device'] != "tablet") & (dfDevice['Device'] != "mobile"), :]  #大網
        #dfTablet = dfDevice.loc[(dfDevice['Device'] != "desktop") & (dfDevice['Device'] != "mobile"), :]  #大網
        dfTablet = dfDevice.loc[(dfDevice['Device'] == "tablet"), :]  #小網
        dfMobile = dfDevice.loc[(dfDevice['Device'] == "mobile"), :]  #小網
        
        dfDesktop = dfDesktop.drop(columns=['QueryDate','Device'], axis=1)
        dfTablet = dfTablet.drop(columns=['QueryDate','Device'], axis=1)
        dfMobile = dfMobile.drop(columns=['QueryDate','Device'], axis=1)
        dfDesktop.rename(columns = {'PageViews':'DesktopViews'}, inplace = True)        
        dfTablet.rename(columns = {'PageViews':'TabletViews'}, inplace = True)
        dfMobile.rename(columns = {'PageViews':'MobileViews'}, inplace = True)   

        dfCar = pd.merge(dfCar, dfDesktop, on=["CarID"], how='left').fillna(0)
        dfCar = pd.merge(dfCar, dfTablet, on=["CarID"], how='left').fillna(0)
        dfCar = pd.merge(dfCar, dfMobile, on=["CarID"], how='left').fillna(0)
        
        dfCar['DesktopViews'] = dfCar['DesktopViews'].astype(int)
        dfCar['TabletViews'] = dfCar['TabletViews'].astype(int)
        dfCar['MobileViews'] = dfCar['MobileViews'].astype(int)
        dfCar['MobileViews'] = dfCar['TabletViews'] + dfCar['MobileViews']
        finalGA = dfCar[['QueryDate','CarID','DesktopViews','MobileViews']].copy()
        
        #final = prep_data(response)
        #final = final.drop(columns=['pagePathLevel1'], axis=1)        
        #final.rename(columns = {'date':'QueryDate', 
        #                        'pagePathLevel2':'CarID', 
        #                        'hostName':'HostName',
        #                        #'deviceCategory': 'Device', 
        #                        'pageviews':'PageViews'}, inplace = True)
        #finalGAPageView = final.groupby(['QueryDate', 'CarID', 'HostName'],as_index = False)['PageViews'].agg('sum')
        
        #dfDesktop = finalGAPageView.loc[(finalGAPageView['HostName'] != "m"), :]  #大網
        #dfMobile = finalGAPageView.loc[(finalGAPageView['HostName'] == "m"), :]  #小網
        
        #dfDesktop = dfDesktop.drop(columns=['QueryDate','HostName'], axis=1)      
        #dfDesktop.rename(columns = {'PageViews':'DesktopViews'}, inplace = True)
        #dfMobile = dfMobile.drop(columns=['QueryDate','HostName'], axis=1)      
        #dfMobile.rename(columns = {'PageViews':'MobileViews'}, inplace = True)

        #finalGAPageAll = finalGAPageView.groupby(['QueryDate', 'CarID'],as_index = False)['PageViews'].agg('sum')
        #finalGAPageAll = finalGAPageAll.drop(columns=['PageViews'], axis=1)
        
        #finalGAPageDesktop = pd.merge(finalGAPageAll, dfDesktop, on=["CarID"], how='left').fillna(0)
        #finalGA = pd.merge(finalGAPageDesktop, dfMobile, on=["CarID"], how='left').fillna(0)


        # 清整GA點擊數
        finalEvent = prep_data(responseEvent)
        finalEvent = finalEvent.drop(columns=['medium','sourceMedium','eventLabel'], axis=1) 
        finalEvent.rename(columns = {'date':'QueryDate', 
                                     'eventAction':'EventAction',
                                     'totalEvents':'TotalEvents'}, inplace = True)
        finalGAEventAction = finalEvent.groupby(['QueryDate', 'CarID', 'EventAction'],as_index = False)['TotalEvents'].agg('sum')      
        finalGAEventAction.insert(3, "DeviceCategory", 1, True)       
        finalGAEventAction['DeviceCategory'] = np.where(finalGAEventAction.EventAction.str.contains('小網'), 2, 1)
    except Exception as e:
        msg = 'PART 3: 清整GA資料出現錯誤! The error message : {}'.format(e)
        send_error_message(script=script, message=msg)
        print(msg)
        sys.exit(1)
    
    print('PART 3: Finished!')
    
    try:
        # 轉入前先移除當日資料
        clear_db()
        # 轉入當日資料 瀏覽數
        send_to_db(finalGA, 'GAPageView')
        # 轉入當日資料 點擊數
        send_to_db(finalGAEventAction, 'GAEventAction')
    except Exception as e:
        msg = 'PART 4: 寫入資料庫出現錯誤! The error message : {}'.format(e)
        send_error_message(script=script, message=msg)
        print(msg)
        sys.exit(1)
        
    print('PART 4: Finished!')