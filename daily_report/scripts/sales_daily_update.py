# coding: utf-8
import pandas as pd
import numpy as np
import os
import sys

sys.path.insert(0, '/home/hadoop/jessielu/')
#sys.path.insert(0, '/home/jessie/MyNotebook/daily_report/')

import matplotlib as mpl
import matplotlib.pyplot as plt
import functools
from openpyxl import load_workbook

from datetime import datetime, timedelta
from Module.ConnFunc import read_mysql
# from Module.HelpFunc import display_set
from Module.EmailFunc import send_daily_report, send_daily_report_error
from Module.drawExcel import header, target, addValueVertical
# import Image

# display_set(100, 30)
mpl.pyplot.switch_backend('agg')

path = '/home/hadoop/jessielu/daily_report/files/'
#path = '/home/jessie/MyNotebook/easyRent/scripts/'

os.chdir(path)

from datetime import datetime, timedelta, date
from dateutil.relativedelta import relativedelta     

#原本
now = date.today() + relativedelta(days=1)

# 補發用
# now = date.today()
# now = date(2022, 2, 28)


#每月一號的的計算邏輯要特別調整
if(now.day == 1):
    tmpDay = now - relativedelta(days=1)
    first_day = date(tmpDay.year, tmpDay.month, 1)
    tmpMonth = tmpDay - relativedelta(months=1)
    lastMonth_first_day = date(tmpMonth.year, tmpMonth.month, 1)
    dl = tmpDay - relativedelta(months=2)
    lfirst_day = date(dl.year, dl.month, 1)
    lastMonth_end_day = date(first_day.year, first_day.month, 1) - relativedelta(days=1)
else:
    #上個月的今天
    d = now - relativedelta(months=1)
    #這個月的第一天
    first_day = date(now.year, now.month, 1)
    #上個月的第一天
    lastMonth_first_day = date(d.year, d.month, 1)
    #上上個月的今天
    dl = now - relativedelta(months=2)
    #上上個月的第一天
    lfirst_day = date(dl.year, dl.month, 1)
    #上個月的最後一天
    lastMonth_end_day = date(first_day.year, first_day.month, 1) - relativedelta(days=1)
    
# 建立日期區間
start_date = lastMonth_first_day #上個月的第一天
end_date = (now - timedelta(1)).strftime("%Y-%m-%d") #前一天

#start_date = '2020-07-18'
#end_date = '2021-01-23'

# 建立MySQL-report連線
# PART 1: 讀取數據
def read_data():
    query1 = f"""
    select date Date, Category, VIPLevel, count(*) Sales from report.status_new
    where date >= '{start_date}' and date <= '{end_date}' group by date, Category, VIPLevel
    """
    # 讀取會員數
    query2 = f"""
    select date Date, Category, VIPLevel, count(*) Members from report.mstatus_vip
    where date >= '{start_date}' and date <= '{end_date}' group by date, Category, VIPLevel
    """
    # 讀取一般賣家會員數
    query3 = f"""
    select Date, Numbers Members from report.mstatus_all
    where Category = 2 and date >= '{start_date}' and date <= '{end_date}'
    """
    # 讀取車格空格數
    query4 = f"""
    SELECT Date, Empties from report.empty_202010 where Date >= '{start_date}' and
    Date <= '{end_date}'
    """
    # 讀取GA網站整體成效(瀏覽數, 到站人次）
    query5 = f"""
    select a.QueryDate Date, a.visitor, a.New_visitor, a.all from CollectGA.ga_visitor_report_new a
    where QueryDate >= '{start_date}' and 
    QueryDate <= '{end_date}'
    """
    # 讀取GA物件瀏覽數
    query6 = f"""
    select substr(date, 1, 10) Date, sum(totalEvents) from report.ga_main
    where eventAction like '/car/' and date >= '{start_date}' and date <= '{end_date}' group by date
    """
    # 讀取有上架車商會員數
    query7 = f"""
    select Date, Category, VIPLevel, count(distinct(MemberID)) from report.status_new 
    where Date >= '{start_date}' and Date <= '{end_date}' group by Date, Category, VIPLevel
    """
    # 讀取站上物件數
    query8 = f"""
    SELECT * from report.on_shelf_202010 where Date >= '{start_date}' and Date <= '{end_date}'
    """
    query9 = f"""
    select a.Unique Combine, a.Group_2 from report.index_memcategory a
    """

    #取得聯盟館數
    query10 = f"""
    select Date, Number from report.alliance 
    where Date >= '{start_date}' and Date <= '{end_date}'
    """

    # #取得認證館(HAA)數
    # query11 = f"""
    # select Date, Number from report.certification 
    # where Date >= '{start_date}' and Date <= '{end_date}'
    # """

    # 執行讀取程式
    car_status = read_mysql(query1, 'report')
    mem_status = read_mysql(query2, 'report')
    person_status = read_mysql(query3, 'report')
    empty = read_mysql(query4, 'report')
    ga = read_mysql(query5, 'CollectGA')
    ga_car = read_mysql(query6, 'report')
    upload_members = read_mysql(query7, 'report')
    on_shelf = read_mysql(query8, 'report')
    index_mcategory = read_mysql(query9, 'report')
    alliance = read_mysql(query10, 'report')
    # certification = read_mysql(query11, 'report')

    # 檢查筆數是否為0
    #2021-09-08 註解GA資料
    tbl_in_list = pd.DataFrame({'car_status_rows': [len(car_status)],
                                'mem_status_rows': [len(mem_status)],
                                'person_status_rows': [len(person_status)],
                                'empty_status_rows': [len(empty)],
                                # 'ga_ov_rows': [len(ga)],
                                # 'ga_car_rows': [len(ga_car)],
                                'upload_members_rows': [len(upload_members)],
                                'on_shelf_rows': [len(on_shelf)],
                                'alliance_rows': [len(alliance)]})
                                # 'certification_rows': [len(certification)]})

    # return car_status, mem_status, person_status, empty, ga, ga_car, upload_members, \
    #        on_shelf, index_mcategory, alliance, certification, tbl_in_list

    return car_status, mem_status, person_status, empty, ga, ga_car, upload_members, \
           on_shelf, index_mcategory, alliance, tbl_in_list


# PART 2: 上架數+會員數+一般賣家會員數+站上物件數新增'Combine'欄位
# 會員身份mapping表格重新命名使其一致
def add_combine_col():

    # 修改欄位格式Combine
    index_mcategory.Combine = index_mcategory.Combine.map(str)

    # 新增欄位：Category + VIPLevel = Combine
    # dlist=[car_status, mem_status, on_shelf, upload_members]
    dlist=[car_status, mem_status, upload_members]
    clist=['Category','VIPLevel']

    for data in dlist:
        for col in clist:
            data[col] = data[col].astype(int).astype(str)

        data['Combine']=data['Category']+data['VIPLevel']
        data=data.drop(columns=['Category', 'VIPLevel'], axis=1, inplace=True)

    # 一般賣家補Combine欄位
    person_status['Combine']='20'

    # 將會員數資料整合在一起
    mem_status_all = mem_status.append(person_status)

    return index_mcategory, mem_status_all, car_status, on_shelf, upload_members



# PART 3: 產出以會員身份為主的表格
def groupby_transpose(dt, values, value_name):
    select_dt = dt.copy()

    # 重新命名為原廠認證
    index_mcategory.loc[index_mcategory.Group_2 == '原廠', 'Group_2'] = '原廠認證'

    # 取得Group_2欄位
    select_dt = pd.merge(select_dt, index_mcategory, on='Combine', how='left')

    # 轉置成所需資料格式
    select_dt = select_dt.groupby(['Date', 'Group_2'])[values].sum().reset_index()
    select_dt = select_dt.pivot(index='Date', columns='Group_2',
                                          values=[values]).reset_index()
    select_dt.columns = select_dt.columns.droplevel()
    select_dt.rename(columns={'':'Date'}, inplace=True)

    # 重新命名欄位
    for col in select_dt.columns[1:]:
        select_dt=select_dt.rename(columns={'{0}'.format(col):'{0}{1}'.format(col, value_name)})

    return select_dt


# PART 4: 產出總量及資料整合main sheet的表格
# def organize_sheetMain(cstatus_dt, alliance, certification):
def organize_sheetMain(cstatus_dt, alliance):
    # 計算總量
    members_dt['總會員數'] = members_dt.loc[:, ['一般車商會員數', '原廠認證會員數', '平輸好店會員數', '網路好店會員數',
                                               '買賣顧問會員數']].sum(axis=1)

    cstatus_dt = cstatus_dt.merge(alliance, how='left', on='Date')
    cstatus_dt.rename(columns={'Number':'聯盟館上架數'}, inplace=True)

    cstatus_dt['聯盟館上架數'] = cstatus_dt['聯盟館上架數'].astype(int)
    
    cstatus_dt['總上架數'] = cstatus_dt.iloc[:, 1:].sum(axis=1)
    # onshelf_dt['總待售數'] = onshelf_dt.iloc[:, 1:].sum(axis=1)

    # 重新命名欄位
    # empty.rename(columns={'Empties':'總空格數'}, inplace=True)
    ga_car.rename(columns={'sum(totalEvents)':'物件總瀏覽數'}, inplace=True)
    ga.columns = ['Date', '到站人次', '到站新客', '總瀏覽數']

    # 修改ga data欄位格式
    try:
        ga.Date = ga.Date.apply(lambda x: datetime.strftime(x, '%Y-%m-%d'))
    except:
        pass

    # 整合表格
    # dfs = [members_dt, cstatus_dt, onshelf_dt, empty, ga, ga_car]
    dfs = [members_dt, cstatus_dt, ga, ga_car]

    full_dt = functools.reduce(lambda left, right: pd.merge(left, right, on='Date', how='outer'), dfs)

    # EXCEL中sheet 'main'表                  
    # order_list = ['Date', '一般車商會員數', '原廠認證會員數', '平輸好店會員數', '網路好店會員數', '買賣顧問會員數', '總會員數',
    #               '一般車商上架數', '一般賣家上架數', '原廠認證上架數', '平輸好店上架數', '網路好店上架數', '買賣顧問上架數',
    #               '聯盟館上架數', '認證館(HAA)上架數', '總上架數', '到站人次', '到站新客', '總瀏覽數', '物件總瀏覽數']

    order_list = ['Date', '一般車商會員數', '原廠認證會員數', '平輸好店會員數', '網路好店會員數', '買賣顧問會員數', '總會員數',
                  '一般車商上架數', '一般賣家上架數', '原廠認證上架數', '平輸好店上架數', '網路好店上架數', '買賣顧問上架數',
                  '聯盟館上架數', '總上架數', '到站人次', '到站新客', '總瀏覽數', '物件總瀏覽數']

    #2021-09-08 註解補0
    # # 如果欄位不存增加欄位補0
    # cur_col = full_dt.columns
    # miss_col = list(set(order_list) - set(cur_col))
    # for col in miss_col:
    #     full_dt[col] = 0

    # 排序
    main_dt = full_dt[order_list]
    # main_dt.iloc[:, 1:] = main_dt.iloc[:, 1:].fillna(0).astype(int)
    main_dt.iloc[:, 1:] = main_dt.iloc[:, 1:].fillna('')

    return main_dt, cstatus_dt


# PART 5: 產出總量及資料整合side sheet的表格
def organize_sheetSide(dt):
    up_mem = dt.copy()
    up_mem['總上架會員數'] = up_mem.iloc[:, 1:].sum(axis=1)
    # EXCEL中sheet 'side'表
    order_list = ['Date', '一般車商上架會員數', '一般賣家上架會員數', '原廠認證上架會員數', '平輸好店上架會員數', '網路好店上架會員數',
                  '買賣顧問上架會員數', '總上架會員數']
    side_dt = up_mem[order_list]
    return side_dt

# 2020/10 新增功能
# PART 6: 取得每天、當月、上個月各館上架數及上架會員數
def calPercentAndAverage(cstatus_dt, side_dt):
    cstatus_dt_ori = list(cstatus_dt.tail(1).iloc[0, 1:]) #當日上架數
    cstatus_dt_percent = [] #當日上架數百分比

    up_members_ori = list(side_dt.iloc[-1, 1:]) #當日上架會員數
    up_members_percent = [] #當日上架會員數百分比

    for i in cstatus_dt_ori:
        cstatus_dt_percent.append(int((i/cstatus_dt_ori[-1])*100) if i != 0 else 0)

    for i in up_members_ori:
        up_members_percent.append(int((i/up_members_ori[-1])*100) if i != 0 else 0)

    #=======================上月平均上架數=============
    cstatus_dt_LastMonth = cstatus_dt[(cstatus_dt['Date']>=str(lastMonth_first_day)) & (cstatus_dt['Date'] <= str(lastMonth_end_day))]

    x1 = cstatus_dt_LastMonth.loc[:, '一般車商上架數'].replace(0, np.NaN).mean(axis=0) if np.isnan(cstatus_dt_LastMonth.loc[:, '一般車商上架數'].replace(0, np.NaN).mean(axis=0))==False else 0
    x2 = cstatus_dt_LastMonth.loc[:, '一般賣家上架數'].replace(0, np.NaN).mean(axis=0) if np.isnan(cstatus_dt_LastMonth.loc[:, '一般賣家上架數'].replace(0, np.NaN).mean(axis=0))==False else 0
    x3 = cstatus_dt_LastMonth.loc[:, '原廠認證上架數'].replace(0, np.NaN).mean(axis=0) if np.isnan(cstatus_dt_LastMonth.loc[:, '原廠認證上架數'].replace(0, np.NaN).mean(axis=0))==False else 0
    x4 = cstatus_dt_LastMonth.loc[:, '平輸好店上架數'].replace(0, np.NaN).mean(axis=0) if np.isnan(cstatus_dt_LastMonth.loc[:, '平輸好店上架數'].replace(0, np.NaN).mean(axis=0))==False else 0
    x5 = cstatus_dt_LastMonth.loc[:, '網路好店上架數'].replace(0, np.NaN).mean(axis=0) if np.isnan(cstatus_dt_LastMonth.loc[:, '網路好店上架數'].replace(0, np.NaN).mean(axis=0))==False else 0
    x6 = cstatus_dt_LastMonth.loc[:, '買賣顧問上架數'].replace(0, np.NaN).mean(axis=0) if np.isnan(cstatus_dt_LastMonth.loc[:, '買賣顧問上架數'].replace(0, np.NaN).mean(axis=0))==False else 0
    x7 = cstatus_dt_LastMonth.loc[:, '聯盟館上架數'].replace(0, np.NaN).mean(axis=0) if np.isnan(cstatus_dt_LastMonth.loc[:, '聯盟館上架數'].replace(0, np.NaN).mean(axis=0))==False else 0
    # x8 = cstatus_dt_LastMonth.loc[:, '認證館(HAA)上架數'].replace(0, np.NaN).mean(axis=0) if np.isnan(cstatus_dt_LastMonth.loc[:, '認證館(HAA)上架數'].replace(0, np.NaN).mean(axis=0))==False else 0
    
    # cstatus_dt_LastMonth_ori = [x1, x2, x3, x4, x5, x6, x7, x8]
    cstatus_dt_LastMonth_ori = [x1, x2, x3, x4, x5, x6, x7]
    x9 = sum(cstatus_dt_LastMonth_ori)
    cstatus_dt_LastMonth_ori.append(x9)
    cstatus_dt_LastMonth_percent = []

    for i in cstatus_dt_LastMonth_ori:
        cstatus_dt_LastMonth_percent.append(int((i/cstatus_dt_LastMonth_ori[-1])*100) if i != 0 else 0)
        
    cstatus_dt_LastMonth_ori = list(map(lambda x: int(x) , cstatus_dt_LastMonth_ori))

    #=======================上月平均上架會員數=============
    side_dt_LastMonth = side_dt[(side_dt['Date']>=str(lastMonth_first_day)) & (side_dt['Date'] <= str(lastMonth_end_day))]
    x1 = side_dt_LastMonth.loc[:, '一般車商上架會員數'].replace(0, np.NaN).mean(axis=0) if np.isnan(side_dt_LastMonth.loc[:, '一般車商上架會員數'].replace(0, np.NaN).mean(axis=0))==False else 0
    x2 = side_dt_LastMonth.loc[:, '一般賣家上架會員數'].replace(0, np.NaN).mean(axis=0) if np.isnan(side_dt_LastMonth.loc[:, '一般賣家上架會員數'].replace(0, np.NaN).mean(axis=0))==False else 0
    x3 = side_dt_LastMonth.loc[:, '原廠認證上架會員數'].replace(0, np.NaN).mean(axis=0) if np.isnan(side_dt_LastMonth.loc[:, '原廠認證上架會員數'].replace(0, np.NaN).mean(axis=0))==False else 0
    x4 = side_dt_LastMonth.loc[:, '平輸好店上架會員數'].replace(0, np.NaN).mean(axis=0) if np.isnan(side_dt_LastMonth.loc[:, '平輸好店上架會員數'].replace(0, np.NaN).mean(axis=0))==False else 0
    x5 = side_dt_LastMonth.loc[:, '網路好店上架會員數'].replace(0, np.NaN).mean(axis=0) if np.isnan(side_dt_LastMonth.loc[:, '網路好店上架會員數'].replace(0, np.NaN).mean(axis=0))==False else 0
    x6 = side_dt_LastMonth.loc[:, '買賣顧問上架會員數'].replace(0, np.NaN).mean(axis=0) if np.isnan(side_dt_LastMonth.loc[:, '買賣顧問上架會員數'].replace(0, np.NaN).mean(axis=0))==False else 0

    side_dt_LastMonth_ori = [x1, x2, x3, x4, x5, x6]
    x7 = sum(side_dt_LastMonth_ori)
    side_dt_LastMonth_ori.append(x7)
    side_dt_LastMonth_percent = []

    for i in side_dt_LastMonth_ori:
        side_dt_LastMonth_percent.append(int((i/side_dt_LastMonth_ori[-1])*100) if i != 0 else 0)

    side_dt_LastMonth_ori = list(map(lambda x: int(x) , side_dt_LastMonth_ori))
    
    #=======================當月平均上架數=============
    cstatus_dt_ThisMonth = cstatus_dt[(cstatus_dt['Date'] >= str(first_day)) & (cstatus_dt['Date'] <= str(end_date))]
    
    x1 = cstatus_dt_ThisMonth.loc[:, '一般車商上架數'].replace(0, np.NaN).mean(axis=0) if np.isnan(cstatus_dt_ThisMonth.loc[:, '一般車商上架數'].replace(0, np.NaN).mean(axis=0))==False else 0
    x2 = cstatus_dt_ThisMonth.loc[:, '一般賣家上架數'].replace(0, np.NaN).mean(axis=0) if np.isnan(cstatus_dt_ThisMonth.loc[:, '一般賣家上架數'].replace(0, np.NaN).mean(axis=0))==False else 0
    x3 = cstatus_dt_ThisMonth.loc[:, '原廠認證上架數'].replace(0, np.NaN).mean(axis=0) if np.isnan(cstatus_dt_ThisMonth.loc[:, '原廠認證上架數'].replace(0, np.NaN).mean(axis=0))==False else 0
    x4 = cstatus_dt_ThisMonth.loc[:, '平輸好店上架數'].replace(0, np.NaN).mean(axis=0) if np.isnan(cstatus_dt_ThisMonth.loc[:, '平輸好店上架數'].replace(0, np.NaN).mean(axis=0))==False else 0
    x5 = cstatus_dt_ThisMonth.loc[:, '網路好店上架數'].replace(0, np.NaN).mean(axis=0) if np.isnan(cstatus_dt_ThisMonth.loc[:, '網路好店上架數'].replace(0, np.NaN).mean(axis=0))==False else 0
    x6 = cstatus_dt_ThisMonth.loc[:, '買賣顧問上架數'].replace(0, np.NaN).mean(axis=0) if np.isnan(cstatus_dt_ThisMonth.loc[:, '買賣顧問上架數'].replace(0, np.NaN).mean(axis=0))==False else 0
    x7 = cstatus_dt_ThisMonth.loc[:, '聯盟館上架數'].replace(0, np.NaN).mean(axis=0) if np.isnan(cstatus_dt_ThisMonth.loc[:, '聯盟館上架數'].replace(0, np.NaN).mean(axis=0))==False else 0
    # x8 = cstatus_dt_ThisMonth.loc[:, '認證館(HAA)上架數'].replace(0, np.NaN).mean(axis=0) if np.isnan(cstatus_dt_ThisMonth.loc[:, '認證館(HAA)上架數'].replace(0, np.NaN).mean(axis=0))==False else 0

    # cstatus_dt_ThisMonth_ori = [x1, x2, x3, x4, x5, x6, x7, x8]
    cstatus_dt_ThisMonth_ori = [x1, x2, x3, x4, x5, x6, x7]
    x9 = sum(cstatus_dt_ThisMonth_ori)
    cstatus_dt_ThisMonth_ori.append(x9)
    cstatus_dt_ThisMonth_percent = []

    for i in cstatus_dt_ThisMonth_ori:
        cstatus_dt_ThisMonth_percent.append(int((i/cstatus_dt_ThisMonth_ori[-1])*100) if i != 0 else 0)
        
    cstatus_dt_ThisMonth_ori = list(map(lambda x: int(x) , cstatus_dt_ThisMonth_ori))

    #=======================當月平均上架會員數=============
    side_dt_ThisMonth = side_dt[(side_dt['Date']>=str(first_day)) & (side_dt['Date'] <= str(end_date))]
    x1 = side_dt_ThisMonth.loc[:, '一般車商上架會員數'].replace(0, np.NaN).mean(axis=0) if np.isnan(side_dt_ThisMonth.loc[:, '一般車商上架會員數'].replace(0, np.NaN).mean(axis=0))==False else 0
    x2 = side_dt_ThisMonth.loc[:, '一般賣家上架會員數'].replace(0, np.NaN).mean(axis=0) if np.isnan(side_dt_ThisMonth.loc[:, '一般賣家上架會員數'].replace(0, np.NaN).mean(axis=0))==False else 0 
    x3 = side_dt_ThisMonth.loc[:, '原廠認證上架會員數'].replace(0, np.NaN).mean(axis=0) if np.isnan(side_dt_ThisMonth.loc[:, '原廠認證上架會員數'].replace(0, np.NaN).mean(axis=0))==False else 0
    x4 = side_dt_ThisMonth.loc[:, '平輸好店上架會員數'].replace(0, np.NaN).mean(axis=0) if np.isnan(side_dt_ThisMonth.loc[:, '平輸好店上架會員數'].replace(0, np.NaN).mean(axis=0))==False else 0
    x5 = side_dt_ThisMonth.loc[:, '網路好店上架會員數'].replace(0, np.NaN).mean(axis=0) if np.isnan(side_dt_ThisMonth.loc[:, '網路好店上架會員數'].replace(0, np.NaN).mean(axis=0))==False else 0
    x6 = side_dt_ThisMonth.loc[:, '買賣顧問上架會員數'].replace(0, np.NaN).mean(axis=0) if np.isnan(side_dt_ThisMonth.loc[:, '買賣顧問上架會員數'].replace(0, np.NaN).mean(axis=0))==False else 0

    side_dt_ThisMonth_ori = [x1, x2, x3, x4, x5, x6]
    x7 = sum(side_dt_ThisMonth_ori)
    side_dt_ThisMonth_ori.append(x7)
    side_dt_ThisMonth_percent = []

    for i in side_dt_ThisMonth_ori:
        side_dt_ThisMonth_percent.append(int((i/side_dt_ThisMonth_ori[-1])*100) if i != 0 else 0)
        
    side_dt_ThisMonth_ori = list(map(lambda x: int(x) , side_dt_ThisMonth_ori))

    return  cstatus_dt_ori, cstatus_dt_percent, up_members_ori, up_members_percent, cstatus_dt_LastMonth_ori, cstatus_dt_LastMonth_percent, side_dt_LastMonth_ori, side_dt_LastMonth_percent, cstatus_dt_ThisMonth_ori, cstatus_dt_ThisMonth_percent, side_dt_ThisMonth_ori, side_dt_ThisMonth_percent


# PART 7: 數據寫入excel中
def append_to_excel():

    # 將資料寫入工作表
    wb = load_workbook(path + 'sales_update.xlsx')
    
    # 選擇第一個工作表 'average'工作表
    ws_ave = wb.worksheets[0]
    # 塞入average的值
    make_new_workbook(ws_ave)

    # 選擇第二個工作表 'main'工作表
    ws_main = wb.worksheets[1]
    # 擷取需要append至Excel的值
    row_main = list(main_dt.tail(1).iloc[0, :])
    print(row_main)

    #2021-09-08關掉GA為0就不寄送
    # for i in row_main:
    #     if (i == 0):
    #         msg = 'main數據中有0值, 此程式停止執行!'
    #         print(msg)
    #         sys.exit(1)

    ws_main.append(row_main)

    # 選擇第三個工作表 'side'工作表
    ws_side = wb.worksheets[2]
    # 擷取需要append至Excel的值
    row_side = list(side_dt.tail(1).iloc[0,:])
    print(row_side)
    for i in row_side:
        if (i == 0):
            msg = 'side數據中有0值, 此程式停止執行!'
            print(msg)
            sys.exit(1)
    ws_side.append(row_side)
    # SAVE the worksheet
    wb.save(path+'sales_update.xlsx')



# PART 8: 數據寫入image中
def append_to_graph():
# 擷取製圖需要的數據
    b1day_dt =  main_dt.tail(1)
    print(b1day_dt)
    # if (0 in b1day_dt.values) == True:
    #     msg = '數據中有0值, 此程式停止執行!'
    #     print(msg)
    #     sys.exit(1)
#     b1day_dt = main_dt_lastDay[main_dt_lastDay['Date'] == end_date]

    x1 = b1day_dt.loc[:, '一般車商上架數'].values
    x2 = b1day_dt.loc[:, '原廠認證上架數'].values
    x3 = b1day_dt.loc[:, '平輸好店上架數'].values
    x4 = b1day_dt.loc[:, '網路好店上架數'].values
    x5 = b1day_dt.loc[:, '買賣顧問上架數'].values
    x6 = b1day_dt.loc[:, '總上架數'].values
    x7 = b1day_dt.loc[:, '到站人次'].values
    x8 = b1day_dt.loc[:, '到站新客'].values
    x9 = b1day_dt.loc[:, '總瀏覽數'].values
    x10 = b1day_dt.loc[:, '物件總瀏覽數'].values

    labels = f'一般車商上架數{x1}', f'原廠認證上架數{x2}', f'平輸好店上架數{x3}', f'網路好店上架數{x4}', \
             f'買賣顧問上架數{x5}', f'總上架數{x6}', f'到站人次{x7}', f'到站新客{x8}', f'總瀏覽數{x9}', f'總物件瀏覽數{x10}'
    size = [10, 10, 10, 10, 10, 10, 10, 10, 10, 10]
    explode = (0, 0, 0, 0, 0, 0, 0, 0, 0, 0)

    # 設置製圖的變數
    mpl.rcParams[u'font.sans-serif'] = ['simhei']
    mpl.rcParams['axes.unicode_minus'] = False
    mpl.rcParams['font.size'] = 20.0
    mpl.rcParams['figure.figsize'] = 14, 9
    colors = ['red', 'orange', 'yellow', 'green', 'blue', 'lightblue', 'purple', 'pink', 'lightgreen', 'grey']

    # 選取圖形為圓餅圖
    plt.pie(size, labels=labels, colors=colors, shadow=True, explode=explode)

    # 顯示圖中間的字體
    plt.text(-0.55, 0.03, '{0}日營運指標'.format(end_date), size=25)

    centre_circle = plt.Circle((0, 0), 0.6, color='grey', fc='white', linewidth=1)
    fig = plt.gcf()
    fig.gca().add_artist(centre_circle)
    plt.axis('equal')
    fig.savefig(path+'sales_graph.pdf')
    

# 每天製作新的工作表
def make_new_workbook(ws):
    # wb = Workbook()
    # ws = wb.active

    #========上架數========== 
    headerTitle = ['上架車格數', end_date+'當日', '當月平均', '上月平均']
    index = 0
    for i in range (1, 8):
        if(((i+1)%2) == 0 ):
            if(headerTitle[index] == '上架車格數'):
                header(ws, 2, i+1, headerTitle[index], '占比')
                index+=1
            else:
                header(ws, 2, i+1, headerTitle[index])
                index+=1

    targetTitle = ['一般車商上架數', '一般賣家上架數', '原廠認證上架數', '平輸好店上架數', '網路好店上架數', '買賣顧問上架數', '聯盟館上架數', '合計']
    index = 0        
    for i in range (3, 19):
        if(((i+1)%2) == 0 ):        
            target(ws, i+1, targetTitle[index])
            index+=1

    #當日
    df = pd.DataFrame(cstatus_dt_ori, columns=[end_date])
    df_per = pd.DataFrame(cstatus_dt_percent, columns=[end_date]).astype(str)
    index = 0
    for i in range (3, 19):
        if(((i+1)%2) == 0 ):  
            addValueVertical(ws, 4, i+1, df[end_date][index], df_per[end_date][index])
            index +=1

    #當月
    df = pd.DataFrame(cstatus_dt_ThisMonth_ori, columns=[end_date])
    df_per = pd.DataFrame(cstatus_dt_ThisMonth_percent, columns=[end_date]).astype(str)
    index = 0
    for i in range (3, 19):
        if(((i+1)%2) == 0 ):  
            addValueVertical(ws, 6, i+1, df[end_date][index], df_per[end_date][index])
            index +=1

    #上月
    df = pd.DataFrame(cstatus_dt_LastMonth_ori, columns=[end_date])
    df_per = pd.DataFrame(cstatus_dt_LastMonth_percent, columns=[end_date]).astype(str)
    index = 0        
    for i in range (3, 19):
        if(((i+1)%2) == 0 ):  
            addValueVertical(ws, 8, i+1, df[end_date][index], df_per[end_date][index])
            index +=1

    #========車商上架會員數========== 
    headerTitle = ['車商上架家數', end_date+'當日', '當月平均', '上月平均']
    index = 0
    for i in range (1, 8):
        if(((i+1)%2) == 0 ):
            if(headerTitle[index] == '車商上架家數'):
                header(ws, 22, i+1, headerTitle[index], '占比')
                index+=1
            else:
                header(ws, 22, i+1, headerTitle[index])
                index+=1

    targetTitle = ['一般車商上架會員數', '一般賣家上架會員數', '原廠認證上架會員數', '平輸好店上架會員數', '網路好店上架會員數', '買賣顧問上架會員數', '合計']
    index = 0 
    for i in range (23, 37):
        if(((i+1)%2) == 0 ):        
            target(ws, i+1, targetTitle[index])
            index+=1

    #當日
    df = pd.DataFrame(up_members_ori, columns=[end_date])
    df_per = pd.DataFrame(up_members_percent, columns=[end_date]).astype(str)
    index = 0
    for i in range (23, 37):
        if(((i+1)%2) == 0 ):  
            addValueVertical(ws, 4, i+1, df[end_date][index], df_per[end_date][index])
            index +=1

    #當月
    df = pd.DataFrame(side_dt_ThisMonth_ori, columns=[end_date])
    df_per = pd.DataFrame(side_dt_ThisMonth_percent, columns=[end_date]).astype(str)
    index = 0
    for i in range (23, 37):
        if(((i+1)%2) == 0 ):  
            addValueVertical(ws, 6, i+1, df[end_date][index], df_per[end_date][index])
            index +=1

    #上月
    df = pd.DataFrame(side_dt_LastMonth_ori, columns=[end_date])
    df_per = pd.DataFrame(side_dt_LastMonth_percent, columns=[end_date]).astype(str)
    index = 0        
    for i in range (23, 37):
        if(((i+1)%2) == 0 ):  
            addValueVertical(ws, 8, i+1, df[end_date][index], df_per[end_date][index])
            index +=1
            
    ws.column_dimensions['B'].width = 23

    # wb.save("styled.xlsx")
    
if __name__ == "__main__":
    try:
        # car_status, mem_status, person_status, empty, ga, ga_car, upload_members, \
        #            on_shelf, index_mcategory, alliance, certification, tbl_in_list = read_data()
        car_status, mem_status, person_status, empty, ga, ga_car, upload_members, \
            on_shelf, index_mcategory, alliance, tbl_in_list = read_data()

        if (0 in tbl_in_list.values) == True:
            msg = 'PART 1: 數據中有0值, 此程式停止執行!'
            print(msg)
            sys.exit(1)
        else:
            print('PART 1: 數據正確且無0值')

    except Exception as e:
        msg = 'PART 1:讀取數據發生錯誤. The error message : {}'.format(e)
        send_daily_report_error(msg)
        print(msg)
        sys.exit(1)

    try:
        index_mcategory, mem_status_all, car_status, on_shelf, upload_members = add_combine_col()
        msg = 'PART 2 : 新增Combine欄位正確'
        print(msg)
    except Exception as e:
        msg = 'PART 2: 新增Combine欄位發生錯誤. The error message :{}'.format(e)
        send_daily_report_error(msg)
        print(msg)
        sys.exit(1)

    try:
        members_dt = groupby_transpose(mem_status_all, 'Members', '會員數')
        cstatus_dt = groupby_transpose(car_status, 'Sales', '上架數')
        # onshelf_dt = groupby_transpose(on_shelf, 'ONshelf', '待售數')
        up_members = groupby_transpose(upload_members, 'count(distinct(MemberID))', '上架會員數')
        msg = 'PART 3 : 產出以會員身份為主的數據OK'
        print(msg)
    except Exception as e:
        msg = 'PART 3 : 產出以會員身份為主的數據發生錯誤. The error message :{}'.format(e)
        send_daily_report_error(msg)
        print(msg)
        sys.exit(1)

    try:
        # main_dt, cstatus_dt = organize_sheetMain(cstatus_dt, alliance, certification)
        main_dt, cstatus_dt = organize_sheetMain(cstatus_dt, alliance)
        side_dt = organize_sheetSide(up_members)
        msg = 'PART 4~5 : 資料整合OK'
        print(msg)
    except Exception as e:
        msg = 'PART 4~5 : 資料整合發生錯誤. The error message : {}'.format(e)
        send_daily_report_error(msg)
        print(msg)
        sys.exit(1)

    try:
        cstatus_dt_ori, cstatus_dt_percent, up_members_ori, up_members_percent, cstatus_dt_LastMonth_ori, cstatus_dt_LastMonth_percent, side_dt_LastMonth_ori, side_dt_LastMonth_percent, cstatus_dt_ThisMonth_ori, cstatus_dt_ThisMonth_percent, side_dt_ThisMonth_ori, side_dt_ThisMonth_percent = calPercentAndAverage(cstatus_dt, side_dt)
        msg = 'PART 6 : 計算平均及百分比OK'
        print(msg)
    except Exception as e:
        msg = 'PART 6: 計算平均及百分比發生錯誤. The error message : {}'.format(e)
        send_daily_report_error(msg)
        print(msg)
        sys.exit(1)

    try:
        append_to_graph()
        append_to_excel()
        msg = 'PART 7~8 : 數據寫入Excel/Graph OK'
        print(msg)
    except Exception as e:
        msg = 'PART 7~8 : 數據寫入Excel/Graph出現錯誤. The error message : {}'.format(e)
        send_daily_report_error(msg)
        print(msg)
        sys.exit(1)

    try:
        send_daily_report(receiver='j', path=path)
        msg = 'PART 9 : 寄送日報OK'
        print(msg)
    except Exception as e:
        msg = 'PART 9 : 寄送日報發生錯誤. The error message : {}'.format(e)
        send_daily_report_error(msg)
        print(msg)
        sys.exit(1)

# full_dt.fillna(0, inplace=True)
# mem_uploads_num.fillna(0, inplace=True)
#
# writer = pd.ExcelWriter('/media/sf_F_DRIVE/temp/sales_update_revise.xlsx', engine='openpyxl')
# full_dt.to_excel(writer, sheet_name='main', index=False)
# mem_uploads_num.to_excel(writer, sheet_name='side', index=False)
# workbook = writer.book
# worksheet1 = writer.sheets['main']
# worksheet2 = writer.sheets['side']
# writer.save()
