# coding: utf-8
import sys

env = "1"
if(env=="1"):
    # 正式機
    sys.path.insert(0, '/home/hadoop/jessielu/')
else:
    # 測試機
    sys.path.insert(0, '/home/jessie-ws/Dev/jupyterlab/daily_report/')


from Module.EmailFunc import send_error_message
from Module.ConnFunc import read_mysql
from datetime import datetime, timedelta, date
from dateutil.relativedelta import relativedelta  

import pymysql
import time
import pandas as pd
import requests

now = date.today()
exec_date = (now).strftime("%Y-%m-%d") 

# 讀取數據
def ReadData():
    query1 = f"""
    SELECT Date,CarID,0 as ViewCount 
    FROM status_new_v2
    WHERE Date = (SELECT MAX(Date) FROM status_new_v2)
    AND ViewCount is null 
    ORDER BY CarID DESC
    """
    Car = read_mysql(query1, 'report')    
    
    return Car
        
if __name__ == "__main__":

    # 設定日期
    script = 'abccar_viewcount_update'
    print(exec_date)
    
    try:
        Car = ReadData()
    except Exception as e:
        msg = '{0}: PART 1: 數據連線讀取失敗! The error message : {1}'.format(script, e)
        send_error_message(script='{}'.format(script), message=msg)
        print(msg)
        sys.exit(1)

    try:

        if(env=="1"):
            mysql_db = '172.31.42.43'
            api_host = '172.31.41.195'
        else:
            mysql_db = '35.75.189.198'
            api_host = '13.231.123.131'
            
        # 連結 SQL                   
        connect_db = pymysql.connect(host=mysql_db, port=3306, user='abccar', passwd='abccar', db='report', charset='utf8')       
        start_time = time.time()
        for index in Car.index: 
            #url = "http://13.231.123.131/Api/Car/GetCarInfo?CarID={0}&ViewCount=0".format(Car['CarID'][index])
            url = "http://{0}/Api/Car/GetCarInfo?CarID={1}&ViewCount=0".format(api_host,Car['CarID'][index])
            
            resp = requests.get(url)
            resp_dict = resp.json()
            Car['ViewCount'][index] = resp_dict.get('CarInfo').get('ViewCount')

            with connect_db.cursor() as cursor:
                sql = "UPDATE status_new_v2 SET ViewCount={0} WHERE Date='{1}' AND CarID={2};".format(Car['ViewCount'][index],Car['Date'][index],Car['CarID'][index])
                # 執行 SQL 指令
                cursor.execute(sql)
                connect_db.commit()

        # 關閉 SQL 連線
        connect_db.close()

        print("--- %s seconds ---" % (time.time() - start_time))
        print('Done!')
    except Exception as e:
        msg = '{0}: PART 2: 數據寫入資料庫出現錯誤! The error message : {1}'.format(script, e)
        send_error_message(script='{}'.format(script), message=msg)
        print(msg)
        sys.exit(1)