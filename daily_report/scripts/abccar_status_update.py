# coding: utf-8
import sys
sys.path.insert(0, '/home/hadoop/jessielu/')

from Module.EmailFunc import send_error_message
from Module.ConnFunc import read_testdb, to_mysql, read_test_mssql, read_mssql
# from Module.HelpFunc import display_set
from datetime import datetime, timedelta, date
from dateutil.relativedelta import relativedelta     

import pandas as pd
import requests

# display_set(200, 30)

#原本
now = date.today()
#補發用
# now = date.today() - relativedelta(days=1)

#這個月的第一天
first_day = date(now.year, now.month, 1)
#下個月的第一天
nextMonth_first_day = first_day + relativedelta(months=1)


# 讀取數據
def ReadData():
    # 紀錄每日上架CarID
    car_status_sg = f"""
    SELECT car1.CarID, car1.Email, car1.MemberID, car1.Status 
    FROM ABCCar.dbo.Car car1
    JOIN ABCCar.dbo.CarModel carmodel ON carmodel.IsEnable = 1 
                                      AND car1.BrandID = carmodel.BrandID 
                                      AND car1.SeriesID = carmodel.SeriesID 
                                      AND car1.CategoryID = carmodel.CategoryID
    INNER JOIN ABCCar.dbo.Member member ON car1.MemberID = member.MemberID
                                        AND member.Status = 1
                                        AND member.Category in (1,2,3,4,5)
    WHERE car1.Status = 1
    AND GETDATE() BETWEEN car1.StartDate AND car1.EndDate 
	AND car1.CarID not in (
			  -- 車商未綁卡且無申請儲值金
			  SELECT car.CarID FROM ABCCar.dbo.Car car
											JOIN ABCCar.dbo.CarModel carmodel ON carmodel.IsEnable = 1 
																			  AND car.BrandID = carmodel.BrandID 
																			  AND car.SeriesID = carmodel.SeriesID 
																			  AND car.CategoryID = carmodel.CategoryID
											INNER JOIN ABCCar.dbo.Member member  ON car.MemberID = member.MemberID
																			  AND member.Status = 1
																			  AND member.Category in (1,2,3,4,5)

											LEFT JOIN (SELECT * FROM [ABCCar].[dbo].[BindingCardMember] sm WITH (NOLOCK) 
														WHERE sm.[ID] = (SELECT Max(ID) FROM [ABCCar].[dbo].[BindingCardMember] sd WITH (NOLOCK) WHERE  sd.[MemberID]=sm.[MemberID] AND sd.[Status] in (0,99)) 
														) bm ON member.[MemberID] = bm.[MemberID] AND bm.[Status] in (0,99) 
											LEFT JOIN [ABCCar].[dbo].[MemberUserAccountSalesMapping] mua WITH (NOLOCK) ON member.[MemberID] = mua.[MemberID]
											LEFT JOIN [ABCCar].[dbo].[UserAccountSales] ua WITH (NOLOCK) ON ua.[ID] = mua.[UserAccountSalesID]
											OUTER APPLY 
													(SELECT TOP 1 *
													FROM [ABCCar].[dbo].[PoolPointsHistory] ph1 with (NOLOCK)
													WHERE member.MemberID = ph1.MemberID AND ph1.RecordTypeID = 10
													ORDER BY ph1.ID DESC
											) AS ph
											WHERE car.Status = 1
											AND GETDATE() BETWEEN car.StartDate AND car.EndDate
											AND member.Category<>'4'
											AND bm.BindingDate IS NULL 
											AND ph.ID IS NULL
	)
    """
    # 紀錄每日上架CarID/負責業代(區域代碼) 計算上架天數 
    car_status_sg_detail = f"""
    SELECT car.CarID
            ,DATEDIFF(day, car.StartDate, getdate()) AS OnlineDays
            ,ISNULL( (CASE WHEN (ua.[ZoneName]>0 and ua.[ZoneName] <= 5) THEN ua.[ZoneName] ELSE '0' END), '0') AS ZoneID
            ,ISNULL(ua.[NameSeq], '0') AS SaleSeq
            ,ISNULL(ua.[ID], '0') AS SaleID
            ,car.MemberID 
            ,member.Category
            ,member.VIPLevel
			--,bm.BindingDate
			--,ph.ID            
    FROM ABCCar.dbo.Car car
    JOIN ABCCar.dbo.CarModel carmodel ON carmodel.IsEnable = 1 
                                      AND car.BrandID = carmodel.BrandID 
                                      AND car.SeriesID = carmodel.SeriesID 
                                      AND car.CategoryID = carmodel.CategoryID
    INNER JOIN ABCCar.dbo.Member member  ON car.MemberID = member.MemberID
                                      AND member.Status = 1
                                      AND member.Category in (1,2,3,4,5) 
	LEFT JOIN (SELECT * FROM [ABCCar].[dbo].[BindingCardMember] sm WITH (NOLOCK) 
		           WHERE sm.[ID] = (SELECT Max(ID) FROM [ABCCar].[dbo].[BindingCardMember] sd WITH (NOLOCK) WHERE  sd.[MemberID]=sm.[MemberID] AND sd.[Status] in (0,99)) 
				  ) bm ON member.[MemberID] = bm.[MemberID] AND bm.[Status] in (0,99) 
    LEFT JOIN [ABCCar].[dbo].[MemberUserAccountSalesMapping] mua WITH (NOLOCK) ON member.[MemberID] = mua.[MemberID]
    LEFT JOIN [ABCCar].[dbo].[UserAccountSales] ua WITH (NOLOCK) ON ua.[ID] = mua.[UserAccountSalesID]
	OUTER APPLY 
            (SELECT TOP 1 *
            FROM [ABCCar].[dbo].[PoolPointsHistory] ph1 with (NOLOCK)
            WHERE member.MemberID = ph1.MemberID AND ph1.RecordTypeID = 10
            ORDER BY ph1.ID DESC
	) AS ph 
    WHERE car.Status = 1
    AND GETDATE() BETWEEN car.StartDate AND car.EndDate 
    AND (bm.BindingDate IS NOT NULL OR ph.ID IS NOT NULL)
    """

    # 用於mapping會員資訊於每日上架CarID
    mem_sg = f"""
    SELECT MemberID, Category, VIPLevel, Status FROM ABCCar.dbo.Member
    """
    # 紀錄每日空格
    empty_status_sg = f"""
    SELECT sum(GridBalance) Empties
    from ABCCar.dbo.GridStock with (nolock)
    where GridIfEnabled IN (0,1)
    """
    # 紀錄每日會員狀態
    mem_status_sg = f"""
    SELECT Email, MemberID, Category, VIPLevel, Status 
    FROM ABCCar.dbo.Member WHERE Status = '1' AND Category IN (3,4,5) AND MemberID NOT IN (18384, 46142, 204034)
    """
    # 紀錄個人身份有viplevel非0
    person_status_sg = f"""
    SELECT Email, MemberID, Category, VIPLevel, Status 
    FROM ABCCar.dbo.Member WHERE Status = '1' AND Category in (2) AND VIPLevel > '0'
    """
    # 紀錄每日各Category會員數 # VIPLevel有個人賣家 (業務說個人賣家應該沒有viplevel非0)
    member_num_sg = f"""
    SELECT Category, count(*) Numbers FROM ABCCar.dbo.Member WHERE Status = '1' AND 
    Category in (1, 2, 3, 4) GROUP BY Category
    """
    # 讀取站上物件數
    onshelf_new_sg = f"""
    select e.MemberID, e.Category, e.VIPLevel, count(*) as ONshelf
    from ABCCar.dbo.Car as a with (nolock)
    left join ABCCar.dbo.Member as e on a.MemberID=e.MemberID
    where a.status = 0
    GROUP BY e.MemberID, e.Category, e.VIPLevel;
    """

    # 每日新增聯盟館數量
    alliance_sa = f"""
    --抓最新批次的認證車
    select 123 HotCarBatchID, count(*) Number
    from (
        select * from abccar.dbo.HotCar
        where HotCarBatchID = (select top 1 ID from abccar.dbo.HotCarBatch
        order by CreateDate desc)  and CERSTS = 4
        UNION ALL
        --抓最新批次的非認證車且在6/30(ID=272)批次中
        select * from abccar.dbo.HotCar
        where HotCarBatchID = (select top 1 ID from abccar.dbo.HotCarBatch
        order by CreateDate desc)  and CERSTS <> 4 and SourceKey in (select SourceKey from abccar.dbo.HotCar
        where HotCarBatchID = 272)
        --and CERSTS <> 4
    ) x
    """

    # 每日新增認證館數量
    certification_sa = f"""
    select count(*) as Number from [ABCCar].[dbo].Car with (nolock)
    where Source = 'HAA'
    and SourceKey is not null
    and Status = 1
    and getdate() between StartDate and EndDate
    """ 

    #取得個別Lexus CPO、Toyota CPO的數量
    cpo_sg = f"""
    select 
        (select count(*) from abccar.dbo.Car with (nolock) where Status = 1 and GETDATE() between StartDate and EndDate and MemberID in (select MemberID from abccar.dbo.member with (nolock) where Category = 4 and Status = 1 and BrandID IN (101)) ) as LCPO
        ,(select count(*) from abccar.dbo.Car with (nolock) where Status = 1 and GETDATE() between StartDate and EndDate and MemberID in (select MemberID from abccar.dbo.member with (nolock) where Category = 4 and Status = 1 and BrandID IN (132)) ) as TCPO
        ,(select count(*) from abccar.dbo.Car with (nolock) where Status = 1 and GETDATE() between StartDate and EndDate and MemberID in (select MemberID from abccar.dbo.member with (nolock) where Category = 4 and Status = 1 and BrandID not IN (132, 101)) ) as OtherCPO
        ,(select count(*) from abccar.dbo.Car with (nolock) where Status = 1 and GETDATE() between StartDate and EndDate and MemberID in (select MemberID from abccar.dbo.member with (nolock) where Category = 4 and Status = 1 )) as Total
    """

     #不二價瀏覽數
    car_fixprice_sg = f"""
	select 
    '901' as CampaignID,
    c.CarID,
    c.ViewCount
    from abccar.dbo.Car c with (NOLOCK)
    where c.CarID in (
    SELECT CarID
    FROM abccar.dbo.CampaignItem with (NOLOCK)
    WHERE (CampaignID = 901) )
	and c.Status = 1
	and GetDate() Between c.[StartDate] And c.[EndDate]
    """
    
    # 執行讀取程式
    car_status_new = read_test_mssql(car_status_sg, 'ABCCar')
    car_status_new_detail = read_test_mssql(car_status_sg_detail, 'ABCCar')
    mem = read_test_mssql(mem_sg, 'ABCCar')
    empty_new = read_test_mssql(empty_status_sg, 'ABCCar')
    mem_status = read_test_mssql(mem_status_sg, 'ABCCar')
    person_status = read_test_mssql(person_status_sg, 'ABCCar')
    member_num = read_test_mssql(member_num_sg, 'ABCCar')
    onshelf_new = read_test_mssql(onshelf_new_sg, 'ABCCar')
    alliance = read_test_mssql(alliance_sa, 'ABCCar')
    certification = read_test_mssql(certification_sa, 'ABCCar')
    cpo = read_test_mssql(cpo_sg, 'ABCCar')
    car_fixprice = read_test_mssql(car_fixprice_sg, 'ABCCar')

    return car_status_new, car_status_new_detail, mem, empty_new, mem_status, person_status, member_num, onshelf_new, alliance, certification, cpo, car_fixprice

def data_prep():

    # 每日上架CarID - 新增member檔中的Category, VIPLevel, Date
    car_status = pd.merge(car_status_new, mem, on='MemberID', how='left')
    car_status.rename(columns={'Status_x':'Statusc', 'Status_y':'Statusm'}, inplace=True)
    car_status.insert(0, 'Date', b1day)

    # 新增Date
    car_status_detail = car_status_new_detail.copy()
    car_status_detail.insert(0, 'Date', b1day)
    
    # 新增Date
    empty = empty_new.copy()
    empty.insert(0, 'Date', b1day)
    # 新增Date
    mem_status = mem_status_new.append(person_status_new)
    mem_status.insert(0, 'Date', b1day)
    # 新增Date
    member_num.insert(0, 'Date', b1day)
    # 新增Date
    onshelf = onshelf_new.copy()
    onshelf = onshelf.groupby(['Category', 'VIPLevel'])['ONshelf'].sum().reset_index()
    onshelf.insert(0, 'Date', b1day)

    # 新增Date
    alliance_new = alliance.copy()
    alliance_new.insert(0, 'Date', b1day)

    # 新增Date
    certification_new = certification.copy()
    certification_new.insert(0, 'Date', b1day)

    # 新增Date
    cpo_new = cpo.copy()
    cpo_new.insert(0, 'Date', b1day)
    
    # 新增Date
    car_fixprice_new = car_fixprice.copy()
    car_fixprice_new.insert(0, 'Date', b1day)

    return car_status, car_status_detail, empty, mem_status, member_num, onshelf, alliance_new, certification_new, cpo_new, car_fixprice_new


def send_to_db(dt, tbl_name):
    num = 1000
    loop_no = round(len(dt) / num) + 1
    for i in range(0, loop_no):
        data = dt[i * num:i * num + num]
        to_mysql('report', data, tbl_name, 'append')


if __name__ == "__main__":

    # 設定日期
    b1day = now
    script = 'abccar_status_update'

    try:
        car_status_new, car_status_new_detail, mem, empty_new, mem_status_new, person_status_new, member_num, onshelf_new, alliance, certification, cpo, car_fixprice = ReadData()
    except Exception as e:
        msg = '{0}: PART 1: 數據連線讀取失敗! The error message : {1}'.format(script, e)
        send_error_message(script='{}'.format(script), message=msg)
        print(msg)
        sys.exit(1)

    try:
        car_status, car_status_detail, empty, mem_status, member_num, onshelf, alliance_new, certification_new, cpo_new, car_fixprice_new = data_prep()
    except Exception as e:
        msg = '{0}: PART 2: 數據清整過程出現錯誤! The error message : {1}'.format(script, e)
        send_error_message(script='{}'.format(script), message=msg)
        print(msg)
        sys.exit(1)

    try:
        send_to_db(car_status, 'status_new') #每日車輛狀態
        send_to_db(car_status_detail, 'status_new_detail') #每日車輛狀態明細
        # send_to_db(empty, 'empty_202010')
        send_to_db(mem_status, 'mstatus_vip') #每日會員狀態
        # send_to_db(member_num, 'mstatus_all')
        # send_to_db(onshelf, 'on_shelf_202010')
        send_to_db(alliance_new, 'alliance') # HOT車輛
        # send_to_db(certification_new, 'certification')
        send_to_db(cpo_new, 'cpo') # 原廠館
 
        print('status_new_v2 start.')
        send_to_db(car_status, 'status_new_v2') #每日車輛狀態v2 [新資料表]
               
               
        for index in car_fixprice_new.index:    
            url = "http://13.231.123.131/Api/Car/GetCarInfo?CarID={0}&ViewCount=0".format(car_fixprice_new['CarID'][index])
            resp = requests.get(url)
            resp_dict = resp.json()
            car_fixprice_new['ViewCount'][index] = resp_dict.get('CarInfo').get('ViewCount')
        send_to_db(car_fixprice_new, 'car_fixprice_new') # 不二價物件瀏覽
        

        print('Done!')
    except Exception as e:
        msg = '{0}: PART 3: 數據寫入資料庫出現錯誤! The error message : {1}'.format(script, e)
        send_error_message(script='{}'.format(script), message=msg)
        print(msg)
        sys.exit(1)
