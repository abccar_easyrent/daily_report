# coding: utf-8
import smtplib
from datetime import datetime, timedelta
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from email.utils import COMMASPACE


def send(receiver, path):
    # 設定日期
    b1day = (datetime.today()-timedelta(1)).strftime('%Y-%m-%d')

    sender = "hotaicyberbi@gmail.com"
    if receiver == 'g':
        receivers_list = ["ALEX@mail.hotaimotor.com.tw","BENSONWU@mail.hotaimotor.com.tw",
                          "JASMINE@mail.hotaimotor.com.tw","ERICHUANG@mail.hotaimotor.com.tw",
                          "EMMACHAO@mail.hotaimotor.com.tw","STEVENWEI@mail.hotaimotor.com.tw",
                          "SHUJUNGHSU@mail.hotaimotor.com.tw","KOILO@mail.hotaimotor.com.tw",
                          "ANDREW@mail.hotaimotor.com.tw","VERAWANG@mail.hotaimotor.com.tw",
                          "CHENTSUNGTE@mail.hotaimotor.com.tw","MAGGIECHEN@mail.hotaimotor.com.tw",
                          "JACYRAO@mail.hotaimotor.com.tw","MANDYLIN@mail.hotaimotor.com.tw","ANDREW@mail.hotaimotor.com.tw","ROMANCHEN@hotaileasing.com.tw"]

        #receivers_list = ["jasminewang1017@gmail.com", "jessielu@mail.hotaimotor.com.tw","SAMCHAN415@hotaimotor.com.tw"]
    elif receiver == 'j':
        receivers_list = ["ROMANCHEN@hotaileasing.com.tw"]


    msg = MIMEMultipart()
    msg['From'] = sender
    msg['To'] = COMMASPACE.join(receivers_list)
    msg['Subject'] = '虛擬號碼來電數日報_{0}'.format(b1day)

    # string to store the body of the mail
    body = """
    虛擬號碼日報。[備註:1.刪除內部的測試帳號.2.會員身份固定以每日早上9點狀態為主. 
    3.成功接聽數:通話10秒內+通話5分鐘內+通話5-10分鐘內+通話超過10分鐘.
    4.接聽率:成功接聽數/被撥打數。]"
    """
    # attach the body with the msg instance
    msg.attach(MIMEText(body, 'plain'))

    # open the file to be sent
    filename = "虛擬號碼來電數日報.xlsx"

    # instance of MIMEBase and named as p
    p = MIMEApplication(open(path+"虛擬號碼來電數日報.xlsx", "rb").read())
    p.add_header('Content-Disposition', "attachment", filename = filename)


    # attach the instance 'p' to instance 'msg'
    msg.attach(p)

    try:
        smtp = smtplib.SMTP('smtp.gmail.com:587')
        smtp.ehlo()
        smtp.starttls()
        smtp.login("hotaicyberbi@gmail.com", "@abccar2018")
        smtp.sendmail(sender, receivers_list, msg.as_string())
        print('send successfully')
    except:
        print('unable to send email')

def send_error(message):
    # 設定日期
    b1day = (datetime.today()-timedelta(1)).strftime('%Y-%m-%d')

    sender = "hotaicyberbi@gmail.com"
    receivers_list = ["ROMANCHEN@hotaileasing.com.tw"]

    msg = MIMEMultipart()
    msg['From'] = sender
    msg['To'] = COMMASPACE.join(receivers_list)
    msg['Subject'] = '虛擬號碼日報出錯訊息_{0}'.format(b1day)

    # string to store the body of the mail
    body = """
    虛擬號碼日報排程錯誤訊息。主要錯誤發生段落:{}
    """.format(message)

    # attach the body with the msg instance
    msg.attach(MIMEText(body, 'plain'))

    try:
        smtp = smtplib.SMTP('smtp.gmail.com:587')
        smtp.ehlo()
        smtp.starttls()
        smtp.login("hotaicyberbi@gmail.com", "@abccar2018")
        smtp.sendmail(sender, receivers_list, msg.as_string())
        print('send successfully')
    except:
        print('unable to send email')

