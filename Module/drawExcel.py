from openpyxl.styles import Border, Side, PatternFill, Font, GradientFill, Alignment
from openpyxl import Workbook

#===================Way One=====================
thin = Side(border_style="thin", color="000000")
double = Side(border_style="double", color="ff0000")

# top_left_cell.font  = Font(b=True, color="FF0000")

def header(ws, r, n, title, rate='占比'):
    
    #========Start part=========
    #四格為一組
    mg = ws.merge_cells(start_row=r, start_column=n, end_row=r+1, end_column=n)
    #左上
    left_t = ws.cell(row=r, column=n)
    left_t.value = title
    left_t.border = Border(top=thin, left=thin)
    left_t.fill = PatternFill("solid", fgColor="ffcc99")
    left_t.alignment = Alignment(horizontal="center", vertical="center")
    #左下
    left_d = ws.cell(row=r+1, column=n)
    left_d.border = Border(bottom=thin, left=thin)
    #右上
    right_t = ws.cell(row=r, column=n+1)
    right_t.border = Border(top=thin, right=thin, bottom=thin)
    right_t.fill = PatternFill("solid", fgColor="ffcc99")
    #右下
    right_d = ws.cell(row=r+1, column=n+1)
    right_d.border = Border(top=thin, right=thin, left=thin, bottom=thin)
    right_d.value = rate
    right_d.alignment = Alignment(horizontal="center", vertical="center")
    right_d.fill = PatternFill("solid", fgColor="ffcc99")

    #========End part===========

def target(ws, n, title):

    #========Start part=========
    mg = ws.merge_cells(start_row=n, start_column=2, end_row=n+1, end_column=2)
    
    left_t = ws.cell(row=n, column=2)
    left_t.value = title
    left_t.border = Border(top=thin, left=thin)
    left_t.alignment = Alignment(horizontal="center", vertical="center")
    
    left_d = ws.cell(row=n+1, column=2)
    left_d.border = Border(bottom=thin, left=thin)

    right_t = ws.cell(row=n, column=3)
    right_t.border = Border(top=thin, right=thin, bottom=thin)

    right_d = ws.cell(row=n+1, column=3)
    right_d.border = Border(top=thin, right=thin, left=thin, bottom=thin)
    right_d.value = "占比"
    right_d.alignment = Alignment(horizontal="center", vertical="center")

    #=========End part=========
    
def addValueVertical(ws, c, n, val, per):
    #sample{(ws, 4, 4, 744, 12), (ws, 4, 6, 55, 0.9)}, {(ws, 6, 4, 747, 12), (ws, 6, 6, 53, 0.9)}
    #========Start part=========
    formatVal = "{:,d}".format(val)
    mg = ws.merge_cells(start_row=n, start_column=c, end_row=n+1, end_column=c)
    
    left_t = ws.cell(row=n, column=c)
    left_t.value = formatVal
    left_t.border = Border(top=thin, left=thin)
    left_t.alignment = Alignment(horizontal="center", vertical="center")
    
    left_d = ws.cell(row=n+1, column=c)
    left_d.border = Border(bottom=thin, left=thin)

    right_t = ws.cell(row=n, column=c+1)
    right_t.border = Border(top=thin, right=thin, bottom=thin)

    right_d = ws.cell(row=n+1, column=c+1)
    right_d.border = Border(top=thin, right=thin, left=thin, bottom=thin)
    right_d.value = per+'%'
    right_d.alignment = Alignment(horizontal="center", vertical="center")
    
    #========End part=========
