import pandas as pd
from sklearn import preprocessing
import seaborn as sns
import matplotlib.pyplot as plt

# display format setting
def display_set(width, no_columns):

    pd.set_option('display.width', width)
    pd.set_option("display.max_columns", no_columns)

# 找重複值
def find_duplicate(df, indexColumn):

    duplicates = pd.concat(g for _, g in df.groupby(indexColumn) if len(g) > 1)
    return duplicates

# 標準化
def standardize_x(X_variables, dataframe):

    Standardize_columns = dataframe[X_variables]
    features = Standardize_columns.columns
    scaler = preprocessing.StandardScaler()
    dataframe[features] = scaler.fit_transform(dataframe[features])

    return dataframe

# pearson correlation & heatmap
def plot_corr(testData):

    correlation = testData.corr()
    plt.figure(figsize=(50, 30))
    sns.heatmap(correlation, vmax=1, square=True, annot=True, cmap='Greens')
    plt.title('Correlation between fearures')
