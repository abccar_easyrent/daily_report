# -*- coding: utf-8 -*-

import requests, os, time, base64

# 報表測試團隊[abc每日戰報-鄭翔如]
#prd_webhook_url = "https://hotaimotor.webhook.office.com/webhookb2/c2322d11-c40d-4cdb-9ca9-8fba4dbf5850@953deb22-c3f6-4a45-9c51-06e32ff88bf6/IncomingWebhook/9b10db07019e4f98820f845d0072e9f1/4970d771-3d45-43e5-9180-c1e247dac2bc"

# 報表測試團隊[一般-Sam]
prd_webhook_url = "https://hotaimotor.webhook.office.com/webhookb2/c2322d11-c40d-4cdb-9ca9-8fba4dbf5850@953deb22-c3f6-4a45-9c51-06e32ff88bf6/IncomingWebhook/4d6d6eae35d54e2f8561c3a16b598840/4970d771-3d45-43e5-9180-c1e247dac2bc"

dev_webhook_url = "https://hotaimotor.webhook.office.com/webhookb2/c2322d11-c40d-4cdb-9ca9-8fba4dbf5850@953deb22-c3f6-4a45-9c51-06e32ff88bf6/IncomingWebhook/4d6d6eae35d54e2f8561c3a16b598840/4970d771-3d45-43e5-9180-c1e247dac2bc"

# LINE [abc好車網每日戰報]
prd_token = '8EiMRmKg8M6bwjmSYCJZRhzd81rfGBiETvnD1ek9mdf'
# TL LINE [abc好車網每日戰報]
prd_token_TL = '1iHlkPXtEZdYn0R5oxhH1gtGYiDcY2joWXxm0CHkttc'

# HOT LINE [hot營運週報]
hot_prd_token = 'g97htk4bA3ewanOkNERvrZWISXCQ29kgegcaeXPY1c0'

# HAA LINE [HAA勁拍中心]
haa_prd_token = 'WVSOX85DlqMDdKogsJhoJORStQhcG2lUWXoQLH1pRmU'

# LINE [和運租車測試]
dev_token = 'HSuRzCKXXAluBiM33mD2EKU8tFF7rDnT12TXyYJBKtz'

# LINE [和運租車測試]
hot_dev_token = 'HSuRzCKXXAluBiM33mD2EKU8tFF7rDnT12TXyYJBKtz'

# LINE [和運租車測試]
haa_dev_token = 'HSuRzCKXXAluBiM33mD2EKU8tFF7rDnT12TXyYJBKtz'

# LINE [和運租車測試]
hr_dev_token = 'HSuRzCKXXAluBiM33mD2EKU8tFF7rDnT12TXyYJBKtz'

# HR LINE [HR]
#hr_prd_token = 'WX7irFxrJz3GlerkpQA8k7tPl8pJ05J93aSkAxBnvUZ'
hr_prd_token ='gIMCp7nRMppBOpSeEvTeeNBJIDB10qk3i0xWiwxTMUB'

# Teams_Channel 
def Teams_Notify(receiver, path, msg, FN):
    try:
        url = dev_webhook_url
        if receiver == 'g':
            url = prd_webhook_url

        with open( path+FN , "rb") as img_file:
            b64_string = base64.b64encode(img_file.read())

        b64_image=f"""data:image/png;base64,{b64_string.decode('utf-8')}"""
        
        # https://docs.microsoft.com/en-us/microsoftteams/platform/task-modules-and-cards/cards/cards-format?tabs=adaptive-md%2Cconnector-html
        # 只純傳圖檔,無文字訊息
        if msg == "":
            data = {
                "type": "message",
                "attachments": [{
                    "contentType": "application/vnd.microsoft.card.adaptive",
                    "contentUrl": "",
                    "content": {
                        "$schema": "http://adaptivecards.io/schemas/adaptive-card.json",
                        "type": "AdaptiveCard",
                        "version": 1.2,
                        "body": [{
                            "type": "Container", 
                            "items": []
                            },
                            {
                                "url": b64_image,
                                "type": "Image",
                                "msTeams": {
                                  "allowExpand": "true"
                                },
                                "horizontalAlignment": "right",
                                "width": "auto"
                            }]
                    }
                }]
            }
        else:
            data = {
                "type": "message",
                "attachments": [{
                    "contentType": "application/vnd.microsoft.card.adaptive",
                    "contentUrl": "",
                    "content": {
                        "$schema": "http://adaptivecards.io/schemas/adaptive-card.json",
                        "type": "AdaptiveCard",
                        "version": 1.2,
                        "body": [{
                            "type": "Container", 
                            "items": [{ 
                                        "text": msg,
                                        "type": "TextBlock",
                                        "color": "dark",
                                        "size": "large"
                                    }]
                            },
                            {
                                "url": b64_image,
                                "type": "Image",
                                "msTeams": {
                                  "allowExpand": "true"
                                },
                                "horizontalAlignment": "right",
                                "width": "auto"
                            }]
                    }
                }]
            }            
        headers = {
            "Content-Type": "application/json"
        }

        result = requests.post(url, json=data, headers=headers)
        if 200 <= result.status_code < 300:
            print(f"Webhook sent successfully.{result.status_code}")
        else:
            print(f"Webhook not sent with {result.status_code}, response:\n{result.json()}")
            
    except Exception as e:
        print('Webhook not sent because %s' % e)
        
# Line Notify 
def Line_Notify(receiver, path, msg, FN):
    try:

        token = dev_token
        if receiver == 'g':
            token = prd_token

        # HTTP 標頭參數與資料,不可無訊息文字
        headers = { "Authorization": "Bearer " + token }
        data = { 'message': msg }

        # 以 requests 發送 POST 請求
        if FN != '': 
            image = open( path + FN, 'rb')
            files = { 'imageFile': image }
            requests.post("https://notify-api.line.me/api/notify",
                            headers = headers, data = data, files = files)
        else:
            requests.post("https://notify-api.line.me/api/notify",
                            headers = headers, data = data)
            
        print('Line notify sent successfully.')
        
    except Exception as e:
        print('Line notify not sent because %s' % e)

# Line Notify 
def Line_Notify_TL(receiver, path, msg, FN):
    try:

        token = dev_token
        if receiver == 'g':
            token = prd_token_TL

        # HTTP 標頭參數與資料,不可無訊息文字
        headers = { "Authorization": "Bearer " + token }
        data = { 'message': msg }

        # 以 requests 發送 POST 請求
        if FN != '': 
            image = open( path + FN, 'rb')
            files = { 'imageFile': image }
            requests.post("https://notify-api.line.me/api/notify",
                            headers = headers, data = data, files = files)
        else:
            requests.post("https://notify-api.line.me/api/notify",
                            headers = headers, data = data)
            
        print('Line notify sent successfully.')
        
    except Exception as e:
        print('Line notify not sent because %s' % e)
      
# Hot Line Notify 每週營運報表 
def HOT_Line_Notify(receiver, path, msg, FN):
    try:

        token = hot_dev_token
        if receiver == 'g':
            token = hot_prd_token

        # HTTP 標頭參數與資料,不可無訊息文字
        headers = { "Authorization": "Bearer " + token }
        data = { 'message': msg }

        if FN != '': 
            image = open( path + FN, 'rb')
            files = { 'imageFile': image }

            # 以 requests 發送 POST 請求
            requests.post("https://notify-api.line.me/api/notify",
                            headers = headers, data = data, files = files)
        else:
            requests.post("https://notify-api.line.me/api/notify",
                            headers = headers, data = data)
            
        print('Line notify sent successfully.')
        
    except Exception as e:
        print('Line notify not sent because %s' % e)
        
# HAA Line Notify 拍場報表[星期二.星期五] 
def HAA_Line_Notify(receiver, path, msg, FN):
    try:

        token = haa_dev_token
        if receiver == 'g':
            token = haa_prd_token

        # HTTP 標頭參數與資料,不可無訊息文字
        headers = { "Authorization": "Bearer " + token }
        data = { 'message': msg }

        if FN != '': 
            image = open( path + FN, 'rb')
            files = { 'imageFile': image }

            # 以 requests 發送 POST 請求
            requests.post("https://notify-api.line.me/api/notify",
                            headers = headers, data = data, files = files)
        else:
            requests.post("https://notify-api.line.me/api/notify",
                            headers = headers, data = data)
            
        print('Line notify sent successfully.')
        
    except Exception as e:
        print('Line notify not sent because %s' % e)

# HR Line Notify 每月服務廠人力編制
def HR_Line_Notify(receiver, path, msg, FN):
    try:

        token = hr_dev_token
        if receiver == 'g':
            token = hr_prd_token

        # HTTP 標頭參數與資料,不可無訊息文字
        headers = { "Authorization": "Bearer " + token }
        data = { 'message': msg }

        if FN != '': 
            image = open( path + FN, 'rb')
            files = { 'imageFile': image }

            # 以 requests 發送 POST 請求
            requests.post("https://notify-api.line.me/api/notify",
                            headers = headers, data = data, files = files)
        else:
            requests.post("https://notify-api.line.me/api/notify",
                            headers = headers, data = data)
            
        print('Line notify sent successfully.')
        
    except Exception as e:
        print('Line notify not sent because %s' % e)                  