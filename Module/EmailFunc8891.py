# -*- coding: utf-8 -*-
import smtplib
from datetime import datetime, timedelta
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from email.mime.image import MIMEImage
from email.utils import COMMASPACE
from dateutil.relativedelta import relativedelta  


#  test
# global variables
b1day = (datetime.today() + relativedelta(days=1)).strftime('%Y-%m-%d') 
group_receiver_ls = ["PETERMAY@hotaimotor.com.tw", "HUNG57@hotaimotor.com.tw", "31879@hotaimotor.com.tw", "SAM903@hotaimotor.com.tw", "maggiechen@hotaimotor.com.tw", "HENNY@hotaimotor.com.tw", "LIN32093@hotaimotor.com.tw", "CHERRY5588@mail.hotaimotor.com.tw", "AARON711018@hotaimotor.com.tw", "TWEETY@hotaimotor.com.tw", "LSTTR12@hotaimotor.com.tw", "SS0210@hotaimotor.com.tw", "MIA0111@hotaimotor.com.tw", "MASA0204@hotaimotor.com.tw", "JOANNE373@hotaimotor.com.tw", "JONATHANKO@hotaimotor.com.tw", "WILLIAM@hotaimotor.com.tw", "STEVEWU@hotaimotor.com.tw", "MANJAK06@hotaimotor.com.tw", "ROMANCHEN@hotaileasing.com.tw"]
#group_receiver_ls = ["SAMCHAN415@hotaimotor.com.tw"]
private_receiver_ls = ["ROMANCHEN@hotaileasing.com.tw"]
sender_ls = "abccar.easyrent@gmail.com"


def smtp_connect(sender, receivers_list, msg):
    try:
        # Gmail Sign In
        gmail_sender = sender_ls
        gmail_passwd = 'sgawhwivypmmvqrk'  # 要至官網申請應用程式密碼

        smtp = smtplib.SMTP('smtp.gmail.com:587')
        smtp.ehlo()
        smtp.starttls()
        smtp.login(gmail_sender, gmail_passwd)
        smtp.sendmail(sender, receivers_list, msg.as_string())
        smtp.quit()
        print('send successfully')
    except Exception as e:
        print('Not sent email because %s' % e)

# 8891月報：Email寄送報告
def send_8891_report(receiver, path):

    sender = sender_ls
    if receiver == 'g':
        receivers_list = group_receiver_ls
    elif receiver == 'j':
        receivers_list = private_receiver_ls

    msg = MIMEMultipart()
    msg['From'] = sender
    msg['To'] = COMMASPACE.join(receivers_list)
    msg['Subject'] = 'abc好車網 8891月報_{0}'.format(b1day)

    # string to store the body of the mail
    body = """
    abc好車網 8891月報。
    請參閱，謝謝！
    """
    # attach the body with the msg instance
    msg.attach(MIMEText(body, 'plain'))

    # instance of MIMEApplication and named as p
    filename = "8891_StoreList.xlsx"
    p = MIMEApplication(open(path+"8891_StoreList.xlsx", "rb").read())
    p.add_header('Content-Disposition', "attachment", filename = filename)

    # attach the instance 'p' to instance 'msg'
    msg.attach(p)

    # log in smtp and send out email
    smtp_connect(sender, receivers_list, msg)


# 日報：Email寄送出錯訊息
def send_8891_report_error(message):

    sender = sender_ls
    receivers_list = ["ROMANCHEN@hotaileasing.com.tw"]

    msg = MIMEMultipart()
    msg['From'] = sender
    msg['To'] = COMMASPACE.join(receivers_list)
    msg['Subject'] = '8891日報出錯訊息_{0}'.format(b1day)

    # string to store the body of the mail
    body = """
    8891日報程式排程錯誤訊息。主要錯誤發生段落:{}
    """.format(message)

    # attach the body with the msg instance
    msg.attach(MIMEText(body, 'plain'))

    # log in smtp and send out email
    smtp_connect(sender, receivers_list, msg)

