# -*- coding: utf-8 -*-
import smtplib
from datetime import datetime, timedelta
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from email.mime.image import MIMEImage
from email.utils import COMMASPACE
from dateutil.relativedelta import relativedelta

#  test
# global variables
#原本
b1day = (datetime.today()).strftime('%Y-%m-%d')
#補發用
#b1day = (datetime.today() - relativedelta(days=1)).strftime('%Y-%m-%d')


group_receiver_ls = ["PETERMAY@hotaimotor.com.tw","HUNG57@hotaimotor.com.tw","31879@hotaimotor.com.tw","maggiechen@hotaimotor.com.tw","SAM903@hotaimotor.com.tw","TWEETY@hotaimotor.com.tw","LSTTR12@hotaimotor.com.tw", "SS0210@hotaimotor.com.tw", "MANJAK06@hotaimotor.com.tw", "MIA0111@hotaimotor.com.tw", "RUDYJIN@hotaimotor.com.tw","JOANNE373@hotaimotor.com.tw", "DENNYH@hotaimotor.com.tw", "WILLIAM@hotaimotor.com.tw","STEVEWU@hotaimotor.com.tw", "CHERRY5588@mail.hotaimotor.com.tw","EAGLE@hotaimotor.com.tw", "HENNY@hotaimotor.com.tw", "LIN32093@hotaimotor.com.tw", "ROMANCHEN@hotaileasing.com.tw"]
#group_receiver_ls = ["SAMCHAN415@hotaimotor.com.tw"]
private_receiver_ls = ["ROMANCHEN@hotaileasing.com.tw"]
cc = ["YSLIU@hotaimotor.com.tw", "FRED@hotaimotor.com.tw", "CARLO@hotaimotor.com.tw", "YIHUA23@hotaimotor.com.tw", "30709@hotaimotor.com.tw"]
sender_ls = "abccar.easyrent@gmail.com"


def smtp_connect(sender, receivers_list, msg):
    try:
        # Gmail Sign In
        gmail_sender = sender_ls
        gmail_passwd = 'sgawhwivypmmvqrk'  # 要至官網申請應用程式密碼

        smtp = smtplib.SMTP('smtp.gmail.com:587')
        smtp.ehlo()
        smtp.starttls()
        smtp.login(gmail_sender, gmail_passwd)
        smtp.sendmail(sender, receivers_list, msg.as_string())
        smtp.quit()
        print('send successfully')
    except Exception as e:
        print('Not sent email because %s' % e)
        
# 日報：Email寄送報告
def send_daily_report(receiver, path):

    sender = sender_ls
    msg = MIMEMultipart()
    msg['From'] = sender

    if receiver == 'g':
        receivers_list = group_receiver_ls
        msg['Cc'] = COMMASPACE.join(cc)

    elif receiver == 'j':
        receivers_list = private_receiver_ls
        # msg['Cc'] = COMMASPACE.join(receivers_list)

    msg['To'] = COMMASPACE.join(receivers_list)
    msg['Subject'] = 'abc好車網 業務日報_{0}'.format(b1day)

    # string to store the body of the mail
    body = """
    abc好車網_業務業績日報。
    請參閱，謝謝！
    """
    # attach the body with the msg instance
    msg.attach(MIMEText(body, 'plain'))

    # instance of MIMEApplication and named as p
    filename = "abc業績日報表.xlsx"
    p = MIMEApplication(open(path+"abc業績日報表.xlsx", "rb").read())
    p.add_header('Content-Disposition', "attachment", filename = filename)

    # attach the instance 'p' to instance 'msg'
    msg.attach(p)

    # log in smtp and send out email
    smtp_connect(sender, receivers_list, msg)

    

# 日報：Email寄送出錯訊息
def send_daily_report_error(message):

    sender = sender_ls
    receivers_list = ["ROMANCHEN@hotaileasing.com.tw"]

    msg = MIMEMultipart()
    msg['From'] = sender
    msg['To'] = COMMASPACE.join(receivers_list)
    msg['Subject'] = '業務日報出錯訊息_{0}'.format(b1day)

    # string to store the body of the mail
    body = """
    業務日報程式排程錯誤訊息。主要錯誤發生段落:{}
    """.format(message)

    # attach the body with the msg instance
    msg.attach(MIMEText(body, 'plain'))

    # log in smtp and send out email
    smtp_connect(sender, receivers_list, msg)


# 針對非日報, 處理資料中發生錯誤寄出錯誤訊息
def send_error_message(script, message):

    sender = sender_ls
    # 固定寄給自己
    receivers_list = private_receiver_ls

    msg = MIMEMultipart()
    msg['From'] = sender
    msg['To'] = COMMASPACE.join(receivers_list)
    msg['Subject'] = '執行 {} 程式時發生的錯誤訊息'.format(script)

    # string to store the body of the mail
    body = """
    當在執行 {0} 程式時出現錯誤訊息。主要錯誤發生段落:{1}
    """.format(script, message)

    # attach the body with the msg instance
    msg.attach(MIMEText(body, 'plain'))

    # log in smtp and send out email
    smtp_connect(sender, receivers_list, msg)

# 同意書日報：Email寄送報告
def send_daily_report_memberAgree(receiver, path):

    sender = sender_ls
    msg = MIMEMultipart()
    msg['From'] = sender

    if receiver == 'g':
        receivers_list = group_receiver_ls
        msg['Cc'] = COMMASPACE.join(cc)

    elif receiver == 'j':
        receivers_list = private_receiver_ls
        # msg['Cc'] = COMMASPACE.join(receivers_list)

    msg['To'] = COMMASPACE.join(receivers_list)
    msg['Subject'] = 'abc好車網 會員同意書日報_{0}'.format(b1day)

    # string to store the body of the mail
    body = """
    abc好車網 會員同意書日報。
    請參閱，謝謝！
    """
    # attach the body with the msg instance
    msg.attach(MIMEText(body, 'plain'))

    # instance of MIMEApplication and named as p
    filename = "abc好車網_會員同意書報表.xlsx"
    p = MIMEApplication(open(path+"abc好車網_會員同意書報表.xlsx", "rb").read())
    p.add_header('Content-Disposition', "attachment", filename = filename)

    # attach the instance 'p' to instance 'msg'
    msg.attach(p)

    # log in smtp and send out email
    smtp_connect(sender, receivers_list, msg)

# 架上台數日報：Email寄送報告
def send_daily_report_onself(receiver, path):

    group_receiver_ls = ['WILLIAM@hotaimotor.com.tw', 'STEVEWU@hotaimotor.com.tw','ROMANCHEN@hotaileasing.com.tw']
    #group_receiver_ls = ["SAMCHAN415@hotaimotor.com.tw"]
    private_receiver_ls = ["ROMANCHEN@hotaileasing.com.tw"]
    #cc = ['lujessie950410@gmail.com']
    sender_ls = "abccar.easyrent@gmail.com"

    sender = sender_ls
    msg = MIMEMultipart()
    msg['From'] = sender

    if receiver == 'g':
        receivers_list = group_receiver_ls
        # msg['Cc'] = COMMASPACE.join(cc)

    elif receiver == 'j':
        receivers_list = private_receiver_ls
        # msg['Cc'] = COMMASPACE.join(cc)

    msg['To'] = COMMASPACE.join(receivers_list)
    msg['Subject'] = 'abc好車網 業務架上台數報表_{0}'.format(b1day)

    # string to store the body of the mail
    body = """
    abc好車網 業務架上台數報表。
    請參閱，謝謝！
    """
    # attach the body with the msg instance
    msg.attach(MIMEText(body, 'plain'))

    # instance of MIMEApplication and named as p
    filename = "abc好車網_業務架上台數報表.xlsx"
    p = MIMEApplication(open(path+"abc好車網_業務架上台數報表.xlsx", "rb").read())
    p.add_header('Content-Disposition', "attachment", filename = filename)

    # attach the instance 'p' to instance 'msg'
    msg.attach(p)

    # log in smtp and send out email
    smtp_connect(sender, receivers_list, msg)

# 架上台數日報：Email寄送報告(無營運日報版本)
def send_daily_report_onself_miss(receiver, path):

    sender = sender_ls
    msg = MIMEMultipart()
    msg['From'] = sender

    if receiver == 'g':
        receivers_list = group_receiver_ls
        msg['Cc'] = COMMASPACE.join(cc)

    elif receiver == 'j':
        receivers_list = private_receiver_ls
        # msg['Cc'] = COMMASPACE.join(receivers_list)

    msg['To'] = COMMASPACE.join(receivers_list)
    msg['Subject'] = 'abc好車網 業務架上台數報表_{0}'.format(b1day)

    # string to store the body of the mail
    body = """
    abc好車網 業務架上台數報表。
    請參閱，謝謝！
    """
    # attach the body with the msg instance
    msg.attach(MIMEText(body, 'plain'))

    # instance of MIMEApplication and named as p
    filename = "abc好車網_業務架上台數報表.xlsx"
    p = MIMEApplication(open(path+"abc好車網_業務架上台數報表.xlsx", "rb").read())
    p.add_header('Content-Disposition', "attachment", filename = filename)

    # attach the instance 'p' to instance 'msg'
    msg.attach(p)

    # log in smtp and send out email
    smtp_connect(sender, receivers_list, msg)


# 架上台數日報：Email寄送報告__for 企劃 & 業務
def send_daily_report_onself_office(receiver, path):

    #原本
    #b1day = (datetime.today() - relativedelta(days=1)).strftime('%Y-%m-%d')
    
    b1day = (datetime.today() - relativedelta(days=1)).strftime('%Y-%m-%d')

    group_receiver_ls = ['HUNG57@hotaimotor.com.tw', 'JOANNE373@hotaimotor.com.tw', 'EAGLE@hotaimotor.com.tw', 'maggiechen@hotaimotor.com.tw', 'SAM903@hotaimotor.com.tw', 'HENNY@hotaimotor.com.tw', 'LIN32093@hotaimotor.com.tw', 'JONATHANKO@hotaimotor.com.tw', 'WILLIAM@hotaimotor.com.tw', 'STEVEWU@hotaimotor.com.tw', 'MANDYLIN@hotaimotor.com.tw', 'CHERRY5588@mail.hotaimotor.com.tw', 'TWEETY@hotaimotor.com.tw', 'LSTTR12@hotaimotor.com.tw', 'MIA0111@hotaimotor.com.tw', 'MASA0204@hotaimotor.com.tw', 'AARON711018@hotaimotor.com.tw', 'SS0210@hotaimotor.com.tw', 'MANJAK06@hotaimotor.com.tw', 'QIAN18@hotaileasing.com.tw', 'AARONWU@hotaimotor.com.tw','ANGELLIN@hotaileasing.com.tw', '0900EMMA@hotaileasing.com.tw', 'YULI1549@hotaileasing.com.tw', 'ROMANCHEN@hotaileasing.com.tw']
    # group_receiver_ls = ['WILLIAM@hotaimotor.com.tw', 'STEVEWU@hotaimotor.com.tw', 'MANDYLIN@hotaimotor.com.tw', 'MARKYAO@hotaimotor.com.tw','FARA@hotaimotor.com.tw']
    #group_receiver_ls = ["SAMCHAN415@hotaimotor.com.tw"]
    private_receiver_ls = ["ROMANCHEN@hotaileasing.com.tw"]
    # cc = ['JOANNE373@hotaimotor.com.tw', 'EAGLE@hotaimotor.com.tw', 'maggiechen@hotaimotor.com.tw', 'SAM903@hotaimotor.com.tw', 'HENNY@hotaimotor.com.tw', 'LIN32093@hotaimotor.com.tw']
    sender_ls = "abccar.easyrent@gmail.com"


    sender = sender_ls
    msg = MIMEMultipart()
    msg['From'] = sender

    if receiver == 'g':
        receivers_list = group_receiver_ls
        # msg['Cc'] = COMMASPACE.join(cc)

    elif receiver == 'j':
        receivers_list = private_receiver_ls
        # msg['Cc'] = COMMASPACE.join(receivers_list)

    msg['To'] = COMMASPACE.join(receivers_list)
    msg['Subject'] = 'abc好車網 架上台數報表_{0}'.format(b1day)

    # string to store the body of the mail
    body = """
    abc好車網 架上台數報表。
    請參閱，謝謝！
    """
    # attach the body with the msg instance
    msg.attach(MIMEText(body, 'plain'))

    # instance of MIMEApplication and named as p
    filename = "abc好車網_架上台數報表_企劃.xlsx"
    p = MIMEApplication(open(path+"abc好車網_架上台數報表_企劃.xlsx", "rb").read())
    p.add_header('Content-Disposition', "attachment", filename = filename)

    ### instance of MIMEApplication and named as p
    ##filename = "sales_graph.pdf"
    ##p2 = MIMEApplication(open(path+"sales_graph.pdf", "rb").read())
    ##p2.add_header('Content-Disposition', "attachment", filename = filename)

    # attach the instance 'p' and 'msgImage' to instance 'msg'
    msg.attach(p)
    ##msg.attach(p2)

    # log in smtp and send out email
    smtp_connect(sender, receivers_list, msg)

# 架上台數日報：Email寄送報告__for 長官
def send_daily_report_onself_sales(receiver, path):

    #原本
    #b1day = (datetime.today() - relativedelta(days=1)).strftime('%Y-%m-%d')
    b1day = (datetime.today() - relativedelta(days=1)).strftime('%Y-%m-%d')

    group_receiver_ls = ['YSLIU@hotaimotor.com.tw', 'FRED@hotaimotor.com.tw', 'TWUDER@hotaimotor.com.tw', 'AMANDACHEN@hotaimotor.com.tw', 'YIHUA23@hotaimotor.com.tw', 'PETERMAY@hotaimotor.com.tw', 'HUNG57@hotaimotor.com.tw', 'SAM903@hotaimotor.com.tw', '31879@hotaimotor.com.tw', 'DENNYH@hotaimotor.com.tw', 'JONATHANKO@hotaimotor.com.tw', 'WILLIAM@hotaimotor.com.tw', 'MANDYLIN@hotaimotor.com.tw', 'AARONWU@hotaimotor.com.tw','ANGELLIN@hotaileasing.com.tw', 'ROMANCHEN@hotaileasing.com.tw']
    # group_receiver_ls = ['WILLIAM@hotaimotor.com.tw', 'STEVEWU@hotaimotor.com.tw', 'MANDYLIN@hotaimotor.com.tw', 'MARKYAO@hotaimotor.com.tw','FARA@hotaimotor.com.tw']
    #group_receiver_ls = ["SAMCHAN415@hotaimotor.com.tw"]
    private_receiver_ls = ["ROMANCHEN@hotaileasing.com.tw"]
    # cc = ['YSLIU@hotaimotor.com.tw', 'FRED@hotaimotor.com.tw', 'CARLO@hotaimotor.com.tw', 'YIHUA23@hotaimotor.com.tw']
    sender_ls = "abccar.easyrent@gmail.com"

    sender = sender_ls
    msg = MIMEMultipart()
    msg['From'] = sender

    if receiver == 'g':
        receivers_list = group_receiver_ls
        # msg['Cc'] = COMMASPACE.join(cc)

    elif receiver == 'j':
        receivers_list = private_receiver_ls
        # msg['Cc'] = COMMASPACE.join(receivers_list)

    msg['To'] = COMMASPACE.join(receivers_list)
    msg['Subject'] = 'abc好車網 架上台數報表_{0}'.format(b1day)

    # string to store the body of the mail
    body = """
    abc好車網 架上台數報表。
    請參閱，謝謝！
    """
    # attach the body with the msg instance
    msg.attach(MIMEText(body, 'plain'))

    # instance of MIMEApplication and named as p
    filename = "abc好車網_架上台數報表.pdf"
    p = MIMEApplication(open(path+"abc好車網_架上台數報表.pdf", "rb").read())
    p.add_header('Content-Disposition', "attachment", filename = filename)

    # attach the instance 'p' to instance 'msg'
    msg.attach(p)

    # log in smtp and send out email
    smtp_connect(sender, receivers_list, msg)

# 欠款報表日報：Email寄送報告
def send_order_sales(receiver, path, mailList, FN):

    group_receiver_ls = mailList

    sender = sender_ls
    msg = MIMEMultipart()
    msg['From'] = sender

    if receiver == 'g':
        receivers_list = group_receiver_ls
        # msg['Cc'] = COMMASPACE.join(cc)

    elif receiver == 'j':
        receivers_list = private_receiver_ls
        # msg['Cc'] = COMMASPACE.join(receivers_list)

    msg['To'] = COMMASPACE.join(receivers_list)
    msg['Subject'] = 'abc好車網 會員欠款通知報表_{0}'.format(b1day)

    # string to store the body of the mail
    body = """
    abc好車網 會員欠款通知報表。
    請參閱，謝謝！
    """
    # attach the body with the msg instance
    msg.attach(MIMEText(body, 'plain'))

    # instance of MIMEApplication and named as p
    filename = FN + ".xlsx"
    p = MIMEApplication(open(path+ FN +".xlsx", "rb").read())
    p.add_header('Content-Disposition', "attachment", filename = filename)

    # attach the instance 'p' to instance 'msg'
    msg.attach(p)

    # log in smtp and send out email
    smtp_connect(sender, receivers_list, msg)

# 驗證過戶：Email寄送報告
def send_check_sales(receiver, path, mailList, FN):

    group_receiver_ls = mailList

    sender = sender_ls
    msg = MIMEMultipart()
    msg['From'] = sender

    if receiver == 'g':
        receivers_list = group_receiver_ls
        # msg['Cc'] = COMMASPACE.join(cc)

    elif receiver == 'j':
        receivers_list = private_receiver_ls
        # msg['Cc'] = COMMASPACE.join(receivers_list)

    msg['To'] = COMMASPACE.join(receivers_list)
    msg['Subject'] = 'abc好車網 驗證過戶報表_{0}'.format(b1day)

    # string to store the body of the mail
    body = """
    abc好車網 驗證過戶報表。
    請參閱，謝謝！
    """
    # attach the body with the msg instance
    msg.attach(MIMEText(body, 'plain'))

    # instance of MIMEApplication and named as p
    filename = FN + ".xlsx"
    p = MIMEApplication(open(path+ FN +".xlsx", "rb").read())
    p.add_header('Content-Disposition', "attachment", filename = filename)

    # attach the instance 'p' to instance 'msg'
    msg.attach(p)

    # log in smtp and send out email
    smtp_connect(sender, receivers_list, msg)

# 架上車輛總覽：Email寄送報告
def send_carList_report(receiver, path):

    group_receiver_ls = ['JONATHANKO@hotaimotor.com.tw', 'WILLIAM@hotaimotor.com.tw', 'STEVEWU@hotaimotor.com.tw', 'MANDYLIN@hotaimotor.com.tw', 'ROMANCHEN@hotaileasing.com.tw']
    # group_receiver_ls = ['WILLIAM@hotaimotor.com.tw', 'STEVEWU@hotaimotor.com.tw', 'MANDYLIN@hotaimotor.com.tw', 'MARKYAO@hotaimotor.com.tw','FARA@hotaimotor.com.tw']
    #group_receiver_ls = ["SAMCHAN415@hotaimotor.com.tw"]
    private_receiver_ls = ["ROMANCHEN@hotaileasing.com.tw"]
    #cc = ['lujessie950410@gmail.com']
    sender_ls = "abccar.easyrent@gmail.com"

    sender = sender_ls
    msg = MIMEMultipart()
    msg['From'] = sender

    if receiver == 'g':
        receivers_list = group_receiver_ls
        # msg['Cc'] = COMMASPACE.join(cc)

    elif receiver == 'j':
        receivers_list = private_receiver_ls
        # msg['Cc'] = COMMASPACE.join(cc)

    msg['To'] = COMMASPACE.join(receivers_list)
    msg['Subject'] = 'abc好車網 架上車輛總覽報表_{0}'.format(b1day)

    # string to store the body of the mail
    body = """
    abc好車網 架上車輛總覽報表。
    請參閱，謝謝！
    """
    # attach the body with the msg instance
    msg.attach(MIMEText(body, 'plain'))

    # instance of MIMEApplication and named as p
    filename = "架上車輛總覽_" +  str(b1day) + ".xlsx"
    p = MIMEApplication(open(path+"架上車輛總覽_" +  str(b1day) + ".xlsx", "rb").read())
    p.add_header('Content-Disposition', "attachment", filename = filename)

    # attach the instance 'p' to instance 'msg'
    msg.attach(p)

    # log in smtp and send out email
    smtp_connect(sender, receivers_list, msg)

# 安心購：Email寄送報告
def send_carBuy_report(receiver, path):

    group_receiver_ls = ['JONATHANKO@hotaimotor.com.tw', 'WILLIAM@hotaimotor.com.tw', 'STEVEWU@hotaimotor.com.tw', 'MANDYLIN@hotaimotor.com.tw', 'ROMANCHEN@hotaileasing.com.tw']
    # group_receiver_ls = ['WILLIAM@hotaimotor.com.tw', 'STEVEWU@hotaimotor.com.tw', 'MANDYLIN@hotaimotor.com.tw', 'MARKYAO@hotaimotor.com.tw','FARA@hotaimotor.com.tw']
    #group_receiver_ls = ["SAMCHAN415@hotaimotor.com.tw"]
    private_receiver_ls = ["ROMANCHEN@hotaileasing.com.tw"]
    #cc = ['lujessie950410@gmail.com']
    sender_ls = "abccar.easyrent@gmail.com"

    sender = sender_ls
    msg = MIMEMultipart()
    msg['From'] = sender

    if receiver == 'g':
        receivers_list = group_receiver_ls
        # msg['Cc'] = COMMASPACE.join(cc)

    elif receiver == 'j':
        receivers_list = private_receiver_ls
        # msg['Cc'] = COMMASPACE.join(cc)

    msg['To'] = COMMASPACE.join(receivers_list)
    msg['Subject'] = 'abc好車網 安心購報表_{0}'.format(b1day)

    # string to store the body of the mail
    body = """
    abc好車網 安心購報表。
    請參閱，謝謝！
    """
    # attach the body with the msg instance
    msg.attach(MIMEText(body, 'plain'))

    # instance of MIMEApplication and named as p
    filename = "安心購日報.xlsx"
    p = MIMEApplication(open(path+"安心購日報.xlsx", "rb").read())
    p.add_header('Content-Disposition', "attachment", filename = filename)

    # attach the instance 'p' to instance 'msg'
    msg.attach(p)

    # log in smtp and send out email
    smtp_connect(sender, receivers_list, msg)


# CPO (CPO_LEXUS_LINE) 網站成效CSV 寄送 
def send_cpo_report(receiver, path, FN):

    group_receiver_ls = ['STEVEWU@hotaimotor.com.tw', 'MANDYLIN@hotaimotor.com.tw', 'ROMANCHEN@hotaileasing.com.tw']
    # group_receiver_ls = ['WILLIAM@hotaimotor.com.tw', 'STEVEWU@hotaimotor.com.tw', 'MANDYLIN@hotaimotor.com.tw', 'MARKYAO@hotaimotor.com.tw','FARA@hotaimotor.com.tw']
    #group_receiver_ls = ['SAMCHAN415@hotaimotor.com.tw']
    private_receiver_ls = ["ROMANCHEN@hotaileasing.com.tw"]
    #cc = ['SAMCHAN415@hotaimotor.com.tw']
    sender_ls = "abccar.easyrent@gmail.com"

    sender = sender_ls
    msg = MIMEMultipart()
    msg['From'] = sender

    if receiver == 'g':
        receivers_list = group_receiver_ls
        # msg['Cc'] = COMMASPACE.join(cc)

    elif receiver == 'j':
        receivers_list = private_receiver_ls
        # msg['Cc'] = COMMASPACE.join(cc)
    
    # 系統日減一個月    
    b1month = (datetime.today() - relativedelta(months=1)).strftime('%Y-%m')
    
    msg['To'] = COMMASPACE.join(receivers_list)
    msg['Subject'] = 'CPO 網站成效_{0}'.format(b1month)

    # string to store the body of the mail
    body = """
    GA CPO 網站成效。
    請參閱，謝謝！
    """
    # attach the body with the msg instance
    msg.attach(MIMEText(body, 'plain'))

    # instance of MIMEApplication and named as p
    p = MIMEApplication(open(path + FN, "rb").read())
    p.add_header('Content-Disposition', "attachment", filename = FN)

    # attach the instance 'p' to instance 'msg'
    msg.attach(p)

    # log in smtp and send out email
    smtp_connect(sender, receivers_list, msg)
    
# 安心購上架物件數 寄送 
def send_safty_report(receiver, path, FN):

    group_receiver_ls = ['STEVEWU@hotaimotor.com.tw', 'ROMANCHEN@hotaileasing.com.tw']
    #group_receiver_ls = ['SAMCHAN415@hotaimotor.com.tw']
    private_receiver_ls = ["ROMANCHEN@hotaileasing.com.tw"]
    #cc = ['SAMCHAN415@hotaimotor.com.tw']
    sender_ls = "abccar.easyrent@gmail.com"

    sender = sender_ls
    msg = MIMEMultipart()
    msg['From'] = sender

    if receiver == 'g':
        receivers_list = group_receiver_ls
        # msg['Cc'] = COMMASPACE.join(cc)

    elif receiver == 'j':
        receivers_list = private_receiver_ls
        # msg['Cc'] = COMMASPACE.join(cc)
    

    msg['To'] = COMMASPACE.join(receivers_list)
    #msg['Subject'] = '安心購上架物件數_{0}'.format(b1day)
    msg['Subject'] = '安心購上架物件數'

    # string to store the body of the mail
    body = """
    安心購上架物件數。
    請參閱，謝謝！
    """
    # attach the body with the msg instance
    msg.attach(MIMEText(body, 'plain'))

    # instance of MIMEApplication and named as p
    p = MIMEApplication(open(path + FN, "rb").read())
    p.add_header('Content-Disposition', "attachment", filename = FN)

    # attach the instance 'p' to instance 'msg'
    msg.attach(p)

    # log in smtp and send out email
    smtp_connect(sender, receivers_list, msg)    
    
    
# 聊聊日報 寄送 
def send_chatroom_report(receiver, path, FN):

    group_receiver_ls = group_receiver_ls = ['JOANNE373@hotaimotor.com.tw', 'EAGLE@hotaimotor.com.tw', 'maggiechen@hotaimotor.com.tw', 'SAM903@hotaimotor.com.tw', 'HENNY@hotaimotor.com.tw', 'LIN32093@hotaimotor.com.tw', 'JONATHANKO@hotaimotor.com.tw', 'WILLIAM@hotaimotor.com.tw', 'STEVEWU@hotaimotor.com.tw', 'MANDYLIN@hotaimotor.com.tw', 'CHERRY5588@mail.hotaimotor.com.tw', 'TWEETY@hotaimotor.com.tw', 'LSTTR12@hotaimotor.com.tw', 'MIA0111@hotaimotor.com.tw', 'MASA0204@hotaimotor.com.tw', 'AARON711018@hotaimotor.com.tw', 'SS0210@hotaimotor.com.tw', 'MANJAK06@hotaimotor.com.tw', 'AARONWU@hotaimotor.com.tw', 'ANGELLIN@hotaileasing.com.tw', '0900EMMA@hotaileasing.com.tw', 'QIAN18@hotaileasing.com.tw', 'YULI1549@hotaileasing.com.tw', 'ROMANCHEN@hotaileasing.com.tw']
    # group_receiver_ls = ['WILLIAM@hotaimotor.com.tw', 'STEVEWU@hotaimotor.com.tw', 'MANDYLIN@hotaimotor.com.tw', 'MARKYAO@hotaimotor.com.tw','FARA@hotaimotor.com.tw']
    #group_receiver_ls = ['SAMCHAN415@hotaimotor.com.tw']
    private_receiver_ls = ["ROMANCHEN@hotaileasing.com.tw"]
    #cc = ['SAMCHAN415@hotaimotor.com.tw']
    sender_ls = "abccar.easyrent@gmail.com"

    sender = sender_ls
    msg = MIMEMultipart()
    msg['From'] = sender

    if receiver == 'g':
        receivers_list = group_receiver_ls
        # msg['Cc'] = COMMASPACE.join(cc)

    elif receiver == 'j':
        receivers_list = private_receiver_ls
        # msg['Cc'] = COMMASPACE.join(cc)
    

    msg['To'] = COMMASPACE.join(receivers_list)
    msg['Subject'] = 'abc好車網 聊聊日報_{0}'.format(b1day)

    # string to store the body of the mail
    body = """
    abc好車網 聊聊日報。
    請參閱，謝謝！
    """
    # attach the body with the msg instance
    msg.attach(MIMEText(body, 'plain'))

    # instance of MIMEApplication and named as p
    p = MIMEApplication(open(path + FN, "rb").read())
    p.add_header('Content-Disposition', "attachment", filename = FN)

    # attach the instance 'p' to instance 'msg'
    msg.attach(p)

    # log in smtp and send out email
    smtp_connect(sender, receivers_list, msg)

# 估車日報 寄送 
def send_estimate_report(receiver, path, FN):

    group_receiver_ls = ['HUNG57@hotaimotor.com.tw', '31879@hotaimotor.com.tw', 'MASA0204@hotaimotor.com.tw', 'maggiechen@hotaimotor.com.tw', 'SAM903@hotaimotor.com.tw', 'EAGLE@hotaimotor.com.tw', 'HENNY@hotaimotor.com.tw', 'LIN32093@hotaimotor.com.tw', 'JOANNE373@hotaimotor.com.tw', 'JONATHANKO@hotaimotor.com.tw', 'WILLIAM@hotaimotor.com.tw', 'STEVEWU@hotaimotor.com.tw', 'MANDYLIN@hotaimotor.com.tw','DENNYH@hotaileasing.com.tw', 'AARONWU@hotaimotor.com.tw','ANGELLIN@hotaileasing.com.tw','CHERRY5588@hotaileasing.com.tw','TWEETY@hotaileasing.com.tw','LSTTR12@hotaileasing.com.tw','MIA0111@hotaileasing.com.tw','AARON711018@hotaileasing.com.tw','SS0210@hotaileasing.com.tw','MANJAK06@hotaileasing.com.tw','QIAN18@hotaileasing.com.tw', '0900EMMA@hotaileasing.com.tw', 'YULI1549@hotaileasing.com.tw', 'ROMANCHEN@hotaileasing.com.tw']
    # group_receiver_ls = ['WILLIAM@hotaimotor.com.tw', 'STEVEWU@hotaimotor.com.tw', 'MANDYLIN@hotaimotor.com.tw', 'MARKYAO@hotaimotor.com.tw','FARA@hotaimotor.com.tw']
    private_receiver_ls = ["ROMANCHEN@hotaileasing.com.tw"]
    #cc = ['SAMCHAN415@hotaimotor.com.tw']
    sender_ls = "abccar.easyrent@gmail.com"

    sender = sender_ls
    msg = MIMEMultipart()
    msg['From'] = sender

    if receiver == 'g':
        receivers_list = group_receiver_ls
        # msg['Cc'] = COMMASPACE.join(cc)

    elif receiver == 'j':
        receivers_list = private_receiver_ls
        # msg['Cc'] = COMMASPACE.join(cc)
    

    msg['To'] = COMMASPACE.join(receivers_list)
    msg['Subject'] = 'abc好車網 線上預約估車日報_{0}'.format(b1day)

    # string to store the body of the mail
    body = """
    abc好車網 線上預約估車日報。
    請參閱，謝謝！
    """
    # attach the body with the msg instance
    msg.attach(MIMEText(body, 'plain'))

    # instance of MIMEApplication and named as p
    p = MIMEApplication(open(path + FN, "rb").read())
    p.add_header('Content-Disposition', "attachment", filename = FN)

    # attach the instance 'p' to instance 'msg'
    msg.attach(p)

    # log in smtp and send out email
    smtp_connect(sender, receivers_list, msg)  

# abc估車日報(新) 寄送 
def send_estimateNew_report(receiver, path, FN):

    group_receiver_ls = ['HUNG57@hotaileasing.com.tw','EAGLE@hotaileasing.com.tw','MAGGIECHEN@hotaileasing.com.tw','SAM903@hotaileasing.com.tw','HENNY@hotaileasing.com.t','LIN32093@hotaileasing.com.tw','CHERRY5588@hotaileasing.com.tw','TWEETY@hotaileasing.com.tw','LSTTR12@hotaileasing.com.tw','MIA0111@hotaileasing.com.tw','AARON711018@hotaileasing.com.tw','SS0210@hotaileasing.com.tw','MANJAK06@hotaileasing.com.tw','MASA0204@hotaileasing.com.tw','JOANNE373@hotaileasing.com.tw','AARONWU@hotaileasing.com.tw','JONATHANKO@hotaileasing.com.tw','WILLIAM@hotaileasing.com.tw','STEVEWU@hotaileasing.com.tw','MANDYLIN@hotaileasing.com.tw','ANGELLIN@hotaileasing.com.tw','ROMANCHEN@hotaileasing.com.tw']
    private_receiver_ls = ["ROMANCHEN@hotaileasing.com.tw"]
    sender_ls = "abccar.easyrent@gmail.com"

    sender = sender_ls
    msg = MIMEMultipart()
    msg['From'] = sender

    if receiver == 'g':
        receivers_list = group_receiver_ls       

    elif receiver == 'j':
        receivers_list = private_receiver_ls      

    msg['To'] = COMMASPACE.join(receivers_list)
    msg['Subject'] = 'abc好車網 abc估車日報_{0}'.format(b1day)

    # string to store the body of the mail
    body = """
    abc好車網 abc估車日報。
    請參閱，謝謝！
    """
    # attach the body with the msg instance
    msg.attach(MIMEText(body, 'plain'))

    # instance of MIMEApplication and named as p
    p = MIMEApplication(open(path + FN, "rb").read())
    p.add_header('Content-Disposition', "attachment", filename = FN)

    # attach the instance 'p' to instance 'msg'
    msg.attach(p)

    # log in smtp and send out email
    smtp_connect(sender, receivers_list, msg)  

# 實車在店日報：Email寄送報告
def send_carinstore_sales(receiver, path, mailList, FN):

    group_receiver_ls = mailList

    sender = sender_ls
    msg = MIMEMultipart()
    msg['From'] = sender

    if receiver == 'g':
        receivers_list = group_receiver_ls
        # msg['Cc'] = COMMASPACE.join(cc)

    elif receiver == 'j':
        receivers_list = private_receiver_ls
        # msg['Cc'] = COMMASPACE.join(receivers_list)

    msg['To'] = COMMASPACE.join(receivers_list)
    msg['Subject'] = 'abc好車網 實車在店報表_{0}'.format(b1day)

    # string to store the body of the mail
    body = """
    abc好車網 實車在店報表。
    請參閱，謝謝！
    """
    # attach the body with the msg instance
    msg.attach(MIMEText(body, 'plain'))

    # instance of MIMEApplication and named as p
    filename = FN + ".xlsx"
    p = MIMEApplication(open(path+ FN +".xlsx", "rb").read())
    p.add_header('Content-Disposition', "attachment", filename = filename)

    # attach the instance 'p' to instance 'msg'
    msg.attach(p)

    # log in smtp and send out email
    smtp_connect(sender, receivers_list, msg)


# 架上台數月報：Email寄送報告__for 企劃 & 業務
def send_monthly_report_onself_office(receiver, path):

    b1day = (datetime.today() - relativedelta(days=1)).strftime('%Y-%m')

    group_receiver_ls = ['HUNG57@hotaimotor.com.tw', 'JOANNE373@hotaimotor.com.tw', 'EAGLE@hotaimotor.com.tw', 'maggiechen@hotaimotor.com.tw', 'SAM903@hotaimotor.com.tw', 'HENNY@hotaimotor.com.tw', 'LIN32093@hotaimotor.com.tw', 'JONATHANKO@hotaimotor.com.tw', 'WILLIAM@hotaimotor.com.tw', 'STEVEWU@hotaimotor.com.tw', 'MANDYLIN@hotaimotor.com.tw', 'CHERRY5588@mail.hotaimotor.com.tw', 'TWEETY@hotaimotor.com.tw', 'LSTTR12@hotaimotor.com.tw', 'MIA0111@hotaimotor.com.tw', 'MASA0204@hotaimotor.com.tw', 'AARON711018@hotaimotor.com.tw', 'SS0210@hotaimotor.com.tw', 'MANJAK06@hotaimotor.com.tw', 'ROMANCHEN@hotaileasing.com.tw']
    # group_receiver_ls = ['WILLIAM@hotaimotor.com.tw', 'STEVEWU@hotaimotor.com.tw', 'MANDYLIN@hotaimotor.com.tw', 'MARKYAO@hotaimotor.com.tw','FARA@hotaimotor.com.tw']
    #group_receiver_ls = ["SAMCHAN415@hotaimotor.com.tw"]
    private_receiver_ls = ["ROMANCHEN@hotaileasing.com.tw"]
    # cc = ['JOANNE373@hotaimotor.com.tw', 'EAGLE@hotaimotor.com.tw', 'maggiechen@hotaimotor.com.tw', 'SAM903@hotaimotor.com.tw', 'HENNY@hotaimotor.com.tw', 'LIN32093@hotaimotor.com.tw']
    sender_ls = "abccar.easyrent@gmail.com"


    sender = sender_ls
    msg = MIMEMultipart()
    msg['From'] = sender

    if receiver == 'g':
        receivers_list = group_receiver_ls
        # msg['Cc'] = COMMASPACE.join(cc)

    elif receiver == 'j':
        receivers_list = private_receiver_ls
        # msg['Cc'] = COMMASPACE.join(receivers_list)

    msg['To'] = COMMASPACE.join(receivers_list)
    msg['Subject'] = 'abc好車網 架上台數月報表_{0}'.format(b1day)

    # string to store the body of the mail
    body = """
    abc好車網 架上台數月報表。
    請參閱，謝謝！
    """
    # attach the body with the msg instance
    msg.attach(MIMEText(body, 'plain'))

    # instance of MIMEApplication and named as p
    filename = "abc好車網_架上台數月報表.xlsx"
    p = MIMEApplication(open(path+"abc好車網_架上台數月報表.xlsx", "rb").read())
    p.add_header('Content-Disposition', "attachment", filename = filename)

    msg.attach(p)


    # log in smtp and send out email
    smtp_connect(sender, receivers_list, msg)