
#==============
# Path: /home/hadoop/jessielu/hr_report
# Description: Get google sheet transform format to pdf and cut image. Send notify to Line.
# Date: 2024/01/29
# Author: Sam 
#==============

# coding: utf-8

import sys, os, importlib
# 正式機
sys.path.insert(0, '/home/hadoop/jessielu/')

# 測試機
#sys.path.insert(0, '/home/jessie-ws/Dev/jupyterlab/daily_report/')

from openpyxl import Workbook, load_workbook

import pandas as pd
from datetime import datetime, timedelta, date
from dateutil.relativedelta import relativedelta  
#import math
#import shutil
import subprocess

from pdf2image import convert_from_path
from PIL import Image

# 重新載入更新後 Module
#importlib.reload(sys.modules['Module.NotifyFunc'])

from Module.NotifyFunc import HR_Line_Notify

#系統日
now = date.today()
#系統日前一日
end_date = (now - timedelta(1)).strftime("%Y-%m-%d")
#正式區路徑
path = '/home/hadoop/jessielu/hr_report/files/'

#測試機路徑
#path = '/home/jessie-ws/Dev/jupyterlab/daily_report/hr_report/files/'

os.chdir(path)

# 移除前次產出圖檔
imgs = ['hr_project.png','hr_project_crop.png']
for r in imgs:
    if os.path.isfile(r):
        os.remove(r)
        
def doc2pdf_linux(docPath, pdfPath):
    cmd = 'libreoffice --headless --convert-to pdf'.split()+[docPath] + ['--outdir'] + [pdfPath]
    p = subprocess.Popen(cmd, stderr=subprocess.PIPE, stdout=subprocess.PIPE,bufsize=1)
    p.wait(timeout=30)
    # stdout, stderr = p.communicate()
    # print(stdout, stderr)
    p.communicate()

def doc2pdf(docPath, pdfPath):
    docPathTrue = os.path.abspath(docPath)  
    return doc2pdf_linux(docPathTrue, pdfPath)

def mergeData():

    url = "https://docs.google.com/spreadsheets/d/e/2PACX-1vTBbHrxYxeGdqb0YPQLVIOckZlCS4Rx1yRX_yWRphFHULJEcOPXnZwtzFSKRq8IQH5V0RV__7apc7kW/pubhtml?gid=288668100&single=true"
    google_table = pd.read_html(url, encoding = 'utf8',)[0].fillna(0)
    #print(google_table)  
    google_table.columns = ['idx', 'title', 'lastmonth', 'grow', 'staff']
    tmpHead = google_table.loc[(google_table['lastmonth'] == '月底人數') , :]
    tmpA = google_table.loc[(google_table['title'] == '長租一') , :]
    tmpB = google_table.loc[(google_table['title'] == '長租二') , :]
    tmpC = google_table.loc[(google_table['title'] == '大口處') , :].iloc[:1]
    tmpD = google_table.loc[(google_table['title'] == '營改室') , :]
    
    tmpE = google_table.loc[(google_table['title'] == '北一') , :]
    tmpF = google_table.loc[(google_table['title'] == '北二') , :]
    tmpG = google_table.loc[(google_table['title'] == '直營一') , :]
    tmpH = google_table.loc[(google_table['title'] == '直營二') , :]
    tmpI = google_table.loc[(google_table['title'] == '桃園') , :]
    tmpJ = google_table.loc[(google_table['title'] == '新竹') , :]
    tmpK = google_table.loc[(google_table['title'] == '大口處') , :].iloc[-1:]
    tmpL = google_table.loc[(google_table['title'] == '台中') , :]
    tmpM = google_table.loc[(google_table['title'] == '彰化') , :]
    tmpN = google_table.loc[(google_table['title'] == '嘉義') , :]
    tmpO = google_table.loc[(google_table['title'] == '台南') , :]
    tmpP = google_table.loc[(google_table['title'] == '北高') , :]
    tmpQ = google_table.loc[(google_table['title'] == '南高') , :]
    tmpR = google_table.loc[(google_table['title'] == '中南直營') , :]

    ReportSendDate = google_table.loc[(google_table['title'] == '發送日') , :]
    
    #測試機路徑
    wordpath= path + 'HR.xlsx'
    #=========================== Load office file      
    wb = load_workbook(wordpath)

    #Sheet 1
    ws_main = wb.worksheets[0]

    # 年月
    data = ws_main.cell(row=1, column=1)
    data.value = tmpHead['title'].to_string(index=False)
    data = ws_main.cell(row=7, column=2)
    data.value = '{0}人數月報'.format(tmpHead['title'].to_string(index=False))
    data = ws_main.cell(row=13, column=2)
    data.value = '{0}人數月報'.format(tmpHead['title'].to_string(index=False))

    # 租賃營業本部_長租一 月底人數
    data = ws_main.cell(row=3, column=2)
    data.value = int(float(tmpA['lastmonth'].to_string(index=False)))
    # 租賃營業本部_長租一 編制人數
    data = ws_main.cell(row=4, column=2)
    data.value = int(float(tmpA['staff'].to_string(index=False)))
    
    # 租賃營業本部_長租二 月底人數
    data = ws_main.cell(row=3, column=4)
    data.value = int(float(tmpB['lastmonth'].to_string(index=False)))
    # 租賃營業本部_長租二 編制人數
    data = ws_main.cell(row=4, column=4)
    data.value = int(float(tmpB['staff'].to_string(index=False)))
    
    # 租賃營業本部_大口處 月底人數 
    data = ws_main.cell(row=3, column=6)
    data.value = int(float(tmpC['lastmonth'].to_string(index=False)))
    # 租賃營業本部_大口處 編制人數
    data = ws_main.cell(row=4, column=6)
    data.value = int(float(tmpC['staff'].to_string(index=False)))
    
    # 租賃營業本部_營改室 月底人數
    data = ws_main.cell(row=3, column=8)
    data.value = int(float(tmpD['lastmonth'].to_string(index=False)))
    # 租賃營業本部_營改室 編制人數
    data = ws_main.cell(row=4, column=8)
    data.value = int(float(tmpD['staff'].to_string(index=False)))
    
    
    
    # 北一 月底人數 / 增減 / 本年計劃人數 
    data = ws_main.cell(row=8, column=4)
    data.value = int(float(tmpE['lastmonth'].to_string(index=False)))
    data = ws_main.cell(row=9, column=4)
    data.value = int(float(tmpE['grow'].to_string(index=False)))
    data = ws_main.cell(row=10, column=4)
    data.value = int(float(tmpE['staff'].to_string(index=False)))
    #北二 月底人數 / 增減 / 本年計劃人數
    data = ws_main.cell(row=8, column=5)
    data.value = int(float(tmpF['lastmonth'].to_string(index=False)))
    data = ws_main.cell(row=9, column=5)
    data.value = int(float(tmpF['grow'].to_string(index=False)))
    data = ws_main.cell(row=10, column=5)
    data.value = int(float(tmpF['staff'].to_string(index=False)))
    #直營一 月底人數 / 增減 / 本年計劃人數 
    data = ws_main.cell(row=8, column=6)
    data.value = int(float(tmpG['lastmonth'].to_string(index=False)))
    data = ws_main.cell(row=9, column=6)
    data.value = int(float(tmpG['grow'].to_string(index=False)))
    data = ws_main.cell(row=10, column=6)
    data.value = int(float(tmpG['staff'].to_string(index=False)))
    #直營二 月底人數 / 增減 / 本年計劃人數 
    data = ws_main.cell(row=8, column=7)
    data.value = int(float(tmpH['lastmonth'].to_string(index=False)))
    data = ws_main.cell(row=9, column=7)
    data.value = int(float(tmpH['grow'].to_string(index=False)))
    data = ws_main.cell(row=10, column=7)
    data.value = int(float(tmpH['staff'].to_string(index=False)))
    #桃園 月底人數 / 增減 / 本年計劃人數
    data = ws_main.cell(row=8, column=8)
    data.value = int(float(tmpI['lastmonth'].to_string(index=False)))
    data = ws_main.cell(row=9, column=8)
    data.value = int(float(tmpI['grow'].to_string(index=False)))
    data = ws_main.cell(row=10, column=8)
    data.value = int(float(tmpI['staff'].to_string(index=False)))
    #新竹 月底人數 / 增減 / 本年計劃人數
    data = ws_main.cell(row=8, column=9)
    data.value = int(float(tmpJ['lastmonth'].to_string(index=False)))
    data = ws_main.cell(row=9, column=9)
    data.value = int(float(tmpJ['grow'].to_string(index=False)))
    data = ws_main.cell(row=10, column=9)
    data.value = int(float(tmpJ['staff'].to_string(index=False)))
    #大口處 月底人數 / 增減 / 本年計劃人數 
    data = ws_main.cell(row=8, column=10)
    data.value = int(float(tmpK['lastmonth'].to_string(index=False)))
    data = ws_main.cell(row=9, column=10)
    data.value = int(float(tmpK['grow'].to_string(index=False)))
    data = ws_main.cell(row=10, column=10)
    data.value = int(float(tmpK['staff'].to_string(index=False)))
    
    #台中 月底人數 / 增減 / 本年計劃人數
    data = ws_main.cell(row=14, column=4)
    data.value = int(float(tmpL['lastmonth'].to_string(index=False)))
    data = ws_main.cell(row=15, column=4)
    data.value = int(float(tmpL['grow'].to_string(index=False)))
    data = ws_main.cell(row=16, column=4)
    data.value = int(float(tmpL['staff'].to_string(index=False)))
    #彰化 月底人數 / 增減 / 本年計劃人數
    data = ws_main.cell(row=14, column=5)
    data.value = int(float(tmpM['lastmonth'].to_string(index=False)))
    data = ws_main.cell(row=15, column=5)
    data.value = int(float(tmpM['grow'].to_string(index=False)))
    data = ws_main.cell(row=16, column=5)
    data.value = int(float(tmpM['staff'].to_string(index=False)))
    #嘉義 月底人數 / 增減 / 本年計劃人數
    data = ws_main.cell(row=14, column=6)
    data.value = int(float(tmpN['lastmonth'].to_string(index=False)))
    data = ws_main.cell(row=15, column=6)
    data.value = int(float(tmpN['grow'].to_string(index=False)))
    data = ws_main.cell(row=16, column=6)
    data.value = int(float(tmpN['staff'].to_string(index=False)))
    #台南 月底人數 / 增減 / 本年計劃人數
    data = ws_main.cell(row=14, column=7)
    data.value = int(float(tmpO['lastmonth'].to_string(index=False)))
    data = ws_main.cell(row=15, column=7)
    data.value = int(float(tmpO['grow'].to_string(index=False)))
    data = ws_main.cell(row=16, column=7)
    data.value = int(float(tmpO['staff'].to_string(index=False)))
    #北高 月底人數 / 增減 / 本年計劃人數
    data = ws_main.cell(row=14, column=8)
    data.value = int(float(tmpP['lastmonth'].to_string(index=False)))
    data = ws_main.cell(row=15, column=8)
    data.value = int(float(tmpP['grow'].to_string(index=False)))
    data = ws_main.cell(row=16, column=8)
    data.value = int(float(tmpP['staff'].to_string(index=False)))
    #南高 月底人數 / 增減 / 本年計劃人數
    data = ws_main.cell(row=14, column=9)
    data.value = int(float(tmpQ['lastmonth'].to_string(index=False)))
    data = ws_main.cell(row=15, column=9)
    data.value = int(float(tmpQ['grow'].to_string(index=False)))
    data = ws_main.cell(row=16, column=9)
    data.value = int(float(tmpQ['staff'].to_string(index=False)))
    #中南直營 月底人數 / 增減 / 本年計劃人數
    data = ws_main.cell(row=14, column=10)
    data.value = int(float(tmpR['lastmonth'].to_string(index=False)))
    data = ws_main.cell(row=15, column=10)
    data.value = int(float(tmpR['grow'].to_string(index=False)))
    data = ws_main.cell(row=16, column=10)
    data.value = int(float(tmpR['staff'].to_string(index=False)))

    wb.save(wordpath)

    #Excel 轉 PDF
    pdfPath = path
    doc2pdf(wordpath, pdfPath)   

    return ReportSendDate

if __name__ == "__main__":
    
    
    print(now.strftime("%Y-%m-%d"))
    # 設定日期
    script = 'send_hr_notify_project'

    SendDate1 = now.strftime("%Y%m%d")
    SendDate2 = now.strftime("%Y%-m%-d")
    try:
        # 讀取資料寫入EXCEL轉PDF
        DF_Send = mergeData()
        ## 取得設定發送日
        google_send_date=str(DF_Send['lastmonth'].to_string(index=False).replace("-","").replace("/","")).strip()
        
        if (google_send_date==SendDate1 or google_send_date==SendDate2):
            print('Part 1 done!')
        else:
            sys.exit(1)
            
    except Exception as e:
        msg = '{0}: PART 1: 填入HR[租賃營業本部]值失敗! The error message : {1}'.format(script, e)
        print(msg)
        sys.exit(1)

    try:
        # PDF轉圖檔
        images = convert_from_path(path + 'HR.pdf')
        images[0].save(path + 'hr_project.png')
        # 裁圖另存
        img = Image.open(path + 'hr_project.png')
        
        # img.crop(左，上，右，下)
        # 正式區
        im1 = img.crop((95, 140, 1560, 830))
        
        # 測試區
        #im1 = img.crop((95, 140, 1560, 925))
        

        im1.save(path + 'hr_project_crop.png', quality=50) 
        
        print('Part 2 done!')
    except Exception as e:
        msg = '{0}: PART 2: HR[租賃營業本部]轉檔失敗! The error message : {1}'.format(script, e)
        print(msg)
        sys.exit(1)

    try:
        # 發送訊息 receiver=> j [測試] / g [正式]
        receiver = 'g'
        file = 'hr_project_crop.png'
        #message = "【租賃營業本部】(%s)" % end_date
        message = "【租賃營業本部】人數月報"
        # \n Line 換行符號       
        message =  "\n" + message   
        HR_Line_Notify(receiver = receiver, path = path,msg = message, FN = file)
        print('Part 3 done!')
    except Exception as e:
        msg = '{0}: PART 3: HR[租賃營業本部]Notify 失敗! The error message : {1}'.format(script, e)
        print(msg)
        sys.exit(1)