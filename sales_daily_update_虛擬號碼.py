# coding: utf-8
import pandas as pd
import numpy as np
import os
import sys
sys.path.insert(0, '/home/hadoop/jessielu/')

import matplotlib as mpl
import matplotlib.pyplot as plt
import functools
from openpyxl import load_workbook

from datetime import datetime, timedelta
from Module.ConnFunc import read_mysql
from Module.HelpFunc import display_set
from Module.EmailFunc import send_daily_report, send_daily_report_error

display_set(100, 30)
mpl.pyplot.switch_backend('agg')

path = '/home/hadoop/jessielu/daily_report/files/'
#path = '/home/jessielu/PycharmProjects/daily_report/'
os.chdir(path)


# 建立日期區間
start_date = (datetime.today() - timedelta(1)).strftime("%Y-%m-%d")
end_date = (datetime.today() - timedelta(0)).strftime("%Y-%m-%d")

# 建立MySQL-report連線
# PART 1: 讀取數據
def read_data():
    query1 = f"""
    select date Date, Category, VIPLevel, count(*) Sales from report.status_new
    where date >= '{start_date}' and date <= '{end_date}' group by date, Category, VIPLevel
    """
    # 讀取會員數
    query2 = f"""
    select date Date, Category, VIPLevel, count(*) Members from report.mstatus_vip
    where date >= '{start_date}' and date <= '{end_date}' group by date, Category, VIPLevel
    """
    # 讀取個人賣家會員數
    query3 = f"""
    select Date, Numbers Members from report.mstatus_all
    where Category = 2 and date >= '{start_date}' and date <= '{end_date}'
    """
    # 讀取車格空格數
    query4 = f"""
    SELECT Date, count(*) Empties from report.empty_new where Date >= '{start_date}' and
    Date <= '{end_date}' group by Date
    """
    # 讀取GA網站整體成效(瀏覽數, 到站人次）
    query5 = f"""
    select a.QueryDate Date, a.visitor, a.New_visitor, a.all from CollectGA.ga_visitor_report_new a
    where QueryDate >= '{start_date}' and 
    QueryDate <= '{end_date}'
    """
    # 讀取GA物件瀏覽數
    query6 = f"""
    select substr(date, 1, 10) Date, sum(totalEvents) from ga_main
    where eventAction like '/car/' and date >= '{start_date}' and date <= '{end_date}' group by date
    """
    # 讀取虛擬號碼數
    query7 = f"""
    select 日期 Date, 狀態, count(*) from report.virtual where 日期 >= '{start_date}' and 日期 <= '{end_date}' 
    group by 日期, 狀態
    """
    # 讀取有上架車商會員數
    query8 = f"""
    select Date, Category, VIPLevel, count(distinct(MemberID)) from report.status_new 
    where Date >= '{start_date}' and Date <= '{end_date}' group by Date, Category, VIPLevel
    """
    # 讀取站上物件數
    query9 = f"""
    SELECT * from report.on_shelf where Date >= '{start_date}' and Date <= '{end_date}'
    """
    query10 = f"""
    select a.Unique Combine, a.Group_2 from report.index_memcategory a
    """

    # 執行讀取程式
    car_status = read_mysql(query1, 'report')
    mem_status = read_mysql(query2, 'report')
    person_status = read_mysql(query3, 'report')
    empty = read_mysql(query4, 'report')
    ga = read_mysql(query5, 'CollectGA')
    ga_car = read_mysql(query6, 'report')
    virtual = read_mysql(query7, 'report')
    upload_members = read_mysql(query8, 'report')
    on_shelf = read_mysql(query9, 'report')
    index_mcategory = read_mysql(query10, 'report')

    # 檢查筆數是否為0
    tbl_in_list = pd.DataFrame({'car_status_rows': [len(car_status)],
                                'mem_status_rows': [len(mem_status)],
                                'person_status_rows': [len(person_status)],
                                'empty_status_rows': [len(empty)],
                                'ga_ov_rows': [len(ga)],
                                'ga_car_rows': [len(ga_car)],
                                'virtual_rows': [len(virtual)],
                                'upload_members_rows': [len(upload_members)],
                                'on_shelf_rows': [len(on_shelf)]})

    return car_status, mem_status, person_status, empty, ga, ga_car, virtual, upload_members, \
           on_shelf, index_mcategory, tbl_in_list


# PART 2: 上架數+會員數+個人賣家會員數+站上物件數新增'Combine'欄位
# 會員身份mapping表格重新命名使其一致
def add_combine_col():

    # 修改欄位格式Combine
    index_mcategory.Combine = index_mcategory.Combine.map(str)

    # 新增欄位：Category + VIPLevel = Combine
    dlist=[car_status, mem_status, on_shelf, upload_members]
    clist=['Category','VIPLevel']

    for data in dlist:
        for col in clist:
            data[col] = data[col].astype(int).astype(str)

        data['Combine']=data['Category']+data['VIPLevel']
        data=data.drop(columns=['Category', 'VIPLevel'], axis=1, inplace=True)

    # 個人賣家補Combine欄位
    person_status['Combine']='20'

    # 將會員數資料整合在一起
    mem_status_all = mem_status.append(person_status)

    return index_mcategory, mem_status_all, car_status, on_shelf, upload_members


# PART 3: 虛擬號碼數據整理
def virtual_call_cal(dt):
    # 通話10秒以上設定為成功通話數 #其他狀態設為未成功通話數
    call_type = [
        (dt['狀態'] == '通話10秒內') | (dt['狀態'] == '通話5分鐘內') | (dt['狀態'] == '通話5-10分鐘內') |
        (dt['狀態'] == '通話超過10分鐘'),
        (dt['狀態'] == '消費者等待時掛斷') | (dt['狀態'] == '車商聽語音時掛斷') | (dt['狀態'] == '車商未接') |
        (dt['狀態'] == '消費者撥打後立刻掛斷') | (dt['狀態'] == '消費者未輸入分機') | (dt['狀態'] == '車商未按任意鍵') |
        (dt['狀態'] == '消費者輸入的分機錯誤') | (dt['狀態'] == '未設定車商轉接號碼')]
    call_category = ['成功通話數', '未成功通話數']

    # 清整及轉置成所需的格式
    dt['通話數'] = np.select(call_type, call_category)
    dt = dt.groupby(['Date', '通話數'])['count(*)'].sum().reset_index(name='撥打數')
    dt_r = dt.pivot(index='Date', columns='通話數', values='撥打數').reset_index()
    dt_r['總通話數'] = dt_r.iloc[:, 1:3].sum(axis=1).astype(float)

    return dt_r


# PART 4: 產出以會員身份為主的表格
def groupby_transpose(dt, values, value_name):

    select_dt = dt.copy()

    # 重新命名為原廠認證
    index_mcategory.loc[index_mcategory.Group_2 == '原廠', 'Group_2'] = '原廠認證'

    # 取得Group_2欄位
    select_dt = pd.merge(select_dt, index_mcategory, on='Combine', how='left')

    # 轉置成所需資料格式
    select_dt = select_dt.groupby(['Date', 'Group_2'])[values].sum().reset_index()
    select_dt = select_dt.pivot(index='Date', columns='Group_2',
                                          values=[values]).reset_index()

    select_dt.columns = select_dt.columns.droplevel()
    select_dt.rename(columns={'':'Date'}, inplace=True)

    # 重新命名欄位
    for col in select_dt.columns[1:]:
        select_dt=select_dt.rename(columns={'{0}'.format(col):'{0}{1}'.format(col, value_name)})

    return select_dt


# PART 5: 產出總量及資料整合main sheet的表格
def organize_sheetMain():

    # 計算總量
    members_dt['總會員數'] = members_dt.loc[:, ['一般車商會員數', '原廠認證會員數', '平輸好店會員數', '網路好店會員數',
                                               '買賣顧問會員數']].sum(axis=1)
    cstatus_dt['總上架數'] = cstatus_dt.iloc[:, 1:].sum(axis=1)
    onshelf_dt['總待售數'] = onshelf_dt.iloc[:, 1:].sum(axis=1)

    # 重新命名欄位
    empty.rename(columns={'Empties':'總空格數'}, inplace=True)
    ga_car.rename(columns={'sum(totalEvents)':'物件總瀏覽數'}, inplace=True)
    ga.columns = ['Date', '到站人次', '到站新客', '總瀏覽數']

    # 修改ga data欄位格式
    try:
        ga.Date = ga.Date.apply(lambda x: datetime.strftime(x, '%Y-%m-%d'))
    except:
        pass

    # 整合表格
    dfs = [members_dt, cstatus_dt, onshelf_dt, empty, ga, ga_car, vc_r]
    full_dt = functools.reduce(lambda left, right: pd.merge(left, right, on='Date', how='outer'), dfs)

    # EXCEL中sheet 'main'表
    order_list = ['Date', '一般車商會員數', '原廠認證會員數', '平輸好店會員數', '網路好店會員數', '買賣顧問會員數', '總會員數',
                  '一般車商上架數', '個人賣家上架數', '原廠認證上架數', '平輸好店上架數', '網路好店上架數', '買賣顧問上架數',
                  '總上架數', '一般車商待售數', '平輸好店待售數', '網路好店待售數', '買賣顧問待售數', '總待售數',
                  '總空格數', '到站人次', '到站新客', '總瀏覽數', '物件總瀏覽數', '成功通話數', '未成功通話數', '總通話數']

    # 如果欄位不存增加欄位補0
    cur_col = full_dt.columns
    miss_col = list(set(order_list) - set(cur_col))
    for col in miss_col:
        full_dt[col] = 0

    # 排序
    main_dt = full_dt[order_list]
    main_dt.iloc[:, 1:] = main_dt.iloc[:, 1:].fillna(0).astype(int)

    return main_dt


# PART 6: 產出總量及資料整合side sheet的表格
def organize_sheetSide(dt):
    up_mem = dt.copy()
    up_mem['總上架會員數'] = up_mem.iloc[:, 1:].sum(axis=1)
    # EXCEL中sheet 'side'表
    order_list = ['Date', '一般車商上架會員數', '原廠認證上架會員數', '平輸好店上架會員數', '網路好店上架會員數',
                  '買賣顧問上架會員數', '個人賣家上架會員數', '總上架會員數']
    side_dt = up_mem[order_list]
    return side_dt


# PART 7: 數據寫入excel中
def append_to_excel():

    # 將資料寫入工作表
    wb = load_workbook(path + 'sales_update.xlsx')
    # 選擇第一個工作表 'main'工作表
    ws_main = wb.worksheets[0]
    # 擷取需要append至Excel的值
    row_main = list(main_dt.iloc[0, :])
    ws_main.append(row_main)
    # 選擇第二個工作表 'side'工作表
    ws_side = wb.worksheets[1]
    # 擷取需要append至Excel的值
    row_side = list(side_dt.iloc[0,:])
    ws_side.append(row_side)
    # SAVE the worksheet
    wb.save(path+'sales_update.xlsx')


# PART 8: 數據寫入image中
def append_to_graph():
# 擷取製圖需要的數據
    b1day_dt = main_dt[main_dt['Date'] == start_date]

    x1 = b1day_dt.loc[:, '一般車商上架數'].values
    x2 = b1day_dt.loc[:, '原廠認證上架數'].values
    x3 = b1day_dt.loc[:, '平輸好店上架數'].values
    x4 = b1day_dt.loc[:, '網路好店上架數'].values
    x5 = b1day_dt.loc[:, '買賣顧問上架數'].values
    x6 = b1day_dt.loc[:, '總上架數'].values
    x7 = b1day_dt.loc[:, '到站人次'].values
    x8 = b1day_dt.loc[:, '到站新客'].values
    x9 = b1day_dt.loc[:, '總瀏覽數'].values
    x10 = b1day_dt.loc[:, '物件總瀏覽數'].values

    labels = f'一般車商上架數{x1}', f'原廠認證上架數{x2}', f'平輸好店上架數{x3}', f'網路好店上架數{x4}', \
             f'買賣顧問上架數{x5}', f'總上架數{x6}', f'到站人次{x7}', f'到站新客{x8}', f'總瀏覽數{x9}', f'總物件瀏覽數{x10}'
    size = [10, 10, 10, 10, 10, 10, 10, 10, 10, 10]
    explode = (0, 0, 0, 0, 0, 0, 0, 0, 0, 0)

    # 設置製圖的變數
    mpl.rcParams[u'font.sans-serif'] = ['simhei']
    mpl.rcParams['axes.unicode_minus'] = False
    mpl.rcParams['font.size'] = 20.0
    mpl.rcParams['figure.figsize'] = 14, 9
    colors = ['red', 'orange', 'yellow', 'green', 'blue', 'lightblue', 'purple', 'pink', 'lightgreen', 'grey']

    # 選取圖形為圓餅圖
    plt.pie(size, labels=labels, colors=colors, shadow=True, explode=explode)

    # 顯示圖中間的字體
    plt.text(-0.55, 0.03, '{0}日營運指標'.format(start_date), size=25)

    centre_circle = plt.Circle((0, 0), 0.6, color='grey', fc='white', linewidth=1)
    fig = plt.gcf()
    fig.gca().add_artist(centre_circle)
    plt.axis('equal')
    fig.savefig(path+'sales_graph')


if __name__ == "__main__":

    try:
        car_status, mem_status, person_status, empty, ga, ga_car, virtual, upload_members, \
                   on_shelf, index_mcategory, tbl_in_list = read_data()

        if (0 in tbl_in_list.values) == True:
            msg = 'PART 1: 數據中有0值, 此程式停止執行!'
            send_daily_report_error(msg)
            sys.exit(1)
        else:
            print('PART 1: 數據正確且無0值')

    except Exception as e:
        msg = 'PART 1:讀取數據發生錯誤. The error message : {}'.format(e)
        send_daily_report_error(msg)
        print(msg)
        sys.exit(1)

    try:
        index_mcategory, mem_status_all, car_status, on_shelf, upload_members = add_combine_col()
        msg = 'PART 2 : 新增Combine欄位正確'
        print(msg)
    except Exception as e:
        msg = 'PART 2: 新增Combine欄位發生錯誤. The error message :{}'.format(e)
        send_daily_report_error(msg)
        print(msg)
        sys.exit(1)

    try:
        vc_r=virtual_call_cal(virtual)
        msg = 'PART 3 : 虛擬號碼數據整理OK'
    except Exception as e:
        msg = 'PART 3 : 虛擬號碼數據整理發生錯誤. The error messsage :{}'.format(e)
        send_daily_report_error(msg)
        print(msg)
        sys.exit(1)

    try:
        members_dt = groupby_transpose(mem_status_all, 'Members', '會員數')
        cstatus_dt = groupby_transpose(car_status, 'Sales', '上架數')
        onshelf_dt = groupby_transpose(on_shelf, 'ONshelf', '待售數')
        up_members = groupby_transpose(upload_members, 'count(distinct(MemberID))', '上架會員數')
        msg = 'PART 4 : 產出以會員身份為主的數據OK'
        print(msg)
    except Exception as e:
        msg = 'PART 4 : 產出以會員身份為主的數據發生錯誤. The error message :{}'.format(e)
        send_daily_report_error(msg)
        print(msg)
        sys.exit(1)

    try:
        main_dt = organize_sheetMain()
        side_dt = organize_sheetSide(up_members)
        msg = 'PART 5~6 : 資料整合OK'
        print(msg)
    except Exception as e:
        msg = 'PART 5~6 : 資料整合發生錯誤. The error message : {}'.format(e)
        send_daily_report_error(msg)
        print(msg)
        sys.exit(1)

    try:
        append_to_excel()
        append_to_graph()
        msg = 'PART 7~8 : 數據寫入Excel/Graph OK'
        print(msg)
    except Exception as e:
        msg = 'PART 7~8 : 數據寫入Excel/Graph出現錯誤. The error message : {}'.format(e)
        send_daily_report_error(msg)
        print(msg)
        sys.exit(1)

    try:
        send_daily_report(receiver='g', path=path)
        msg = 'PART 9 : 寄送日報OK'
        print(msg)
    except Exception as e:
        msg = 'PART 9 : 寄送日報發生錯誤. The error message : {}'.format(e)
        send_daily_report_error(msg)
        print(msg)
        sys.exit(1)

# full_dt.fillna(0, inplace=True)
# mem_uploads_num.fillna(0, inplace=True)
#
# writer = pd.ExcelWriter('/media/sf_F_DRIVE/temp/sales_update_revise.xlsx', engine='openpyxl')
# full_dt.to_excel(writer, sheet_name='main', index=False)
# mem_uploads_num.to_excel(writer, sheet_name='side', index=False)
# workbook = writer.book
# worksheet1 = writer.sheets['main']
# worksheet2 = writer.sheets['side']
# writer.save()
