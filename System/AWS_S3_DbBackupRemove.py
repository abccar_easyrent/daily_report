from datetime import datetime, timedelta, date
from dateutil.relativedelta import relativedelta
import re
import boto3

r = re.compile("(ABCCar|ABCCarMemberCenter)_[\d]{4}-[\d]{2}-[\d]{2}.bak")

#now = date.today()
now = datetime.now()+ timedelta(hours=8)

#保留 7 天備份檔
dates = []
for x in range(0,8):
    if x == 0:
        WeekLastDay = date.today() + relativedelta(days=x)
    else:
        WeekFirstDay = date.today() + relativedelta(days=-x)
    dates.append('{YMD}.bak'.format(YMD=(date.today() + relativedelta(days=-x)).strftime("%Y-%m-%d")))  
#print(dates)

print("Execute " + now.strftime("%Y-%m-%d %H:%M:%S") + " , Keep " + WeekFirstDay.strftime("%Y-%m-%d") + " ~ " + WeekLastDay.strftime("%Y-%m-%d") + " bak files.")

session = boto3.Session( 
         aws_access_key_id='AKIA5VB4VRKJFCIQ6I2S',
         aws_secret_access_key='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',
         region_name='ap-northeast-1')

s3 = session.resource('s3')
my_bucket = s3.Bucket('abc-sqlserver')

#測試刪除
#my_bucket.Object("ABCCarMemberCenter_2022-06-09.bak").delete()

# 7日內備份檔案
ABCCarList = []
ABCCarMemberCenterList = []
# 超過7日備份檔案
ABCCar_Droplist = []
ABCCarMemberCenter_Droplist = []

#objs = my_bucket.objects.all():
objs = my_bucket.objects.filter(Prefix="ABCCar")

for obj in objs:
    #if obj.key.startswith('ABCCar_') or obj.key.startswith('ABCCarMemberCenter_'):
    #    print(obj.key)    
    if r.match(obj.key):
        str = obj.key.replace('ABCCarMemberCenter_', '').replace('ABCCar_', '')
        if str in dates:
            if obj.key.startswith('ABCCarMemberCenter'):
                ABCCarMemberCenterList.append(obj.key)
            else:
                ABCCarList.append(obj.key)           
        else:
            if obj.key.startswith('ABCCarMemberCenter'):
                ABCCarMemberCenter_Droplist.append(obj.key)
            else:
                ABCCar_Droplist.append(obj.key)           


#檢查無7日內備份檔,不執行刪除備份檔案
if len(ABCCarList)>0:
    #print(ABCCarList)
    #print(ABCCar_Droplist)
    for x in ABCCar_Droplist:
        my_bucket.Object(x).delete()
        print("Delete " +x)
    
    
if len(ABCCarMemberCenterList)>0:
    #print(ABCCarMemberCenterList)
    #print(ABCCarMemberCenter_Droplist)
    for x in ABCCarMemberCenter_Droplist:
        my_bucket.Object(x).delete()
        print("Delete " + x)